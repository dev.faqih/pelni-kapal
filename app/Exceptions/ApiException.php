<?php
/**
 * Created by IntelliJ IDEA.
 * User: almustafa dekaulitz (sulaimanfahmi@gmail.com)
 * Date: 02/06/2018
 * Time: 10:25
 */

namespace App\Exceptions;


class ApiException extends \Exception
{
    protected $status = array();
    protected $messageDetail = "";

    /**
     * Constructor
     *
     * @param string $message Error message
     * @param int $code HTTP status code
     * @param string $responseHeaders HTTP response header
     * @param mixed $responseBody HTTP body of the server response either as Json or string
     */
    public function __construct($status = array(), $message, $code = 0)
    {
        parent::__construct("", $code);
        $this->status = $status;
        $this->messageDetail = $message;
    }

    /**
     * @return array
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * @return string
     */
    public function getMessageDetail()
    {
        return $this->messageDetail;
    }
}