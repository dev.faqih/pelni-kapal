<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 25/09/2017
 * Time: 22:35
 */

namespace Exceptions;


class AuthorizationException extends \Exception
{
    protected $status = array();
    protected $messageDetail = "";

    /**
     * Constructor
     *
     * @param string $message Error message
     * @param int $code HTTP status code
     * @param string $responseHeaders HTTP response header
     * @param mixed $responseBody HTTP body of the server response either as Json or string
     */
    public function __construct($status = array(), $message, $code = 0)
    {
        parent::__construct("", $code);
        $this->status = $status;
        $this->messageDetail = $message;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return array
     */
    public function getMessageDetail()
    {
        return $this->messageDetail;
    }


}