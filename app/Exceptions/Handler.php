<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $e
     * @return mixed
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof NotFoundHttpException)
            return redirect("/page-not-found");

        else if ($e Instanceof ApiException) {
            return \response()->json([
                "meta" => [
                    "status" => $e->getStatus(),
                    "detailMessage" => $e->getMessageDetail()
                ],
                "data" => []
            ], 422);
        } else if ($e instanceof DefaultException) {
            return redirect()->back()->with("errMessage", $e->getMessage());
        }
         return parent::render($request, $e);
    }
}
