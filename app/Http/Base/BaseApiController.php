<?php
/**
 * This project has been generated by nuts generator
 * nuts has been made by fahmi sulaiman
 * sulaimanfahmi@gmail.com
 */


namespace App\Http\Base;

use App\Exceptions\ApiException;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Providers\IAM\Core\AuthManager\Jwt;
use App\Http\Controllers\Providers\IAM\Core\AuthManager\StatusCode;
use App\Http\Models\MenusModel;
use Hamcrest\Thingy;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class BaseApiController extends Controller
{

    public $data = [];
    protected $baseService = "";
    protected $service;
    protected $mainMenu;

    public function __construct()
    {
        $this->baseService;

    }

    public function jwt($userId, $loginAs = 0)
    {
        $jwt = new Jwt();
        $jwt->setLogginAs($loginAs);
        $jwt->setId($userId);
        return $jwt->buldJwtToken()->getJwtToken();

    }

    public function extractJwt($token)
    {
        Jwt::jwt()->setJwtToken($token);
        $token = Jwt::jwt();
        return $token->extractJwtToken();
    }

    public function checkToken($request)
    {
        $request = \Request::instance();
        $token = $request->header("Authorization");
        if (empty($request->header("Authorization"))) {
            throw new ApiException(StatusCode::$tokenNotFound_error, "token not found");
        } else {
            $this->extractJwt($token);
        }
    }

    public function responseOk($data = array())
    {
        $res = [
            "meta" => [
                "status" => StatusCode::$success,
                "requestTime" => (new \DateTime())->getTimestamp(),
                "version" => "v.1"
            ],
            "data" => $data
        ];
        return \response()->json($res);
    }

}