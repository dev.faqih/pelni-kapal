<?php
/**
 * Created by IntelliJ IDEA.
 * User: Fahmi Sulaiman
 * Date: 22/12/2017
 * Time: 1:36
 */

namespace App\Http\Base;


use App\Http\Controllers\Providers\IAM\Core\Mail\CoreMail;
use App\Http\Controllers\Providers\IAM\Core\Mail\TemplateBuilder;

class BaseMail
{
    protected $receiver;
    protected $content;
    protected $subject;

    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }


    /**
     * @param mixed $receiver
     */
    public function setReceiver($receiver)
    {
        $this->receiver = $receiver;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $template = new TemplateBuilder();
        $template->setContent($content);
        $this->content = $template->getTemplateEmail();
    }


    public function sendMail($subject,$receiver)
    {
        $mail= new CoreMail();
        $mail->setSubject($subject);
        $mail->setMailReceiver($receiver);
        $mail->setBody($this->content);
        return $mail->send();
    }


}