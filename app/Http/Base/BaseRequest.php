<?php
/**
 * Created by IntelliJ IDEA.
 * User: CODE.ID
 * Date: 07/01/2018
 * Time: 23:14
 */

namespace App\Http\Base;


use Illuminate\Http\Request;


class BaseRequest
{
    private static $instance;
    /**
     * @return Request
     */
    static function instance()
    {
        if (self::$instance == null) {
            self::$instance = \Request()->instance();
        }
        return \Request()->instance();
    }

}