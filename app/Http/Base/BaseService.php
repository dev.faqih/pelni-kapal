<?php
/**
 * Created by IntelliJ IDEA.
 * User: Fahmi Sulaiman
 * Date: 09/11/2017
 * Time: 13:32
 */

namespace App\Http\Base;


use App\Http\Handler\CurlHandler;

class BaseService
{
    public $model = null;

    public function __construct()
    {

    }

    /**
     * @return bool
     */
    public function hasInternetConnection()
    {
        $is_conn = false;
        $connected = @fsockopen($GLOBALS["env"]["HEAD_OFFICE_URL"], 80);
        //website, port  (try 80 or 443)
        if ($connected) {
            $is_conn = true; //action when connected
            fclose($connected);
        }
        return $is_conn;
    }


    public function postApi()
    {
        return new CurlHandler();
    }
}