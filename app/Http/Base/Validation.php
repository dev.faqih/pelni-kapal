<?php
/**
 * Created by IntelliJ IDEA.
 * User: Fahmi Sulaiman
 * Date: 07/11/2017
 * Time: 22:47
 */

namespace App\Http\Base;


class Validation
{
    public $error = [];

    public function hasError()
    {
        if (!empty($this->getError())) {
            return true;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getError()
    {
        return array_collapse($this->error);
    }

    public function validateView()
    {
        return redirect()->back()->withInput()->with("errValidation", $this->getError());
    }

    public function validateJson()
    {
        return response()->json($this->getError())->setStatusCode(422);
    }
}