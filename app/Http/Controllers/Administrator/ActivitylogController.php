<?php
/**
 * This project has been generated by worker generator
 * has been made by fahmi sulaiman
 * sulaimanfahmi@gmail.com
 * check out my github @dekaulitz
 */

namespace App\Http\Controllers\Administrator;

use App\Exceptions\DefaultException;
use App\Http\Base\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Services\ActivitylogService;
use App\Http\Validators\ActivitylogValidator;
use App\Http\Services\UsersService;

class ActivitylogController extends BaseController
{
    protected $baseService;
    protected $validator;
    private $users;

    /**
     * Activitylog constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->baseService = new ActivitylogService();
        $this->validator = new ActivitylogValidator();
        $this->users = new UsersService();

    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws DefaultException
     */
    public function store(Request $request)
    {
        $this->validator->beforeInsert($request->all());
        if ($this->validator->hasError())
            return $this->validator->validateView();
        DB::beginTransaction();
        try {
            $this->baseService->store($request->all());
            DB::commit();
            $this->sync();
            return redirect()->to("administrator/activitylog")->with("successMessages", "Data has been created");
        } catch (\Exception $e) {
            DB::rollBack();
            throw new DefaultException($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function paging(Request $request)
    {
        $this->data["criteria"] = [
            "module" => "Module"
            , "date" => "Date"
            , "refId" => "Ref ID"
        ];
        $this->data["ordering"] = [
             "date:asc" => "Date &uarr;"
            , "date:desc" => "Date &darr;"
            , "namaTransaksi:asc" => "Module &uarr;"
            , "namaTransaksi:desc" => "Module &darr;"
            , "refId:asc" => "Ref. ID &uarr;"
            , "refId:desc" => "Ref. ID &darr;"
        ];
        $this->data["activitylogs"] = $this->baseService->paging($request);
        return view("modules.activitylog.activitylog_index", $this->data);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $this->data["userss"] = $this->users->index();
        $this->data["activitylog"] = $this->baseService->show($id);
        return view("modules.activitylog.activitylog_edit", $this->data);
    }


    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws DefaultException
     */
    public function update(Request $request, $id)
    {
        $this->validator->beforeUpdate($request->all(), $id);
        if ($this->validator->hasError())
            return $this->validator->validateView();
        DB::beginTransaction();
        try {
            $this->baseService->update($id, $request->all());
            DB::commit();
            $this->sync();
            return redirect()->to("administrator/activitylog")->with("successMessages", "Data has been updated");
        } catch (\Exception $e) {
            DB::rollBack();
            throw new DefaultException($e->getMessage());
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $this->data["userss"] = $this->users->index();
        return view("modules.activitylog.activitylog_create", $this->data);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $this->data["userss"] = $this->users->index();
        $this->data["activitylog"] = $this->baseService->show($id);
        return view("modules.activitylog.activitylog_show", $this->data);
    }

    /**
     *  index function that get all data from database by default
     * @param $ids
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function destroy($ids)
    {
        $this->baseService->delete($ids);
        return redirect()->back()->with("successMessages", "Data has been deleted");
    }


}