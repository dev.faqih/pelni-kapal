<?php

namespace App\Http\Controllers\Administrator;

use App\Exceptions\DefaultException;
use App\Http\Base\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Services\AdjustmentbbmsService;
use App\Http\Models\ItemModel;
use App\Http\Services\ApplicationparameterService;
use App\Http\Services\VoyagesService;
use App\Http\Services\StokkapalService;
use App\Http\Services\ActivitylogService;
use App\Http\Services\LokasiitemService;
use App\Http\Models\LokasiitemModel;

class AdjustmentbbmController extends BaseController
{
    protected $baseService;
    protected $validator;
    private $voyage;
    private $activityLog;
    private $appParam;

    public function __construct()
    {
        parent::__construct();
        $this->baseService = new AdjustmentbbmsService();
        $this->stokKapal = new StokkapalService();
        $this->voyage = new VoyagesService();
        $this->location = new LokasiitemService();
        $this->appParam = new ApplicationparameterService();
        $this->activityLog = new ActivitylogService();
    }

    public function paging(Request $request)
    {
        $this->data["voyages"] = $this->voyage->getVoyages($request);
        $this->data["adjustmentBBMs"] = $this->baseService->paging($request);
        return view("modules.adjustmentbbm.adjustmentbbm_index", $this->data);
    }

    public function show(Request $request, $id)
    {
        $this->data["bbm"] = $this->baseService->show($id);
        if ($this->data["bbm"]) {
            $this->data["locations"] = $this->location->index($request);
            return view("modules.adjustmentbbm.adjustmentbbm_show", $this->data);
        } else {
            return redirect()->back()->with("errMessage", "No data found. Please check this with your Administrator/Nahkoda.");
        }
    }
    public function create(Request $request)
    {
        $auth = json_decode($request->session()->get('auth'));
        $categoryBBM = explode(";", $auth->sysParams->PELNIBBMPELUMAS)[0];
        $this->data["bbmItem"] = $bbmItem = (new ItemModel())->getQuery()->where("type_id", "=", $categoryBBM)->where("enabled_flex", "=", "Y")->first();
        $this->data["sisaawal"] = $this->stokKapal->getStockBasedOnItemID($auth->kapalId, $bbmItem->id);
        $this->data["locations"] = $this->location->getBBM($request);
        $this->data["lotNumber"] = $this->appParam->generateLotNumber();
        $this->data["portBunker"] = explode(";", $auth->sysParams->PORTBUNKER);
        return view("modules.adjustmentbbm.adjustmentbbm_create", $this->data);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        DB::beginTransaction();
        try {
            $auth = json_decode($request->session()->get('auth'));

            $input["date"] = date("Y-m-d H:i:s");
            $input["username"] = $auth->username;
            $input["activity"] = "CREATE";
            $input["refId"] = $input["transactionNo"] = $this->baseService->getLastestID("AB" . date("y") . $auth->orgId);

            $subInv = (new LokasiitemModel())->getQuery()->where("id", "=", $input["locationId"])->first();
            if ($subInv) {
                $input["namaLokasi"] = $subInv->namaLokasi;
            } else {
                throw new Exception("Sub Inventory is invalid", 1);
            }

            $voyage = $this->voyage->show($input["voyageKode"], "voyageKode", "=");
            if ($voyage) {
                $input["voyageName"] = $voyage->voyageName;
            } else {
                throw new Exception("Voyage is invalid", 1);
            }

            $ID = $this->baseService->store($input);
            $data = $this->baseService->getPrepareUpdateStock($input);
            $this->stokKapal->updateStock($data);

            $input["deskripsi"] = $auth->name . " just <b>created</b> new Adjustment BBM <a href=\"" . url("administrator/adjustmentbbm/" . $ID) . "\">" . $input["transactionNo"] . "</a>";
            $this->activityLog->store($input);
            $this->baseService->saveStaging($input);

            DB::commit();
            $this->sync();
            return redirect()->to("administrator/adjustmentbbm/")->with("successMessages", "Data sudah berhasil disimpan dengan No #" . $input["transactionNo"]);
        } catch (\Exception $e) {
            DB::rollBack();
            throw new DefaultException($e->getMessage());
        }
    }
}
