<?php
/**
 * This project has been generated by worker generator
 * has been made by fahmi sulaiman
 * sulaimanfahmi@gmail.com
 * check out my github @dekaulitz
 */

namespace App\Http\Controllers\Administrator;

use App\Exceptions\DefaultException;
use App\Http\Base\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Services\AdministratormenuService;
use App\Http\Validators\AdministratormenuValidator;

class AdministratormenuController extends BaseController
{
    protected $baseService;
    protected $validator;

    /**
     * Administratormenu constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->baseService = new AdministratormenuService();
        $this->validator = new AdministratormenuValidator();

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function paging(Request $request)
    {
        $this->data["header"] = ["id" => "id"
            , "menuname" => "menuname"
            , "url" => "url"
            , "parentmenu" => "parentmenu"
            , "icon" => "icon"
            , "description" => "description"
            , "position" => "position"
        ];
        $this->data["criteria"] = ["id" => "id"
            , "menuname" => "menuname"
            , "url" => "url"
            , "parentmenu" => "parentmenu"
            , "icon" => "icon"
            , "description" => "description"
            , "position" => "position"
        ];
        $this->data["ordering"] = ["id:asc" => "id"
            , "menuname:asc" => "menuname"
            , "url:asc" => "url"
            , "parentmenu:asc" => "parentmenu"
            , "icon:asc" => "icon"
            , "description:asc" => "description"
            , "position:asc" => "position"
        ];
        $this->data["administratormenus"] = $this->baseService->paging($request->all());
        return view("modules.administratormenu.administratormenu_index", $this->data);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $this->data["administratormenu"] = $this->baseService->show($id);
        return view("modules.administratormenu.administratormenu_show", $this->data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view("modules.administratormenu.administratormenu_create", $this->data);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $this->data["administratormenu"] = $this->baseService->show($id);
        return view("modules.administratormenu.administratormenu_edit", $this->data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->data["administratormenus"] = $this->baseService->index();
        return view("modules.administratormenu.administratormenu_index", $this->data);
    }

    /**
     *  index function that get all data from database by default
     * @param $ids
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function destroy($ids)
    {
        $this->baseService->delete($ids);
        return redirect()->back()->with("successMessages", "Data has been deleted");
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws DefaultException
     */
    public function store(Request $request)
    {
        $this->validator->beforeInsert($request->all());
        if ($this->validator->hasError())
            return $this->validator->validateView();
        DB::beginTransaction();
        try {
            $this->baseService->store($request->all());
            DB::commit();
            $this->sync();
            return redirect()->to("administrator/administratormenu")->with("successMessages", "Data has been created");
        } catch (\Exception $e) {
            DB::rollBack();
            throw new DefaultException($e->getMessage());
        }
    }


    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws DefaultException
     */
    public function update(Request $request, $id)
    {
        $this->validator->beforeUpdate($request->all(), $id);
        if ($this->validator->hasError())
            return $this->validator->validateView();
        DB::beginTransaction();
        try {
            $this->baseService->update($id, $request->all());
            DB::commit();
            $this->sync();
            return redirect()->to("administrator/administratormenu")->with("successMessages", "Data has been updated");
        } catch (\Exception $e) {
            DB::rollBack();
            throw new DefaultException($e->getMessage());
        }
    }


}