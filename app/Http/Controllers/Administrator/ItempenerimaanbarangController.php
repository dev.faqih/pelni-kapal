<?php
/**
 * This project has been generated by worker generator
 * has been made by fahmi sulaiman
 * sulaimanfahmi@gmail.com
 * check out my github @dekaulitz
 */

namespace App\Http\Controllers\Administrator;

use App\Exceptions\DefaultException;
use App\Http\Base\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Services\ItempenerimaanbarangService;
use App\Http\Validators\ItempenerimaanbarangValidator;
use App\Http\Services\ItemService;
use App\Http\Services\PenerimaanbarangService;

class ItempenerimaanbarangController extends BaseController
{
    protected $baseService;
    protected $validator;
    private $item;
    private $penerimaanbarang;

    /**
     * Itempenerimaanbarang constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->baseService = new ItempenerimaanbarangService();
        $this->validator = new ItempenerimaanbarangValidator();
        $this->item = new ItemService();
        $this->penerimaanbarang = new PenerimaanbarangService();

    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws DefaultException
     */
    public function store(Request $request)
    {
        $this->validator->beforeInsert($request->all());
        if ($this->validator->hasError())
            return $this->validator->validateView();
        DB::beginTransaction();
        try {
            $this->baseService->store($request->all());            
            DB::commit();
            $this->sync();
            return redirect()->to("administrator/itempenerimaanbarang")->with("successMessages", "Data has been created");
        } catch (\Exception $e) {
            DB::rollBack();
            throw new DefaultException($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function paging(Request $request)
    {
        $this->data["header"] = ["id" => "id"
            , "itemId" => "itemId"
            , "penerimaanBarangId" => "penerimaanBarangId"
            , "quantity" => "quantity"
            , "satuan" => "satuan"
        ];
        $this->data["criteria"] = ["id" => "id"
            , "itemId" => "itemId"
            , "penerimaanBarangId" => "penerimaanBarangId"
            , "quantity" => "quantity"
            , "satuan" => "satuan"
        ];
        $this->data["ordering"] = ["id:asc" => "id"
            , "itemId:asc" => "itemId"
            , "penerimaanBarangId:asc" => "penerimaanBarangId"
            , "quantity:asc" => "quantity"
            , "satuan:asc" => "satuan"
        ];
        $this->data["itempenerimaanbarangs"] = $this->baseService->paging($request->all());
        return view("modules.itempenerimaanbarang.itempenerimaanbarang_index", $this->data);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $this->data["items"] = $this->item->index();
        $this->data["penerimaanbarangs"] = $this->penerimaanbarang->index();
        $this->data["itempenerimaanbarang"] = $this->baseService->show($id);
        return view("modules.itempenerimaanbarang.itempenerimaanbarang_edit", $this->data);
    }


    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws DefaultException
     */
    public function update(Request $request, $id)
    {
        $this->validator->beforeUpdate($request->all(), $id);
        if ($this->validator->hasError())
            return $this->validator->validateView();
        DB::beginTransaction();
        try {
            $this->baseService->update($id, $request->all());
            DB::commit();
            $this->sync();
            return redirect()->to("administrator/itempenerimaanbarang")->with("successMessages", "Data has been updated");
        } catch (\Exception $e) {
            DB::rollBack();
            throw new DefaultException($e->getMessage());
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $this->data["items"] = $this->item->index();
        $this->data["penerimaanbarangs"] = $this->penerimaanbarang->index();
        return view("modules.itempenerimaanbarang.itempenerimaanbarang_create", $this->data);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $this->data["items"] = $this->item->index();
        $this->data["penerimaanbarangs"] = $this->penerimaanbarang->index();
        $this->data["itempenerimaanbarang"] = $this->baseService->show($id);
        return view("modules.itempenerimaanbarang.itempenerimaanbarang_show", $this->data);
    }

    /**
     *  index function that get all data from database by default
     * @param $ids
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function destroy($ids)
    {
        $this->baseService->delete($ids);
        return redirect()->back()->with("successMessages", "Data has been deleted");
    }


}