<?php
/**
 * This project has been generated by worker generator
 * has been made by fahmi sulaiman
 * sulaimanfahmi@gmail.com
 * check out my github @dekaulitz
 */

namespace App\Http\Controllers\Administrator;

use App\Exceptions\DefaultException;
use App\Http\Base\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Services\PendingsyncService;


class PendingsyncController extends BaseController
{
    protected $baseService;
    /**
     * Item constructor.
     */
    public function __construct(Request $request)
    {
        parent::__construct();
        $this->baseService = new PendingsyncService();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function paging(Request $request)
    {
        $auth = json_decode($request->session()->get('auth'));
        
        $this->data["ordering"] = [
            "table_name:asc"     => "Nama Table &uarr;"
            , "table_name:desc"  => "Nama Table &darr;"
            , "updated_at:asc"   => "Date Updated &uarr;"
            , "updated_at:desc"  => "Date Updated &darr;"
        ];

        
        
        if($auth->aplikasi == "HO") {
            $this->data["logs"] = $this->baseService->pagingHO($request);
            return view("modules.pendingsync.pendingho_index", $this->data);
        } else {
            // $this->data["tables"] = $this->baseService->getTableName();
            $this->data["logs"] = $this->baseService->pagingKapal($request);
            return view("modules.pendingsync.pendingkapal_index", $this->data);
        }
    }
}