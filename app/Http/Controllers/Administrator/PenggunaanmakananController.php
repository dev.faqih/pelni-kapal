<?php
/**
 * This project has been generated by worker generator
 * has been made by fahmi sulaiman
 * sulaimanfahmi@gmail.com
 * check out my github @dekaulitz
 */

namespace App\Http\Controllers\Administrator;

use App\Exceptions\DefaultException;
use App\Http\Base\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Services\PenggunaanmakananService;
use App\Http\Validators\PenggunaanmakananValidator;
// use App\Http\Services\KapalService;
use App\Http\Services\LokasiitemService;
use App\Http\Services\WorkflowService;
use App\Http\Services\ItempenggunaanmakananService;
// use App\Http\Services\UsersService;
use App\Http\Services\VoyagesService;
use App\Http\Services\AccountService;
use App\Http\Services\StokkapalService;
use App\Http\Services\ActivitylogService;

class PenggunaanmakananController extends BaseController
{
    protected $baseService;
    protected $validator;
    // private $kapal;
    private $lokasiitem;
    private $account;
    private $itempenggunaanmakanan;
    private $voyage;
    private $workflow;
    private $stokkapal;
    private $activityLog;
    
    // private $users;

    /**
     * Penggunaanmakanan constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->baseService = new PenggunaanmakananService();
        $this->validator = new PenggunaanmakananValidator();
        // $this->kapal = new KapalService();
        $this->workflow = new WorkflowService();
        $this->lokasiitem = new LokasiitemService();
        $this->account = new AccountService();
        $this->itempenggunaanmakanan = new ItempenggunaanmakananService();
        $this->voyage = new VoyagesService();
        // $this->users = new UsersService();
        $this->stokkapal = new StokkapalService();
        $this->activityLog= new ActivitylogService();

    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws DefaultException
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $this->validator->beforeInsert($input);
        if ($this->validator->hasError())
            return $this->validator->validateView();
        DB::beginTransaction();
        try {            
            $auth = json_decode($request->session()->get('auth'));

            // $input["refId"] = $input["msNumber"] = $this->baseService->getLastestID("PELNI/" . $input["tipeTransaksi"] . " " . date("y") . " " . $input["kodeKapal"]);   
            $input["refId"] = $input["msNumber"] = $this->baseService->getLastestID($input["tipeTransaksi"] . date("y") . $auth->orgId);
            $ID = $this->baseService->store($input);
            $this->itempenggunaanmakanan->store($input);
            $this->workflow->store($input);

            $input["date"] = date("Y-m-d H:i:s");
            $input["username"] = $auth->username;
            $input["activity"] = "CREATE";
            $input["deskripsi"] = $auth->name . " just <b>created</b> new Mics. Issue <a href=\"" . url("administrator/penggunaanmakanan/" . $ID ) . "\">" . $input["refId"] . "</a>";
            $this->activityLog->store($input);

            DB::commit();
            $this->sync();
            return redirect()->to("administrator/penggunaanmakanan")->with("successMessages", "Data sudah berhasil disimpan dengan No #" . $input["refId"]);
        } catch (\Exception $e) {
            DB::rollBack();
            throw new DefaultException($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function paging(Request $request)
    {
        $this->data["header"] = [
            "id" => "ID"
            , "namaLokasi" => "Asal"
            , "destinationAccount" => "Account"
            , "date" => "Tanggal Penggunaan"
        ];
        $this->data["criteria"] = [
            "id" => "ID"
            , "namaLokasi" => "Asal"
            , "destinationAccount" => "Account"
            , "date" => "Tanggal Penggunaan"
        ];
        $this->data["ordering"] = [
            "msNumber:asc" => "No Transaksi &uarr;"
            , "msNumber:desc" => "No Transaksi &darr;"
            , "date:asc" => "Tgl Transaksi &uarr;"
            , "date:desc" => "Tgl Transaksi &darr;"
            , "voyageKode:asc" => "Voyage &uarr;"
            , "voyageKode:desc" => "Voyage &darr;"
        ];
        $this->data["voyages"] = $this->voyage->getVoyages($request);
        $this->data["penggunaanmakanans"] = $this->baseService->paging($request);
        return view("modules.penggunaanmakanan.penggunaanmakanan_index", $this->data);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $this->data["penggunaanmakanan"] = $this->baseService->show($id);

        $this->data["criteria"] = [
            "id" => "ID" , 
            "namaItem" => "Nama Item"
            , "deskripsi" => "Deskripsi"
        ];

        $this->data["listItems"] = $this->itempenggunaanmakanan->showBasedOnPenggunaanmakanan($request, $this->data["penggunaanmakanan"] );

        return view("modules.penggunaanmakanan.penggunaanmakanan_edit", $this->data);
    }


    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws DefaultException
     */
    public function update(Request $request, $id)
    {
        $this->validator->beforeUpdate($request->all(), $id);
        if ($this->validator->hasError())
            return $this->validator->validateView();
        DB::beginTransaction();
        try {
            $this->baseService->update($id, $request->all());
            DB::commit();
            $this->sync();
            return redirect()->to("administrator/penggunaanmakanan")->with("successMessages", "Data has been updated");
        } catch (\Exception $e) {
            DB::rollBack();
            throw new DefaultException($e->getMessage());
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request) {
        $this->data["lokasiitems"] = $this->lokasiitem->index($request);
        $this->data["accounts"] = $this->account->AccountMakanan();
        // $this->data["voyages"] = $this->voyage->index();

        $this->data["criteria"] = [
            "namaItem"      => "Kode Item",
            "deskripsi"     => "Deskripsi" /* ,
            "partnumber"    => "Part Number" */
        ];

        return view("modules.penggunaanmakanan.penggunaanmakanan_create", $this->data);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, $id)
    {
        try{
            $this->data["penggunaanmakanan"] = $this->baseService->show($id);
            if($this->data["penggunaanmakanan"]) {
                $this->data["listItems"] = $this->itempenggunaanmakanan->showBasedOnPenggunaanMakanan($request, $this->data["penggunaanmakanan"]);
                return view("modules.penggunaanmakanan.penggunaanmakanan_show", $this->data);
            } else {
                return redirect()->back()->with("errMessage", "No data found. Please check this with your Administrator/Nahkoda.");
            }
        } catch (\Exception $e) {
            return redirect()->back()->with("errMessage", "No data found. Please check this with your Administrator/Nahkoda.");
        }
    }

    /**
     *  index function that get all data from database by default
     * @param $ids
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function destroy($ids)
    {
        $this->baseService->delete($ids);
        return redirect()->back()->with("successMessages", "Data has been deleted");
    }


    public function approval(Request $request, $id)
    {       
        $this->data["penggunaanmakanan"] = $this->baseService->show($id);
        $this->data["listItems"] = $this->itempenggunaanmakanan->showBasedOnPenggunaanmakanan($request, $this->data["penggunaanmakanan"]);
        $this->data["isApproval"] = true;
        return view("modules.penggunaanmakanan.penggunaanmakanan_show", $this->data);
    }

    public function saveApproval(Request $request)
    {       
        $auth = json_decode($request->session()->get('auth'));
        $input = $request->all();
        $input["refId"] = $input["msNumber"];
        $this->validator->beforeApprove($input);
        if ($this->validator->hasError()) {
            return $this->validator->validateView();
        }

        $transaction = $this->baseService->showByTransactionNo($input["refId"]);
        if($transaction->status > 0) {
            return redirect()->back()->with("errMessage", "Status transaksi #" . $input["refId"] . " sudah tidak open lagi!");
        } else {        

            DB::beginTransaction();
            try {
                $this->baseService->approve($input);
                $this->workflow->approve($input);

                if($input["status"] == 1) {
                    $staging["header"] = $transaction; // $this->baseService->showByTransactionNo($input["msNumber"]);
                    $staging["footer"] = $input["items"] = $this->itempenggunaanmakanan->showBasedOnPenggunaanMakanan($request, $staging["header"]);
                    $stockHistory = [
                        "transaksi"     => "penggunaanmakanan",
                        "noTransaksi"   => $input["refId"],
                        "username"      => $auth->username,
                        "periode"       => $staging["header"]->date
                    ];
                    
                    $this->stokkapal->reduceStock($input["items"], $stockHistory);
                    
                    $staging["auth"] = $auth;
                    $this->baseService->saveStaging($staging);
                }

                $input["date"] = date("Y-m-d H:i:s");
                $input["username"] = $auth->username;
                $input["activity"] = "APPROVAL";
                $input["namaTransaksi"] = "Workflow";
                $input["deskripsi"] = $auth->name . " just <b>approved</b> transaction Penggunaan Makanan <b>#" . $input["refId"] . "</b>";
                $this->activityLog->store($input);

                DB::commit();
                $this->sync();
                return redirect()->to("administrator/workflow")->with("successMessages", "Data dengan No #" . $input["refId"] . " sudah berhasil disimpan");
            } catch (\Exception $e) {
                DB::rollBack();
                throw new DefaultException($e->getMessage());
            }
        }
    }


    public function search(Request $request) {
        $data = $this->stokkapal->getStokList($request, 2);
        echo json_encode($data); 
    }


}