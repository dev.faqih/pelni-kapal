<?php
/**
 * Created by IntelliJ IDEA.
 * User: almustafa dekaulitz (sulaimanfahmi@gmail.com)
 * Date: 20/06/2018
 * Time: 19:10
 */

namespace App\Http\Handler;
error_reporting(E_ALL ^ E_WARNING);

class BodyGuardHandler
{
    public $rawBody = array();
    public $zipBody;
    public $key;
    public $body;

    /**
     * @param mixed $body
     * @return BodyGuardHandler
     */
    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    public function decodeBody()
    {
        $this->body = gzdecode($this->body);
        return $this;
    }

    public function encodeBody()
    {
        $this->body = gzencode($this->body, 9);
        return $this;
    }

    public function setKey()
    {
        $this->key = substr(sha1($this->body), 0, 6);
    }

    public function itsValid($key)
    {
        if (!$this->decodeBody()) return false;
        $this->encodeBody()->setKey();
        return $key === $this->key;
    }


}