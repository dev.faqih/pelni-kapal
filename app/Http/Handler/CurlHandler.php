<?php


namespace App\Http\Handler;


/**
 * Description of FirebaseHandler
 *
 * @author Fahmi Sulaiman
 */
class CurlHandler
{

    // sending push message to single user by firebase reg id
    protected $url = "";
    protected $body = array();
    protected $header = array('Content-Type: application/json');


    /**
     * @param $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @param $body
     * @return $this
     */
    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function send()
    {

        $url = $this->url;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($this->body));

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            throw new \Exception(curl_error($ch));
        } else {
            curl_close($ch);
            return $result;
        }
    }


    /**
     * @param array $header
     * @return $this
     */
    public function setHeader($header = array())
    {
        $this->header = $header;
        return $this;
    }


}
