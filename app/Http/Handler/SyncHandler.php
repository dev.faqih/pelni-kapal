<?php
/**
 * Created by IntelliJ IDEA.
 * User: almustafa dekaulitz (sulaimanfahmi@gmail.com)
 * Date: 20/06/2018
 * Time: 18:32
 */

namespace App\Http\Handler;


use Predis\Client;

class SyncHandler
{
    protected $url;
    protected $body = array();
    protected $headers = array("Content-Type: application/json");


    /**
     * @param mixed $url
     * @return SyncHandler
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @param array $body
     * @return SyncHandler
     */
    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @param array $headers
     * @return SyncHandler
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
        return $this;
    }

    public function sync()
    {
        $predis = new Client();
        $predis->publish("sync", json_encode([
            "url" => $this->url,
            "body" => $this->body,
            "headers" => $this->headers
        ]));
    }
}