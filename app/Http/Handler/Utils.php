<?php
/**
 * Created by IntelliJ IDEA.
 * User: almustafa dekaulitz (sulaimanfahmi@gmail.com)
 * Date: 02/06/2018
 * Time: 11:31
 */

namespace App\Http\Handler;


use App\Exceptions\DefaultException;
use App\Http\Controllers\Providers\IAM\Core\AuthManager\Jwt;
use App\Http\Controllers\Providers\IAM\Core\AuthManager\WorkerAuth;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Mockery\Exception;

class Utils
{

    static $fileDirectory = __DIR__ . './../../../public';

    public static function randomString($int)
    {
        $string = "abcdfghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456790" . sha1(Carbon::now());
        $randomString = substr(str_shuffle($string), 0, $int);
        return $randomString;
    }

    public static function userId()
    {
        if (!empty(WorkerAuth::getAuth())) {
            return WorkerAuth::getAuth()->id;
        } else if (!empty(Jwt::jwt()->getUserId())) {
            return Jwt::jwt()->getUserId();
        }
    }

    public static function generatePermalink($tile)
    {
        return str_replace(" ", "-", $tile);
    }

    public static function isValueExistInObject($value, $object, $field)
    {
        return collect($object)->where($field, $value)->first();
    }

    public static function inArray($value, $array)
    {
        $isExist = array_search($value, $array);
        if ($isExist === 0) {
            return true;
        } else {
            if ($isExist > 0) {
                return true;
            }
        }
    }

    public static function uploadImage(UploadedFile $file, $directory)
    {
        $ext = $file->getClientOriginalExtension();
        if (($ext !== "jpg") && ($ext !== "png") && ($ext !== "jpeg") && ($ext !== "JPG"))
            throw new DefaultException("Invalid file type");

        if ($file->getSize() <= 2500)
            throw new DefaultException("maximal size of file 2.5MB");
        $fileName = Carbon::now()->format("Y_m_d_H_i_s") . '.' . $file->getClientOriginalExtension();

        $file->move($directory, $fileName);
        return $fileName;
    }

    public static function uploadBinnary($imageBase, $dir)
    {
        if (!file_exists(BASE_DIR . "/" . $dir)) {
            mkdir(BASE_DIR . "/" . $dir, 0777, true);
        }

        $strImage = $imageBase;
        list($type, $strImage) = explode(';', $strImage);
        list(, $extension) = explode('/', $type);
        list(, $strImage) = explode(',', $strImage);

        $fileName = md5(Carbon::now()) . uniqid() . '.' . $extension;
        $image = base64_decode($strImage);
        file_put_contents(BASE_DIR . "/$dir/" . $fileName, $image);
        $server_url = baseURL . "/public/$dir/" . $fileName;
        return $server_url;
    }

}