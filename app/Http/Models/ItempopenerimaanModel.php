<?php

/**
 * This project has been generated by worker generator
 * has been made by fahmi sulaiman
 * sulaimanfahmi@gmail.com
 * check out my github @dekaulitz
 */

namespace App\Http\Models;

use App\Http\Base\BaseModel;
use App\Http\Providers\iam\core\model\OracleOCIConnector;
use Carbon\Carbon;

class ItempopenerimaanModel extends BaseModel
{
    /**
     * table name from database
     */
    protected $table = "itempopenerimaan";

    /**
     *   alias table for relation issues
     */
    protected $aliasTable = "itempopenerimaan";

    /**
     * primary key from table remember eloquent doesnt support composite primary key
     */
    protected $primaryKey = "id";
    /**
     * check the incrementing boolean if the incrementing from database is false then the value is false by default
     */
    // public $incrementing = false;
    /**
     * fillable property for eloquent attribute cast
     */
    protected $fillable = [
        "id", "poNumber", "itemId", "lokasiId", "receiptDate", "dateCreated", "quantity", "lotNumber", "date_updated", "qtyRemaining", "status", "kapalId"
    ];
    /**
     * Creating raw object from table that, this purpose for making you easier for remembering all attribute
     */


    /**
     * paging for default paging purpose
     * if you want to add new paging property you have to add the property in this array
     * you have to add the relation value
     * remember this is not suported casting value you have to handle by your self
     */
    protected $aliasPaging = [
        "id" => "itempopenerimaan.id",
        "poNumber" => "itempopenerimaan.poNumber",
        "part_number" => "itemId.part_number",
        "itemId" => "itempopenerimaan.itemId",
        "part_number" => "itemId.part_number",
        "namaItem" => "itemId.namaItem",
        "deskripsi" => "itemId.deskripsi",
        "lokasiId" => "lokasiId.id",
        "namaLokasi" => "lokasiId.namaLokasi"
    ];

    /**
     * relation from ' . item . ' by ' . itemId
     * join item on item.id = itempopenerimaan.itemId
     * this join use inner join if you want to change to left or right join or you want add some callback  you may refer to eloquent documentation
     * but you do like this $this->>getQuery()->leftJoin(function($query){}) we use query builder for our model
     * @param string $alias
     * @return $this
     */
    public function joinRelationItemId($alias = "itemId")
    {
        $this->getQuery()->join("item as $alias", "$alias.id", "=", "itempopenerimaan.itemId");
        return $this;
    }

    /**
     * relation from ' . lokasiitem . ' by ' . itemId
     * join item on item.id = itempopenerimaan.itemId
     * this join use inner join if you want to change to left or right join or you want add some callback  you may refer to eloquent documentation
     * but you do like this $this->>getQuery()->leftJoin(function($query){}) we use query builder for our model
     * @param string $alias
     * @return $this
     */
    public function joinRelationLokasiId($alias = "lokasiId")
    {
        $this->getQuery()->join("lokasiitem as $alias", "$alias.id", "=", "itempopenerimaan.lokasiId");
        return $this;
    }

    /**
     * relation from ' . popenerimaan . ' by ' . poPenerimaanId
     * join popenerimaan on popenerimaan.poNumber = itempopenerimaan.poPenerimaanId
     * this join use inner join if you want to change to left or right join or you want add some callback  you may refer to eloquent documentation
     * but you do like this $this->>getQuery()->leftJoin(function($query){}) we use query builder for our model
     * @param string $alias
     * @return $this
     */
    public function joinRelationPoPenerimaanId($alias = "poPenerimaanId")
    {
        $this->getQuery()->join("popenerimaan as $alias", "$alias.poNumber", "=", "itempopenerimaan.poNumber");
        return $this;
    }

    public function syncFromOracleHO($syncTable)
    {
        echo "start sync item po penerimaan data from oracle", PHP_EOL;
        (new LogsyncoracleModel)->setLogSync("Detail Penerimaan PO", Carbon::now()->toDateTimeString(), "", "start");
        $oracleOCI = new OracleOCIConnector();
        $oracleOCI->getConnection()->select(self::getOracleQuery($syncTable, 0, 0))->execute();
        while (($rows = $oracleOCI->fetchAll()) !== false) {
            $lastUpdate = $syncTable->last_sync;
            $item = new self();
            $item->storingDataFromOracle($rows);
            if (!empty($rows->LAST_UPDATE_DATE)) $lastUpdate = $rows->LAST_UPDATE_DATE;
            $syncTable = \App\Http\Models\SyncUtilities\StagingSyncTableModel::find($syncTable->id);
            $syncTable->last_sync = $lastUpdate;
            $syncTable->update();


        }
        (new LogsyncoracleModel)->setLogSync("Detail Penerimaan PO", Carbon::now()->toDateTimeString(), "", "success");
        $oracleOCI->closeConnection();
    }

    public static function getOracleQuery($staging, $startRow, $endRow)
    {
        $query = "SELECT
                    POITEMV.PO_NUMBER,
                    POITEMV.ITEM_ID,
                    POITEMV.QUANTITY,
                    POITEMV.LAST_UPDATE_DATE,
                    POITEMV.SEGMENT1,
                    POITEMV.SHIP_TO_ORGANIZATION_ID
                    FROM pelni_scm_po_lines_v POITEMV
                    WHERE
                    POITEMV.LAST_UPDATE_DATE >= TO_DATE('" . Carbon::parse($staging->last_sync)->toDateTimeString() . "', 'yyyy-mm-dd HH24:MI:SS') 
                    ORDER BY POITEMV.LAST_UPDATE_DATE ASC";
        return $query;
    }


    public function mapperOracle($oracle)
    {
        if (!$oracle->QUANTITY) {
            $oracle->QUANTITY = 0;
        }

        $this->poNumber = $oracle->PO_NUMBER;
        $this->itemId = $oracle->ITEM_ID;
        $this->quantity = $oracle->QUANTITY;
        $this->qtyRemaining = $oracle->QUANTITY;
        $this->kapalId = $oracle->SHIP_TO_ORGANIZATION_ID;
        $this->date_updated = $oracle->LAST_UPDATE_DATE;
        $this->status = 0;
        $this->lokasiId = 0;
    }

    public function storingDataFromOracle($oracle)
    {
        $modelExists = (new self())->getQuery()->where("itempopenerimaan.poNumber", "=", $oracle->PO_NUMBER)
            ->where("itempopenerimaan.kapalId", "=", $oracle->SHIP_TO_ORGANIZATION_ID)
            ->where("itempopenerimaan.itemId", '=', $oracle->ITEM_ID)->first();
        if (!$modelExists) {
            try {
                $model = new self();
                $model->mapperOracle($oracle);
                $model->save();
                echo $model->itemId . " inserted ", PHP_EOL;
                (new LogsyncoracleModel)->setLogSync("Detail Penerimaan PO : " . $model->poNumber . " - " . $oracle->SEGMENT1, Carbon::now()->toDateTimeString(), "", "inserted");
            } catch (\Exception $exception) {

                // FUNCTION TO INSERT INTO SYNCERROR TABLE IN CASE THE PROCESS ABOVE HAS SOME ERROR
                $params = json_encode([
                    "PO_NUMBER" => $oracle->PO_NUMBER,
                    "IO" => $oracle->SHIP_TO_ORGANIZATION_ID]);
                $data = json_encode($oracle);

                $syncErrExist = (new SyncerrorModel())->getQuery()->where("table_name", "=", "pelni_scm_pr_to_po_v")
                    ->where("params", "LIKE", $params)
                    ->where("aplikasi", "=", "HO")
                    ->first();
                if (!$syncErrExist) {
                    $insertSyncErr = new SyncerrorModel();
                    $insertSyncErr->table_name = "pelni_scm_pr_to_po_v";
                    $insertSyncErr->params = $params;
                    $insertSyncErr->data = $data;
                    $insertSyncErr->aplikasi = "HO";
                    $insertSyncErr->date_created = Carbon::now()->toDateTimeString();
                    $insertSyncErr->save();
                }
                // (new SyncerrorModel)->saveError("pelni_scm_pr_to_po_v", $params, $data, "HO");

                echo $exception->getMessage(), PHP_EOL;
            }
        } else {
            $model = self::find($modelExists->id);
            try {
                if (!$oracle->QUANTITY) {
                    $oracle->QUANTITY = 0;
                }

                $model->poNumber = $oracle->PO_NUMBER;
                $model->itemId = $oracle->ITEM_ID;
                $model->quantity = $oracle->QUANTITY;
                $model->kapalId = $oracle->SHIP_TO_ORGANIZATION_ID;
                $model->date_updated = $oracle->LAST_UPDATE_DATE;

                $model->update();
                echo $model->itemId . " updated ", PHP_EOL;
                (new LogsyncoracleModel)->setLogSync("Detail Penerimaan PO : ".$model->poNumber." - ".$oracle->SEGMENT1, Carbon::now()->toDateTimeString(), "", "updated");
            } catch (\Exception $exception) {
                echo $exception->getMessage(), PHP_EOL;
            }
        }
    }




    public function syncItemFromOracleHO($po, $kapalID)
    {
        echo "start sync item po penerimaan data from oracle", PHP_EOL;
        (new LogsyncoracleModel)->setLogSync("Detail Penerimaan PO", Carbon::now()->toDateTimeString(), "", "start");
        $oracleOCI = new OracleOCIConnector();
        $oracleOCI->getConnection()->select(self::getOracleQueryItem($po, $kapalID))->execute();
        while (($rows = $oracleOCI->fetchAll()) !== false) {
            $item = new self();
            $item->storingDataFromOracle($rows);
        }
        (new LogsyncoracleModel)->setLogSync("Detail Penerimaan PO", Carbon::now()->toDateTimeString(), "", "success");
        $oracleOCI->closeConnection();
    }

    public static function getOracleQueryItem($poNumber, $kapalID)
    {
        $query = "  SELECT
                        POITEMV.PO_NUMBER,
                        POITEMV.ITEM_ID,
                        POITEMV.QUANTITY,
                        POITEMV.LAST_UPDATE_DATE,
                        POITEMV.SEGMENT1,
                        POITEMV.SHIP_TO_ORGANIZATION_ID
                    FROM pelni_scm_po_lines_v POITEMV
                    WHERE POITEMV.PO_NUMBER = '" . $poNumber . "' AND SHIP_TO_ORGANIZATION_ID = '" . $kapalID . "'                    
                    ORDER BY POITEMV.LAST_UPDATE_DATE ASC";
        return $query;
    }

}
