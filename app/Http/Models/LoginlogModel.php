<?php

/**
 This project has been generated by worker generator
 has been made by fahmi sulaiman
 sulaimanfahmi@gmail.com
 check out my github @dekaulitz
 */

namespace App\Http\Models;
use App\Http\Base\BaseModel;

class LoginlogModel extends BaseModel
{
    /**
     * table name from database
     */
    protected $table="loginlog";

    /**
     *   alias table for relation issues
     */
    protected $aliasTable="loginlog";

	/**
	* primary key from table remember eloquent doesnt support composite primary key
	*/
	protected $primaryKey = "id";
	/**
	* check the incrementing boolean if the incrementing from database is false then the value is false by default
	*/
	// public $incrementing = false;
	/**
	* fillable property for eloquent attribute cast
	*/
	protected $fillable=[
		"id", "username", "loginStartDate", "loginEndDate", "date_updated", "kapalId"
	];
	/**
	* Creating raw object from table that, this purpose for making you easier for remembering all attribute
	*/


    /**
     * paging for default paging purpose
     * if you want to add new paging property you have to add the property in this array
     * you have to add the relation value
     * remember this is not suported casting value you have to handle by your self
    */
	protected $aliasPaging = [
		"id" => "loginlog.id",
		"name" => "userId.name",
		"loginStartDate" => "loginlog.loginStartDate",
		"loginEndDate" => "loginlog.loginEndDate",
	];

	/**
	* relation from ' . users . ' by ' . userId 
	* join users on users.id = loginlog.userId
	* this join use inner join if you want to change to left or right join or you want add some callback  you may refer to eloquent documentation
	* but you do like this $this->>getQuery()->leftJoin(function($query){}) we use query builder for our model
	* @param string $alias
	* @return $this
	*/
	public function joinRelationUserId($alias = "userId")
	{
		$this->getQuery()->join("users as $alias", "$alias.username", "=", "loginlog.username");
		return $this;
	}

}