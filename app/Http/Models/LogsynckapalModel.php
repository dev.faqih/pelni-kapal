<?php

/**
 * This project has been generated by worker generator
 * has been made by fahmi sulaiman
 * sulaimanfahmi@gmail.com
 * check out my github @dekaulitz
 */

namespace App\Http\Models;

use App\Http\Base\BaseModel;

class LogsynckapalModel extends BaseModel
{
    /**
     * table name from database
     */
    protected $table = "logsynckapal";

    /**
     *   alias table for relation issues
     */
    protected $aliasTable = "logsynckapal";

    /**
     * primary key from table remember eloquent doesnt support composite primary key
     */
    protected $primaryKey = "id";
    /**
     * fillable property for eloquent attribute cast
     */
    protected $fillable = [
        "id", "process_name", "process_date", "status", "error_msg"
    ];
    /**
     * Creating raw object from table that, this purpose for making you easier for remembering all attribute
     */


    /**
     * paging for default paging purpose
     * if you want to add new paging property you have to add the property in this array
     * you have to add the relation value
     * remember this is not suported casting value you have to handle by your self
     */
    protected $aliasPaging = [
        "id" => "roles.id",
        "process_name" => "logsynckapal.process_name",
        "process_date" => "logsynckapal.process_date",
        "status" => "logsynckapal.status",
        "error_msg" => "logsynckapal.error_msg",
    ];


    public static function setLogSync($processName, $date, $errorMsg, $status)
    {
        $model = new self();
        $model->process_name = $processName;
        $model->process_date = $date;
        $model->error_msg = $errorMsg;// . debug_backtrace()[1]['function'] . " .... " . debug_backtrace()[0]['function'];
        $model->status = $status;
        $model->save();
    }

    public static function trimLogSync($numRows = 50000)
    {
        $model = new self();
        $model->orderBy("id", "DESC")->take(1000000)->skip($numRows)->get()->each(function($row){ $row->delete(); });

    }

}