<?php

/**
 * This project has been generated by worker generator
 * has been made by fahmi sulaiman
 * sulaimanfahmi@gmail.com
 * check out my github @dekaulitz
 */

namespace App\Http\Models;

use App\Http\Base\BaseModel;
use App\Http\Providers\iam\core\model\OracleOCIConnector;
use Carbon\Carbon;
use Illuminate\Database\Capsule\Manager;
use Illuminate\Support\Facades\DB;

class LokasiitemModel extends BaseModel
{
    /**
     * table name from database
     */
    protected $table = "lokasiitem";

    /**
     *   alias table for relation issues
     */
    protected $aliasTable = "lokasiitem";

    /**
     * primary key from table remember eloquent doesnt support composite primary key
     */
    protected $primaryKey = "id";
    /**
     * check the incrementing boolean if the incrementing from database is false then the value is false by default
     */
    // public $incrementing = false;
    /**
     * fillable property for eloquent attribute cast
     */
    protected $fillable = [
        "id", "namaLokasi", "date_updated" // , "lokasi", "alamatLokasi", "ORGCode"
    ];
    /**
     * Creating raw object from table that, this purpose for making you easier for remembering all attribute
     */


    /**
     * paging for default paging purpose
     * if you want to add new paging property you have to add the property in this array
     * you have to add the relation value
     * remember this is not suported casting value you have to handle by your self
     */
    protected $aliasPaging = [
        //"id" => "lokasiitem.id",
        //"namaLokasi" => "lokasiitem.namaLokasi"
        /* ,
        "lokasi" => "lokasiitem.lokasi",
        "alamatLokasi" => "lokasiitem.alamatLokasi",
        "ORGCode" => "lokasiitem.ORGCode", */
    ];

    /**
     * relation from ' . pemakaianstok . ' by ' . locationId
     * join pemakaianstok on pemakaianstok.id = lokasiitem.locationId
     * this join use inner join if you want to change to left or right join or you want add some callback  you may refer to eloquent documentation
     * but you do like this $this->>getQuery()->leftJoin(function($query){}) we use query builder for our model
     * @param string $alias
     * @return $this
     */
    public function joinRelationPemakaianstokLocationId($alias = "pemakaianstok")
    {
        $this->getQuery()->join("pemakaianstok as $alias", "$alias.locationId", "=", "lokasiitem.id");
        return $this;
    }

    /**
     * relation from ' . penerimaanbarang . ' by ' . locationId
     * join penerimaanbarang on penerimaanbarang.id = lokasiitem.locationId
     * this join use inner join if you want to change to left or right join or you want add some callback  you may refer to eloquent documentation
     * but you do like this $this->>getQuery()->leftJoin(function($query){}) we use query builder for our model
     * @param string $alias
     * @return $this
     */
    public function joinRelationPenerimaanbarangLocationId($alias = "penerimaanbarang")
    {
        $this->getQuery()->join("penerimaanbarang as $alias", "$alias.locationId", "=", "lokasiitem.id");
        return $this;
    }

    /**
     * relation from ' . penyesuaianstok . ' by ' . locationId
     * join penyesuaianstok on penyesuaianstok.id = lokasiitem.locationId
     * this join use inner join if you want to change to left or right join or you want add some callback  you may refer to eloquent documentation
     * but you do like this $this->>getQuery()->leftJoin(function($query){}) we use query builder for our model
     * @param string $alias
     * @return $this
     */
    public function joinRelationPenyesuaianstokLocationId($alias = "penyesuaianstok")
    {
        $this->getQuery()->join("penyesuaianstok as $alias", "$alias.locationId", "=", "lokasiitem.id");
        return $this;
    }

    /**
     * relation from ' . permintaanbarang . ' by ' . locationId
     * join permintaanbarang on permintaanbarang.id = lokasiitem.locationId
     * this join use inner join if you want to change to left or right join or you want add some callback  you may refer to eloquent documentation
     * but you do like this $this->>getQuery()->leftJoin(function($query){}) we use query builder for our model
     * @param string $alias
     * @return $this
     */
    public function joinRelationPermintaanbarangLocationId($alias = "permintaanbarang")
    {
        $this->getQuery()->join("permintaanbarang as $alias", "$alias.locationId", "=", "lokasiitem.id");
        return $this;
    }

    /**
     * relation from ' . pindahstok . ' by ' . fromLocationId
     * join pindahstok on pindahstok.id = lokasiitem.fromLocationId
     * this join use inner join if you want to change to left or right join or you want add some callback  you may refer to eloquent documentation
     * but you do like this $this->>getQuery()->leftJoin(function($query){}) we use query builder for our model
     * @param string $alias
     * @return $this
     */
    public function joinRelationPindahstokFromLocationId($alias = "pindahstok")
    {
        $this->getQuery()->join("pindahstok as $alias", "$alias.fromLocationId", "=", "lokasiitem.id");
        return $this;
    }

    /**
     * relation from ' . pindahstok . ' by ' . toLocationId
     * join pindahstok on pindahstok.id = lokasiitem.toLocationId
     * this join use inner join if you want to change to left or right join or you want add some callback  you may refer to eloquent documentation
     * but you do like this $this->>getQuery()->leftJoin(function($query){}) we use query builder for our model
     * @param string $alias
     * @return $this
     */
    public function joinRelationPindahstokToLocationId($alias = "pindahstok")
    {
        $this->getQuery()->join("pindahstok as $alias", "$alias.toLocationId", "=", "lokasiitem.id");
        return $this;
    }

    /**
     * relation from ' . stokkapal . ' by ' . lokasiId
     * join stokkapal on stokkapal.id = lokasiitem.lokasiId
     * this join use inner join if you want to change to left or right join or you want add some callback  you may refer to eloquent documentation
     * but you do like this $this->>getQuery()->leftJoin(function($query){}) we use query builder for our model
     * @param string $alias
     * @return $this
     */
    public function joinRelationStokkapalLokasiId($alias = "stokkapal")
    {
        $this->getQuery()->leftjoin("stokkapal as $alias", "$alias.lokasiId", "=", "lokasiitem.id");
        return $this;
    }


    public function joinRelationKapalId($alias = "kapalId", $alias2 = "kapalLokasiItem")
    {
        $this->getQuery()->join("kapallokasiitem as $alias2", "$alias2.lokasiId", "=", "lokasiitem.id")
        ->join("kapal as $alias", "$alias.id", "=", "kapalLokasiItem.kapalId");
        return $this;
    }
    
    public function joinRelationRoleId($alias = "roleId", $alias2 = "roleLokasiItem")
    {
        $this->getQuery()->join("rolelokasiitem as $alias2", "$alias.lokasiId", "=", "lokasiitem.id")
                        ->join("roles as $alias", "$alias.id", "=", "roleLokasiItem.roleId");
        return $this;
    }    

    public function joinRelationRoleLokasiItem($alias = "roleLokasiItem")
    {
        $this->getQuery()->join("rolelokasiitem as $alias", "$alias.lokasiId", "=", "lokasiitem.id");
        return $this;
    }

    public function joinRelationKapalLokasiItem($alias = "kapalLokasiItem")
    {
        $this->getQuery()->join("kapallokasiitem as $alias", "$alias.lokasiId", "=", "lokasiitem.id");
        return $this;
    }


    public function mapperOracle($oracle)
    {
        $this->namaLokasi = $oracle->SUBINV_CODE;
       /*  $this->lokasi = $oracle->DESCRIPTION;
        $this->ORGCode = $oracle->MTL_IO_CODE;
        $this->kapalId = $oracle->MTL_IO_ID; */
        $this->date_updated = $oracle->LAST_UPDATE_DATE;
    }

    public function syncErrorFromOracleHO($namaLokasi, $kapalID, $fromCMS = false)
    {
        echo "start sync recovery lokasi item data from oracle", PHP_EOL;
        (new LogsyncoracleModel)->setLogSync("Sync Recovery Master Sub-Inventori", Carbon::now()->toDateTimeString(), "", "start");
        $oracleOCI = new OracleOCIConnector();
        $oracleOCI->getConnection()->select(self::getSyncErrorOracleQuery($namaLokasi, $kapalID))->execute();
        $rows = $oracleOCI->fetchAll(); $return = false;
        if($rows) {
            $item = new self();
            $return = $item->storingDataFromOracle($rows, $fromCMS);
        } else {
            $return = false;
        }
        (new LogsyncoracleModel)->setLogSync("Sync Recovery Master Sub-Inventori", Carbon::now()->toDateTimeString(), "", "success");
        $oracleOCI->closeConnection();
        return $return;
    }

    public function syncFromOracleHO($syncTable, $fromCMS = false)
    {
        echo "start lokasi item data from oracle", PHP_EOL;
        (new LogsyncoracleModel)->setLogSync("Master Sub-Inventori", Carbon::now()->toDateTimeString(), "", "start");
        $oracleOCI = new OracleOCIConnector();
        $oracleOCI->getConnection()->select(self::getOracleQuery($syncTable, 0, 0))->execute();
        $lastUpdate = $syncTable->last_sync;
        while (($rows = $oracleOCI->fetchAll()) !== false) {
            $item = new self();
            $item->storingDataFromOracle($rows, $fromCMS);
            if (!empty($rows->LAST_UPDATE_DATE)) $lastUpdate = $rows->LAST_UPDATE_DATE;
            else $lastUpdate = Carbon::now()->toDateTimeString();
        }
        $syncTable = \App\Http\Models\SyncUtilities\StagingSyncTableModel::find($syncTable->id);
        $syncTable->last_sync = $lastUpdate;
        $syncTable->update();
        (new LogsyncoracleModel)->setLogSync("Master Sub-Inventori", Carbon::now()->toDateTimeString(), "", "success");
        $oracleOCI->closeConnection();

    }

    public static function getOracleQuery($staging, $startRow, $endRow)
    {
        $query = "  SELECT SUBINV.*
                    FROM pelni_scm_subinv_v SUBINV
                    WHERE SUBINV.LAST_UPDATE_DATE >= TO_DATE('" . Carbon::parse($staging->last_sync)->toDateTimeString() . "', 'yyyy-mm-dd HH24:MI:SS')
                    ORDER BY SUBINV.LAST_UPDATE_DATE ASC";
        return $query;
    }

    public static function getSyncErrorOracleQuery($namaLokasi, $kapalID)
    {
        echo $query = "SELECT SUBINV.* FROM pelni_scm_subinv_v SUBINV WHERE SUBINV.SUBINV_CODE = '" . $namaLokasi . "' AND SUBINV.MTL_IO_ID = '" . $kapalID . "'";
        return $query;
    }

    public function storingDataFromOracle($oracle, $fromCMS = false)
    {
        $modelExist = (new self())->getQuery() // ->where("lokasiitem.kapalId", "=", $oracle->MTL_IO_ID)
            ->where("lokasiitem.namaLokasi", "=", $oracle->SUBINV_CODE)->first();
        if ($modelExist) {
            if($fromCMS) { DB::beginTransaction(); } else { Manager::beginTransaction(); }
            try {
                echo "masuk 1";
                (new KapallokasiitemModel)->storingDataKapalLokasiItem($modelExist->id, $oracle);
                if($fromCMS) { DB::commit(); } else { Manager::commit(); }
                echo $modelExist->namaLokasi . " updated ", PHP_EOL;
                (new LogsyncoracleModel)->setLogSync("Master Sub-Inventori : ".$oracle->MTL_IO_ID." - ".$oracle->SUBINV_CODE, Carbon::now()->toDateTimeString(), "", "updated");
                return true;
            } catch (\Exception $exception) {
                if($fromCMS) { DB::rollBack(); } else { Manager::rollBack(); }
                echo $exception->getMessage(), PHP_EOL;
                (new LogsyncoracleModel)->setLogSync("Master Sub-Inventori : ".$oracle->MTL_IO_ID." - ".$oracle->SUBINV_CODE, Carbon::now()->toDateTimeString(), $exception->getMessage(), "failed");

                // FUNCTION TO INSERT INTO SYNCERROR TABLE IN CASE THE PROCESS ABOVE HAS SOME ERROR
                $params = json_encode([
                    "SUBINV_CODE" => $oracle->SUBINV_CODE,
                    "MTL_IO_ID" => $oracle->MTL_IO_ID
                ] );
                $data = json_encode($oracle);
                (new SyncerrorModel)->saveError("pelni_scm_subinv_v", $params, $data, "HO");

                echo $exception->getMessage(), PHP_EOL;
                (new LogsyncoracleModel)->setLogSync("Master Sub-Inventori : ".$oracle->MTL_IO_ID." - ".$oracle->SUBINV_CODE, Carbon::now()->toDateTimeString(), $exception->getMessage(), "failed");
                return false;
            }
        } else {
            if($fromCMS) { DB::beginTransaction(); } else { Manager::beginTransaction(); }
            try {
                echo "masuk 3";
                $model = new self();
                $model->mapperOracle($oracle);
                $model->save();
                (new KapallokasiitemModel)->storingDataKapalLokasiItem($model->id, $oracle);
                if($fromCMS) { DB::commit(); } else { Manager::commit(); }
                echo $model->namaLokasi . " inserted ", PHP_EOL;
                (new LogsyncoracleModel)->setLogSync("Master Sub-Inventori : ".$oracle->MTL_IO_ID." - ".$oracle->SUBINV_CODE, Carbon::now()->toDateTimeString(), "", "inserted");
                return true;
            } catch (\Exception $exception) {
                if($fromCMS) { DB::rollBack(); } else { Manager::rollBack(); }
                // FUNCTION TO INSERT INTO SYNCERROR TABLE IN CASE THE PROCESS ABOVE HAS SOME ERROR
                $params = json_encode([
                    "SUBINV_CODE" => $oracle->SUBINV_CODE,
                    "MTL_IO_ID" => $oracle->MTL_IO_ID
                ] );
                $data = json_encode($oracle);
                (new SyncerrorModel)->saveError("pelni_scm_subinv_v", $params, $data, "HO");

                echo $exception->getMessage(), PHP_EOL;
                (new LogsyncoracleModel)->setLogSync("Master Sub-Inventori : ".$oracle->MTL_IO_ID." - ".$oracle->SUBINV_CODE, Carbon::now()->toDateTimeString(), $exception->getMessage(), "failed");
                return false;
            }
        }
    }

    public static function storeFromHO($data)
    {
        $model = (new self())->getQuery()->where("lokasiitem.id", "=", $data->id)->first();
        if ($model) {
            $model = self::find($model->id);
            $model->id = $data->id;
            $model->namaLokasi = $data->namaLokasi;
  /*           $model->lokasi = $data->lokasi;
            $model->alamatLokasi = $data->alamatLokasi;
            $model->ORGCode = $data->ORGCode; */
            $model->date_updated = $data->date_updated;
            $model->update();
            (new LogsynckapalModel)->setLogSync("Sub-Inventori : ".$data->id." - ".$data->namaLokasi, Carbon::now()->toDateTimeString(), "","updated");
        } else {
            $model = new self();
            $model->id = $data->id;
            $model->namaLokasi = $data->namaLokasi;
        /*     $model->lokasi = $data->lokasi;
            $model->alamatLokasi = $data->alamatLokasi;
            $model->ORGCode = $data->ORGCode; */
            $model->date_updated = $data->date_updated;
            $model->save([], $data->id, 0);
            (new LogsynckapalModel)->setLogSync("Sub-Inventori : ".$data->id." - ".$data->namaLokasi, Carbon::now()->toDateTimeString(), "","inserted");
        }
        echo $data->namaLokasi . " sync", PHP_EOL;
    }

    public static function getLokasiItem()
    {
        (new LogsynckapalModel)->setLogSync("Sub-Inventory HO to Kapal", Carbon::now()->toDateTimeString(), "", "start");
        $url = $GLOBALS["env"]["HEAD_OFFICE_URL"] . "/api/sync_lokasi_item";
        $syncTable = (new \App\Http\Models\SyncUtilities\SyncTableHandlerModel())->getQuery()->where("sync_table.id", "=", 4)->first();
        $fetch = true;
        $page = 1;
        do {
            if ($syncTable) {

                $lastUpdate = Carbon::parse($syncTable->last_sync)->toAtomString();
                $url = $url . "?date_updated=" . $lastUpdate . "&page=" . $page;
                $syncHandler = new \App\Http\Handler\CurlHandler();
                try {
                    $response = $syncHandler->setUrl($url)->send();
                    $res = json_decode($response, false);
                    $no = 1;
                    if (!empty($res->data)) {
                        if($page == 1) {
                            \App\Http\Models\LokasiitemModel::truncate();
                        }
                        foreach ($res->data as $item) {
                            echo "data " . $no;
                            \App\Http\Models\LokasiitemModel::storeFromHO($item);
                            $lastUpdate = $item->date_updated;
                            $no = $no + 1;
                        }
                        $sync = \App\Http\Models\SyncUtilities\SyncTableHandlerModel::find($syncTable->id);
                        $sync->last_sync = $lastUpdate;
                        $sync->update();
                    } else {
                        $fetch = false;
                    }

                } catch (\Exception $e) {
                    echo $e->getMessage();
                    (new LogsynckapalModel)->setLogSync("Sub-Inventory HO to Kapal", Carbon::now()->toDateTimeString(), $e->getMessage(), "failed");
                }
                $page = $page + 1;
            }
        } while ($fetch);
        (new LogsynckapalModel)->setLogSync("Sub-Inventory HO to Kapal", Carbon::now()->toDateTimeString(), "", "success");
    }
}