<?php

/**
 This project has been generated by worker generator
 has been made by fahmi sulaiman
 sulaimanfahmi@gmail.com
 check out my github @dekaulitz
 */

namespace App\Http\Models;

use App\Http\Base\BaseModel;

class PenerimaanbarangModel extends BaseModel
{
	/**
	 * table name from database
	 */
	protected $table = "penerimaanbarang";

	/**
	 *   alias table for relation issues
	 */
	protected $aliasTable = "penerimaanbarang";

	/**
	 * primary key from table remember eloquent doesnt support composite primary key
	 */
	protected $primaryKey = "id";
	/**
	 * check the incrementing boolean if the incrementing from database is false then the value is false by default
	 */
	// public $incrementing = false;
	/**
	 * fillable property for eloquent attribute cast
	 */
	protected $fillable = [
		"id", "receiptNumber", "poNumber", "userId", "kapalId", "locationId", "status", "unitOperasi", "tanggalPenerimaan", "tanggalPO", "qtyDiterima", "voyageKode", "voyageStart", "voyageEnd", "date_updated"
	];
	/**
	 * Creating raw object from table that, this purpose for making you easier for remembering all attribute
	 */


	/**
	 * paging for default paging purpose
	 * if you want to add new paging property you have to add the property in this array
	 * you have to add the relation value
	 * remember this is not suported casting value you have to handle by your self
	 */
	protected $aliasPaging = [
		"poNumber" => "penerimaanbarang.poNumber",
		"kapalId" => "kapalId.id",
		"locationId" => "locationId.id",
		"userId" => "userId.id",
		"receiptNumber" => "penerimaanbarang.receiptNumber",
		"receiptDate" => "penerimaanbarang.receiptDate",
	];

	/**
	 * relation from ' . kapal . ' by ' . kapalId 
	 * join kapal on kapal.id = penerimaanbarang.kapalId
	 * this join use inner join if you want to change to left or right join or you want add some callback  you may refer to eloquent documentation
	 * but you do like this $this->>getQuery()->leftJoin(function($query){}) we use query builder for our model
	 * @param string $alias
	 * @return $this
	 */
	public function joinRelationKapalId($alias = "kapalId")
	{
		$this->getQuery()->join("kapal as $alias", "$alias.id", "=", "penerimaanbarang.kapalId");
		return $this;
	}
	/**
	 * relation from ' . lokasiitem . ' by ' . locationId 
	 * join lokasiitem on lokasiitem.id = penerimaanbarang.locationId
	 * this join use inner join if you want to change to left or right join or you want add some callback  you may refer to eloquent documentation
	 * but you do like this $this->>getQuery()->leftJoin(function($query){}) we use query builder for our model
	 * @param string $alias
	 * @return $this
	 */
	public function joinRelationLocationId($alias = "locationId")
	{
		$this->getQuery()->join("lokasiitem as $alias", "$alias.id", "=", "penerimaanbarang.locationId");
		return $this;
	}
	/**
	 * relation from ' . users . ' by ' . userId 
	 * join users on users.id = penerimaanbarang.userId
	 * this join use inner join if you want to change to left or right join or you want add some callback  you may refer to eloquent documentation
	 * but you do like this $this->>getQuery()->leftJoin(function($query){}) we use query builder for our model
	 * @param string $alias
	 * @return $this
	 */
	public function joinRelationUserId($alias = "userId")
	{
		$this->getQuery()->join("users as $alias", "$alias.id", "=", "penerimaanbarang.userId");
		return $this;
	}
	/**
	 * relation from ' . itempenerimaanbarang . ' by ' . penerimaanBarangId 
	 * join itempenerimaanbarang on itempenerimaanbarang.poNumber = penerimaanbarang.penerimaanBarangId
	 * this join use inner join if you want to change to left or right join or you want add some callback  you may refer to eloquent documentation
	 * but you do like this $this->>getQuery()->leftJoin(function($query){}) we use query builder for our model
	 * @param string $alias
	 * @return $this
	 */
	public function joinRelationItemPenerimaanBarangId($alias = "itempenerimaanbarang")
	{
		$this->getQuery()->join("itempenerimaanbarang as $alias", "$alias.penerimaanBarangId", "=", "penerimaanbarang.receiptNumber");
		return $this;
	}

	public function joinRelationVoyageKode($alias = "voyageKode")
	{
		$this->getQuery()->leftjoin("voyages as $alias", function ($join) use ($alias) {
			$join->on("$alias.voyageKode", "=", "penerimaanbarang.voyageKode");
			$join->on("$alias.kapalId", "=", "penerimaanbarang.kapalId");
		});
		return $this;
	}
}
