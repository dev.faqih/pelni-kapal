<?php

/**
 This project has been generated by worker generator
 has been made by fahmi sulaiman
 sulaimanfahmi@gmail.com
 check out my github @dekaulitz
 */

namespace App\Http\Models;
use App\Http\Base\BaseModel;

class PenggunaanmakananModel extends BaseModel
{
    /**
     * table name from database
     */
    protected $table = "penggunaanmakanan";

    /**
     *   alias table for relation issues
     */
    protected $aliasTable = "penggunaanmakanan";

	/**
	* primary key from table remember eloquent doesnt support composite primary key
	*/
	protected $primaryKey = "id";
	/**
	* check the incrementing boolean if the incrementing from database is false then the value is false by default
	*/
	// public $incrementing = false;
	/**
	* fillable property for eloquent attribute cast
	*/
	protected $fillable=[
		"id", "msNumber", "requesterId", "type", "kapalId", "locationId", "accountId", "date",
		"voyageKode",
		"voyageStart",
		"voyageEnd",
		"status",
		"totalBarang",
		"tglApproved",
		"feedback",
		"komentar", "date_updated"
	];
	/**
	* Creating raw object from table that, this purpose for making you easier for remembering all attribute
	*/


    /**
     * paging for default paging purpose
     * if you want to add new paging property you have to add the property in this array
     * you have to add the relation value
     * remember this is not suported casting value you have to handle by your self
    */
	protected $aliasPaging = [
		"id" => "penggunaanmakanan.id",
		"status" => "penggunaanmakanan.status", 
		"msNumber" => "penggunaanmakanan.msNumber",
		"date" => "penggunaanmakanan.date",
		"locationId" => "penggunaanmakanan.locationId",
		"accountId" => "penggunaanmakanan.accountId",
		"voyageKode" => "penggunaanmakanan.voyageKode",
		"voyageStart" => "penggunaanmakanan.voyageStart",
		"voyageEnd" => "penggunaanmakanan.voyageEnd"
	];

	/**
	* relation from ' . kapal . ' by ' . kapalId 
	* join kapal on kapal.id = penggunaanmakanan.kapalId
	* this join use inner join if you want to change to left or right join or you want add some callback  you may refer to eloquent documentation
	* but you do like this $this->>getQuery()->leftJoin(function($query){}) we use query builder for our model
	* @param string $alias
	* @return $this
	*/
	public function joinRelationKapalId($alias = "kapalId")
	{
		$this->getQuery()->join("kapal as $alias", "$alias.id", "=", "penggunaanmakanan.kapalId");
		return $this;
	}
	/**
	* relation from ' . lokasiitem . ' by ' . locationId 
	* join lokasiitem on lokasiitem.id = penggunaanmakanan.locationId
	* this join use inner join if you want to change to left or right join or you want add some callback  you may refer to eloquent documentation
	* but you do like this $this->>getQuery()->leftJoin(function($query){}) we use query builder for our model
	* @param string $alias
	* @return $this
	*/
	public function joinRelationLocationId($alias = "locationId")
	{
		$this->getQuery()->join("lokasiitem as $alias", "$alias.id", "=", "penggunaanmakanan.locationId");
		return $this;
	}
	/**
	* relation from ' . users . ' by ' . userId 
	* join users on users.id = penggunaanmakanan.userId
	* this join use inner join if you want to change to left or right join or you want add some callback  you may refer to eloquent documentation
	* but you do like this $this->>getQuery()->leftJoin(function($query){}) we use query builder for our model
	* @param string $alias
	* @return $this
	*/
	public function joinRelationRequesterId($alias = "requesterId")
	{
		$this->getQuery()->join("users as $alias", "$alias.id", "=", "penggunaanmakanan.requesterId");
		return $this;
	}
	/**
	* relation from ' . itempenggunaanmakanan . ' by ' . penggunaanMakananId 
	* join itempenggunaanmakanan on itempenggunaanmakanan.penggunaanMakananId = penggunaanmakanan.id
	* this join use inner join if you want to change to left or right join or you want add some callback  you may refer to eloquent documentation
	* but you do like this $this->>getQuery()->leftJoin(function($query){}) we use query builder for our model
	* @param string $alias
	* @return $this
	*/
	public function joinRelationItempenggunaanmakananId($alias = "itempenggunaanmakanan")
	{
		$this->getQuery()->join("itempenggunaanmakanan as $alias", "$alias.msNumber", "=", "penggunaanmakanan.msNumber");
		$this->getQuery()->join("item", function($join) {
			$join->on("item.id", "=", "itempenggunaanmakanan.itemId");
			$join->on("item.kapalId", "=", "penggunaanmakanan.kapalId");
		} );
		return $this;
	}


	public function joinRelationAccountId($alias = "account")
	{
		$this->getQuery()->leftjoin("account as $alias", "$alias.id", "=", "penggunaanmakanan.accountId");
		return $this;
	}

	public function joinRelationVoyageKode($alias = "voyageKode")
	{
		$this->getQuery()->leftjoin("voyages as $alias", function($join) use ($alias) {
			$join->on("$alias.voyageKode", "=", "penggunaanmakanan.voyageKode");
			$join->on("$alias.kapalId", "=", "penggunaanmakanan.kapalId");
		});
		return $this;
	}

	public function joinRelationApproverId($alias = "approverId")
	{
		$this->getQuery()->join("users as $alias", "$alias.id", "=", "penggunaanmakanan.approverId");
		return $this;
	}
}