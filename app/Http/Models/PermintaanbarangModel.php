<?php

/**
 This project has been generated by worker generator
 has been made by fahmi sulaiman
 sulaimanfahmi@gmail.com
 check out my github @dekaulitz
 */

namespace App\Http\Models;

use App\Http\Base\BaseModel;

class PermintaanbarangModel extends BaseModel
{
	/**
	 * table name from database
	 */
	protected $table = "permintaanbarang";

	/**
	 *   alias table for relation issues
	 */
	protected $aliasTable = "permintaanbarang";

	/**
	 * primary key from table remember eloquent doesnt support composite primary key
	 */
	protected $primaryKey = "id";
	/**
	 * check the incrementing boolean if the incrementing from database is false then the value is false by default
	 */
	// public $incrementing = false;
	/**
	 * fillable property for eloquent attribute cast
	 */
	protected $fillable = [
		"prNumber",
		"requesterId",
		"approverId",
		"kapalId",
		"destinationType",
		"operatingUnit",
		"organization",
		"keperluan",
		"totalHarga",
		"totalBarang",
		"komentar",
		"deskripsi",
		"tglPermintaan",
		"feedback",
		"tglApproved",
		"isApproved", "voyageKode", "voyageStart", "voyageEnd", "date_updated"
	];

	/**
	 * Creating raw object from table that, this purpose for making you easier for remembering all attribute
	 */


	/**
	 * paging for default paging purpose
	 * if you want to add new paging property you have to add the property in this array
	 * you have to add the relation value
	 * remember this is not suported casting value you have to handle by your self
	 */
	protected $aliasPaging = [
		"prNumber" => "permintaanbarang.prNumber",
		"isApproved" => "permintaanbarang.isApproved",
		// "namaItem" => "item.namaItem", 
		"tglPermintaan" => "permintaanbarang.tglPermintaan",
		"prNumber" => "permintaanbarang.prNumber",
		"voyageKode" => "permintaanbarang.voyageKode",
		"voyageStart" => "permintaanbarang.voyageStart",
		"voyageEnd" => "permintaanbarang.voyageEnd"
	];

	/**
	 * relation from ' . kapal . ' by ' . kapalId 
	 * join kapal on kapal.id = permintaanbarang.kapalId
	 * this join use inner join if you want to change to left or right join or you want add some callback  you may refer to eloquent documentation
	 * but you do like this $this->>getQuery()->leftJoin(function($query){}) we use query builder for our model
	 * @param string $alias
	 * @return $this
	 */
	public function joinRelationKapalId($alias = "kapalId")
	{
		$this->getQuery()->leftjoin("kapal as $alias", "$alias.id", "=", "permintaanbarang.kapalId");
		return $this;
	}
	/**
	 * relation from ' . users . ' by ' . approverId 
	 * join users on users.id = permintaanbarang.approverId
	 * this join use inner join if you want to change to left or right join or you want add some callback  you may refer to eloquent documentation
	 * but you do like this $this->>getQuery()->leftJoin(function($query){}) we use query builder for our model
	 * @param string $alias
	 * @return $this
	 */
	public function joinRelationApproverId($alias = "approverId")
	{
		$this->getQuery()->leftjoin("users as $alias", "$alias.id", "=", "permintaanbarang.approverId");
		return $this;
	}
	/**
	 * relation from ' . users . ' by ' . requesterId 
	 * join users on users.id = permintaanbarang.requesterId
	 * this join use inner join if you want to change to left or right join or you want add some callback  you may refer to eloquent documentation
	 * but you do like this $this->>getQuery()->leftJoin(function($query){}) we use query builder for our model
	 * @param string $alias
	 * @return $this
	 */
	public function joinRelationRequesterId($alias = "requesterId")
	{
		$this->getQuery()->leftjoin("users as $alias", "$alias.id", "=", "permintaanbarang.requesterId");
		return $this;
	}
	/**
	 * relation from ' . itempermintaanbarang . ' by ' . prNumber 
	 * join itempermintaanbarang on itempermintaanbarang.prNumber = permintaanbarang.prNumber
	 * this join use inner join if you want to change to left or right join or you want add some callback  you may refer to eloquent documentation
	 * but you do like this $this->>getQuery()->leftJoin(function($query){}) we use query builder for our model
	 * @param string $alias
	 * @return $this
	 */
	public function joinRelationItempermintaanbarangPrNumber($alias = "itempermintaanbarang")
	{
		$this->getQuery()->join("itempermintaanbarang as $alias", "$alias.prNumber", "=", "permintaanbarang.prNumber");
		return $this;
	}

	public function joinRelationVoyageKode($alias = "voyageKode")
	{
		$this->getQuery()->leftjoin("voyages as $alias", function ($join) use ($alias) {
			$join->on("$alias.voyageKode", "=", "permintaanbarang.voyageKode");
			$join->on("$alias.kapalId", "=", "permintaanbarang.kapalId");
		});
		return $this;
	}
}
