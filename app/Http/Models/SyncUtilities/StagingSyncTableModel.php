<?php
/**
 * Created by IntelliJ IDEA.
 * User: almustafa dekaulitz (sulaimanfahmi@gmail.com)
 * Date: 17/09/2018
 * Time: 1:07
 */

namespace App\Http\Models\SyncUtilities;


use App\Http\Base\BaseModel;
use Faker\Provider\Base;

class StagingSyncTableModel extends BaseModel
{
    protected $table = "staging_sync_table";
    protected $fillable = [
        "id", "staging_table", "last_sync", "target_table", "target_model"
    ];
}