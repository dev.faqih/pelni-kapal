<?php
/**
 * Created by IntelliJ IDEA.
 * User: almustafa dekaulitz (sulaimanfahmi@gmail.com)
 * Date: 16/07/2018
 * Time: 0:11
 */

namespace App\Http\Models\SyncUtilities;


use App\Http\Base\BaseModel;

class SyncHandlersModel extends BaseModel
{
    /**
     * table name from database
     */
    protected $table = "sync";

    /**
     *   alias table for relation issues
     */
    protected $aliasTable = "sync";

    /**
     * primary key from table remember eloquent doesnt support composite primary key
     */
    protected $primaryKey = "id";
    /**
     * check the incrementing boolean if the incrementing from database is false then the value is false by default
     */
    public $incrementing = false;
    /**
     * fillable property for eloquent attribute cast
     */
    protected $fillable = [
        "id", "local_id", "server_id", "table_name", "marked_deleted", "need_sync", "sync_error",
        "sync_date", "last_sync", "created_at", "updated_at"
    ];


    public function getSyncData()
    {
        $this->getQuery()->where("sync.need_sync", "=", 1);
        return $this->getQuery()->get();
    }

}