<?php
/**
 * Created by IntelliJ IDEA.
 * User: almustafa dekaulitz (sulaimanfahmi@gmail.com)
 * Date: 12/08/2018
 * Time: 22:40
 */

namespace App\Http\Models\SyncUtilities;


use App\Http\Base\BaseModel;

class SyncTableHandlerModel extends BaseModel
{
    /**
     * table name from database
     */
    protected $table = "sync_table";

    /**
     *   alias table for relation issues
     */
    protected $aliasTable = "sync_table";

    /**
     * primary key from table remember eloquent doesnt support composite primary key
     */
    protected $primaryKey = "id";
    /**
     * check the incrementing boolean if the incrementing from database is false then the value is false by default
     */
    public $incrementing = false;
    /**
     * fillable property for eloquent attribute cast
     */
    protected $fillable = [
        "id", "table_name", "last_sync"
    ];
}