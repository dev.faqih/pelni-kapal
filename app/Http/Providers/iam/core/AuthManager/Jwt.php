<?php
/**
 * Created by IntelliJ IDEA.
 * User: Fahmi Sulaiman
 * Date: 09/11/2017
 * Time: 13:26
 */

namespace App\Http\Controllers\Providers\IAM\Core\AuthManager;

use App\Http\Base\BaseRequest;
use Exceptions\AuthorizationException;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class Jwt
{
    private static $instance;
    protected $_id, $jwtToken, $iis, $iat, $jti, $nbf, $exp, $aud, $curentTime, $logginAs, $model;

    public function __construct()
    {
        $this->curentTime = time() - 200;
    }

    static function jwt()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @param mixed $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function getJti()
    {
        return $this->jti;
    }

    /**
     * @return $this
     * @todo #phase test uncomment $thi->setiis();
     */
    public function buldJwtToken()
    {
        $this->setIis(BaseRequest::instance()->getHost());
        $this->setIat($this->curentTime);
        $this->setExp($this->curentTime + 3600);
        $this->setNbf($this->curentTime + 1800);
        $this->setJti(str_random(10));
        $this->setAud(BaseRequest::instance()->getClientIp());
        $signer = new Sha256();
        $token = (new Builder())->setIssuer($this->getIis())
            ->setAudience(BaseRequest::instance()->getClientIp())
            ->setId(str_random(10), true)
            ->setIssuedAt($this->getIat())
            ->setNotBefore($this->getNbf())
            ->setExpiration($this->getExp())
            ->set('_id', $this->getUserId())
            ->set("logginAs", $this->getLogginAs())
            ->sign($signer, $GLOBALS["container"]["app"]["jwt_key"])
            ->getToken();
        $this->setJwtToken($token->__toString());
        return $this;
    }

    /**
     * @param mixed $iis
     */
    public function setIis($iis)
    {
        $this->iis = $iis;
    }

    /**
     * @param mixed $iat
     */
    public function setIat($iat)
    {
        $this->iat = $iat;
    }

    /**
     * @param mixed $exp
     */
    public function setExp($exp)
    {
        $this->exp = $exp;
    }

    /**
     * @param mixed $nbf
     */
    public function setNbf($nbf)
    {
        $this->nbf = $nbf;
    }

    /**
     * @param mixed $jti
     */
    public function setJti($jti)
    {
        $this->jti = $jti;
    }

    /**
     * @param mixed $aud
     */
    public function setAud($aud)
    {
        $this->aud = $aud;
    }

    /**
     * @return mixed
     */
    public function getIis()
    {
        return $this->iis;
    }

    /**
     * @return mixed
     */
    public function getIat()
    {
        return $this->iat;
    }

    /**
     * @return mixed
     */
    public function getNbf()
    {
        return $this->nbf;
    }

    /**
     * @return mixed
     */
    public function getExp()
    {
        return $this->exp;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->_id;
    }

    /**
     * @return mixed
     */
    public function getLogginAs()
    {
        return $this->logginAs;
    }

    /**
     * @param mixed $jwtToken
     */
    public function setJwtToken($jwtToken = "")
    {
        if (empty($jwtToken)) {
            throw new AuthorizationException(StatusCode::$tokenNotFound_error, "Bearer token not found");
        }
        if (is_array($jwtToken)) {
            $this->jwtToken = str_replace("Bearer ", "", $jwtToken[0], $count);
            if ($count == 0) {
                throw new AuthorizationException(StatusCode::$invalidToken_error, "invalid token");
            }
        } else {
            $this->jwtToken = $jwtToken;
        }
        return $this;
    }

    public function extractJwtToken()
    {
        $signer = $signer = new Sha256();
        $jwtVerify = (new Parser())->parse($this->getJwtToken());
        if ($jwtVerify->verify($signer, $GLOBALS["container"]["app"]["jwt_key"]) == false) {
            throw new AuthorizationException(StatusCode::$invalidToken_error, "invalid token");
        }
        $this->setLogginAs($jwtVerify->getClaim("logginAs"));
        $this->setIis($jwtVerify->getClaim("iss"));
        $this->setId($jwtVerify->getClaim("_id"));
        $this->setIat($jwtVerify->getClaim("iat"));
        $this->setJti($jwtVerify->getClaim("jti"));
        $this->setAud($jwtVerify->getClaim("aud"));
        $this->setExp($jwtVerify->getClaim("exp"));
        $this->setNbf($jwtVerify->getClaim("nbf"));
        return $this;
    }

    /**
     * @return mixed
     */
    public function getJwtToken()
    {
        return $this->jwtToken;
    }

    /**
     * @param mixed $logginAs
     */
    public function setLogginAs($logginAs)
    {
        $this->logginAs = $logginAs;
        return $this;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->_id = $id;
        return $this;
    }

    public function isValidAud()
    {
        if ($this->getAud() !== BaseRequest::instance()->getClientIp()) {
            throw new AuthorizationException(StatusCode::$invalidAud_error, "invalid audience");
        }
    }

    /**
     * @return mixed
     */
    public function getAud()
    {
        return $this->aud;
    }

    public function validateToken()
    {
        if (empty($this->getNbf()) || empty($this->getExp())) {
            throw new \Exception("invalid nbf or exp");
        }
        $this->isExpired();
        $this->isValidIss();
    }

    public function isExpired()
    {

        if ($this->getExp() < $this->curentTime) {
            if ($this->getNbf() < $this->curentTime) {
                throw new AuthorizationException(StatusCode::$tokenExpired_error, "Token Expired");
            } else {
                throw new AuthorizationException(StatusCode::$tokenNeedRefresh_error, "Your token is almost expired need REFRESH TOKEN !");
            }
        }
    }

    public function isValidIss()
    {
        if ($this->getIis() !== BaseRequest::instance()->getHost()) {
            throw new AuthorizationException(StatusCode::$invalidIss_error, "invalid Issuer");
        }
    }


}