<?php
/**
 * Created by IntelliJ IDEA.
 * User: almustafa dekaulitz (sulaimanfahmi@gmail.com)
 * Date: 01/03/2018
 * Time: 12:22
 */

namespace App\Http\Controllers\Providers\IAM\Core\AuthManager;


class PasswordHashes
{
    protected $options = [];

    /**
     * @param $password
     * @return bool|string
     */
    static function hashingPassword($password): string
    {
        return password_hash($password, PASSWORD_DEFAULT, self::getOptions());
    }

    /**
     * @return array
     */
    public static function getOptions(): array
    {
        return [
            'cost' => 12,
        ];
    }

    /**
     * @param $password
     * @param $hash
     * @return bool
     */
    static function verifyPassword($password, $hash): bool
    {

        if (password_verify($password, $hash)) {
            return true;
        }
        return false;
    }
}
