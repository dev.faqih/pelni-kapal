<?php

namespace App\Http\Controllers\Providers\IAM\Core\AuthManager;
/*
 * Success Code
 */

class StatusCode
{
    /* general response  */

    static $success = [
        "code" => 2000,
        "message" => "SUCCESS"
    ];
    static $created = [
        "code" => 2002,
        "message" => "DATA_CREATED"
    ];
    static $deleted = [
        "code" => 2003,
        "message" => "DATA_DELETED"
    ];
    static $updated = [
        "code" => 2004,
        "message" => "DATA_UPDATED"
    ];
    /** end  */

    /* account list code */
    static $accountActivated_success = [
        "code" => 2010,
        "message" => "ACCOUNT_ACTIVATED"
    ];
    static $accountNotFound_error = [
        "code" => 2011,
        "message" => "ACCOUNT_NOT_FOUND"
    ];
    static $emailNotFound_error = [
        "code" => 2012,
        "message" => "Email_NOT_FOUND"
    ];
    static $invalidUserName_error = [
        "code" => 2013,
        "message" => "INVALID_USERANAME_OR_PASSWORD"
    ];
    static $expiredEmailToken = [
        "code" => 2014,
        "message" => "EXPIRED_EMAIL_TOKEN"
    ];
    static $accountAlreadyActive_error = [
        "code" => 2015,
        "message" => "EMAIL_ALREADY_ACTIVATED"
    ];
    static $accountIsNotActivated_error = [
        "code" => 2016,
        "message" => "ACCOUNT_NEED_ACTIVATED"
    ];
    static $emailAlreadyExist_error = [
        "code" => 2017,
        "message" => "EMAIL_ALREADY_EXIST"
    ];
    static $uNameAlreadyExist_error = [
        "code" => 2018,
        "message" => "UNAME_ALREADY_EXIST"
    ];
    /* end account code */

    /* validation code */
    static $validationException_error = [
        "code" => 3000,
        "message" => "VALIDATION_EXCEPTION"
    ];
    static $validationFieldRequired_error = [
        "code" => 3001,
        "message" => "FIELD_CANNOT_BE_NULL"
    ];
    static $dataNotFound_error = [
        "code" => 3002,
        "message" => "DATA_NOT_FOUND"
    ];

    static $propertyNotFound_error = [
        "code" => 3003,
        "message" => "PROPERTY_NOT_FOUND"
    ];


    static $validationBodyData_error = [
        "code" => 1002,
        "message" => "MISSING BODY DATA"
    ];

    /* validation end */

    /* server error */
    static $pageNotFound_error = [
        "code" => 4000,
        "message" => "PAGE_NOT_FOUND"
    ];
    static $internalServerError_error = [
        "code" => 4001,
        "message" => "INTERNAL_SERVER_ERROR"
    ];
    /* end server code */

    /* permissions access */
    static $unauthorizedAccess_error = [
        "code" => 5000,
        "message" => "UNAUTHORIZED_ACCESS"
    ];
    static $tokenExpired_error = [
        "code" => 5001,
        "message" => "TOKEN_ACCESS_EXPIRED"
    ];

    static $invalidToken_error = [
        "code" => 5002,
        "message" => "TOKEN_INVALID"
    ];
    static $tokenNeedRefresh_error = [
        "code" => 5003,
        "message" => "TOKEN_NEED_REFRESH"
    ];

    static $invalidAud_error = [
        "code" => 5004,
        "message" => "TOKEN_INVALID_AUD"
    ];
    static $invalidIss_error = [
        "code" => 5005,
        "message" => "TOKEN_INVALID_ISS"
    ];
    static $tokenNotFound_error = [
        "code" => 5005,
        "message" => "TOKEN_NOT_FOUND"
    ];

    static $projectTokenNotFound_error = [
        "code" => 6000,
        "message" => "TOKEN_NOT_FOUND"
    ];
    static $projectTokenIsReachMax_error = [
        "code" => 6001,
        "message" => "TOKEN_IS_OVER_USAGE"
    ];
    static $projectNotFound_error = [
        "code" => 6002,
        "message" => "PROJECT_NOT_FOUND"
    ];

}





