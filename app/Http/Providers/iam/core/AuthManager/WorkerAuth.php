<?php
/**
 * Created by IntelliJ IDEA.
 * User: Fahmi Sulaiman
 * Date: 09/11/2017
 * Time: 13:26
 */

namespace App\Http\Controllers\Providers\IAM\Core\AuthManager;


use App\Http\Base\BaseMail;
use App\Http\Models\AdministratormenuModel;
use App\Http\Models\SmpappmenuModel;
use App\Http\Models\LoginlogModel;
use App\Http\Models\SystemparameterModel;

use App\Http\Models\LokasiitemModel;
use App\Http\Models\ItemcategoryModel;


class WorkerAuth
{
    //  use AuthTrait;
    private static $instance;
    private static $id;
    private static $groupId;
    public $model, $menuModel, $groupAdminModel, $groupAccess;
    protected $session;


    static function auth()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public static function getAuth()
    {
        return json_decode(\Session::get("auth"), false);
    }

    public static function getMenu()
    {
        return session("authMenu");
    }

    public function authLogin($property)
    {

        $exist = $this->model->getQuery();
        $exist->where($this->model->getAliasTable() . '.username', "=", $property["username"]);
        
        $exist->leftjoin("roles as roles", "roles.id", "=", "users.roleId")
                ->leftJoin("kapal as kapal", "kapal.id", "=", "users.kapalId")
                ->select(["users.*", "roles.id as groupId", "kapal.kodeKapal", "kapal.namaKapal"]);
        // echo   $exist->toSql(); exit();
        $exist = $exist->first();
        if ($exist) {
            if($exist->aplikasi == $GLOBALS["env"]["APLICATION_TYPE"]) {
                if($exist->kapalId == $GLOBALS["env"]["KAPAL_ID"]) {
                    if($exist->aktif > 0) {
                        if (PasswordHashes::verifyPassword($property["password"], $exist->password)) {
                            self::$id = $exist->id;
                            self::$groupId = $exist->groupId;
                            $exist->orgId =  substr("000000" . $exist->kapalId, -4);

                            $exist->roleLokasiItem = (new LokasiitemModel())->joinRelationRoleLokasiItem()->getQuery()->select(["lokasiitem.*"])->where("roleLokasiItem.roleId", "=", $exist->roleId)->get();
                            $exist->roleCategory = (new ItemcategoryModel())->joinRelationRoleCategory()->getQuery()->select(["itemCategory.*"])->where("roleCategory.roleId", "=", $exist->roleId)->get();
                            
                            $modelSysParams = (new SystemparameterModel())->getQuery()->get(); $sysParams = [];
                            foreach($modelSysParams as $sysParam) { $sysParams[$sysParam->variableName] = $sysParam->variableValue; } $exist->sysParams = (object) $sysParams;

                            $arrTemp = []; foreach($exist->roleLokasiItem as $row) { $arrTemp[] = $row->id; } $exist->arrLokasi = $arrTemp;
                            $arrTemp = []; foreach($exist->roleCategory as $row) { $arrTemp[] = $row->id; } $exist->arrCategory = $arrTemp;

                            \Session::put('auth', collect($exist)->toJson());
                            \Session::put('authMenu', $this->renderMenu());
                            
                            $input["username"] = $property["username"];
                            $input["date_updated"] = $input["loginStartDate"] = date("Y-m-d H:i:s");
                            $input["kapalId"] = $exist->kapalId;

                            $login = new LoginlogModel();
                            $login->fill($input);
                            $login->save();
                        } else {
                            return "Password Anda tidak cocok.";
                        }
                    } else {
                        return "User sudah tidak aktif! Silahkan hubungi Nahkoda/Admin untuk informasi lebih lanjut!";
                    }
                } else {
                    return "Aplikasi Kapal #" .  $GLOBALS["env"]["KAPAL_ID"] . ", dan tidak sesuai dengan setting login Anda.";
                }
            } else {
                return "Aplikasi Kapal " .  $GLOBALS["env"]["APLICATION_TYPE"] . ": Maaf, tidak sesuai dengan setting login Anda.";
            }
        } else {
            return "User login anda tidak ditemukan.";
        }

    }

    public function renderMenu()
    {
        $menu = $this->collectingMenu();
        return $this->generateMenu($menu);

    }

    private function collectingMenu($menuId = 0)
    {
        $i = 0;
        $list = [];
        $menuModel = (new AdministratormenuModel())->getQuery();
        $menuModel
            ->select([
                "administratormenu.id",
                "administratormenu.menuname",
                "administratormenu.url",
                "administratormenu.icon",
                "administratormenu.parentmenu"
            ]);
        $menuModel->where("parentmenu", "=", $menuId);
        if ($menuId !== 0) {
            $menuModel->join("administratorgroupaccess as administratorgroupaccess", "administratorgroupaccess.accessid", "=", "administratormenu.id");
            $menuModel->where("administratorgroupaccess.groupid", "=", self::$groupId)->where("administratormenu.active", "=", 1);
        }
        $menuModel->orderBy("position");
        $menus = $menuModel->get();
        foreach ($menus as $menu) {
            $list[$i]["menudId"] = $menu->id;
            $list[$i]["parentId"] = $menu->parentmenu;
            $list[$i]["menuname"] = $menu->menuname;
            $list[$i]["url"] = $menu->url;
            $list[$i]["icon"] = $menu->icon;
            $list[$i]["child"] = $this->collectingMenu($menu->id);
            if (($menu->parentmenu == 0))
                $list[$i]["isParent"] = 1;
            else if (!empty($list[$i]["child"]))
                $list[$i]["isParent"] = 1;
            else
                $list[$i]["isParent"] = 0;
            if (($menu->parentmenu != $menu->id) && ($menu->parentmenu != 0))
                $list[$i]["hasParent"] = 1;
            else {
                $list[$i]["hasParent"] = 0;
            }
            $i++;
        }

        return $list;
    }

    public function generateMenu($menu = null)
    {
        $list = "";
        for ($i = 0; $i < count($menu); $i++) {
            if ($menu[$i]["isParent"] == 1) {
                if (!empty($menu[$i]["child"])) {
                    $list .= '<li class="nav-item dropdown">';
                    $list .= '<a class="nav-link dropdown-toggle text-white" href="#" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">';
                    $list .= '<i class="' . $menu[$i]["icon"] . '"></i> <span>' . $menu[$i]['menuname'] . '</span>';
                    $list .= '</a>';
                    $list .= '<div class="dropdown-menu">';
                    $list .= $this->generateSubMenu($menu[$i]["child"]);
                    $list .= '</div>';
                }
            } else {
                $list .= ' <li class="nav-item"><a  class="nav-link "  href="' . url($menu[$i]["url"]) . '" url="' . url($menu[$i]["url"]) . '">' . $menu[$i]['menuname'] . '</a></li>';
            }
            $list .= '</li>';
        }
        return $list;
    }


    public function generateSubMenu($menu)
    {
        $list = "";
        for ($i = 0; $i < count($menu); $i++) {
            if (!empty($menu[$i]["child"])) {
                $list .= '<div class="tree-view" href="#">';
                $list .= '<a class="dropdown-item dropdown-toggle" href="#">' . $menu[$i]['menuname'] . '</a>';
                $list .= ' <div class="dropdown-menu">';
                $list .= $this->generateSubMenu($menu[$i]["child"]);
                $list .= '</div>';
                $list .= '</div>';
            } else {
                $list .= ' <a class="dropdown-item" href="' . url($menu[$i]["url"]) . '"><i class="' . $menu[$i]["icon"] . '"></i> ' . $menu[$i]['menuname'] . '</a>';
            }
        }
        return $list;
    }

    public function hasLogin()
    {
        return \Session::has("auth");
    }

    public function logout()
    {
        if($this->hasLogin()) {
            $userSession = $this->getAuth();

            $logout = LoginlogModel::where("username", $userSession->username)->orderBy("loginStartDate", "desc")->first();
            $input["loginEndDate"] = $input["date_updated"] = date("Y-m-d H:i:s");
            if(! $logout->update($input)) {
                // throw new \Exception();
            }
        }
        \Session::flush('authMenu');
        \Session::flush("auth");
    }

    public function hasAccess()
    {

    }

    public function forgotPassword($user, $password)
    {
        $mail = new BaseMail();
        $text = "";
        $text .= '<h1> Hai ' . $user->userName . ' </h1>';
        $text .= '<h3> this your new password ' . $password;
        $text .= '</h3>';
        $text .= " Lets code cherrs !!";
        $text .= '</p>';
        $mail->setContent($text);
        $mail->sendMail("Reovery Password", $user->email);
    }


}