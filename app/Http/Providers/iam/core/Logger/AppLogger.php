<?php
/**
 * Created by IntelliJ IDEA.
 * User: almustafa dekaulitz (sulaimanfahmi@gmail.com)
 * Date: 20/09/2018
 * Time: 11:36
 */

namespace App\Http\Controllers\Providers\IAM\Logger;


use Carbon\Carbon;
use Monolog\Handler\FirePHPHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class AppLogger extends Logger
{
    /**
     * @throws \Exception
     */
    public static function syncLogger($data)
    {
        $logger = new Logger('sync-logger');
        $logger->pushHandler(new StreamHandler(BASE_PATH . "../storage/logs/sync/" . 'sync-' . (new \DateTime())->format("Ym") . ".logs", Logger::DEBUG));
        $logger->pushHandler(new FirePHPHandler());
        if (is_array($data)) {
            $data = json_encode($data);
        }
        $logger->info($data);
    }
}