<?php
/**
 * Created by IntelliJ IDEA.
 * User: Fahmi Sulaiman
 * Date: 09/11/2017
 * Time: 14:48
 */

namespace App\Http\Controllers\Providers\IAM\Core\Mail;



class CoreMail extends \PHPMailer
{
    protected $app = array();
    protected $mailReceiver;
    public function __construct()
    {

        $this->app = $GLOBALS["env"]["mail"];
        $this->setUpMailConf();
        parent::__construct(\phpmailerException::class);
    }

    public function setUpMailConf()
    {
        $this->SMTPDebug = $this->app["MAIL_DEBUG_MODE"];
        $this->isSMTP();
        $this->isHTML();
        $this->Host = $this->app["MAIL_HOST"];
        $this->SMTPAuth = TRUE;
        $this->SMTPSecure = $this->app["MAIL_SMTPSecure"];
        $this->Port = $this->app["MAIL_PORT"];
        $this->Username = $this->app["MAIL_USERNAME"];
        $this->Password = $this->app["MAIL_PASSWORD"];
        $this->setFrom($this->app["MAIL_SENDER"], $this->app["MAIL_SENDER_ALIAS"]);
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->Subject = $subject;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->Subject;
    }

    public function setMailReceiver($email)
    {
        $this->addAddress($email);
        $this->mailReceiver = $email;
    }

    /**
     * @return mixed
     */
    public function getMailReceiver()
    {
        return $this->mailReceiver;
    }

    public function setBody($body = "")
    {

        $this->Body = $body;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->Body;
    }
}