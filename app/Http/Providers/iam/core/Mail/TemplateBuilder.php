<?php
/**
 * Created by IntelliJ IDEA.
 * User: Fahmi Sulaiman
 * Date: 09/11/2017
 * Time: 14:48
 */

namespace App\Http\Controllers\Providers\IAM\Core\Mail;


use App\Http\Controllers\Providers\IAM\Core\Mail\CoreMail;

class TemplateBuilder extends CoreMail
{

    protected $header;
    protected $footer;
    protected $content = "";

    /**
     * @return mixed
     */
    public function getHeader()
    {
        $this->header = "";
        $this->header .= '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html lang="en"><head> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> <meta name="viewport" content="width=device-width, initial-scale=1"> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> <title>Gen</title> <style type="text/css"> body, table, td, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; font-family: \'Roboto\', sans-serif;}.container{padding: 20px;}.content{min-height: 100px;color: #811555; font-size: 15px;
    line-height: 20px;}.title{padding: 20px;}.title a{color: antiquewhite; font-size: 25px; line-height: 18px; text-decoration: none;}.menu-left{float: left;}.menu-right{float: right;}.links ul{list-style-type: none; margin: 0; padding: 0;}.links ul li{padding: 5px; float: left;}.footer{border-top: 4px solid #4285f4;}</style></head><body style="margin:0; padding:0; "><div style="height: 100px; background-color: #7f0055"> <div style="float: left;width: 100%"> <div class="container title"> <h3><a href="asdasd">WorkerApi</a></h3> </div></div></div>';
        $this->header .= '<div class="container content">';
        return $this->header;
    }


    public function getFooter()
    {
        $this->footer = "";
        $this->footer .= '</div>';
        $this->footer .= '<footer> <div class="container footer"> <div class="links menu-left"> <ul> <li><a>links</a></li><li><a>links</a></li><li><a>links</a></li><li><a>links</a></li><li><a>links</a></li></ul> </div><div class="links menu-right"> <ul> <li><a>l2inks</a></li><li><a>links</a></li><li><a>links</a></li><li><a>links</a></li><li><a>links</a></li></ul> </div></div></footer></body></html>';
        return $this->footer;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {

        $this->content = $content;
    }


    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    public function getTemplateEmail()
    {
        $string = "";
        $string .= $this->getHeader();
        $string .= $this->getContent();
        $string .= $this->getFooter();
        return $string;


    }
}