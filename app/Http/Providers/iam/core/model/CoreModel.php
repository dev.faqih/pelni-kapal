<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 10/08/2017
 * Time: 20:29
 */

namespace App\Http\Controllers\Providers\IAM\Core\Model;

use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Eloquent\Model;

class CoreModel extends Model
{
    use Pagination;

    public $timestamps = false;
    public $query, $corePagination;
    protected $aliasTable = "";
    protected $fillable = [];
    protected $paginate = [];
    protected $aliasPaging = [];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);
        $this->query = $this->getManager();
        $this->corePagination = new CorePagination($this);
    }


    public function getManager()
    {
        $alias = !empty($this->getAliasTable()) ? " as " . $this->getAliasTable() : " as " . $this->getTable();
        return Manager::table($this->getTable() . $alias);

    }
    public function destroyBulk($property, $params = [])
    {
        Manager::table($this->getTable())->whereIn($property, $params)->delete();
    }

    public function softDelete()
    {
        $this->getQuery()->update([
            $this->getAliasTable() . "status" => 0
        ]);
    }


    public function softDeleteBulk($property, $params = [])
    {
        Manager::table($this->getTable())->whereIn($property, $params)->update([
            "status" => 0
        ]);
    }

    public function deleteData()
    {
        return Manager::table($this->getTable());
    }
    public function deleteBulk($property, $params = [])
    {
        Manager::table($this->getTable())->whereIn($property, $params)->update([
            "status" => 0
        ]);
    }
    public function getAliasTable()
    {
        return $this->aliasTable;
    }

    public function getAliasPaging()
    {
        return $this->aliasPaging;
    }

    public function beginTransaction()
    {
        Manager::connection()->beginTransaction();
    }

    public function rollBackTransaction()
    {
        return Manager::connection()->rollBack();
    }

    public function commitTransaction()
    {
        return Manager::connection()->commit();
    }

    public function setProperty($attributes)
    {
        $this->fill($attributes);
        return $this;
    }

    public function withRelation()
    {
        $this->callRelation();
        return $this;
    }

    public function callRelation()
    {

        $class_methods = get_class_methods($this);
        foreach ($class_methods as $method_name) {
            $method = substr($method_name, 12);
            if (method_exists($this, "joinRelation" . $method)) {
                call_user_func(array($this, "joinRelation" . $method));
            }
        }
        return $this;
    }

    public function paging()
    {
        return $this->corePagination;
    }


    /**
     * @return \Illuminate\Database\Query\Builder
     */
    public function getQuery()
    {
        return $this->query;
    }

    public function setBuilder()
    {

        return $this;
    }

    public function delete()
    {
        if (is_null($this->getKeyName())) {
            throw new \Exception('No primary key defined on model.');
        }
        if ($this->exists) {
            if ($this->fireModelEvent('deleting') === false) {
                return false;
            }
            $this->touchOwners();
            $this->performDeleteOnModel();
            $this->exists = false;
            return true;
        }
    }

    public function update(array $attributes = [], array $options = [])
    {
        if (!$this->exists) {
            return false;
        }
        return $this->fill($attributes)->save($options);
    }

//    public function save(array $options = [])
//    {
//        //  $this->beforeEnterIntoDatabase();
//        $query = $this->newQueryWithoutScopes();
//        if ($this->exists) {
//            $saved = $this->performUpdate($query, $options);
//        } else {
//            $saved = $this->performInsert($query, $options);
//        }
//        if ($saved) {
//            $this->finishSave($options);
//        }
//        return $saved;
//    }

    public function beforeEnterIntoDatabase()
    {
        return $this;
    }

}