<?php

/**
 * Created by IntelliJ IDEA.
 * User: Fahmi Sulaiman
 * Date: 22/11/2017
 * Time: 14:13
 */

namespace App\Http\Controllers\Providers\IAM\Core\Model;


class CorePagination
{
    public $model, $countRow, $sizeRow, $input, $request, $sortRow, $direction, $page;
    private $tmp, $searchBy = "", $searchValue = "", $tmpModel;


    public function __construct($model)
    {
        $this->model = $model;
        $this->tmp = $model;
        $this->request = \Request::instance();
        $this->size = $this->request->input("size") ? $this->request->input("size") : 10;
        $this->page = $this->request->input("page") ? $this->request->input("page") : 1;
    }

    private function paginate()
    {

        $this->sizeRow = $this->request->input("size") ? $this->request->input("size") : 10;
        $this->page = $this->request->input("page") ? $this->request->input("page") : 1;
        $this->model->getQuery()->take($this->size)
            ->offset($this->size * ($this->page - 1));
        return $this;
    }

    public function sorting()
    {
        $query = explode(":", $this->request->input("sort"));
        if (key_exists($query[0], $this->model->getAliasPaging())) {
            $direction = $query[1] == "desc" ? "desc" : "asc";
            $this->model->getQuery()->orderBy($this->model->getAliasPaging()[$query[0]], $direction);
        } else { }
        return $this;
    }


    public function filter($condition = [], $boolean = "and")
    {
        if (!empty($condition)) {
            foreach ($condition as $query) {
                if ($boolean == "and")
                    $this->model->getQuery()->where($query[0], $query[1], $query[2]);
                else
                    $this->model->getQuery()->whereOr($query[0], $query[1], $query[2]);
            }
        }

        if (!empty($this->request->input("q"))) {
            $q = explode("||", $this->request->input("q"));
            $fieldIDs = ["id", "fromLocationId", "toLocationId", "kapalId", "isApproved", "expense_account", "status", "toKapalId", "fromKapalId", "itemId", "lokasiId", "type", "roleId", "voyageKode", "destinationAccount", "tahun"];
            $fieldDates = ["tglPermintaan", "dateCreated", "process_date", "voyageStart", "voyageEnd", "date", "loginStartDate", "expectedReceivedDate", "dateRequired", "tglPenggunaan", "transactionDate"];
            foreach ($q as $row) {
                $query = explode(":", $row);
                if (count($query) > 1) {
                    if ($query[0] == "keyword") {
                        $this->model->getQuery()->where("item.namaItem", "ILIKE", "%" . $query[1] . "%")
                            ->orWhere("item.deskripsi", "ILIKE", "%" . $query[1] . "%")
                            ->orWhere("item.part_number", "ILIKE", "%" . $query[1] . "%")
                            ->orWhere("kapalId.namaKapal", "ILIKE", "%" . $query[1] . "%");

                        // $this->model->getQuery()->where("item.namaItem" , "ILIKE", "%" . $query[1] . "%");
                        // $this->model->getQuery()->where("item.deskripsi", "ILIKE", "%" . $query[1] . "%");
                        // $this->model->getQuery()->where("", "ILIKE", "%" . $query[1] . "%");
                    } else if (key_exists($query[0], $this->model->getAliasPaging())) {
                        if (in_array($query[0], $fieldIDs)) {
                            $this->model->getQuery()->where($this->model->getAliasPaging()[$query[0]], "=", $query[1]);
                        } else if (in_array($query[0], $fieldDates)) {
                            $this->model->getQuery()->whereRaw('"' . str_replace(".", '"."', $this->model->getAliasPaging()[$query[0]]) . "\"::date =  date '" . $query[1] . "'");
                        } else if ($query[0] == "dateStart") {
                            $this->model->getQuery()->whereRaw('"' . str_replace(".", '"."', $this->model->getAliasPaging()[$query[0]]) . "\"::date >=  date '" . $query[1] . "'");
                        } else if ($query[0] == "dateEnd") {
                            $this->model->getQuery()->whereRaw('"' . str_replace(".", '"."', $this->model->getAliasPaging()[$query[0]]) . "\"::date <=  date '" . $query[1] . "'");
                        } else {
                            $this->model->getQuery()->where($this->model->getAliasPaging()[$query[0]], "ILIKE", "%" . $query[1] . "%");
                        }
                        $this->searchBy = $query[0];
                        $this->searchValue = $query[1];
                    }
                }
            }
        }

        $this->tmpModel = $this->model;
        return $this;
    }

    public function getPaging()
    {
        $this->paginate();
        $rows = $this->model->getQuery()->get();
        $this->countRow = $this->getCount($this->model);

        $totalData = $this->countRow;
        $pageCount = ceil($totalData / $this->size); //
        return [
            "searchBy" => $this->searchBy,
            "searchValue" => $this->searchValue,
            "rows" => $rows,
            "size" => $this->sizeRow,
            "page" => $this->page,
            'rowCount' => $totalData,
            'pageCount' => $pageCount,
        ];
    }

    public static function getCount($model)
    {
        $model->query->orders = null;
        $model->query->limit = null;
        $model->query->offset = null;
        return $model->query->count();
    }

    public function getPagingData()
    {

        return $this->model->getQuery()->get();
    }

    public function getSizeRow()
    {
        return $this->sizeRow;
    }

    public function getPage()
    {
        return $this->page;
    }
}
