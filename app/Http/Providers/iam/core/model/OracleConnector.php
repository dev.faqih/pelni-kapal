<?php
/**
 * Created by IntelliJ IDEA.
 * User: almustafa dekaulitz (sulaimanfahmi@gmail.com)
 * Date: 20/09/2018
 * Time: 10:39
 */

namespace App\Http\Providers\iam\core\model;


use PDO;

class OracleConnector
{
    private static $instance;
    protected $conn = null;
    protected $query = "";
    private $host, $username, $password;

    public function __construct()
    {
        $this->host = $GLOBALS["env"]["oracle"]["host"];
        $this->username = $GLOBALS["env"]["oracle"]["username"];
        $this->password = $GLOBALS["env"]["oracle"]["password"];
        $this->conn = new PDO($this->host, $this->username, $this->password);

    }


    /**
     * @return $this
     */
    public function getConn()
    {
        $this->conn;
        return $this;
    }

    /**
     * @param $query
     */
    public function select($query)
    {
        $this->query = $this->conn->query($query);
        return $this;
    }


    /**
     * @return array
     */
    public function fetchAll()
    {
        return $this->query->fetchAll(PDO::FETCH_OBJ);
    }


}