<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 12/08/2017
 * Time: 0:00
 */

namespace App\Http\Controllers\Providers\IAM\Core\Model;

/*
custom pagination library from eloquent that will injecting the query string param directly into the model query
*/

trait Pagination
{
    public $pagingData = array();
    public $request, $size, $sort, $direction, $page, $selectData, $input, $pagination, $countRow;

    /*
     * initilized the pagination variable and parameter
     *
     * pagination variable
     * size  for getting how much data it will take by default 10
     * sort for the sorting variable what you want and dir for level sorting
     * page for directing to page number
     */
    public function paginating($input)
    {

        $this->request = \Request::instance();
        $this->input = $input;
        $this->paginate();
        return $this;
    }

    public function paginate()
    {

        $this->size = $this->request->input("size") ? $this->request->input("size") : 10;
        $this->page = $this->request->input("page") ? $this->request->input("page") : 1;
        $this->getQuery()->take($this->size)
            ->offset($this->size * ($this->page - 1));
        return $this;
    }

    public function queryFilter()
    {
        if (null == ($this->request->input("q")))
            return $this;
        $query = explode(":", $this->request->input("q"));
        if (key_exists($query[0], $this->aliasPaging))
            $this->query->where($this->aliasPaging[$query[0]], "like", "%" . $query[1] . "%");

        return $this;
    }


    public function filtering()
    {
        if (!empty($this->input("criteria"))) {
            foreach ($this->input("criteria") as $crt) {
                $value = $crt["value"];
                if (array_key_exists($crt["criteria"], $this->aliasPaging)) {
                    $this->query->where($this->aliasPaging[$crt["criteria"]], "=", $value);
                }
            }
        }

        return $this;
    }

    public function getPaginate()
    {

        $pageCount = ceil($this->countRow / $this->size); //
        return [
            "rows" => $this->getPagingData(),
            "size" => $this->size,
            "page" => $this->page,
            'rowCount' => $this->countRow,
            'pageCount' => $pageCount,
        ];
    }

    public function getPagingData()
    {
        return !empty($this->pagingData) ? $this->pagingData : $this->query->get();
    }

    public function flipAliasPaging()
    {
        $string = "";

        foreach ($this->aliasPaging as $key => $value) {
            $string .= $value . ' as ' . $key;
            $string .= ",";
        }

        return $string;
    }


    public function ordering()
    {
        $this->sort = $this->request->input("sort") ? $this->request->input("sort") : $this->getKeyName();
        $this->direction = $this->request->input("direction") ? $this->request->input("direction") : "desc";
        $this->query->orderBy($this->aliasPaging[$this->sort], $this->direction);
        return $this;
    }
}