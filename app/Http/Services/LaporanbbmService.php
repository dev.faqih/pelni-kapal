<?php

/**
 This project has been generated by worker generator
 has been made by fahmi sulaiman
 sulaimanfahmi@gmail.com
 check out my github @dekaulitz
 */

namespace App\Http\Services;

use App\Http\Models\LaporanbbmModel;
use App\Http\Services\LaporanbbmdetailsService;
use App\Http\Base\BaseService;
use App\Http\Models\SystemparameterModel;
use App\Http\Models\LaporanbbmdetailsModel;

class LaporanbbmService extends BaseService
{
	/**
     PelumasService constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->model = new LaporanbbmModel();
		$this->detailService = new LaporanbbmdetailsService();

		$this->penerimaanBarang = new PenerimaanbarangService();
		$this->penerimaanBarangIO = new PenerimaanbarangioService();
		$this->penggunaanBarang = new PenggunaanbarangService();
		$this->adjustmentBBM = new AdjustmentbbmsService();
	}

	public function index()
	{
		return $this->model->getQuery()->get();
	}

	public function pagingBulan($request)
	{
		$auth = json_decode($request->session()->get('auth'));

		$this->model->getQuery()
			->where("laporanbbm.kapalId", "=", $auth->kapalId)
			->where("laporanbbm.tipe", "=", "MONTH");

		$input = $request->all();
		if (!isset($input["sort"])) {
			$this->model->getQuery()->orderBy("laporanbbm.tahun", "desc")->orderBy("laporanbbm.bulan", "desc");
		}

		$this->model->paging()->filter()->sorting();
		return $this->model->paging()->getPaging();
	}

	public function pagingVoyage($request)
	{
		$auth = json_decode($request->session()->get('auth'));

		$this->model->getQuery()
			->where("laporanbbm.kapalId", "=", $auth->kapalId)
			->where("laporanbbm.tipe", "=", "VOYAGE");

		$input = $request->all();
		if (!isset($input["sort"])) {
			$this->model->getQuery()->orderBy("laporanbbm.voyage_to", "desc")->orderBy("laporanbbm.voyage_from", "desc");
		}

		$this->model->paging()->filter()->sorting();
		return $this->model->paging()->getPaging();
	}

	/**
	 *  get one raw row data from table by id
	 *  you can ad the relation like $this->model->joinSomething()->getQuery()->where("aliastable.field","=",$value)->first()
	 *  or like $this->model->joinSomething()->getQuery()->where("aliastable.field","=",$value)->select(["aliastable.fieldname"])->first(); its mean you want to join an take spesific value
	 *  and you want to take value like this $model->fieldname
	 *  @param $id
	 *  @return LaporanbbmModel
	 */
	public function show($request, $id)
	{
		$auth = json_decode($request->session()->get('auth'));

		$model = $this->model->getQuery()
			->where("laporanbbm.kapalId", "=", $auth->kapalId)
			->where("laporanbbm.id", "=", $id)->first();

		if ($model) {
			if ($model->tipe == "VOYAGE") {
				$model->details = $this->detailService->showByVoyage($model->id);
			} else if ($model->tipe == "MONTH") {
				$model->details = $this->detailService->showByMonth($model);
			} else {
				$model = null;
			}
		}
		return $model;
	}

	public function getLatestDataReport()
	{
		return $this->model->getQuery()
			->where("laporanbbm.tipe", "=", "VOYAGE")
			->orderBy("laporanbbm.voyage_to", "desc")
			->first();
	}


	/**
	 *  this is a base function for storing data you with transaction by default
	 *  you can customization base on your business requirement as you want, you can add other object or you can to loop anc excecute the process in single process
	 *  @param array $input
	 *  @return LaporanbbmModel
	 *  @throws \Exception
	 */
	public function store($input)
	{

		$data = [
			"tahun"         => date("Y"),
			"bulan"         => date("n", strtotime($input["voyage_from"])),
			"bulan_text"    => date("F", strtotime($input["voyage_from"])),
			"voyage_id"     => $input["voyageKode"],
			"voyage_name"   => $input["voyageName"],
			"voyage_num"    => $input["voyageKode"],
			"voyage_from"   => $input["voyage_from"],
			"voyage_to"     => $input["voyage_to"],
			"keterangan"    => $input["keterangan"],
			"jarak_tempuh" 	=> $input["jarak_tempuh"],
			"tipe"          => "VOYAGE",
			"is_locked"     => "N",
			"kapalId"       => $input["kapalId"]
		];

		$dock = new LaporanbbmModel();
		$dock->fill($data);
		if (!$dock->save()) {
			throw new \Exception();
		}
		return $dock->id;
	}


	/**
	 * this function will delete much data, in one action
	 * the ids is a parameter from view like 1,2,3,4 and the parameter will be exploded into single array
	 * you can remove this main alogorithm with your business requirement like soft delete or anything you want
	 *  @param $ids
	 */
	public function delete($ids)
	{
		$params = explode(".", $ids);
		$this->model->deleteBulk("id", $params);
	}


	/**
	 *  this is a base function for update and delete data by default.
	 *  you can add and custom as you want base on your requirement you can add this function as core function for handle update data from table or 
	 *  you can add multiple relation proses after update in this function 
	 *  @param $id
	 *  @param array $input 
	 *  @return LaporanbbmModel
	 *  @throws \Exception
	 */
	public function update($ID, $input)
	{
		$bbm = LaporanbbmModel::find($ID);
		$bbm->fill(["keterangan" => $input["keterangan"], "jarak_tempuh" => $input["jarak_tempuh"]]);
		if (!$bbm->update()) {
			throw new \Exception();
		}

		if (isset($input["pelabuhan"])) {
			foreach ($input["pelabuhan"] as $itemDetailID => $pelabuhan) {
				$bbm = LaporanbbmdetailsModel::find($itemDetailID);
				$bbm->fill(["pelabuhan" => $pelabuhan]);
				if (!$bbm->update()) {
					throw new \Exception();
				}
			}
		}

		return $bbm;
	}

	//update is_locked
	public function isLocked($id, $isLocked)
	{
		$bbm = LaporanbbmModel::find($id);
		$bbm->fill(["is_locked" => $isLocked]);
		if (!$bbm->update()) {
			throw new \Exception();
		}
		return $bbm;
	}


	/**
	 *  this function will be confirting paging property into file excel
	 *  the main library is base on excel spreadsheet library
	 *  @param array $input
	 *   @return array|static[]
	 */
	public function exportToExcel($input = array())
	{
		return $this->model->getQuery()->get();
	}

	// public function repopulateData($laporan)
	// {
	// 	$this->detailService->repopulateData($laporan);
	// }

	public function repopulateData($laporan)
	{
		$saldo_awal = $this->getPreviousSaldoAkhir($laporan->voyage_from);
		$pemakaian = $this->penggunaanBarang->getSumPenggunaanBarang($laporan->itemId, $laporan->voyage_id, $laporan->voyage_from, $laporan->voyage_to);
		$koreksi = $this->adjustmentBBM->getSumItemVoyage($laporan->itemId, $laporan->voyage_id, $laporan->voyage_from, $laporan->voyage_to);
		$totalPenerimaan = $this->repopulateDataItem($laporan->id, $laporan->itemId, $laporan);
		$saldo_akhir = $saldo_awal + $totalPenerimaan + $koreksi - $pemakaian;

		$input = [
			"saldo_awal" 	=> $saldo_awal,
			"pemakaian" 	=> $pemakaian,
			"penerimaan" 	=> $totalPenerimaan,
			"koreksi" 		=> $koreksi,
			"saldo_akhir" 	=> $saldo_akhir
		];

		$model = LaporanbbmModel::find($laporan->id);
		$model->fill($input);
		if (!$model->update()) {
			throw new \Exception();
		}
		$reportID = $model->id;
	}

	public function repopulateDataMonth($laporan)
	{
		$saldo_awal = $this->getPreviousSaldoAkhirBulan($laporan->tahun, $laporan->bulan);
		$pemakaian = $this->penggunaanBarang->getSumPenggunaanBarangBulan($laporan->itemId, $laporan->tahun, $laporan->bulan);
		$koreksi = $this->adjustmentBBM->getSumItemBulan($laporan->itemId, $laporan->tahun, $laporan->bulan);
		$totalPenerimaan = $this->repopulateDataItemBulan($laporan->itemId, $laporan);
		$saldo_akhir = $saldo_awal + $totalPenerimaan + $koreksi - $pemakaian;

		$input = [
			"saldo_awal" 	=> $saldo_awal,
			"pemakaian" 	=> $pemakaian,
			"penerimaan" 	=> $totalPenerimaan,
			"koreksi" 		=> $koreksi,
			"saldo_akhir" 	=> $saldo_akhir
		];

		$model = LaporanbbmModel::find($laporan->id);
		$model->fill($input);
		if (!$model->update()) {
			throw new \Exception();
		}
		$reportID = $model->id;
	}

	public function getPreviousSaldoAkhir($voyageFrom)
	{
		$model = (new LaporanbbmModel())->getQuery()
			->where("laporanbbm.voyage_to", "<=", date("Y-m-d", strtotime($voyageFrom)))
			->where("laporanbbm.tipe", "=", "VOYAGE")
			->orderBy("laporanbbm.voyage_to", "DESC")
			->orderBy("laporanbbm.voyage_from", "DESC")
			->first();
		if ($model) {
			return $model->saldo_akhir;
		} else {
			return 0;
		}
	}

	public function getPreviousSaldoAkhirBulan($tahun, $bulan)
	{
		if ($bulan > 12) {
			$bulan = 12;
		}
		if ($bulan <= 1) {
			$tahun--;
			$bulan = 12;
		} else {
			$bulan--;
		}
		$model = (new LaporanbbmModel())->getQuery()
			->where("laporanbbm.tahun", "=", $tahun)
			->where("laporanbbm.bulan", "=", $bulan)
			->where("laporanbbm.tipe", "=", "MONTH")
			->first();
		if ($model) {
			return $model->saldo_akhir;
		} else {
			return 0;
		}
	}

	public function repopulateDataItem($reportID, $itemID, $laporan)
	{
		// DETAIL ID-> REPORTID + ITEM ID
		$totalPenerimaan = 0;

		// select penerimaan BASED ON transaction date & itemID
		$penerimaans = $this->penerimaanBarang->getItemPenerimaanBasedOnVoyage($itemID, $laporan->voyage_id, $laporan->voyage_from, $laporan->voyage_to);

		foreach ($penerimaans as $penerimaan) {
			// cek if available @ itemdetail

			$itemDetails = (new LaporanbbmdetailsModel())->getQuery()
				->where("laporanbbmdetails.reportId", "=", $reportID)
				->where("laporanbbmdetails.transactionNo", "=", $penerimaan->receiptNumber)
				->first();

			$totalPenerimaan += $penerimaan->quantity;

			if ($itemDetails) {
				$input = [
					"transactionDate" 	=> $penerimaan->tanggalPenerimaan,
					"transactionNo" 	=> $penerimaan->receiptNumber,
					"qty" 				=> $penerimaan->quantity,
					"itemId" 			=> $penerimaan->itemId,
					"tahun" 			=> date("Y", strtotime($penerimaan->tanggalPenerimaan)),
					"bulan"				=> date("n", strtotime($penerimaan->tanggalPenerimaan))
				];

				$model = LaporanbbmdetailsModel::find($itemDetails->id);
				$model->fill($input);
				if (!$model->update()) {
					throw new \Exception();
				}
			} else {
				$input = [
					"reportId" 			=> $reportID,
					"transactionDate" 	=> $penerimaan->tanggalPenerimaan,
					"transactionNo" 	=> $penerimaan->receiptNumber,
					"qty" 				=> $penerimaan->quantity,
					"itemId" 			=> $penerimaan->itemId,
					"tahun" 			=> date("Y", strtotime($penerimaan->tanggalPenerimaan)),
					"bulan"				=> date("n", strtotime($penerimaan->tanggalPenerimaan))
				];

				$model = new LaporanbbmdetailsModel();
				$model->fill($input);
				$model->save();
			}
		}
		return $totalPenerimaan;
	}

	public function repopulateDataItemBulan($itemID, $laporan)
	{
		// DETAIL ID-> REPORTID + ITEM ID
		$totalPenerimaan = 0;

		$itemDetails = (new LaporanbbmdetailsModel())->getQuery()
			->where("laporanbbmdetails.itemId", "=", $itemID)
			->where("laporanbbmdetails.tahun", "=", $laporan->tahun)
			->where("laporanbbmdetails.bulan", "=", $laporan->bulan)
			->get();

		foreach ($itemDetails as $itemDetail) {
			$totalPenerimaan += $itemDetail->qty;
		}
		return $totalPenerimaan;
	}

	public function getDataReport($tahun, $tipe = "MONTH")
	{

		if ($tipe == "VOYAGE") {
			$laporans = (new LaporanbbmModel())->joinRelationBbmDetails()->getQuery()
				->select(["laporanbbm.*", "laporanbbmdetails.qty", "laporanbbmdetails.transactionDate", "laporanbbmdetails.pelabuhan"])
				->where("laporanbbm.tahun", "=", $tahun)
				->where("laporanbbm.tipe", "=", $tipe)
				->orderBy("laporanbbm.voyage_from", "ASC")
				->orderBy("laporanbbm.voyage_to", "ASC")
				->orderBy("laporanbbmdetails.transactionDate", "ASC")
				->get();
		} else {
			$laporans = (new LaporanbbmModel())->joinRelationBbmMonthDetails()->getQuery()
				->select(["laporanbbm.*", "laporanbbmdetails.qty", "laporanbbmdetails.transactionDate", "laporanbbmdetails.pelabuhan"])
				->where("laporanbbm.tahun", "=", $tahun)
				->where("laporanbbm.tipe", "=", $tipe)
				->orderBy("laporanbbm.voyage_from", "ASC")
				->orderBy("laporanbbm.voyage_to", "ASC")
				->orderBy("laporanbbmdetails.transactionDate", "ASC")
				->get();
		}
		return $laporans;
	}
}
