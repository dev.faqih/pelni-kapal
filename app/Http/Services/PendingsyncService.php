<?php

/**
 * This project has been generated by worker generator
 * has been made by fahmi sulaiman
 * sulaimanfahmi@gmail.com
 * check out my github @dekaulitz
 */

namespace App\Http\Services;

use App\Http\Models\SynclogModel;
use App\Http\Base\BaseService;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Http\Models\StgpelniiotintModel;
use App\Http\Models\StgpelniprintModel;
use App\Http\Models\StgpelniscmmiscrcvintModel;


class PendingsyncService extends BaseService
{
    /**
     * ItemService constructor.
     */
    public function __construct()
    {
        parent::__construct();
		$this->model = new SynclogModel();
    }

    /**
     *  index function that get all data from database by default
     * @return array|static[]
     */
    public function index()
    {
        return $this->model->getQuery()->get();
    }


    /**
     *  this function is magic pagination injector from my library you want to add new property or field you have to inject into model in alias paging property
     * @param array $input
     * @return array
     */

	public function pagingHO1(Request $request) {
			

			$stgpelniprint = (new StgpelniprintModel())->getQuery()
			// ->where("flagsync", "!=", 1)
					->select([
							"stgpelniprint.req_number_segment1 AS transNo",
							"stgpelniprint.item_segment1 AS item",
							"stgpelniprint.quantity AS qty"
					])
					->get();

			/* $stgpelniscmmiscrcvint = (new StgpelniscmmiscrcvintModel())->getQuery()
				// 	->where("flagsync", "!=", 1)
					->select([
							"stgpelniscmmiscrcvint.transaction_number AS transNo",
							"stgpelniscmmiscrcvint.item_segment1 AS item",
							"stgpelniscmmiscrcvint.transaction_quantity AS qty"
					])
					->union($stgpelniprint)
					->get(); */
							
					
			$stgpelniiotint =new StgpelniiotintModel();
			$stgpelniiotint->getQuery()		
					// ->where("flagsync", "!=", 1)
					->select([
							"stgpelniiotint.transaction_number AS transNo",
							"stgpelniiotint.item_segment1 AS item",
							"stgpelniiotint.transaction_quantity AS qty"
					])
					->unionAll($stgpelniscmmiscrcvint)
					->get();

			$stgpelniiotint->unionAll($stgpelniprint);
			$stgpelniiotint->unionAll($stgpelniscmmiscrcvint);
			var_dump($stgpelniiotint->getQuery()->toSql()); dd();
			
			$stgpelniiotint->paging(); //->filter()->sorting();
			$model = $stgpelniiotint->paging()->getPaging(); 

			return $model;
	}

	public function pagingHO($request) {
		$input = $request->all();
		$size = [10, 25, 50, 100];

		$page = isset($input["page"]) && intval($input["page"]) > 0 ? intval($input["page"]) : 1; 
		$size = isset($input["size"]) && intval($input["size"]) > 0 && in_array($input["size"], $size) ? intval($input["size"]) : 10; 
		$offset = (($page-1) * $size);

		$tableName = $keyword = '';
		if(isset($input["q"]) && ! empty($input["q"])) {
			$arrKeys = explode("||", $input["q"]);
			foreach($arrKeys as $key) {
				$arrKey = explode(":", $key);
				if(isset($arrKey[0]) && $arrKey[1] && $arrKey[0] == "table_name" && ! empty($arrKey[0])) {
					$tableName = $arrKey[1];
				}
				if(isset($arrKey[0]) && $arrKey[1] && $arrKey[0] == "keyword" && ! empty($arrKey[0])) {
					$keyword = $arrKey[1];
				}
			}
		}
		
		$sqlCount = $sqlRaw = '';
		if(empty($tableName) || $tableName == "stgpelniiotint") {
			$sqlRaw .= ' SELECT \'Transfer/Penerimaan IO\' AS "moduleName", "stgpelniiotint"."transaction_number" AS "transNo", 
							"stgpelniiotint"."item_segment1" AS "item", "stgpelniiotint"."transaction_quantity" AS "qty"
						FROM "stg_pelni_iot_int" AS "stgpelniiotint" 
						WHERE flagsync IS NULL '; 
			$sqlCount .= ' SELECT COUNT("id") AS "nRows" FROM "stg_pelni_iot_int" WHERE flagsync IS NULL ';
		}
		
		if(empty($tableName) || $tableName == "stgpelniprint") {
			if(! empty($sqlRaw)) { $sqlRaw .= ' UNION ALL '; $sqlCount .= ' UNION ALL '; }
			$sqlRaw .= ' SELECT \'Permintaan Barang\' AS "moduleName", "stgpelniprint"."req_number_segment1" AS "transNo", 
							"stgpelniprint"."item_segment1" AS "item", "stgpelniprint"."quantity" AS "qty"
						FROM "stg_pelni_pr_int" AS "stgpelniprint" 
						WHERE flagsync IS NULL ';
			$sqlCount .= ' SELECT COUNT("id") AS "nRows" FROM "stg_pelni_pr_int" WHERE flagsync IS NULL ';
		}
		
		if(empty($tableName) || $tableName == "stgpelniscmmiscrcvint") {
			if(! empty($sqlRaw)) { $sqlRaw .= ' UNION ALL '; $sqlCount .= ' UNION ALL '; }
			$sqlRaw .= ' SELECT \'Penerimaan/Penggunaan Makanan\' AS "moduleName", "stgpelniscmmiscrcvint"."transaction_number" AS "transNo", 
							"stgpelniscmmiscrcvint"."item_segment1" AS "item", "stgpelniscmmiscrcvint"."transaction_quantity" AS "qty"
						FROM "stg_pelni_scm_misc_rcv_int" AS "stgpelniscmmiscrcvint" 
						WHERE flagsync IS NULL ';		
			$sqlCount .= ' SELECT COUNT("id") AS "nRows" FROM "stg_pelni_scm_misc_rcv_int" WHERE flagsync IS NULL ';	
		}		
		
		if(empty($tableName) || $tableName == "stgpelniscmmoiint") {
			if(! empty($sqlRaw)) { $sqlRaw .= ' UNION ALL '; $sqlCount .= ' UNION ALL '; }
			$sqlRaw .= ' SELECT \'Penggunaan Barang\' AS "moduleName", "stgpelniscmmoiint"."transaction_number" AS "transNo", 
							"stgpelniscmmoiint"."item_segment1" AS "item", "stgpelniscmmoiint"."transaction_quantity" AS "qty"
						FROM "stg_pelni_scm_moi_int" AS "stgpelniscmmoiint" 
						WHERE flagsync IS NULL ';
			$sqlCount .= ' SELECT COUNT("id") AS "nRows" FROM "stg_pelni_scm_moi_int" WHERE flagsync IS NULL ';
		}
		
		if(empty($tableName) || $tableName == "stgpelniscmmotint") {
			if(! empty($sqlRaw)) { $sqlRaw .= ' UNION ALL '; $sqlCount .= ' UNION ALL '; }
			$sqlRaw .= ' SELECT \'Pindah Barang Subinventory\' AS "moduleName", "stgpelniscmmotint"."transaction_number" AS "transNo", 
							"stgpelniscmmotint"."item_segment1" AS "item", "stgpelniscmmotint"."transaction_quantity" AS "qty"
						FROM "stg_pelni_scm_mot_int" AS "stgpelniscmmotint" 
						WHERE flagsync IS NULL ';
			$sqlCount .= ' SELECT COUNT("id") AS "nRows" FROM "stg_pelni_scm_mot_int" WHERE flagsync IS NULL ';
		}
		
		if(empty($tableName) || $tableName == "stgpelniscmrcvint") {
			if(! empty($sqlRaw)) { $sqlRaw .= ' UNION ALL '; $sqlCount .= ' UNION ALL '; }
			$sqlRaw .= ' SELECT \'Penerimaan Barang\' AS "moduleName", "stgpelniscmrcvint"."transaction_number" AS "transNo", 
							"stgpelniscmrcvint"."attribute1" AS "item", \'0\' AS "qty"
						FROM "stg_pelni_scm_rcv_int" AS "stgpelniscmrcvint" 
						WHERE flagsync IS NULL ';
			$sqlCount .= ' SELECT COUNT("id") AS "nRows" FROM "stg_pelni_scm_rcv_int" WHERE flagsync IS NULL ';
		}
		
		if(empty($tableName) || $tableName == "stgpelniscmrcvint") {
			if(! empty($sqlRaw)) { $sqlRaw .= ' UNION ALL '; $sqlCount .= ' UNION ALL '; }
			$sqlRaw .= ' SELECT \'Penerimaan Barang (Item)\' AS "moduleName", "stgpelniscmrcvtrxint"."transaction_number" AS "transNo", 
							"stgpelniscmrcvtrxint"."item_segment1" AS "item", "stgpelniscmrcvtrxint"."quantity" AS "qty"
						FROM "stg_pelni_scm_rcv_trx_int" AS "stgpelniscmrcvtrxint" 
						WHERE flagsync IS NULL ';
			$sqlCount .= ' SELECT COUNT("id") AS "nRows" FROM "stg_pelni_scm_rcv_trx_int" WHERE flagsync IS NULL ';
		}
		$sqlRaw .= ' LIMIT ' . $size . ' OFFSET ' . $offset;

		$sql = DB::select($sqlRaw);		
		$sqlCount = DB::select($sqlCount);

		$nRows = 0;
		foreach($sqlCount as $row) {
			$nRows += $row->nRows;
		}
		$pageCount = ceil($nRows / $size);

		$model = [	"searchBy" => "",
					"searchValue" => "",
					"rows" => $sql,
					"size" => $size,
					"page" => $page,
					'rowCount' => $nRows,
					'pageCount' => $pageCount 
				];
		// var_dump($sqlRaw); dd();
		return $model;
	}

    public function pagingKapal(Request $request)
    {        
        $input = $request->all();

        $this->model->getQuery()->select(["synclog.*"])
                    ->where("need_sync", "=", 1);
		if(! isset($input["sort"])) {
			$this->model->getQuery()->orderBy("synclog.updated_at", "desc");
		}

        $this->model->paging()->filter()->sorting();
        $model = $this->model->paging()->getPaging();

        $newRow = [];
        foreach($model["rows"] as $idx => $row) {
            $data = "";
            $rsData = DB::select("SELECT * FROM " . $row->table_name . " WHERE id = ?;", [$row->local_id]);
            if($rsData && ! empty($rsData)) {
                switch($row->table_name) {
                    case "itempenerimaanbarang" : 
                            $rsItem = DB::select("SELECT * FROM item WHERE id = ?;", [$rsData[0]->itemId]);
                            $data = "Item Penerimaan Barang. #" . $rsData[0]->penerimaanBarangId . ". Lot No: " . $rsData[0]->lotNumber . ". &nbsp; Qty: " . $rsData[0]->quantity;
                            if($rsItem && ! empty($rsItem)) {
                                $data .= " " . $rsItem[0]->UOM . ". &nbsp; Kode Item: " . $rsItem[0]->namaItem . " &nbsp;-&nbsp; "  . $rsItem[0]->deskripsi;
                            }
                            break;
                    case "itempenerimaanbarangio" : 
                            $rsItem = DB::select("SELECT * FROM item WHERE id = ?;", [$rsData[0]->itemId]);
                            $data = "Item Penerimaan IO Barang. #" . $rsData[0]->rcNumber . ". Lot No: " . $rsData[0]->lotNumber . ". &nbsp; Qty: " . $rsData[0]->quantity;
                            if($rsItem && ! empty($rsItem)) {
                                $data .= " " . $rsItem[0]->UOM . ". &nbsp; Kode Item: " . $rsItem[0]->namaItem . " &nbsp;-&nbsp; "  . $rsItem[0]->deskripsi;
                            }
                            break;
                    case "itempenerimaanmakanan" : 
                            $rsItem = DB::select("SELECT * FROM item WHERE id = ?;", [$rsData[0]->itemId]);
                            $data = "Item Penerimaan Makanan. #" . $rsData[0]->mrNumber . ". &nbsp; Qty: " . $rsData[0]->quantity;
                            if($rsItem && ! empty($rsItem)) {
                                $data .= " " . $rsItem[0]->UOM . ". &nbsp; Kode Item: " . $rsItem[0]->namaItem . " &nbsp;-&nbsp; "  . $rsItem[0]->deskripsi;
                            }
                            break;

                    case "itempenggunaanbarang" : 
                            $rsItem = DB::select("SELECT * FROM item WHERE id = ?;", [$rsData[0]->itemId]);
                            $data = "Item Penggunaan Barang. #" . $rsData[0]->miNumber . ". Lot No: " . $rsData[0]->lotNumber . ". &nbsp; Qty: " . $rsData[0]->quantity;
                            if($rsItem && ! empty($rsItem)) {
                                $data .= " " . $rsItem[0]->UOM . ". &nbsp; Kode Item: " . $rsItem[0]->namaItem . " &nbsp;-&nbsp; "  . $rsItem[0]->deskripsi;
                            }
                            break;

                    case "itempenggunaanmakanan" : 
                            $rsItem = DB::select("SELECT * FROM item WHERE id = ?;", [$rsData[0]->itemId]);
                            $data = "Item Penggunaan Makanan. #" . $rsData[0]->msNumber . ". &nbsp; Qty: " . $rsData[0]->quantity;
                            if($rsItem && ! empty($rsItem)) {
                                $data .= " " . $rsItem[0]->UOM . ". &nbsp; Kode Item: " . $rsItem[0]->namaItem . " &nbsp;-&nbsp; "  . $rsItem[0]->deskripsi;
                            }
                            break;

                    case "itempermintaanbarang1" : 
                            $rsItem = DB::select("SELECT * FROM item WHERE id = ?;", [$rsData[0]->itemId]);
                            $data = "Item Permintaan Barang. #" . $rsData[0]->prNumber . ". &nbsp; Qty: " . $rsData[0]->quantity;
                            if($rsItem && ! empty($rsItem)) {
                                $data .= " " . $rsItem[0]->UOM . ". &nbsp; Kode Item: " . $rsItem[0]->namaItem . " &nbsp;-&nbsp; "  . $rsItem[0]->deskripsi;
                            }
                            break;

                    case "itempindahbarangio" : 
                            $rsItem = DB::select("SELECT * FROM item WHERE id = ?;", [$rsData[0]->itemId]);
                            $data = "Item Transfer IO Barang. #" . $rsData[0]->ioNumber . ". Lot No: " . $rsData[0]->lotNumber . ". &nbsp; Qty: " . $rsData[0]->quantity;
                            if($rsItem && ! empty($rsItem)) {
                                $data .= " " . $rsItem[0]->UOM . ". &nbsp; Kode Item: " . $rsItem[0]->namaItem . " &nbsp;-&nbsp; "  . $rsItem[0]->deskripsi;
                            }
                            break;

                    case "itempindahbarangsubinv" : 
                            $rsItem = DB::select("SELECT * FROM item WHERE id = ?;", [$rsData[0]->itemId]);
                            $data = "Item Pindah SubInventory. #" . $rsData[0]->moNumber . ". Lot No: " . $rsData[0]->lotNumber . ". &nbsp; Qty: " . $rsData[0]->quantity;
                            if($rsItem && ! empty($rsItem)) {
                                $data .= " " . $rsItem[0]->UOM . ". &nbsp; Kode Item: " . $rsItem[0]->namaItem . " &nbsp;-&nbsp; "  . $rsItem[0]->deskripsi;
                            }
                            break;

                    case "penerimaanbarang" : 
                            $data = "Penerimaan Barang. #" . $rsData[0]->receiptNumber . " dari PO No: #" . $rsData[0]->poNumber . ". Qty: " . $rsData[0]->qtyDiterima;
                            break;
                    
                    case "penerimaanbarangio" : 
                            $data = "Penerimaan Barang IO. #" . $rsData[0]->rcNumber . " dari PO No: #" . $rsData[0]->ioNumber . ". Qty: " . $rsData[0]->qtyDiterima;
                            break;

                    case "penerimaanmakanan" : 
                            $data = "Penerimaan Makanan. #" . $rsData[0]->mrNumber;
                            break;

                    case "penggunaanbarang" : 
                            $data = "Penggunaan Barang. #" . $rsData[0]->miNumber;
                            break;

                    case "penggunaanmakanan" : 
                            $data = "Penggunaan Makanan. #" . $rsData[0]->msNumber;
                            break;

                    case "permintaanbarang" : 
                            $data = "Permintaan Barang. #" . $rsData[0]->prNumber;
                            break;

                    case "pindahbarangio" : 
                            $data = "Transfer IO. #" . $rsData[0]->ioNumber;
                            break;

                    case "pindahbarangsubinv" : 
                            $data = "Pndah SubInventory IO. #" . $rsData[0]->moNumber;
                            break;

                    case "popenerimaanio" : 
                            $data = "Penerimaan IO. #" . $rsData[0]->ioNumber;
                            break;

                    case "popenerimaan" : 
                            $data = "PO Penerimaan Barang. #" . $rsData[0]->poNumber;
                            break;
                
                    case "stg_pelni_iot_int" : 
                            $data = "Staging Transfer/Penerimaan IO [" . $rsData[0]->inout . "] #" . $rsData[0]->transaction_number . " " . 
                                        $rsData[0]->item_segment1 . " &nbsp; Qty.: " . $rsData[0]->transaction_quantity . " "  . $rsData[0]->transaction_uom ;
							break;
					
                case "stg_pelni_pr_int" : 
                            $data = "Staging Permintaan Barang #" . $rsData[0]->req_number_segment1 . " " . 
                                        $rsData[0]->item_segment1 . " &nbsp; Qty.: " . $rsData[0]->quantity ;
							break;
							
                case "stg_pelni_scm_misc_rcv_int" : 
                            $data = "Staging Penerimaan/Penggunaan Makanan [" . $rsData[0]->type_misc . "] #" . $rsData[0]->transaction_number . " " . 
                                        $rsData[0]->item_segment1 . " &nbsp; Qty.: " . $rsData[0]->transaction_quantity . " "  . $rsData[0]->transaction_uom ;
							break;

                case "stg_pelni_scm_moi_int" : 
                            $data = "Staging Penggunaan Barang #" . $rsData[0]->transaction_number . " " . 
                                        $rsData[0]->item_segment1 . " &nbsp; Qty.: " . $rsData[0]->transaction_quantity ;
							break;

                case "stg_pelni_scm_mot_int" : 
                            $data = "Staging Pindah Barang Subinventory #" . $rsData[0]->transaction_number . " " . 
                                        $rsData[0]->item_segment1 . " &nbsp; Qty.: " . $rsData[0]->transaction_quantity . " "  . $rsData[0]->transaction_uom ;
							break;

                case "stg_pelni_scm_rcv_int" : 
                            $data = "Staging Penerimaan Barang #" . $rsData[0]->transaction_number . " dari PO No #" . 
                                        $rsData[0]->attribute1;
							break;

					case "stg_pelni_scm_rcv_trx_int" : 
                $data = "Staging Item Penerimaan Barang #" . $rsData[0]->transaction_number . " " . 
                                        $rsData[0]->item_segment1 . " &nbsp; Qty.: " . $rsData[0]->quantity;
							break;

					case "workflow" : 
                            $data = "Workflow: " . $rsData[0]->namaTransaksi . " #" . $rsData[0]->refId;
							break;
                            
                }
            }
            
            $model["rows"][$idx]->data = $data;        
		}
		return $model;
    }

    public function getTableName() {
        return $this->model->getQuery()->distinct()->select(["synclog.table_name"])
                    ->where("need_sync", "=", 1)
                    ->orderBy("synclog.table_name", "ASC")
                    ->get();
    }
}
