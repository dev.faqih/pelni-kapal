<?php

/**
 This project has been generated by worker generator
 has been made by fahmi sulaiman
 sulaimanfahmi@gmail.com
 check out my github @dekaulitz
 */
namespace App\Http\Validators;

use App\Http\Base\Validation;

class RolesValidator extends Validation
{
    /**
    * this function will call before insert, you can custom as your business requirement
    * @param array $input
    * @return $this
    */
    public function beforeInsert($input){
		if(empty($input["roleName"])){
			array_push($this->error, ["roleName" => "cannot be null"]);
		}
		else{
			$lengthrolename = strlen($input["roleName"]);
			if($lengthrolename>100){
			array_push($this->error, ["rolename" => "value too long"]);
			}
        }
        if(!empty($input["roleName"]))
        {
            $roleName = (new \App\Http\Models\RolesModel())->getQuery()
            ->where("roles.roleName","=",$input["roleName"])->first();
            if($roleName) {
                array_push($this->error, ["roleName" => "Value Already Exist"]);
            }
        }

        return $this;
    }

    /**
    * this function will call before update into database, you can custom as your business requirement
    * @param array $input
    * @param $id
    * @return $this
    */
    public function beforeUpdate($input,$id){
		if(empty($input["roleName"])){
			array_push($this->error, ["roleName" => "cannot be null"]);
		}
		else{
			$lengthrolename = strlen($input["roleName"]);
			if($lengthrolename>100){
			array_push($this->error, ["rolename" => "value too long"]);
			}
		}

        return $this;
    }

    /**
    * this function will call before delete from database, you can custom as your business requirement
    * @param $id
    * @return $this
    */
    public function beforeDelete($id){
        $users = (new \App\Http\Models\UsersModel())->getQuery()->whereIn("roleId", explode(",", $id))->first();
		if(empty($users)){			
        } else {
            array_push($this->error, ["Role" => "This role is used by one or more users"]);
        }
        
	    return $this;

    }


}