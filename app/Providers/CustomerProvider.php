<?php

namespace App\Providers;
use Auth;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Auth\CustomerUserProvider;

class CustomerProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
	public function boot(GateContract $gate)
	{
		$this->registerPolicies($gate);

		// Binding eloquent.admin to our EloquentAdminUserProvider
		Auth::provider('eloquent.customer', function($app, array $config) {
			return new CustomerUserProvider($app['hash'], $config['model']);
		});
	}

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
