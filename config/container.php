<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 28/07/2017
 * Time: 11:55
 */
$notFoundHandler = function ($c) {
    return function ($request, $response) use ($c) {
        return $c['response']
            ->withStatus(404)
            ->withHeader('Content-type', 'application/json')
            ->withHeader("Access-Control-Allow-Origin", "*")
            ->write(json_encode([
                "meta" => [
                    "timestamp" => (integer)microtime(true),
                    "status" => json_decode(PAGE_NOT_FOUND),
                    "message" => "PAGE_NOT_FOUND",
                ]
            ]));
    };
};

$UnexpectedValueException = function ($c) {
    return function ($request, $response, $exception) use ($c) {
        return $c['response']
            ->withStatus(422)
            ->withHeader('Content-type', 'application/json')
            ->withHeader("Access-Control-Allow-Origin", "*")
            ->write(json_encode([
                "meta" => [
                    "timestamp" => (integer)microtime(true),
                    "status_code" => 10010,
                    "message" => "berak",
                    "http_code" => 422
                ]
            ]));
    };
};

$errorHandler = function ($c) {
    return function ($request, $response, Exception $exception) use ($c) {
        if ($exception instanceof \Handler\DataValidationException) {
            return $response
                ->withHeader("Access-Control-Allow-Origin", "*")
                ->withJson([
                    "meta" => [
                        "timestamp" => (integer)microtime(true),
                        "errors" => json_decode($exception->getMessage()),
                        "detail" => $exception->getDetailMessage(),
                    ]], 422);
        }
        return $response
            ->withHeader("Access-Control-Allow-Origin", "*")
            ->withJson([
                "meta" => [
                    "timestamp" => (integer)microtime(true),
                    "errors" => INTERNAL_SERVER_ERROR,
                    "detail" => $exception->getMessage(),
                ]], 500);

    };
};

