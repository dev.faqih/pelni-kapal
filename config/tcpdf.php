<?php

return [
	'mode'                  => 'utf-8',
	'format'                => 'A4',
	'author'                => 'Seth Setiadha @ Code Development Indonesia for PT. Pelayaran Nasional Indonesia',
	'subject'               => 'PELNI Report',
	'keywords'              => 'PELNI',
	'creator'               => 'PT. Pelayaran Nasional Indonesia',
	'display_mode'          => 'fullpage',
	'tempDir'               => base_path('../temp/')
];
