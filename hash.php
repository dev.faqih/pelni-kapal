<?php

require_once __DIR__ . '/vendor/autoload.php';

use Predis\Client;

try {
    $redis = new Client();

    $redis->hset('user:123', 'name', 'Eko Yuliarto');
    $redis->hset('user:123', 'email', 'ekoyuliarto23@gmail.com');
    $redis->hset('user:123', 'dob', '2020-07-23');

    print_r($redis->hgetall('user:123'));

    echo $redis->hget('user:123', 'name')."\n";
    echo $redis->hget('user:123', 'email')."\n";
    echo $redis->hget('user:123', 'dob')."\n";

    print_r($redis->hkeys('user:123'));
    print_r($redis->hvals('user:123'));

    echo $redis->hstrlen('user:123', 'name')."\n";
    echo $redis->hlen('user:123')."\n";
    echo $redis->hexists('user:123', 'email')."\n";
    echo $redis->hexists('user:123', 'website')."\n";

    $redis->hdel('user:123', 'dob')."\n";
    echo $redis->hexists('user:123', 'dob')."\n";

    $redis->del('user:123');

}
catch (Exception $e) {
    die ($e->getMessage());
}

?>
