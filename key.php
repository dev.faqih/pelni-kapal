<?php

require_once __DIR__ . '/vendor/autoload.php';

use Predis\Client;

try {
    $redis = new Client();

    $redis->set('user:12345', 'Eko Yuliarto');

    $user = $redis->get('user:12345');

    echo $user."\n";
}
catch (Exception $e) {
    die ($e->getMessage());
}

?>
