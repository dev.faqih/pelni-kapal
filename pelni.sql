-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 08, 2018 at 09:39 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.1.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pelni`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `namaAccount` varchar(200) NOT NULL,
  `deskripsi` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`id`, `namaAccount`, `deskripsi`) VALUES
(1, 'Makanan', 'no desk'),
(2, 'Gudang', 'gudang desk');

-- --------------------------------------------------------

--
-- Table structure for table `activitylog`
--

CREATE TABLE `activitylog` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userId` bigint(20) UNSIGNED NOT NULL,
  `activity` tinyint(2) UNSIGNED NOT NULL,
  `date` datetime NOT NULL,
  `module` tinyint(2) UNSIGNED NOT NULL,
  `refId` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `administratoradministrator`
--

CREATE TABLE `administratoradministrator` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `groupid` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(45) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `img` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administratoradministrator`
--

INSERT INTO `administratoradministrator` (`id`, `groupid`, `username`, `password`, `email`, `fullname`, `img`) VALUES
(1, 1, 'root', '$2y$12$U5BsEAXwbNHZgvSZx.Bqre8aXUgzLzNShrJ.3hXQZMcFSQh/unDP2', 'admin@admin.id', 'Administrator', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `administratorgroupaccess`
--

CREATE TABLE `administratorgroupaccess` (
  `id` int(10) UNSIGNED NOT NULL,
  `groupid` bigint(20) UNSIGNED NOT NULL,
  `accessid` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `administratorgroupaccess`
--

INSERT INTO `administratorgroupaccess` (`id`, `groupid`, `accessid`) VALUES
(7, 2, 2),
(45, 1, 2),
(46, 1, 4),
(47, 1, 6),
(48, 1, 10),
(49, 1, 12),
(50, 1, 14),
(51, 1, 16),
(52, 1, 18),
(53, 1, 20),
(54, 1, 24),
(55, 1, 26),
(56, 1, 28),
(57, 1, 30),
(58, 1, 32),
(59, 1, 34),
(60, 1, 36),
(61, 1, 38),
(62, 1, 40),
(63, 1, 46),
(64, 1, 47),
(65, 1, 48),
(66, 1, 49),
(67, 1, 50),
(68, 1, 51),
(69, 1, 52),
(70, 1, 53),
(71, 1, 54),
(72, 1, 56),
(73, 1, 55),
(74, 1, 58),
(75, 1, 59);

-- --------------------------------------------------------

--
-- Table structure for table `administratormenu`
--

CREATE TABLE `administratormenu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `menuname` varchar(45) NOT NULL,
  `url` varchar(45) NOT NULL,
  `parentmenu` bigint(20) UNSIGNED DEFAULT '0',
  `icon` varchar(45) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `position` tinyint(2) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administratormenu`
--

INSERT INTO `administratormenu` (`id`, `menuname`, `url`, `parentmenu`, `icon`, `description`, `position`, `active`) VALUES
(1, 'Master', '', 0, 'fa fa-industry', NULL, 1, 1),
(2, 'Activitiy Logs', 'administrator/activitylog', 42, 'fa fa-circle-o', NULL, 31, 1),
(4, 'Item Pemakaian Stok', 'administrator/itempemakaianstok', 41, 'fa fa-laptop', NULL, NULL, 0),
(6, 'Item Pindah Stok', 'administrator/itempindahstok', 41, 'fa fa-pie-chart', NULL, NULL, 0),
(10, 'Permintaan Barang', 'administrator/permintaanbarang', 44, 'fa fa-bullhorn', NULL, 51, 1),
(12, 'Application Parameter', 'administrator/applicationparameter', 1, 'fa fa-circle-o', NULL, 14, 1),
(14, 'Item Penerimaan Barang', 'administrator/itempenerimaanbarang', 44, 'fa fa-laptop', NULL, 52, 0),
(16, 'Kapal', 'administrator/kapal', 1, 'fa fa-bookmark-o', NULL, 11, 1),
(18, 'Pemakaian Stok', 'administrator/pemakaianstok', 41, 'fa fa-bookmark-o', NULL, 24, 0),
(20, 'Pindah Stok', 'administrator/pindahstokkapal', 41, 'fa fa-circle-o', NULL, 23, 0),
(22, 'Approver', 'administrator/approver', 21, ' fa-file-text', NULL, NULL, 0),
(24, 'Item Penyesuaian Stok', 'administrator/itempenyesuaianstok', 41, 'fa fa-send', NULL, NULL, 0),
(26, 'Penerimaan Barang', 'administrator/penerimaanbarang', 44, 'fa fa-gear', NULL, 53, 0),
(28, 'Item Permintaan Barang', 'administrator/itempermintaanbarang', 44, 'fa fa-laptop', NULL, 54, 0),
(30, 'Lokasi Item', 'administrator/lokasiitem', 1, 'fa fa-industry', NULL, 13, 1),
(32, 'Penyesuaian Stok', 'administrator/penyesuaianstok', 41, 'fa fa-laptop', NULL, 22, 0),
(34, 'Stok Kapal', 'administrator/stokkapal', 41, 'fa fa-pie-chart', NULL, 21, 1),
(36, 'Login Log', 'administrator/loginlog', 42, 'fa fa-industry', NULL, 32, 1),
(38, 'Roles', 'administrator/roles', 43, 'fa fa-laptop', NULL, 42, 1),
(40, 'Users', 'administrator/users', 43, 'fa fa-gear', NULL, 41, 1),
(41, 'Inventory', '', 0, 'fa fa-gear', NULL, 2, 1),
(42, 'Logs', '', 0, 'fa fa-industry', NULL, 3, 1),
(43, 'Management Access', '', 0, 'fa fa-users', NULL, 4, 1),
(44, 'Transactions', '', 0, 'fa fa-pie-chart', NULL, 5, 1),
(46, 'Item', 'administrator/item', 1, 'fa fa-gears', NULL, 12, 1),
(47, 'Ga Tau', '', 0, 'fa fa-book', NULL, 6, 1),
(48, 'Transfer IO', 'administrator/transferio', 44, 'fa fa-anchor', NULL, 61, 1),
(49, 'Pindah Stok Subinv', 'administrator/pindahsubinv', 44, 'fa fa-balance-scale', NULL, 64, 1),
(50, 'Penggunaan Barang', 'administrator/penggunaanbarang', 44, 'fa fa-automobile', NULL, 66, 1),
(51, 'Penggunaan Makanan', 'administrator/penggunaanmakanan', 44, 'fa fa-birthday-cake', NULL, 69, 1),
(52, 'PO Penerimaan', 'administrator/popenerimaan', 44, 'fa fa-bomb', NULL, 56, 1),
(53, 'Item PO Penerimaan', 'administrator/itempopenerimaan', 44, 'fa fa-gear', NULL, 57, 0),
(54, 'Penerimaan IO', 'administrator/popenerimaanio', 44, 'fa fa-binoculars', NULL, 62, 1),
(55, 'Item PO Penerimaan IO', 'administrator/itempopenerimaanio', 44, 'fa fa-gear', NULL, 57, 0),
(56, 'Penerimaan Barang IO', 'administrator/penerimaanbarangio', 44, 'fa fa-gear', NULL, 53, 0),
(57, 'Item Penerimaan Barang IO', 'administrator/itempenerimaanbarangio', 44, 'fa fa-laptop', NULL, 52, 0),
(58, 'Workflow', 'administrator/workflow', 44, 'fa fa-cab', NULL, 70, 1),
(59, 'Penerimaan Makanan', 'administrator/penerimaanmakanan', 44, 'fa fa-coffee', NULL, 68, 1);

-- --------------------------------------------------------

--
-- Table structure for table `applicationparameter`
--

CREATE TABLE `applicationparameter` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `variableName` varchar(200) NOT NULL,
  `variableValue` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `applicationparameter`
--

INSERT INTO `applicationparameter` (`id`, `variableName`, `variableValue`) VALUES
(1, '234', '2343535');

-- --------------------------------------------------------

--
-- Table structure for table `approver`
--

CREATE TABLE `approver` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `roleId` bigint(20) UNSIGNED NOT NULL,
  `formIdApprover` tinyint(2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `namaItem` varchar(200) NOT NULL,
  `deskripsi` varchar(1000) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `UOM` varchar(100) DEFAULT NULL,
  `harga` int(10) UNSIGNED DEFAULT '0',
  `image` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`id`, `namaItem`, `deskripsi`, `category`, `UOM`, `harga`, `image`) VALUES
(1, 'Buku', 'Barang', 'Barang', '0824', 50, NULL),
(2, 'Laptop', 'Komputar', 'Barang', '987324', 100, NULL),
(3, 'Handphone', 'Samsung Galazy Noted', '123', '3234', 35, NULL),
(4, 'Coffee', 'Minuman', 'F&B', '123', 45, NULL),
(5, 'Kerupuk', 'makanan', 'F&B', '123', 75, NULL),
(6, 'Nasi Goreng 1', 'Makanan', 'F&B', 'piring', 20000, NULL),
(7, 'Nasi Goreng 2', 'Makanan', 'F&B', 'piring', 20000, NULL),
(8, 'Nasi Goreng 3', 'Makanan', 'F&B', 'piring', 20000, NULL),
(9, 'Nasi Goreng 4', 'Makanan', 'F&B', 'piring', 20000, NULL),
(10, 'Nasi Goreng 5', 'Makanan', 'F&B', 'piring', 20000, NULL),
(11, 'Nasi Goreng 6', 'Makanan', 'F&B', 'piring', 20000, NULL),
(12, 'Nasi Goreng 7', 'Makanan', 'F&B', 'piring', 20000, NULL),
(13, 'Nasi Goreng 8', 'Makanan', 'F&B', 'piring', 20000, NULL),
(14, 'Nasi Goreng 9', 'Makanan', 'F&B', 'piring', 20000, NULL),
(15, 'Nasi Goreng 10', 'Makanan', 'F&B', 'piring', 20000, NULL),
(16, 'Nasi Goreng 11', 'Makanan', 'F&B', 'piring', 20000, NULL),
(17, 'Nasi Goreng 12', 'Makanan', 'F&B', 'piring', 20000, NULL),
(18, 'Nasi Goreng 13', 'Makanan', 'F&B', 'piring', 20000, NULL),
(19, 'Nasi Goreng 14', 'Makanan', 'F&B', 'piring', 20000, NULL),
(20, 'Nasi Goreng 15', 'Makanan', 'F&B', 'piring', 20000, NULL),
(21, 'Nasi Goreng 16', 'Makanan', 'F&B', 'piring', 20000, NULL),
(22, 'Nasi Goreng 17', 'Makanan', 'F&B', 'piring', 20000, NULL),
(23, 'Nasi Goreng 18', 'Makanan', 'F&B', 'piring', 20000, NULL),
(24, 'Nasi Goreng 19', 'Makanan', 'F&B', 'piring', 20000, NULL),
(25, 'Nasi Goreng 20', 'Makanan', 'F&B', 'piring', 20000, NULL),
(26, 'Nasi Goreng 21', 'Makanan', 'F&B', 'piring', 20000, NULL),
(27, 'Nasi Goreng 22', 'Makanan', 'F&B', 'piring', 20000, NULL),
(28, 'Nasi Goreng 23', 'Makanan', 'F&B', 'piring', 20000, NULL),
(29, 'Nasi Goreng 24', 'Makanan', 'F&B', 'piring', 20000, NULL),
(30, 'Nasi Goreng 25', 'Makanan', 'F&B', 'piring', 20000, NULL),
(31, 'Nasi Goreng 26', 'Makanan', 'F&B', 'piring', 20000, NULL),
(32, 'Nasi Goreng 27', 'Makanan', 'F&B', 'piring', 20000, NULL),
(33, 'Nasi Goreng 28', 'Makanan', 'F&B', 'piring', 20000, NULL),
(34, 'Nasi Goreng 29', 'Makanan', 'F&B', 'piring', 20000, NULL),
(35, 'Nasi Goreng 30', 'Makanan', 'F&B', 'piring', 20000, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `itempenerimaanbarang`
--

CREATE TABLE `itempenerimaanbarang` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `penerimaanBarangId` varchar(50) NOT NULL,
  `itemId` bigint(20) UNSIGNED NOT NULL,
  `receiptDate` datetime NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `location` bigint(20) UNSIGNED NOT NULL,
  `lotNumber` varchar(100) DEFAULT NULL,
  `status` tinyint(2) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `itempenerimaanbarang`
--

INSERT INTO `itempenerimaanbarang` (`id`, `penerimaanBarangId`, `itemId`, `receiptDate`, `quantity`, `location`, `lotNumber`, `status`) VALUES
(1, 'PELNI/PB 18 UMS 000001', 2, '2018-07-26 00:00:00', 21, 1, '123', 0),
(2, 'PELNI/PB 18 UMS 000002', 2, '2018-07-26 00:00:00', 234, 1, '234', 0),
(3, 'PELNI/PB 18 UMS 000003', 2, '2018-07-28 00:00:00', 32, 2, '32', 0),
(6, 'PELNI/PB 18 UMS 000004', 2, '2018-08-01 00:00:00', 23, 3, '23', 0),
(14, 'PELNI/PB 18 UMS 000005', 2, '2018-08-01 00:00:00', 2, 2, '2', 0),
(30, 'PELNI/PB 18 UMS 000006', 2, '2018-08-01 00:00:00', 2, 2, '2', 0),
(40, 'PELNI/PB 18 UMS 000007', 2, '2018-08-01 00:00:00', 2, 2, '2', 0),
(41, 'PELNI/PB 18 UMS 000008', 2, '2018-08-01 00:00:00', 5, 3, '2', 0),
(42, 'PELNI/PB 18 UMS 000009', 2, '2018-08-04 00:00:00', 23, 1, '23', 0);

-- --------------------------------------------------------

--
-- Table structure for table `itempenerimaanbarangio`
--

CREATE TABLE `itempenerimaanbarangio` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `rcNumber` varchar(50) NOT NULL,
  `itemId` bigint(20) UNSIGNED NOT NULL,
  `receiptDate` datetime NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `lokasiId` bigint(20) UNSIGNED NOT NULL,
  `lotNumber` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `itempenerimaanbarangio`
--

INSERT INTO `itempenerimaanbarangio` (`id`, `rcNumber`, `itemId`, `receiptDate`, `quantity`, `lokasiId`, `lotNumber`) VALUES
(1, 'PELNI/RC 18 UMS 000001', 2, '2018-07-30 00:00:00', 12, 3, '12'),
(3, 'PELNI/RC 18 UMS 000002', 2, '2018-08-01 00:00:00', 2, 3, '2'),
(4, 'PELNI/RC 18 UMS 000003', 1, '2018-08-01 00:00:00', 23, 2, '3');

-- --------------------------------------------------------

--
-- Table structure for table `itempenerimaanmakanan`
--

CREATE TABLE `itempenerimaanmakanan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `mrNumber` varchar(50) NOT NULL,
  `itemId` bigint(20) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `lotNumber` varchar(100) NOT NULL,
  `locationId` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `itempenerimaanmakanan`
--

INSERT INTO `itempenerimaanmakanan` (`id`, `mrNumber`, `itemId`, `quantity`, `lotNumber`, `locationId`) VALUES
(1, 'PELNI/MS 18 UMS 000001', 3, 32, '23', 3),
(2, 'PELNI/MS 18 UMS 000001', 4, 34, '34', 3),
(3, 'PELNI/MS 18 UMS 000002', 12, 10, '23', 3),
(4, 'PELNI/MS 18 UMS 000002', 25, 5, '12', 3);

-- --------------------------------------------------------

--
-- Table structure for table `itempenggunaanbarang`
--

CREATE TABLE `itempenggunaanbarang` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `miNumber` varchar(50) NOT NULL,
  `itemId` bigint(20) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `subInventory` varchar(200) NOT NULL,
  `lotNumber` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `itempenggunaanbarang`
--

INSERT INTO `itempenggunaanbarang` (`id`, `miNumber`, `itemId`, `quantity`, `subInventory`, `lotNumber`) VALUES
(1, '1', 4, 21, '2', '123'),
(2, '1', 4, 34, '3', '234');

-- --------------------------------------------------------

--
-- Table structure for table `itempenggunaanmakanan`
--

CREATE TABLE `itempenggunaanmakanan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `msNumber` varchar(50) NOT NULL,
  `itemId` bigint(20) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `lotNumber` varchar(100) NOT NULL,
  `locationId` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `itempenggunaanmakanan`
--

INSERT INTO `itempenggunaanmakanan` (`id`, `msNumber`, `itemId`, `quantity`, `lotNumber`, `locationId`) VALUES
(1, '1', 4, 23, '234', 0),
(2, '1', 5, 34, '234', 0),
(3, 'PELNI/MS 18 UMS 000001', 2, 23, '2018-08-21', 3);

-- --------------------------------------------------------

--
-- Table structure for table `itempermintaanbarang`
--

CREATE TABLE `itempermintaanbarang` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `prNumber` varchar(50) NOT NULL,
  `itemId` bigint(20) NOT NULL,
  `needBy` datetime NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `itempermintaanbarang`
--

INSERT INTO `itempermintaanbarang` (`id`, `prNumber`, `itemId`, `needBy`, `quantity`) VALUES
(1, 'PELNI/PR 18 UMS 000003', 1, '2018-07-11 00:00:00', 12),
(2, 'PELNI/PR 18 UMS 000003', 2, '2018-07-17 00:00:00', 12),
(3, 'PELNI/PR 18 UMS 000005', 2, '2018-07-17 00:00:00', 123),
(4, 'PELNI/PR 18 UMS 000005', 1, '2018-07-17 00:00:00', 12),
(5, 'PELNI/PR 18 UMS 000006', 2, '0000-00-00 00:00:00', 100),
(6, 'PELNI/PR 18 UMS 000007', 1, '0000-00-00 00:00:00', 10),
(7, 'PELNI/PR 18 UMS 000007', 3, '0000-00-00 00:00:00', 5),
(8, 'PELNI/PR 18 UMS 000009', 2, '0000-00-00 00:00:00', 1232),
(9, 'PELNI/PR 18 UMS 000010', 2, '0000-00-00 00:00:00', 23),
(10, 'PELNI/PR 18 UMS 000010', 1, '0000-00-00 00:00:00', 23);

-- --------------------------------------------------------

--
-- Table structure for table `itempindahbarangio`
--

CREATE TABLE `itempindahbarangio` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ioNumber` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `itemId` bigint(20) UNSIGNED NOT NULL,
  `lokasiId` bigint(20) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `lotNumber` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `itempindahbarangio`
--

INSERT INTO `itempindahbarangio` (`id`, `ioNumber`, `itemId`, `lokasiId`, `quantity`, `lotNumber`) VALUES
(1, '0', 1, 2, 12, '122'),
(2, '0', 3, 2, 34, '3'),
(3, '0', 2, 2, 2, '2'),
(4, '0', 1, 3, 12, '2'),
(5, '0', 2, 1, 20, '45'),
(6, 'PELNI/IO 18 UMS 000001', 1, 3, 12, '12'),
(7, 'PELNI/IO 18 UMS 000001', 3, 4, 90, '12'),
(8, 'PELNI/IO 18 UMS 000002', 2, 3, 23, '23'),
(9, 'PELNI/IO 18 UMS 000002', 4, 4, 23, '23');

-- --------------------------------------------------------

--
-- Table structure for table `itempindahbarangsubinv`
--

CREATE TABLE `itempindahbarangsubinv` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `moNumber` varchar(50) NOT NULL,
  `itemId` bigint(20) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `lotNumber` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `itempindahbarangsubinv`
--

INSERT INTO `itempindahbarangsubinv` (`id`, `moNumber`, `itemId`, `quantity`, `lotNumber`) VALUES
(3, 'PELNI/MO 18 UMS 000002', 1, 12, '21'),
(4, 'PELNI/MO 18 UMS 000002', 3, 23, '23');

-- --------------------------------------------------------

--
-- Table structure for table `itempopenerimaan`
--

CREATE TABLE `itempopenerimaan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `poNumber` varchar(50) NOT NULL,
  `itemId` bigint(20) UNSIGNED NOT NULL,
  `lokasiId` bigint(20) UNSIGNED NOT NULL,
  `receiptDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `quantity` varchar(100) NOT NULL,
  `lotNumber` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `itempopenerimaan`
--

INSERT INTO `itempopenerimaan` (`id`, `poNumber`, `itemId`, `lokasiId`, `receiptDate`, `dateCreated`, `quantity`, `lotNumber`) VALUES
(1, 'PELNI/PB 18 UMS 000001', 2, 2, '2018-07-04 00:00:00', '2018-07-18 00:00:00', '12', '12');

-- --------------------------------------------------------

--
-- Table structure for table `itempopenerimaanio`
--

CREATE TABLE `itempopenerimaanio` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ioNumber` varchar(50) NOT NULL,
  `itemId` bigint(20) UNSIGNED NOT NULL,
  `receiptDate` datetime NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `location` bigint(20) UNSIGNED NOT NULL,
  `lotNumber` varchar(100) DEFAULT NULL,
  `status` tinyint(2) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `itempopenerimaanio`
--

INSERT INTO `itempopenerimaanio` (`id`, `ioNumber`, `itemId`, `receiptDate`, `quantity`, `location`, `lotNumber`, `status`) VALUES
(1, 'PELNI/IO 18 UMS 00001', 1, '2018-07-30 00:00:00', 12, 1, '12', 0),
(2, 'PELNI/IO 18 UMS 00002', 2, '2018-07-30 00:00:00', 12, 2, '12', 0);

-- --------------------------------------------------------

--
-- Table structure for table `kapal`
--

CREATE TABLE `kapal` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kodeKapal` varchar(3) NOT NULL,
  `namaKapal` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kapal`
--

INSERT INTO `kapal` (`id`, `kodeKapal`, `namaKapal`) VALUES
(1, 'HOW', 'PELNI Kantor Pusat'),
(2, 'TJP', 'PELNI Tj. Priok'),
(3, 'SBY', 'PELNI Surabaya'),
(4, 'MKS', 'PELNI Makassar'),
(5, 'UMS', 'PELNI KP Umsini'),
(6, 'KMT', 'PELNI KP Kelimutu'),
(7, 'LWT', 'PELNI KP Lawit'),
(8, 'TDR', 'PELNI KP Tidar'),
(9, 'TML', 'PELNI KP Tatamailau'),
(10, 'SRM', 'PELNI KP Sirimau'),
(11, 'AWU', 'PELNI KP AWU'),
(12, 'CRI', 'PELNI KP Ciremai'),
(13, 'DBS', 'PELNI KP Dobonsolo'),
(14, 'LEU', 'PELNI KP Leuser'),
(15, 'BNY', 'PELNI KP Binaya'),
(16, 'BKR', 'PELNI KP Bukit Raya'),
(17, 'TKB', 'PELNI KP Tilong Kabila'),
(18, 'BKS', 'PELNI KP Bukit Siguntang'),
(19, 'LBL', 'PELNI KP Lambelu'),
(20, 'SNB', 'PELNI KP Sinabung'),
(21, 'KLD', 'PELNI KP Kelud'),
(22, 'PRG', 'PELNI KP Pangrango'),
(23, 'SGG', 'PELNI KP Sangiang'),
(24, 'WLS', 'PELNI KP Wilis'),
(25, 'GWT', 'PELNI KP Ganda Winata'),
(26, 'EGN', 'PELNI KP Egon'),
(27, 'DLN', 'PELNI KP Dorolonda'),
(28, 'NGP', 'PELNI KP Nggapulu'),
(29, 'LBR', 'PELNI KP Labobar'),
(30, 'DMP', 'PELNI KP Dempo'),
(31, 'JLR', 'PELNI KP Jet Liner');

-- --------------------------------------------------------

--
-- Table structure for table `loginlog`
--

CREATE TABLE `loginlog` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userId` bigint(20) UNSIGNED NOT NULL,
  `loginStartDate` datetime DEFAULT NULL,
  `loginEndDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `lokasiitem`
--

CREATE TABLE `lokasiitem` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `namaLokasi` varchar(200) NOT NULL,
  `lokasi` varchar(200) DEFAULT NULL,
  `alamatLokasi` varchar(200) DEFAULT NULL,
  `ORGCode` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lokasiitem`
--

INSERT INTO `lokasiitem` (`id`, `namaLokasi`, `lokasi`, `alamatLokasi`, `ORGCode`) VALUES
(1, 'Gudang', 'Gudang', 'Jl. Kemarin', '29034'),
(2, 'Gudang Dapur', 'Dapur', 'jl. dapur', '214234'),
(3, 'Gudang II', 'Deck Atas', '123', '123'),
(4, 'Gudang III', 'Deck Bawah', '123', '123');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userId` bigint(20) UNSIGNED NOT NULL,
  `notificationType` tinyint(2) UNSIGNED NOT NULL,
  `refId` bigint(20) UNSIGNED NOT NULL,
  `date` datetime NOT NULL,
  `isRead` tinyint(2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `penerimaanbarang`
--

CREATE TABLE `penerimaanbarang` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `receiptNumber` varchar(50) NOT NULL,
  `poNumber` varchar(50) NOT NULL,
  `userId` bigint(20) NOT NULL,
  `kapalId` bigint(20) NOT NULL,
  `locationId` bigint(20) NOT NULL,
  `unitOperasi` varchar(100) DEFAULT NULL,
  `tanggalPenerimaan` datetime NOT NULL,
  `tanggalPO` datetime NOT NULL,
  `qtyDiterima` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `voyageKode` varchar(10) DEFAULT NULL,
  `voyageStart` datetime DEFAULT NULL,
  `voyageEnd` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `penerimaanbarang`
--

INSERT INTO `penerimaanbarang` (`id`, `receiptNumber`, `poNumber`, `userId`, `kapalId`, `locationId`, `unitOperasi`, `tanggalPenerimaan`, `tanggalPO`, `qtyDiterima`, `voyageKode`, `voyageStart`, `voyageEnd`, `status`) VALUES
(1, 'PELNI/PB 18 UMS 000001', 'PELNI/PO 18 UMS 000001', 1, 5, 1, '213', '2018-07-26 00:00:00', '2018-07-26 00:00:00', 21, NULL, NULL, NULL, 0),
(2, 'PELNI/PB 18 UMS 000002', 'PELNI/PO 18 UMS 000001', 1, 5, 1, '213', '2018-07-26 00:00:00', '2018-07-26 00:00:00', 234, NULL, NULL, NULL, 0),
(3, 'PELNI/PB 18 UMS 000003', 'PELNI/PO 18 UMS 000001', 1, 5, 1, '213', '2018-07-28 00:00:00', '2018-07-26 00:00:00', 32, NULL, NULL, NULL, 0),
(6, 'PELNI/PB 18 UMS 000004', 'PELNI/PB 18 UMS 000001', 1, 5, 1, '213', '2018-08-01 00:00:00', '2018-07-26 00:00:00', 23, NULL, NULL, NULL, 0),
(14, 'PELNI/PB 18 UMS 000005', 'PELNI/PB 18 UMS 000001', 1, 5, 1, '213', '2018-08-01 00:00:00', '2018-07-26 00:00:00', 2, NULL, NULL, NULL, 0),
(30, 'PELNI/PB 18 UMS 000006', 'PELNI/PB 18 UMS 000001', 1, 5, 1, '213', '2018-08-01 00:00:00', '2018-07-26 00:00:00', 2, NULL, NULL, NULL, 0),
(40, 'PELNI/PB 18 UMS 000007', 'PELNI/PB 18 UMS 000001', 1, 5, 1, '213', '2018-08-01 00:00:00', '2018-07-26 00:00:00', 2, NULL, NULL, NULL, 0),
(41, 'PELNI/PB 18 UMS 000008', 'PELNI/PB 18 UMS 000001', 1, 5, 1, '213', '2018-08-01 00:00:00', '2018-07-26 00:00:00', 5, NULL, NULL, NULL, 0),
(42, 'PELNI/PB 18 UMS 000009', 'PELNI/PB 18 UMS 000001', 1, 5, 1, '213', '2018-08-04 00:00:00', '2018-07-26 00:00:00', 23, '0014', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `penerimaanbarangio`
--

CREATE TABLE `penerimaanbarangio` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `rcNumber` varchar(50) NOT NULL,
  `ioNumber` varchar(50) NOT NULL,
  `userId` bigint(20) UNSIGNED NOT NULL,
  `kapalId` bigint(20) UNSIGNED NOT NULL,
  `locationId` bigint(20) UNSIGNED NOT NULL,
  `unitOperasi` varchar(100) DEFAULT NULL,
  `tanggalPenerimaan` datetime NOT NULL,
  `tanggalPO` datetime NOT NULL,
  `qtyDiterima` int(10) UNSIGNED NOT NULL,
  `voyageKode` varchar(10) DEFAULT NULL,
  `voyageStart` datetime DEFAULT NULL,
  `voyageEnd` datetime DEFAULT NULL,
  `status` tinyint(2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `penerimaanbarangio`
--

INSERT INTO `penerimaanbarangio` (`id`, `rcNumber`, `ioNumber`, `userId`, `kapalId`, `locationId`, `unitOperasi`, `tanggalPenerimaan`, `tanggalPO`, `qtyDiterima`, `voyageKode`, `voyageStart`, `voyageEnd`, `status`) VALUES
(1, 'PELNI/RC 18 UMS 000001', 'PELNI/IO 18 UMS 00002', 1, 5, 1, '12', '2018-07-30 00:00:00', '2018-07-29 00:00:00', 12, NULL, NULL, NULL, 0),
(3, 'PELNI/RC 18 UMS 000002', 'PELNI/IO 18 UMS 00002', 1, 5, 1, '12', '2018-08-01 00:00:00', '2018-07-29 00:00:00', 2, NULL, NULL, NULL, 0),
(4, 'PELNI/RC 18 UMS 000003', 'PELNI/IO 18 UMS 00001', 1, 5, 1, '12', '2018-08-01 00:00:00', '2018-07-29 00:00:00', 23, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `penerimaanmakanan`
--

CREATE TABLE `penerimaanmakanan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `requesterId` bigint(20) UNSIGNED NOT NULL,
  `approverId` bigint(20) UNSIGNED DEFAULT NULL,
  `kapalId` bigint(20) UNSIGNED NOT NULL,
  `mrNumber` varchar(50) NOT NULL,
  `locationId` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(2) UNSIGNED NOT NULL,
  `accountId` bigint(20) UNSIGNED NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `totalBarang` int(10) UNSIGNED DEFAULT '0',
  `komentar` text,
  `voyageKode` varchar(10) DEFAULT NULL,
  `voyageStart` datetime DEFAULT NULL,
  `voyageEnd` datetime DEFAULT NULL,
  `status` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `tglApproved` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `penerimaanmakanan`
--

INSERT INTO `penerimaanmakanan` (`id`, `requesterId`, `approverId`, `kapalId`, `mrNumber`, `locationId`, `type`, `accountId`, `date`, `totalBarang`, `komentar`, `voyageKode`, `voyageStart`, `voyageEnd`, `status`, `tglApproved`) VALUES
(1, 1, NULL, 5, 'PELNI/MS 18 UMS 000001', 1, 1, 2, '2018-08-23 00:00:00', 0, '34 t34 t34t', '0021', '2018-08-15 00:00:00', '2018-08-27 00:00:00', 0, '2018-08-08 11:54:53'),
(2, 1, NULL, 5, 'PELNI/MS 18 UMS 000002', 1, 1, 1, '2018-08-02 00:00:00', 0, 'kumpulan nasi goreng', '0000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2018-08-08 12:14:13');

-- --------------------------------------------------------

--
-- Table structure for table `penggunaanbarang`
--

CREATE TABLE `penggunaanbarang` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `miNumber` varchar(50) NOT NULL,
  `requesterId` bigint(20) UNSIGNED NOT NULL,
  `approverId` bigint(20) UNSIGNED DEFAULT NULL,
  `transactionType` tinyint(2) UNSIGNED NOT NULL,
  `kapalId` bigint(20) UNSIGNED NOT NULL,
  `locationId` bigint(20) UNSIGNED NOT NULL,
  `destinationAccount` varchar(100) NOT NULL,
  `tglPenggunaan` datetime NOT NULL,
  `jlhBarang` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `voyageKode` varchar(10) DEFAULT NULL,
  `voyageStart` datetime DEFAULT NULL,
  `voyageEnd` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `penggunaanbarang`
--

INSERT INTO `penggunaanbarang` (`id`, `miNumber`, `requesterId`, `approverId`, `transactionType`, `kapalId`, `locationId`, `destinationAccount`, `tglPenggunaan`, `jlhBarang`, `voyageKode`, `voyageStart`, `voyageEnd`) VALUES
(1, '', 1, 1, 1, 2, 1, '1', '0000-00-00 00:00:00', 0, NULL, NULL, NULL),
(2, '', 1, 1, 3, 3, 3, '2', '0000-00-00 00:00:00', 0, NULL, NULL, NULL),
(3, 'PELNI/MI 18 UMS 000001', 1, 0, 1, 5, 1, '1', '2018-07-27 00:00:00', 0, NULL, NULL, NULL),
(4, 'PELNI/MI 18 UMS 000002', 1, 0, 1, 5, 3, '1', '2018-07-27 00:00:00', 3, NULL, NULL, NULL),
(5, 'PELNI/MI 18 UMS 000003', 1, 0, 1, 5, 2, '2', '2018-07-27 00:00:00', 3, NULL, NULL, NULL),
(6, 'PELNI/MI 18 UMS 000004', 1, 0, 1, 5, 2, '2', '2018-07-27 00:00:00', 3, '0202', '2018-09-01 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `penggunaanmakanan`
--

CREATE TABLE `penggunaanmakanan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `requesterId` bigint(20) UNSIGNED NOT NULL,
  `approverId` bigint(20) UNSIGNED DEFAULT NULL,
  `kapalId` bigint(20) UNSIGNED NOT NULL,
  `msNumber` varchar(50) NOT NULL,
  `locationId` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(2) UNSIGNED NOT NULL,
  `accountId` bigint(20) UNSIGNED NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `totalBarang` int(10) UNSIGNED DEFAULT '0',
  `komentar` text,
  `voyageKode` varchar(10) DEFAULT NULL,
  `voyageStart` datetime DEFAULT NULL,
  `voyageEnd` datetime DEFAULT NULL,
  `status` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `tglApproved` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `penggunaanmakanan`
--

INSERT INTO `penggunaanmakanan` (`id`, `requesterId`, `approverId`, `kapalId`, `msNumber`, `locationId`, `type`, `accountId`, `date`, `totalBarang`, `komentar`, `voyageKode`, `voyageStart`, `voyageEnd`, `status`, `tglApproved`) VALUES
(1, 1, NULL, 3, '', 2, 2, 213, '2018-07-29 16:20:15', 0, NULL, '0101', NULL, NULL, 0, '2018-08-06 17:07:15'),
(2, 1, NULL, 2, '', 3, 3, 234, '2018-07-29 16:20:15', 0, NULL, '0212', NULL, NULL, 0, '2018-08-06 17:08:05'),
(3, 1, 1, 5, 'PELNI/MS 18 UMS 000001', 1, 1, 2, '2018-08-21 00:00:00', 0, NULL, '0013', NULL, NULL, 1, '2018-08-06 18:47:15');

-- --------------------------------------------------------

--
-- Table structure for table `permintaanbarang`
--

CREATE TABLE `permintaanbarang` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `prNumber` varchar(50) NOT NULL,
  `requesterId` bigint(20) UNSIGNED NOT NULL,
  `approverId` bigint(20) UNSIGNED DEFAULT NULL,
  `kapalId` bigint(20) UNSIGNED NOT NULL,
  `locationId` bigint(20) UNSIGNED NOT NULL,
  `destinationType` varchar(200) DEFAULT NULL,
  `operatingUnit` varchar(200) NOT NULL,
  `organization` varchar(200) NOT NULL,
  `keperluan` varchar(200) DEFAULT NULL,
  `totalHarga` int(15) UNSIGNED DEFAULT '0',
  `totalBarang` int(10) UNSIGNED DEFAULT '0',
  `sumber` varchar(200) DEFAULT NULL,
  `komentar` text,
  `deskripsi` varchar(200) DEFAULT NULL,
  `tglPermintaan` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tglApproved` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isApproved` tinyint(2) UNSIGNED NOT NULL,
  `voyageKode` varchar(10) DEFAULT NULL,
  `voyageStart` datetime DEFAULT NULL,
  `voyageEnd` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permintaanbarang`
--

INSERT INTO `permintaanbarang` (`id`, `prNumber`, `requesterId`, `approverId`, `kapalId`, `locationId`, `destinationType`, `operatingUnit`, `organization`, `keperluan`, `totalHarga`, `totalBarang`, `sumber`, `komentar`, `deskripsi`, `tglPermintaan`, `tglApproved`, `isApproved`, `voyageKode`, `voyageStart`, `voyageEnd`) VALUES
(1, 'PELNI/PR 18 HOW 000001', 1, 0, 1, 2, '', 'sdf', 'sdfsdf', 'sdfsdf', 55995, 1242, 'sdfsdf', 'fsdfsdfsdfsdf', 'sdfsdf', '2018-07-25 17:44:28', '2018-07-31 16:11:01', 0, NULL, NULL, NULL),
(7, 'PELNI/PR 18 HOW 000002', 1, 0, 1, 1, '', '23', '234 ', '234', 23400, 234, '23 4 234', '23 423 4234', '23 4', '2018-07-26 02:32:53', '2018-07-31 16:11:01', 0, NULL, NULL, NULL),
(8, 'PELNI/PR 18 HOW 000003', 1, 0, 1, 1, '', 'asd', 'sdad', 'asd', 24495, 333, 'asdas', 'dasdas', 'asd', '2018-07-26 02:44:31', '2018-07-31 16:11:01', 0, NULL, NULL, NULL),
(9, 'PELNI/PR 18 HOW 000004', 1, 0, 1, 1, '', '234', '234', '234', 11885, 236, '234', '234234', '234', '2018-07-26 02:57:22', '2018-07-31 16:11:01', 0, NULL, NULL, NULL),
(11, 'PELNI/PR 18 UMS 000001', 1, 0, 5, 1, '', '234', '234', '234', 0, 0, '234', '234', '234', '2018-07-26 03:09:47', '2018-07-31 16:11:01', 0, NULL, NULL, NULL),
(12, 'PELNI/PR 18 UMS 000002', 1, 0, 5, 1, '', '123', '123', '123', 0, 0, '123', '123123', '123', '2018-07-26 03:31:31', '2018-07-31 16:11:01', 0, NULL, NULL, NULL),
(13, 'PELNI/PR 18 UMS 000003', 1, 0, 5, 2, '', '23', '234', '234', 18720, 468, '234', '234 23423 4234', '23', '2018-07-26 04:25:06', '2018-07-31 16:11:01', 0, NULL, NULL, NULL),
(14, 'PELNI/PR 18 UMS 000004', 1, 0, 5, 1, '', 'wer', 'wer', 'wer', 2270, 55, 'wer', 'wer', 'wer', '2018-07-26 04:26:58', '2018-07-31 16:11:01', 0, NULL, NULL, NULL),
(15, 'PELNI/PR 18 UMS 000005', 1, 0, 5, 1, '', '213', '123', '123', 12900, 135, '123', '123123', '123', '2018-07-26 04:28:42', '2018-07-31 16:11:01', 0, NULL, NULL, NULL),
(16, 'PELNI/PR 18 UMS 000006', 1, 0, 5, 2, '', '123', '123', '213', 10000, 100, '123', '23123123', '123', '2018-07-31 03:11:47', '2018-07-31 16:11:01', 0, NULL, NULL, NULL),
(17, 'PELNI/PR 18 UMS 000007', 1, 0, 5, 3, '', '123', '123', '123', 675, 15, '123', '123123 12312 3', '123', '2018-07-31 03:43:11', '2018-07-31 16:11:01', 0, NULL, NULL, NULL),
(20, 'PELNI/PR 18 UMS 000008', 1, 1, 5, 1, '', '123', '123', '123', 0, 0, '123', '123123', '123', '2018-07-31 04:26:27', '2018-08-01 09:36:38', 1, NULL, NULL, NULL),
(21, 'PELNI/PR 18 UMS 000009', 1, 0, 5, 2, '', '232r', '23r', '3r2', 123200, 1232, '23r', '32r 23r 23r ', '32r23r 23r 23', '2018-08-04 16:07:55', '2018-08-04 23:08:33', 0, '0000', NULL, NULL),
(22, 'PELNI/PR 18 UMS 000010', 1, 0, 5, 1, '', '23', '32', '32', 3450, 46, '3r2', '23r', '32', '2018-08-08 03:49:08', '2018-08-08 10:49:38', 0, '0011', '2018-08-08 00:00:00', '2018-08-29 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pindahbarangio`
--

CREATE TABLE `pindahbarangio` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ioNumber` varchar(50) NOT NULL,
  `type` tinyint(2) UNSIGNED DEFAULT '1',
  `requesterId` bigint(20) UNSIGNED NOT NULL,
  `approverId` bigint(20) UNSIGNED DEFAULT NULL,
  `fromKapalId` bigint(20) UNSIGNED NOT NULL,
  `toKapalId` bigint(20) UNSIGNED NOT NULL,
  `fromLocationId` bigint(20) UNSIGNED NOT NULL,
  `toLocationId` bigint(20) UNSIGNED NOT NULL,
  `toOrganization` varchar(200) NOT NULL,
  `jlhBarang` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `expectedReceivedDate` datetime NOT NULL,
  `tglTransaksi` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `komentar` text,
  `status` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `tglApproved` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `voyageKode` varchar(10) DEFAULT NULL,
  `voyageStart` datetime DEFAULT NULL,
  `voyageEnd` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pindahbarangio`
--

INSERT INTO `pindahbarangio` (`id`, `ioNumber`, `type`, `requesterId`, `approverId`, `fromKapalId`, `toKapalId`, `fromLocationId`, `toLocationId`, `toOrganization`, `jlhBarang`, `expectedReceivedDate`, `tglTransaksi`, `komentar`, `status`, `tglApproved`, `voyageKode`, `voyageStart`, `voyageEnd`) VALUES
(1, '234', 2, 2, 2, 5, 6, 1, 2, '234', 10, '2018-07-31 00:00:00', '2018-07-27 16:02:35', NULL, 0, '2018-08-03 09:45:00', NULL, NULL, NULL),
(3, 'PELNI/IO 18 UMS 0000', 1, 1, 0, 5, 15, 0, 0, 'xxx', 0, '2018-07-30 00:00:00', '2018-07-27 23:09:26', NULL, 0, '2018-08-03 09:45:00', NULL, NULL, NULL),
(4, 'PELNI/IO 18 UMS 0000', 1, 1, 1, 5, 14, 0, 0, 'xxx', 0, '2018-08-01 00:00:00', '2018-07-31 11:37:21', NULL, 2, '2018-08-06 18:14:04', NULL, NULL, NULL),
(5, 'PELNI/IO 18 UMS 0000', 1, 1, 1, 5, 20, 0, 0, 'xxx', 0, '0000-00-00 00:00:00', '2018-07-31 11:38:45', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ultrices velit in condimentum viverra. Cras tortor erat, aliquam ut venenatis sit amet, maximus eget arcu.\r\n\r\n', 1, '2018-08-03 09:45:00', NULL, NULL, NULL),
(6, 'PELNI/IO 18 UMS 0000', 1, 1, 0, 5, 16, 0, 0, 'xxx', 0, '2018-08-10 00:00:00', '2018-08-07 09:27:10', NULL, 0, '2018-08-07 09:27:10', NULL, NULL, NULL),
(7, 'PELNI/IO 18 UMS 000001', 1, 1, 0, 5, 10, 0, 0, 'xxx', 0, '2018-08-10 00:00:00', '2018-08-07 10:12:50', NULL, 0, '2018-08-07 10:16:06', NULL, NULL, NULL),
(8, 'PELNI/IO 18 UMS 000002', 1, 1, 0, 5, 13, 0, 1, '-', 0, '2018-08-26 00:00:00', '2018-08-07 10:19:12', 'masuk komentar', 0, '2018-08-07 10:19:12', '0022', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pindahbarangsubinv`
--

CREATE TABLE `pindahbarangsubinv` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `moNumber` varchar(50) NOT NULL,
  `kapalId` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(2) UNSIGNED NOT NULL,
  `requesterId` bigint(20) UNSIGNED NOT NULL,
  `approverId` bigint(20) UNSIGNED DEFAULT NULL,
  `fromLocationId` bigint(20) UNSIGNED NOT NULL,
  `toLocationId` bigint(20) UNSIGNED NOT NULL,
  `transactionType` tinyint(2) UNSIGNED NOT NULL,
  `voyageId` bigint(20) UNSIGNED DEFAULT NULL,
  `voyageStart` datetime DEFAULT NULL,
  `voyageEnd` datetime DEFAULT NULL,
  `dateRequired` datetime NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `komentar` tinytext,
  `status` tinyint(2) UNSIGNED NOT NULL,
  `tglApproved` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pindahbarangsubinv`
--

INSERT INTO `pindahbarangsubinv` (`id`, `moNumber`, `kapalId`, `type`, `requesterId`, `approverId`, `fromLocationId`, `toLocationId`, `transactionType`, `voyageId`, `voyageStart`, `voyageEnd`, `dateRequired`, `quantity`, `dateCreated`, `komentar`, `status`, `tglApproved`) VALUES
(1, 'PELNI/MO 18 UMS 000001', 5, 2, 2, 1, 2, 1, 1, 0, NULL, NULL, '2018-08-02 00:00:00', 20, '2018-08-02 12:07:31', NULL, 0, '2018-08-03 09:46:22'),
(3, 'PELNI/MO 18 UMS 000002', 5, 0, 1, 1, 3, 4, 0, 12, '2018-09-02 00:00:00', '2018-09-06 00:00:00', '2018-09-01 00:00:00', 35, '2018-08-02 16:36:07', 'qweqweq qw eqwe qwe qw eqw', 2, '2018-08-06 18:14:16');

-- --------------------------------------------------------

--
-- Table structure for table `popenerimaan`
--

CREATE TABLE `popenerimaan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `poNumber` varchar(50) NOT NULL,
  `kapalId` bigint(20) UNSIGNED NOT NULL,
  `date` datetime NOT NULL,
  `status` tinyint(2) UNSIGNED NOT NULL,
  `operatingUnit` varchar(200) DEFAULT NULL,
  `quantity` varchar(200) DEFAULT NULL,
  `qtyReceiptNumber` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `popenerimaan`
--

INSERT INTO `popenerimaan` (`id`, `poNumber`, `kapalId`, `date`, `status`, `operatingUnit`, `quantity`, `qtyReceiptNumber`) VALUES
(1, '123', 1, '2018-07-30 00:00:00', 1, '246', '12', 0),
(2, 'PELNI/PB 18 UMS 000001', 5, '2018-07-26 00:00:00', 1, '213', '12', 0);

-- --------------------------------------------------------

--
-- Table structure for table `popenerimaanio`
--

CREATE TABLE `popenerimaanio` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ioNumber` varchar(50) NOT NULL,
  `kapalId` bigint(20) UNSIGNED NOT NULL,
  `date` datetime NOT NULL,
  `status` tinyint(2) UNSIGNED NOT NULL,
  `operatingUnit` varchar(200) DEFAULT NULL,
  `quantity` varchar(200) DEFAULT NULL,
  `qtyReceiptNumber` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `popenerimaanio`
--

INSERT INTO `popenerimaanio` (`id`, `ioNumber`, `kapalId`, `date`, `status`, `operatingUnit`, `quantity`, `qtyReceiptNumber`) VALUES
(1, 'PELNI/IO 18 UMS 00001', 5, '2018-07-29 00:00:00', 1, '12', '12', 0),
(2, 'PELNI/IO 18 UMS 00002', 5, '2018-07-29 00:00:00', 1, '12', '12', 0);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `roleName` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `roleName`) VALUES
(1, 'administrator');

-- --------------------------------------------------------

--
-- Table structure for table `stokkapal`
--

CREATE TABLE `stokkapal` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `itemId` bigint(20) UNSIGNED NOT NULL,
  `kapalId` bigint(20) UNSIGNED NOT NULL,
  `lokasiId` bigint(20) UNSIGNED NOT NULL,
  `jumlah` int(10) UNSIGNED NOT NULL,
  `satuan` varchar(50) DEFAULT NULL,
  `status` tinyint(2) UNSIGNED NOT NULL,
  `type` tinyint(2) UNSIGNED DEFAULT '1' COMMENT '1: general items. 2: dapur/resto'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stokkapal`
--

INSERT INTO `stokkapal` (`id`, `itemId`, `kapalId`, `lokasiId`, `jumlah`, `satuan`, `status`, `type`) VALUES
(7, 2, 5, 2, 34, 'kg', 1, 1),
(8, 1, 5, 1, 23, 'buah', 1, 1),
(9, 12, 5, 3, 10, '', 1, 2),
(10, 25, 5, 3, 5, '', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) NOT NULL,
  `roleId` bigint(20) UNSIGNED NOT NULL,
  `kapalId` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `roleId`, `kapalId`, `username`, `password`) VALUES
(1, 'administrator', 1, 5, 'sioado', '$2y$12$U5BsEAXwbNHZgvSZx.Bqre8aXUgzLzNShrJ.3hXQZMcFSQh/unDP2'),
(2, 'Seth Setiadha', 1, 5, 'sethsetiadha', '$2y$12$U5BsEAXwbNHZgvSZx.Bqre8aXUgzLzNShrJ.3hXQZMcFSQh/unDP2');

-- --------------------------------------------------------

--
-- Table structure for table `voyages`
--

CREATE TABLE `voyages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `voyageKode` varchar(10) DEFAULT '0',
  `voyageName` varchar(100) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `voyages`
--

INSERT INTO `voyages` (`id`, `voyageKode`, `voyageName`) VALUES
(1, '0000', 'None'),
(2, '0011', 'Voyage 001 - Call 1'),
(3, '0012', 'Voyage 001 - Call 2'),
(4, '0013', 'Voyage 001 - Call 3'),
(5, '0014', 'Voyage 001 - Call 4'),
(6, '0021', 'Voyage 002 - Call 1'),
(7, '0022', 'Voyage 002 - Call 2'),
(8, '0023', 'Voyage 002 - Call 3'),
(9, '0024', 'Voyage 002 - Call 4'),
(10, '0031', 'Voyage 003 - Call 1'),
(11, '0032', 'Voyage 003 - Call 2'),
(12, '0033', 'Voyage 003 - Call 3'),
(13, '0034', 'Voyage 003 - Call 4'),
(14, '0041', 'Voyage 004 - Call 1'),
(15, '0042', 'Voyage 004 - Call 2'),
(16, '0043', 'Voyage 004 - Call 3'),
(17, '0044', 'Voyage 004 - Call 4'),
(18, '0051', 'Voyage 005 - Call 1'),
(19, '0052', 'Voyage 005 - Call 2'),
(20, '0053', 'Voyage 005 - Call 3'),
(21, '0054', 'Voyage 005 - Call 4'),
(22, '0061', 'Voyage 006 - Call 1'),
(23, '0062', 'Voyage 006 - Call 2'),
(24, '0063', 'Voyage 006 - Call 3'),
(25, '0064', 'Voyage 006 - Call 4'),
(26, '0071', 'Voyage 007 - Call 1'),
(27, '0072', 'Voyage 007 - Call 2'),
(28, '0073', 'Voyage 007 - Call 3'),
(29, '0074', 'Voyage 007 - Call 4'),
(30, '0081', 'Voyage 008 - Call 1'),
(31, '0082', 'Voyage 008 - Call 2'),
(32, '0083', 'Voyage 008 - Call 3'),
(33, '0084', 'Voyage 008 - Call 4'),
(34, '0091', 'Voyage 009 - Call 1'),
(35, '0092', 'Voyage 009 - Call 2'),
(36, '0093', 'Voyage 009 - Call 3'),
(37, '0094', 'Voyage 009 - Call 4'),
(38, '0101', 'Voyage 010 - Call 1'),
(39, '0102', 'Voyage 010 - Call 2'),
(40, '0103', 'Voyage 010 - Call 3'),
(41, '0104', 'Voyage 010 - Call 4'),
(42, '0111', 'Voyage 011 - Call 1'),
(43, '0112', 'Voyage 011 - Call 2'),
(44, '0113', 'Voyage 011 - Call 3'),
(45, '0114', 'Voyage 011 - Call 4'),
(46, '0121', 'Voyage 012 - Call 1'),
(47, '0122', 'Voyage 012 - Call 2'),
(48, '0123', 'Voyage 012 - Call 3'),
(49, '0124', 'Voyage 012 - Call 4'),
(50, '0131', 'Voyage 013 - Call 1'),
(51, '0132', 'Voyage 013 - Call 2'),
(52, '0133', 'Voyage 013 - Call 3'),
(53, '0134', 'Voyage 013 - Call 4'),
(54, '0141', 'Voyage 014 - Call 1'),
(55, '0142', 'Voyage 014 - Call 2'),
(56, '0143', 'Voyage 014 - Call 3'),
(57, '0144', 'Voyage 014 - Call 4'),
(58, '0151', 'Voyage 015 - Call 1'),
(59, '0152', 'Voyage 015 - Call 2'),
(60, '0153', 'Voyage 015 - Call 3'),
(61, '0154', 'Voyage 015 - Call 4'),
(62, '0161', 'Voyage 016 - Call 1'),
(63, '0162', 'Voyage 016 - Call 2'),
(64, '0163', 'Voyage 016 - Call 3'),
(65, '0164', 'Voyage 016 - Call 4'),
(66, '0171', 'Voyage 017 - Call 1'),
(67, '0172', 'Voyage 017 - Call 2'),
(68, '0173', 'Voyage 017 - Call 3'),
(69, '0174', 'Voyage 017 - Call 4'),
(70, '0181', 'Voyage 018 - Call 1'),
(71, '0182', 'Voyage 018 - Call 2'),
(72, '0183', 'Voyage 018 - Call 3'),
(73, '0184', 'Voyage 018 - Call 4'),
(74, '0191', 'Voyage 019 - Call 1'),
(75, '0192', 'Voyage 019 - Call 2'),
(76, '0193', 'Voyage 019 - Call 3'),
(77, '0194', 'Voyage 019 - Call 4'),
(78, '0201', 'Voyage 020 - Call 1'),
(79, '0202', 'Voyage 020 - Call 2'),
(80, '0203', 'Voyage 020 - Call 3'),
(81, '0204', 'Voyage 020 - Call 4'),
(82, '0211', 'Voyage 021 - Call 1'),
(83, '0212', 'Voyage 021 - Call 2'),
(84, '0213', 'Voyage 021 - Call 3'),
(85, '0214', 'Voyage 021 - Call 4'),
(86, '0221', 'Voyage 022 - Call 1'),
(87, '0222', 'Voyage 022 - Call 2'),
(88, '0223', 'Voyage 022 - Call 3'),
(89, '0224', 'Voyage 022 - Call 4'),
(90, '0231', 'Voyage 023 - Call 1'),
(91, '0232', 'Voyage 023 - Call 2'),
(92, '0233', 'Voyage 023 - Call 3'),
(93, '0234', 'Voyage 023 - Call 4'),
(94, '0241', 'Voyage 024 - Call 1'),
(95, '0242', 'Voyage 024 - Call 2'),
(96, '0243', 'Voyage 024 - Call 3'),
(97, '0244', 'Voyage 024 - Call 4'),
(98, '0251', 'Voyage 025 - Call 1'),
(99, '0252', 'Voyage 025 - Call 2'),
(100, '0253', 'Voyage 025 - Call 3'),
(101, '0254', 'Voyage 025 - Call 4'),
(102, '0261', 'Voyage 026 - Call 1'),
(103, '0262', 'Voyage 026 - Call 2'),
(104, '0263', 'Voyage 026 - Call 3'),
(105, '0264', 'Voyage 026 - Call 4'),
(106, '0271', 'Voyage 027 - Call 1'),
(107, '0272', 'Voyage 027 - Call 2'),
(108, '0273', 'Voyage 027 - Call 3'),
(109, '0274', 'Voyage 027 - Call 4'),
(110, '0281', 'Voyage 028 - Call 1'),
(111, '0282', 'Voyage 028 - Call 2'),
(112, '0283', 'Voyage 028 - Call 3'),
(113, '0284', 'Voyage 028 - Call 4'),
(114, '0291', 'Voyage 029 - Call 1'),
(115, '0292', 'Voyage 029 - Call 2'),
(116, '0293', 'Voyage 029 - Call 3'),
(117, '0294', 'Voyage 029 - Call 4'),
(118, '0301', 'Voyage 030 - Call 1'),
(119, '0302', 'Voyage 030 - Call 2'),
(120, '0303', 'Voyage 030 - Call 3'),
(121, '0304', 'Voyage 030 - Call 4'),
(122, '0311', 'Voyage 031 - Call 1'),
(123, '0312', 'Voyage 031 - Call 2'),
(124, '0313', 'Voyage 031 - Call 3'),
(125, '0314', 'Voyage 031 - Call 4'),
(126, '0321', 'Voyage 032 - Call 1'),
(127, '0322', 'Voyage 032 - Call 2'),
(128, '0323', 'Voyage 032 - Call 3'),
(129, '0324', 'Voyage 032 - Call 4'),
(130, '0331', 'Voyage 033 - Call 1'),
(131, '0332', 'Voyage 033 - Call 2'),
(132, '0333', 'Voyage 033 - Call 3'),
(133, '0334', 'Voyage 033 - Call 4'),
(134, '0341', 'Voyage 034 - Call 1'),
(135, '0342', 'Voyage 034 - Call 2'),
(136, '0343', 'Voyage 034 - Call 3'),
(137, '0344', 'Voyage 034 - Call 4'),
(138, '0351', 'Voyage 035 - Call 1'),
(139, '0352', 'Voyage 035 - Call 2'),
(140, '0353', 'Voyage 035 - Call 3'),
(141, '0354', 'Voyage 035 - Call 4'),
(142, '0361', 'Voyage 036 - Call 1'),
(143, '0362', 'Voyage 036 - Call 2'),
(144, '0363', 'Voyage 036 - Call 3'),
(145, '0364', 'Voyage 036 - Call 4'),
(146, '0371', 'Voyage 037 - Call 1'),
(147, '0372', 'Voyage 037 - Call 2'),
(148, '0373', 'Voyage 037 - Call 3'),
(149, '0374', 'Voyage 037 - Call 4'),
(150, '0381', 'Voyage 038 - Call 1'),
(151, '0382', 'Voyage 038 - Call 2'),
(152, '0383', 'Voyage 038 - Call 3'),
(153, '0384', 'Voyage 038 - Call 4'),
(154, '0391', 'Voyage 039 - Call 1'),
(155, '0392', 'Voyage 039 - Call 2'),
(156, '0393', 'Voyage 039 - Call 3'),
(157, '0394', 'Voyage 039 - Call 4'),
(158, '0401', 'Voyage 040 - Call 1'),
(159, '0402', 'Voyage 040 - Call 2'),
(160, '0403', 'Voyage 040 - Call 3'),
(161, '0404', 'Voyage 040 - Call 4'),
(162, '0411', 'Voyage 041 - Call 1'),
(163, '0412', 'Voyage 041 - Call 2'),
(164, '0413', 'Voyage 041 - Call 3'),
(165, '0414', 'Voyage 041 - Call 4'),
(166, '0421', 'Voyage 042 - Call 1'),
(167, '0422', 'Voyage 042 - Call 2'),
(168, '0423', 'Voyage 042 - Call 3'),
(169, '0424', 'Voyage 042 - Call 4'),
(170, '0431', 'Voyage 043 - Call 1'),
(171, '0432', 'Voyage 043 - Call 2'),
(172, '0433', 'Voyage 043 - Call 3'),
(173, '0434', 'Voyage 043 - Call 4'),
(174, '0441', 'Voyage 044 - Call 1'),
(175, '0442', 'Voyage 044 - Call 2'),
(176, '0443', 'Voyage 044 - Call 3'),
(177, '0444', 'Voyage 044 - Call 4'),
(178, '0451', 'Voyage 045 - Call 1'),
(179, '0452', 'Voyage 045 - Call 2'),
(180, '0453', 'Voyage 045 - Call 3'),
(181, '0454', 'Voyage 045 - Call 4'),
(182, '0461', 'Voyage 046 - Call 1'),
(183, '0462', 'Voyage 046 - Call 2'),
(184, '0463', 'Voyage 046 - Call 3'),
(185, '0464', 'Voyage 046 - Call 4'),
(186, '0471', 'Voyage 047 - Call 1'),
(187, '0472', 'Voyage 047 - Call 2'),
(188, '0473', 'Voyage 047 - Call 3'),
(189, '0474', 'Voyage 047 - Call 4'),
(190, '0481', 'Voyage 048 - Call 1'),
(191, '0482', 'Voyage 048 - Call 2'),
(192, '0483', 'Voyage 048 - Call 3'),
(193, '0484', 'Voyage 048 - Call 4'),
(194, '0491', 'Voyage 049 - Call 1'),
(195, '0492', 'Voyage 049 - Call 2'),
(196, '0493', 'Voyage 049 - Call 3'),
(197, '0494', 'Voyage 049 - Call 4'),
(198, '0501', 'Voyage 050 - Call 1'),
(199, '0502', 'Voyage 050 - Call 2'),
(200, '0503', 'Voyage 050 - Call 3'),
(201, '0504', 'Voyage 050 - Call 4'),
(202, 'TVC', 'Total Voyage Call'),
(203, '0135', 'Voyage 013 - Call 5'),
(204, '0066', 'Voyage 006 - Call 6'),
(205, '0069', 'Voyage 006 - Call 9'),
(206, '0155', 'Voyage 015 - Call 5'),
(207, '0245', 'Voyage 024 - Call 5'),
(208, '0068', 'Voyage 006 - Call 8'),
(209, '0116', 'Voyage 011 - Call 6'),
(210, '0126', 'Voyage 012 - Call 6'),
(211, '0247', 'Voyage 024 - Call 7'),
(212, '0015', 'Voyage 001 - Call 5'),
(213, '0016', 'Voyage 001 - Call 6'),
(214, '0017', 'Voyage 001 - Call 7'),
(215, '0018', 'Voyage 001 - Call 8'),
(216, '0019', 'Voyage 001 - Call 9'),
(217, '0025', 'Voyage 002 - Call 5'),
(218, '0026', 'Voyage 002 - Call 6'),
(219, '0028', 'Voyage 002 - Call 8'),
(220, '0035', 'Voyage 003 - Call 5'),
(221, '0036', 'Voyage 003 - Call 6'),
(222, '0065', 'Voyage 006 - Call 5'),
(223, '0255', 'Voyage 025 - Call 5'),
(224, '0115', 'Voyage 011 - Call 5'),
(225, '0067', 'Voyage 006 - Call 7'),
(226, '0125', 'Voyage 012 - Call 5'),
(227, '0175', 'Voyage 017 - Call 5'),
(228, '0345', 'Voyage 034 - Call 5'),
(229, '0346', 'Voyage 034 - Call 6'),
(230, '0347', 'Voyage 034 - Call 7'),
(231, '0348', 'Voyage 034 - Call 8'),
(232, '0349', 'Voyage 034 - Call 9'),
(233, '0355', 'Voyage 035 - Call 5'),
(234, '0356', 'Voyage 035 - Call 6'),
(235, '0357', 'Voyage 035 - Call 7'),
(236, '0358', 'Voyage 035 - Call 8'),
(237, '0359', 'Voyage 035 - Call 9'),
(238, '0055', 'Voyage 005 - Call 5'),
(239, '0056', 'Voyage 005 - Call 6'),
(240, '0057', 'Voyage 005 - Call 7'),
(241, '0058', 'Voyage 005 - Call 8'),
(242, '0059', 'Voyage 005 - Call 9'),
(243, '0075', 'Voyage 007 - Call 5'),
(244, '0076', 'Voyage 007 - Call 6'),
(245, '0077', 'Voyage 007 - Call 7'),
(246, '0078', 'Voyage 007 - Call 8'),
(247, '0079', 'Voyage 007 - Call 9'),
(248, '0085', 'Voyage 008 - Call 5'),
(249, '0086', 'Voyage 008 - Call 6'),
(250, '0087', 'Voyage 008 - Call 7'),
(251, '0088', 'Voyage 008 - Call 8'),
(252, '0089', 'Voyage 008 - Call 9'),
(253, '0095', 'Voyage 009 - Call 5'),
(254, '0096', 'Voyage 009 - Call 6'),
(255, '0097', 'Voyage 009 - Call 7'),
(256, '0098', 'Voyage 009 - Call 8'),
(257, '0099', 'Voyage 009 - Call 9'),
(258, '0215', 'Voyage 021 - Call 5'),
(259, '0105', 'Voyage 010 - Call 5'),
(260, '0106', 'Voyage 010 - Call 6'),
(261, '0107', 'Voyage 010 - Call 7'),
(262, '0108', 'Voyage 010 - Call 8'),
(263, '0109', 'Voyage 010 - Call 9'),
(264, '0117', 'Voyage 011 - Call 7'),
(265, '0118', 'Voyage 011 - Call 8'),
(266, '0119', 'Voyage 011 - Call 9'),
(267, '0127', 'Voyage 012 - Call 7'),
(268, '0128', 'Voyage 012 - Call 8'),
(269, '0129', 'Voyage 012 - Call 9'),
(270, '0136', 'Voyage 013 - Call 6'),
(271, '0137', 'Voyage 013 - Call 7'),
(272, '0138', 'Voyage 013 - Call 8'),
(273, '0139', 'Voyage 013 - Call 9'),
(274, '0145', 'Voyage 014 - Call 5'),
(275, '0146', 'Voyage 014 - Call 6'),
(276, '0147', 'Voyage 014 - Call 7'),
(277, '0148', 'Voyage 014 - Call 8'),
(278, '0149', 'Voyage 014 - Call 9'),
(279, '0156', 'Voyage 015 - Call 6'),
(280, '0157', 'Voyage 015 - Call 7'),
(281, '0158', 'Voyage 015 - Call 8'),
(282, '0159', 'Voyage 015 - Call 9'),
(283, '0165', 'Voyage 016 - Call 5'),
(284, '0166', 'Voyage 016 - Call 6'),
(285, '0167', 'Voyage 016 - Call 7'),
(286, '0168', 'Voyage 016 - Call 8'),
(287, '0169', 'Voyage 016 - Call 9'),
(288, '0177', 'Voyage 017 - Call 7'),
(289, '0178', 'Voyage 017 - Call 8'),
(290, '0179', 'Voyage 017 - Call 9'),
(291, '0185', 'Voyage 018 - Call 5'),
(292, '0186', 'Voyage 018 - Call 6'),
(293, '0187', 'Voyage 018 - Call 7'),
(294, '0188', 'Voyage 018 - Call 8'),
(295, '0189', 'Voyage 018 - Call 9'),
(296, '0195', 'Voyage 019 - Call 5'),
(297, '0196', 'Voyage 019 - Call 6'),
(298, '0197', 'Voyage 019 - Call 7'),
(299, '0198', 'Voyage 019 - Call 8'),
(300, '0199', 'Voyage 019 - Call 9'),
(301, '0205', 'Voyage 020 - Call 5'),
(302, '0206', 'Voyage 020 - Call 6'),
(303, '0207', 'Voyage 020 - Call 7'),
(304, '0208', 'Voyage 020 - Call 8'),
(305, '0209', 'Voyage 020 - Call 9'),
(306, '0225', 'Voyage 022 - Call 5'),
(307, '0226', 'Voyage 022 - Call 6'),
(308, '0227', 'Voyage 022 - Call 7'),
(309, '0228', 'Voyage 022 - Call 8'),
(310, '0229', 'Voyage 022 - Call 9'),
(311, '0235', 'Voyage 023 - Call 5'),
(312, '0236', 'Voyage 023 - Call 6'),
(313, '0237', 'Voyage 023 - Call 7'),
(314, '0238', 'Voyage 023 - Call 8'),
(315, '0239', 'Voyage 023 - Call 9'),
(316, '0246', 'Voyage 024 - Call 6'),
(317, '0248', 'Voyage 024 - Call 8'),
(318, '0249', 'Voyage 024 - Call 9'),
(319, '0256', 'Voyage 025 - Call 6'),
(320, '0257', 'Voyage 025 - Call 7'),
(321, '0258', 'Voyage 025 - Call 8'),
(322, '0259', 'Voyage 025 - Call 9'),
(323, '0265', 'Voyage 026 - Call 5'),
(324, '0266', 'Voyage 026 - Call 6'),
(325, '0267', 'Voyage 026 - Call 7'),
(326, '0268', 'Voyage 026 - Call 8'),
(327, '0269', 'Voyage 026 - Call 9'),
(328, '0285', 'Voyage 028 - Call 5'),
(329, '0286', 'Voyage 028 - Call 6'),
(330, '0287', 'Voyage 028 - Call 7'),
(331, '0288', 'Voyage 028 - Call 8'),
(332, '0289', 'Voyage 028 - Call 9'),
(333, '0295', 'Voyage 029 - Call 5'),
(334, '0296', 'Voyage 029 - Call 6'),
(335, '0297', 'Voyage 029 - Call 7'),
(336, '0298', 'Voyage 029 - Call 8'),
(337, '0299', 'Voyage 029 - Call 9'),
(338, '0305', 'Voyage 030 - Call 5'),
(339, '0306', 'Voyage 030 - Call 6'),
(340, '0307', 'Voyage 030 - Call 7'),
(341, '0308', 'Voyage 030 - Call 8'),
(342, '0309', 'Voyage 030 - Call 9'),
(343, '0315', 'Voyage 031 - Call 5'),
(344, '0316', 'Voyage 031 - Call 6'),
(345, '0317', 'Voyage 031 - Call 7'),
(346, '0318', 'Voyage 031 - Call 8'),
(347, '0319', 'Voyage 031 - Call 9'),
(348, '0325', 'Voyage 032 - Call 5'),
(349, '0326', 'Voyage 032 - Call 6'),
(350, '0327', 'Voyage 032 - Call 7'),
(351, '0328', 'Voyage 032 - Call 8'),
(352, '0329', 'Voyage 032 - Call 9'),
(353, '0335', 'Voyage 033 - Call 5'),
(354, '0336', 'Voyage 033 - Call 6'),
(355, '0337', 'Voyage 033 - Call 7'),
(356, '0338', 'Voyage 033 - Call 8'),
(357, '0339', 'Voyage 033 - Call 9'),
(358, '0505', 'Voyage 050 - Call 5'),
(359, '0506', 'Voyage 050 - Call 6'),
(360, '0507', 'Voyage 050 - Call 7'),
(361, '0508', 'Voyage 050 - Call 8'),
(362, '0509', 'Voyage 050 - Call 9'),
(363, '0027', 'Voyage 002 - Call 7'),
(364, '0029', 'Voyage 002 - Call 9'),
(365, '0037', 'Voyage 003 - Call 7'),
(366, '0038', 'Voyage 003 - Call 8'),
(367, '0039', 'Voyage 003 - Call 9'),
(368, '0045', 'Voyage 004 - Call 5'),
(369, '0046', 'Voyage 004 - Call 6'),
(370, '0047', 'Voyage 004 - Call 7'),
(371, '0048', 'Voyage 004 - Call 8'),
(372, '0049', 'Voyage 004 - Call 9'),
(373, '0216', 'Voyage 021 - Call 6'),
(374, '0176', 'Voyage 017 - Call 6'),
(375, '0217', 'Voyage 021 - Call 7');

-- --------------------------------------------------------

--
-- Table structure for table `workflow`
--

CREATE TABLE `workflow` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kapalId` bigint(20) UNSIGNED NOT NULL,
  `tipeTransaksi` varchar(2) NOT NULL COMMENT 'PR, PM, MI, IO, RC, MO, MS, MR',
  `urlTransaksi` varchar(100) NOT NULL,
  `namaTransaksi` varchar(100) NOT NULL,
  `refId` varchar(100) NOT NULL,
  `dateUpdated` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dateCreated` datetime DEFAULT CURRENT_TIMESTAMP,
  `requesterId` bigint(20) UNSIGNED NOT NULL,
  `approverId` bigint(20) UNSIGNED DEFAULT NULL,
  `status` tinyint(2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `workflow`
--

INSERT INTO `workflow` (`id`, `kapalId`, `tipeTransaksi`, `urlTransaksi`, `namaTransaksi`, `refId`, `dateUpdated`, `dateCreated`, `requesterId`, `approverId`, `status`) VALUES
(1, 2, 'PR', '', '', '0', '2018-08-02 18:00:43', '2018-07-31 10:12:14', 1, NULL, 1),
(2, 2, 'PR', '', '', '0', '2018-08-02 18:00:43', '2018-07-31 10:44:14', 1, NULL, 1),
(3, 5, 'PR', 'permintaanbarang', 'Permintaan Barang', 'PELNI/PR 18 UMS 000008', '2018-08-01 09:36:38', '2018-07-31 11:26:34', 1, 1, 1),
(6, 5, 'MO', 'pindahsubinv', 'Pindah Barang Sub-Inventory', 'PELNI/MO 18 UMS 000002', '2018-08-06 18:14:16', '2018-08-02 16:36:07', 1, 1, 2),
(7, 5, 'PR', 'permintaanbarang', 'Permintaan Barang', 'PELNI/PR 18 UMS 000009', '2018-08-04 23:08:33', '2018-08-04 23:08:33', 1, NULL, 0),
(8, 5, 'MS', 'penggunaanmakanan', 'Penggunaan Makanan', 'PELNI/MS 18 UMS 000001', '2018-08-06 18:47:15', '2018-08-06 18:00:09', 1, 1, 1),
(10, 5, 'IO', 'transferio', 'Transfer IO', 'PELNI/IO 18 UMS 000001', '2018-08-07 10:15:46', '2018-08-07 03:12:10', 1, NULL, 0),
(11, 5, 'IO', 'transferio', 'Transfer IO', 'PELNI/IO 18 UMS 000002', '2018-08-07 10:19:12', '2018-08-07 03:18:31', 1, NULL, 0),
(12, 5, 'PR', 'permintaanbarang', 'Permintaan Barang', 'PELNI/PR 18 UMS 000010', '2018-08-08 10:49:38', '2018-08-08 10:49:38', 1, NULL, 0),
(13, 5, 'MS', 'penerimaanmakanan', 'Penerimaan Makanan', 'PELNI/MS 18 UMS 000001', '2018-08-08 11:54:53', '2018-08-08 11:54:53', 1, NULL, 0),
(14, 5, 'MS', 'penerimaanmakanan', 'Penerimaan Makanan', 'PELNI/MS 18 UMS 000002', '2018-08-08 12:14:13', '2018-08-08 12:14:13', 1, NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `activitylog`
--
ALTER TABLE `activitylog`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `administratoradministrator`
--
ALTER TABLE `administratoradministrator`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_admin_UNIQUE` (`id`),
  ADD UNIQUE KEY `username_admin_UNIQUE` (`username`),
  ADD UNIQUE KEY `email_admin_UNIQUE` (`email`);

--
-- Indexes for table `administratorgroupaccess`
--
ALTER TABLE `administratorgroupaccess`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `administratormenu`
--
ALTER TABLE `administratormenu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `applicationparameter`
--
ALTER TABLE `applicationparameter`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD UNIQUE KEY `variableName_UNIQUE` (`variableName`);

--
-- Indexes for table `approver`
--
ALTER TABLE `approver`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `itempenerimaanbarang`
--
ALTER TABLE `itempenerimaanbarang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `itempenerimaanbarangio`
--
ALTER TABLE `itempenerimaanbarangio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `itempenerimaanmakanan`
--
ALTER TABLE `itempenerimaanmakanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `itempenggunaanbarang`
--
ALTER TABLE `itempenggunaanbarang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `itempenggunaanmakanan`
--
ALTER TABLE `itempenggunaanmakanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `itempermintaanbarang`
--
ALTER TABLE `itempermintaanbarang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `itempindahbarangio`
--
ALTER TABLE `itempindahbarangio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `itempindahbarangsubinv`
--
ALTER TABLE `itempindahbarangsubinv`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `itempopenerimaan`
--
ALTER TABLE `itempopenerimaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `itempopenerimaanio`
--
ALTER TABLE `itempopenerimaanio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kapal`
--
ALTER TABLE `kapal`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `loginlog`
--
ALTER TABLE `loginlog`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `lokasiitem`
--
ALTER TABLE `lokasiitem`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `penerimaanbarang`
--
ALTER TABLE `penerimaanbarang`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `receiptNumber` (`receiptNumber`);

--
-- Indexes for table `penerimaanbarangio`
--
ALTER TABLE `penerimaanbarangio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penerimaanmakanan`
--
ALTER TABLE `penerimaanmakanan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `penggunaanbarang`
--
ALTER TABLE `penggunaanbarang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penggunaanmakanan`
--
ALTER TABLE `penggunaanmakanan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `permintaanbarang`
--
ALTER TABLE `permintaanbarang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pindahbarangio`
--
ALTER TABLE `pindahbarangio`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `pindahbarangsubinv`
--
ALTER TABLE `pindahbarangsubinv`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `popenerimaan`
--
ALTER TABLE `popenerimaan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `poNumber` (`poNumber`);

--
-- Indexes for table `popenerimaanio`
--
ALTER TABLE `popenerimaanio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `stokkapal`
--
ALTER TABLE `stokkapal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`);

--
-- Indexes for table `voyages`
--
ALTER TABLE `voyages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `workflow`
--
ALTER TABLE `workflow`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `activitylog`
--
ALTER TABLE `activitylog`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `administratoradministrator`
--
ALTER TABLE `administratoradministrator`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `administratorgroupaccess`
--
ALTER TABLE `administratorgroupaccess`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `administratormenu`
--
ALTER TABLE `administratormenu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `applicationparameter`
--
ALTER TABLE `applicationparameter`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `approver`
--
ALTER TABLE `approver`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `itempenerimaanbarang`
--
ALTER TABLE `itempenerimaanbarang`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `itempenerimaanbarangio`
--
ALTER TABLE `itempenerimaanbarangio`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `itempenerimaanmakanan`
--
ALTER TABLE `itempenerimaanmakanan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `itempenggunaanbarang`
--
ALTER TABLE `itempenggunaanbarang`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `itempenggunaanmakanan`
--
ALTER TABLE `itempenggunaanmakanan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `itempermintaanbarang`
--
ALTER TABLE `itempermintaanbarang`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `itempindahbarangio`
--
ALTER TABLE `itempindahbarangio`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `itempindahbarangsubinv`
--
ALTER TABLE `itempindahbarangsubinv`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `itempopenerimaan`
--
ALTER TABLE `itempopenerimaan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `itempopenerimaanio`
--
ALTER TABLE `itempopenerimaanio`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kapal`
--
ALTER TABLE `kapal`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `loginlog`
--
ALTER TABLE `loginlog`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lokasiitem`
--
ALTER TABLE `lokasiitem`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `penerimaanbarang`
--
ALTER TABLE `penerimaanbarang`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `penerimaanbarangio`
--
ALTER TABLE `penerimaanbarangio`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `penerimaanmakanan`
--
ALTER TABLE `penerimaanmakanan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `penggunaanbarang`
--
ALTER TABLE `penggunaanbarang`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `penggunaanmakanan`
--
ALTER TABLE `penggunaanmakanan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `permintaanbarang`
--
ALTER TABLE `permintaanbarang`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `pindahbarangio`
--
ALTER TABLE `pindahbarangio`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pindahbarangsubinv`
--
ALTER TABLE `pindahbarangsubinv`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `popenerimaan`
--
ALTER TABLE `popenerimaan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `popenerimaanio`
--
ALTER TABLE `popenerimaanio`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `stokkapal`
--
ALTER TABLE `stokkapal`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `voyages`
--
ALTER TABLE `voyages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=376;

--
-- AUTO_INCREMENT for table `workflow`
--
ALTER TABLE `workflow`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
