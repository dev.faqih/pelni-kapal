--
-- PostgreSQL database dump
--

-- Dumped from database version 10.4
-- Dumped by pg_dump version 10.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = true;

--
-- Name: account; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.account (
    id bigint NOT NULL,
    "namaAccount" character varying(200) NOT NULL,
    deskripsi character varying(500),
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    "noAccount" bigint
);


ALTER TABLE public.account OWNER TO postgres;

--
-- Name: account_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.account_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_id_seq OWNER TO postgres;

--
-- Name: account_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.account_id_seq OWNED BY public.account.id;


--
-- Name: activitylog; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.activitylog (
    id bigint NOT NULL,
    username character varying DEFAULT 200 NOT NULL,
    "urlTransaksi" character varying(100),
    date timestamp without time zone,
    activity character varying(100) NOT NULL,
    "refId" character varying(100) NOT NULL,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    "kapalId" bigint,
    "namaTransaksi" character varying(100),
    deskripsi character varying(200)
);


ALTER TABLE public.activitylog OWNER TO postgres;

--
-- Name: activitylog_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.activitylog_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.activitylog_id_seq OWNER TO postgres;

--
-- Name: activitylog_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.activitylog_id_seq OWNED BY public.activitylog.id;


--
-- Name: administratoradministrator; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.administratoradministrator (
    id bigint NOT NULL,
    groupid bigint NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    email character varying(45) NOT NULL,
    fullname character varying(255) NOT NULL,
    img text,
    date_updated timestamp without time zone
);


ALTER TABLE public.administratoradministrator OWNER TO postgres;

--
-- Name: administratoradministrator_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.administratoradministrator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.administratoradministrator_id_seq OWNER TO postgres;

--
-- Name: administratoradministrator_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.administratoradministrator_id_seq OWNED BY public.administratoradministrator.id;


--
-- Name: administratorgroupaccess; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.administratorgroupaccess (
    id bigint NOT NULL,
    groupid bigint NOT NULL,
    accessid bigint NOT NULL,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.administratorgroupaccess OWNER TO postgres;

--
-- Name: administratorgroupaccess_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.administratorgroupaccess_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.administratorgroupaccess_id_seq OWNER TO postgres;

--
-- Name: administratorgroupaccess_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.administratorgroupaccess_id_seq OWNED BY public.administratorgroupaccess.id;


--
-- Name: administratormenu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.administratormenu (
    id bigint NOT NULL,
    menuname character varying(45) NOT NULL,
    url character varying(45) NOT NULL,
    parentmenu bigint DEFAULT 0,
    icon character varying(45) NOT NULL,
    description character varying(255),
    "position" smallint,
    active smallint,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.administratormenu OWNER TO postgres;

--
-- Name: administratormenu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.administratormenu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.administratormenu_id_seq OWNER TO postgres;

--
-- Name: administratormenu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.administratormenu_id_seq OWNED BY public.administratormenu.id;


--
-- Name: applicationparameter; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.applicationparameter (
    id bigint NOT NULL,
    "variableName" character varying(200) NOT NULL,
    "variableValue" character varying(2000) NOT NULL,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.applicationparameter OWNER TO postgres;

--
-- Name: applicationparameter_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.applicationparameter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.applicationparameter_id_seq OWNER TO postgres;

--
-- Name: applicationparameter_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.applicationparameter_id_seq OWNED BY public.applicationparameter.id;


--
-- Name: approver; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.approver (
    id bigint NOT NULL,
    "roleId" bigint NOT NULL,
    "formIdApprover" smallint NOT NULL,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.approver OWNER TO postgres;

--
-- Name: approver_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.approver_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.approver_id_seq OWNER TO postgres;

--
-- Name: approver_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.approver_id_seq OWNED BY public.approver.id;


--
-- Name: item; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.item (
    id bigint NOT NULL,
    "namaItem" character varying(200) NOT NULL,
    deskripsi character varying(1000),
    category character varying(100),
    "UOM" character varying(100),
    harga bigint DEFAULT 0,
    image character varying(200),
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    expense_account bigint DEFAULT 51090106,
    destination_type_code character varying(50),
    item_price numeric(16,2),
    "kapalId" bigint,
    expense_account_id bigint
);


ALTER TABLE public.item OWNER TO postgres;

--
-- Name: item_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.item_id_seq OWNER TO postgres;

--
-- Name: item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.item_id_seq OWNED BY public.item.id;


SET default_with_oids = false;

--
-- Name: item_onhand; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.item_onhand (
    "INVENTORY_ITEM_ID" bigint NOT NULL,
    "SEGMENT1" character varying(200),
    "SUBINVENTORY_CODE" character varying(200),
    "ORGANIZATION_ID" bigint,
    "ACCT_PERIOD_ID" bigint,
    "PERIOD_NAME" character varying(40),
    "PERIOD_START_DATE" date,
    "SCHEDULE_CLOSE_DATE" date,
    "QTY" bigint,
    "ID" bigint
);


ALTER TABLE public.item_onhand OWNER TO postgres;

--
-- Name: item_onhand_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.item_onhand_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.item_onhand_id_seq OWNER TO postgres;

--
-- Name: item_onhand_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.item_onhand_id_seq OWNED BY public.item_onhand."ID";


SET default_with_oids = true;

--
-- Name: itempenerimaanbarang; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.itempenerimaanbarang (
    id bigint NOT NULL,
    "penerimaanBarangId" character varying(50) NOT NULL,
    "itemId" bigint NOT NULL,
    "receiptDate" timestamp without time zone,
    quantity bigint NOT NULL,
    "lokasiId" bigint NOT NULL,
    "lotNumber" character varying(100),
    status smallint DEFAULT 0 NOT NULL,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.itempenerimaanbarang OWNER TO postgres;

--
-- Name: itempenerimaanbarang_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.itempenerimaanbarang_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.itempenerimaanbarang_id_seq OWNER TO postgres;

--
-- Name: itempenerimaanbarang_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.itempenerimaanbarang_id_seq OWNED BY public.itempenerimaanbarang.id;


--
-- Name: itempenerimaanbarangio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.itempenerimaanbarangio (
    id bigint NOT NULL,
    "rcNumber" character varying(50) NOT NULL,
    "itemId" bigint NOT NULL,
    "receiptDate" timestamp without time zone,
    quantity bigint NOT NULL,
    "lokasiId" bigint NOT NULL,
    "lotNumber" character varying(100),
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.itempenerimaanbarangio OWNER TO postgres;

--
-- Name: itempenerimaanbarangio_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.itempenerimaanbarangio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.itempenerimaanbarangio_id_seq OWNER TO postgres;

--
-- Name: itempenerimaanbarangio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.itempenerimaanbarangio_id_seq OWNED BY public.itempenerimaanbarangio.id;


--
-- Name: itempenerimaanmakanan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.itempenerimaanmakanan (
    id bigint NOT NULL,
    "mrNumber" character varying(50) NOT NULL,
    "itemId" bigint NOT NULL,
    quantity bigint NOT NULL,
    "lotNumber" character varying(100) NOT NULL,
    "locationId" bigint NOT NULL,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.itempenerimaanmakanan OWNER TO postgres;

--
-- Name: itempenerimaanmakanan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.itempenerimaanmakanan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.itempenerimaanmakanan_id_seq OWNER TO postgres;

--
-- Name: itempenerimaanmakanan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.itempenerimaanmakanan_id_seq OWNED BY public.itempenerimaanmakanan.id;


--
-- Name: itempenggunaanbarang; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.itempenggunaanbarang (
    id bigint NOT NULL,
    "miNumber" character varying(50) NOT NULL,
    "itemId" bigint NOT NULL,
    quantity integer NOT NULL,
    "lotNumber" character varying(100) NOT NULL,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.itempenggunaanbarang OWNER TO postgres;

--
-- Name: itempenggunaanbarang_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.itempenggunaanbarang_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.itempenggunaanbarang_id_seq OWNER TO postgres;

--
-- Name: itempenggunaanbarang_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.itempenggunaanbarang_id_seq OWNED BY public.itempenggunaanbarang.id;


--
-- Name: itempenggunaanmakanan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.itempenggunaanmakanan (
    id bigint NOT NULL,
    "msNumber" character varying(50) NOT NULL,
    "itemId" bigint NOT NULL,
    quantity bigint NOT NULL,
    "lotNumber" character varying(100) NOT NULL,
    "locationId" bigint NOT NULL,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    "stokId" bigint
);


ALTER TABLE public.itempenggunaanmakanan OWNER TO postgres;

--
-- Name: itempenggunaanmakanan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.itempenggunaanmakanan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.itempenggunaanmakanan_id_seq OWNER TO postgres;

--
-- Name: itempenggunaanmakanan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.itempenggunaanmakanan_id_seq OWNED BY public.itempenggunaanmakanan.id;


--
-- Name: itempermintaanbarang; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.itempermintaanbarang (
    id bigint NOT NULL,
    "prNumber" character varying(50) NOT NULL,
    "itemId" bigint NOT NULL,
    "needBy" timestamp without time zone,
    quantity integer NOT NULL,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.itempermintaanbarang OWNER TO postgres;

--
-- Name: itempermintaanbarang_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.itempermintaanbarang_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.itempermintaanbarang_id_seq OWNER TO postgres;

--
-- Name: itempermintaanbarang_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.itempermintaanbarang_id_seq OWNED BY public.itempermintaanbarang.id;


--
-- Name: itempindahbarangio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.itempindahbarangio (
    id bigint NOT NULL,
    "ioNumber" character varying(50) NOT NULL,
    "itemId" bigint NOT NULL,
    "lokasiId" bigint NOT NULL,
    quantity bigint NOT NULL,
    "lotNumber" character varying(100) NOT NULL,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    "stokId" bigint
);


ALTER TABLE public.itempindahbarangio OWNER TO postgres;

--
-- Name: itempindahbarangio_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.itempindahbarangio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.itempindahbarangio_id_seq OWNER TO postgres;

--
-- Name: itempindahbarangio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.itempindahbarangio_id_seq OWNED BY public.itempindahbarangio.id;


--
-- Name: itempindahbarangsubinv; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.itempindahbarangsubinv (
    id bigint NOT NULL,
    "moNumber" character varying(50) NOT NULL,
    "itemId" bigint NOT NULL,
    quantity bigint NOT NULL,
    "lotNumber" character varying(100),
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    "stokId" bigint
);


ALTER TABLE public.itempindahbarangsubinv OWNER TO postgres;

--
-- Name: itempindahbarangsubinv_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.itempindahbarangsubinv_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.itempindahbarangsubinv_id_seq OWNER TO postgres;

--
-- Name: itempindahbarangsubinv_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.itempindahbarangsubinv_id_seq OWNED BY public.itempindahbarangsubinv.id;


--
-- Name: itempopenerimaan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.itempopenerimaan (
    id bigint NOT NULL,
    "poNumber" character varying(50) NOT NULL,
    "itemId" bigint NOT NULL,
    "lokasiId" bigint NOT NULL,
    "receiptDate" timestamp without time zone,
    "dateCreated" timestamp without time zone,
    quantity integer NOT NULL,
    "lotNumber" character varying(100),
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    "qtyRemaining" integer,
    status smallint DEFAULT 0
);


ALTER TABLE public.itempopenerimaan OWNER TO postgres;

--
-- Name: itempopenerimaan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.itempopenerimaan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.itempopenerimaan_id_seq OWNER TO postgres;

--
-- Name: itempopenerimaan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.itempopenerimaan_id_seq OWNED BY public.itempopenerimaan.id;


--
-- Name: itempopenerimaanio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.itempopenerimaanio (
    id bigint NOT NULL,
    "ioNumber" character varying(50) NOT NULL,
    "itemId" bigint NOT NULL,
    "receiptDate" timestamp without time zone,
    quantity integer NOT NULL,
    "lokasiId" bigint NOT NULL,
    "lotNumber" character varying(100),
    status smallint DEFAULT 0 NOT NULL,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    "qtyRemaining" integer
);


ALTER TABLE public.itempopenerimaanio OWNER TO postgres;

--
-- Name: itempopenerimaanio_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.itempopenerimaanio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.itempopenerimaanio_id_seq OWNER TO postgres;

--
-- Name: itempopenerimaanio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.itempopenerimaanio_id_seq OWNED BY public.itempopenerimaanio.id;


--
-- Name: kapal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.kapal (
    id bigint NOT NULL,
    "kodeKapal" character varying(3) NOT NULL,
    "namaKapal" character varying(200) NOT NULL,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.kapal OWNER TO postgres;

--
-- Name: kapal_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.kapal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kapal_id_seq OWNER TO postgres;

--
-- Name: kapal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.kapal_id_seq OWNED BY public.kapal.id;


--
-- Name: loginlog; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.loginlog (
    id bigint NOT NULL,
    "loginStartDate" timestamp(6) with time zone,
    "loginEndDate" timestamp(6) with time zone,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    "kapalId" bigint,
    username character varying(200) NOT NULL
);


ALTER TABLE public.loginlog OWNER TO postgres;

--
-- Name: loginlog_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.loginlog_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.loginlog_id_seq OWNER TO postgres;

--
-- Name: loginlog_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.loginlog_id_seq OWNED BY public.loginlog.id;


--
-- Name: lokasiitem; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.lokasiitem (
    id bigint NOT NULL,
    "namaLokasi" character varying(200) NOT NULL,
    lokasi character varying(200),
    "alamatLokasi" character varying(200),
    "ORGCode" character varying(100),
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    "kapalId" bigint
);


ALTER TABLE public.lokasiitem OWNER TO postgres;

--
-- Name: lokasiitem_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.lokasiitem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lokasiitem_id_seq OWNER TO postgres;

--
-- Name: lokasiitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.lokasiitem_id_seq OWNED BY public.lokasiitem.id;


SET default_with_oids = false;

--
-- Name: lotnumber; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.lotnumber (
    lotnumber character varying(50) NOT NULL,
    refid character varying(50),
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    "kapalId" bigint,
    "itemId" bigint
);


ALTER TABLE public.lotnumber OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: notifications; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.notifications (
    id bigint NOT NULL,
    "userId" bigint NOT NULL,
    "notificationType" smallint NOT NULL,
    "refId" bigint NOT NULL,
    date timestamp without time zone,
    "isRead" smallint NOT NULL,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    "kapalId" bigint
);


ALTER TABLE public.notifications OWNER TO postgres;

--
-- Name: notifications_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.notifications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notifications_id_seq OWNER TO postgres;

--
-- Name: notifications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.notifications_id_seq OWNED BY public.notifications.id;


--
-- Name: penerimaanbarang; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.penerimaanbarang (
    id bigint NOT NULL,
    "receiptNumber" character varying(50) NOT NULL,
    "poNumber" character varying(50) NOT NULL,
    "userId" bigint NOT NULL,
    "kapalId" bigint NOT NULL,
    "locationId" bigint NOT NULL,
    "unitOperasi" character varying(100),
    "tanggalPenerimaan" timestamp without time zone,
    "tanggalPO" timestamp without time zone,
    "qtyDiterima" bigint DEFAULT 0 NOT NULL,
    "voyageKode" character varying(10),
    "voyageStart" timestamp without time zone,
    "voyageEnd" timestamp without time zone,
    status smallint NOT NULL,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.penerimaanbarang OWNER TO postgres;

--
-- Name: penerimaanbarang_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.penerimaanbarang_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.penerimaanbarang_id_seq OWNER TO postgres;

--
-- Name: penerimaanbarang_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.penerimaanbarang_id_seq OWNED BY public.penerimaanbarang.id;


--
-- Name: penerimaanbarangio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.penerimaanbarangio (
    id bigint NOT NULL,
    "rcNumber" character varying(50) NOT NULL,
    "ioNumber" character varying(50) NOT NULL,
    "userId" bigint NOT NULL,
    "kapalId" bigint NOT NULL,
    "locationId" bigint NOT NULL,
    "unitOperasi" character varying(100),
    "tanggalPenerimaan" timestamp without time zone,
    "tanggalPO" timestamp without time zone,
    "qtyDiterima" bigint NOT NULL,
    "voyageKode" character varying(10),
    "voyageStart" timestamp without time zone,
    "voyageEnd" timestamp without time zone,
    status smallint NOT NULL,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.penerimaanbarangio OWNER TO postgres;

--
-- Name: penerimaanbarangio_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.penerimaanbarangio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.penerimaanbarangio_id_seq OWNER TO postgres;

--
-- Name: penerimaanbarangio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.penerimaanbarangio_id_seq OWNED BY public.penerimaanbarangio.id;


--
-- Name: penerimaanmakanan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.penerimaanmakanan (
    id bigint NOT NULL,
    "requesterId" bigint NOT NULL,
    "approverId" bigint,
    "kapalId" bigint NOT NULL,
    "mrNumber" character varying(50) NOT NULL,
    "locationId" bigint NOT NULL,
    type smallint NOT NULL,
    "accountId" bigint NOT NULL,
    date timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    "totalBarang" bigint DEFAULT 0,
    komentar text,
    "voyageKode" character varying(10),
    "voyageStart" timestamp without time zone,
    "voyageEnd" timestamp without time zone,
    status smallint DEFAULT 0 NOT NULL,
    "tglApproved" timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    feedback text
);


ALTER TABLE public.penerimaanmakanan OWNER TO postgres;

--
-- Name: penerimaanmakanan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.penerimaanmakanan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.penerimaanmakanan_id_seq OWNER TO postgres;

--
-- Name: penerimaanmakanan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.penerimaanmakanan_id_seq OWNED BY public.penerimaanmakanan.id;


--
-- Name: penggunaanbarang; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.penggunaanbarang (
    id bigint NOT NULL,
    "miNumber" character varying(50) NOT NULL,
    "requesterId" bigint NOT NULL,
    "approverId" bigint,
    "transactionType" smallint NOT NULL,
    "kapalId" bigint NOT NULL,
    "locationId" bigint NOT NULL,
    "destinationAccount" bigint NOT NULL,
    "tglPenggunaan" timestamp without time zone,
    "jlhBarang" bigint DEFAULT 0 NOT NULL,
    "voyageKode" character varying(10),
    "voyageStart" timestamp without time zone,
    "voyageEnd" timestamp without time zone,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.penggunaanbarang OWNER TO postgres;

--
-- Name: penggunaanbarang_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.penggunaanbarang_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.penggunaanbarang_id_seq OWNER TO postgres;

--
-- Name: penggunaanbarang_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.penggunaanbarang_id_seq OWNED BY public.penggunaanbarang.id;


--
-- Name: penggunaanmakanan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.penggunaanmakanan (
    id bigint NOT NULL,
    "requesterId" bigint NOT NULL,
    "approverId" bigint,
    "kapalId" bigint NOT NULL,
    "msNumber" character varying(50) NOT NULL,
    "locationId" bigint NOT NULL,
    type smallint NOT NULL,
    "accountId" bigint NOT NULL,
    date timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    "totalBarang" bigint DEFAULT 0,
    komentar text,
    "voyageKode" character varying(10),
    "voyageStart" timestamp without time zone,
    "voyageEnd" timestamp without time zone,
    status smallint DEFAULT 0 NOT NULL,
    "tglApproved" timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    feedback text
);


ALTER TABLE public.penggunaanmakanan OWNER TO postgres;

--
-- Name: penggunaanmakanan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.penggunaanmakanan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.penggunaanmakanan_id_seq OWNER TO postgres;

--
-- Name: penggunaanmakanan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.penggunaanmakanan_id_seq OWNED BY public.penggunaanmakanan.id;


--
-- Name: permintaanbarang; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.permintaanbarang (
    id bigint NOT NULL,
    "prNumber" character varying(50) NOT NULL,
    "requesterId" bigint NOT NULL,
    "approverId" bigint,
    "kapalId" bigint NOT NULL,
    "locationId" bigint NOT NULL,
    "destinationType" character varying(200),
    "operatingUnit" character varying(200) NOT NULL,
    organization character varying(200),
    keperluan character varying(200),
    "totalHarga" bigint DEFAULT 0,
    "totalBarang" bigint DEFAULT 0,
    sumber character varying(200),
    komentar text,
    deskripsi character varying(200),
    "tglPermintaan" timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    "tglApproved" timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    "isApproved" smallint NOT NULL,
    "voyageKode" character varying(10),
    "voyageStart" timestamp without time zone,
    "voyageEnd" timestamp without time zone,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    feedback text
);


ALTER TABLE public.permintaanbarang OWNER TO postgres;

--
-- Name: permintaanbarang_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.permintaanbarang_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.permintaanbarang_id_seq OWNER TO postgres;

--
-- Name: permintaanbarang_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.permintaanbarang_id_seq OWNED BY public.permintaanbarang.id;


--
-- Name: pindahbarangio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pindahbarangio (
    id bigint NOT NULL,
    "ioNumber" character varying(50) NOT NULL,
    type smallint DEFAULT 1,
    "requesterId" bigint NOT NULL,
    "approverId" bigint,
    "fromKapalId" bigint NOT NULL,
    "toKapalId" bigint NOT NULL,
    "fromLocationId" bigint NOT NULL,
    "toLocationId" bigint NOT NULL,
    "toOrganization" character varying(200) NOT NULL,
    "jlhBarang" bigint DEFAULT 0 NOT NULL,
    "expectedReceivedDate" timestamp without time zone,
    "tglTransaksi" timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    komentar text,
    status smallint DEFAULT 0 NOT NULL,
    "tglApproved" timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    "voyageKode" character varying(10),
    "voyageStart" timestamp without time zone,
    "voyageEnd" timestamp without time zone,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    feedback text
);


ALTER TABLE public.pindahbarangio OWNER TO postgres;

--
-- Name: pindahbarangio_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pindahbarangio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pindahbarangio_id_seq OWNER TO postgres;

--
-- Name: pindahbarangio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pindahbarangio_id_seq OWNED BY public.pindahbarangio.id;


--
-- Name: pindahbarangsubinv; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pindahbarangsubinv (
    id bigint NOT NULL,
    "moNumber" character varying(50) NOT NULL,
    "kapalId" bigint NOT NULL,
    type smallint NOT NULL,
    "requesterId" bigint NOT NULL,
    "approverId" bigint,
    "fromLocationId" bigint NOT NULL,
    "toLocationId" bigint NOT NULL,
    "transactionType" smallint NOT NULL,
    "voyageId" bigint,
    "voyageStart" timestamp without time zone,
    "voyageEnd" timestamp without time zone,
    "dateRequired" timestamp without time zone,
    quantity bigint NOT NULL,
    "dateCreated" timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    komentar character varying(765),
    status smallint NOT NULL,
    "tglApproved" timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    feedback text
);


ALTER TABLE public.pindahbarangsubinv OWNER TO postgres;

--
-- Name: pindahbarangsubinv_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pindahbarangsubinv_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pindahbarangsubinv_id_seq OWNER TO postgres;

--
-- Name: pindahbarangsubinv_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pindahbarangsubinv_id_seq OWNED BY public.pindahbarangsubinv.id;


--
-- Name: popenerimaan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.popenerimaan (
    id bigint NOT NULL,
    "poNumber" character varying(50) NOT NULL,
    "kapalId" bigint NOT NULL,
    date timestamp without time zone,
    status smallint NOT NULL,
    "operatingUnit" character varying(200),
    quantity character varying(200),
    "qtyReceiptNumber" bigint DEFAULT 0 NOT NULL,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    "vendorId" bigint,
    "vendorName" character varying(200)
);


ALTER TABLE public.popenerimaan OWNER TO postgres;

--
-- Name: popenerimaan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.popenerimaan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.popenerimaan_id_seq OWNER TO postgres;

--
-- Name: popenerimaan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.popenerimaan_id_seq OWNED BY public.popenerimaan.id;


--
-- Name: popenerimaanio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.popenerimaanio (
    id bigint NOT NULL,
    "ioNumber" character varying(50) NOT NULL,
    "kapalId" bigint NOT NULL,
    date timestamp without time zone,
    status smallint NOT NULL,
    "operatingUnit" character varying(200),
    quantity character varying(200),
    "qtyReceiptNumber" bigint DEFAULT 0 NOT NULL,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    "vendorId" bigint,
    "vendorName" character varying(200)
);


ALTER TABLE public.popenerimaanio OWNER TO postgres;

--
-- Name: popenerimaanio_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.popenerimaanio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.popenerimaanio_id_seq OWNER TO postgres;

--
-- Name: popenerimaanio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.popenerimaanio_id_seq OWNED BY public.popenerimaanio.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roles (
    id bigint NOT NULL,
    "roleName" character varying(100) NOT NULL,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    "kapalId" bigint
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


SET default_with_oids = false;

--
-- Name: staging_sync_table; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.staging_sync_table (
    id integer NOT NULL,
    staging_table character varying(25),
    target_table character varying(25),
    target_model character varying(25),
    mappertable character varying(25),
    last_sync timestamp without time zone
);


ALTER TABLE public.staging_sync_table OWNER TO postgres;

--
-- Name: staging_sync_table_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.staging_sync_table_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.staging_sync_table_id_seq OWNER TO postgres;

--
-- Name: staging_sync_table_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.staging_sync_table_id_seq OWNED BY public.staging_sync_table.id;


--
-- Name: stg_pelni_iot_int; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stg_pelni_iot_int (
    transaction_header_id bigint,
    transaction_interface_id bigint,
    source_code character varying(30),
    source_line_id bigint,
    source_header_id bigint,
    process_flag bigint,
    transaction_mode bigint,
    lock_flag bigint,
    last_update_date date,
    creation_date date,
    created_by bigint,
    last_update_login bigint,
    inventory_item_id bigint,
    item_segment1 character varying(40),
    organization_id bigint,
    transaction_quantity bigint,
    transaction_uom character varying(3),
    transaction_date date,
    subinventory_code character varying(10),
    locator_name character varying(2000),
    distribution_account_id bigint,
    transaction_source_type_id bigint,
    transaction_action_id bigint,
    transaction_type_id bigint,
    attribute1 character varying(150),
    attribute2 character varying(150),
    attribute3 character varying(150),
    attribute4 character varying(150),
    attribute5 character varying(150),
    attribute6 character varying(150),
    attribute7 character varying(150),
    attribute8 character varying(150),
    attribute9 character varying(150),
    attribute10 character varying(150),
    transfer_flag character varying(1),
    transfer_date timestamp without time zone,
    error_message character varying(240),
    process_id bigint,
    transaction_number character varying(200),
    to_org_id bigint,
    to_sub_inventory character varying(200),
    "inout" character varying(10),
    to_subinventory character varying(200)
);


ALTER TABLE public.stg_pelni_iot_int OWNER TO postgres;

--
-- Name: stg_pelni_item_onhand_int; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stg_pelni_item_onhand_int (
    organization_id bigint,
    inventory_item_id bigint,
    segment1 character varying(40),
    description character varying(240),
    primary_uom_code character varying(3),
    subinventory_code character varying(10),
    transaction_quantity bigint,
    lot_bigint character varying(80),
    attribute1 character varying(150),
    attribute2 character varying(150),
    attribute3 character varying(150),
    attribute4 character varying(150),
    attribute5 character varying(150),
    attribute6 character varying(150),
    attribute7 character varying(150),
    attribute8 character varying(150),
    attribute9 character varying(150),
    attribute10 character varying(150),
    transfer_flag character varying(1),
    transfer_date timestamp without time zone,
    error_message character varying(240),
    process_id bigint
);


ALTER TABLE public.stg_pelni_item_onhand_int OWNER TO postgres;

--
-- Name: stg_pelni_pr_int; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stg_pelni_pr_int (
    deliver_to_location_code character varying(60),
    deliver_to_location_id bigint,
    deliver_to_requestor_name character varying(240),
    deliver_to_requestor_id bigint,
    authorization_status character varying(25),
    requisition_header_id bigint,
    requisition_type character varying(25),
    creation_date timestamp without time zone,
    created_by bigint,
    quantity bigint,
    unit_price bigint,
    item_id bigint,
    item_segment1 character varying(40),
    item_segment2 character varying(40),
    item_segment3 character varying(40),
    item_segment4 character varying(40),
    item_segment5 character varying(40),
    item_description character varying(240),
    category_id bigint,
    currency_code character varying(15),
    preparer_id bigint,
    preparer_name character varying(240),
    approver_id bigint,
    apprver_name character varying(240),
    group_code character varying(30),
    charge_account_id bigint,
    charge_account_segment1 character varying(25),
    charge_account_segment2 character varying(25),
    charge_account_segment3 character varying(25),
    charge_account_segment4 character varying(25),
    charge_account_segment5 character varying(25),
    charge_account_segment6 character varying(25),
    document_type_code character varying(25),
    req_bigint_segment1 character varying(20),
    source_type_code character varying(25),
    header_description character varying(240),
    batch_id bigint,
    destination_type_code character varying(3),
    destination_organization_id bigint,
    interface_source_code character varying(25),
    unit_of_measure character varying(25),
    need_by_date timestamp without time zone,
    org_id bigint,
    header_attribute_category character varying(30),
    header_attribute1 character varying(150),
    header_attribute2 character varying(150),
    header_attribute3 character varying(150),
    header_attribute4 character varying(150),
    header_attribute5 character varying(150),
    header_attribute6 character varying(150),
    header_attribute7 character varying(150),
    header_attribute8 character varying(150),
    header_attribute9 character varying(150),
    header_attribute10 character varying(150),
    header_attribute11 character varying(150),
    header_attribute12 character varying(150),
    header_attribute13 character varying(150),
    header_attribute14 character varying(150),
    header_attribute15 character varying(150),
    line_attribute_category character varying(30),
    line_attribute1 character varying(150),
    line_attribute2 character varying(150),
    line_attribute3 character varying(150),
    line_attribute4 character varying(150),
    line_attribute5 character varying(150),
    line_attribute6 character varying(150),
    line_attribute7 character varying(150),
    line_attribute8 character varying(150),
    line_attribute9 character varying(150),
    line_attribute10 character varying(150),
    line_attribute11 character varying(150),
    line_attribute12 character varying(150),
    line_attribute13 character varying(150),
    line_attribute14 character varying(150),
    line_attribute15 character varying(150),
    transfer_flag character varying(20),
    transfer_msg character varying(2000),
    transfer_date timestamp without time zone,
    process_id bigint,
    req_number_segment1 character varying(20)
);


ALTER TABLE public.stg_pelni_pr_int OWNER TO postgres;

--
-- Name: stg_pelni_scm_misc_rcv_int; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stg_pelni_scm_misc_rcv_int (
    transaction_header_id bigint,
    transaction_interface_id bigint,
    source_code character varying(30),
    source_line_id bigint,
    source_header_id bigint,
    process_flag bigint,
    transaction_mode bigint,
    lock_flag bigint,
    inventory_item_id bigint,
    item_segment1 character varying(40),
    organization_id bigint,
    transaction_quantity bigint,
    transaction_uom character varying(3),
    transaction_date date,
    subinventory_code character varying(10),
    locator_name character varying(2000),
    distribution_account_id bigint,
    transaction_source_type_id bigint,
    transaction_action_id bigint,
    transaction_type_id bigint,
    transfer_flag character varying(1),
    transfer_date timestamp without time zone,
    error_message character varying(240),
    process_id bigint,
    transaction_number character varying(200),
    type_misc character varying(20),
    attribute2 character varying(150)
);


ALTER TABLE public.stg_pelni_scm_misc_rcv_int OWNER TO postgres;

--
-- Name: stg_pelni_scm_moi_int; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stg_pelni_scm_moi_int (
    transaction_header_id bigint,
    transaction_interface_id bigint,
    source_code character varying(30),
    source_line_id bigint,
    source_header_id bigint,
    process_flag bigint,
    transaction_mode bigint,
    lock_flag bigint,
    inventory_item_id bigint,
    item_segment1 character varying(40),
    organization_id bigint,
    transaction_quantity bigint,
    transaction_uom character varying(3),
    transaction_date date,
    subinventory_code character varying(10),
    locator_name character varying(2000),
    distribution_account_id bigint,
    transaction_source_type_id bigint,
    transaction_action_id bigint,
    transaction_type_id bigint,
    transfer_flag character varying(1),
    transfer_date timestamp without time zone,
    error_message character varying(240),
    process_id bigint,
    transaction_number character varying(200)
);


ALTER TABLE public.stg_pelni_scm_moi_int OWNER TO postgres;

--
-- Name: stg_pelni_scm_mot_int; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stg_pelni_scm_mot_int (
    transaction_header_id bigint,
    transaction_interface_id bigint,
    source_code character varying(30),
    source_line_id bigint,
    source_header_id bigint,
    process_flag bigint,
    transaction_mode bigint,
    lock_flag bigint,
    inventory_item_id bigint,
    item_segment1 character varying(40),
    organization_id bigint,
    transaction_quantity bigint,
    transaction_uom character varying(3),
    transaction_date date,
    subinventory_code character varying(10),
    locator_name character varying(2000),
    distribution_account_id bigint,
    transaction_source_type_id bigint,
    transaction_action_id bigint,
    transaction_type_id bigint,
    transfer_subinventory character varying(20),
    transfer_locator bigint,
    transfer_flag character varying(1),
    transfer_date timestamp without time zone,
    error_message character varying(240),
    process_id bigint,
    transaction_number character varying(200),
    attribute2 character varying(150)
);


ALTER TABLE public.stg_pelni_scm_mot_int OWNER TO postgres;

--
-- Name: stg_pelni_scm_rcv_int; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stg_pelni_scm_rcv_int (
    header_interface_id bigint,
    group_id bigint,
    processing_status_code character varying(25),
    receipt_source_code character varying(25),
    transaction_type character varying(25),
    receipt_num character varying(25),
    vendor_id bigint,
    vendor_name character varying(240),
    validation_flag character varying(1),
    auto_transact_code character varying(25),
    org_id bigint,
    attribute1 character varying(150),
    attribute2 character varying(150),
    attribute3 character varying(150),
    attribute4 character varying(150),
    attribute5 character varying(150),
    attribute6 character varying(150),
    attribute7 character varying(150),
    attribute8 character varying(150),
    attribute9 character varying(150),
    attribute10 character varying(150),
    attribute11 character varying(150),
    attribute12 character varying(150),
    attribute13 character varying(150),
    attribute14 character varying(150),
    attribute15 character varying(150),
    transfer_flag character varying(1),
    transfer_date timestamp without time zone,
    error_message character varying(240),
    process_id bigint,
    po_number character varying(200),
    transaction_number character varying(200),
    creation_date date,
    last_update_date date
);


ALTER TABLE public.stg_pelni_scm_rcv_int OWNER TO postgres;

--
-- Name: stg_pelni_scm_rcv_trx_int; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stg_pelni_scm_rcv_trx_int (
    interface_transaction_id bigint,
    header_interface_id bigint,
    group_id bigint,
    transaction_type character varying(25),
    transaction_date timestamp without time zone,
    processing_status_code character varying(25),
    transaction_status_code character varying(25),
    processing_mode_code character varying(25),
    quantity bigint,
    unit_of_measure character varying(25),
    auto_transact_code character varying(25),
    validation_flag character varying(1),
    to_organization_code character varying(3),
    item_id bigint,
    item_num character varying(81),
    ship_to_location_code character varying(60),
    po_header_id bigint,
    document_num character varying(50),
    po_line_id bigint,
    document_line_num character varying(50),
    transfer_flag character varying(1),
    transfer_date timestamp without time zone,
    error_message character varying(240),
    process_id bigint,
    transaction_number character varying(200),
    creation_date date,
    last_update_date date,
    attribute2 character varying(150)
);


ALTER TABLE public.stg_pelni_scm_rcv_trx_int OWNER TO postgres;

--
-- Name: stg_pelni_ship_int; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stg_pelni_ship_int (
    flex_value character varying(150),
    enabled_flag character varying(1),
    summary_flag character varying(1),
    description character varying(240),
    attribute1 character varying(150),
    attribute2 character varying(150),
    attribute3 character varying(150),
    attribute4 character varying(150),
    attribute5 character varying(150),
    attribute6 character varying(150),
    attribute7 character varying(150),
    attribute8 character varying(150),
    attribute9 character varying(150),
    attribute10 character varying(150),
    transfer_flag character varying(1),
    transfer_date timestamp without time zone,
    error_message character varying(240),
    process_id bigint
);


ALTER TABLE public.stg_pelni_ship_int OWNER TO postgres;

--
-- Name: stg_pelni_system_items_int; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stg_pelni_system_items_int (
    inventory_item_id bigint,
    organization_id bigint,
    enabled_flag character varying(1),
    description character varying(240),
    segment1 character varying(40),
    lot_control_code bigint,
    serial_bigint_control_code bigint,
    primary_uom_code character varying(3),
    inventory_item_status_code character varying(10),
    category_id bigint,
    concatenated_segments character varying(163),
    attribute1 character varying(150),
    attribute2 character varying(150),
    attribute3 character varying(150),
    attribute4 character varying(150),
    attribute5 character varying(150),
    attribute6 character varying(150),
    attribute7 character varying(150),
    attribute8 character varying(150),
    attribute9 character varying(150),
    attribute10 character varying(150),
    transfer_flag character varying(1),
    transfer_date timestamp without time zone,
    error_message character varying(240),
    process_id bigint
);


ALTER TABLE public.stg_pelni_system_items_int OWNER TO postgres;

--
-- Name: stg_pelni_wh_int; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stg_pelni_wh_int (
    organization_id bigint,
    organization_code character varying(3),
    name character varying(240),
    date_from date,
    date_to date,
    location_id bigint,
    location_code character varying(60),
    address_line_1 character varying(240),
    secondary_inventory_name character varying(10),
    description character varying(50),
    attribute1 character varying(150),
    attribute2 character varying(150),
    attribute3 character varying(150),
    attribute4 character varying(150),
    attribute5 character varying(150),
    attribute6 character varying(150),
    attribute7 character varying(150),
    attribute8 character varying(150),
    attribute9 character varying(150),
    attribute10 character varying(150),
    transfer_flag character varying(1),
    transfer_date timestamp without time zone,
    error_message character varying(240),
    process_id bigint
);


ALTER TABLE public.stg_pelni_wh_int OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: stokkapal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stokkapal (
    id bigint NOT NULL,
    "itemId" bigint NOT NULL,
    "kapalId" bigint NOT NULL,
    "lokasiId" bigint NOT NULL,
    jumlah bigint NOT NULL,
    satuan character varying(50),
    status smallint NOT NULL,
    type smallint DEFAULT 1,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    "lotNumber" character varying(50),
    "tglMasuk" timestamp without time zone
);


ALTER TABLE public.stokkapal OWNER TO postgres;

--
-- Name: stokkapal_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.stokkapal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stokkapal_id_seq OWNER TO postgres;

--
-- Name: stokkapal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.stokkapal_id_seq OWNED BY public.stokkapal.id;


SET default_with_oids = false;

--
-- Name: sync; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sync (
    id bigint NOT NULL,
    local_id bigint,
    server_id bigint,
    table_name character varying(255) NOT NULL,
    marked_deleted smallint DEFAULT 0,
    need_sync smallint DEFAULT 1,
    sync_error text,
    sync_date timestamp without time zone,
    last_sync timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.sync OWNER TO postgres;

--
-- Name: sync_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sync_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sync_id_seq OWNER TO postgres;

--
-- Name: sync_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sync_id_seq OWNED BY public.sync.id;


--
-- Name: sync_table; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sync_table (
    id bigint NOT NULL,
    table_name character varying(255),
    last_sync timestamp without time zone
);


ALTER TABLE public.sync_table OWNER TO postgres;

--
-- Name: sync_table_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sync_table_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sync_table_id_seq OWNER TO postgres;

--
-- Name: sync_table_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sync_table_id_seq OWNED BY public.sync_table.id;


SET default_with_oids = true;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying(200) NOT NULL,
    "roleId" bigint NOT NULL,
    "kapalId" bigint NOT NULL,
    username character varying(100) NOT NULL,
    password character varying(100) NOT NULL,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    "lokasiId" character varying(20),
    aplikasi character varying(20) DEFAULT 'KAPAL'::character varying,
    jabatan character varying(100),
    nrp character varying(50)
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: voyages; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.voyages (
    id bigint NOT NULL,
    "voyageKode" character varying(10) DEFAULT '0'::character varying,
    "voyageName" character varying(100) DEFAULT '0'::character varying,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.voyages OWNER TO postgres;

--
-- Name: voyages_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.voyages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.voyages_id_seq OWNER TO postgres;

--
-- Name: voyages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.voyages_id_seq OWNED BY public.voyages.id;


--
-- Name: workflow; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.workflow (
    id bigint NOT NULL,
    "kapalId" bigint NOT NULL,
    "tipeTransaksi" character varying(10) NOT NULL,
    "urlTransaksi" character varying(100) NOT NULL,
    "namaTransaksi" character varying(100) NOT NULL,
    "refId" character varying(100) NOT NULL,
    "dateUpdated" timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    "dateCreated" timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    "requesterId" bigint NOT NULL,
    "approverId" bigint DEFAULT 0,
    status smallint DEFAULT 0 NOT NULL,
    date_updated timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.workflow OWNER TO postgres;

--
-- Name: workflow_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.workflow_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.workflow_id_seq OWNER TO postgres;

--
-- Name: workflow_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.workflow_id_seq OWNED BY public.workflow.id;


--
-- Name: account id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account ALTER COLUMN id SET DEFAULT nextval('public.account_id_seq'::regclass);


--
-- Name: activitylog id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.activitylog ALTER COLUMN id SET DEFAULT nextval('public.activitylog_id_seq'::regclass);


--
-- Name: administratoradministrator id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.administratoradministrator ALTER COLUMN id SET DEFAULT nextval('public.administratoradministrator_id_seq'::regclass);


--
-- Name: administratorgroupaccess id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.administratorgroupaccess ALTER COLUMN id SET DEFAULT nextval('public.administratorgroupaccess_id_seq'::regclass);


--
-- Name: administratormenu id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.administratormenu ALTER COLUMN id SET DEFAULT nextval('public.administratormenu_id_seq'::regclass);


--
-- Name: applicationparameter id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicationparameter ALTER COLUMN id SET DEFAULT nextval('public.applicationparameter_id_seq'::regclass);


--
-- Name: approver id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.approver ALTER COLUMN id SET DEFAULT nextval('public.approver_id_seq'::regclass);


--
-- Name: item id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.item ALTER COLUMN id SET DEFAULT nextval('public.item_id_seq'::regclass);


--
-- Name: item_onhand ID; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.item_onhand ALTER COLUMN "ID" SET DEFAULT nextval('public.item_onhand_id_seq'::regclass);


--
-- Name: itempenerimaanbarang id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.itempenerimaanbarang ALTER COLUMN id SET DEFAULT nextval('public.itempenerimaanbarang_id_seq'::regclass);


--
-- Name: itempenerimaanbarangio id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.itempenerimaanbarangio ALTER COLUMN id SET DEFAULT nextval('public.itempenerimaanbarangio_id_seq'::regclass);


--
-- Name: itempenerimaanmakanan id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.itempenerimaanmakanan ALTER COLUMN id SET DEFAULT nextval('public.itempenerimaanmakanan_id_seq'::regclass);


--
-- Name: itempenggunaanbarang id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.itempenggunaanbarang ALTER COLUMN id SET DEFAULT nextval('public.itempenggunaanbarang_id_seq'::regclass);


--
-- Name: itempenggunaanmakanan id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.itempenggunaanmakanan ALTER COLUMN id SET DEFAULT nextval('public.itempenggunaanmakanan_id_seq'::regclass);


--
-- Name: itempermintaanbarang id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.itempermintaanbarang ALTER COLUMN id SET DEFAULT nextval('public.itempermintaanbarang_id_seq'::regclass);


--
-- Name: itempindahbarangio id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.itempindahbarangio ALTER COLUMN id SET DEFAULT nextval('public.itempindahbarangio_id_seq'::regclass);


--
-- Name: itempindahbarangsubinv id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.itempindahbarangsubinv ALTER COLUMN id SET DEFAULT nextval('public.itempindahbarangsubinv_id_seq'::regclass);


--
-- Name: itempopenerimaan id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.itempopenerimaan ALTER COLUMN id SET DEFAULT nextval('public.itempopenerimaan_id_seq'::regclass);


--
-- Name: itempopenerimaanio id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.itempopenerimaanio ALTER COLUMN id SET DEFAULT nextval('public.itempopenerimaanio_id_seq'::regclass);


--
-- Name: kapal id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kapal ALTER COLUMN id SET DEFAULT nextval('public.kapal_id_seq'::regclass);


--
-- Name: loginlog id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.loginlog ALTER COLUMN id SET DEFAULT nextval('public.loginlog_id_seq'::regclass);


--
-- Name: lokasiitem id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lokasiitem ALTER COLUMN id SET DEFAULT nextval('public.lokasiitem_id_seq'::regclass);


--
-- Name: notifications id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notifications ALTER COLUMN id SET DEFAULT nextval('public.notifications_id_seq'::regclass);


--
-- Name: penerimaanbarang id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.penerimaanbarang ALTER COLUMN id SET DEFAULT nextval('public.penerimaanbarang_id_seq'::regclass);


--
-- Name: penerimaanbarangio id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.penerimaanbarangio ALTER COLUMN id SET DEFAULT nextval('public.penerimaanbarangio_id_seq'::regclass);


--
-- Name: penerimaanmakanan id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.penerimaanmakanan ALTER COLUMN id SET DEFAULT nextval('public.penerimaanmakanan_id_seq'::regclass);


--
-- Name: penggunaanbarang id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.penggunaanbarang ALTER COLUMN id SET DEFAULT nextval('public.penggunaanbarang_id_seq'::regclass);


--
-- Name: penggunaanmakanan id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.penggunaanmakanan ALTER COLUMN id SET DEFAULT nextval('public.penggunaanmakanan_id_seq'::regclass);


--
-- Name: permintaanbarang id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permintaanbarang ALTER COLUMN id SET DEFAULT nextval('public.permintaanbarang_id_seq'::regclass);


--
-- Name: pindahbarangio id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pindahbarangio ALTER COLUMN id SET DEFAULT nextval('public.pindahbarangio_id_seq'::regclass);


--
-- Name: pindahbarangsubinv id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pindahbarangsubinv ALTER COLUMN id SET DEFAULT nextval('public.pindahbarangsubinv_id_seq'::regclass);


--
-- Name: popenerimaan id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.popenerimaan ALTER COLUMN id SET DEFAULT nextval('public.popenerimaan_id_seq'::regclass);


--
-- Name: popenerimaanio id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.popenerimaanio ALTER COLUMN id SET DEFAULT nextval('public.popenerimaanio_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- Name: staging_sync_table id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.staging_sync_table ALTER COLUMN id SET DEFAULT nextval('public.staging_sync_table_id_seq'::regclass);


--
-- Name: stokkapal id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.stokkapal ALTER COLUMN id SET DEFAULT nextval('public.stokkapal_id_seq'::regclass);


--
-- Name: sync id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sync ALTER COLUMN id SET DEFAULT nextval('public.sync_id_seq'::regclass);


--
-- Name: sync_table id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sync_table ALTER COLUMN id SET DEFAULT nextval('public.sync_table_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: voyages id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.voyages ALTER COLUMN id SET DEFAULT nextval('public.voyages_id_seq'::regclass);


--
-- Name: workflow id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workflow ALTER COLUMN id SET DEFAULT nextval('public.workflow_id_seq'::regclass);


--
-- Data for Name: account; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.account VALUES (1, 'Beban Perlengkapan Kapal', 'Beban Perlengkapan Kapal', NULL, 51090106);
INSERT INTO public.account VALUES (2, 'Beban Bahan Bakar', 'Beban Bahan Bakar', NULL, 51040101);
INSERT INTO public.account VALUES (3, 'Beban Pelumas', 'Beban Pelumas', '2018-08-24 10:30:57.428909', 51040103);
INSERT INTO public.account VALUES (4, 'Beban Suku Cadang Kapal', 'Beban Suku Cadang Kapal', '2018-08-24 10:30:57.428909', 51090101);
INSERT INTO public.account VALUES (5, 'Beban Makanan Penumpang', 'Beban Makanan Penumpang', '2018-08-24 10:30:57.428909', 51020101);
INSERT INTO public.account VALUES (6, 'Beban Cetak Tiket/Dokumen', 'Beban Cetak Tiket/Dokumen', '2018-08-24 10:30:57.428909', 52030104);


--
-- Data for Name: activitylog; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.activitylog VALUES (29, 'sioado', 'permintaanbarang', '2018-09-25 02:42:53', 'CREATE', 'PR180177000003', '2018-09-25 09:42:53.202556', 177, 'Permintaan Barang', 'Administrator just <b>created</b> new Permintaan Barang <a href="http://localhost:8000/administrator/permintaanbarang/22">PR180177000003</a>');
INSERT INTO public.activitylog VALUES (27, 'sioado', 'permintaanbarang', '2018-09-24 07:53:04', 'CREATE', 'PR180177000001', '2018-09-24 14:53:04.119059', 177, 'Permintaan Barang', 'Administrator just <b>created</b> new Permintaan Barang <a href="http://localhost:8000/administrator/permintaanbarang/20">PR180177000001</a>');
INSERT INTO public.activitylog VALUES (7, 'sioado', 'roles', '2018-09-04 04:41:08', 'CREATE', '4', '2018-09-04 04:41:08', 5, 'Roles', 'Administrator just <b>created</b> new Role <a href=''http://localhost:8000/administrator/roles/4''>here</a>');
INSERT INTO public.activitylog VALUES (8, 'sioado', 'permintaanbarang', '2018-09-04 06:53:39', 'CREATE', 'PELNI/PR 18 UMS 000005', '2018-09-04 13:53:39.471648', 5, 'Permintaan Barang', 'Administrator just <b>created</b> new Permintaan Barang <a href=''http://localhost:8000/administrator/permintaanbarang?q=prNumber:PELNI/PR 18 UMS 000005''>here</a>');
INSERT INTO public.activitylog VALUES (10, 'sioado', 'penerimaanbarang', '2018-09-04 07:03:28', 'CREATE', 'PELNI/PB 18 UMS 000003', '2018-09-04 14:03:28.011247', 5, 'Penerimaan Barang', 'Administrator just <b>created</b> new Penerimaan Barang <a href=''http://localhost:8000/administrator/penerimaanbarang''>here</a>');
INSERT INTO public.activitylog VALUES (11, 'sioado', 'penerimaanbarang', '2018-09-04 07:10:54', 'CREATE', 'PELNI/PB 18 UMS 000004', '2018-09-04 14:10:54.489806', 5, 'Penerimaan Barang', 'Administrator just <b>created</b> new Penerimaan Barang');
INSERT INTO public.activitylog VALUES (12, 'sioado', 'transferio', '2018-09-04 07:17:30', 'CREATE', 'PELNI/IO 18 UMS 000006', '2018-09-04 14:17:30.622802', 5, 'Transfer IO', 'Administrator just <b>created</b> new Transfer I/O PELNI/IO 18 UMS 000006');
INSERT INTO public.activitylog VALUES (13, 'sioado', 'penerimaanbarang', '2018-09-04 11:58:48', 'CREATE', 'PELNI/PB 18 UMS 000005', '2018-09-04 18:58:48.573299', 5, 'Penerimaan Barang', 'Administrator just <b>created</b> new Penerimaan Barang <a href=''http://localhost:8000/administrator/penerimaanbarang/19''>PELNI/PB 18 UMS 000005</a>');
INSERT INTO public.activitylog VALUES (14, 'sioado', 'users', '2018-09-10 04:07:02', 'UPDATE', '1', '2018-09-10 04:07:02', 5, 'Users', 'Administrator just <b>updated</b> User <a href=''http://localhost:8000/administrator/users/1''>here</a>');
INSERT INTO public.activitylog VALUES (15, 'sioado', 'users', '2018-09-10 04:08:55', 'UPDATE', '1', '2018-09-10 04:08:55', 5, 'Users', 'Administrator just <b>updated</b> User <a href=''http://localhost:8000/administrator/users/1''>here</a>');
INSERT INTO public.activitylog VALUES (16, 'sioado', 'users', '2018-09-10 05:15:05', 'UPDATE', '1', '2018-09-10 05:15:05', 5, 'Users', 'Administrator just <b>updated</b> User <a href=''http://localhost:8000/administrator/users/1''>here</a>');
INSERT INTO public.activitylog VALUES (17, 'sioado', 'penggunaanbarang', '2018-09-12 02:48:37', 'CREATE', 'PELNI/MI 18 UMS 000005', '2018-09-12 09:48:37.072036', 5, 'Penggunaan Barang', 'Administrator just <b>created</b> new Penggunaan Barang <a href=''http://localhost:8000/administrator/penggunaanbarang/14''>PELNI/MI 18 UMS 000005</a>');
INSERT INTO public.activitylog VALUES (20, 'sioado', 'users', '2018-09-20 02:57:26', 'UPDATE', '1', '2018-09-20 02:57:26', 5, 'Users', 'Administrator just <b>updated</b> User <a href=''http://localhost:8000/administrator/users/1''>here</a>');
INSERT INTO public.activitylog VALUES (21, 'sioado', 'users', '2018-09-20 02:57:40', 'UPDATE', '3', '2018-09-20 02:57:40', 5, 'Users', 'Administrator just <b>updated</b> User <a href=''http://localhost:8000/administrator/users/3''>here</a>');
INSERT INTO public.activitylog VALUES (22, 'sioado', 'users', '2018-09-20 03:15:17', 'UPDATE', '3', '2018-09-20 03:15:17', 5, 'Users', 'Administrator just <b>updated</b> User <a href=''http://localhost:8000/administrator/users/3''>here</a>');
INSERT INTO public.activitylog VALUES (23, 'sioado', 'users', '2018-09-20 03:15:27', 'UPDATE', '1', '2018-09-20 03:15:27', 5, 'Users', 'Administrator just <b>updated</b> User <a href=''http://localhost:8000/administrator/users/1''>here</a>');
INSERT INTO public.activitylog VALUES (24, 'sioado', 'permintaanbarang', '2018-09-21 09:10:57', 'CREATE', 'PR180177 000001', '2018-09-21 16:10:57.624686', 177, 'Permintaan Barang', 'Administrator just <b>created</b> new Permintaan Barang <a href=''http://localhost:8000/administrator/permintaanbarang/19''>PR180177 000001</a>');
INSERT INTO public.activitylog VALUES (25, 'sioado', 'transferio', '2018-09-24 03:25:19', 'CREATE', 'IO180177000001', '2018-09-24 10:25:19.341985', 177, 'Transfer IO', 'Administrator just <b>created</b> new Transfer I/O <a href=''http://localhost:8000/administrator/transferio/9''>IO180177000001</a>');
INSERT INTO public.activitylog VALUES (26, 'sioado', 'pindahsubinv', '2018-09-24 07:01:09', 'CREATE', 'MO180177000001', '2018-09-24 14:01:09.557615', 177, 'Pindah Barang Sub-Inventory', 'Administrator just <b>created</b> new Pindah Barang SUb-Inventory <a href=''http://localhost:8000/administrator/pindahsubinv/2''>MO180177000001</a>');
INSERT INTO public.activitylog VALUES (28, 'sioado', 'permintaanbarang', '2018-09-25 02:37:14', 'CREATE', 'PR180177000002', '2018-09-25 09:37:14.756697', 177, 'Permintaan Barang', 'Administrator just <b>created</b> new Permintaan Barang <a href=''http://localhost:8000/administrator/permintaanbarang/21''>PR180177000002</a>');
INSERT INTO public.activitylog VALUES (1, 'sioado', 'lokasiitem', '2018-09-04 03:03:52', 'CREATE', '7', '2018-09-04 03:03:52', 5, 'Sub-Inventory', 'Administrator just created new Sub-Inventory <a href="http://localhost:8000/administrator/lokasiitem/7">here</a>');
INSERT INTO public.activitylog VALUES (2, 'sioado', 'lokasiitem', '2018-09-04 03:10:05', 'UPDATE', '7', '2018-09-04 03:10:05', 5, 'Sub-Inventory', 'Administrator just updated record Sub-Inventory <a href="http://localhost:8000/administrator/lokasiitem/7">here</a>');
INSERT INTO public.activitylog VALUES (3, 'sioado', 'lokasiitem', '2018-09-04 03:10:59', 'UPDATE', '7', '2018-09-04 03:10:59', 5, 'Sub-Inventory', 'Administrator just <b>updated</b> record Sub-Inventory <a href="http://localhost:8000/administrator/lokasiitem/7">here</a>');
INSERT INTO public.activitylog VALUES (4, 'sioado', 'applicationparameter', '2018-09-04 03:17:08', 'UPDATE', '1', '2018-09-04 03:17:08', 5, 'Application Parameters', 'Administrator just <b>updated</b> record Application Parameters <a href="http://localhost:8000/administrator/applicationparameter/1">here</a>');
INSERT INTO public.activitylog VALUES (5, 'sioado', 'users', '2018-09-04 04:09:35', 'CREATE', '3', '2018-09-04 04:09:35', 5, 'Users', 'Administrator just <b>created</b> new User <a href="http://localhost:8000/administrator/users/3">here</a>');
INSERT INTO public.activitylog VALUES (6, 'sioado', 'users/setpassword', '2018-09-04 04:11:56', 'UPDATE', '3', '2018-09-04 04:11:56', 5, 'User''s Password', 'Administrator just <b>updated</b> User''s Password <a href="http://localhost:8000/administrator/users/setpassword/3">here</a>');
INSERT INTO public.activitylog VALUES (38, 'sioado', NULL, '2018-09-25 07:00:03', 'APPROVAL', 'MR180177000002', '2018-09-25 14:00:03.462108', 177, 'Workflow', 'Administrator just <b>approved</b> transaction Penerimaan Makanan <b>#MR180177000002</b>');
INSERT INTO public.activitylog VALUES (35, 'sioado', 'penggunaanbarang', '2018-09-25 03:51:32', 'CREATE', 'MI180177000001', '2018-09-25 10:51:32.366262', 177, 'Penggunaan Barang', 'Administrator just <b>created</b> new Penggunaan Barang <a href="http://localhost:8000/administrator/penggunaanbarang/15">MI180177000001</a>');
INSERT INTO public.activitylog VALUES (36, 'sioado', 'penerimaanmakanan', '2018-09-25 04:45:21', 'CREATE', 'MR180177000001', '2018-09-25 11:45:21.672284', 177, 'Penerimaan Makanan', 'Administrator just <b>created</b> new Misc. Receipt <a href="http://localhost:8000/administrator/penerimaanmakanan/3">MR180177000001</a>');
INSERT INTO public.activitylog VALUES (37, 'sioado', 'penerimaanmakanan', '2018-09-25 06:41:53', 'CREATE', 'MR180177000002', '2018-09-25 13:41:53.176646', 177, 'Penerimaan Makanan', 'Administrator just <b>created</b> new Misc. Receipt <a href="http://localhost:8000/administrator/penerimaanmakanan/4">MR180177000002</a>');
INSERT INTO public.activitylog VALUES (39, 'sioado', 'penggunaanmakanan', '2018-09-25 07:01:31', 'CREATE', 'MS180177000001', '2018-09-25 14:01:31.560168', 177, 'Penggunaan Makanan', 'Administrator just <b>created</b> new Mics. Issue <a href="http://localhost:8000/administrator/penggunaanmakanan/6">MS180177000001</a>');
INSERT INTO public.activitylog VALUES (40, 'sioado', 'penggunaanmakanan', '2018-09-25 07:02:26', 'CREATE', 'MS180177000002', '2018-09-25 14:02:26.608736', 177, 'Penggunaan Makanan', 'Administrator just <b>created</b> new Mics. Issue <a href="http://localhost:8000/administrator/penggunaanmakanan/7">MS180177000002</a>');
INSERT INTO public.activitylog VALUES (42, 'sioado', 'penerimaanbarang', '2018-09-26 07:54:36', 'CREATE', 'PB180177000001', '2018-09-26 14:54:36.223793', 177, 'Penerimaan Barang', 'Administrator just <b>created</b> new Penerimaan Barang <a href="http://localhost:8000/administrator/penerimaanbarang/1">PB180177000001</a>');
INSERT INTO public.activitylog VALUES (43, 'sioado', 'penerimaanbarang', '2018-09-26 08:09:45', 'CREATE', 'PB180177000002', '2018-09-26 15:09:45.010987', 177, 'Penerimaan Barang', 'Administrator just <b>created</b> new Penerimaan Barang <a href="http://localhost:8000/administrator/penerimaanbarang/2">PB180177000002</a>');
INSERT INTO public.activitylog VALUES (44, 'sioado', 'penerimaanbarang', '2018-09-26 08:15:27', 'CREATE', 'PB180177000003', '2018-09-26 15:15:27.847028', 177, 'Penerimaan Barang', 'Administrator just <b>created</b> new Penerimaan Barang <a href="http://localhost:8000/administrator/penerimaanbarang/3">PB180177000003</a>');
INSERT INTO public.activitylog VALUES (45, 'sioado', 'penerimaanbarang', '2018-09-26 08:17:06', 'CREATE', 'PB180177000004', '2018-09-26 15:17:06.80115', 177, 'Penerimaan Barang', 'Administrator just <b>created</b> new Penerimaan Barang <a href="http://localhost:8000/administrator/penerimaanbarang/4">PB180177000004</a>');
INSERT INTO public.activitylog VALUES (46, 'sioado', 'penerimaanbarang', '2018-09-26 08:34:15', 'CREATE', 'PB180177000005', '2018-09-26 15:34:15.19491', 177, 'Penerimaan Barang', 'Administrator just <b>created</b> new Penerimaan Barang <a href="http://localhost:8000/administrator/penerimaanbarang/5">PB180177000005</a>');
INSERT INTO public.activitylog VALUES (47, 'sioado', 'roles', '2018-09-26 09:48:53', 'UPDATE', '1', '2018-09-26 09:48:53', 177, 'Roles', 'Administrator just <b>updated</b> Role <a href="http://localhost:8000/administrator/roles/1">here</a>');
INSERT INTO public.activitylog VALUES (48, 'sioado', 'permintaanbarang', '2018-09-30 10:39:04', 'CREATE', 'PR180177000004', '2018-09-30 10:39:04.334454', 177, 'Permintaan Barang', 'Administrator just <b>created</b> new Permintaan Barang <a href="http://localhost:8000/administrator/permintaanbarang/23">PR180177000004</a>');
INSERT INTO public.activitylog VALUES (49, 'sioado', NULL, '2018-09-30 10:45:46', 'APPROVAL', 'PR180177000004', '2018-09-30 10:45:46.124547', 177, 'Workflow', 'Administrator just <b>approved</b> transaction Permintaan Barang <b>#PR180177000004</b>');
INSERT INTO public.activitylog VALUES (50, 'sioado', 'permintaanbarang', '2018-09-30 16:53:13', 'CREATE', 'PR180177000005', '2018-09-30 16:53:13.672825', 177, 'Permintaan Barang', 'Administrator just <b>created</b> new Permintaan Barang <a href="http://localhost:8000/administrator/permintaanbarang/24">PR180177000005</a>');
INSERT INTO public.activitylog VALUES (51, 'sioado', 'transferio', '2018-09-30 16:56:05', 'CREATE', 'IO180177000002', '2018-09-30 16:56:05.346869', 177, 'Transfer IO', 'Administrator just <b>created</b> new Transfer I/O <a href="http://localhost:8000/administrator/transferio/10">IO180177000002</a>');
INSERT INTO public.activitylog VALUES (52, 'sioado', 'pindahsubinv', '2018-09-30 20:00:42', 'CREATE', 'MO180177000002', '2018-09-30 20:00:42.076002', 177, 'Pindah Barang Sub-Inventory', 'Administrator just <b>created</b> new Pindah Barang Sub-Inventory <a href="http://localhost:8000/administrator/pindahsubinv/3">MO180177000002</a>');
INSERT INTO public.activitylog VALUES (53, 'sioado', NULL, '2018-09-30 20:00:54', 'APPROVAL', 'MO180177000002', '2018-09-30 20:00:54.887229', 177, 'Workflow', 'Administrator just <b>approved</b> transaction Permintaan Barang <b>#MO180177000002</b>');
INSERT INTO public.activitylog VALUES (54, 'sioado', NULL, '2018-09-30 20:05:55', 'APPROVAL', 'IO180177000002', '2018-09-30 20:05:55.896517', 177, 'Workflow', 'Administrator just <b>approved</b> transaction Transaction IO <b>#IO180177000002</b>');
INSERT INTO public.activitylog VALUES (55, 'sioado', 'penggunaanbarang', '2018-09-30 20:06:28', 'CREATE', 'MI180177000002', '2018-09-30 20:06:28.45499', 177, 'Penggunaan Barang', 'Administrator just <b>created</b> new Penggunaan Barang <a href="http://localhost:8000/administrator/penggunaanbarang/16">MI180177000002</a>');
INSERT INTO public.activitylog VALUES (56, 'sioado', 'penerimaanmakanan', '2018-09-30 20:10:00', 'CREATE', 'MR180177000003', '2018-09-30 20:10:00.194915', 177, 'Penerimaan Makanan', 'Administrator just <b>created</b> new Misc. Receipt <a href="http://localhost:8000/administrator/penerimaanmakanan/5">MR180177000003</a>');
INSERT INTO public.activitylog VALUES (57, 'sioado', NULL, '2018-09-30 20:13:39', 'APPROVAL', 'MR180177000003', '2018-09-30 20:13:38.998261', 177, 'Workflow', 'Administrator just <b>approved</b> transaction Penerimaan Makanan <b>#MR180177000003</b>');
INSERT INTO public.activitylog VALUES (58, 'sioado', NULL, '2018-09-30 20:21:07', 'APPROVAL', 'MS180177000002', '2018-09-30 20:21:07.249743', 177, 'Workflow', 'Administrator just <b>approved</b> transaction Penggunaan Makanan <b>#MS180177000002</b>');
INSERT INTO public.activitylog VALUES (59, 'sioado', 'permintaanbarang', '2018-10-01 14:45:34', 'CREATE', 'PR180177000006', '2018-10-01 14:45:34.220572', 177, 'Permintaan Barang', 'Administrator just <b>created</b> new Permintaan Barang <a href="http://localhost:8000/administrator/permintaanbarang/25">PR180177000006</a>');
INSERT INTO public.activitylog VALUES (60, 'sioado', 'permintaanbarang', '2018-10-01 16:39:12', 'CREATE', 'PR180177000007', '2018-10-01 16:39:12.005853', 177, 'Permintaan Barang', 'Administrator just <b>created</b> new Permintaan Barang <a href="http://localhost:8000/administrator/permintaanbarang/26">PR180177000007</a>');
INSERT INTO public.activitylog VALUES (61, 'sioado', 'permintaanbarang', '2018-10-01 16:54:21', 'CREATE', 'PR180177000008', '2018-10-01 16:54:21.527981', 177, 'Permintaan Barang', 'Administrator just <b>created</b> new Permintaan Barang <a href="http://localhost:8000/administrator/permintaanbarang/27">PR180177000008</a>');
INSERT INTO public.activitylog VALUES (62, 'sioado', 'permintaanbarang', '2018-10-01 16:57:40', 'CREATE', 'PR180177000009', '2018-10-01 16:57:40.732821', 177, 'Permintaan Barang', 'Administrator just <b>created</b> new Permintaan Barang <a href="http://localhost:8000/administrator/permintaanbarang/28">PR180177000009</a>');
INSERT INTO public.activitylog VALUES (63, 'sioado', 'permintaanbarang', '2018-10-01 17:10:01', 'CREATE', 'PR180177000010', '2018-10-01 17:10:01.780586', 177, 'Permintaan Barang', 'Administrator just <b>created</b> new Permintaan Barang <a href="http://localhost:8000/administrator/permintaanbarang/29">PR180177000010</a>');
INSERT INTO public.activitylog VALUES (64, 'sioado', 'permintaanbarang', '2018-10-01 17:11:39', 'CREATE', 'PR180177000011', '2018-10-01 17:11:39.807117', 177, 'Permintaan Barang', 'Administrator just <b>created</b> new Permintaan Barang <a href="http://localhost:8000/administrator/permintaanbarang/30">PR180177000011</a>');
INSERT INTO public.activitylog VALUES (65, 'sioado', NULL, '2018-10-02 11:32:30', 'APPROVAL', 'PR180177000011', '2018-10-02 11:32:30.932222', 177, 'Workflow', 'Administrator just <b>approved</b> transaction Permintaan Barang <b>#PR180177000011</b>');
INSERT INTO public.activitylog VALUES (66, 'sioado', 'penerimaanbarang', '2018-10-02 13:24:42', 'CREATE', 'PB180177000006', '2018-10-02 13:24:41.79474', 177, 'Penerimaan Barang', 'Administrator just <b>created</b> new Penerimaan Barang <a href="http://localhost:8000/administrator/penerimaanbarang/6">PB180177000006</a>');
INSERT INTO public.activitylog VALUES (67, 'sioado', 'penerimaanbarang', '2018-10-02 13:27:02', 'CREATE', 'PB180177000007', '2018-10-02 13:27:01.965301', 177, 'Penerimaan Barang', 'Administrator just <b>created</b> new Penerimaan Barang <a href="http://localhost:8000/administrator/penerimaanbarang/7">PB180177000007</a>');
INSERT INTO public.activitylog VALUES (68, 'sioado', 'penerimaanbarang', '2018-10-02 13:31:17', 'CREATE', 'PB180177000008', '2018-10-02 13:31:17.605021', 177, 'Penerimaan Barang', 'Administrator just <b>created</b> new Penerimaan Barang <a href="http://localhost:8000/administrator/penerimaanbarang/8">PB180177000008</a>');
INSERT INTO public.activitylog VALUES (69, 'sioado', 'permintaanbarang', '2018-10-02 14:57:55', 'CREATE', 'PR180177000012', '2018-10-02 14:57:55.41997', 177, 'Permintaan Barang', 'Administrator just <b>created</b> new Permintaan Barang <a href="http://localhost:8000/administrator/permintaanbarang/31">PR180177000012</a>');
INSERT INTO public.activitylog VALUES (70, 'sioado', NULL, '2018-10-02 15:05:13', 'APPROVAL', 'PR180177000012', '2018-10-02 15:05:13.79288', 177, 'Workflow', 'Administrator just <b>approved</b> transaction Permintaan Barang <b>#PR180177000012</b>');
INSERT INTO public.activitylog VALUES (71, 'sioado', 'permintaanbarang', '2018-10-02 15:30:11', 'CREATE', 'PR180177000013', '2018-10-02 15:30:11.805754', 177, 'Permintaan Barang', 'Administrator just <b>created</b> new Permintaan Barang <a href="http://localhost:8000/administrator/permintaanbarang/32">PR180177000013</a>');


--
-- Data for Name: administratoradministrator; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.administratoradministrator VALUES (1, 1, 'root', '$2y$12$U5BsEAXwbNHZgvSZx.Bqre8aXUgzLzNShrJ.3hXQZMcFSQh/unDP2', 'admin@admin.id', 'Administrator', 'NULL', NULL);


--
-- Data for Name: administratorgroupaccess; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.administratorgroupaccess VALUES (16, 4, 30, '2018-09-04 11:41:08.575146');
INSERT INTO public.administratorgroupaccess VALUES (17, 4, 46, '2018-09-04 11:41:08.575146');
INSERT INTO public.administratorgroupaccess VALUES (18, 4, 16, '2018-09-04 11:41:08.575146');
INSERT INTO public.administratorgroupaccess VALUES (19, 4, 12, '2018-09-04 11:41:08.575146');
INSERT INTO public.administratorgroupaccess VALUES (20, 4, 34, '2018-09-04 11:41:08.575146');
INSERT INTO public.administratorgroupaccess VALUES (21, 4, 36, '2018-09-04 11:41:08.575146');
INSERT INTO public.administratorgroupaccess VALUES (22, 4, 2, '2018-09-04 11:41:08.575146');
INSERT INTO public.administratorgroupaccess VALUES (23, 4, 40, '2018-09-04 11:41:08.575146');
INSERT INTO public.administratorgroupaccess VALUES (24, 4, 38, '2018-09-04 11:41:08.575146');
INSERT INTO public.administratorgroupaccess VALUES (25, 4, 59, '2018-09-04 11:41:08.575146');
INSERT INTO public.administratorgroupaccess VALUES (26, 4, 58, '2018-09-04 11:41:08.575146');
INSERT INTO public.administratorgroupaccess VALUES (27, 4, 54, '2018-09-04 11:41:08.575146');
INSERT INTO public.administratorgroupaccess VALUES (28, 4, 52, '2018-09-04 11:41:08.575146');
INSERT INTO public.administratorgroupaccess VALUES (29, 4, 51, '2018-09-04 11:41:08.575146');
INSERT INTO public.administratorgroupaccess VALUES (30, 4, 50, '2018-09-04 11:41:08.575146');
INSERT INTO public.administratorgroupaccess VALUES (31, 4, 49, '2018-09-04 11:41:08.575146');
INSERT INTO public.administratorgroupaccess VALUES (32, 4, 48, '2018-09-04 11:41:08.575146');
INSERT INTO public.administratorgroupaccess VALUES (33, 4, 10, '2018-09-04 11:41:08.575146');
INSERT INTO public.administratorgroupaccess VALUES (34, 1, 34, '2018-09-26 16:48:53.721046');
INSERT INTO public.administratorgroupaccess VALUES (35, 1, 30, '2018-09-26 16:48:53.721046');
INSERT INTO public.administratorgroupaccess VALUES (36, 1, 46, '2018-09-26 16:48:53.721046');
INSERT INTO public.administratorgroupaccess VALUES (37, 1, 16, '2018-09-26 16:48:53.721046');
INSERT INTO public.administratorgroupaccess VALUES (38, 1, 12, '2018-09-26 16:48:53.721046');
INSERT INTO public.administratorgroupaccess VALUES (39, 1, 60, '2018-09-26 16:48:53.721046');
INSERT INTO public.administratorgroupaccess VALUES (40, 1, 36, '2018-09-26 16:48:53.721046');
INSERT INTO public.administratorgroupaccess VALUES (41, 1, 2, '2018-09-26 16:48:53.721046');
INSERT INTO public.administratorgroupaccess VALUES (42, 1, 40, '2018-09-26 16:48:53.721046');
INSERT INTO public.administratorgroupaccess VALUES (43, 1, 38, '2018-09-26 16:48:53.721046');
INSERT INTO public.administratorgroupaccess VALUES (44, 1, 59, '2018-09-26 16:48:53.721046');
INSERT INTO public.administratorgroupaccess VALUES (45, 1, 58, '2018-09-26 16:48:53.721046');
INSERT INTO public.administratorgroupaccess VALUES (46, 1, 54, '2018-09-26 16:48:53.721046');
INSERT INTO public.administratorgroupaccess VALUES (47, 1, 52, '2018-09-26 16:48:53.721046');
INSERT INTO public.administratorgroupaccess VALUES (48, 1, 51, '2018-09-26 16:48:53.721046');
INSERT INTO public.administratorgroupaccess VALUES (49, 1, 50, '2018-09-26 16:48:53.721046');
INSERT INTO public.administratorgroupaccess VALUES (50, 1, 49, '2018-09-26 16:48:53.721046');
INSERT INTO public.administratorgroupaccess VALUES (51, 1, 48, '2018-09-26 16:48:53.721046');
INSERT INTO public.administratorgroupaccess VALUES (52, 1, 10, '2018-09-26 16:48:53.721046');
INSERT INTO public.administratorgroupaccess VALUES (53, 1, 18, '2018-09-26 16:48:53.721046');
INSERT INTO public.administratorgroupaccess VALUES (54, 1, 61, '2018-10-02 05:08:29.621717');
INSERT INTO public.administratorgroupaccess VALUES (55, 1, 62, '2018-10-02 05:10:23.198955');


--
-- Data for Name: administratormenu; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.administratormenu VALUES (1, 'Master', '', 0, 'fa fa-industry', 'NULL', 1, 1, NULL);
INSERT INTO public.administratormenu VALUES (2, 'Activitiy Logs', 'administrator/activitylog', 42, 'fa fa-circle-o', 'NULL', 31, 1, NULL);
INSERT INTO public.administratormenu VALUES (10, 'Permintaan Barang', 'administrator/permintaanbarang', 44, 'fa fa-bullhorn', 'NULL', 51, 1, NULL);
INSERT INTO public.administratormenu VALUES (12, 'Application Parameter', 'administrator/applicationparameter', 1, 'fa fa-circle-o', 'NULL', 14, 1, NULL);
INSERT INTO public.administratormenu VALUES (14, 'Item Penerimaan Barang', 'administrator/itempenerimaanbarang', 44, 'fa fa-laptop', 'NULL', 52, 0, NULL);
INSERT INTO public.administratormenu VALUES (16, 'Kapal', 'administrator/kapal', 1, 'fa fa-bookmark-o', 'NULL', 11, 1, NULL);
INSERT INTO public.administratormenu VALUES (26, 'Penerimaan Barang', 'administrator/penerimaanbarang', 44, 'fa fa-gear', 'NULL', 53, 0, NULL);
INSERT INTO public.administratormenu VALUES (28, 'Item Permintaan Barang', 'administrator/itempermintaanbarang', 44, 'fa fa-laptop', 'NULL', 54, 0, NULL);
INSERT INTO public.administratormenu VALUES (32, 'Penyesuaian Stok', 'administrator/penyesuaianstok', 41, 'fa fa-laptop', 'NULL', 22, 0, NULL);
INSERT INTO public.administratormenu VALUES (36, 'Login Log', 'administrator/loginlog', 42, 'fa fa-industry', 'NULL', 32, 1, NULL);
INSERT INTO public.administratormenu VALUES (38, 'Roles', 'administrator/roles', 43, 'fa fa-laptop', 'NULL', 42, 1, NULL);
INSERT INTO public.administratormenu VALUES (40, 'Users', 'administrator/users', 43, 'fa fa-gear', 'NULL', 41, 1, NULL);
INSERT INTO public.administratormenu VALUES (42, 'Logs', '', 0, 'fa fa-industry', 'NULL', 3, 1, NULL);
INSERT INTO public.administratormenu VALUES (43, 'Management Access', '', 0, 'fa fa-users', 'NULL', 4, 1, NULL);
INSERT INTO public.administratormenu VALUES (44, 'Transactions', '', 0, 'fa fa-pie-chart', 'NULL', 5, 1, NULL);
INSERT INTO public.administratormenu VALUES (47, 'Ga Tau', '', 0, 'fa fa-book', 'NULL', 6, 1, NULL);
INSERT INTO public.administratormenu VALUES (48, 'Transfer IO', 'administrator/transferio', 44, 'fa fa-anchor', 'NULL', 61, 1, NULL);
INSERT INTO public.administratormenu VALUES (49, 'Pindah Stok Subinv', 'administrator/pindahsubinv', 44, 'fa fa-balance-scale', 'NULL', 64, 1, NULL);
INSERT INTO public.administratormenu VALUES (50, 'Penggunaan Barang', 'administrator/penggunaanbarang', 44, 'fa fa-automobile', 'NULL', 66, 1, NULL);
INSERT INTO public.administratormenu VALUES (51, 'Penggunaan Makanan', 'administrator/penggunaanmakanan', 44, 'fa fa-birthday-cake', 'NULL', 69, 1, NULL);
INSERT INTO public.administratormenu VALUES (52, 'PO Penerimaan', 'administrator/popenerimaan', 44, 'fa fa-bomb', 'NULL', 56, 1, NULL);
INSERT INTO public.administratormenu VALUES (53, 'Item PO Penerimaan', 'administrator/itempopenerimaan', 44, 'fa fa-gear', 'NULL', 57, 0, NULL);
INSERT INTO public.administratormenu VALUES (54, 'Penerimaan IO', 'administrator/popenerimaanio', 44, 'fa fa-binoculars', 'NULL', 62, 1, NULL);
INSERT INTO public.administratormenu VALUES (55, 'Item PO Penerimaan IO', 'administrator/itempopenerimaanio', 44, 'fa fa-gear', 'NULL', 57, 0, NULL);
INSERT INTO public.administratormenu VALUES (56, 'Penerimaan Barang IO', 'administrator/penerimaanbarangio', 44, 'fa fa-gear', 'NULL', 53, 0, NULL);
INSERT INTO public.administratormenu VALUES (57, 'Item Penerimaan Barang IO', 'administrator/itempenerimaanbarangio', 44, 'fa fa-laptop', 'NULL', 52, 0, NULL);
INSERT INTO public.administratormenu VALUES (58, 'Workflow', 'administrator/workflow', 44, 'fa fa-cab', 'NULL', 70, 1, NULL);
INSERT INTO public.administratormenu VALUES (59, 'Penerimaan Makanan', 'administrator/penerimaanmakanan', 44, 'fa fa-coffee', 'NULL', 68, 1, NULL);
INSERT INTO public.administratormenu VALUES (46, 'Items', 'administrator/item', 1, 'fa fa-gears', 'NULL', 12, 1, NULL);
INSERT INTO public.administratormenu VALUES (30, 'Sub-Inventory', 'administrator/lokasiitem', 1, 'fa fa-industry', 'NULL', 13, 1, NULL);
INSERT INTO public.administratormenu VALUES (34, 'Stok Kapal', 'administrator/stokkapal', 1, 'fa fa-pie-chart', 'NULL', 21, 1, NULL);
INSERT INTO public.administratormenu VALUES (41, 'Report', '', 0, 'fa fa-gear', 'NULL', 2, 1, NULL);
INSERT INTO public.administratormenu VALUES (18, 'Rekapitulasi Layanan Makan', 'administrator/rekapitulasimakan', 41, 'fa fa-bookmark-o', 'NULL', 24, 1, NULL);
INSERT INTO public.administratormenu VALUES (61, 'Log Sync Kapal', 'administrator/logsynckapal', 42, 'fa fa-ship', NULL, 34, 1, '2018-10-02 05:07:41.8754');
INSERT INTO public.administratormenu VALUES (62, 'Log Sync Oracle', 'administrator/logsyncoracle', 42, 'fa fa-windows', NULL, 35, 1, '2018-10-02 05:15:58.077377');
INSERT INTO public.administratormenu VALUES (20, 'Pindah Stok', 'administrator/pindahstokkapal', 41, 'fa fa-circle-o', 'NULL', 23, 0, NULL);
INSERT INTO public.administratormenu VALUES (60, 'Sync Logs', 'administrator/sync', 42, 'fa fa-music', NULL, 33, 0, '2018-09-26 09:38:37.08756');


--
-- Data for Name: applicationparameter; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.applicationparameter VALUES (1, 'LAST_LOT_NUMBER', '16', NULL);


--
-- Data for Name: approver; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: item; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.item VALUES (41660, 'E.SIT.017.19.00.000', 'LAYANAN DISASTER RECOVERY CENTER', 'PELNI EXP', 'YR', 0, NULL, '2018-09-21 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41644, 'E.SIT.014.16.00.000', 'LAYANAN MAINTENANCE PERANGKAT & APLIKASI KAPAL PERINTIS', 'PELNI EXP', 'YR', 0, NULL, '2018-09-21 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41643, 'E.SIT.013.15.00.000', 'LAYANAN MAINTENANCE CABANG & KAPAL', 'PELNI EXP', 'YR', 0, NULL, '2018-09-21 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41642, 'E.SIT.012.14.00.000', 'LAYANAN MANAGED SERVICE HELPDESK TI', 'PELNI EXP', 'YR', 0, NULL, '2018-09-21 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41659, 'E.SIT.011.13.00.000', 'LAYANAN MANAGED SERVICE BACKBONE GEDUNG KANTOR PUSAT', 'PELNI EXP', 'YR', 0, NULL, '2018-09-21 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41658, 'E.SIT.010.12.00.000', 'LAYANAN MANAGED SERVICE PERANGKAT SERVER DATA CENTER', 'PELNI EXP', 'YR', 0, NULL, '2018-09-21 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41657, 'E.SIT.009.11.00.000', 'LAYANAN MANAGED SERVICE DATA CENTER CO-LOCATION', 'PELNI EXP', 'YR', 0, NULL, '2018-09-21 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41656, 'E.SIT.008.10.00.000', 'LAYANAN SEAT MANAGEMENT 4', 'PELNI EXP', 'YR', 0, NULL, '2018-09-21 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41655, 'E.SIT.007.09.00.000', 'LAYANAN SEAT MANAGEMENT 3', 'PELNI EXP', 'YR', 0, NULL, '2018-09-21 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41654, 'E.SIT.006.08.00.000', 'LAYANAN SEAT MANAGEMENT 2', 'PELNI EXP', 'YR', 0, NULL, '2018-09-21 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41653, 'E.SIT.005.07.00.000', 'LAYANAN SEAT MANAGEMENT 1', 'PELNI EXP', 'YR', 0, NULL, '2018-09-21 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41652, 'E.SIT.004.06.00.000', 'LAYANAN MAINTENANCE APLIKASI ORACLE EBS FINANCE', 'PELNI EXP', 'YR', 0, NULL, '2018-09-21 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41651, 'E.SIT.003.05.00.000', 'LAYANAN ACCESS POINT NAME (APN) PELNI', 'PELNI EXP', 'YR', 0, NULL, '2018-09-21 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41650, 'E.SIT.002.04.00.000', 'LAYANAN PEMELIHARAAAN & PENGEMBANGAN APLIKASI ORACLE', 'PELNI EXP', 'YR', 0, NULL, '2018-09-21 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41649, 'E.SIT.001.03.00.000', 'LAYANAN PEREMAJAAN PERANGKAT JARINGAN', 'PELNI EXP', 'YR', 0, NULL, '2018-09-21 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41648, 'E.SIT.001.02.00.000', 'LAYANAN INTERNET', 'PELNI EXP', 'YR', 0, NULL, '2018-09-21 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41647, 'E.SIT.001.01.00.000', 'LAYANAN JARINGAN PELNI NET', 'PELNI EXP', 'YR', 0, NULL, '2018-09-21 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41645, 'E.SIT.015.17.00.000', 'LAYANAN SISTEM TELKOMUNIKASI DI ATAS KAPAL', 'PELNI EXP', 'YR', 0, NULL, '2018-09-21 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41482, 'A.SFW.001.02.02.000', 'OPERATING SYSTEM LINUX UBUNTU SERVER 18.04 LTS', 'PELNI FA', 'PCS', 0, NULL, '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41567, 'A.HDW.006.01.08.000', 'ROUTER LINKSYS LRT224', 'PELNI FA', 'UNT', 0, 'upload/items/79dbcaff44e9c6a6df519a0ac3050b17.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41618, 'A.HDW.012.01.01.000', 'KABEL LISTRIK SUPREME 2X1,5 SQMM 50 METER', 'PELNI FA', 'ROL', 0, 'upload/items/29ea207f2686f3f26c6f73660520640a.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41619, 'A.HDW.012.02.02.000', 'KABEL LISTRIK PRIMA 2X1,5 SQMM 50 METER', 'PELNI FA', 'ROL', 0, 'upload/items/faccc7ba1e6bd0d12d121aec86427310.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41620, 'A.HDW.012.03.03.000', 'KABEL LISTRIK ETERNA 2X1,5 SQMM 50 METER', 'PELNI FA', 'ROL', 0, 'upload/items/7d22f50548217d882472531d2971553d.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41621, 'A.HDW.015.01.01.000', 'KABEL JARINGAN BELDEN 7814A008 1000FT', 'PELNI FA', 'ROL', 0, 'upload/items/27256b4afc5f3eef783f83b19a77e271.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41622, 'A.HDW.015.02.02.000', 'KABEL JARINGAN AMP COMSSCOPE BLUE', 'PELNI FA', 'ROL', 0, 'upload/items/0ade80d975c0adf640a80cf7af9aafc2.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41433, 'A.HDW.024.01.01.000', 'SCANNER BARCODE ZEBRA DS4208', 'PELNI FA', 'UNT', 0, 'upload/items/23150295b295ca06d34d9348b01e7620.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41434, 'A.HDW.025.01.01.000', 'SCANNER EPSON DS6500', 'PELNI FA', 'UNT', 0, 'upload/items/f81633ed3345c298e4d829a48e664975.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41435, 'A.HDW.025.02.02.000', 'SCANNER EPSON DS7500', 'PELNI FA', 'UNT', 0, 'upload/items/684c23de54bbd8c22458fd1661868d51.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41437, 'A.HDW.027.01.01.000', 'DVD EKSTERNAL LENOVO THINKPAD ULTRASLIM USB', 'PELNI FA', 'PCS', 0, 'upload/items/2fcef3ebd8ca6d28020ef73ebe98ecf1.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41438, 'A.HDW.028.01.01.000', 'PHOTO COPY EPSON WORKFORCE PRO WF-C869R', 'PELNI FA', 'UNT', 0, 'upload/items/70d21e70499a3cca025ea7431ddd11de.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41439, 'A.HDW.028.02.01.000', 'PHOTO COPY CANON IR 1024', 'PELNI FA', 'UNT', 0, 'upload/items/d1581fb0521a99c810fb5c8f2b23e29e.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41440, 'A.HDW.028.02.02.000', 'PHOTO COPY CANON IR 1435', 'PELNI FA', 'UNT', 0, 'upload/items/eb17028f60d5ded5dc5f99f15a931f98.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41441, 'A.HDW.029.01.01.000', 'PAPER SHREDDER KOZURE KS-9630MC', 'PELNI FA', 'UNT', 0, 'upload/items/c664b75b0085d23aad9d907b44186d7c.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41442, 'A.HDW.030.01.01.000', 'FIREWALL FORTIGATE 1500D', 'PELNI FA', 'UNT', 0, 'upload/items/4fc560f03da8dd14895a9a8a505ec165.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41443, 'A.HDW.030.01.02.000', 'FIREWALL FORTINATE 200D', 'PELNI FA', 'UNT', 0, 'upload/items/b96f2497f4e86cced8eb10066713b924.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41444, 'A.HDW.031.01.01.000', 'PABX ALCATEL OMNIPCX ENTERPRISE TELEPHONE SYSTEMS', 'PELNI FA', 'UNT', 0, 'upload/items/ceecfcfa955218fa55f5c3295d7857e4.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41445, 'A.HDW.032.01.01.000', 'KVM SWITCH ATEN CS1708A', 'PELNI FA', 'UNT', 0, 'upload/items/c3fbe3aa74ff4d6f491c644dd0a640d2.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41446, 'A.HDW.032.01.02.000', 'KVM SWITCH ATEN CS9138', 'PELNI FA', 'UNT', 0, 'upload/items/c1fe94f85f527bc8f4ca6640dc227611.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41448, 'A.HDW.033.01.01.000', 'IP-PHONE CISCO SPA303-G2', 'PELNI FA', 'UNT', 0, 'upload/items/685e81532e9c5873f195910525999f50.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41449, 'A.HDW.034.01.01.000', 'MONITOR LENOVO 19.5 INCH', 'PELNI FA', 'PCS', 0, 'upload/items/1e5083bdc310ea88e44ed7533f6f9b39.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41461, 'A.HDW.035.02.09.000', 'PART SERVER LENOVO X3550 M5 8X2.5'' SATADOM ASSY KIT', 'PELNI FA', 'PCS', 0, NULL, '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41481, 'A.SFW.001.02.01.000', 'OPERATING SYSTEM LINUX UBUNTU SERVER 16.04 LTS', 'PELNI FA', 'PCS', 0, NULL, '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41483, 'A.SFW.001.02.03.000', 'OPERATING SYSTEM LINUX CENTOS 5', 'PELNI FA', 'PCS', 0, NULL, '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41484, 'A.SFW.001.02.04.000', 'OPERATING SYSTEM LINUX CENTOS 6', 'PELNI FA', 'PCS', 0, NULL, '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41485, 'A.SFW.001.02.05.000', 'OPERATING SYSTEM LINUX CENTOS 7', 'PELNI FA', 'PCS', 0, NULL, '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41486, 'A.SFW.001.02.06.000', 'OPERATING SYSTEM LINUX RED HAT ENTERPRISE LINUX 4', 'PELNI FA', 'PCS', 0, NULL, '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41487, 'A.SFW.001.02.07.000', 'OPERATING SYSTEM LINUX RED HAT ENTERPRISE LINUX 5', 'PELNI FA', 'PCS', 0, NULL, '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41488, 'A.SFW.001.02.08.000', 'OPERATING SYSTEM LINUX ORACLE LINUX 6', 'PELNI FA', 'PCS', 0, NULL, '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41451, 'A.HDW.035.01.02.000', 'PART SERVER INTEL X520 DUAL PORT 10GBE SFP+ ADAPTER', 'PELNI FA', 'PCS', 0, 'upload/items/fc3b8f537bc6bf37ad4a7aa5a2c27924.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41452, 'A.HDW.035.01.03.000', 'PART SERVER INTEL XEON PROCESSOR E5-2650 V4 12C 2.2GHZ', 'PELNI FA', 'PCS', 0, 'upload/items/6798891cef91ea78809120669273b13e.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41454, 'A.HDW.035.02.02.000', 'PART SERVER LENOVO 16GB TRUDDR4 MEMORY (2RX4, 1.2V) PC4-19200 CL17 2400MHZ LP RDIMM', 'PELNI FA', 'PCS', 0, 'upload/items/c414ce56e08d3d31076b247d9b73fa76.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41455, 'A.HDW.035.02.03.000', 'PART SERVER LENOVO SYSTEM X 900W HIGH EFFICIENCY PLATINUM AC POWER SUPPLY', 'PELNI FA', 'PCS', 0, 'upload/items/465a1c289b885648f8a6736e9474fafa.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41456, 'A.HDW.035.02.04.000', 'PART SERVER LENOVO SYSTEM X3550 M5 PCLE RISER CARD 2, L-2 CPU (LP X16 CPUI R LP X16 CPU0)', 'PELNI FA', 'PCS', 0, 'upload/items/b67ff8da251c23067f834e04b98df32e.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41457, 'A.HDW.035.02.05.000', 'PART SERVER LENOVO SYSTEM X3550 M5 PCLE RISER CARD I (H LP X16 CPU0)', 'PELNI FA', 'PCS', 0, 'upload/items/b67ff8da251c23067f834e04b98df32e.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41458, 'A.HDW.035.02.06.000', 'PART SERVER LENOVO SYSTEM X3550 M5 FRONT IO CAGE ADVANCED', 'PELNI FA', 'PCS', 0, 'upload/items/d34cdc9417a9e09a6ac72fa4719467d0.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41459, 'A.HDW.035.02.07.000', 'PART SERVER LENOVO SYSTEM X3550 M5 4X 2.5" HS HDD KIT', 'PELNI FA', 'PCS', 0, 'upload/items/08cf9804bdb689354f512adb8017dfd8.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41460, 'A.HDW.035.02.08.000', 'PART SERVER LENOVO SYSTEM X3550 M5 4X 2.5" HS HDD KIT PLUS', 'PELNI FA', 'PCS', 0, 'upload/items/bc7cb04151ae635a051d396a40fdad4e.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41462, 'A.HDW.035.02.10.000', 'PART SERVER LENOVO X3550 M5 MLK PLANAR', 'PELNI FA', 'PCS', 0, 'upload/items/6b1773fb33811985ae59dc79f61e9e6a.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41463, 'A.HDW.035.02.11.000', 'PART SERVER LENOVO SYSTEM X ADVANCED LCD LIGHT PATH KIT', 'PELNI FA', 'PCS', 0, 'upload/items/6b1773fb33811985ae59dc79f61e9e6a.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41465, 'A.HDW.035.02.13.000', 'PART SERVER LENOVO SYSTEM X ENTERPRISE 1U CABLE MANAGEMENT ARM (CMA)', 'PELNI FA', 'PCS', 0, 'upload/items/2a6752cdd37104459bc51000992e6ac9.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41466, 'A.SFW.001.01.01.000', 'OPERATING SYSTEM MICROSOFT WINDOWS XP PROFESSIONAL', 'PELNI FA', 'PCS', 0, 'upload/items/c8d886bd88237557f9c01932d5844a75.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41467, 'A.SFW.001.01.02.000', 'OPERATING SYSTEM MICROSOFT WINDOWS VISTA BUSINESS', 'PELNI FA', 'PCS', 0, 'upload/items/05af9ed49e0024354c6bfad3a31489c4.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41468, 'A.SFW.001.01.03.000', 'OPERATING SYSTEM MICROSOFT WINDOWS 7 PROFESSIONAL', 'PELNI FA', 'PCS', 0, 'upload/items/54f363fe7c4b94551b7297cfb32af138.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41469, 'A.SFW.001.01.04.000', 'OPERATING SYSTEM MICROSOFT WINDOWS 7 ULTIMATE', 'PELNI FA', 'PCS', 0, 'upload/items/e7f74c5fc35ae06da0a422f9ed6fcc60.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41470, 'A.SFW.001.01.05.000', 'OPERATING SYSTEM MICROSOFT WINDOWS 8 PRO', 'PELNI FA', 'PCS', 0, 'upload/items/9905c384c60b41fbd748dab30b430523.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41471, 'A.SFW.001.01.06.000', 'OPERATING SYSTEM MICROSOFT WINDOWS 8.1 PRO', 'PELNI FA', 'PCS', 0, 'upload/items/d54cba7921aaab4014481eee6290f2eb.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41472, 'A.SFW.001.01.07.000', 'OPERATING SYSTEM MICROSOFT WINDOWS 10 PRO', 'PELNI FA', 'PCS', 0, 'upload/items/20b2a566aef2853cafc81e8f330fca8d.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41473, 'A.SFW.001.01.08.000', 'OPERATING SYSTEM MICROSOFT WINDOWS 10 ENTERPRISE', 'PELNI FA', 'PCS', 0, 'upload/items/bdb3fe0dd964d7086984b827e549b6d6.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41474, 'A.SFW.001.01.09.000', 'OPERATING SYSTEM MICROSOFT WINDOWS SERVER STANDARD 2008', 'PELNI FA', 'PCS', 0, 'upload/items/f615db52abadf6c229b0e0e2deadfef4.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41475, 'A.SFW.001.01.10.000', 'OPERATING SYSTEM MICROSOFT WINDOWS SERVER ENTERPRISE 2008', 'PELNI FA', 'PCS', 0, 'upload/items/60b86b02d830ee1c958670318bbc27d5.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41476, 'A.SFW.001.01.11.000', 'OPERATING SYSTEM MICROSOFT WINDOWS WEB SERVER 2008', 'PELNI FA', 'PCS', 0, 'upload/items/0c4b8dd6635b39aa1759fab8d81c5896.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41478, 'A.SFW.001.01.13.000', 'OPERATING SYSTEM MICROSOFT WINDOWS SERVER 2012 ESSENTIALS', 'PELNI FA', 'PCS', 0, 'upload/items/c363b3817fffd91a112623c992e70cef.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41479, 'A.SFW.001.01.14.000', 'OPERATING SYSTEM MICROSOFT WINDOWS SERVER 2016 STANDARD', 'PELNI FA', 'PCS', 0, 'upload/items/3a1fb405022d973e52b3e9bd6c089876.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41480, 'A.SFW.001.01.15.000', 'OPERATING SYSTEM MICROSOFT WINDOWS SERVER 2016 ESSENTIALS', 'PELNI FA', 'PCS', 0, 'upload/items/5d753e2914a77cc7d1fbb03c6cb7c365.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41489, 'A.SFW.002.01.01.000', 'APPLICATION MICROSOFT OFFICE STANDARD 2007', 'PELNI FA', 'PCS', 0, NULL, '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41490, 'A.SFW.002.01.02.000', 'APPLICATION MICROSOFT VISIO STANDARD 2007', 'PELNI FA', 'PCS', 0, NULL, '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41491, 'A.SFW.002.01.03.000', 'APPLICATION MICROSOFT OFFICE STANDARD 2010', 'PELNI FA', 'PCS', 0, NULL, '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41492, 'A.SFW.002.01.04.000', 'APPLICATION MICROSOFT VISIO STANDARD 2010', 'PELNI FA', 'PCS', 0, NULL, '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41493, 'A.SFW.002.01.05.000', 'APPLICATION MICROSOFT OFFICE STANDARD 2013', 'PELNI FA', 'PCS', 0, NULL, '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41494, 'A.SFW.002.01.06.000', 'APPLICATION MICROSOFT VISIO STANDARD 2013', 'PELNI FA', 'PCS', 0, NULL, '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41495, 'A.SFW.002.01.07.000', 'APPLICATION MICROSOFT OFFICE STANDARD 2016', 'PELNI FA', 'PCS', 0, NULL, '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41496, 'A.SFW.002.01.08.000', 'APPLICATION MICROSOFT VISIO STANDARD 2016', 'PELNI FA', 'PCS', 0, NULL, '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41497, 'A.SFW.002.01.09.000', 'APPLICATION MICROSOFT OFFICE STANDARD 2019', 'PELNI FA', 'PCS', 0, NULL, '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41498, 'A.SFW.002.01.10.000', 'APPLICATION MICROSOFT VISIO STANDARD 2019', 'PELNI FA', 'PCS', 0, NULL, '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41499, 'A.SFW.002.02.01.000', 'APPLICATION TEAMVIEWER TEAMVIEWER 13', 'PELNI FA', 'PCS', 0, NULL, '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41500, 'A.SFW.002.03.01.000', 'APPLICATION SYMANTEC PROTECTION SUITE ENTERPRISE EDITION 4', 'PELNI FA', 'PCS', 0, NULL, '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41501, 'A.SFW.002.03.02.000', 'APPLICATION SYMANTEC ENDPOINT PROTECTION 12.1', 'PELNI FA', 'PCS', 0, NULL, '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41502, 'A.SFW.002.04.01.000', 'APPLICATION NUTANIX SW STACK ON NUTANIX ACROPOLIS', 'PELNI FA', 'PCS', 0, NULL, '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41503, 'A.SFW.002.04.02.000', 'APPLICATION NUTANIX PRO EDITION', 'PELNI FA', 'PCS', 0, NULL, '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (42001, '8WFS-C60011', 'Faster Ballpoint C-600 (Blue)', 'FG', 'PCS', 0, NULL, '2018-09-26 00:00:00', 62900199, 'INVENTORY', NULL, 101, 2479);
INSERT INTO public.item VALUES (43001, '8WFS-C60012', 'Faster Ballpoint C-600 (Blue)', 'FG', 'PCS', 0, NULL, '2018-09-27 00:00:00', 62900199, 'INVENTORY', NULL, 101, 2479);
INSERT INTO public.item VALUES (41282, 'I.MSN.001.01.05.0002', 'CAT HEMPEL HEMPATEX ENAMEL WHITE 56360-10000', 'PELNI SPAREPART', 'L', 0, NULL, '2018-09-25 00:00:00', 62900199, 'INVENTORY', NULL, 101, 2479);
INSERT INTO public.item VALUES (41565, 'A.HDW.006.01.06.000', 'ROUTER JUNIPER SRX100', 'PELNI FA', 'UNT', 0, 'upload/items/5dbe6f2c2ef3d1e874a7794f7271f1b8.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41566, 'A.HDW.006.01.07.000', 'ROUTER IBM SAN04B-R', 'PELNI FA', 'UNT', 0, 'upload/items/eb82bdedd79d91fc58ff77ab182b0e5e.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41436, 'A.HDW.026.01.01.000', 'WIFI ADAPTER TP-LINK WDN4800', 'PELNI FA', 'PCS', 0, 'upload/items/e618a8c46d84807469f373ce4121f121.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41447, 'A.HDW.032.02.01.000', 'KVM SWITCH ALTUSEN KH0116', 'PELNI FA', 'UNT', 0, 'upload/items/322f0865f1a0c4bd94b8f18ea9567d13.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41450, 'A.HDW.035.01.01.000', 'PART SERVER INTEL INTEL S3610 480GB ENTERPRISE MAINSTREAM SATA G3HS 2.5'' SSD', 'PELNI FA', 'PCS', 0, 'upload/items/f3e805b17a94ff3ef8e18d900f26049f.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41453, 'A.HDW.035.02.01.000', 'PART SERVER LENOVO N2215 SAS/SATA HBA', 'PELNI FA', 'PCS', 0, 'upload/items/74cbe65a9aa504a2d2e050b64143675a.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41464, 'A.HDW.035.02.12.000', 'PART SERVER LENOVO 2.8M, 13A/125-10A/250V, C13 TO IEC 320-C14 RACK POWER CABLE', 'PELNI FA', 'PCS', 0, 'upload/items/0993072a4a36442fc8f1448dd41daac7.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);
INSERT INTO public.item VALUES (41477, 'A.SFW.001.01.12.000', 'OPERATING SYSTEM MICROSOFT WINDOWS SERVER 2012 STANDARD', 'PELNI FA', 'PCS', 0, 'upload/items/87e3f9f4d13697e16068099826f858b3.jpg', '2018-09-25 00:00:00', 62900199, 'EXPENSE', NULL, 101, 2479);


--
-- Data for Name: item_onhand; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: itempenerimaanbarang; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.itempenerimaanbarang VALUES (30, 'PB180177000001', 2, '2018-09-19 00:00:00', 10, 4, '10', 0, '2018-09-26 14:54:36.223793');
INSERT INTO public.itempenerimaanbarang VALUES (31, 'PB180177000001', 1, '2018-09-19 00:00:00', 8, 1, '10', 0, '2018-09-26 14:54:36.223793');
INSERT INTO public.itempenerimaanbarang VALUES (32, 'PB180177000002', 1, '2018-09-19 00:00:00', 12, 4, '10', 0, '2018-09-26 15:09:45.010987');
INSERT INTO public.itempenerimaanbarang VALUES (33, 'PB180177000003', 1, '2018-09-19 00:00:00', 4, 4, '10', 0, '2018-09-26 15:15:27.847028');
INSERT INTO public.itempenerimaanbarang VALUES (34, 'PB180177000004', 1, '2018-09-12 00:00:00', 6, 7, '10', 0, '2018-09-26 15:17:06.80115');
INSERT INTO public.itempenerimaanbarang VALUES (39, 'PB180177000005', 1, '2018-09-27 00:00:00', 8, 1, '10', 0, '2018-09-26 15:34:15.19491');
INSERT INTO public.itempenerimaanbarang VALUES (40, 'PB180177000008', 41642, '2018-10-25 00:00:00', 10, 1, 'HOW18000016', 0, '2018-10-02 13:31:17.605021');


--
-- Data for Name: itempenerimaanbarangio; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.itempenerimaanbarangio VALUES (5, 'PELNI/RC 18 UMS 000001', 4, '2018-08-23 00:00:00', 5, 4, '5', '2018-08-23 13:03:26.825221');
INSERT INTO public.itempenerimaanbarangio VALUES (6, 'PELNI/RC 18 UMS 000001', 8, '2018-08-23 00:00:00', 10, 3, '10', '2018-08-23 13:03:26.825221');


--
-- Data for Name: itempenerimaanmakanan; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.itempenerimaanmakanan VALUES (1, 'PELNI/MR 18 UMS 000001', 1, 10, '10', 1, '2018-08-20 11:09:40.513');
INSERT INTO public.itempenerimaanmakanan VALUES (2, 'PELNI/MR 18 UMS 000001', 2, 11, '11', 2, '2018-08-20 11:09:52.554');
INSERT INTO public.itempenerimaanmakanan VALUES (3, 'PELNI/MR 18 UMS 000002', 9, 10, '10', 3, NULL);
INSERT INTO public.itempenerimaanmakanan VALUES (4, 'PELNI/MR 18 UMS 000002', 1, 30, '13', 6, NULL);
INSERT INTO public.itempenerimaanmakanan VALUES (5, 'MR180177000001', 5, 20, 'undefined', 3, '2018-09-25 11:45:21.672284');
INSERT INTO public.itempenerimaanmakanan VALUES (6, 'MR180177000001', 8, 90, 'undefined', 3, '2018-09-25 11:45:21.672284');
INSERT INTO public.itempenerimaanmakanan VALUES (7, 'MR180177000001', 7, 100, 'undefined', 4, '2018-09-25 11:45:21.672284');
INSERT INTO public.itempenerimaanmakanan VALUES (8, 'MR180177000002', 1, 30, 'undefined', 4, '2018-09-25 13:41:53.176646');
INSERT INTO public.itempenerimaanmakanan VALUES (9, 'MR180177000002', 10, 80, 'undefined', 7, '2018-09-25 13:41:53.176646');
INSERT INTO public.itempenerimaanmakanan VALUES (10, 'MR180177000003', 3, 100, 'HOW18000012', 3, '2018-09-30 20:10:00.194915');
INSERT INTO public.itempenerimaanmakanan VALUES (11, 'MR180177000003', 1, 200, 'HOW18000013', 7, '2018-09-30 20:10:00.194915');


--
-- Data for Name: itempenggunaanbarang; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.itempenggunaanbarang VALUES (1, 'PELNI/MI 18 UMS 000003', 5, 91, '91', '2018-08-24 20:08:30.526488');
INSERT INTO public.itempenggunaanbarang VALUES (2, 'PELNI/MI 18 UMS 000003', 2, 3, '5', '2018-08-24 20:08:30.526488');
INSERT INTO public.itempenggunaanbarang VALUES (11, 'PELNI/MI 18 UMS 000004', 5, 91, '91', '2018-08-24 20:19:01.764487');
INSERT INTO public.itempenggunaanbarang VALUES (12, 'PELNI/MI 18 UMS 000004', 2, 3, '25', '2018-08-24 20:19:01.764487');
INSERT INTO public.itempenggunaanbarang VALUES (14, 'PELNI/MI 18 UMS 000005', 8, 6, '34', '2018-09-12 09:48:37.072036');
INSERT INTO public.itempenggunaanbarang VALUES (15, 'PELNI/MI 18 UMS 000005', 8, 6, '34', '2018-09-12 09:48:37.072036');
INSERT INTO public.itempenggunaanbarang VALUES (26, 'MI180177000001', 8, 7, '34', '2018-09-25 10:51:32.366262');
INSERT INTO public.itempenggunaanbarang VALUES (27, 'MI180177000001', 8, 10, '10', '2018-09-25 10:51:32.366262');
INSERT INTO public.itempenggunaanbarang VALUES (28, 'MI180177000002', 3, 8, '3', '2018-09-30 20:06:28.45499');


--
-- Data for Name: itempenggunaanmakanan; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.itempenggunaanmakanan VALUES (1, 'PELNI/MS 18 UMS 000001', 11, 10, '23', 4, NULL, NULL);
INSERT INTO public.itempenggunaanmakanan VALUES (2, 'PELNI/MS 18 UMS 000002', 11, 2, '2', 3, NULL, NULL);
INSERT INTO public.itempenggunaanmakanan VALUES (3, 'PELNI/MS 18 UMS 000002', 9, 9, '99', 6, NULL, NULL);
INSERT INTO public.itempenggunaanmakanan VALUES (4, 'PELNI/MS 18 UMS 000003', 9, 5, '45', 2, '2018-08-23 18:36:38.940478', 10);
INSERT INTO public.itempenggunaanmakanan VALUES (5, 'PELNI/MS 18 UMS 000003', 11, 4, '4', 4, '2018-08-23 18:36:38.940478', 7);
INSERT INTO public.itempenggunaanmakanan VALUES (6, 'MS180177000001', 42, 30, 'undefined', 3, '2018-09-25 14:01:31.560168', 42);
INSERT INTO public.itempenggunaanmakanan VALUES (7, 'MS180177000002', 41, 20, 'undefined', 3, '2018-09-25 14:02:26.608736', 41);
INSERT INTO public.itempenggunaanmakanan VALUES (8, 'MS180177000002', 11, 15, '13', 7, '2018-09-25 14:02:26.608736', 11);


--
-- Data for Name: itempermintaanbarang; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.itempermintaanbarang VALUES (2, 'PELNI/PR 18 UMS 000001', 1, '2018-08-01 00:00:00', 10, '2018-08-13 15:01:17.397086');
INSERT INTO public.itempermintaanbarang VALUES (3, 'PELNI/PR 18 UMS 000001', 10, '2018-08-17 00:00:00', 15, '2018-08-13 15:01:17.397086');
INSERT INTO public.itempermintaanbarang VALUES (4, 'PELNI/PR 18 UMS 000002', 2, '2018-08-08 00:00:00', 12, '2018-08-13 15:53:34.534384');
INSERT INTO public.itempermintaanbarang VALUES (5, 'PELNI/PR 18 UMS 000002', 30, '2018-08-09 00:00:00', 19, '2018-08-13 15:53:34.534384');
INSERT INTO public.itempermintaanbarang VALUES (6, 'PELNI/PR 18 KMT 000001', 6, '2018-08-08 00:00:00', 6, '2018-08-13 17:14:06.134576');
INSERT INTO public.itempermintaanbarang VALUES (7, 'PELNI/PR 18 KMT 000001', 31, '2018-08-14 00:00:00', 26, '2018-08-13 17:14:06.134576');
INSERT INTO public.itempermintaanbarang VALUES (8, 'PELNI/PR 18 UMS 000003', 9, '2018-08-23 00:00:00', 10, '2018-08-14 10:12:28.058806');
INSERT INTO public.itempermintaanbarang VALUES (9, 'PELNI/PR 18 UMS 000003', 2, '2018-08-15 00:00:00', 10, '2018-08-14 10:12:28.058806');
INSERT INTO public.itempermintaanbarang VALUES (10, 'PELNI/PR 18 UMS 000004', 1, '2018-08-23 00:00:00', 10, '2018-08-14 10:48:22.160569');
INSERT INTO public.itempermintaanbarang VALUES (11, 'PELNI/PR 18 UMS 000004', 13, '2018-08-23 00:00:00', 15, '2018-08-14 10:48:22.160569');
INSERT INTO public.itempermintaanbarang VALUES (12, 'PELNI/PR 18 UMS 000005', 7, '2018-08-29 00:00:00', 23, '2018-09-04 13:53:39.471648');
INSERT INTO public.itempermintaanbarang VALUES (13, 'PR180177 000001', 5, '2018-09-27 00:00:00', 23, '2018-09-21 16:10:57.624686');
INSERT INTO public.itempermintaanbarang VALUES (14, 'PR180177 000001', 6, '2018-09-05 00:00:00', 32, '2018-09-21 16:10:57.624686');
INSERT INTO public.itempermintaanbarang VALUES (15, 'PR180177000001', 5, '2018-08-28 00:00:00', 23, '2018-09-24 14:53:04.119059');
INSERT INTO public.itempermintaanbarang VALUES (16, 'PR180177000002', 6, '2018-09-06 00:00:00', 23, '2018-09-25 09:37:14.756697');
INSERT INTO public.itempermintaanbarang VALUES (17, 'PR180177000002', 3, '2018-09-19 00:00:00', 23, '2018-09-25 09:37:14.756697');
INSERT INTO public.itempermintaanbarang VALUES (18, 'PR180177000003', 11, '2018-09-07 00:00:00', 50, '2018-09-25 09:42:53.202556');
INSERT INTO public.itempermintaanbarang VALUES (19, 'PR180177000003', 9, '2018-09-12 00:00:00', 20, '2018-09-25 09:42:53.202556');
INSERT INTO public.itempermintaanbarang VALUES (20, 'PR180177000004', 39038, '2018-09-05 00:00:00', 50, '2018-09-30 10:39:04.334454');
INSERT INTO public.itempermintaanbarang VALUES (21, 'PR180177000004', 39105, '2018-09-04 00:00:00', 70, '2018-09-30 10:39:04.334454');
INSERT INTO public.itempermintaanbarang VALUES (22, 'PR180177000005', 5, '2018-09-06 00:00:00', 23, '2018-09-30 16:53:13.672825');
INSERT INTO public.itempermintaanbarang VALUES (23, 'PR180177000006', 3, '2018-10-11 00:00:00', 30, '2018-10-01 14:45:34.220572');
INSERT INTO public.itempermintaanbarang VALUES (24, 'PR180177000006', 2, '2018-10-24 00:00:00', 324, '2018-10-01 14:45:34.220572');
INSERT INTO public.itempermintaanbarang VALUES (25, 'PR180177000006', 39107, '2018-10-11 00:00:00', 234, '2018-10-01 14:45:34.220572');
INSERT INTO public.itempermintaanbarang VALUES (26, 'PR180177000007', 41644, '2018-10-18 00:00:00', 50, '2018-10-01 16:39:12.005853');
INSERT INTO public.itempermintaanbarang VALUES (27, 'PR180177000007', 41453, '2018-10-24 00:00:00', 30, '2018-10-01 16:39:12.005853');
INSERT INTO public.itempermintaanbarang VALUES (28, 'PR180177000008', 41652, '2018-10-17 00:00:00', 10, '2018-10-01 16:54:21.527981');
INSERT INTO public.itempermintaanbarang VALUES (29, 'PR180177000008', 41644, '2018-10-17 00:00:00', 90, '2018-10-01 16:54:21.527981');
INSERT INTO public.itempermintaanbarang VALUES (30, 'PR180177000008', 41655, '2018-10-18 00:00:00', 12, '2018-10-01 16:54:21.527981');
INSERT INTO public.itempermintaanbarang VALUES (31, 'PR180177000009', 41644, '2018-10-31 00:00:00', 12, '2018-10-01 16:57:40.732821');
INSERT INTO public.itempermintaanbarang VALUES (32, 'PR180177000010', 41660, '2018-10-10 00:00:00', 70, '2018-10-01 17:10:01.780586');
INSERT INTO public.itempermintaanbarang VALUES (33, 'PR180177000011', 41643, '2018-10-10 00:00:00', 89, '2018-10-01 17:11:39.807117');
INSERT INTO public.itempermintaanbarang VALUES (34, 'PR180177000011', 41649, '2018-10-11 00:00:00', 90, '2018-10-01 17:11:39.807117');
INSERT INTO public.itempermintaanbarang VALUES (35, 'PR180177000012', 41482, '2018-10-10 00:00:00', 21, '2018-10-02 14:57:55.41997');
INSERT INTO public.itempermintaanbarang VALUES (36, 'PR180177000013', 41643, '2018-10-11 00:00:00', 12, '2018-10-02 15:30:11.805754');


--
-- Data for Name: itempindahbarangio; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.itempindahbarangio VALUES (1, 'PELNI/IO 18 UMS 000001', 1, 3, 10, '20', NULL, NULL);
INSERT INTO public.itempindahbarangio VALUES (2, 'PELNI/IO 18 UMS 000001', 2, 1, 10, '10', NULL, NULL);
INSERT INTO public.itempindahbarangio VALUES (3, 'PELNI/IO 18 UMS 000002', 3, 3, 32, '23', NULL, NULL);
INSERT INTO public.itempindahbarangio VALUES (4, 'PELNI/IO 18 UMS 000002', 11, 4, 10, '121', NULL, NULL);
INSERT INTO public.itempindahbarangio VALUES (5, 'PELNI/IO 18 UMS 000003', 3, 4, 30, '30', NULL, NULL);
INSERT INTO public.itempindahbarangio VALUES (6, 'PELNI/IO 18 UMS 000004', 1, 4, 5, '23', NULL, NULL);
INSERT INTO public.itempindahbarangio VALUES (7, 'PELNI/IO 18 UMS 000004', 5, 4, 3, '23', NULL, NULL);
INSERT INTO public.itempindahbarangio VALUES (8, 'PELNI/IO 18 UMS 000005', 1, 1, 3, '4', '2018-08-23 18:14:24.297269', 21);
INSERT INTO public.itempindahbarangio VALUES (9, 'PELNI/IO 18 UMS 000005', 9, 1, 5, '5', '2018-08-23 18:14:24.297269', 10);
INSERT INTO public.itempindahbarangio VALUES (10, 'PELNI/IO 18 UMS 000006', 4, 2, 5, '32', '2018-09-04 14:17:30.622802', 33);
INSERT INTO public.itempindahbarangio VALUES (11, 'IO180177000001', 4, 7, 5, '32', '2018-09-24 10:25:19.341985', 31);
INSERT INTO public.itempindahbarangio VALUES (12, 'IO180177000002', 1, 3, 5, '5', '2018-09-30 16:56:05.346869', 20);


--
-- Data for Name: itempindahbarangsubinv; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.itempindahbarangsubinv VALUES (1, 'PELNI/MO 18 UMS 000001', 4, 5, '32', '2018-08-23 15:21:55.876471', 29);
INSERT INTO public.itempindahbarangsubinv VALUES (2, 'PELNI/MO 18 UMS 000001', 8, 6, '34', '2018-08-23 15:21:55.876471', 30);
INSERT INTO public.itempindahbarangsubinv VALUES (3, 'MO180177000001', 3, 10, '3', '2018-09-24 14:01:09.557615', 4);
INSERT INTO public.itempindahbarangsubinv VALUES (4, 'MO180177000002', 3, 2, '3', '2018-09-30 20:00:42.076002', 4);


--
-- Data for Name: itempopenerimaan; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.itempopenerimaan VALUES (2, 'PO180177000001', 41643, 3, '2018-08-14 01:30:47.664', '2018-08-14 01:30:49.951', 20, '10', '2018-08-14 01:30:57.81', 20, 0);
INSERT INTO public.itempopenerimaan VALUES (1, 'PO180177000001', 41642, 3, '2018-08-14 01:30:27.395', '2018-08-14 01:30:29.766', 10, '10', '2018-08-14 01:30:35.977', 0, 1);


--
-- Data for Name: itempopenerimaanio; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.itempopenerimaanio VALUES (1, 'IO180177000001', 4, '2018-08-18 04:56:17.318', 5, 3, 'r', 0, '2018-08-18 04:56:28.345', 5);
INSERT INTO public.itempopenerimaanio VALUES (2, 'IO180177000001', 8, '2018-08-18 04:56:42.876', 40, 2, 'rt', 0, '2018-08-18 04:57:17.454', 40);


--
-- Data for Name: kapal; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.kapal VALUES (81, '', 'PELNI GROUP', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (101, '', 'PELNI Kantor Pusat', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (102, '', 'PELNI Kaimana', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (103, '', 'PELNI Merauke', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (104, '', 'SBU Hotel Bahtera', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (105, '', 'SBU Galangan Surya', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (106, '', 'PELNI Timika', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (107, '', 'PELNI Biak', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (108, '', 'PELNI Serui', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (109, '', 'PELNI Fak-Fak', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (110, '', 'PELNI Nabire', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (111, '', 'PELNI Tanjung Pinang', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (112, '', 'PELNI Manokwari', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (113, '', 'PELNI Batam', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (114, '', 'PELNI Jayapura', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (115, '', 'PELNI Medan', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (116, '', 'PELNI Sorong', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (117, '', 'PELNI Tanjung Balai Karimun', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (118, '', 'PELNI Dobo', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (120, '', 'PELNI Namlea', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (121, '', 'PELNI Surabaya', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (122, '', 'PELNI Tual', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (123, '', 'PELNI Semarang', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (124, '', 'PELNI Ambon', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (125, '', 'PELNI Kupang', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (126, '', 'PELNI Toli-Toli', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (127, '', 'PELNI Maumere', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (128, '', 'PELNI Kendari', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (129, '', 'PELNI Denpasar', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (130, '', 'PELNI Luwuk', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (131, '', 'PELNI Larantuka', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (132, '', 'PELNI Palu', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (133, '', 'PELNI Bima', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (134, '', 'PELNI Pare-Pare', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (135, '', 'PELNI Ende', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (136, '', 'PELNI Ternate', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (137, '', 'PELNI Waingapu', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (138, '', 'PELNI Bitung', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (139, '', 'PELNI Labuan Bajo', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (140, '', 'PELNI Bau-Bau', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (141, '', 'PELNI Makasar', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (142, '', 'PELNI Balikpapan', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (143, '', 'PELNI Pontianak', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (144, '', 'PELNI Kumai/P.Bun', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (145, '', 'PELNI Tarakan', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (146, '', 'PELNI Nunukan', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (147, '', 'PELNI Sampit', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (148, '', 'PELNI Kotabaru/Batulicin', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (149, '', 'PELNI Singapore', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (154, 'TML', 'PELNI KP Tatamailau', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (155, 'SRM', 'PELNI KP Sirimau', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (156, 'AWU', 'PELNI KP AWU', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (157, 'CRI', 'PELNI KP Ciremai', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (158, 'DBS', 'PELNI KP Dobonsolo', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (159, 'LEU', 'PELNI KP Leuser', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (160, 'BNY', 'PELNI KP Binaya', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (161, 'BKR', 'PELNI KP Bukit Raya', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (162, 'TKB', 'PELNI KP Tilong Kabila', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (163, 'BKS', 'PELNI KP Bukit Siguntang', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (164, 'LBL', 'PELNI KP Lambelu', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (169, 'WLS', 'PELNI KP Wilis', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (170, 'GWT', 'PELNI KP Ganda Winata', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (172, 'DLN', 'PELNI KP Dorolonda', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (173, 'NGP', 'PELNI KP Nggapulu', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (174, 'LBR', 'PELNI KP Labobar', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (175, 'DMP', 'PELNI KP Dempo', '2018-09-21 13:37:58.868659');
INSERT INTO public.kapal VALUES (150, 'UMS', 'PELNI Kantor Pusat', '2018-09-24 00:00:00');
INSERT INTO public.kapal VALUES (153, 'TDR', 'PELNI Kantor Pusat', '2018-09-24 00:00:00');
INSERT INTO public.kapal VALUES (166, 'KLD', 'PELNI Kantor Pusat', '2018-09-25 00:00:00');
INSERT INTO public.kapal VALUES (177, 'HOW', 'PELNI Kantor Pusat', '2018-09-25 00:00:00');
INSERT INTO public.kapal VALUES (152, 'LWT', 'PELNI Kantor Pusat', '2018-09-25 00:00:00');
INSERT INTO public.kapal VALUES (171, 'EGN', 'PELNI Kantor Pusat', '2018-09-26 00:00:00');
INSERT INTO public.kapal VALUES (151, 'KMT', 'PELNI Kantor Pusat', '2018-09-26 00:00:00');
INSERT INTO public.kapal VALUES (119, 'TJP', 'PELNI Kantor Pusat', '2018-09-27 00:00:00');
INSERT INTO public.kapal VALUES (176, 'JLR', 'PELNI Kantor Pusat', '2018-09-27 00:00:00');
INSERT INTO public.kapal VALUES (168, 'SGG', 'PELNI Kantor Pusat', '2018-09-27 00:00:00');
INSERT INTO public.kapal VALUES (165, 'SNB', 'PELNI Kantor Pusat', '2018-09-27 00:00:00');
INSERT INTO public.kapal VALUES (167, 'PRG', 'PELNI Kantor Pusat', '2018-09-27 00:00:00');


--
-- Data for Name: loginlog; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.loginlog VALUES (2, '2018-09-03 03:36:02+07', '2018-09-03 03:47:57+07', '2018-09-03 03:36:02', NULL, 'sioado');
INSERT INTO public.loginlog VALUES (3, '2018-09-03 03:49:38+07', '2018-09-03 03:49:49+07', '2018-09-03 03:49:49', 5, 'sioado');
INSERT INTO public.loginlog VALUES (4, '2018-09-03 03:59:11+07', NULL, '2018-09-03 03:59:11', 5, 'sioado');
INSERT INTO public.loginlog VALUES (5, '2018-09-04 01:54:49+07', NULL, '2018-09-04 01:54:49', 5, 'sioado');
INSERT INTO public.loginlog VALUES (6, '2018-09-04 11:55:09+07', NULL, '2018-09-04 11:55:09', 5, 'sioado');
INSERT INTO public.loginlog VALUES (7, '2018-09-05 02:56:59+07', NULL, '2018-09-05 02:56:59', 5, 'sioado');
INSERT INTO public.loginlog VALUES (8, '2018-09-05 11:33:57+07', '2018-09-05 11:34:03+07', '2018-09-05 11:34:03', 5, 'sioado');
INSERT INTO public.loginlog VALUES (9, '2018-09-05 11:34:14+07', NULL, '2018-09-05 11:34:14', 5, 'sioado');
INSERT INTO public.loginlog VALUES (10, '2018-09-06 04:43:30+07', NULL, '2018-09-06 04:43:30', 5, 'sioado');
INSERT INTO public.loginlog VALUES (11, '2018-09-06 08:16:19+07', NULL, '2018-09-06 08:16:19', 5, 'sioado');
INSERT INTO public.loginlog VALUES (12, '2018-09-06 10:54:41+07', NULL, '2018-09-06 10:54:41', 5, 'sioado');
INSERT INTO public.loginlog VALUES (13, '2018-09-07 02:25:40+07', NULL, '2018-09-07 02:25:40', 5, 'sioado');
INSERT INTO public.loginlog VALUES (14, '2018-09-07 06:50:48+07', NULL, '2018-09-07 06:50:48', 5, 'sioado');
INSERT INTO public.loginlog VALUES (15, '2018-09-07 10:00:57+07', NULL, '2018-09-07 10:00:57', 5, 'sioado');
INSERT INTO public.loginlog VALUES (16, '2018-09-10 02:35:09+07', '2018-09-10 04:15:08+07', '2018-09-10 04:15:08', 5, 'sioado');
INSERT INTO public.loginlog VALUES (17, '2018-09-10 04:15:10+07', NULL, '2018-09-10 04:15:10', 5, 'sioado');
INSERT INTO public.loginlog VALUES (18, '2018-09-12 01:57:27+07', '2018-09-12 06:19:07+07', '2018-09-12 06:19:07', 5, 'sioado');
INSERT INTO public.loginlog VALUES (19, '2018-09-12 06:19:10+07', '2018-09-12 08:58:55+07', '2018-09-12 08:58:55', 5, 'sioado');
INSERT INTO public.loginlog VALUES (20, '2018-09-12 08:58:58+07', '2018-09-12 09:00:24+07', '2018-09-12 09:00:24', 5, 'sioado');
INSERT INTO public.loginlog VALUES (21, '2018-09-12 09:00:26+07', NULL, '2018-09-12 09:00:26', 5, 'sioado');
INSERT INTO public.loginlog VALUES (22, '2018-09-13 02:09:08+07', NULL, '2018-09-13 02:09:08', 5, 'sioado');
INSERT INTO public.loginlog VALUES (23, '2018-09-13 06:12:24+07', NULL, '2018-09-13 06:12:24', 5, 'sioado');
INSERT INTO public.loginlog VALUES (24, '2018-09-13 08:45:37+07', NULL, '2018-09-13 08:45:37', 5, 'sioado');
INSERT INTO public.loginlog VALUES (25, '2018-09-13 12:06:51+07', NULL, '2018-09-13 12:06:51', 5, 'sioado');
INSERT INTO public.loginlog VALUES (26, '2018-09-14 02:00:02+07', '2018-09-14 03:25:29+07', '2018-09-14 03:25:29', 5, 'sioado');
INSERT INTO public.loginlog VALUES (27, '2018-09-14 03:25:32+07', NULL, '2018-09-14 03:25:32', 5, 'sioado');
INSERT INTO public.loginlog VALUES (28, '2018-09-14 06:53:31+07', NULL, '2018-09-14 06:53:31', 5, 'sioado');
INSERT INTO public.loginlog VALUES (29, '2018-09-17 02:19:55+07', NULL, '2018-09-17 02:19:55', 5, 'sioado');
INSERT INTO public.loginlog VALUES (30, '2018-09-17 08:47:32+07', NULL, '2018-09-17 08:47:32', 5, 'sioado');
INSERT INTO public.loginlog VALUES (31, '2018-09-18 02:34:30+07', NULL, '2018-09-18 02:34:30', 5, 'sioado');
INSERT INTO public.loginlog VALUES (32, '2018-09-18 06:23:10+07', NULL, '2018-09-18 06:23:10', 5, 'sioado');
INSERT INTO public.loginlog VALUES (33, '2018-09-19 03:11:56+07', NULL, '2018-09-19 03:11:56', 5, 'sioado');
INSERT INTO public.loginlog VALUES (34, '2018-09-19 06:40:25+07', NULL, '2018-09-19 06:40:25', 5, 'sioado');
INSERT INTO public.loginlog VALUES (35, '2018-09-20 02:06:14+07', NULL, '2018-09-20 02:06:14', 5, 'sioado');
INSERT INTO public.loginlog VALUES (36, '2018-09-20 07:08:16+07', NULL, '2018-09-20 07:08:16', 5, 'sioado');
INSERT INTO public.loginlog VALUES (37, '2018-09-20 10:38:29+07', NULL, '2018-09-20 10:38:29', 5, 'sioado');
INSERT INTO public.loginlog VALUES (38, '2018-09-21 02:33:26+07', NULL, '2018-09-21 02:33:26', 5, 'sioado');
INSERT INTO public.loginlog VALUES (39, '2018-09-21 06:46:23+07', '2018-09-21 06:50:10+07', '2018-09-21 06:50:10', 177, 'sioado');
INSERT INTO public.loginlog VALUES (40, '2018-09-21 06:50:12+07', NULL, '2018-09-21 06:50:12', 177, 'sioado');
INSERT INTO public.loginlog VALUES (41, '2018-09-24 02:09:33+07', '2018-09-24 03:59:33+07', '2018-09-24 03:59:33', 177, 'sioado');
INSERT INTO public.loginlog VALUES (42, '2018-09-24 03:59:37+07', '2018-09-24 03:59:45+07', '2018-09-24 03:59:45', 177, 'sioado');
INSERT INTO public.loginlog VALUES (43, '2018-09-24 03:59:52+07', NULL, '2018-09-24 03:59:52', 177, 'sioado');
INSERT INTO public.loginlog VALUES (44, '2018-09-25 02:03:35+07', '2018-09-25 10:17:49+07', '2018-09-25 10:17:49', 177, 'sioado');
INSERT INTO public.loginlog VALUES (45, '2018-09-25 10:17:51+07', NULL, '2018-09-25 10:17:51', 177, 'sioado');
INSERT INTO public.loginlog VALUES (46, '2018-09-26 02:44:46+07', '2018-09-26 02:56:17+07', '2018-09-26 02:56:17', 177, 'sioado');
INSERT INTO public.loginlog VALUES (47, '2018-09-26 02:56:20+07', '2018-09-26 06:48:48+07', '2018-09-26 06:48:48', 177, 'sioado');
INSERT INTO public.loginlog VALUES (48, '2018-09-26 06:48:50+07', '2018-09-26 09:47:55+07', '2018-09-26 09:47:55', 177, 'sioado');
INSERT INTO public.loginlog VALUES (49, '2018-09-26 09:48:13+07', '2018-09-26 09:49:08+07', '2018-09-26 09:49:08', 177, 'sioado');
INSERT INTO public.loginlog VALUES (50, '2018-09-26 09:56:54+07', NULL, '2018-09-26 09:56:54', 177, 'sioado');
INSERT INTO public.loginlog VALUES (51, '2018-09-27 07:08:42+07', NULL, '2018-09-27 07:08:42', 177, 'sioado');
INSERT INTO public.loginlog VALUES (52, '2018-09-28 02:00:30+07', NULL, '2018-09-28 02:00:30', 177, 'sioado');
INSERT INTO public.loginlog VALUES (53, '2018-09-28 06:22:29+07', NULL, '2018-09-28 06:22:29', 177, 'sioado');
INSERT INTO public.loginlog VALUES (54, '2018-09-29 16:03:55+07', NULL, '2018-09-29 16:03:55', 177, 'sioado');
INSERT INTO public.loginlog VALUES (55, '2018-09-29 21:40:15+07', NULL, '2018-09-29 21:40:15', 177, 'sioado');
INSERT INTO public.loginlog VALUES (56, '2018-09-30 02:30:55+07', NULL, '2018-09-30 02:30:55', 177, 'sioado');
INSERT INTO public.loginlog VALUES (57, '2018-09-30 10:22:25+07', NULL, '2018-09-30 10:22:25', 177, 'sioado');
INSERT INTO public.loginlog VALUES (58, '2018-09-30 15:56:03+07', NULL, '2018-09-30 15:56:03', 177, 'sioado');
INSERT INTO public.loginlog VALUES (59, '2018-09-30 23:13:35+07', NULL, '2018-09-30 23:13:35', 177, 'sioado');
INSERT INTO public.loginlog VALUES (60, '2018-10-01 09:11:12+07', '2018-10-01 10:47:58+07', '2018-10-01 10:47:58', 177, 'sioado');
INSERT INTO public.loginlog VALUES (61, '2018-10-01 10:48:01+07', NULL, '2018-10-01 10:48:01', 177, 'sioado');
INSERT INTO public.loginlog VALUES (62, '2018-10-01 13:37:39+07', NULL, '2018-10-01 13:37:39', 177, 'sioado');
INSERT INTO public.loginlog VALUES (63, '2018-10-02 03:08:03+07', '2018-10-02 05:08:44+07', '2018-10-02 05:08:44', 177, 'sioado');
INSERT INTO public.loginlog VALUES (64, '2018-10-02 05:08:47+07', '2018-10-02 05:10:28+07', '2018-10-02 05:10:28', 177, 'sioado');
INSERT INTO public.loginlog VALUES (65, '2018-10-02 05:10:31+07', '2018-10-02 05:16:06+07', '2018-10-02 05:16:06', 177, 'sioado');
INSERT INTO public.loginlog VALUES (66, '2018-10-02 05:16:09+07', NULL, '2018-10-02 05:16:09', 177, 'sioado');
INSERT INTO public.loginlog VALUES (67, '2018-10-02 09:46:16+07', '2018-10-02 15:09:51+07', '2018-10-02 15:09:51', 177, 'sioado');
INSERT INTO public.loginlog VALUES (68, '2018-10-02 15:09:53+07', '2018-10-02 15:10:38+07', '2018-10-02 15:10:38', 177, 'sioado');
INSERT INTO public.loginlog VALUES (69, '2018-10-02 15:10:40+07', NULL, '2018-10-02 15:10:40', 177, 'sioado');
INSERT INTO public.loginlog VALUES (70, '2018-10-02 15:12:31+07', NULL, '2018-10-02 15:12:31', 177, 'sioado');
INSERT INTO public.loginlog VALUES (71, '2018-10-02 15:14:30+07', NULL, '2018-10-02 15:14:30', 177, 'sioado');
INSERT INTO public.loginlog VALUES (72, '2018-10-02 15:17:11+07', NULL, '2018-10-02 15:17:11', 177, 'sioado');
INSERT INTO public.loginlog VALUES (73, '2018-10-02 15:29:42+07', NULL, '2018-10-02 15:29:42', 177, 'sioado');
INSERT INTO public.loginlog VALUES (74, '2018-10-02 16:14:53+07', NULL, '2018-10-02 16:14:53', 177, 'sioado');
INSERT INTO public.loginlog VALUES (75, '2018-10-02 18:24:41+07', NULL, '2018-10-02 18:24:41', 177, 'sioado');
INSERT INTO public.loginlog VALUES (76, '2018-10-03 09:52:57+07', NULL, '2018-10-03 09:52:57', 177, 'sioado');
INSERT INTO public.loginlog VALUES (77, '2018-10-03 10:14:21+07', NULL, '2018-10-03 10:14:21', 177, 'sioado');
INSERT INTO public.loginlog VALUES (78, '2018-10-03 11:16:29+07', NULL, '2018-10-03 11:16:29', 177, 'sioado');
INSERT INTO public.loginlog VALUES (79, '2018-10-03 13:49:10+07', NULL, '2018-10-03 13:49:10', 177, 'sioado');


--
-- Data for Name: lokasiitem; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.lokasiitem VALUES (3, 'Gudang II', 'Deck Atas', '123', '123', NULL, 177);
INSERT INTO public.lokasiitem VALUES (1, 'Gudang I', 'Gudang I', 'Jl. Kemarin', '29034', NULL, 177);
INSERT INTO public.lokasiitem VALUES (4, 'GANDROOM', 'Deck Bawah', '123', '123', NULL, 177);
INSERT INTO public.lokasiitem VALUES (2, 'Gudang Dapur', 'Dapur', 'jl. dapur', '214234', NULL, 153);
INSERT INTO public.lokasiitem VALUES (6, 'Gudang Nama V', 'Gudang Lokasi V', 'Gudang Alamat V', 'Gudang ORG V', NULL, 153);
INSERT INTO public.lokasiitem VALUES (5, 'Gudang IV', 'Dech IV', 'Jl. Deck IV', 'DeckIV', NULL, 153);
INSERT INTO public.lokasiitem VALUES (7, 'Lokasi I', 'Lokasi Kapal Senayan', 'Alamat lokasi baru', 'ORG nya saja', '2018-09-04 10:03:52.574545', 177);


--
-- Data for Name: lotnumber; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: notifications; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: penerimaanbarang; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.penerimaanbarang VALUES (1, 'PB180177000001', 'PO180177000001', 1, 177, 1, '1', '2018-09-19 00:00:00', '2018-08-14 00:00:00', 18, '0013', '2018-09-19 00:00:00', '2018-09-29 00:00:00', 0, '2018-09-26 14:54:36.223793');
INSERT INTO public.penerimaanbarang VALUES (2, 'PB180177000002', 'PO180177000001', 1, 177, 1, '1', '2018-09-19 00:00:00', '2018-08-14 00:00:00', 12, '0011', '2018-09-19 00:00:00', '2018-09-30 00:00:00', 0, '2018-09-26 15:09:45.010987');
INSERT INTO public.penerimaanbarang VALUES (3, 'PB180177000003', 'PO180177000001', 1, 177, 1, '1', '2018-09-19 00:00:00', '2018-08-14 00:00:00', 4, '0011', '2018-09-11 00:00:00', '2018-09-21 00:00:00', 0, '2018-09-26 15:15:27.847028');
INSERT INTO public.penerimaanbarang VALUES (4, 'PB180177000004', 'PO180177000001', 1, 177, 1, '1', '2018-09-12 00:00:00', '2018-08-14 00:00:00', 6, '0012', '2018-09-27 00:00:00', '2018-09-27 00:00:00', 0, '2018-09-26 15:17:06.80115');
INSERT INTO public.penerimaanbarang VALUES (5, 'PB180177000005', 'PO180177000001', 1, 177, 1, '1', '2018-09-27 00:00:00', '2018-08-14 00:00:00', 8, '0013', '2018-09-27 00:00:00', '2018-09-30 00:00:00', 0, '2018-09-26 15:34:15.19491');
INSERT INTO public.penerimaanbarang VALUES (6, 'PB180177000006', 'PO180177000001', 1, 177, 1, '1', '2018-10-18 00:00:00', '2018-08-14 00:00:00', 5, '0011', '2018-10-24 00:00:00', '2018-10-27 00:00:00', 0, '2018-10-02 13:24:41.79474');
INSERT INTO public.penerimaanbarang VALUES (7, 'PB180177000007', 'PO180177000001', 1, 177, 1, '1', '2018-10-24 00:00:00', '2018-08-14 00:00:00', 10, '0011', '2018-10-23 00:00:00', '2018-10-27 00:00:00', 0, '2018-10-02 13:27:01.965301');
INSERT INTO public.penerimaanbarang VALUES (8, 'PB180177000008', 'PO180177000001', 1, 177, 1, '1', '2018-10-25 00:00:00', '2018-08-14 00:00:00', 10, '0011', '2018-11-01 00:00:00', '2018-11-09 00:00:00', 0, '2018-10-02 13:31:17.605021');


--
-- Data for Name: penerimaanbarangio; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.penerimaanbarangio VALUES (6, 'PELNI/RC 18 UMS 000001', 'PELNI/IO 18 UMS 000001', 1, 5, 1, '1', '2018-08-23 00:00:00', '2018-08-18 00:00:00', 15, '0011', '2018-08-23 00:00:00', '2018-08-30 00:00:00', 0, '2018-08-23 13:03:26.825221');


--
-- Data for Name: penerimaanmakanan; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.penerimaanmakanan VALUES (1, 1, 1, 5, 'PELNI/MR 18 UMS 000001', 1, 1, 2, '2018-08-15 00:00:00', 0, 'komentar komentar komentar komentar', '0012', '2018-08-17 00:00:00', '2018-09-07 00:00:00', 1, '2018-08-20 17:44:15.930455', NULL, '345 345 345 345');
INSERT INTO public.penerimaanmakanan VALUES (2, 1, 1, 5, 'PELNI/MR 18 UMS 000002', 1, 1, 1, '2018-08-15 00:00:00', 0, 'data-date-format="yyyy-mm-dd" data-date-autoclose="true"', '0013', '2018-08-08 00:00:00', '2018-08-14 00:00:00', 1, '2018-08-20 18:27:53.001345', NULL, '9-10-1-13');
INSERT INTO public.penerimaanmakanan VALUES (3, 1, NULL, 177, 'MR180177000001', 1, 1, 1, '2018-09-06 00:00:00', 0, '05 20 08 90 07 100', '0011', '2018-09-01 00:00:00', '2018-09-20 00:00:00', 0, '2018-09-25 11:45:21.672284', '2018-09-25 11:45:21.672284', NULL);
INSERT INTO public.penerimaanmakanan VALUES (4, 1, 1, 177, 'MR180177000002', 1, 1, 1, '2018-09-01 00:00:00', 110, '423 423 4234 234', '0012', '2018-09-07 00:00:00', '2018-09-20 00:00:00', 1, '2018-09-25 13:41:53.176646', '2018-09-25 13:41:53.176646', '110 pcs');
INSERT INTO public.penerimaanmakanan VALUES (5, 1, 1, 177, 'MR180177000003', 1, 1, 1, '2018-09-06 00:00:00', 300, '21e12 12 e12 e12', '0031', '2018-09-01 00:00:00', '2018-09-19 00:00:00', 1, '2018-09-30 20:10:00.194915', '2018-09-30 20:10:00.194915', 'ew fwe fewf ');


--
-- Data for Name: penggunaanbarang; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.penggunaanbarang VALUES (6, 'PELNI/MI 18 UMS 000001', 1, 0, 1, 5, 4, 51090106, '2018-08-24 00:00:00', 96, '0011', '2018-09-05 00:00:00', '2018-09-07 00:00:00', '2018-08-24 19:49:29.859206');
INSERT INTO public.penggunaanbarang VALUES (7, 'PELNI/MI 18 UMS 000002', 1, 0, 1, 5, 3, 51090106, '2018-08-24 00:00:00', 91, '0013', '2018-08-29 00:00:00', '2018-08-16 00:00:00', '2018-08-24 19:59:46.75834');
INSERT INTO public.penggunaanbarang VALUES (8, 'PELNI/MI 18 UMS 000003', 1, 0, 1, 5, 3, 51090106, '2018-08-24 00:00:00', 94, '0012', '2018-08-22 00:00:00', '2018-08-30 00:00:00', '2018-08-24 20:08:30.526488');
INSERT INTO public.penggunaanbarang VALUES (13, 'PELNI/MI 18 UMS 000004', 1, 0, 1, 5, 3, 51090106, '2018-08-24 00:00:00', 94, '0033', '2018-08-22 00:00:00', '2018-08-29 00:00:00', '2018-08-24 20:19:01.764487');
INSERT INTO public.penggunaanbarang VALUES (14, 'PELNI/MI 18 UMS 000005', 1, 0, 1, 5, 3, 51090106, '2018-09-12 00:00:00', 12, '0000', '2018-09-05 00:00:00', '2018-09-27 00:00:00', '2018-09-12 09:48:37.072036');
INSERT INTO public.penggunaanbarang VALUES (15, 'MI180177000001', 1, 0, 1, 177, 3, 51090106, '2018-09-25 00:00:00', 17, '0011', '2018-09-01 00:00:00', '2018-09-30 00:00:00', '2018-09-25 10:51:32.366262');
INSERT INTO public.penggunaanbarang VALUES (16, 'MI180177000002', 1, 0, 1, 177, 3, 51090106, '2018-09-30 00:00:00', 8, '0011', '2018-09-12 00:00:00', '2018-09-27 00:00:00', '2018-09-30 20:06:28.45499');


--
-- Data for Name: penggunaanmakanan; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.penggunaanmakanan VALUES (1, 1, 1, 5, 'PELNI/MS 18 UMS 000001', 1, 1, 2, '2018-08-24 00:00:00', 0, 'penggunaanmakanan penggunaanmakanan penggunaanmakanan penggunaanmakanan', '0012', '2018-08-02 00:00:00', '2018-08-15 00:00:00', 1, '2018-08-20 20:03:33.082167', NULL, 'setuju');
INSERT INTO public.penggunaanmakanan VALUES (5, 1, 1, 5, 'PELNI/MS 18 UMS 000003', 1, 1, 1, '2018-08-22 00:00:00', 0, 'required required required', '0000', '2018-08-07 00:00:00', '2018-08-07 00:00:00', 0, '2018-08-23 18:36:38.940478', '2018-08-23 18:36:38.940478', 'feed badk. stok id 10 & 7');
INSERT INTO public.penggunaanmakanan VALUES (2, 1, 1, 5, 'PELNI/MS 18 UMS 000002', 1, 1, 2, '2018-08-10 00:00:00', 0, ' 9 9 9 99 9  9', '0012', '2018-08-08 00:00:00', '2018-08-22 00:00:00', 1, '2018-08-20 20:04:10.351285', NULL, 'asas');
INSERT INTO public.penggunaanmakanan VALUES (6, 1, NULL, 177, 'MS180177000001', 1, 1, 1, '2018-09-07 00:00:00', 30, 'sdfsdf sdfsdf sdf  sdf sdf sdf', '0011', '2018-09-01 00:00:00', '2018-09-30 00:00:00', 0, '2018-09-25 14:01:31.560168', '2018-09-25 14:01:31.560168', NULL);
INSERT INTO public.penggunaanmakanan VALUES (7, 1, 1, 177, 'MS180177000002', 1, 1, 1, '2018-09-12 00:00:00', 35, '20 & 15', '0011', '2018-09-14 00:00:00', '2018-09-30 00:00:00', 1, '2018-09-25 14:02:26.608736', '2018-09-25 14:02:26.608736', 'wefwef w ef we fwef');


--
-- Data for Name: permintaanbarang; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.permintaanbarang VALUES (14, 'PELNI/PR 18 UMS 000002', 1, 1, 5, 3, '', 'Unit 2', 'Unit 2', 'Unit 2', 381200, 31, 'Unit 2', 'Unit Unit Unit Unit Unit Unit Unit Unit Unit Unit Unit Unit Unit Unit Unit Unit 2', 'Unit 2', '2018-08-13 08:52:34', '2018-08-13 15:53:34.534384', 2, '0012', '2018-08-03 00:00:00', '2018-08-06 00:00:00', '2018-08-13 15:53:34.534384', NULL);
INSERT INTO public.permintaanbarang VALUES (13, 'PELNI/PR 18 UMS 000001', 1, 1, 5, 1, '', 'Operasi1', 'Operasi1', 'Operasi1', 300500, 25, 'Operasi1', 'Operasi Operasi Operasi Operasi Operasi Operasi Operasi Operasi 1', 'Operasi1', '2018-08-13 08:00:33', '2018-08-13 15:01:17.397086', 1, '0011', '2018-08-01 00:00:00', '2018-08-08 00:00:00', '2018-08-13 15:01:17.397086', NULL);
INSERT INTO public.permintaanbarang VALUES (15, 'PELNI/PR 18 KMT 000001', 2, 0, 6, 2, '', 'Deskripsi kapal 6', 'Deskripsi kapal 6', 'Deskripsi kapal 6', 640000, 32, 'Deskripsi kapal 6', 'Deskripsi Deskripsi Deskripsi kapal 6', 'Deskripsi kapal 6', '2018-08-13 10:13:12', '2018-08-13 17:14:06.134576', 0, '0013', '2018-08-08 00:00:00', '2018-08-30 00:00:00', '2018-08-13 17:14:06.134576', NULL);
INSERT INTO public.permintaanbarang VALUES (25, 'PR180177000006', 1, 0, 177, 3, '', 'PELNI HO WAREHOUSE', NULL, '234', 0, 588, '234', '23rr 23 r23 r', '123', '2018-10-01 14:44:55', '2018-10-01 14:45:34.220572', 0, '0011', '2018-10-17 00:00:00', '2018-11-02 00:00:00', '2018-10-01 14:45:34.220572', NULL);
INSERT INTO public.permintaanbarang VALUES (17, 'PELNI/PR 18 UMS 000004', 1, 1, 5, 1, '', 'Unit Operasi', 'Keperluan', 'Keperluan', 300500, 25, 'Keperluan', 'komentar komentar', 'Deskripsi', '2018-08-14 03:40:12', '2018-08-14 10:48:22.160569', 1, '0013', '2018-08-29 00:00:00', '2018-08-31 00:00:00', '2018-08-14 10:48:22.160569', NULL);
INSERT INTO public.permintaanbarang VALUES (26, 'PR180177000007', 1, 0, 177, 4, '', 'PELNI HO WAREHOUSE', NULL, 'mana ku tahu, tempe, mendoan', 0, 80, 'sumber sono', ' dsfd sfsdfs dfsd fsd f sdf sdf', 'masukkan deskripsi', '2018-10-01 16:38:07', '2018-10-01 16:39:12.005853', 0, '0013', '2018-10-17 00:00:00', '2018-10-31 00:00:00', '2018-10-01 16:39:12.005853', NULL);
INSERT INTO public.permintaanbarang VALUES (27, 'PR180177000008', 1, 0, 177, 1, '', 'PELNI HO WAREHOUSE', NULL, 'keperluan', 0, 112, 'lkwejfwef', 'lkwef wef w ef wefnwkef wenmf', 'deksripsi', '2018-10-01 16:52:56', '2018-10-01 16:54:21.527981', 0, '0032', '2018-10-17 00:00:00', '2018-11-01 00:00:00', '2018-10-01 16:54:21.527981', NULL);
INSERT INTO public.permintaanbarang VALUES (16, 'PELNI/PR 18 UMS 000003', 1, 1, 5, 4, '', 'Deskripsi', 'Deskripsi', 'Deskripsi', 201000, 20, 'Deskripsi', 'Deskripsi DeskripsiDeskripsiDeskripsi Deskripsi Deskripsi', 'Deskripsi', '2018-08-14 03:10:38', '2018-08-14 10:12:28.058806', 1, '0013', '2018-08-21 00:00:00', '2018-08-31 00:00:00', '2018-08-14 10:12:28.058806', 'Feedback Feedback Feedback Feedback Feedback 3 PELNI/PR 18 UMS 000003');
INSERT INTO public.permintaanbarang VALUES (18, 'PELNI/PR 18 UMS 000005', 1, 1, 5, 3, '', '123', '123', '123', 0, 23, '123', '123', '123', '2018-09-04 06:53:24', '2018-09-04 13:53:39.471648', 1, '0011', '2018-09-19 00:00:00', '2018-09-27 00:00:00', '2018-09-04 13:53:39.471648', 'setuju');
INSERT INTO public.permintaanbarang VALUES (19, 'PR180177000001', 1, 0, 177, 2, '', 'wer', '23r', '23r', 0, 55, '23r', '23r23r ef', 'wer', '2018-09-21 09:10:17', '2018-09-21 16:10:57.624686', 0, '0011', '2018-09-12 00:00:00', '2018-09-25 00:00:00', '2018-09-21 16:10:57.624686', NULL);
INSERT INTO public.permintaanbarang VALUES (20, 'PR180177000001', 1, 0, 177, 3, '', '32f', '23r', '32r', 0, 23, '3r', '23r23r', '32r', '2018-09-24 07:51:56', '2018-09-24 14:53:04.119059', 0, '0011', '2018-09-20 00:00:00', '2018-09-29 00:00:00', '2018-09-24 14:53:04.119059', NULL);
INSERT INTO public.permintaanbarang VALUES (21, 'PR180177000002', 1, 0, 177, 3, '', 'wef', '23r', '3r', 0, 46, '23r', '23r r 23 r23r ', 'ewf', '2018-09-25 02:36:47', '2018-09-25 09:37:14.756697', 0, '0011', '2018-09-19 00:00:00', '2018-09-20 00:00:00', '2018-09-25 09:37:14.756697', NULL);
INSERT INTO public.permintaanbarang VALUES (22, 'PR180177000003', 1, 0, 177, 3, '', '43', '123423', '123890', 0, 70, '12365', 'hereby declare that I have checked and verified the content of this application and the authenticity of the supporting documents (if any) and I will be fully responsible for any risk and loss due to error in such checking and verification', '34t3t4', '2018-09-25 02:42:04', '2018-09-25 09:42:53.202556', 0, '0011', '2018-09-01 00:00:00', '2018-09-30 00:00:00', '2018-09-25 09:42:53.202556', NULL);
INSERT INTO public.permintaanbarang VALUES (28, 'PR180177000009', 1, 0, 177, 3, '', 'PELNI HO WAREHOUSE', NULL, 'qwertyui', 0, 12, 'wertyui', 'wertyuio', 'tyqwertyu', '2018-10-01 16:52:56', '2018-10-01 16:57:40.732821', 0, '0011', '2018-10-10 00:00:00', '2018-10-23 00:00:00', '2018-10-01 16:57:40.732821', NULL);
INSERT INTO public.permintaanbarang VALUES (23, 'PR180177000004', 1, 1, 177, 1, '', 'ghh hhhj h g g g ', ' hv hv gh hg ', 'vfyvhf hv gf gf gf ', 0, 120, 'hg hv hv vhv ', 'jhj hb hvhv gc gc gf gc', ' h gyg yg gy ug ', '2018-09-30 10:34:16', '2018-09-30 10:39:04.334454', 1, '0011', '2018-09-01 00:00:00', '2018-09-30 00:00:00', '2018-09-30 10:39:04.334454', 'ok setuju');
INSERT INTO public.permintaanbarang VALUES (24, 'PR180177000005', 1, 0, 177, 3, '', 'PELNI HO WAREHOUSE', '', 'e2e23r23r', 0, 23, 'wefwef', 'wefwef23r23r', 'wefwef', '2018-09-30 16:51:58', '2018-09-30 16:53:13.672825', 0, '0011', '2018-09-12 00:00:00', '2018-09-27 00:00:00', '2018-09-30 16:53:13.672825', NULL);
INSERT INTO public.permintaanbarang VALUES (29, 'PR180177000010', 1, 0, 177, 3, '', 'PELNI HO WAREHOUSE', NULL, 'wqrewrewrwer', 0, 70, 'ewrwe rw er wer w', ' sd  fs df sd f sd fsdf', 'lnjhjhj hj hj kjh kjh', '2018-10-01 17:09:24', '2018-10-01 17:10:01.780586', 0, '0012', '2018-10-17 00:00:00', '2018-10-31 00:00:00', '2018-10-01 17:10:01.780586', NULL);
INSERT INTO public.permintaanbarang VALUES (30, 'PR180177000011', 1, 1, 177, 3, '', 'PELNI HO WAREHOUSE', NULL, 'kjh', 0, 179, 'jk', 'jjhk n  b j bn bn', 'sdmnm', '2018-10-01 17:11:08', '2018-10-01 17:11:39.807117', 2, '0011', '2018-10-23 00:00:00', '2018-10-31 00:00:00', '2018-10-01 17:11:39.807117', 'efwefwef');
INSERT INTO public.permintaanbarang VALUES (31, 'PR180177000012', 1, 1, 177, 3, '', 'PELNI HO WAREHOUSE', NULL, '2e1 e 12e1', 0, 21, '12e 12e2e', '2e12 e12e12e ', '12e12e', '2018-10-02 14:57:24', '2018-10-02 14:57:55.41997', 1, '0013', '2018-10-24 00:00:00', '2018-11-01 00:00:00', '2018-10-02 14:57:55.41997', 'ewfwe wef ');
INSERT INTO public.permintaanbarang VALUES (32, 'PR180177000013', 1, 0, 177, 3, '', 'PELNI Kantor Pusat', NULL, '21e', 0, 12, '12 e12e', '12 e12e12e12 e12 e12e12e', '12 e12e', '2018-10-02 15:29:50', '2018-10-02 15:30:11.805754', 0, '0021', '2018-10-24 00:00:00', '2018-11-01 00:00:00', '2018-10-02 15:30:11.805754', NULL);


--
-- Data for Name: pindahbarangio; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.pindahbarangio VALUES (7, 'PELNI/IO 18 UMS 000005', 1, 1, 1, 5, 13, 0, 0, '-', 8, '2018-08-16 00:00:00', '2018-08-23 18:14:24.297269', 'lepek 3
prg oval besar 5', 0, '2018-08-23 18:14:24.297269', '0011', '2018-08-22 00:00:00', '2018-08-30 00:00:00', '2018-08-23 18:14:24.297269', 'wqe');
INSERT INTO public.pindahbarangio VALUES (1, 'PELNI/IO 18 UMS 000001', 1, 1, 1, 5, 18, 0, 1, '-', 0, '2018-08-22 00:00:00', '2018-08-13 17:02:12.076069', 'Laptop Laptop Laptop Laptop Laptop Laptop Laptop 1', 1, '2018-08-13 17:02:12.076069', '0013', '2018-07-31 00:00:00', '2018-08-21 00:00:00', NULL, NULL);
INSERT INTO public.pindahbarangio VALUES (3, 'PELNI/IO 18 UMS 000003', 1, 1, 1, 5, 1, 0, 0, '-', 30, '2018-08-09 00:00:00', '2018-08-13 17:20:23.009573', '234 234 234 234 234', 1, '2018-08-13 17:20:23.009573', '0021', '2018-08-02 00:00:00', '2018-08-28 00:00:00', NULL, NULL);
INSERT INTO public.pindahbarangio VALUES (8, 'PELNI/IO 18 UMS 000006', 1, 1, 0, 5, 1, 0, 0, '-', 5, '2018-09-01 00:00:00', '2018-09-04 14:17:30.622802', 'qwewq qweqw eqw e', 0, '2018-09-04 14:17:30.622802', '0011', '2018-09-18 00:00:00', '2018-09-28 00:00:00', '2018-09-04 14:17:30.622802', NULL);
INSERT INTO public.pindahbarangio VALUES (9, 'IO180177000001', 1, 1, 1, 177, 171, 0, 0, '-', 5, '2018-09-21 00:00:00', '2018-09-24 10:25:19.341985', 'reg', 1, '2018-09-24 10:25:19.341985', '0011', '2018-09-19 00:00:00', '2018-09-27 00:00:00', '2018-09-24 10:25:19.341985', 'Let''s see');
INSERT INTO public.pindahbarangio VALUES (10, 'IO180177000002', 1, 1, 1, 177, 81, 0, 0, '-', 5, '2018-09-21 00:00:00', '2018-09-30 16:56:05.346869', 'wferfer', 1, '2018-09-30 16:56:05.346869', '0021', '2018-09-19 00:00:00', '2018-09-27 00:00:00', '2018-09-30 16:56:05.346869', 'dqw qwdqw');
INSERT INTO public.pindahbarangio VALUES (2, 'PELNI/IO 18 UMS 000002', 1, 1, 1, 5, 8, 0, 0, '-', 0, '2018-08-23 00:00:00', '2018-08-13 17:17:23.35231', '23 23 23 23 23 ', 1, '2018-08-13 17:17:23.35231', '0013', '2018-08-01 00:00:00', '2018-08-14 00:00:00', NULL, NULL);
INSERT INTO public.pindahbarangio VALUES (4, 'PELNI/IO 18 UMS 000004', 1, 1, 1, 5, 15, 0, 0, '-', 8, '2018-08-24 00:00:00', '2018-08-13 19:16:55.693576', 'komentar komentar komentar komentar 4', 1, '2018-08-13 19:16:55.693576', '0013', '2018-08-01 00:00:00', '2018-08-21 00:00:00', NULL, 'Feedback Feedback Feedback 2');


--
-- Data for Name: pindahbarangsubinv; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.pindahbarangsubinv VALUES (1, 'PELNI/MO 18 UMS 000001', 5, 1, 1, 1, 4, 3, 1, NULL, NULL, NULL, '2018-08-23 00:00:00', 15, '2018-08-23 15:21:55.876471', '234 23423 423 4', 0, '2018-08-23 15:21:55.876471', '2018-08-23 15:21:55.876471', NULL);
INSERT INTO public.pindahbarangsubinv VALUES (2, 'MO180177000001', 177, 1, 1, 0, 3, 1, 1, NULL, NULL, NULL, '2018-09-13 00:00:00', 10, '2018-09-24 14:01:09.557615', 'lot 3 qty 10 dair gdg II ke gdg I', 0, '2018-09-24 14:01:09.557615', '2018-09-24 14:01:09.557615', NULL);
INSERT INTO public.pindahbarangsubinv VALUES (3, 'MO180177000002', 177, 1, 1, 1, 3, 1, 1, NULL, NULL, NULL, '2018-09-03 00:00:00', 2, '2018-09-30 20:00:42.076002', '12e12e', 1, '2018-09-30 20:00:42.076002', '2018-09-30 20:00:42.076002', NULL);


--
-- Data for Name: popenerimaan; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.popenerimaan VALUES (2, 'PO180101000033', 177, '2018-09-27 00:00:00', 1, 'PELNI Kantor Pusat', '700', 0, '2018-09-30 10:33:06.81149', NULL, NULL);
INSERT INTO public.popenerimaan VALUES (1, 'PO180177000001', 177, '2018-08-14 01:29:37.669', 0, '1', '1', 0, '2018-08-14 01:29:56.45', NULL, 'Daniel Sahuleka');


--
-- Data for Name: popenerimaanio; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.popenerimaanio VALUES (1, 'IO180177000001', 177, '2018-08-18 04:55:33.929', 0, '1', '10', 0, '2018-08-18 04:55:52.002', NULL, NULL);
INSERT INTO public.popenerimaanio VALUES (2, '002', 177, '2018-07-17 00:00:00', 0, 'KLD', NULL, 0, '2018-07-17 00:00:00', NULL, NULL);
INSERT INTO public.popenerimaanio VALUES (3, 'TestReport 2', 177, '2018-07-20 00:00:00', 0, 'KLD', NULL, 0, '2018-07-20 00:00:00', NULL, NULL);
INSERT INTO public.popenerimaanio VALUES (4, '222', 177, '2018-09-26 00:00:00', 0, 'KLD', NULL, 0, '2018-09-26 00:00:00', NULL, NULL);


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.roles VALUES (1, 'administrator', NULL, 177);
INSERT INTO public.roles VALUES (2, 'Jenang I', '2018-08-28 18:55:21.776627', 177);
INSERT INTO public.roles VALUES (3, 'Jenang II', '2018-08-29 10:04:17.068998', 177);


--
-- Data for Name: staging_sync_table; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.staging_sync_table VALUES (3, 'pelni_scm_pr_to_po_v
', 'popenerimaan', NULL, NULL, '2001-01-01 01:01:01');
INSERT INTO public.staging_sync_table VALUES (4, 'pelni_scm_po_lines_v', 'itempopenerimaan', NULL, NULL, '2001-01-01 01:01:01');
INSERT INTO public.staging_sync_table VALUES (2, 'pelni_scm_io_v
', 'kapal', NULL, NULL, '2001-01-01 01:01:01');
INSERT INTO public.staging_sync_table VALUES (5, 'pelni_iot_v', 'popenerimaanio', NULL, NULL, '2001-01-01 01:01:01');
INSERT INTO public.staging_sync_table VALUES (6, 'pelni_iot_v', 'itempopenerimaanio', NULL, NULL, '2001-01-01 01:01:01');
INSERT INTO public.staging_sync_table VALUES (7, 'pelni_voyage_v', 'voyages', NULL, NULL, '2001-01-01 01:01:01');
INSERT INTO public.staging_sync_table VALUES (8, 'pelni_scm_item_onhand_v', 'item_onhand', NULL, NULL, '2001-01-01 01:01:01');
INSERT INTO public.staging_sync_table VALUES (9, 'pelni_scm_subinv_v', 'lokasiitem', NULL, NULL, '2001-01-01 01:01:01');
INSERT INTO public.staging_sync_table VALUES (1, 'pelni_scm_items_v', 'item', NULL, NULL, '2018-09-19 00:00:00');


--
-- Data for Name: stg_pelni_iot_int; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.stg_pelni_iot_int VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '4', NULL, 5, NULL, '2018-09-24', '7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'IO180177000001', NULL, NULL, NULL, NULL);
INSERT INTO public.stg_pelni_iot_int VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8', 177, 7, NULL, '2018-09-25', NULL, NULL, 51090106, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'MI180177000001', NULL, NULL, NULL, NULL);
INSERT INTO public.stg_pelni_iot_int VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8', 177, 10, NULL, '2018-09-25', NULL, NULL, 51090106, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'MI180177000001', NULL, NULL, NULL, NULL);
INSERT INTO public.stg_pelni_iot_int VALUES (NULL, NULL, NULL, NULL, NULL, 1, 3, 2, '2018-09-30', '2018-09-30', NULL, NULL, 1, 'I.MFP.001.01.01.0001', 177, 5, 'PCS', '2018-09-30', 'Gudang II', NULL, NULL, 13, NULL, NULL, NULL, '5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'IO180177000002', 81, NULL, 'OUT', 'Gudang II');
INSERT INTO public.stg_pelni_iot_int VALUES (NULL, NULL, 'INV Issue', NULL, NULL, 1, 3, 2, NULL, NULL, NULL, NULL, 3, 'I.MFP.001.01.03.0003', 177, 8, NULL, '2018-09-30', 'Gudang II', NULL, NULL, 13, 27, NULL, NULL, '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'MI180177000002', NULL, NULL, NULL, NULL);


--
-- Data for Name: stg_pelni_item_onhand_int; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: stg_pelni_pr_int; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.stg_pelni_pr_int VALUES (NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23, NULL, NULL, '7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sioado', NULL, 'sioado', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '123', NULL, NULL, '', 5, NULL, NULL, '2018-08-29 00:00:00', NULL, NULL, 'PELNI/PR 18 UMS 000005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.stg_pelni_pr_int VALUES (NULL, NULL, NULL, NULL, 'APPROVED', NULL, NULL, NULL, NULL, 50, NULL, 39038, 'I.MFP.002.01.26.0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sioado', NULL, 'Administrator (sioado)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PURCHASE', NULL, 'VENDOR', NULL, NULL, NULL, NULL, 'BEGINNING BALANCE', NULL, '2018-09-05 00:00:00', 177, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PR180177000004');
INSERT INTO public.stg_pelni_pr_int VALUES (NULL, NULL, NULL, NULL, 'APPROVED', NULL, NULL, NULL, NULL, 70, NULL, 39105, 'I.MFP.005.01.04.0002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sioado', NULL, 'Administrator (sioado)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PURCHASE', NULL, 'VENDOR', NULL, NULL, NULL, NULL, 'BEGINNING BALANCE', NULL, '2018-09-04 00:00:00', 177, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PR180177000004');


--
-- Data for Name: stg_pelni_scm_misc_rcv_int; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.stg_pelni_scm_misc_rcv_int VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', 177, 80, NULL, NULL, '7', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'MR180177000002', NULL, NULL);
INSERT INTO public.stg_pelni_scm_misc_rcv_int VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 177, 30, NULL, NULL, '4', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'MR180177000002', NULL, NULL);
INSERT INTO public.stg_pelni_scm_misc_rcv_int VALUES (NULL, NULL, 'BEGBAL', NULL, NULL, 1, 3, 2, 1, 'I.MFP.001.01.01.0001', 177, 200, 'PCS', '2018-09-30', 'Lokasi I', NULL, NULL, 13, 27, NULL, NULL, '2018-09-06 00:00:00', NULL, NULL, 'MR180177000003', 'RCV', 'HOW18000013');
INSERT INTO public.stg_pelni_scm_misc_rcv_int VALUES (NULL, NULL, 'BEGBAL', NULL, NULL, 1, 3, 2, 3, 'I.MFP.001.01.03.0003', 177, 100, 'PCS', '2018-09-30', 'Gudang II', NULL, NULL, 13, 27, NULL, NULL, '2018-09-06 00:00:00', NULL, NULL, 'MR180177000003', 'RCV', 'HOW18000012');
INSERT INTO public.stg_pelni_scm_misc_rcv_int VALUES (NULL, NULL, 'INV Issue', NULL, NULL, 1, 3, 2, 11, 'I.MFP.001.01.02.0011', 177, 15, 'PCS', '2018-09-25', 'Lokasi I', NULL, NULL, 13, 27, NULL, NULL, '2018-09-12 00:00:00', NULL, NULL, 'MS180177000002', 'ISSUE', '13');


--
-- Data for Name: stg_pelni_scm_moi_int; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: stg_pelni_scm_mot_int; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.stg_pelni_scm_mot_int VALUES (NULL, NULL, 'BEGBAL', NULL, NULL, 1, 3, 2, 3, 'I.MFP.001.01.03.0003', 177, 2, 'PCS', '2018-09-30', 'Gudang II', NULL, NULL, 13, 27, 0, 'Gudang I', NULL, NULL, '2018-09-03 00:00:00', NULL, NULL, 'MO180177000002', '3');


--
-- Data for Name: stg_pelni_scm_rcv_int; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.stg_pelni_scm_rcv_int VALUES (NULL, NULL, NULL, NULL, NULL, 'PB180177000001', NULL, NULL, NULL, NULL, 177, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-19 00:00:00', NULL, NULL, 'PO180177000001', NULL, NULL, NULL);
INSERT INTO public.stg_pelni_scm_rcv_int VALUES (NULL, NULL, NULL, NULL, NULL, 'PB180177000002', NULL, NULL, NULL, NULL, 177, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-19 00:00:00', NULL, NULL, 'PO180177000001', NULL, NULL, NULL);
INSERT INTO public.stg_pelni_scm_rcv_int VALUES (NULL, NULL, NULL, NULL, NULL, 'PB180177000003', NULL, NULL, NULL, NULL, 177, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-19 00:00:00', NULL, NULL, 'PO180177000001', NULL, NULL, NULL);
INSERT INTO public.stg_pelni_scm_rcv_int VALUES (NULL, NULL, NULL, NULL, NULL, 'PB180177000004', NULL, NULL, NULL, NULL, 177, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-12 00:00:00', NULL, NULL, 'PO180177000001', NULL, NULL, NULL);
INSERT INTO public.stg_pelni_scm_rcv_int VALUES (NULL, NULL, NULL, NULL, NULL, 'PB180177000005', NULL, NULL, NULL, NULL, 177, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-27 00:00:00', NULL, NULL, 'PO180177000001', NULL, NULL, NULL);


--
-- Data for Name: stg_pelni_scm_rcv_trx_int; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.stg_pelni_scm_rcv_trx_int VALUES (NULL, NULL, NULL, NULL, '2018-09-19 00:00:00', NULL, NULL, NULL, 10, NULL, NULL, NULL, '177', 2, NULL, '4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.stg_pelni_scm_rcv_trx_int VALUES (NULL, NULL, NULL, NULL, '2018-09-19 00:00:00', NULL, NULL, NULL, 8, NULL, NULL, NULL, '177', 1, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.stg_pelni_scm_rcv_trx_int VALUES (NULL, NULL, NULL, NULL, '2018-09-19 00:00:00', NULL, NULL, NULL, 12, NULL, NULL, NULL, '177', 1, NULL, '4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.stg_pelni_scm_rcv_trx_int VALUES (NULL, NULL, NULL, NULL, '2018-09-19 00:00:00', NULL, NULL, NULL, 4, NULL, NULL, NULL, '177', 1, NULL, '4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.stg_pelni_scm_rcv_trx_int VALUES (NULL, NULL, NULL, NULL, '2018-09-12 00:00:00', NULL, NULL, NULL, 6, NULL, NULL, NULL, '177', 1, NULL, '7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.stg_pelni_scm_rcv_trx_int VALUES (NULL, NULL, NULL, NULL, '2018-09-27 00:00:00', NULL, NULL, NULL, 8, NULL, NULL, NULL, '177', 1, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


--
-- Data for Name: stg_pelni_ship_int; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: stg_pelni_system_items_int; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: stg_pelni_wh_int; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: stokkapal; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.stokkapal VALUES (34, 8, 177, 3, 0, '', 1, 1, '2018-08-24 09:49:57.968934', '34', '2018-08-24 02:49:58');
INSERT INTO public.stokkapal VALUES (30, 8, 177, 3, 0, '', 1, 1, '2018-08-23 13:03:26.825221', '10', '2018-08-23 06:03:26');
INSERT INTO public.stokkapal VALUES (41, 10, 177, 7, 80, '', 1, 2, '2018-09-25 14:00:03.462108', 'undefined', '2018-09-25 07:00:03');
INSERT INTO public.stokkapal VALUES (42, 1, 177, 4, 30, '', 1, 2, '2018-09-25 14:00:03.462108', 'undefined', '2018-09-25 07:00:03');
INSERT INTO public.stokkapal VALUES (36, 2, 177, 6, 4, '', 1, 1, '2018-09-04 14:03:28.011247', '10', '2018-09-04 07:03:28');
INSERT INTO public.stokkapal VALUES (3, 2, 177, 6, 35, 'buat', 1, 1, NULL, NULL, NULL);
INSERT INTO public.stokkapal VALUES (37, 1, 177, 4, 10, '', 1, 1, '2018-09-04 14:10:54.489806', '10', '2018-09-04 07:10:54');
INSERT INTO public.stokkapal VALUES (38, 1, 177, 4, 5, '', 1, 1, '2018-09-04 18:58:48.573299', '10', '2018-09-04 11:58:48');
INSERT INTO public.stokkapal VALUES (32, 8, 177, 3, 6, '', 1, 1, '2018-08-23 17:54:15.032049', '34', '2018-08-23 10:54:15');
INSERT INTO public.stokkapal VALUES (29, 4, 177, 4, 10, '', 1, 1, '2018-08-23 13:03:26.825221', '5', '2018-08-23 06:03:26');
INSERT INTO public.stokkapal VALUES (21, 1, 177, 4, 10, '', 1, 1, '2018-08-23 10:57:05.014365', '3', '2018-08-23 03:57:05');
INSERT INTO public.stokkapal VALUES (10, 9, 177, 3, 10, '', 1, 2, '2018-08-20 19:52:08.331335', '10', '2018-08-20 00:00:00');
INSERT INTO public.stokkapal VALUES (25, 2, 177, 4, 10, '', 1, 1, '2018-08-23 11:09:43.216781', '3', '2018-08-23 04:09:43');
INSERT INTO public.stokkapal VALUES (5, 11, 177, 4, 60, 'piring', 1, 1, NULL, NULL, NULL);
INSERT INTO public.stokkapal VALUES (47, 2, 177, 4, 10, '', 1, 1, '2018-09-26 14:54:36.223793', '10', '2018-09-26 07:54:36');
INSERT INTO public.stokkapal VALUES (6, 3, 177, 4, 5, '', 1, 1, '2018-08-15 07:29:56.434', '2', '2018-08-15 07:30:04.622');
INSERT INTO public.stokkapal VALUES (48, 1, 177, 1, 8, '', 1, 1, '2018-09-26 14:54:36.223793', '10', '2018-09-26 07:54:36');
INSERT INTO public.stokkapal VALUES (1, 1, 177, 1, 108, 'buah', 1, 1, NULL, NULL, NULL);
INSERT INTO public.stokkapal VALUES (49, 1, 177, 4, 12, '', 1, 1, '2018-09-26 15:09:45.010987', '10', '2018-09-26 08:09:45');
INSERT INTO public.stokkapal VALUES (50, 1, 177, 4, 4, '', 1, 1, '2018-09-26 15:15:27.847028', '10', '2018-09-26 08:15:27');
INSERT INTO public.stokkapal VALUES (51, 1, 177, 7, 6, '', 1, 1, '2018-09-26 15:17:06.80115', '10', '2018-09-26 08:17:06');
INSERT INTO public.stokkapal VALUES (12, 2, 177, 1, 4, '', 1, 1, '2018-08-23 10:11:15.409939', '4', '2018-08-23 03:11:15');
INSERT INTO public.stokkapal VALUES (19, 2, 177, 4, 3, '', 1, 1, '2018-08-23 10:45:26.652114', '3', '2018-08-23 03:45:26');
INSERT INTO public.stokkapal VALUES (22, 2, 177, 6, 5, '', 1, 1, '2018-08-23 10:57:05.014365', '5', '2018-08-23 03:57:05');
INSERT INTO public.stokkapal VALUES (26, 1, 177, 6, 5, '', 1, 1, '2018-08-23 11:09:43.216781', '5', '2018-08-23 04:09:43');
INSERT INTO public.stokkapal VALUES (7, 11, 177, 6, 6, '1', 1, 2, NULL, 'yuyiuiu', '2018-08-19 05:36:59.136');
INSERT INTO public.stokkapal VALUES (33, 4, 177, 3, 5, '', 1, 1, '2018-08-24 09:49:57.968934', '32', '2018-08-24 02:49:58');
INSERT INTO public.stokkapal VALUES (2, 5, 177, 2, 900, 'buah', 1, 1, NULL, NULL, NULL);
INSERT INTO public.stokkapal VALUES (31, 4, 177, 3, 0, '', 1, 1, '2018-08-23 17:54:15.032049', '32', '2018-08-23 10:54:15');
INSERT INTO public.stokkapal VALUES (56, 1, 177, 1, 8, '', 1, 1, '2018-09-26 15:34:15.19491', '10', '2018-09-26 08:34:15');
INSERT INTO public.stokkapal VALUES (57, 3, 177, 1, 2, '', 1, 1, '2018-09-30 20:00:54.887229', '3', '2018-09-30 20:00:54');
INSERT INTO public.stokkapal VALUES (20, 1, 177, 6, 0, '', 1, 1, '2018-08-23 10:45:26.652114', '5', '2018-08-23 03:45:26');
INSERT INTO public.stokkapal VALUES (4, 3, 177, 3, 0, 'buah', 1, 1, NULL, '3', '2018-07-15 07:30:07.024');
INSERT INTO public.stokkapal VALUES (60, 1, 177, 7, 200, '', 1, 2, '2018-09-30 20:13:38.998261', 'HOW18000013', '2018-09-30 20:13:39');
INSERT INTO public.stokkapal VALUES (61, 3, 177, 3, 100, '', 1, 2, '2018-09-30 20:13:38.998261', 'HOW18000012', '2018-09-30 20:13:39');
INSERT INTO public.stokkapal VALUES (11, 1, 177, 6, 15, '', 1, 2, '2018-08-20 19:52:08.331335', '13', '2018-08-20 00:00:00');
INSERT INTO public.stokkapal VALUES (62, 41642, 177, 1, 5, '', 1, 1, '2018-10-02 13:24:41.79474', 'HOW18000014', '2018-10-02 13:24:41');
INSERT INTO public.stokkapal VALUES (63, 41643, 177, 1, 10, '', 1, 1, '2018-10-02 13:27:01.965301', 'HOW18000015', '2018-10-02 13:27:01');
INSERT INTO public.stokkapal VALUES (64, 41642, 177, 1, 10, '', 1, 1, '2018-10-02 13:31:17.605021', 'HOW18000016', '2018-10-02 13:31:17');


--
-- Data for Name: sync; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.sync VALUES (25, 19, NULL, 'penerimaanbarang', 0, 1, NULL, NULL, NULL, '2018-09-04 11:58:48', '2018-09-12 08:58:55');
INSERT INTO public.sync VALUES (26, 8, NULL, 'loginlog', 0, 0, NULL, NULL, NULL, '2018-09-05 11:34:03', '2018-10-02 13:31:17');
INSERT INTO public.sync VALUES (21, 10, NULL, 'stokkapal', 0, 0, NULL, NULL, NULL, '2018-08-23 11:17:43', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (13, 4, NULL, 'stokkapal', 0, 0, NULL, NULL, NULL, '2018-08-13 11:41:55', '2018-10-02 09:56:05');
INSERT INTO public.sync VALUES (22, 7, NULL, 'stokkapal', 0, 0, NULL, NULL, NULL, '2018-08-23 11:40:16', '2018-10-02 13:27:01');
INSERT INTO public.sync VALUES (16, 11, NULL, 'item', 0, 1, NULL, NULL, NULL, '2018-08-19 05:26:23', '2018-08-19 05:26:23');
INSERT INTO public.sync VALUES (86, 41457, 41457, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:32', '2018-10-01 12:12:38');
INSERT INTO public.sync VALUES (80, 41451, 41451, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:24', '2018-10-01 12:12:32');
INSERT INTO public.sync VALUES (31, 34, NULL, 'stokkapal', 0, 0, NULL, NULL, NULL, '2018-09-12 02:48:37', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (12, 6, NULL, 'lokasiitem', 0, 0, NULL, NULL, NULL, '2018-08-13 10:58:48', '2018-10-02 13:24:41');
INSERT INTO public.sync VALUES (29, 14, NULL, 'penggunaanbarang', 0, 0, NULL, NULL, NULL, '2018-09-12 02:48:37', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (11, 1, NULL, 'stokkapal', 0, 0, NULL, NULL, NULL, '2018-08-13 10:56:22', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (60, 41622, 41622, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:48', '2018-10-01 12:12:20');
INSERT INTO public.sync VALUES (27, 16, NULL, 'loginlog', 0, 0, NULL, NULL, NULL, '2018-09-10 04:15:08', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (33, 20, NULL, 'loginlog', 0, 0, NULL, NULL, NULL, '2018-09-12 09:00:24', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (15, 5, NULL, 'stokkapal', 0, 0, NULL, NULL, NULL, '2018-08-13 12:15:10', '2018-10-02 09:56:05');
INSERT INTO public.sync VALUES (20, 21, 21, 'stokkapal', 0, 0, NULL, '2018-10-02 09:57:55', '2018-10-02 09:57:55', '2018-08-23 11:17:43', '2018-08-23 11:17:43');
INSERT INTO public.sync VALUES (175, 67, 67, 'loginlog', 0, 0, NULL, '2018-10-02 10:30:12', '2018-10-02 10:30:12', '2018-10-02 15:09:51', '2018-10-02 15:09:51');
INSERT INTO public.sync VALUES (24, 3, 3, 'users', 0, 0, NULL, '2018-10-02 09:57:56', '2018-10-02 09:57:56', '2018-08-28 02:53:37', '2018-10-02 05:38:13');
INSERT INTO public.sync VALUES (174, 31, 31, 'permintaanbarang', 0, 0, NULL, '2018-10-02 09:57:56', '2018-10-02 09:57:56', '2018-10-02 14:57:55', '2018-10-02 14:57:55');
INSERT INTO public.sync VALUES (17, 12, NULL, 'item', 0, 0, NULL, NULL, NULL, '2018-08-19 05:26:44', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (55, 41567, 41567, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:42', '2018-10-01 12:12:17');
INSERT INTO public.sync VALUES (34, 26, NULL, 'loginlog', 0, 0, NULL, NULL, NULL, '2018-09-14 03:25:29', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (93, 41464, 41464, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:35', '2018-10-01 12:12:40');
INSERT INTO public.sync VALUES (94, 41465, 41465, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:36', '2018-10-01 12:12:42');
INSERT INTO public.sync VALUES (95, 41466, 41466, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:37', '2018-10-01 12:12:43');
INSERT INTO public.sync VALUES (96, 41467, 41467, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:38', '2018-10-01 12:12:43');
INSERT INTO public.sync VALUES (97, 41468, 41468, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:38', '2018-10-01 12:12:44');
INSERT INTO public.sync VALUES (98, 41469, 41469, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:39', '2018-10-01 12:12:44');
INSERT INTO public.sync VALUES (99, 41470, 41470, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:40', '2018-10-01 12:12:45');
INSERT INTO public.sync VALUES (23, 25, 25, 'stokkapal', 0, 0, NULL, '2018-10-02 09:57:56', '2018-10-02 09:57:56', '2018-08-24 13:19:01', '2018-08-24 13:19:01');
INSERT INTO public.sync VALUES (36, 41644, 41644, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:37', '2018-10-01 12:12:13');
INSERT INTO public.sync VALUES (37, 41643, 41643, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:37', '2018-10-01 12:12:13');
INSERT INTO public.sync VALUES (38, 41642, 41642, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:37', '2018-10-01 12:12:13');
INSERT INTO public.sync VALUES (39, 41659, 41659, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:37', '2018-10-01 12:12:13');
INSERT INTO public.sync VALUES (40, 41658, 41658, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:37', '2018-10-01 12:12:13');
INSERT INTO public.sync VALUES (41, 41657, 41657, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:37', '2018-10-01 12:12:13');
INSERT INTO public.sync VALUES (43, 41655, 41655, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:37', '2018-10-01 12:12:13');
INSERT INTO public.sync VALUES (45, 41653, 41653, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:37', '2018-10-01 12:12:13');
INSERT INTO public.sync VALUES (53, 41565, 41565, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:38', '2018-10-01 12:12:14');
INSERT INTO public.sync VALUES (54, 41566, 41566, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:42', '2018-10-01 12:12:17');
INSERT INTO public.sync VALUES (87, 41458, 41458, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:32', '2018-10-01 12:12:39');
INSERT INTO public.sync VALUES (57, 41619, 41619, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:43', '2018-10-01 12:12:19');
INSERT INTO public.sync VALUES (58, 41620, 41620, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:45', '2018-10-01 12:12:19');
INSERT INTO public.sync VALUES (59, 41621, 41621, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:46', '2018-10-01 12:12:19');
INSERT INTO public.sync VALUES (61, 41433, 41433, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:54', '2018-10-01 12:12:20');
INSERT INTO public.sync VALUES (62, 41434, 41434, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:54', '2018-10-01 12:12:20');
INSERT INTO public.sync VALUES (32, 18, NULL, 'loginlog', 0, 0, NULL, NULL, NULL, '2018-09-12 06:19:07', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (63, 41435, 41435, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:55', '2018-10-01 12:12:21');
INSERT INTO public.sync VALUES (64, 41436, 41436, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:55', '2018-10-01 12:12:21');
INSERT INTO public.sync VALUES (65, 41437, 41437, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:55', '2018-10-01 12:12:21');
INSERT INTO public.sync VALUES (66, 41438, 41438, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:56', '2018-10-01 12:12:21');
INSERT INTO public.sync VALUES (68, 41440, 41440, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:06', '2018-10-01 12:12:23');
INSERT INTO public.sync VALUES (69, 41441, 41441, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:06', '2018-10-01 12:12:23');
INSERT INTO public.sync VALUES (70, 41442, 41442, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:12', '2018-10-01 12:12:25');
INSERT INTO public.sync VALUES (71, 41443, 41443, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:12', '2018-10-01 12:12:26');
INSERT INTO public.sync VALUES (72, 41444, 41444, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:13', '2018-10-01 12:12:26');
INSERT INTO public.sync VALUES (73, 41445, 41445, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:14', '2018-10-01 12:12:27');
INSERT INTO public.sync VALUES (74, 41446, 41446, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:14', '2018-10-01 12:12:27');
INSERT INTO public.sync VALUES (75, 41447, 41447, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:15', '2018-10-01 12:12:27');
INSERT INTO public.sync VALUES (76, 41482, 41482, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:15', '2018-10-01 12:12:27');
INSERT INTO public.sync VALUES (81, 41452, 41452, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:28', '2018-10-01 12:12:36');
INSERT INTO public.sync VALUES (82, 41453, 41453, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:29', '2018-10-01 12:12:36');
INSERT INTO public.sync VALUES (83, 41454, 41454, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:29', '2018-10-01 12:12:36');
INSERT INTO public.sync VALUES (84, 41455, 41455, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:29', '2018-10-01 12:12:36');
INSERT INTO public.sync VALUES (18, 29, NULL, 'stokkapal', 0, 0, NULL, NULL, NULL, '2018-08-23 10:54:15', '2018-10-01 17:10:01');
INSERT INTO public.sync VALUES (88, 41459, 41459, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:33', '2018-10-01 12:12:39');
INSERT INTO public.sync VALUES (89, 41460, 41460, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:33', '2018-10-01 12:12:39');
INSERT INTO public.sync VALUES (91, 41462, 41462, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:34', '2018-10-01 12:12:40');
INSERT INTO public.sync VALUES (92, 41463, 41463, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:34', '2018-10-01 12:12:40');
INSERT INTO public.sync VALUES (42, 41656, 41656, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:37', '2018-10-01 12:12:13');
INSERT INTO public.sync VALUES (56, 41618, 41618, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:43', '2018-10-01 12:12:18');
INSERT INTO public.sync VALUES (78, 41449, 41449, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:19', '2018-10-01 12:12:28');
INSERT INTO public.sync VALUES (79, 41450, 41450, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:23', '2018-10-01 12:12:31');
INSERT INTO public.sync VALUES (7, 2, NULL, 'item', 0, 0, NULL, NULL, NULL, '2018-08-08 15:39:49', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (85, 41456, 41456, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:30', '2018-10-01 12:12:38');
INSERT INTO public.sync VALUES (77, 41448, 41448, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:19', '2018-10-01 12:12:28');
INSERT INTO public.sync VALUES (30, 32, NULL, 'stokkapal', 0, 0, NULL, NULL, NULL, '2018-09-12 02:48:37', '2018-10-02 15:30:11');
INSERT INTO public.sync VALUES (19, 30, NULL, 'stokkapal', 0, 0, NULL, NULL, NULL, '2018-08-23 10:54:15', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (9, NULL, NULL, 'permintaanbarang', 0, 0, NULL, NULL, NULL, '2018-08-13 08:01:17', '2018-10-03 13:49:10');
INSERT INTO public.sync VALUES (10, NULL, NULL, 'workflow', 0, 0, NULL, NULL, NULL, '2018-08-13 08:01:17', '2018-10-02 15:30:11');
INSERT INTO public.sync VALUES (52, 41645, 41645, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:37', '2018-10-01 12:12:13');
INSERT INTO public.sync VALUES (67, 41439, 41439, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:56', '2018-10-01 12:12:22');
INSERT INTO public.sync VALUES (90, 41461, 41461, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:33', '2018-10-01 12:12:39');
INSERT INTO public.sync VALUES (100, 41471, 41471, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:41', '2018-10-01 12:12:46');
INSERT INTO public.sync VALUES (101, 41472, 41472, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:43', '2018-10-01 12:25:27');
INSERT INTO public.sync VALUES (102, 41473, 41473, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:44', '2018-10-01 12:25:30');
INSERT INTO public.sync VALUES (103, 41474, 41474, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:44', '2018-10-01 12:26:44');
INSERT INTO public.sync VALUES (104, 41475, 41475, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:46', '2018-10-01 12:26:47');
INSERT INTO public.sync VALUES (105, 41476, 41476, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:47', '2018-10-01 12:26:51');
INSERT INTO public.sync VALUES (106, 41477, 41477, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:48', '2018-10-01 12:26:54');
INSERT INTO public.sync VALUES (107, 41478, 41478, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:48', '2018-10-01 12:26:57');
INSERT INTO public.sync VALUES (108, 41479, 41479, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:49', '2018-10-01 12:27:00');
INSERT INTO public.sync VALUES (109, 41480, 41480, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:49', '2018-10-01 12:27:04');
INSERT INTO public.sync VALUES (110, 41481, 41481, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:49', '2018-10-01 12:27:04');
INSERT INTO public.sync VALUES (127, 41499, 41499, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:49', '2018-10-01 12:27:04');
INSERT INTO public.sync VALUES (128, 41500, 41500, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:49', '2018-10-01 12:27:04');
INSERT INTO public.sync VALUES (129, 41501, 41501, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:49', '2018-10-01 12:27:04');
INSERT INTO public.sync VALUES (130, 41502, 41502, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:49', '2018-10-01 12:27:04');
INSERT INTO public.sync VALUES (131, 41503, 41503, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:49', '2018-10-01 12:27:04');
INSERT INTO public.sync VALUES (132, 42001, 42001, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:49', '2018-10-01 12:27:04');
INSERT INTO public.sync VALUES (133, 43001, 43001, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:49', '2018-10-01 12:27:04');
INSERT INTO public.sync VALUES (134, 41282, 41282, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:50', '2018-10-01 12:27:04');
INSERT INTO public.sync VALUES (167, 56, NULL, 'administratormenu', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (111, 41483, 41483, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:49', '2018-10-01 12:27:04');
INSERT INTO public.sync VALUES (172, 41, NULL, 'administratormenu', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (112, 41484, 41484, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:49', '2018-10-01 12:27:04');
INSERT INTO public.sync VALUES (135, 27, 27, 'permintaanbarang', 0, 0, NULL, '2018-10-02 09:57:56', '2018-10-02 09:57:56', '2018-10-01 16:54:21', '2018-10-01 16:54:21');
INSERT INTO public.sync VALUES (155, 42, NULL, 'administratormenu', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (157, 44, NULL, 'administratormenu', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (154, 40, NULL, 'administratormenu', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (142, 166, NULL, 'kapal', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 09:56:53');
INSERT INTO public.sync VALUES (35, 41660, 41660, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:37', '2018-10-01 12:12:13');
INSERT INTO public.sync VALUES (44, 41654, 41654, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:37', '2018-10-01 12:12:13');
INSERT INTO public.sync VALUES (113, 41485, 41485, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:49', '2018-10-01 12:27:04');
INSERT INTO public.sync VALUES (114, 41486, 41486, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:49', '2018-10-01 12:27:04');
INSERT INTO public.sync VALUES (115, 41487, 41487, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:49', '2018-10-01 12:27:04');
INSERT INTO public.sync VALUES (116, 41488, 41488, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:49', '2018-10-01 12:27:04');
INSERT INTO public.sync VALUES (117, 41489, 41489, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:49', '2018-10-01 12:27:04');
INSERT INTO public.sync VALUES (118, 41490, 41490, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:49', '2018-10-01 12:27:04');
INSERT INTO public.sync VALUES (119, 41491, 41491, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:49', '2018-10-01 12:27:04');
INSERT INTO public.sync VALUES (120, 41492, 41492, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:49', '2018-10-01 12:27:04');
INSERT INTO public.sync VALUES (141, 153, NULL, 'kapal', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 09:56:53');
INSERT INTO public.sync VALUES (46, 41652, 41652, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:37', '2018-10-01 12:12:13');
INSERT INTO public.sync VALUES (47, 41651, 41651, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:37', '2018-10-01 12:12:13');
INSERT INTO public.sync VALUES (48, 41650, 41650, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:37', '2018-10-01 12:12:13');
INSERT INTO public.sync VALUES (49, 41649, 41649, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:37', '2018-10-01 12:12:13');
INSERT INTO public.sync VALUES (50, 41648, 41648, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:37', '2018-10-01 12:12:13');
INSERT INTO public.sync VALUES (51, 41647, 41647, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:29:37', '2018-10-01 12:12:13');
INSERT INTO public.sync VALUES (121, 41493, 41493, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:49', '2018-10-01 12:27:04');
INSERT INTO public.sync VALUES (122, 41494, 41494, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:49', '2018-10-01 12:27:04');
INSERT INTO public.sync VALUES (123, 41495, 41495, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:49', '2018-10-01 12:27:04');
INSERT INTO public.sync VALUES (124, 41496, 41496, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:49', '2018-10-01 12:27:04');
INSERT INTO public.sync VALUES (125, 41497, 41497, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:49', '2018-10-01 12:27:04');
INSERT INTO public.sync VALUES (126, 41498, 41498, 'item', 0, 0, NULL, NULL, NULL, '2018-10-01 11:30:49', '2018-10-01 12:27:04');
INSERT INTO public.sync VALUES (168, 57, NULL, 'administratormenu', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (170, 59, NULL, 'administratormenu', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (136, 28, NULL, 'permintaanbarang', 0, 0, NULL, NULL, NULL, '2018-10-01 16:57:40', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (158, 47, NULL, 'administratormenu', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (156, 43, NULL, 'administratormenu', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (171, 46, NULL, 'administratormenu', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (169, 58, NULL, 'administratormenu', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (173, 60, NULL, 'administratormenu', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (143, 177, NULL, 'kapal', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 09:56:53');
INSERT INTO public.sync VALUES (144, 152, NULL, 'kapal', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 09:56:53');
INSERT INTO public.sync VALUES (145, 171, NULL, 'kapal', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 09:56:53');
INSERT INTO public.sync VALUES (146, 151, NULL, 'kapal', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 09:56:53');
INSERT INTO public.sync VALUES (147, 119, NULL, 'kapal', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 09:56:53');
INSERT INTO public.sync VALUES (148, 176, NULL, 'kapal', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 09:56:53');
INSERT INTO public.sync VALUES (149, 168, NULL, 'kapal', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 09:56:53');
INSERT INTO public.sync VALUES (150, 165, NULL, 'kapal', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 09:56:53');
INSERT INTO public.sync VALUES (151, 167, NULL, 'kapal', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 09:56:53');
INSERT INTO public.sync VALUES (153, 38, NULL, 'administratormenu', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (140, 150, NULL, 'kapal', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 09:56:53');
INSERT INTO public.sync VALUES (152, 36, NULL, 'administratormenu', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (137, 63, 63, 'loginlog', 0, 0, NULL, '2018-10-02 09:57:56', '2018-10-02 09:57:56', '2018-10-02 05:08:44', '2018-10-02 05:08:44');
INSERT INTO public.sync VALUES (138, 64, 64, 'loginlog', 0, 0, NULL, '2018-10-02 09:57:56', '2018-10-02 09:57:56', '2018-10-02 05:10:28', '2018-10-02 05:10:28');
INSERT INTO public.sync VALUES (139, 65, 65, 'loginlog', 0, 0, NULL, '2018-10-02 09:57:56', '2018-10-02 09:57:56', '2018-10-02 05:16:06', '2018-10-02 05:16:06');
INSERT INTO public.sync VALUES (160, 49, NULL, 'administratormenu', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (161, 50, NULL, 'administratormenu', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (162, 51, NULL, 'administratormenu', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (163, 52, NULL, 'administratormenu', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (176, 68, 68, 'loginlog', 0, 0, NULL, '2018-10-02 10:30:12', '2018-10-02 10:30:12', '2018-10-02 15:10:38', '2018-10-02 15:10:38');
INSERT INTO public.sync VALUES (164, 53, NULL, 'administratormenu', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (165, 54, NULL, 'administratormenu', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (166, 55, NULL, 'administratormenu', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 10:07:02');
INSERT INTO public.sync VALUES (159, 48, NULL, 'administratormenu', 0, 0, NULL, NULL, NULL, '2018-10-02 05:40:21', '2018-10-02 10:07:02');


--
-- Data for Name: sync_table; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.sync_table VALUES (3, 'users', '2018-09-04 11:09:34.430156');
INSERT INTO public.sync_table VALUES (6, 'administratorgroupaccess', '2018-09-26 09:49:25.853578');
INSERT INTO public.sync_table VALUES (7, 'roles', '2018-10-02 15:19:02.102792');
INSERT INTO public.sync_table VALUES (1, 'item', '2018-09-21 00:00:00');
INSERT INTO public.sync_table VALUES (4, 'lokasiitem', '2018-09-21 00:00:00');
INSERT INTO public.sync_table VALUES (10, 'voyages', '2017-01-16 00:00:00');
INSERT INTO public.sync_table VALUES (11, 'itemonhand', '2018-10-02 11:54:38');
INSERT INTO public.sync_table VALUES (9, 'popenerimaanio', '2001-01-01 01:01:01');
INSERT INTO public.sync_table VALUES (8, 'popenerimaan', '2001-01-01 01:01:01');
INSERT INTO public.sync_table VALUES (2, 'kapal', '2018-09-27 00:00:00');
INSERT INTO public.sync_table VALUES (5, 'administratormenu
', '2018-09-26 09:38:37');


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.users VALUES (1, 'Administrator', 1, 177, 'sioado', '$2y$12$U5BsEAXwbNHZgvSZx.Bqre8aXUgzLzNShrJ.3hXQZMcFSQh/unDP2', NULL, '', 'KAPAL', 'Nahkoda', '45621');
INSERT INTO public.users VALUES (3, 'Janang Dua', 2, 177, 'janang2', '$2y$12$EFxG2uP0NhxU/OYsQlVTLOPC/Fnr65ZH.41Ldw5z7QcdbIxsXtIee', '2018-09-04 11:09:34.430156', '', 'KAPAL', 'Janang', '23992');
INSERT INTO public.users VALUES (2, 'Seth Setiadha', 3, 177, 'sethsetiadha', '$2y$12$U5BsEAXwbNHZgvSZx.Bqre8aXUgzLzNShrJ.3hXQZMcFSQh/unDP2', NULL, '3', 'KAPAL', NULL, NULL);
INSERT INTO public.users VALUES (4, 'daniel', 1, 177, 'Daniel', '$2y$12$m23NJrMdhbY0/QMzsDMy7ONiGFCjPt60EktL/5KJPt2kNgmGgvG0K', '2018-09-14 09:32:04.611558', '', 'KAPAL', 'Pekerja', '1122334455');
INSERT INTO public.users VALUES (5, 'agung', 1, 177, 'agung', '$2y$12$KSfXn1EOpEpQgIqbh01KJuASYmRDmjmS3cWt.FD5b0qkvM6OKg6z.', '2018-09-14 09:33:38.612117', '', 'KAPAL', 'pekerja', '223344');
INSERT INTO public.users VALUES (3, 'daniel', 2, 177, 'daniel', '$2y$12$d9ntpXzLyAwjpTwb3/J9dexU7AAjBu2/EAqTXNDDRkDMt7fIgorHK', '2018-09-06 09:16:07.749226', '', 'KAPAL', 'pekerja', '12345');


--
-- Data for Name: voyages; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.voyages VALUES (2, '0011', 'Voyage 001 - Call 1', NULL);
INSERT INTO public.voyages VALUES (3, '0012', 'Voyage 001 - Call 2', NULL);
INSERT INTO public.voyages VALUES (4, '0013', 'Voyage 001 - Call 3', NULL);
INSERT INTO public.voyages VALUES (5, '0014', 'Voyage 001 - Call 4', NULL);
INSERT INTO public.voyages VALUES (6, '0021', 'Voyage 002 - Call 1', NULL);
INSERT INTO public.voyages VALUES (7, '0022', 'Voyage 002 - Call 2', NULL);
INSERT INTO public.voyages VALUES (8, '0023', 'Voyage 002 - Call 3', NULL);
INSERT INTO public.voyages VALUES (9, '0024', 'Voyage 002 - Call 4', NULL);
INSERT INTO public.voyages VALUES (10, '0031', 'Voyage 003 - Call 1', NULL);
INSERT INTO public.voyages VALUES (11, '0032', 'Voyage 003 - Call 2', NULL);
INSERT INTO public.voyages VALUES (12, '0033', 'Voyage 003 - Call 3', NULL);
INSERT INTO public.voyages VALUES (13, '0034', 'Voyage 003 - Call 4', NULL);
INSERT INTO public.voyages VALUES (14, '0041', 'Voyage 004 - Call 1', NULL);
INSERT INTO public.voyages VALUES (15, '0042', 'Voyage 004 - Call 2', NULL);
INSERT INTO public.voyages VALUES (16, '0043', 'Voyage 004 - Call 3', NULL);
INSERT INTO public.voyages VALUES (17, '0044', 'Voyage 004 - Call 4', NULL);
INSERT INTO public.voyages VALUES (18, '0051', 'Voyage 005 - Call 1', NULL);
INSERT INTO public.voyages VALUES (19, '0052', 'Voyage 005 - Call 2', NULL);
INSERT INTO public.voyages VALUES (20, '0053', 'Voyage 005 - Call 3', NULL);
INSERT INTO public.voyages VALUES (21, '0054', 'Voyage 005 - Call 4', NULL);
INSERT INTO public.voyages VALUES (22, '0061', 'Voyage 006 - Call 1', NULL);
INSERT INTO public.voyages VALUES (23, '0062', 'Voyage 006 - Call 2', NULL);
INSERT INTO public.voyages VALUES (24, '0063', 'Voyage 006 - Call 3', NULL);
INSERT INTO public.voyages VALUES (25, '0064', 'Voyage 006 - Call 4', NULL);
INSERT INTO public.voyages VALUES (26, '0071', 'Voyage 007 - Call 1', NULL);
INSERT INTO public.voyages VALUES (27, '0072', 'Voyage 007 - Call 2', NULL);
INSERT INTO public.voyages VALUES (28, '0073', 'Voyage 007 - Call 3', NULL);
INSERT INTO public.voyages VALUES (29, '0074', 'Voyage 007 - Call 4', NULL);
INSERT INTO public.voyages VALUES (30, '0081', 'Voyage 008 - Call 1', NULL);
INSERT INTO public.voyages VALUES (31, '0082', 'Voyage 008 - Call 2', NULL);
INSERT INTO public.voyages VALUES (32, '0083', 'Voyage 008 - Call 3', NULL);
INSERT INTO public.voyages VALUES (33, '0084', 'Voyage 008 - Call 4', NULL);
INSERT INTO public.voyages VALUES (34, '0091', 'Voyage 009 - Call 1', NULL);
INSERT INTO public.voyages VALUES (35, '0092', 'Voyage 009 - Call 2', NULL);
INSERT INTO public.voyages VALUES (36, '0093', 'Voyage 009 - Call 3', NULL);
INSERT INTO public.voyages VALUES (37, '0094', 'Voyage 009 - Call 4', NULL);
INSERT INTO public.voyages VALUES (38, '0101', 'Voyage 010 - Call 1', NULL);
INSERT INTO public.voyages VALUES (39, '0102', 'Voyage 010 - Call 2', NULL);
INSERT INTO public.voyages VALUES (40, '0103', 'Voyage 010 - Call 3', NULL);
INSERT INTO public.voyages VALUES (41, '0104', 'Voyage 010 - Call 4', NULL);
INSERT INTO public.voyages VALUES (42, '0111', 'Voyage 011 - Call 1', NULL);
INSERT INTO public.voyages VALUES (43, '0112', 'Voyage 011 - Call 2', NULL);
INSERT INTO public.voyages VALUES (44, '0113', 'Voyage 011 - Call 3', NULL);
INSERT INTO public.voyages VALUES (45, '0114', 'Voyage 011 - Call 4', NULL);
INSERT INTO public.voyages VALUES (46, '0121', 'Voyage 012 - Call 1', NULL);
INSERT INTO public.voyages VALUES (47, '0122', 'Voyage 012 - Call 2', NULL);
INSERT INTO public.voyages VALUES (48, '0123', 'Voyage 012 - Call 3', NULL);
INSERT INTO public.voyages VALUES (49, '0124', 'Voyage 012 - Call 4', NULL);
INSERT INTO public.voyages VALUES (50, '0131', 'Voyage 013 - Call 1', NULL);
INSERT INTO public.voyages VALUES (51, '0132', 'Voyage 013 - Call 2', NULL);
INSERT INTO public.voyages VALUES (52, '0133', 'Voyage 013 - Call 3', NULL);
INSERT INTO public.voyages VALUES (53, '0134', 'Voyage 013 - Call 4', NULL);
INSERT INTO public.voyages VALUES (54, '0141', 'Voyage 014 - Call 1', NULL);
INSERT INTO public.voyages VALUES (55, '0142', 'Voyage 014 - Call 2', NULL);
INSERT INTO public.voyages VALUES (56, '0143', 'Voyage 014 - Call 3', NULL);
INSERT INTO public.voyages VALUES (57, '0144', 'Voyage 014 - Call 4', NULL);
INSERT INTO public.voyages VALUES (58, '0151', 'Voyage 015 - Call 1', NULL);
INSERT INTO public.voyages VALUES (59, '0152', 'Voyage 015 - Call 2', NULL);
INSERT INTO public.voyages VALUES (60, '0153', 'Voyage 015 - Call 3', NULL);
INSERT INTO public.voyages VALUES (61, '0154', 'Voyage 015 - Call 4', NULL);
INSERT INTO public.voyages VALUES (62, '0161', 'Voyage 016 - Call 1', NULL);
INSERT INTO public.voyages VALUES (63, '0162', 'Voyage 016 - Call 2', NULL);
INSERT INTO public.voyages VALUES (64, '0163', 'Voyage 016 - Call 3', NULL);
INSERT INTO public.voyages VALUES (65, '0164', 'Voyage 016 - Call 4', NULL);
INSERT INTO public.voyages VALUES (66, '0171', 'Voyage 017 - Call 1', NULL);
INSERT INTO public.voyages VALUES (67, '0172', 'Voyage 017 - Call 2', NULL);
INSERT INTO public.voyages VALUES (68, '0173', 'Voyage 017 - Call 3', NULL);
INSERT INTO public.voyages VALUES (69, '0174', 'Voyage 017 - Call 4', NULL);
INSERT INTO public.voyages VALUES (70, '0181', 'Voyage 018 - Call 1', NULL);
INSERT INTO public.voyages VALUES (71, '0182', 'Voyage 018 - Call 2', NULL);
INSERT INTO public.voyages VALUES (72, '0183', 'Voyage 018 - Call 3', NULL);
INSERT INTO public.voyages VALUES (73, '0184', 'Voyage 018 - Call 4', NULL);
INSERT INTO public.voyages VALUES (74, '0191', 'Voyage 019 - Call 1', NULL);
INSERT INTO public.voyages VALUES (75, '0192', 'Voyage 019 - Call 2', NULL);
INSERT INTO public.voyages VALUES (76, '0193', 'Voyage 019 - Call 3', NULL);
INSERT INTO public.voyages VALUES (77, '0194', 'Voyage 019 - Call 4', NULL);
INSERT INTO public.voyages VALUES (78, '0201', 'Voyage 020 - Call 1', NULL);
INSERT INTO public.voyages VALUES (79, '0202', 'Voyage 020 - Call 2', NULL);
INSERT INTO public.voyages VALUES (80, '0203', 'Voyage 020 - Call 3', NULL);
INSERT INTO public.voyages VALUES (81, '0204', 'Voyage 020 - Call 4', NULL);
INSERT INTO public.voyages VALUES (82, '0211', 'Voyage 021 - Call 1', NULL);
INSERT INTO public.voyages VALUES (83, '0212', 'Voyage 021 - Call 2', NULL);
INSERT INTO public.voyages VALUES (84, '0213', 'Voyage 021 - Call 3', NULL);
INSERT INTO public.voyages VALUES (85, '0214', 'Voyage 021 - Call 4', NULL);
INSERT INTO public.voyages VALUES (86, '0221', 'Voyage 022 - Call 1', NULL);
INSERT INTO public.voyages VALUES (87, '0222', 'Voyage 022 - Call 2', NULL);
INSERT INTO public.voyages VALUES (88, '0223', 'Voyage 022 - Call 3', NULL);
INSERT INTO public.voyages VALUES (89, '0224', 'Voyage 022 - Call 4', NULL);
INSERT INTO public.voyages VALUES (90, '0231', 'Voyage 023 - Call 1', NULL);
INSERT INTO public.voyages VALUES (91, '0232', 'Voyage 023 - Call 2', NULL);
INSERT INTO public.voyages VALUES (92, '0233', 'Voyage 023 - Call 3', NULL);
INSERT INTO public.voyages VALUES (93, '0234', 'Voyage 023 - Call 4', NULL);
INSERT INTO public.voyages VALUES (94, '0241', 'Voyage 024 - Call 1', NULL);
INSERT INTO public.voyages VALUES (95, '0242', 'Voyage 024 - Call 2', NULL);
INSERT INTO public.voyages VALUES (96, '0243', 'Voyage 024 - Call 3', NULL);
INSERT INTO public.voyages VALUES (97, '0244', 'Voyage 024 - Call 4', NULL);
INSERT INTO public.voyages VALUES (98, '0251', 'Voyage 025 - Call 1', NULL);
INSERT INTO public.voyages VALUES (99, '0252', 'Voyage 025 - Call 2', NULL);
INSERT INTO public.voyages VALUES (100, '0253', 'Voyage 025 - Call 3', NULL);
INSERT INTO public.voyages VALUES (101, '0254', 'Voyage 025 - Call 4', NULL);
INSERT INTO public.voyages VALUES (102, '0261', 'Voyage 026 - Call 1', NULL);
INSERT INTO public.voyages VALUES (103, '0262', 'Voyage 026 - Call 2', NULL);
INSERT INTO public.voyages VALUES (104, '0263', 'Voyage 026 - Call 3', NULL);
INSERT INTO public.voyages VALUES (105, '0264', 'Voyage 026 - Call 4', NULL);
INSERT INTO public.voyages VALUES (106, '0271', 'Voyage 027 - Call 1', NULL);
INSERT INTO public.voyages VALUES (107, '0272', 'Voyage 027 - Call 2', NULL);
INSERT INTO public.voyages VALUES (108, '0273', 'Voyage 027 - Call 3', NULL);
INSERT INTO public.voyages VALUES (109, '0274', 'Voyage 027 - Call 4', NULL);
INSERT INTO public.voyages VALUES (110, '0281', 'Voyage 028 - Call 1', NULL);
INSERT INTO public.voyages VALUES (111, '0282', 'Voyage 028 - Call 2', NULL);
INSERT INTO public.voyages VALUES (112, '0283', 'Voyage 028 - Call 3', NULL);
INSERT INTO public.voyages VALUES (113, '0284', 'Voyage 028 - Call 4', NULL);
INSERT INTO public.voyages VALUES (114, '0291', 'Voyage 029 - Call 1', NULL);
INSERT INTO public.voyages VALUES (115, '0292', 'Voyage 029 - Call 2', NULL);
INSERT INTO public.voyages VALUES (116, '0293', 'Voyage 029 - Call 3', NULL);
INSERT INTO public.voyages VALUES (117, '0294', 'Voyage 029 - Call 4', NULL);
INSERT INTO public.voyages VALUES (118, '0301', 'Voyage 030 - Call 1', NULL);
INSERT INTO public.voyages VALUES (119, '0302', 'Voyage 030 - Call 2', NULL);
INSERT INTO public.voyages VALUES (120, '0303', 'Voyage 030 - Call 3', NULL);
INSERT INTO public.voyages VALUES (121, '0304', 'Voyage 030 - Call 4', NULL);
INSERT INTO public.voyages VALUES (122, '0311', 'Voyage 031 - Call 1', NULL);
INSERT INTO public.voyages VALUES (123, '0312', 'Voyage 031 - Call 2', NULL);
INSERT INTO public.voyages VALUES (124, '0313', 'Voyage 031 - Call 3', NULL);
INSERT INTO public.voyages VALUES (125, '0314', 'Voyage 031 - Call 4', NULL);
INSERT INTO public.voyages VALUES (126, '0321', 'Voyage 032 - Call 1', NULL);
INSERT INTO public.voyages VALUES (127, '0322', 'Voyage 032 - Call 2', NULL);
INSERT INTO public.voyages VALUES (128, '0323', 'Voyage 032 - Call 3', NULL);
INSERT INTO public.voyages VALUES (129, '0324', 'Voyage 032 - Call 4', NULL);
INSERT INTO public.voyages VALUES (130, '0331', 'Voyage 033 - Call 1', NULL);
INSERT INTO public.voyages VALUES (131, '0332', 'Voyage 033 - Call 2', NULL);
INSERT INTO public.voyages VALUES (132, '0333', 'Voyage 033 - Call 3', NULL);
INSERT INTO public.voyages VALUES (133, '0334', 'Voyage 033 - Call 4', NULL);
INSERT INTO public.voyages VALUES (134, '0341', 'Voyage 034 - Call 1', NULL);
INSERT INTO public.voyages VALUES (135, '0342', 'Voyage 034 - Call 2', NULL);
INSERT INTO public.voyages VALUES (136, '0343', 'Voyage 034 - Call 3', NULL);
INSERT INTO public.voyages VALUES (137, '0344', 'Voyage 034 - Call 4', NULL);
INSERT INTO public.voyages VALUES (138, '0351', 'Voyage 035 - Call 1', NULL);
INSERT INTO public.voyages VALUES (139, '0352', 'Voyage 035 - Call 2', NULL);
INSERT INTO public.voyages VALUES (140, '0353', 'Voyage 035 - Call 3', NULL);
INSERT INTO public.voyages VALUES (141, '0354', 'Voyage 035 - Call 4', NULL);
INSERT INTO public.voyages VALUES (142, '0361', 'Voyage 036 - Call 1', NULL);
INSERT INTO public.voyages VALUES (143, '0362', 'Voyage 036 - Call 2', NULL);
INSERT INTO public.voyages VALUES (144, '0363', 'Voyage 036 - Call 3', NULL);
INSERT INTO public.voyages VALUES (145, '0364', 'Voyage 036 - Call 4', NULL);
INSERT INTO public.voyages VALUES (146, '0371', 'Voyage 037 - Call 1', NULL);
INSERT INTO public.voyages VALUES (147, '0372', 'Voyage 037 - Call 2', NULL);
INSERT INTO public.voyages VALUES (148, '0373', 'Voyage 037 - Call 3', NULL);
INSERT INTO public.voyages VALUES (149, '0374', 'Voyage 037 - Call 4', NULL);
INSERT INTO public.voyages VALUES (150, '0381', 'Voyage 038 - Call 1', NULL);
INSERT INTO public.voyages VALUES (151, '0382', 'Voyage 038 - Call 2', NULL);
INSERT INTO public.voyages VALUES (152, '0383', 'Voyage 038 - Call 3', NULL);
INSERT INTO public.voyages VALUES (153, '0384', 'Voyage 038 - Call 4', NULL);
INSERT INTO public.voyages VALUES (154, '0391', 'Voyage 039 - Call 1', NULL);
INSERT INTO public.voyages VALUES (155, '0392', 'Voyage 039 - Call 2', NULL);
INSERT INTO public.voyages VALUES (156, '0393', 'Voyage 039 - Call 3', NULL);
INSERT INTO public.voyages VALUES (157, '0394', 'Voyage 039 - Call 4', NULL);
INSERT INTO public.voyages VALUES (158, '0401', 'Voyage 040 - Call 1', NULL);
INSERT INTO public.voyages VALUES (159, '0402', 'Voyage 040 - Call 2', NULL);
INSERT INTO public.voyages VALUES (160, '0403', 'Voyage 040 - Call 3', NULL);
INSERT INTO public.voyages VALUES (161, '0404', 'Voyage 040 - Call 4', NULL);
INSERT INTO public.voyages VALUES (162, '0411', 'Voyage 041 - Call 1', NULL);
INSERT INTO public.voyages VALUES (163, '0412', 'Voyage 041 - Call 2', NULL);
INSERT INTO public.voyages VALUES (164, '0413', 'Voyage 041 - Call 3', NULL);
INSERT INTO public.voyages VALUES (165, '0414', 'Voyage 041 - Call 4', NULL);
INSERT INTO public.voyages VALUES (166, '0421', 'Voyage 042 - Call 1', NULL);
INSERT INTO public.voyages VALUES (167, '0422', 'Voyage 042 - Call 2', NULL);
INSERT INTO public.voyages VALUES (168, '0423', 'Voyage 042 - Call 3', NULL);
INSERT INTO public.voyages VALUES (169, '0424', 'Voyage 042 - Call 4', NULL);
INSERT INTO public.voyages VALUES (170, '0431', 'Voyage 043 - Call 1', NULL);
INSERT INTO public.voyages VALUES (171, '0432', 'Voyage 043 - Call 2', NULL);
INSERT INTO public.voyages VALUES (172, '0433', 'Voyage 043 - Call 3', NULL);
INSERT INTO public.voyages VALUES (173, '0434', 'Voyage 043 - Call 4', NULL);
INSERT INTO public.voyages VALUES (174, '0441', 'Voyage 044 - Call 1', NULL);
INSERT INTO public.voyages VALUES (175, '0442', 'Voyage 044 - Call 2', NULL);
INSERT INTO public.voyages VALUES (176, '0443', 'Voyage 044 - Call 3', NULL);
INSERT INTO public.voyages VALUES (177, '0444', 'Voyage 044 - Call 4', NULL);
INSERT INTO public.voyages VALUES (178, '0451', 'Voyage 045 - Call 1', NULL);
INSERT INTO public.voyages VALUES (179, '0452', 'Voyage 045 - Call 2', NULL);
INSERT INTO public.voyages VALUES (180, '0453', 'Voyage 045 - Call 3', NULL);
INSERT INTO public.voyages VALUES (181, '0454', 'Voyage 045 - Call 4', NULL);
INSERT INTO public.voyages VALUES (182, '0461', 'Voyage 046 - Call 1', NULL);
INSERT INTO public.voyages VALUES (183, '0462', 'Voyage 046 - Call 2', NULL);
INSERT INTO public.voyages VALUES (184, '0463', 'Voyage 046 - Call 3', NULL);
INSERT INTO public.voyages VALUES (185, '0464', 'Voyage 046 - Call 4', NULL);
INSERT INTO public.voyages VALUES (186, '0471', 'Voyage 047 - Call 1', NULL);
INSERT INTO public.voyages VALUES (187, '0472', 'Voyage 047 - Call 2', NULL);
INSERT INTO public.voyages VALUES (188, '0473', 'Voyage 047 - Call 3', NULL);
INSERT INTO public.voyages VALUES (189, '0474', 'Voyage 047 - Call 4', NULL);
INSERT INTO public.voyages VALUES (190, '0481', 'Voyage 048 - Call 1', NULL);
INSERT INTO public.voyages VALUES (191, '0482', 'Voyage 048 - Call 2', NULL);
INSERT INTO public.voyages VALUES (192, '0483', 'Voyage 048 - Call 3', NULL);
INSERT INTO public.voyages VALUES (193, '0484', 'Voyage 048 - Call 4', NULL);
INSERT INTO public.voyages VALUES (194, '0491', 'Voyage 049 - Call 1', NULL);
INSERT INTO public.voyages VALUES (195, '0492', 'Voyage 049 - Call 2', NULL);
INSERT INTO public.voyages VALUES (196, '0493', 'Voyage 049 - Call 3', NULL);
INSERT INTO public.voyages VALUES (197, '0494', 'Voyage 049 - Call 4', NULL);
INSERT INTO public.voyages VALUES (198, '0501', 'Voyage 050 - Call 1', NULL);
INSERT INTO public.voyages VALUES (199, '0502', 'Voyage 050 - Call 2', NULL);
INSERT INTO public.voyages VALUES (200, '0503', 'Voyage 050 - Call 3', NULL);
INSERT INTO public.voyages VALUES (201, '0504', 'Voyage 050 - Call 4', NULL);
INSERT INTO public.voyages VALUES (202, 'TVC', 'Total Voyage Call', NULL);
INSERT INTO public.voyages VALUES (203, '0135', 'Voyage 013 - Call 5', NULL);
INSERT INTO public.voyages VALUES (204, '0066', 'Voyage 006 - Call 6', NULL);
INSERT INTO public.voyages VALUES (205, '0069', 'Voyage 006 - Call 9', NULL);
INSERT INTO public.voyages VALUES (206, '0155', 'Voyage 015 - Call 5', NULL);
INSERT INTO public.voyages VALUES (207, '0245', 'Voyage 024 - Call 5', NULL);
INSERT INTO public.voyages VALUES (208, '0068', 'Voyage 006 - Call 8', NULL);
INSERT INTO public.voyages VALUES (209, '0116', 'Voyage 011 - Call 6', NULL);
INSERT INTO public.voyages VALUES (210, '0126', 'Voyage 012 - Call 6', NULL);
INSERT INTO public.voyages VALUES (211, '0247', 'Voyage 024 - Call 7', NULL);
INSERT INTO public.voyages VALUES (212, '0015', 'Voyage 001 - Call 5', NULL);
INSERT INTO public.voyages VALUES (213, '0016', 'Voyage 001 - Call 6', NULL);
INSERT INTO public.voyages VALUES (214, '0017', 'Voyage 001 - Call 7', NULL);
INSERT INTO public.voyages VALUES (215, '0018', 'Voyage 001 - Call 8', NULL);
INSERT INTO public.voyages VALUES (216, '0019', 'Voyage 001 - Call 9', NULL);
INSERT INTO public.voyages VALUES (217, '0025', 'Voyage 002 - Call 5', NULL);
INSERT INTO public.voyages VALUES (218, '0026', 'Voyage 002 - Call 6', NULL);
INSERT INTO public.voyages VALUES (219, '0028', 'Voyage 002 - Call 8', NULL);
INSERT INTO public.voyages VALUES (220, '0035', 'Voyage 003 - Call 5', NULL);
INSERT INTO public.voyages VALUES (221, '0036', 'Voyage 003 - Call 6', NULL);
INSERT INTO public.voyages VALUES (222, '0065', 'Voyage 006 - Call 5', NULL);
INSERT INTO public.voyages VALUES (223, '0255', 'Voyage 025 - Call 5', NULL);
INSERT INTO public.voyages VALUES (224, '0115', 'Voyage 011 - Call 5', NULL);
INSERT INTO public.voyages VALUES (225, '0067', 'Voyage 006 - Call 7', NULL);
INSERT INTO public.voyages VALUES (226, '0125', 'Voyage 012 - Call 5', NULL);
INSERT INTO public.voyages VALUES (227, '0175', 'Voyage 017 - Call 5', NULL);
INSERT INTO public.voyages VALUES (228, '0345', 'Voyage 034 - Call 5', NULL);
INSERT INTO public.voyages VALUES (229, '0346', 'Voyage 034 - Call 6', NULL);
INSERT INTO public.voyages VALUES (230, '0347', 'Voyage 034 - Call 7', NULL);
INSERT INTO public.voyages VALUES (231, '0348', 'Voyage 034 - Call 8', NULL);
INSERT INTO public.voyages VALUES (232, '0349', 'Voyage 034 - Call 9', NULL);
INSERT INTO public.voyages VALUES (233, '0355', 'Voyage 035 - Call 5', NULL);
INSERT INTO public.voyages VALUES (234, '0356', 'Voyage 035 - Call 6', NULL);
INSERT INTO public.voyages VALUES (235, '0357', 'Voyage 035 - Call 7', NULL);
INSERT INTO public.voyages VALUES (236, '0358', 'Voyage 035 - Call 8', NULL);
INSERT INTO public.voyages VALUES (237, '0359', 'Voyage 035 - Call 9', NULL);
INSERT INTO public.voyages VALUES (238, '0055', 'Voyage 005 - Call 5', NULL);
INSERT INTO public.voyages VALUES (239, '0056', 'Voyage 005 - Call 6', NULL);
INSERT INTO public.voyages VALUES (240, '0057', 'Voyage 005 - Call 7', NULL);
INSERT INTO public.voyages VALUES (241, '0058', 'Voyage 005 - Call 8', NULL);
INSERT INTO public.voyages VALUES (242, '0059', 'Voyage 005 - Call 9', NULL);
INSERT INTO public.voyages VALUES (243, '0075', 'Voyage 007 - Call 5', NULL);
INSERT INTO public.voyages VALUES (244, '0076', 'Voyage 007 - Call 6', NULL);
INSERT INTO public.voyages VALUES (245, '0077', 'Voyage 007 - Call 7', NULL);
INSERT INTO public.voyages VALUES (246, '0078', 'Voyage 007 - Call 8', NULL);
INSERT INTO public.voyages VALUES (247, '0079', 'Voyage 007 - Call 9', NULL);
INSERT INTO public.voyages VALUES (248, '0085', 'Voyage 008 - Call 5', NULL);
INSERT INTO public.voyages VALUES (249, '0086', 'Voyage 008 - Call 6', NULL);
INSERT INTO public.voyages VALUES (250, '0087', 'Voyage 008 - Call 7', NULL);
INSERT INTO public.voyages VALUES (251, '0088', 'Voyage 008 - Call 8', NULL);
INSERT INTO public.voyages VALUES (252, '0089', 'Voyage 008 - Call 9', NULL);
INSERT INTO public.voyages VALUES (253, '0095', 'Voyage 009 - Call 5', NULL);
INSERT INTO public.voyages VALUES (254, '0096', 'Voyage 009 - Call 6', NULL);
INSERT INTO public.voyages VALUES (255, '0097', 'Voyage 009 - Call 7', NULL);
INSERT INTO public.voyages VALUES (256, '0098', 'Voyage 009 - Call 8', NULL);
INSERT INTO public.voyages VALUES (257, '0099', 'Voyage 009 - Call 9', NULL);
INSERT INTO public.voyages VALUES (258, '0215', 'Voyage 021 - Call 5', NULL);
INSERT INTO public.voyages VALUES (259, '0105', 'Voyage 010 - Call 5', NULL);
INSERT INTO public.voyages VALUES (260, '0106', 'Voyage 010 - Call 6', NULL);
INSERT INTO public.voyages VALUES (261, '0107', 'Voyage 010 - Call 7', NULL);
INSERT INTO public.voyages VALUES (262, '0108', 'Voyage 010 - Call 8', NULL);
INSERT INTO public.voyages VALUES (263, '0109', 'Voyage 010 - Call 9', NULL);
INSERT INTO public.voyages VALUES (264, '0117', 'Voyage 011 - Call 7', NULL);
INSERT INTO public.voyages VALUES (265, '0118', 'Voyage 011 - Call 8', NULL);
INSERT INTO public.voyages VALUES (266, '0119', 'Voyage 011 - Call 9', NULL);
INSERT INTO public.voyages VALUES (267, '0127', 'Voyage 012 - Call 7', NULL);
INSERT INTO public.voyages VALUES (268, '0128', 'Voyage 012 - Call 8', NULL);
INSERT INTO public.voyages VALUES (269, '0129', 'Voyage 012 - Call 9', NULL);
INSERT INTO public.voyages VALUES (270, '0136', 'Voyage 013 - Call 6', NULL);
INSERT INTO public.voyages VALUES (271, '0137', 'Voyage 013 - Call 7', NULL);
INSERT INTO public.voyages VALUES (272, '0138', 'Voyage 013 - Call 8', NULL);
INSERT INTO public.voyages VALUES (273, '0139', 'Voyage 013 - Call 9', NULL);
INSERT INTO public.voyages VALUES (274, '0145', 'Voyage 014 - Call 5', NULL);
INSERT INTO public.voyages VALUES (275, '0146', 'Voyage 014 - Call 6', NULL);
INSERT INTO public.voyages VALUES (276, '0147', 'Voyage 014 - Call 7', NULL);
INSERT INTO public.voyages VALUES (277, '0148', 'Voyage 014 - Call 8', NULL);
INSERT INTO public.voyages VALUES (278, '0149', 'Voyage 014 - Call 9', NULL);
INSERT INTO public.voyages VALUES (279, '0156', 'Voyage 015 - Call 6', NULL);
INSERT INTO public.voyages VALUES (280, '0157', 'Voyage 015 - Call 7', NULL);
INSERT INTO public.voyages VALUES (281, '0158', 'Voyage 015 - Call 8', NULL);
INSERT INTO public.voyages VALUES (282, '0159', 'Voyage 015 - Call 9', NULL);
INSERT INTO public.voyages VALUES (283, '0165', 'Voyage 016 - Call 5', NULL);
INSERT INTO public.voyages VALUES (284, '0166', 'Voyage 016 - Call 6', NULL);
INSERT INTO public.voyages VALUES (285, '0167', 'Voyage 016 - Call 7', NULL);
INSERT INTO public.voyages VALUES (286, '0168', 'Voyage 016 - Call 8', NULL);
INSERT INTO public.voyages VALUES (287, '0169', 'Voyage 016 - Call 9', NULL);
INSERT INTO public.voyages VALUES (288, '0177', 'Voyage 017 - Call 7', NULL);
INSERT INTO public.voyages VALUES (289, '0178', 'Voyage 017 - Call 8', NULL);
INSERT INTO public.voyages VALUES (290, '0179', 'Voyage 017 - Call 9', NULL);
INSERT INTO public.voyages VALUES (291, '0185', 'Voyage 018 - Call 5', NULL);
INSERT INTO public.voyages VALUES (292, '0186', 'Voyage 018 - Call 6', NULL);
INSERT INTO public.voyages VALUES (293, '0187', 'Voyage 018 - Call 7', NULL);
INSERT INTO public.voyages VALUES (294, '0188', 'Voyage 018 - Call 8', NULL);
INSERT INTO public.voyages VALUES (295, '0189', 'Voyage 018 - Call 9', NULL);
INSERT INTO public.voyages VALUES (296, '0195', 'Voyage 019 - Call 5', NULL);
INSERT INTO public.voyages VALUES (297, '0196', 'Voyage 019 - Call 6', NULL);
INSERT INTO public.voyages VALUES (298, '0197', 'Voyage 019 - Call 7', NULL);
INSERT INTO public.voyages VALUES (299, '0198', 'Voyage 019 - Call 8', NULL);
INSERT INTO public.voyages VALUES (300, '0199', 'Voyage 019 - Call 9', NULL);
INSERT INTO public.voyages VALUES (301, '0205', 'Voyage 020 - Call 5', NULL);
INSERT INTO public.voyages VALUES (302, '0206', 'Voyage 020 - Call 6', NULL);
INSERT INTO public.voyages VALUES (303, '0207', 'Voyage 020 - Call 7', NULL);
INSERT INTO public.voyages VALUES (304, '0208', 'Voyage 020 - Call 8', NULL);
INSERT INTO public.voyages VALUES (305, '0209', 'Voyage 020 - Call 9', NULL);
INSERT INTO public.voyages VALUES (306, '0225', 'Voyage 022 - Call 5', NULL);
INSERT INTO public.voyages VALUES (307, '0226', 'Voyage 022 - Call 6', NULL);
INSERT INTO public.voyages VALUES (308, '0227', 'Voyage 022 - Call 7', NULL);
INSERT INTO public.voyages VALUES (309, '0228', 'Voyage 022 - Call 8', NULL);
INSERT INTO public.voyages VALUES (310, '0229', 'Voyage 022 - Call 9', NULL);
INSERT INTO public.voyages VALUES (311, '0235', 'Voyage 023 - Call 5', NULL);
INSERT INTO public.voyages VALUES (312, '0236', 'Voyage 023 - Call 6', NULL);
INSERT INTO public.voyages VALUES (313, '0237', 'Voyage 023 - Call 7', NULL);
INSERT INTO public.voyages VALUES (314, '0238', 'Voyage 023 - Call 8', NULL);
INSERT INTO public.voyages VALUES (315, '0239', 'Voyage 023 - Call 9', NULL);
INSERT INTO public.voyages VALUES (316, '0246', 'Voyage 024 - Call 6', NULL);
INSERT INTO public.voyages VALUES (317, '0248', 'Voyage 024 - Call 8', NULL);
INSERT INTO public.voyages VALUES (318, '0249', 'Voyage 024 - Call 9', NULL);
INSERT INTO public.voyages VALUES (319, '0256', 'Voyage 025 - Call 6', NULL);
INSERT INTO public.voyages VALUES (320, '0257', 'Voyage 025 - Call 7', NULL);
INSERT INTO public.voyages VALUES (321, '0258', 'Voyage 025 - Call 8', NULL);
INSERT INTO public.voyages VALUES (322, '0259', 'Voyage 025 - Call 9', NULL);
INSERT INTO public.voyages VALUES (323, '0265', 'Voyage 026 - Call 5', NULL);
INSERT INTO public.voyages VALUES (324, '0266', 'Voyage 026 - Call 6', NULL);
INSERT INTO public.voyages VALUES (325, '0267', 'Voyage 026 - Call 7', NULL);
INSERT INTO public.voyages VALUES (326, '0268', 'Voyage 026 - Call 8', NULL);
INSERT INTO public.voyages VALUES (327, '0269', 'Voyage 026 - Call 9', NULL);
INSERT INTO public.voyages VALUES (328, '0285', 'Voyage 028 - Call 5', NULL);
INSERT INTO public.voyages VALUES (329, '0286', 'Voyage 028 - Call 6', NULL);
INSERT INTO public.voyages VALUES (330, '0287', 'Voyage 028 - Call 7', NULL);
INSERT INTO public.voyages VALUES (331, '0288', 'Voyage 028 - Call 8', NULL);
INSERT INTO public.voyages VALUES (332, '0289', 'Voyage 028 - Call 9', NULL);
INSERT INTO public.voyages VALUES (333, '0295', 'Voyage 029 - Call 5', NULL);
INSERT INTO public.voyages VALUES (334, '0296', 'Voyage 029 - Call 6', NULL);
INSERT INTO public.voyages VALUES (335, '0297', 'Voyage 029 - Call 7', NULL);
INSERT INTO public.voyages VALUES (336, '0298', 'Voyage 029 - Call 8', NULL);
INSERT INTO public.voyages VALUES (337, '0299', 'Voyage 029 - Call 9', NULL);
INSERT INTO public.voyages VALUES (338, '0305', 'Voyage 030 - Call 5', NULL);
INSERT INTO public.voyages VALUES (339, '0306', 'Voyage 030 - Call 6', NULL);
INSERT INTO public.voyages VALUES (340, '0307', 'Voyage 030 - Call 7', NULL);
INSERT INTO public.voyages VALUES (341, '0308', 'Voyage 030 - Call 8', NULL);
INSERT INTO public.voyages VALUES (342, '0309', 'Voyage 030 - Call 9', NULL);
INSERT INTO public.voyages VALUES (343, '0315', 'Voyage 031 - Call 5', NULL);
INSERT INTO public.voyages VALUES (344, '0316', 'Voyage 031 - Call 6', NULL);
INSERT INTO public.voyages VALUES (345, '0317', 'Voyage 031 - Call 7', NULL);
INSERT INTO public.voyages VALUES (346, '0318', 'Voyage 031 - Call 8', NULL);
INSERT INTO public.voyages VALUES (347, '0319', 'Voyage 031 - Call 9', NULL);
INSERT INTO public.voyages VALUES (348, '0325', 'Voyage 032 - Call 5', NULL);
INSERT INTO public.voyages VALUES (349, '0326', 'Voyage 032 - Call 6', NULL);
INSERT INTO public.voyages VALUES (350, '0327', 'Voyage 032 - Call 7', NULL);
INSERT INTO public.voyages VALUES (351, '0328', 'Voyage 032 - Call 8', NULL);
INSERT INTO public.voyages VALUES (352, '0329', 'Voyage 032 - Call 9', NULL);
INSERT INTO public.voyages VALUES (353, '0335', 'Voyage 033 - Call 5', NULL);
INSERT INTO public.voyages VALUES (354, '0336', 'Voyage 033 - Call 6', NULL);
INSERT INTO public.voyages VALUES (355, '0337', 'Voyage 033 - Call 7', NULL);
INSERT INTO public.voyages VALUES (356, '0338', 'Voyage 033 - Call 8', NULL);
INSERT INTO public.voyages VALUES (357, '0339', 'Voyage 033 - Call 9', NULL);
INSERT INTO public.voyages VALUES (358, '0505', 'Voyage 050 - Call 5', NULL);
INSERT INTO public.voyages VALUES (359, '0506', 'Voyage 050 - Call 6', NULL);
INSERT INTO public.voyages VALUES (360, '0507', 'Voyage 050 - Call 7', NULL);
INSERT INTO public.voyages VALUES (361, '0508', 'Voyage 050 - Call 8', NULL);
INSERT INTO public.voyages VALUES (362, '0509', 'Voyage 050 - Call 9', NULL);
INSERT INTO public.voyages VALUES (363, '0027', 'Voyage 002 - Call 7', NULL);
INSERT INTO public.voyages VALUES (364, '0029', 'Voyage 002 - Call 9', NULL);
INSERT INTO public.voyages VALUES (365, '0037', 'Voyage 003 - Call 7', NULL);
INSERT INTO public.voyages VALUES (366, '0038', 'Voyage 003 - Call 8', NULL);
INSERT INTO public.voyages VALUES (367, '0039', 'Voyage 003 - Call 9', NULL);
INSERT INTO public.voyages VALUES (368, '0045', 'Voyage 004 - Call 5', NULL);
INSERT INTO public.voyages VALUES (369, '0046', 'Voyage 004 - Call 6', NULL);
INSERT INTO public.voyages VALUES (370, '0047', 'Voyage 004 - Call 7', NULL);
INSERT INTO public.voyages VALUES (371, '0048', 'Voyage 004 - Call 8', NULL);
INSERT INTO public.voyages VALUES (372, '0049', 'Voyage 004 - Call 9', NULL);
INSERT INTO public.voyages VALUES (373, '0216', 'Voyage 021 - Call 6', NULL);
INSERT INTO public.voyages VALUES (374, '0176', 'Voyage 017 - Call 6', NULL);
INSERT INTO public.voyages VALUES (375, '0217', 'Voyage 021 - Call 7', NULL);


--
-- Data for Name: workflow; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.workflow VALUES (5, 5, 'PR', 'permintaanbarang', 'Permintaan Barang', 'PELNI/PR 18 UMS 000001', '2018-08-13 15:01:17.397086', '2018-08-13 15:01:17.397086', 1, 1, 1, '2018-08-13 15:01:17.397086');
INSERT INTO public.workflow VALUES (6, 5, 'PR', 'permintaanbarang', 'Permintaan Barang', 'PELNI/PR 18 UMS 000002', '2018-08-13 15:53:34.534384', '2018-08-13 15:53:34.534384', 1, 1, 2, '2018-08-13 15:53:34.534384');
INSERT INTO public.workflow VALUES (8, 6, 'PR', 'permintaanbarang', 'Permintaan Barang', 'PELNI/PR 18 KMT 000001', '2018-08-13 17:14:06.134576', '2018-08-13 17:14:06.134576', 2, 0, 0, '2018-08-13 17:14:06.134576');
INSERT INTO public.workflow VALUES (36, 177, 'MO', 'pindahsubinv', 'Pindah Barang Sub-Inventory', 'MO180177000002', '2018-09-30 20:00:42.076002', '2018-09-30 20:00:42.076002', 1, 1, 1, '2018-09-30 20:00:42.076002');
INSERT INTO public.workflow VALUES (14, 5, 'MR', 'penerimaanmakanan', 'Penerimaan Makanan', 'PELNI/MR 18 UMS 000001', '2018-08-20 17:44:15.930455', '2018-08-20 17:44:15.930455', 1, 1, 1, '2018-08-20 17:44:15.930455');
INSERT INTO public.workflow VALUES (35, 177, 'IO', 'transferio', 'Transfer IO', 'IO180177000002', '2018-09-30 16:56:05.346869', '2018-09-30 16:55:37', 1, 1, 1, '2018-09-30 16:56:05.346869');
INSERT INTO public.workflow VALUES (7, 5, 'IO', 'transferio', 'Transfer IO', 'PELNI/IO 18 UMS 000001', '2018-08-13 17:02:12.076069', '2018-08-13 10:00:28', 1, 1, 1, '2018-08-13 17:02:12.076069');
INSERT INTO public.workflow VALUES (10, 5, 'IO', 'transferio', 'Transfer IO', 'PELNI/IO 18 UMS 000003', '2018-08-13 17:20:23.009573', '2018-08-13 10:18:54', 1, 1, 1, '2018-08-13 17:20:23.009573');
INSERT INTO public.workflow VALUES (18, 5, 'MO', 'pindahsubinv', 'Pindah Barang Sub-Inventory', 'PELNI/MO 18 UMS 000001', '2018-08-23 15:21:55.876471', '2018-08-23 15:21:55.876471', 1, 1, 0, '2018-08-23 15:21:55.876471');
INSERT INTO public.workflow VALUES (19, 5, 'IO', 'transferio', 'Transfer IO', 'PELNI/IO 18 UMS 000005', '2018-08-23 18:14:24.297269', '2018-08-23 11:13:07', 1, 1, 0, '2018-08-23 18:14:24.297269');
INSERT INTO public.workflow VALUES (43, 177, 'PR', 'permintaanbarang', 'Permintaan Barang', 'PR180177000011', '2018-10-01 17:11:39.807117', '2018-10-01 17:11:39.807117', 1, 1, 2, '2018-10-01 17:11:39.807117');
INSERT INTO public.workflow VALUES (20, 5, 'MS', 'penggunaanmakanan', 'Penggunaan Makanan', 'PELNI/MS 18 UMS 000003', '2018-08-23 18:36:38.940478', '2018-08-23 18:36:38.940478', 1, 1, 0, '2018-08-23 18:36:38.940478');
INSERT INTO public.workflow VALUES (22, 5, 'IO', 'transferio', 'Transfer IO', 'PELNI/IO 18 UMS 000006', '2018-09-04 14:17:30.622802', '2018-09-04 07:17:08', 1, 0, 0, '2018-09-04 14:17:30.622802');
INSERT INTO public.workflow VALUES (21, 5, 'PR', 'permintaanbarang', 'Permintaan Barang', 'PELNI/PR 18 UMS 000005', '2018-09-04 13:53:39.471648', '2018-09-04 13:53:39.471648', 1, 1, 1, '2018-09-04 13:53:39.471648');
INSERT INTO public.workflow VALUES (17, 5, 'MS', 'penggunaanmakanan', 'Penggunaan Makanan', 'PELNI/MS 18 UMS 000002', '2018-08-20 20:04:10.351285', '2018-08-20 20:04:10.351285', 1, 1, 1, '2018-08-20 20:04:10.351285');
INSERT INTO public.workflow VALUES (23, 177, 'PR', 'permintaanbarang', 'Permintaan Barang', 'PR180177000001', '2018-09-21 16:10:57.624686', '2018-09-21 16:10:57.624686', 1, 0, 0, '2018-09-21 16:10:57.624686');
INSERT INTO public.workflow VALUES (15, 5, 'MR', 'penerimaanmakanan', 'Penerimaan Makanan', 'PELNI/MR 18 UMS 000002', '2018-08-20 18:27:53.001345', '2018-08-20 18:27:53.001345', 1, 1, 1, '2018-08-20 18:27:53.001345');
INSERT INTO public.workflow VALUES (24, 177, 'IO', 'transferio', 'Transfer IO', 'IO180177000001', '2018-09-24 10:25:19.341985', '2018-09-24 03:24:41', 1, 1, 1, '2018-09-24 10:25:19.341985');
INSERT INTO public.workflow VALUES (9, 5, 'IO', 'transferio', 'Transfer IO', 'PELNI/IO 18 UMS 000002', '2018-08-13 17:17:23.35231', '2018-08-13 10:15:05', 1, 1, 1, '2018-08-13 17:17:23.35231');
INSERT INTO public.workflow VALUES (25, 177, 'MO', 'pindahsubinv', 'Pindah Barang Sub-Inventory', 'MO180177000001', '2018-09-24 14:01:09.557615', '2018-09-24 14:01:09.557615', 1, 0, 0, '2018-09-24 14:01:09.557615');
INSERT INTO public.workflow VALUES (16, 5, 'MS', 'penggunaanmakanan', 'Penggunaan Makanan', 'PELNI/MS 18 UMS 000001', '2018-08-20 20:03:33.082167', '2018-08-20 20:03:33.082167', 1, 1, 1, '2018-08-20 20:03:33.082167');
INSERT INTO public.workflow VALUES (13, 5, 'PR', 'permintaanbarang', 'Permintaan Barang', 'PELNI/PR 18 UMS 000004', '2018-08-14 10:48:22.160569', '2018-08-14 10:48:22.160569', 1, 1, 1, '2018-08-14 10:48:22.160569');
INSERT INTO public.workflow VALUES (37, 177, 'MR', 'penerimaanmakanan', 'Penerimaan Makanan', 'MR180177000003', '2018-09-30 20:10:00.194915', '2018-09-30 20:10:00.194915', 1, 1, 1, '2018-09-30 20:10:00.194915');
INSERT INTO public.workflow VALUES (26, 177, 'PR', 'permintaanbarang', 'Permintaan Barang', 'PR180177000001', '2018-09-24 14:53:04.119059', '2018-09-24 14:53:04.119059', 1, 0, 0, '2018-09-24 14:53:04.119059');
INSERT INTO public.workflow VALUES (11, 5, 'IO', 'transferio', 'Transfer IO', 'PELNI/IO 18 UMS 000004', '2018-08-13 19:16:55.693576', '2018-08-13 12:15:56', 1, 1, 1, '2018-08-13 19:16:55.693576');
INSERT INTO public.workflow VALUES (27, 177, 'PR', 'permintaanbarang', 'Permintaan Barang', 'PR180177000002', '2018-09-25 09:37:14.756697', '2018-09-25 09:37:14.756697', 1, 0, 0, '2018-09-25 09:37:14.756697');
INSERT INTO public.workflow VALUES (28, 177, 'PR', 'permintaanbarang', 'Permintaan Barang', 'PR180177000003', '2018-09-25 09:42:53.202556', '2018-09-25 09:42:53.202556', 1, 0, 0, '2018-09-25 09:42:53.202556');
INSERT INTO public.workflow VALUES (29, 177, 'MR', 'penerimaanmakanan', 'Penerimaan Makanan', 'MR180177000001', '2018-09-25 11:45:21.672284', '2018-09-25 11:45:21.672284', 1, 0, 0, '2018-09-25 11:45:21.672284');
INSERT INTO public.workflow VALUES (12, 5, 'PR', 'permintaanbarang', 'Permintaan Barang', 'PELNI/PR 18 UMS 000003', '2018-08-14 10:12:28.058806', '2018-08-14 10:12:28.058806', 1, 1, 1, '2018-08-14 10:12:28.058806');
INSERT INTO public.workflow VALUES (30, 177, 'MR', 'penerimaanmakanan', 'Penerimaan Makanan', 'MR180177000002', '2018-09-25 13:41:53.176646', '2018-09-25 13:41:53.176646', 1, 1, 1, '2018-09-25 13:41:53.176646');
INSERT INTO public.workflow VALUES (31, 177, 'MS', 'penggunaanmakanan', 'Penggunaan Makanan', 'MS180177000001', '2018-09-25 14:01:31.560168', '2018-09-25 14:01:31.560168', 1, 0, 0, '2018-09-25 14:01:31.560168');
INSERT INTO public.workflow VALUES (32, 177, 'MS', 'penggunaanmakanan', 'Penggunaan Makanan', 'MS180177000002', '2018-09-25 14:02:26.608736', '2018-09-25 14:02:26.608736', 1, 1, 1, '2018-09-25 14:02:26.608736');
INSERT INTO public.workflow VALUES (38, 177, 'PR', 'permintaanbarang', 'Permintaan Barang', 'PR180177000006', '2018-10-01 14:45:34.220572', '2018-10-01 14:45:34.220572', 1, 0, 0, '2018-10-01 14:45:34.220572');
INSERT INTO public.workflow VALUES (33, 177, 'PR', 'permintaanbarang', 'Permintaan Barang', 'PR180177000004', '2018-09-30 10:39:04.334454', '2018-09-30 10:39:04.334454', 1, 1, 1, '2018-09-30 10:39:04.334454');
INSERT INTO public.workflow VALUES (34, 177, 'PR', 'permintaanbarang', 'Permintaan Barang', 'PR180177000005', '2018-09-30 16:53:13.672825', '2018-09-30 16:53:13.672825', 1, 0, 0, '2018-09-30 16:53:13.672825');
INSERT INTO public.workflow VALUES (39, 177, 'PR', 'permintaanbarang', 'Permintaan Barang', 'PR180177000007', '2018-10-01 16:39:12.005853', '2018-10-01 16:39:12.005853', 1, 0, 0, '2018-10-01 16:39:12.005853');
INSERT INTO public.workflow VALUES (44, 177, 'PR', 'permintaanbarang', 'Permintaan Barang', 'PR180177000012', '2018-10-02 14:57:55.41997', '2018-10-02 14:57:55.41997', 1, 1, 1, '2018-10-02 14:57:55.41997');
INSERT INTO public.workflow VALUES (40, 177, 'PR', 'permintaanbarang', 'Permintaan Barang', 'PR180177000008', '2018-10-01 16:54:21.527981', '2018-10-01 16:54:21.527981', 1, 0, 0, '2018-10-01 16:54:21.527981');
INSERT INTO public.workflow VALUES (41, 177, 'PR', 'permintaanbarang', 'Permintaan Barang', 'PR180177000009', '2018-10-01 16:57:40.732821', '2018-10-01 16:57:40.732821', 1, 0, 0, '2018-10-01 16:57:40.732821');
INSERT INTO public.workflow VALUES (42, 177, 'PR', 'permintaanbarang', 'Permintaan Barang', 'PR180177000010', '2018-10-01 17:10:01.780586', '2018-10-01 17:10:01.780586', 1, 0, 0, '2018-10-01 17:10:01.780586');
INSERT INTO public.workflow VALUES (45, 177, 'PR', 'permintaanbarang', 'Permintaan Barang', 'PR180177000013', '2018-10-02 15:30:11.805754', '2018-10-02 15:30:11.805754', 1, 0, 0, '2018-10-02 15:30:11.805754');


--
-- Name: account_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.account_id_seq', 2, true);


--
-- Name: activitylog_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.activitylog_id_seq', 71, true);


--
-- Name: administratoradministrator_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.administratoradministrator_id_seq', 1, false);


--
-- Name: administratorgroupaccess_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.administratorgroupaccess_id_seq', 55, true);


--
-- Name: administratormenu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.administratormenu_id_seq', 1, true);


--
-- Name: applicationparameter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.applicationparameter_id_seq', 1, false);


--
-- Name: approver_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.approver_id_seq', 1, false);


--
-- Name: item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.item_id_seq', 10, true);


--
-- Name: item_onhand_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.item_onhand_id_seq', 1, false);


--
-- Name: itempenerimaanbarang_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.itempenerimaanbarang_id_seq', 40, true);


--
-- Name: itempenerimaanbarangio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.itempenerimaanbarangio_id_seq', 6, true);


--
-- Name: itempenerimaanmakanan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.itempenerimaanmakanan_id_seq', 11, true);


--
-- Name: itempenggunaanbarang_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.itempenggunaanbarang_id_seq', 28, true);


--
-- Name: itempenggunaanmakanan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.itempenggunaanmakanan_id_seq', 8, true);


--
-- Name: itempermintaanbarang_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.itempermintaanbarang_id_seq', 36, true);


--
-- Name: itempindahbarangio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.itempindahbarangio_id_seq', 12, true);


--
-- Name: itempindahbarangsubinv_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.itempindahbarangsubinv_id_seq', 4, true);


--
-- Name: itempopenerimaan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.itempopenerimaan_id_seq', 2, true);


--
-- Name: itempopenerimaanio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.itempopenerimaanio_id_seq', 2, true);


--
-- Name: kapal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.kapal_id_seq', 78, true);


--
-- Name: loginlog_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.loginlog_id_seq', 79, true);


--
-- Name: lokasiitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.lokasiitem_id_seq', 1, true);


--
-- Name: notifications_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.notifications_id_seq', 1, false);


--
-- Name: penerimaanbarang_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.penerimaanbarang_id_seq', 18, true);


--
-- Name: penerimaanbarangio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.penerimaanbarangio_id_seq', 6, true);


--
-- Name: penerimaanmakanan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.penerimaanmakanan_id_seq', 2, true);


--
-- Name: penggunaanbarang_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.penggunaanbarang_id_seq', 13, true);


--
-- Name: penggunaanmakanan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.penggunaanmakanan_id_seq', 5, true);


--
-- Name: permintaanbarang_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.permintaanbarang_id_seq', 18, true);


--
-- Name: pindahbarangio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pindahbarangio_id_seq', 8, true);


--
-- Name: pindahbarangsubinv_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pindahbarangsubinv_id_seq', 1, true);


--
-- Name: popenerimaan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.popenerimaan_id_seq', 2, true);


--
-- Name: popenerimaanio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.popenerimaanio_id_seq', 4, true);


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_id_seq', 2, true);


--
-- Name: staging_sync_table_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.staging_sync_table_id_seq', 1, true);


--
-- Name: stokkapal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.stokkapal_id_seq', 64, true);


--
-- Name: sync_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sync_id_seq', 176, true);


--
-- Name: sync_table_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sync_table_id_seq', 1, false);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 4, true);


--
-- Name: voyages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.voyages_id_seq', 1, false);


--
-- Name: workflow_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.workflow_id_seq', 45, true);


--
-- Name: account PRIMARY; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account
    ADD CONSTRAINT "PRIMARY" PRIMARY KEY (id);


--
-- Name: staging_sync_table staging_sync_table_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.staging_sync_table
    ADD CONSTRAINT staging_sync_table_pkey PRIMARY KEY (id);


--
-- Name: sync sync_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sync
    ADD CONSTRAINT sync_pkey PRIMARY KEY (id);


--
-- Name: email_admin_UNIQUE; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX "email_admin_UNIQUE" ON public.administratoradministrator USING btree (email);


--
-- Name: poNumber; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX "poNumber" ON public.popenerimaan USING btree ("poNumber");


--
-- Name: receiptNumber; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX "receiptNumber" ON public.penerimaanbarang USING btree ("receiptNumber");


--
-- Name: username_UNIQUE; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX "username_UNIQUE" ON public.users USING btree (username);


--
-- Name: username_admin_UNIQUE; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX "username_admin_UNIQUE" ON public.administratoradministrator USING btree (username);


--
-- Name: variableName_UNIQUE; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX "variableName_UNIQUE" ON public.applicationparameter USING btree ("variableName");


--
-- PostgreSQL database dump complete
--

