$("select[name='q']").change(function () {
    if ($(this).val() === "status") {
        $("input[name='search-value'],select[name='search-value']").replaceWith('<select class="form-control" name="search-value">Active' +
            '<option value="1">Active</option>' +
            '<option value="0">Deleted</option>' +
            '</select>');
    }

});


var redirectURL = "";
var totalPage = parseInt($("#pageCount").text());
var totalRow = parseInt($("#totalRow").text());
var currentPage = getParameterByName("page");

init();
function init() {
    currentPage = currentPage ? currentPage : 1;
    nextDisabled = currentPage >= totalPage ? "disabled" : "";
    prevDisabled = currentPage <= 1 ? "disabled" : "";
    $('#pagination-next').addClass(nextDisabled);
    $('#pagination-prev').addClass(prevDisabled);
    var sort = getParameterByName("sort");
    $('.datepicker').datepicker('update', '');
    querySearchSelected();
    querySortingSelected();
    querySizeSelected();

}
function querySizeSelected() {
    var size = getParameterByName("size");
    if (size !== null) {
        $("select[name='size']").val(size)
    }
}
function querySortingSelected() {
    var sort = getParameterByName("sort");
    if (sort !== null) {
        $("select[name='orderingBy']").val(sort)
    }
}
$("input[name='selectAll']").click(function () {
    if ($(this).is(':checked')) {
        $("input[name='checkbox-del[]']").prop('checked', true);
    } else {
        $("input[name='checkbox-del[]']").prop('checked', false);
    }
});

$("input[name='checkbox-del[]']").click(function () {
    if ($(this).is(':checked')) {
        if ($("input[name='checkbox-del[]']:checked").length == $("input[name='checkbox-del[]']").length) {
            $("input[name='selectAll']").prop('checked', true);
        } else {
            $("input[name='selectAll']").prop('checked', false);
        }
    } else {
        $("input[name='selectAll']").prop('checked', false);
    }
});

$("button[name='delete-content']").click(function () {
    var checkedData = [];
    $("table[name='table-index']").find("input[name='checkbox-del[]']:checked").each(function () {
        checkedData.push(parseInt($(this).val()))
    });

    if (checkedData.length === 0) {
        $("#generalModal-title .modal-title").html("Delete");
        $("#generalModal-content").html("<p>You dont pick anything ! .</p>");
        $("#generalModal").modal('show');
    } else {
        $("#generalModal-title .modal-title").html("Delete");
        $("#generalModal-content").html("<p>Are you sure want to delete! .</p>");
        $("#generalModal-footer").removeClass("hidden").find($("button").click(function () {
            if (this.id === "modalAgree") {
                window.location.href = currentUrl + "/" + checkedData + "/delete";
            }
        }));
        $("#generalModal").modal('show');


    }
});

function querySearchSelected() {
    var querySearch = getParameterByName("q");
    var collapseItemSearch = "hide";

    if (querySearch !== null) {
        var criteriaList = querySearch.split("||");
        // $("select[name='q']").val(criteria[0]);
        $.each(criteriaList, function(index, value) {
            var criteria = value.split(":");
            $("[name='" + criteria[0] + "']").val(criteria[1]);

            if(criteria[0] != "keyword") { collapseItemSearch = "show"; }
        } );

        if(collapseItemSearch == "show") {
            $("#advanceSearch").collapse(collapseItemSearch);
            $("#detailSearch").addClass("fa-rotate-180");
        }


/* 
        if (criteria[0] === "status") {
            $(".form-control[name='search-value']").replaceWith('<select class="form-control" name="search-value">Featured' +
                '<option value="1">Active</option>' +
                '<option value="0">Deleted</option>' +
                '</select>');
        } else {
            $("input[name='search-value']").val(criteria[1])
        } */


    }
}

$('#advanceSearch').on('show.bs.collapse', function () {
    $("#detailSearch").addClass("fa-rotate-180");
});

$('#advanceSearch').on('hidden.bs.collapse', function () {
    $("#detailSearch").removeClass("fa-rotate-180");
});

$("select[name='orderingBy']").change(function (evt) {
    console.log(paginationParameter.pageCount);
    var sortingValue = "";
    var sort = getParameterByName("sort");
    var sortingBy = $(this).attr("data-sort");
    if (sort === "0") {
        sortingValue = sortingBy + ':' + "asc";
        $(this).find("span").remove();
    } else {
        sortingValue = evt.target.value;
    }
    redirectURL = UpdateQueryString("sort", sortingValue);
    window.location.href = redirectURL;
});
$("button[name='clear-search']").click(function () {
    redirectURL = removeURLParameter("q");
    redirectURL = removeURLParameter("page", redirectURL);
    window.location.href = redirectURL;
});
$("select[name='size']").change(function () {
    var size = getParameterByName("size");
    redirectURL = UpdateQueryString("size", $(this).val());
    redirectURL = removeURLParameter("page", redirectURL);
    $("input[name='size']").val(size);
    window.location.href = redirectURL;
});

// $("button[name='search-btn-pagination']").click(function () {
    /* var searchBy = $("select[name='q']").val();
    var searchValue = $(".form-control[name='search-value']").val(); */
function searchList() {
    var query = "";
    $(".search-value").each(function () {
        // alert( $(this).attr("name") );
        if($(this).val() != "") {
            if(query != "") { query += "||"; }
            query += $(this).attr("name") + ':' + ($(this).val()).replace(/[^a-zA-Z0-9\-\.: ]/g, "") ;
        }        
    });
    if(query != "" ) {
        redirectURL = UpdateQueryString("q", query);
        redirectURL = removeURLParameter("page", redirectURL) 
        window.location.href = redirectURL;
    } // else { alert(query); }
}
// });


$("button[name='first']").click(function () {
    redirectURL = UpdateQueryString("page", 1);
    window.location.href = redirectURL;
});
$("button[name='last']").click(function () {
    lastPage = $("#pageCount").text();
    redirectURL = UpdateQueryString("page", lastPage === "0" ? 1 : lastPage);
    window.location.href = redirectURL;
});
/**
 *Next function on pagination
 */
$("#pagination-next").click(function () {
    if(nextDisabled == "disabled") {  return; }
    prevDisabled = currentPage <= 1 ? "disabled" : "";
    var page = getParameterByName("page");

    currentPage = 0;
    if ((page === null) || (page === 0)) {
        currentPage = 2;
    } else {
        currentPage = parseInt(page) + 1;
    }
    redirectURL = UpdateQueryString("page", currentPage);
    window.location.href = redirectURL;
});
/**
 * prev function pagination
 */
$("#pagination-prev").click(function () {
    if(prevDisabled == "disabled") {  return; }
    var page = getParameterByName("page");
    currentPage = 0;
    if ((page === null) || (page === 0)) {
        currentPage = parseInt(page) - 1;
    } else {
        currentPage = parseInt(page) - 1;
    }    
    redirectURL = UpdateQueryString("page", currentPage);
    window.location.href = redirectURL; 
});

$( document ).ready(function(){
    if(typeof paginationParameter === "undefined") { } else {
        var obj = JSON.parse(paginationParameter);
        if(obj.pageCount > 1) {
            obj.page = parseInt(obj.page);
            var awal = 1;
            var akhir = obj.pageCount; 
            if(obj.pageCount > 9) {
                awal = obj.page - 4;
                akhir = obj.page + 4;
                if(awal < 1) { akhir = (akhir - awal) + 1; awal = 1; }    
                if(akhir > obj.pageCount) { awal = awal - (akhir - obj.pageCount) - 1; akhir = obj.pageCount; }
            }        

            if(awal > 1) {
                $(".pagination #pagination-next").before('<li class="page-item"><a class="page-link" href="' + UpdateQueryString("page", (awal-1)) + '">...</a></li>');
            }  

            for(i=awal;i<=akhir;i++) {          // console.log(i + "..");
                if(obj.page == i) {
                    $(".pagination #pagination-next").before('<li class="page-item"><a class="page-link text-bold text-primary">' + i + '</a></li>');
                } else {
                    $(".pagination #pagination-next").before('<li class="page-item"><a class="page-link" href="' + UpdateQueryString("page", i) + '">' + i + '</a></li>');
                }            
            }
            if(akhir < obj.pageCount) {
                $(".pagination #pagination-next").before('<li class="page-item"><a class="page-link" href="' + UpdateQueryString("page", (akhir + 1)) + '">...</a></li>');
            }  
        }
    }
});

/**
 *
 * @param name
 * @param url
 * @returns {*}
 * @see ini untuk mengambil parameter dari query string
 */
function getParameterByName(name, url) {
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

/**
 *
 * @param parameter
 * @param url
 * @returns {*}
 * @see ini untuk membuang parameter query string dari url
 */
function removeURLParameter(parameter, url) {
    if (!url) {
        url = window.location.href;
    }
    //prefer to use l.search if you have a location/link object
    var urlparts = url.split('?');
    if (urlparts.length >= 2) {
        var prefix = encodeURIComponent(parameter) + '=';
        var pars = urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i = pars.length; i-- > 0;) {
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }
        url = urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : "");
        return url;
    } else {
        return url;
    }
}


function UpdateQueryString(key, value, url) {
    if (!url) url = window.location.href;
    var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi"),
        hash;

    if (re.test(url)) {
        if (typeof value !== 'undefined' && value !== null)
            return url.replace(re, '$1' + key + "=" + value + '$2$3');
        else {
            hash = url.split('#');
            url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
            if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                url += '#' + hash[1];
            return url;
        }
    }
    else {
        if (typeof value !== 'undefined' && value !== null) {
            var separator = url.indexOf('?') !== -1 ? '&' : '?';
            hash = url.split('#');
            url = hash[0] + separator + key + '=' + value;
            if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                url += '#' + hash[1];
            return url;
        }
        else
            return url;
    }
}



/*
* 
*/

$("#loadItemModal").click(function () {
    backItemSearched();
    /* $(".carousel-item").removeAttr("active");
    $(".carousel-item:first").attr("class", "active"); */
    $("#itemModal").modal('show');
    $("#formItemSearch input[name=search-item]").focus();
});

var searchItemModal = function () {
    var searchValue = $("#formItemSearch input[name=search-item]").val();
    if(searchValue == "") {        
        $("#formItemSearch input[name=search-item]").focus();
    } else {
        itemPage = 1;
        showItemModal();
    }
};

var resetItemModal = function () {
    $("#formItemSearch input[name=search-item]").val('');
    showItemModal();
};

var itemCurrentPage = 1;
var itemPage = 1;
var itemPageCount = 0;
var showItemModalNext = function() {
    itemPage = itemCurrentPage + 1;
    showItemModal();
}
var showItemModalPrev = function() {
    itemPage = itemCurrentPage - 1;
    if(itemPage < 1) { itemPage = 1; }
    showItemModal();
}
var showItemModalLast = function() {
    itemPage = itemPageCount;
    showItemModal();
}
var showItemModalFirst = function() {
    itemPage = 1;
    showItemModal();
}

var showItemModal = function() {
    $("#listItemSearched").html("");
    var formItemSearch = $("#formItemSearch");

    var fromLocationId = $("#formPindahSubInv select[name=fromLocationId]").val();     // PINDAH SUBINV CASE

    var noAccount = $("#formPenggunaanBarang select[name=destinationAccount]").val();     // PENGGUNAAN BARANG CASE
    var locationId = $("#formPenggunaanBarang select[name=locationId]").val();     // PENGGUNAAN BARANG CASE

    var searchValue = $("#formItemSearch input[name=search-item]").val();
    var queryString = "/?size=12&page=" + itemPage + "&q=" + $("#formItemSearch select[name=q]").val();

    if(searchValue == "") { } else {
        queryString += ":" + searchValue
    }
    
    if(noAccount) {
        queryString += "||expense_account:" + noAccount;
    }
    if(locationId) {
        queryString += "||lokasiId:" + locationId;
    }
    if(fromLocationId) {
        queryString += "||lokasiId:" + fromLocationId;
    }
    
    $.ajax({
        type: formItemSearch.attr("method"),
        dataType: "json",
        url: formItemSearch.attr("action") + queryString,
        // data: formItemSearch.serialize(),
        success: function(result){
            if(result.rowCount > 0) {
                if(tipeList == "B") {
                    showItemSearchB(result.rows);
                } else {
                    showItemSearchA(result.rows);                   
                } 
                showPagingSearch(result);
            } else {
                $("#listItemSearched").html("<div class='card mb-3 border-0'>Data tidak ditemukan untuk pencarian: " + $("#formItemSearch input[name=search-item]").val() + "</div>");
            }
        }        
    });
}

var showPagingSearch = function(results) {
    $("#itemPageNo").text(results.page);
    itemCurrentPage = parseInt(results.page);
    $("#itemPageCount").text(results.pageCount);
    itemPageCount = parseInt(results.pageCount);
    $("#itemTotalRow").text(results.rowCount);

    $('#itemPageFirst').addClass("disabled");
    $('#itemPagePrev').addClass("disabled");
    $('#itemPageNext').addClass("disabled");
    $('#itemPageLast').addClass("disabled");
   
   /*  if(itemCurrentPage > 1) {
        $('#itemPageNext').addClass("disabled");
        $('#itemPageLast').addClass("disabled");
    } */

    if(itemCurrentPage < itemPageCount) {
        $('#itemPageNext').removeClass("disabled");
        $('#itemPageLast').removeClass("disabled");
    }
 
    if(itemCurrentPage > 1) {
        $('#itemPageFirst').removeClass("disabled");
        $('#itemPagePrev').removeClass("disabled");
    }
}

var showItemSearchA = function(listItems) {
    var itemN = 0; var content = "";
    content = '<div class="card-deck">';
    $.each( listItems, function( keyItem, item ) {
        if(item.stokID) {       } else { item.stokID    = 0; }
        if(item.lotnumber) {    } else { item.lotnumber = 0; }
        if(item.tglMasuk) { item.tglMasuk = item.tglMasuk.substring(0,10); } else { item.tglMasuk = ''; }
        if(item.jumlah) {       } else { item.jumlah    = 0; }
        
        content += "<div id=\"item" + item.id + "\" class=\"card cardof4 bg-light\" data-lokasiid='" + item.lokasiId + "' data-tglmasuk='" + item.tglMasuk + "' data-itemid='" + item.id + "' data-stokid='" + item.stokID + "' data-lotnumber='" + item.lotNumber + "' data-jumlah='" + item.jumlah + "' data-namaitem='" + item.namaItem + "' data-harga='" + item.harga + "' data-deskripsi='" + item.deskripsi + "' data-partnumber='" + item.part_number + "' data-category='" + item.category + "' data-uom='" + item.UOM + "' >";
        var urlImage = "/assets/img/nopicture.jpg";
        if(item.image != "null" && item.image != null) { urlImage = "/" + item.image; }
        content += "<img class=\"card-img-top\" src=\"" + urlImage + "\" onClick=\"addItemDesc('" + item.id + "')\" alt=\"" + item.namaItem + "\">";
        
        content += "<div class=\"card-body\" style=\"padding:0px;\"></div><div class=\"card-footer\">" +
          "<div class=\" font-weight-bold\" onClick=\"addItemDesc('" + item.id + "')\">" + item.namaItem + "</div>" +
          "<div class=\"\" onClick='addItemDesc(" + item.id + ")'>" + item.deskripsi + "</div>";
        if(item.part_number != null) {
            content += "<div class=\"\" onClick='addItemDesc(" + item.id + ")'>Part No.: " + item.part_number + "</div>";
        }
         //   "<div class=\"\" onClick='addItemDesc(" + item.id + ")'>Qty: " + item.jumlah + "</div>" +
        content += "</div></div>";
        // $("#listItemSearched").append(content);       
        itemN++;

        if(itemN % 4 == 0) {
            content += '</div><br /><div class="card-deck">';
        }
    });
    content += '</div>';
    $("#listItemSearched").append(content);
}

var showItemSearchB = function(listItems) {
    var itemN = 0; var content = "";
    content = '<div class="card-deck">';
    $.each( listItems, function( keyItem, item ) {
        if(item.lotNumber) {  } else { item.lotNumber = "[N/A]"; }
        if(item.tglMasuk) { item.tglMasuk = item.tglMasuk.substring(0,10); } else { item.tglMasuk = ''; }
        if(item.stokID) {  } else { item.stokID = 0; }
        if(item.jumlah) {  } else { item.jumlah = 0; }


        content += "<div id=\"item" + item.stokID + "\" class=\"card cardof4 bg-light\" data-lokasiid='" + item.lokasiId + "' data-tglmasuk='" + item.tglMasuk + "' data-itemid='" + item.id + "' data-stokid='" + item.stokID + "' data-lotnumber='" + item.lotNumber + "' data-jumlah='" + item.jumlah + "' data-namaitem='" + item.namaItem + "' data-harga='" + item.harga + "' data-deskripsi='" + item.deskripsi + "' data-partnumber='" + item.part_number + "' data-category='" + item.category + "' data-uom='" + item.UOM + "' >";
        var urlImage = "/assets/img/nopicture.jpg";
        if(item.image != "null" && item.image != null) { urlImage = "/" + item.image; }
        content += "<img class=\"card-img-top\" src=\"" + urlImage + "\" onClick=\"addItemDesc('" + item.stokID + "')\" alt=\"" + item.namaItem + "\">";
        
        content += "<div class=\"card-body\" style=\"padding:0px;\"></div><div class=\"card-footer\">" +
          "<div class=\" font-weight-bold\" onClick=\"addItemDesc('" + item.stokID + "')\">" + item.namaItem + "</div>" +
          "<div class=\"\" onClick='addItemDesc(" + item.stokID + ")'>" + item.deskripsi + "</div>";

        if(! item.part_number) {
          "<div class=\"\" onClick='addItemDesc(" + item.stokID + ")'>Part No.: " + item.part_number + "</div>";
        }
        content += "<div class=\"\" onClick='addItemDesc(" + item.stokID + ")'>Qty: " + item.jumlah + "</div>" +
          "<div class=\"\" onClick='addItemDesc(" + item.stokID + ")'>No. Lot: " + item.lotNumber + "</div>" +
          "<div class=\"\" onClick='addItemDesc(" + item.stokID + ")'>Tgl Masuk: " + item.tglMasuk + "</div>" +
        "</div></div>";
        // $("#listItemSearched").append(content);

        itemN++;

        if(itemN % 4 == 0) {
            content += '</div><br /><div class="card-deck">';
        }
    });

    content += '</div>';
    $("#listItemSearched").append(content);
}

function backItemSearched() {
    $("button[name=saveItemSearched]").hide();
    $("button[name=backItemSearched]").hide();
    // $("#listItemSearched").html("");
    $('#carouselItems').carousel(0);
    // $("#formItemSearch input[name=search-item]").val("");
    $("#formItemSearch input[name=search-item]").focus();
}

function deleteListedItem(itemID) {
    itemList.splice(itemList.indexOf( itemID.toString() ), 1);
    $("#listItem" + itemID).remove();

    var nIteration = 0; 
    $.each(itemList, function(idx, value) {
        nIteration++;    
        $("#noList" + value).html(nIteration + ".");        
    });
}

$("#formItemSearch input[name=search-item]").keypress(function(e){
    if ( e.which == 13 ) { // Enter key = keycode 13
        searchItemModal();  $(this).next().focus(); 
        return false;
    }
});

$("#formItemSearchDetail").find('input').keypress(function(e){
    if ( e.which == 13 ) { // Enter key = keycode 13
        saveItemSearched();
        return false;
    }
});

$("#formItemSearchDetail input[name='qty']").keypress(function(e){
    var charCode = (e.which) ? e.which : e.keyCode;
    /* console.log( e.target.value );
    console.log(charCode); */
    if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57)) {
        return false;
    } else if (charCode == 46 && e.target.value.split('.').length > 1) {
        return false;
    }
    return true;
});

function makeItRed(redBlack, nameText, warningText) {
    if(redBlack) {

    } else {
        
    }
}

// ITEM PAGE SHOW GRID/LIST

$("button[name='buttonGrid']").click(function () {
    redirectURL = UpdateQueryString("list", 'grid');
    window.location.href = redirectURL;
});

$("button[name='buttonList']").click(function () {
    redirectURL = UpdateQueryString("list", 'list');
    window.location.href = redirectURL;
});

$("input[name='voyageEnd']").change(function() {
    var voyageStart = $("input[name='voyageStart']").val();
    if(voyageStart != "") {
        voyageStart = new Date(voyageStart).getTime();

        if($(this).val() != "") {            
            var voyageEnd = new Date($(this).val()).getTime();

            if(voyageEnd < voyageStart) {
                $(this).val("");
                alert("Tanggal Voyage End tidak boleh lebih kecil dari Voyage Start");
            }
        }
    }
});


$("input[name='voyageStart']").change(function() {
    var voyageEnd = $("input[name='voyageEnd']").val();
    if(voyageEnd != "") {
        voyageEnd = new Date(voyageEnd).getTime();

        if($(this).val() != "") {            
            var voyageStart = new Date($(this).val()).getTime();

            if(voyageStart > voyageEnd) {
                $(this).val("");
                alert("Tanggal Voyage Start tidak boleh lebih besar dari Voyage End");
            }
        }
    }
});

function checkIsSmallerThanToday(fieldName) {
    today = new Date().getTime();

    if($("input[name='" + fieldName + "']").val() != "") {            
        var needBy = new Date($("input[name='" + fieldName + "']").val()).getTime();

        if(needBy < today) {
            $("input[name='" + fieldName + "']").val("");
            alert("Tanggal Dibutuhkan tidak boleh lebih kecil dari hari ini");
            $("input[name='" + fieldName + "']").focus();
        }
    }
}

// check Tanggal Penerimaan Barang tidak boleh lebih besar dari hari ini
function checkIsBiggerThanToday(fieldName) {
    today = new Date().getTime();

    if($("input[name='" + fieldName + "']").val() != "") {            
        var acceptedDate = new Date($("input[name='" + fieldName + "']").val()).getTime();

        if(acceptedDate > today) {
            $("input[name='" + fieldName + "']").val("");
            alert("Tanggal tidak boleh lebih besar dari hari ini");
            $("input[name='" + fieldName + "']").focus();
        }
    }
}

// check Tanggal Diterima harus >= Tanggal PO   ===>> Form Penerimaan Barang
function checkIsSmallerThanPODate(fieldName, PODate) {
    if($("input[name='" + fieldName + "']").val() != "") {            
        var needBy = new Date($("input[name='" + fieldName + "']").val()).getTime();
        var PODate = new Date($("input[name='" + PODate + "']").val()).getTime();
        var today = new Date().getTime();

        if(needBy < PODate) { 
            $("input[name='" + fieldName + "']").val("");
            alert("Tanggal diterima tidak boleh lebih kecil dari Tanggal PO");
            $("input[name='" + fieldName + "']").focus();
        }
        if(needBy > today)
        {
            $("input[name='" + fieldName + "']").val("");
            alert("Tanggal barang diterima tidak boleh lebih besar dari hari ini");
            $("input[name='" + fieldName + "']").focus();
        }
    }
}

// check Tanggal Sampai Tujuan   ===> Form Transfer IO
function checkIsBiggerThanArrivedDate(fieldName1, fieldName2) {
    if(fieldName1 == "tglTransaksi")
    {
        var tglTransaksi = new Date($("input[name='" + fieldName1 + "']").val()).getTime();
        var tglSampaiTujuan = new Date($("input[name='" + fieldName2 + "']").val()).getTime();
        if(tglTransaksi > tglSampaiTujuan)
        {
            $("input[name='" + fieldName1 + "']").val("");
            alert("Tanggal Transaksi tidak boleh lebih besar dari Tanggal Barang Sampai Tujuan");
            $("input[name='" + fieldName1 + "']").focus();
        }
    }
    else 
    {
        var tglSampaiTujuan = new Date($("input[name='" + fieldName1 + "']").val()).getTime();
        var tglTransaksi = new Date($("input[name='" + fieldName2 + "']").val()).getTime();
        if(tglSampaiTujuan < tglTransaksi)
        {
            $("input[name='" + fieldName1 + "']").val("");
            alert("Tanggal Barang Sampai Tujuan tidak boleh lebih kecil dari Tanggal Transaksi");
            $("input[name='" + fieldName1 + "']").focus();
        }
    }
}

// check Tanggal Penggunaan Maksimal 30 hari Backdate   ===>> Form Penggunaan Barang
// UPDATE: 30 hari dirubah menjadi 3 hari
function checkIsMoreThan3Days(fieldName, strToday, nDays) {
    today = Date.parse(strToday);

    if($("input[name='" + fieldName + "']").val() != "") {            
        var tglPenggunaan = new Date($("input[name='" + fieldName + "']").val()).getTime();
        var oneDay = 24*60*60*1000; 
        var diff = Math.round(Math.abs(tglPenggunaan - today));

        if(tglPenggunaan < today)
        {
            if(diff/oneDay > parseInt(nDays)) {
                $("input[name='" + fieldName + "']").val(""); 
                alert("Tanggal yang dapat dipilih paling lama adalah " + nDays + " hari sebelum hari ini!");
                $("input[name='" + fieldName + "']").focus();
            }
        }
    }
}

$(function () { $('[data-toggle="tooltip"]').tooltip(); });


$('.submitTransaction').submit(function(e) { 
    // this code prevents form from actually being submitted
    e.preventDefault();
    e.returnValue = false;

    if (itemList.length > 0) { 
        this.submit();
    } else {
        $("#defModalTitle").html("Transaction Form");
        $("#defModalBody").html("Mohon tambahkan item terlebih dahulu!");
        $("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>");
        $('#defModal').modal('show');
    }    
});

$('.nrpValidation').submit(function(e) { 
    // this code prevents form from actually being submitted
    e.preventDefault();
    e.returnValue = false;

    var nrpCreateUser = $("#nrp").val();
    var username = $("#username").val();
    if (/\d{5}/.test(nrpCreateUser) && nrpCreateUser.length == 5) {
        if(/\s/.test(username)){
            alert("Username #" + username + " tidak boleh memiliki spasi!");
        }
        else{
            this.submit();
        }
    } else {
        $("#defModalTitle").html("Users Form");
        $("#defModalBody").html("NRP #" + nrpCreateUser + " tidak valid. Isi hanya dengan 5 digit angka!");
        $("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>");
        $('#defModal').modal('show');
    }  
});


$('#formCreateRole').submit(function(e) { 

    e.preventDefault();
    e.returnValue = false;

    if( $("input[name='access[]']:checked").length > 0 ) {
        if( $("input[name='lokasiitem[]']:checked").length > 0 ) {
            if( $("input[name='category[]']:checked").length > 0 ) {
                var roleName = $("input[name='roleName']").val();
                var submitForm = this;
                $.ajax({
                    url: "/administrator/roles/checkrole",
                    type: "GET",
                    data: { roleName : roleName },
                    dataType: "json",
                    success: function (data, status, jqXHR) {  
                        if(data != null) {
                            $("#defModalTitle").html("Menu Akses");
                            $("#defModalBody").html("Nama role yang Anda masukkan sudah digunakan!");
                            $("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>");
                            $('#defModal').modal('show');
                        } else {      
                            return submitForm.submit();
                        }
                    },
                    error: function (jqXHR, status, err) {
                        $("#defModalTitle").html("Menu Akses");
                        $("#defModalBody").html("Terjadi kesalahan. Mohon ulangi! Atau hubungi administrator!");
                        $("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>");
                        $('#defModal').modal('show');
                    }
                });
            } else {
                $("#defModalTitle").html("Item Category");
                $("#defModalBody").html("Anda belum memilih item category. Pilih minimal satu item category untuk menyimpan role.");
                $("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>");
                $('#defModal').modal('show');
                return false;
            }
        } else {
            $("#defModalTitle").html("Sub-Inventory");
            $("#defModalBody").html("Anda belum memilih sub-inventory. Pilih minimal satu sub-inventory untuk menyimpan role.");
            $("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>");
            $('#defModal').modal('show');
            return false;
        }
    } else {
        $("#defModalTitle").html("Menu Akses");
        $("#defModalBody").html("Anda belum memilih akses menu. Pilih minimal satu menu untuk menyimpan role.");
        $("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>");
        $('#defModal').modal('show');
        return false;
    }
});

$('#formStockCard').submit(function(e) { 

    e.preventDefault();
    e.returnValue = false;

    if( $("input[name='lokasiId[]']:checked").length > 0 ) {
        return this.submit();
    } else {
        $("#defModalTitle").html("Report Stock Card");
        $("#defModalBody").html("Anda belum memilih Sub-Inventory. Pilih minimal satu sub-inventory.");
        $("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>");
        $('#defModal').modal('show');
        return false;
    }
});

/* $('#formSparePart').submit(function(e) { 

    e.preventDefault();
    e.returnValue = false;

    if( $("input[name='lokasiId[]']:checked").length > 0 ) {
        return this.submit();
    } else {
        $("#defModalTitle").html("Report Spare Part");
        $("#defModalBody").html("Anda belum memilih Sub-Inventory. Pilih minimal satu sub-inventory.");
        $("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>");
        $('#defModal').modal('show');
        return false;
    }
}); */

function hasWhiteSpace(fieldName)
{
    var username = $("input[name='" + fieldName + "']").val();    
    if(/\s/.test(username))
    {
        alert("Data tidak boleh berisi spasi");
        $("input[name='" + fieldName + "']").focus();
    }
}

function selectAll(idUtama, nameCheckboxes) {
    var checked = true;
    if( $("#" + idUtama).is(':checked') ) {
        checked = true;
    } else {
        checked = false;
    }
    
    $("#" + idUtama).prop('checked', checked);
    $("." + nameCheckboxes + "").each(function() {
        $(this).prop('checked', checked);
    });
}

function loadDateVoyage(urlgetDates) {
    overlayOn();
    var formVoyageKode = $("select[name='voyageKode']").val();
    $.ajax({
        url: urlgetDates,
        type: "GET",
        data: { voyageKode : formVoyageKode },
        dataType: "json",
        success: function (data, status, jqXHR) {    
            if(data) {
                $('input[name=voyageStart]').val(data.voyage_from);
                $('input[name=voyageEnd]').val(data.voyage_to);
            } else {
                $("#defModalTitle").html("Voyages Dates");
                $("#defModalBody").html("Tanggal untuk voyages yang anda pilih tidak tersedia. Silahkan hubungi Admin/Nahkoda!");
                $("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>");
                $('#defModal').modal('show');
            }
            overlayOff();
        },
        error: function (jqXHR, status, err) {
            overlayOff();
            $("#defModalTitle").html("Voyages Dates");
            $("#defModalBody").html("Terjadi kesalahan, Tutup peringatan ini dan coba kembali, atau refresh halaman ini dan mulai dari awal.!");
            $("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>");
            $('#defModal').modal('show');
        },
        complete: function (jqXHR, status) {
            overlayOff();
        },
        timeout: 60000
    });
}

function loadVoyage(urlgetDates, idTanggal) {
    if(typeof overlayOn !== 'undefined') {
        if($("#" + idTanggal).val() != "") {
            overlayOn();
            $.ajax({
                url: urlgetDates,
                type: "GET",
                data: { tglValidasi : $("#" + idTanggal).val() },
                dataType: "json",
                success: function (data, status, jqXHR) {    
                    if(data) {
                        $("select[name='voyageKode']").html("");
                        $("select[name='voyageKode']").append('<option value="">--Silahkan pilih voyage--</option>');
                        $.each(data, function(idx, value) {
                            $("select[name='voyageKode']").append('<option value="' + value.voyageKode + '">' + value.voyageName + '</option>');
                        });                    
                        // $('input[name=voyageStart]').val(data[0].voyage_from); $('input[name=voyageEnd]').val(data[0].voyage_to);
                        
                    } else {
                        $("#defModalTitle").html("Voyages Dates");
                        $("#defModalBody").html("Tanggal untuk voyages yang anda pilih tidak tersedia. Silahkan hubungi Admin/Nahkoda!");
                        $("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>");
                        $('#defModal').modal('show');
                    }
                    overlayOff();
                },
                error: function (jqXHR, status, err) {
                    overlayOff();
                    $("#defModalTitle").html("Voyages Dates");
                    $("#defModalBody").html("Terjadi kesalahan, Tutup peringatan ini dan coba kembali, atau refresh halaman ini dan mulai dari awal.!");
                    $("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>");
                    $('#defModal').modal('show');
                },
                complete: function (jqXHR, status) {
                    overlayOff();
                },
                timeout: 60000
            });
        } else {
            $("select[name='voyageKode']").html("");
        }
    }
}

// DIPAKAI DI REKAPITULASI BIAYA MAKANAN
function loadVoyagesByDateRange(urlgetDates, dateStart, dateEnd) {
    if(typeof overlayOn !== 'undefined') {
        overlayOn();
        $.ajax({
            url: urlgetDates,
            type: "GET",
            data: { dateStart : $("#" + dateStart).val(), dateEnd : $("#" + dateEnd).val() },
            dataType: "json",
            success: function (data, status, jqXHR) {    
                if(data) {
                    $("select[name='voyageKode']").html("");
                    $.each(data, function(idx, value) {
                        $("select[name='voyageKode']").append('<option value="' + value.voyageKode + '">' + value.voyageName + '</option>');                
                    });                    
                    // $('input[name=voyageStart]').val(data[0].voyage_from); $('input[name=voyageEnd]').val(data[0].voyage_to);
                    
                } else {
                    $("#defModalTitle").html("Voyages Dates");
                    $("#defModalBody").html("Tanggal untuk voyages yang anda pilih tidak tersedia. Silahkan hubungi Admin/Nahkoda!");
                    $("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>");
                    $('#defModal').modal('show');
                }
                overlayOff();
            },
            error: function (jqXHR, status, err) {
                overlayOff();
                $("#defModalTitle").html("Voyages Dates");
                $("#defModalBody").html("Terjadi kesalahan, Tutup peringatan ini dan coba kembali, atau refresh halaman ini dan mulai dari awal.!");
                $("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>");
                $('#defModal').modal('show');
            },
            complete: function (jqXHR, status) {
                overlayOff();
            },
            timeout: 60000
        });
    }
}

function userActivation(userID) {
    var deAct = $("#button" + userID).val();
    if(typeof overlayOn !== 'undefined') { 
        if(userID > 0) {
            overlayOn();
            $.ajax({
                url: currentUrl + "/activation",
                type: "GET",
                data: { activateCode : deAct, userID : userID },
                dataType: "json",
                success: function (data, status, jqXHR) {    
                    if(data) { 
                        if(deAct > 0) {
                            $("#activate" + userID).removeClass("fa-toggle-off");
                            $("#activate" + userID).addClass("fa-toggle-on");
                            $("#button" + userID).val(0);
                            $("#button" + userID).removeAttr("title");
                            $("#button" + userID).attr("title", "Deaktifkan User");
                        } else {
                            $("#activate" + userID).removeClass("fa-toggle-on");
                            $("#activate" + userID).addClass("fa-toggle-off");
                            $("#button" + userID).val(1);
                            $("#button" + userID).removeAttr("title");
                            $("#button" + userID).attr("title", "Aktifkan User");
                        }
                        $("#defModalTitle").html("User Activation");
                        $("#defModalBody").html("Data berhasil diperbaharui! ");
                        $("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>");
                        $('#defModal').modal('show');
                    } else {
                        $("#defModalTitle").html("User Activation");
                        $("#defModalBody").html("Data gagal diperbaharui! Silahkan coba lagi!");
                        $("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>");
                        $('#defModal').modal('show');
                    }
                    overlayOff();
                },
                error: function (jqXHR, status, err) {
                    overlayOff();
                    $("#defModalTitle").html("User Activation");
                    $("#defModalBody").html("Terjadi kesalahan, Tutup peringatan ini dan coba kembali, atau refresh halaman ini dan mulai dari awal.!");
                    $("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>");
                    $('#defModal').modal('show');
                },
                complete: function (jqXHR, status) {
                    overlayOff();
                },
                timeout: 60000
            });
        } else {
            $("#defModalTitle").html("User Activation");
            $("#defModalBody").html("Terjadi kesalahan, User ID tidak valid. Silahkan coba kembali, atau refresh halaman ini!");
            $("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>");
            $('#defModal').modal('show');
        }
    }
}

function loadVoyageDate(urlgetDates, vygCode) {
    if(typeof overlayOn !== 'undefined') { 
        if(vygCode != "") {
            overlayOn();
            $.ajax({
                url: urlgetDates,
                type: "GET",
                data: { voyageKode : vygCode },
                dataType: "json",
                success: function (data, status, jqXHR) {    
                    if(data) {
                        $('input[name=voyageKode]').val(data.voyageKode); 
                        // $('input[name=voyageName]').val(data.voyageName);
                        $('input[name=voyageStart]').val(data.voyage_from); 
                        $('input[name=voyageEnd]').val(data.voyage_to);
                    } else {
                        $("#defModalTitle").html("Voyages Dates");
                        $("#defModalBody").html("Tanggal untuk voyages yang anda pilih tidak tersedia. Silahkan hubungi Admin/Nahkoda!");
                        $("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>");
                        $('#defModal').modal('show');
                    }
                    overlayOff();
                },
                error: function (jqXHR, status, err) {
                    overlayOff();
                    $("#defModalTitle").html("Voyages Dates");
                    $("#defModalBody").html("Terjadi kesalahan, Tutup peringatan ini dan coba kembali, atau refresh halaman ini dan mulai dari awal.!");
                    $("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>");
                    $('#defModal').modal('show');
                },
                complete: function (jqXHR, status) {
                    overlayOff();
                },
                timeout: 60000
            });
        } else {
            $('input[name=voyageStart]').val(""); 
            $('input[name=voyageEnd]').val("");
        }
    }
}

$(".numericOnly").keypress(function(e){
    var regex = new RegExp("^[0-9]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }

    e.preventDefault();
    return false;
});

function checkDockDate(fieldName, tglAkhir, Yesterday) {
    if($("input[name='" + tglAkhir + "']").val() != "") {
        if($("input[name='" + fieldName + "']").val() != "") {            
            var needBy = new Date($("input[name='" + fieldName + "']").val()).getTime();
            var PODate = new Date($("input[name='" + tglAkhir + "']").val()).getTime();
            var today = new Date(Yesterday).getTime();

            if(needBy < PODate) { 
                $("input[name='" + fieldName + "']").val("");
                alert("Tanggal diterima tidak boleh lebih kecil dari Tanggal Awal: " + $("input[name='" + tglAkhir + "']").val());
                $("input[name='" + fieldName + "']").focus();
            }
            if(needBy > today)
            {
                $("input[name='" + fieldName + "']").val("");
                alert("Tanggal barang diterima tidak boleh lebih besar dari hari kemarin");
                $("input[name='" + fieldName + "']").focus();
            }
        }
    } else {
        // $("input[name='" + fieldName + "']").val("");
        // alert("Lengkapi tanggal Voyage terlebih dahulu!");
        // $("input[name='" + fieldName + "']").focus();        
    }
}