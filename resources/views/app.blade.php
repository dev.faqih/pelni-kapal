<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,
minimum-scale=1.0">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{$title}}</title>
    <link href="{{URL::asset("/assets/css/stylesheet.css")}}" rel="stylesheet">
    <style>
        .dropdown-menu > li > a.nav-link {
            color: #2e2e2e !important;
        }

        .dropdown-menu > li > a.nav-link:hover {
            background-color: #eee !important;
        }

        .loader {
          border: 16px solid #f3f3f3;
          border-radius: 50%;
          border-top: 16px solid red;
          border-bottom: 16px solid red;
          width: 120px;
          height: 120px;
          -webkit-animation: spin 2s linear infinite;
          animation: spin 2s linear infinite;
        }

        @-webkit-keyframes spin {
          0% { -webkit-transform: rotate(0deg); }
          100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
          0% { transform: rotate(0deg); }
          100% { transform: rotate(360deg); }
        }

        #overlay {
            position: fixed;
            display: none;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(0,0,0,0.5);
            z-index: 2;
            cursor: pointer;
        }

        #overlayText {
            position: absolute;
            top: 50%;
            left: 50%;
            font-size: 50px;
            color: white;
            transform: translate(-50%,-50%);
            -ms-transform: translate(-50%,-50%);
        }
    </style>
</head>
<body class="page-wrapper">
<div id="overlay">
  <div id="overlayText"><div class="loader"></div></div>
</div>
@yield("content")

<div class="modal fade" id="generalModal" tabindex="-1" role="dialog" aria-labelledby="generalModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div id="generalModal-title" class="modal-header">
        
        <h4 class="modal-title" id="myModalLabel"></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div id="generalModal-content" class="modal-body">
      </div>
      <div id="generalModal-footer" class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button id="modalAgree" type="button" class="btn btn-primary">Yes</button>
      </div>
    </div>
  </div>
</div>
<script>
function overlayOn() {
  document.getElementById("overlay").style.display = "block";
}

function overlayOff() {
  document.getElementById("overlay").style.display = "none";
}
</script>

</body>
</html>