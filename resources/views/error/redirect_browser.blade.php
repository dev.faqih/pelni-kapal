@extends('app')
@section('content')
    <div class="container content">
        <div class=" text-left" style="margin-top: 20%">
            <h1 class=" text-yellow"> Redirect Page!</h1>
            <h2 class=" text-yellow"> Oops! For better experience and compability, please use Chrome as your browser!</h2>
        </div>
    </div>
@endsection

