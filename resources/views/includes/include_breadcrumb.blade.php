<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        @for($i=0;$i<count($segments);$i++)
            <li class="breadcrumb-item">
                <a href="{{url($segments[$i]["url"])}}">
                    {{ucfirst($segments[$i]["name"])}}
                </a>
            </li>
        @endfor
    </ol>
</nav>