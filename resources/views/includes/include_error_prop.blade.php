@if(!empty(session('successMessages')))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Success!</h4>
        {{session('successMessages')}}
    </div>
@endif
@if(session('errMessage'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
        {{session('errMessage')}}
    </div>

@endif
@if (session('errValidation'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
        @if(!is_array(session('errValidation')))
            {{session('errValidation')}}
        @else
            <ul>
                @foreach(session('errValidation') as $key =>$value)
                    <li>{{$key ." : ".$value}}</li>
                @endforeach
            </ul>
        @endif
    </div>
@endif
<div class="clearfix">&nbsp;</div>