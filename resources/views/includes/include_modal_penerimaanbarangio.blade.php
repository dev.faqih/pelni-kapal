<!-- Modal -->
<div class="modal fade" id="itemModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="itemModalLongTitle">Tambah Item</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        
        <div id="carouselItems" class="carousel slide" data-interval="false" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">

                    <form autocomplete="off"  id="formItemSearch" action="{{url("administrator/popenerimaanio/search")}}" role="form" method="get" class="formItem" enctype="multipart/form-data">
                        <input type="hidden" name="_method" value="get">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-md-3 col-xs-12">
                            <select class="form-control input-default" name="q" title="Search Something">
                                    @foreach($criteria as $key => $value)
                                        <option value="ioNumber:{{ $popenerimaanio->ioNumber }}||{{ $key }}">{{ $value }}</option>
                                    @endforeach
                            </select>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <input class="form-control input-default" name="search-item" title="Search Value">
                            </div>
                            <div class="col-md-6">
                                <button type="button" class="btn btn-success" onClick="searchItemModal()" title="Search"><span class="fa fa-search"></span> Search</button> &nbsp; 
                                <button type="button" class="btn btn-success" onClick="resetItemModal()" title="Search"><span class="fa fa-eraser"></span> Reset</button>
                            </div>
                        </div>
                    </form>
                    <br />
                    <div id="listItemSearched"></div>
                    <div id="listItemPaging" class="row">
                        <div class="col-md-6 text-left col-xs-12">
                            Page <span id="itemPageNo"></span> of <span id="itemPageCount"></span>
                            Total Row <span id="itemTotalRow"></span>
                        </div>
                        <div class="col-md-6  col-xs-12">
                           <ul class="pagination pull-right">
                               <li class="page-item" id="itemPageFirst">
                                   <button class="page-link" onClick="showItemModalFirst();" tabindex="-1" name="itemPageFirst" title="First Page">First</button>
                               </li>
                               <li class="page-item" id="itemPagePrev">
                                    <button class="page-link" onClick="showItemModalPrev();" name="itemPagePrev" title="Prev Page"><i class="fa fa-step-backward"></i></button>
                               </li>
                               <li class="page-item" id="itemPageNext">
                                    <button class="page-link" onClick="showItemModalNext();" name="itemPageNext" title="Next Page"><i class="fa fa-step-forward"></i></button></li>
                               <li class="page-item" id="itemPageLast">
                                   <button class="page-link" onClick="showItemModalLast();" name="itemPageLast" title="Last Page">Last</button>
                               </li>
                           </ul>
                        </div>
                    </div>
                </div>

                <div class="carousel-item">

                    <form  autocomplete="off" id="formItemSearchDetail">
                        <input type="hidden" name="id" value="" />
                        <input type="hidden" name="tglMasuk" value="{{ date("Y-m-d") }}">
                        <input type="hidden" name="harga_satuan" value="0">
                        <div class="row">
                            <div class="col-sm-2 col-xs-5">
                                <label for="#namaItem">Kode Item</label>
                            </div>
                            <div class="col-sm-4 col-xs-7">
                                <input type="text" disabled class="form-control input-default "  placeholder="Masukkan nama item" name="namaItem" value="" title="Kode Item">
                            </div>
                            <div class="col-sm-2 col-xs-5">
                                <label for="#deskripsi">Deskripsi</label>
                            </div>
                            <div class="col-sm-4 col-xs-7">
                                <input type="text" disabled class="form-control input-default "  placeholder="Masukkan deskripsi" name="deskripsi" value="" title="Deskripsi">
                            </div>
                        </div> <br />

                        <div class="row">
                            <div class="col-sm-2 col-xs-5">
                                <label for="#partNumber">Part Number</label>
                            </div>
                            <div class="col-sm-4 col-xs-7">
                                <input type="text" disabled class="form-control input-default "  placeholder="-" name="partNumber" value="" title="partNumber">
                            </div>
                            <div class="col-sm-2 col-xs-5">
                                <label for="#lotNumber">Lot Number</label>
                            </div>
                            <div class="col-sm-4 col-xs-7">
                                <input class="form-control input-default " readonly placeholder="Masukkan lot number" name="lotNumber" value="" title="lotNumber">
                            </div>
                        </div> <br />

                        <div class="row">
                            <div class="col-sm-2 col-xs-5">
                                <label for="#category">Kategori</label>
                            </div>
                            <div class="col-sm-4 col-xs-7">
                                <input type="text" disabled class="form-control input-default "  placeholder="Masukkan category" name="category" value="" title="category">
                            </div>
                            <div class="col-sm-2 col-xs-5">
                                <label for="#uom">UOM</label>
                            </div>
                            <div class="col-sm-4 col-xs-7">
                                <input type="text" disabled class="form-control input-default "  placeholder="Masukkan UOM" name="UOM" value="" title="UOM">
                            </div>
                        </div> <br />

                        <div class="row">
                            <div class="col-sm-2 col-xs-5">
                                <label for="#qty">Qty.</label>
                            </div>
                            <div class="col-sm-4 col-xs-7">
                                <input type="text" class="form-control input-default " maxlength="7" placeholder="Masukkan quantity" name="qty" value="" title="qty">
                            </div>
                            <div class="col-sm-2 col-xs-5">
                                <label for="#lokasiId">Sub-Inventory</label>
                            </div>
                            <div class="col-sm-4 col-xs-7">
                                <select class="form-control input-default" name="lokasiId" title="lokasiId">
                                    @foreach($lokasiitems as $lokasiitem)
                                        <option value="{{$lokasiitem->id}}">{{ $lokasiitem->namaLokasi }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div> <br />

                    </form>
                </div>
            </div>

		

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" name="backItemSearched" onClick="backItemSearched()" class="btn btn-default">Back</button>
        <button type="button" name="saveItemSearched" onClick="saveItemSearched();" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
    var itemList = [];
    var tipeList = "A";
    var stockItem = 0;
    function addItemDesc(itemID) {
        $("button[name=saveItemSearched]").show();
        $("button[name=backItemSearched]").show();

        $("#formItemSearchDetail input[name=id]").val(itemID);
        $("#formItemSearchDetail input[name=namaItem]").val( $("#item" + itemID).attr("data-namaitem") );
        $("#formItemSearchDetail input[name=deskripsi]").val( $("#item" + itemID).attr("data-deskripsi") );
        $("#formItemSearchDetail input[name=partNumber]").val( $("#item" + itemID).attr("data-partnumber"));
        $("#formItemSearchDetail input[name=category]").val( $("#item" + itemID).attr("data-category") );
        $("#formItemSearchDetail input[name=UOM]").val( $("#item" + itemID).attr("data-uom") );
        $("#formItemSearchDetail input[name=lotNumber]").val( $("#item" + itemID).attr("data-lotnumber") );
        $("#formItemSearchDetail input[name=harga_satuan]").val( $("#item" + itemID).attr("data-harga") );
        // $("#formItemSearchDetail input[name=tglMasuk]").val( $("#item" + itemID).attr("data-tglmasuk") );

        $("#formItemSearchDetail input[name=qty]").val("");
        $("#formItemSearchDetail select[name=lokasiId]").val("");
        $('#carouselItems').carousel("next");

        /* $.ajax({
            url: "{{url("administrator/popenerimaanio/generateLotNumber")}}",
            type: "GET",
            dataType: "json",
            success: function (data, status, jqXHR) {            
                $("#formItemSearchDetail input[name=lotNumber]").val( $("input[name=kodeKapal]").val() + data );
                $('#carouselItems').carousel("next");
            },
            error: function (jqXHR, status, err) {
                alert("Terjadi kesalahan, Tutup peringatan ini dan coba kembali, atau refresh halaman ini dan mulai dari awal.");
            }
        }); */
    }

    function saveItemSearched() {
        var itemID      = $("#formItemSearchDetail input[name=id]").val();
        var namaItem    = $("#formItemSearchDetail input[name=namaItem]").val();
        var partNumber  = $("#formItemSearchDetail input[name=partNumber]").val();
        var deskripsi   = $("#formItemSearchDetail input[name=deskripsi]").val();
        var category    = $("#formItemSearchDetail input[name=category]").val();
        var UOM         = $("#formItemSearchDetail input[name=UOM]").val();
        var tglMasuk    = $("#formItemSearchDetail input[name=tglMasuk]").val();

        // var harga = parseInt( $("#formItemSearchDetail input[name=harga]").val() );
        stockItem       = parseFloat( $("#item" + itemID).attr("data-jumlah") );
        var qty         = parseFloat( $("#formItemSearchDetail input[name=qty]").val() );
        var lokasiId    = parseInt($("#formItemSearchDetail select[name=lokasiId]").val());
        var namaLokasi  = $("#formItemSearchDetail select[name=lokasiId] option:selected").text();
        var lotNumber   = $("#formItemSearchDetail input[name=lotNumber]").val();
        var harga_satuan   = $("#formItemSearchDetail input[name=harga_satuan]").val();

        if(itemList.indexOf(itemID) >= 0) {            
            alert("Data sudah ada di list!");  
            backItemSearched();
        } else {
            if(! (qty > 0) ) {
                alert("Masukkan jumlah barang!");
                
                $("#formItemSearchDetail input[name=qty]").focus();
            } else if(qty > stockItem) {
                alert("Jumlah yang dimasukkan tidak boleh melebihi kuantiti PO");   

                $("#formItemSearchDetail input[name=qty]").val(stockItem);             
                $("#formItemSearchDetail input[name=qty]").focus();
            } else if(! (lokasiId > 0)) {
                alert("Belum ada subinventory yang dipilih!");

                $("#formItemSearchDetail select[name=lokasiId]").focus();
            } else if(lotNumber == "") {
                $("#formItemSearchDetail input[name=lotNumber]").focus();
            } else {
                itemList.push(itemID);
                printItemIntoList(itemID, namaItem, partNumber, deskripsi, category, UOM, tglMasuk, qty, lokasiId, namaLokasi, lotNumber, harga_satuan);            
                $("#itemModal").modal('hide');
            }
        }
    }

    function printItemIntoList(itemID, namaItem, partNumber, deskripsi, category, UOM, tglMasuk, qty, lokasiId, namaLokasi, lotNumber, harga_satuan) {
        var content = "<tr id=\"listItem" + itemID + "\" ><input type=\"hidden\" name=\"itemID[" + itemID + "]\" value=\"" + itemID + "\" />";
            content += "<input type=\"hidden\" name=\"tglMasuk[" + itemID + "]\" value=\"" + tglMasuk + "\" />";
            content += "<input type=\"hidden\" name=\"namaItem[" + itemID + "]\" value=\"" + namaItem + "\" />";
            content += "<input type=\"hidden\" name=\"namaLokasi[" + itemID + "]\" value=\"" + namaLokasi + "\" />";
            content += "<input type=\"hidden\" name=\"UOM[" + itemID + "]\" value=\"" + UOM + "\" />";
            content += "<input type=\"hidden\" name=\"harga_satuan[" + itemID + "]\" value=\"" + harga_satuan + "\" />";
        // if(item.image != "") { content += "<img class=\"card-img-top\" src=\"" + item.image + "\" alt=\"Card image cap\">"; }
            content += "<td><span id=\"noList" + itemID + "\">" + (itemList.length) + ".</span></td><td>" + namaItem + "</td><td>" + partNumber + "</td><td>" + deskripsi + "</td><td>" + UOM + "</td><td><input type=\"hidden\" name=\"quantity[" + itemID + "]\" value=\"" + qty + "\" />" + qty + "</td>";
            content += "<td><input type=\"hidden\" name=\"lokasiId[" + itemID + "]\" value=\"" + lokasiId + "\" />" + namaLokasi + "</td>";
            content += "<td><input type=\"hidden\" name=\"lotNumber[" + itemID + "]\" value=\"" + lotNumber + "\" />" + lotNumber + "</td>";
            content += "<td><button type=\"button\" class=\"btn outline-dark\" onClick=\"deleteListedItem(" + itemID + ")\" title=\"Delete Item\" name=\"delete-item\"><span class=\"fa fa-trash\"></span></button></td></tr>";
            $("#tableItem tbody").append(content);
    }

    

    $('#itemModal').on('show.bs.modal', function (event) {
        showItemModal();
    });

</script>