<!-- Modal Food Report -->
<div class="modal fade" id="foodReportModal" tabindex="-1" role="dialog" aria-labelledby="foodReportModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <form autocomplete="off" target="_blank" action="{{ url("administrator/permintaanbarang/foodPDF/") }}" method="POST">
            <input type="hidden" class="transactionID" name="transactionID" />
            <input type="hidden" name="_method" value="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="modal-header">
                <h5 class="modal-title" id="foodReportModalLabel">Parameter Laporan Makanan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                

                
                    <div class="row">
                        <div class="col-sm-2 col-xs-5">
                            <label for="#namaItem">No PR</label>
                        </div>
                        <div class="col-sm-4 col-xs-7">
                            <input type="text" readonly class="form-control input-default noPR" name="noPR" >
                        </div>
                        <div class="col-sm-2 col-xs-5">
                            <label for="#deskripsi">Estimasi Jumlah Orang</label>
                        </div>
                        <div class="col-sm-4 col-xs-7">
                            <input type="text" maxlength="7" class="form-control input-default numericOnly" name="jlhOrang" placeholder="Estimasi Jumlah Orang" >
                        </div>
                    </div> <br />

                    <div class="row">
                        <div class="col-sm-2 col-xs-5">
                            <label for="#partNumber">Tanggal Supply</label>
                        </div>
                        <div class="col-sm-4 col-xs-7">
                            <!-- <input type="text" class="form-control input-default "  placeholder="Tanggal Supply" name="tglSupply" value="" title="Tanggal di Supply"> -->
                            <input class=" form-control datepicker input-default search-value" data-date-clear-btn="true" data-date-autoclose="true" data-date-format="yyyy-mm-dd" name="tglSupply" >
                        </div>
                        <div class="col-sm-2 col-xs-5">
                            <label for="#uom">Di Supply di Pelabuhan</label>
                        </div>
                        <div class="col-sm-4 col-xs-7">
                            <input type="text" class="form-control input-default " placeholder="Nama Pelabuhan" name="namaPelabuhan" value="" title="Nama Pelabuhan di Supply" maxlength="25">
                        </div>
                    </div> <br />
                    
                    <div class="row">
                        <div class="col-sm-2 col-xs-5">
                            <label for="#partNumber">Rev</label>
                        </div>
                        <div class="col-sm-4 col-xs-7">
                            <input type="text" class="form-control input-default" placeholder="Rev" name="rev" value="" title="Rev" maxlength="25">
                        </div>
                    </div>
            


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Generate Report</button>
            </div>

            </form>

        </div>
    </div>
</div>


<!-- Modal SPB Report -->
<div class="modal fade" id="spbReportModal" tabindex="-1" role="dialog" aria-labelledby="spbReportModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <form autocomplete="off" target="_blank" action="{{ url("administrator/permintaanbarang/generateSPB/") }}" method="POST">
            <input type="hidden" class="transactionID" name="transactionID" />
            <input type="hidden" name="_method" value="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="modal-header">
                <h5 class="modal-title" id="spbReportModalLabel">Parameter Laporan SPB</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-2 col-xs-5">
                            <label for="#namaItem">No PR</label>
                        </div>
                        <div class="col-sm-4 col-xs-7">
                            <input type="text" readonly class="form-control input-default noPR" name="noPR" >
                        </div>
                        <div class="col-sm-2 col-xs-5">
                            <label for="#partNumber">Kode</label>
                        </div>
                        <div class="col-sm-4 col-xs-7">
                            <input type="text" class="form-control input-default" placeholder="Kode" name="kode" value="" maxlength="25" title="Kode">
                        </div>
                        
                    </div> <br />

                   
                    
                    <div class="row">
                        <div class="col-sm-2 col-xs-5">
                            <label for="#partNumber">Rev</label>
                        </div>
                        <div class="col-sm-4 col-xs-7">
                            <input type="text" class="form-control input-default" placeholder="Rev" name="rev" value="" maxlength="25" title="Rev">
                        </div>
                        <div class="col-sm-2 col-xs-5">
                            <label for="#deskripsi">Bagian</label>
                        </div>
                        <div class="col-sm-4 col-xs-7">
                            <input type="text" class="form-control input-default" placeholder="Bagian" name="bagian" value="" maxlength="25" title="Bagian">
                        </div>
                    </div>
            


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Generate Report</button>
            </div>
            </form>
        </div>
    </div>
</div>



<!-- Modal SPB BBM Report -->
<div class="modal fade" id="bbmReportModal" tabindex="-1" role="dialog" aria-labelledby="bbmReportModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <form autocomplete="off" target="_blank" action="{{ url("administrator/permintaanbarang/generateBBM/") }}" method="POST">
                <input type="hidden" class="transactionID" name="transactionID" />
                <input type="hidden" name="_method" value="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="modal-header">
                    <h5 class="modal-title" id="bbmReportModalLabel">Parameter Laporan SPB BBM &amp; MINYAK PELUMAS</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-2 col-xs-5">
                            <label for="#namaItem">No PR</label>
                        </div>
                        <div class="col-sm-4 col-xs-7">
                            <input type="text" readonly class="form-control input-default noPR" name="noPR" >
                        </div>
                        <div class="col-sm-2 col-xs-5">
                            <label for="#partNumber">Nomor</label>
                        </div>
                        <div class="col-sm-4 col-xs-7">
                            <input type="text" class="form-control input-default" placeholder="Nomor" name="nomor" value="" maxlength="25" title="Nomor">
                        </div>

                    </div> <br />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Generate Report</button>
                </div>
            </form>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="itemModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="itemModalLongTitle">Tambahh Item</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
        
        
            <div id="carouselItems" class="carousel slide" data-interval="false" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">

                        <form autocomplete="off"  id="formItemSearch" action="{{url("administrator/permintaanbarang/search")}}" role="form" method="get" class="formItem" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="get">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="row">
                                <div class="col-md-3 col-xs-12">
                                    <select class="form-control input-default" name="q" title="Search Something">
                                        @if(isset($criteria))
                                            @foreach($criteria as $key => $value)
                                                <option value="{{$key}}">{{ $value }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="col-md-3 col-xs-12">
                                    <input class="form-control input-default" name="search-item" title="Search Value">
                                </div>
                                <div class="col-md-6">
                                    <button type="button" class="btn btn-success" onClick="searchItemModal()" title="Search"><span class="fa fa-search"></span> Search</button> &nbsp;
                                    <button type="button" class="btn btn-success" onClick="resetItemModal()" title="Search"><span class="fa fa-eraser"></span> Reset</button>
                                </div>
                            </div>
                        </form>
                        <br />
                        <div id="listItemSearched"></div>
                        <br />
                        <div id="listItemPaging" class="row">
                            <div class="col-md-6 text-left col-xs-12">
                                Page <span id="itemPageNo"></span> of <span id="itemPageCount"></span>
                                Total Row <span id="itemTotalRow"></span>
                            </div>
                            <div class="col-md-6  col-xs-12">
                               <ul class="pagination pull-right">
                                   <li class="page-item" id="itemPageFirst">
                                       <button class="page-link" onClick="showItemModalFirst();" tabindex="-1" name="itemPageFirst" title="First Page">First</button>
                                   </li>
                                   <li class="page-item" id="itemPagePrev">
                                        <button class="page-link" onClick="showItemModalPrev();" name="itemPagePrev" title="Prev Page"><i class="fa fa-step-backward"></i></button>
                                   </li>
                                   <li class="page-item" id="itemPageNext">
                                        <button class="page-link" onClick="showItemModalNext();" name="itemPageNext" title="Next Page"><i class="fa fa-step-forward"></i></button></li>
                                   <li class="page-item" id="itemPageLast">
                                       <button class="page-link" onClick="showItemModalLast();" name="itemPageLast" title="Last Page">Last</button>
                                   </li>
                               </ul>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">

                        <form  autocomplete="off" id="formItemSearchDetail">
                            <input type="hidden" name="id" value="" />
                            <input type="hidden" name="harga" value="0">
                            <div class="row">
                                <div class="col-sm-2 col-xs-5">
                                    <label for="#namaItem">Kode Item</label>
                                </div>
                                <div class="col-sm-4 col-xs-7">
                                    <input type="text" disabled class="form-control input-default "  placeholder="Masukkan nama item" name="namaItem" value="" title="Kode Item">
                                </div>
                                <div class="col-sm-2 col-xs-5">
                                    <label for="#deskripsi">Deskripsi</label>
                                </div>
                                <div class="col-sm-4 col-xs-7">
                                    <input type="text" disabled class="form-control input-default "  placeholder="Masukkan deskripsi" name="deskripsi" value="" title="Deskripsi">
                                </div>
                            </div> <br />

                            <div class="row">
                                <div class="col-sm-2 col-xs-5">
                                    <label for="#partNumber">Part Number</label>
                                </div>
                                <div class="col-sm-4 col-xs-7">
                                    <input type="text" disabled class="form-control input-default "  placeholder="-" name="partNumber" value="" title="partNumber">
                                </div>
                                <div class="col-sm-2 col-xs-5">
                                    <label for="#uom">UOM</label>
                                </div>
                                <div class="col-sm-4 col-xs-7">
                                    <input type="text" disabled class="form-control input-default "  placeholder="Masukkan UOM" name="UOM" value="" title="UOM">
                                </div>
                            </div> <br />

                            <div class="row">
                                <div class="col-sm-2 col-xs-5">
                                    <label for="#category">Kategori</label>
                                </div>
                                <div class="col-sm-4 col-xs-7">
                                    <input type="text" disabled class="form-control input-default "  placeholder="Masukkan category" name="category" value="" title="category">
                                </div>
                                <div class="col-sm-2 col-xs-5">
                                    <label for="#lotNumber">Tgl Dibutuhkan</label>
                                </div>
                                <div class="col-sm-4 col-xs-7">
                                    <input type="text" onChange="checkIsSmallerThanToday('needBy');" class="form-control input-default datepicker" data-date-format="yyyy-mm-dd" data-date-autoclose="true" require placeholder="" name="needBy" value="" title="needBy">
                                </div>
                            </div> <br />

                            <div class="row">
                                <div class="col-sm-2 col-xs-5">
                                    <label for="#qty">Qty.</label>
                                </div>
                                <div class="col-sm-4 col-xs-7">
                                    <input type="text" class="form-control input-default" maxlength="7" placeholder="Masukkan qty" name="qty" value="" title="qty">
                                </div>
                            </div> <br />

                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <ul class="nav nav-tabs" id="itemTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="bbm-tab" data-toggle="tab" href="#tabBBM" role="tab" aria-controls="bbm" aria-selected="true">BBM/Pelumas</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="itemTabContent">
                                        <div class="tab-pane fade show active" id="tabBBM" role="tabpanel" aria-labelledby="bbm-tab"><br />
                                            <div class="row">
                                                <div class="col-sm-2 col-xs-5">
                                                    <label for="#qty">Qty. Sisa ROB</label>
                                                </div>
                                                <div class="col-sm-4 col-xs-7">
                                                    <input type="text" class="form-control input-default" maxlength="7" placeholder="Masukkan Qty. Sisa ROB" name="qtyRob" value="" title="Masukkan Qty. Sisa ROB">
                                                </div>
                                                <div class="col-sm-2 col-xs-5">
                                                    <label for="#qty">Qty. Penggunaan</label>
                                                </div>
                                                <div class="col-sm-4 col-xs-7">
                                                    <input type="text" class="form-control input-default" maxlength="7" placeholder="Masukkan Qty. Penggunaan Voyage Selanjutnya" name="qtyPenggunaan" value="" title="Masukkan Qty. Penggunaan Voyage Selanjutnya">
                                                </div>
                                            </div> <br />
                                            <div class="row">
                                                <div class="col-sm-2 col-xs-5">
                                                    <label for="#qty">Pelabuhan Pengisian</label>
                                                </div>
                                                <div class="col-sm-4 col-xs-7">
                                                    <select class="form-control input-default" name="portBunker" title="Pelabuhan Pengisian">
                                                    @foreach($portBunker as $port)
                                                        <option value="{{ $port }}">{{ $port }}</option>
                                                    @endforeach
                                                    </select>
                                                </div>
                                            </div> <br />
                                        </div>
                                    </div>
                                </div>
                            </div> <br />

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" name="backItemSearched" onClick="backItemSearched()" class="btn btn-default">Back</button>
            <button type="button" name="saveItemSearched" onClick="saveItemSearched();" class="btn btn-primary">Save changes</button>
        </div>
    </div>
  </div>
</div>


<script type="text/javascript">
    var itemList = [];
    var tipeList = "";
    function addItemDesc(itemID) {
        $("button[name=saveItemSearched]").show();
        $("button[name=backItemSearched]").show();

        $("#formItemSearchDetail input[name=id]").val(itemID);
        $("#formItemSearchDetail input[name=namaItem]").val( $("#item" + itemID).attr("data-namaitem") );
        $("#formItemSearchDetail input[name=deskripsi]").val( $("#item" + itemID).attr("data-deskripsi") );
        $("#formItemSearchDetail input[name=category]").val( $("#item" + itemID).attr("data-category") );
        $("#formItemSearchDetail input[name=UOM]").val( $("#item" + itemID).attr("data-uom") );
        $("#formItemSearchDetail input[name=partNumber]").val( $("#item" + itemID).attr("data-partnumber") );
        $("#formItemSearchDetail input[name=harga]").val( $("#item" + itemID).attr("data-harga") );

        $("#formItemSearchDetail input[name=qty]").val("");
        $("#formItemSearchDetail input[name=needBy]").val("");
        $('#carouselItems').carousel("next");
    }

    function saveItemSearched() {
        var itemID      = $("#formItemSearchDetail input[name=id]").val();
        var namaItem    = $("#formItemSearchDetail input[name=namaItem]").val();
        var deskripsi   = $("#formItemSearchDetail input[name=deskripsi]").val();
        var partNumber  = $("#formItemSearchDetail input[name=partNumber]").val();
        var category    = $("#formItemSearchDetail input[name=category]").val();
        var UOM         = $("#formItemSearchDetail input[name=UOM]").val();
        var qty         = parseFloat( $("#formItemSearchDetail input[name=qty]").val() );
        var needBy      = $("#formItemSearchDetail input[name=needBy]").val();
        var harga       = parseInt( $("#formItemSearchDetail input[name=harga]").val() );
        var qtyRob      = $("#formItemSearchDetail input[name=qtyRob]").val();
        var qtyPenggunaan = $("#formItemSearchDetail input[name=qtyPenggunaan]").val();
        var portBunker = $("#formItemSearchDetail select[name=portBunker] option:selected").text();

        if(itemList.indexOf(itemID) >= 0) {            
            alert("Data sudah ada di list!");  
            backItemSearched();
        } else {           
            if(! (qty > 0) ) {
                alert("Masukkan jumlah barang!");

                $("#formItemSearchDetail input[name=qty]").focus();
            } else if(needBy == "") {
                $("#formItemSearchDetail input[name=needBy]").focus();
            } else {
                itemList.push(itemID);
                // harga = harga * qty;
                printItemIntoList(itemID, namaItem, partNumber, deskripsi, category, UOM, qty, needBy, harga, qtyRob, qtyPenggunaan, portBunker);
                $("#itemModal").modal('hide');
            }
        }
    }

    function printItemIntoList(itemID, namaItem, partNumber, deskripsi, category, UOM, qty, needBy, harga, qtyRob, qtyPenggunaan, portBunker) {
        var content = "<tr id=\"listItem" + itemID + "\" ><td><span id=\"noList" + itemID + "\">" + (itemList.length) + ".</span></td>";
            content += "<td><input type=\"hidden\" name=\"itemID[" + itemID + "]\" value=\"" + itemID + "\" />";
            content += "<input type=\"hidden\" name=\"harga[" + itemID + "]\" value=\"" + harga + "\" />";
            content += "<input type=\"hidden\" name=\"qtyRob[" + itemID + "]\" value=\"" + qtyRob + "\" />";
            content += "<input type=\"hidden\" name=\"qtyPenggunaan[" + itemID + "]\" value=\"" + qtyPenggunaan + "\" />";
            content += "<input type=\"hidden\" name=\"portBunker[" + itemID + "]\" value=\"" + portBunker + "\" />";
            content += namaItem + "</td><td>" + partNumber + "</td><td>" + deskripsi + "</td><td>" + category + "</td><td class='text-center'>" + UOM + "</td>";
            content += "<td class='text-center'><input type=\"hidden\" name=\"quantity[" + itemID + "]\" value=\"" + qty + "\" />" + qty + "</td>";
            content += "<td class='text-center'><input type=\"hidden\" name=\"needBy[" + itemID + "]\" value=\"" + needBy + "\" />" + needBy + "</td>";
            content += "<td><button type=\"button\" class=\"btn outline-dark\" onClick=\"deleteListedItem(" + itemID + ")\" title=\"Delete Item\" name=\"delete-item\"><span class=\"fa fa-trash\"></span></button></td></tr>";
            $("#tableItem tbody").append(content);
    }
    $('#itemModal').on('show.bs.modal', function (event) {
        showItemModal();
    });

    function loadReport(reportType, transactionID, noPR) {
        $(".transactionID").val(transactionID);
        $(".noPR").val(noPR);
        if(reportType == "FOOD") {
            $("#foodReportModal").modal('show');            
        } else if(reportType == "UMUM") {
            $("#spbReportModal").modal('show');
        } else if(reportType == "BBM") {
            $("#bbmReportModal").modal('show');
        }
    }
</script>
