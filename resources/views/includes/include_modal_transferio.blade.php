<!-- Modal -->
<div class="modal fade" id="itemModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="itemModalLongTitle">Tambah Item</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        
        <div id="carouselItems" class="carousel slide" data-interval="false" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">

                    <form autocomplete="off"  id="formItemSearch" action='{{url("administrator/transferio/search")}}' role="form" method="get" class="formItem" enctype="multipart/form-data">
                        <input type="hidden" name="_method" value="get">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-md-3 col-xs-12">
                            <select class="form-control input-default" name="q" title="Search Something">
                                    @foreach($criteria as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                            </select>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <input class="form-control input-default" name="search-item" title="Search Value">
                            </div>
                            <div class="col-md-6">
                                <button type="button" class="btn btn-success" onClick="searchItemModal()" title="Search"><span class="fa fa-search"></span> Search</button> &nbsp; 
                                <button type="button" class="btn btn-success" onClick="resetItemModal()" title="Search"><span class="fa fa-eraser"></span> Reset</button>
                            </div>
                        </div>
                    </form>
                    <br />
                    <div id="listItemSearched"></div>
                    <div id="listItemPaging" class="row">
                        <div class="col-md-6 text-left col-xs-12">
                            Page <span id="itemPageNo"></span> of <span id="itemPageCount"></span>
                            Total Row <span id="itemTotalRow"></span>
                        </div>
                        <div class="col-md-6  col-xs-12">
                           <ul class="pagination pull-right">
                               <li class="page-item" id="itemPageFirst">
                                   <button class="page-link" onClick="showItemModalFirst();" tabindex="-1" name="itemPageFirst" title="First Page">First</button>
                               </li>
                               <li class="page-item" id="itemPagePrev">
                                    <button class="page-link" onClick="showItemModalPrev();" name="itemPagePrev" title="Prev Page"><i class="fa fa-step-backward"></i></button>
                               </li>
                               <li class="page-item" id="itemPageNext">
                                    <button class="page-link" onClick="showItemModalNext();" name="itemPageNext" title="Next Page"><i class="fa fa-step-forward"></i></button></li>
                               <li class="page-item" id="itemPageLast">
                                   <button class="page-link" onClick="showItemModalLast();" name="itemPageLast" title="Last Page">Last</button>
                               </li>
                           </ul>
                        </div>
                    </div>
                </div>

                <div class="carousel-item">

                    <form  autocomplete="off" id="formItemSearchDetail">
                        <input type="hidden" name="id" value="" />
                        <input type="hidden" name="stokID" value="0">
                        <input type="hidden" name="fromLokasiId" value="0">
                        <input type="hidden" name="tglMasuk" value="{{ date("Y-m-d") }}">
                        <div class="row">
                            <div class="col-sm-2 col-xs-5">
                                <label for="#namaItem">Kode Item</label>
                            </div>
                            <div class="col-sm-4 col-xs-7">
                                <input type="text" disabled class="form-control input-default "  placeholder="Masukkan nama item" name="namaItem" value="" title="Kode Item">
                            </div>
                            <div class="col-sm-2 col-xs-5">
                                <label for="#deskripsi">Deskripsi</label>
                            </div>
                            <div class="col-sm-4 col-xs-7">
                                <input type="text" disabled class="form-control input-default "  placeholder="Masukkan deskripsi" name="deskripsi" value="" title="Deskripsi">
                            </div>
                        </div> <br />

                        <div class="row">
                            <div class="col-sm-2 col-xs-5">
                                <label for="#partNumber">Part Number</label>
                            </div>
                            <div class="col-sm-4 col-xs-7">
                                <input type="text" disabled class="form-control input-default "  placeholder="-" name="partNumber" value="" title="partNumber">
                            </div>
                            <div class="col-sm-2 col-xs-5">
                                <label for="#lotNumber">Lot Number</label>
                            </div>
                            <div class="col-sm-4 col-xs-7">
                                <input class="form-control input-default " readonly placeholder="Masukkan lot number" name="lotNumber" value="" title="lotNumber">
                            </div>
                        </div> <br />

                        <div class="row">
                            <div class="col-sm-2 col-xs-5">
                                <label for="#category">Kategori</label>
                            </div>
                            <div class="col-sm-4 col-xs-7">
                                <input type="text" disabled class="form-control input-default "  placeholder="Masukkan category" name="category" value="" title="category">
                            </div>
                            <div class="col-sm-2 col-xs-5">
                                <label for="#uom">UOM</label>
                            </div>
                            <div class="col-sm-4 col-xs-7">
                                <input type="text" disabled class="form-control input-default "  placeholder="Masukkan UOM" name="UOM" value="" title="UOM">
                            </div>
                        </div> <br />

                        <div class="row">
                            <div class="col-sm-2 col-xs-5">
                                <label for="#qty">Qty.</label>
                            </div>
                            <div class="col-sm-4 col-xs-7">
                                <input type="text" class="form-control input-default " maxlength="7" placeholder="Masukkan quantity" name="qty" value="" title="qty">
                            </div>
                            <div class="col-sm-2 col-xs-5">
                                <label for="#subInventory">Sub-Inventory di Kapal Tujuan</label>
                            </div>
                            <div class="col-sm-4 col-xs-7">
                                <select class="form-control input-default"  name="lokasiId" id="lokasiId">                                  
                                </select>
                            </div>
                        </div> <br />

                    </form>
                </div>
            </div>

		

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" onClick="backItemSearched()" name="backItemSearched" class="btn btn-default">Back</button>
        <button type="button" name="saveItemSearched" onClick="saveItemSearched();" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
    var itemList = [];
    var tipeList = "B";
    var stockItem = 0;
    function addItemDesc(stokID) {
        $("button[name=saveItemSearched]").show();
        $("button[name=backItemSearched]").show();

        $("#formItemSearchDetail input[name=id]").val( $("#item" + stokID).attr("data-itemid") );
        $("#formItemSearchDetail input[name=namaItem]").val( $("#item" + stokID).attr("data-namaitem") );
        $("#formItemSearchDetail input[name=deskripsi]").val( $("#item" + stokID).attr("data-deskripsi"));
        $("#formItemSearchDetail input[name=partNumber]").val( $("#item" + stokID).attr("data-partnumber"));
        $("#formItemSearchDetail input[name=category]").val( $("#item" + stokID).attr("data-category") );
        $("#formItemSearchDetail input[name=UOM]").val( $("#item" + stokID).attr("data-uom") );
        $("#formItemSearchDetail input[name=lotNumber]").val( $("#item" + stokID).attr("data-lotnumber") );
        $("#formItemSearchDetail input[name=tglMasuk]").val( $("#item" + stokID).attr("data-tglmasuk") );
        $("#formItemSearchDetail input[name=fromLokasiId]").val( $("#item" + stokID).attr("data-lokasiid") );

        stockItem = parseFloat( $("#item" + stokID).attr("data-jumlah") );
        $("#formItemSearchDetail input[name=stokID]").val( $("#item" + stokID).attr("data-stokid") );
        $("#formItemSearchDetail input[name=qty]").val("");
        $("#formItemSearchDetail input[name=lokasiId]").val("");

        $.ajax({
            url: "{{url("administrator/transferio/getKapalSubInv")}}",
            type: "GET",
            data: { toKapalId : $("#toKapalId").val() },
            dataType: "json",
            success: function (result, status, jqXHR) {       
                $("#lokasiId").html("");     
                $.each(result, function(idx, data) {
                    $("#lokasiId").append('<option value="' + data.id + '">' + data.namaLokasi + '</option>');
                });
                $('#carouselItems').carousel("next");
            },
            error: function (jqXHR, status, err) {
                alert("Terjadi kesalahan, Tutup peringatan ini dan coba kembali, atau refresh halaman ini dan mulai dari awal.");
            }
        });
        
    }

    function saveItemSearched() {
        var itemID      = $("#formItemSearchDetail input[name=id]").val();
        var namaItem    = $("#formItemSearchDetail input[name=namaItem]").val();
        var partNumber  = $("#formItemSearchDetail input[name=partNumber]").val();
        var deskripsi   = $("#formItemSearchDetail input[name=deskripsi]").val();
        var category    = $("#formItemSearchDetail input[name=category]").val();
        var UOM         = $("#formItemSearchDetail input[name=UOM]").val();
        var tglMasuk    = $("#formItemSearchDetail input[name=tglMasuk]").val();
        var fromLokasiId    = $("#formItemSearchDetail input[name=fromLokasiId]").val();

        var stokID      = $("#formItemSearchDetail input[name=stokID]").val();
        var qty         = parseFloat( $("#formItemSearchDetail input[name=qty]").val() );
        var lokasiId    = parseInt ($("#formItemSearchDetail select[name=lokasiId]").val() );
        var lotNumber   = $("#formItemSearchDetail input[name=lotNumber]").val();
        if(itemList.indexOf(stokID) >= 0) {            
            alert("Data sudah ada di list!");  
            backItemSearched();
        } else {
            if(! (qty > 0) ) {
                alert("Masukkan jumlah barang!");
                
                $("#formItemSearchDetail input[name=qty]").focus();
            } else if(qty > stockItem) {
                alert("Jumlah yang dimasukkan tidak boleh melibihi stok yang ada");    

                $("#formItemSearchDetail input[name=qty]").val(stockItem);            
                $("#formItemSearchDetail input[name=qty]").focus();
            } else if(! (lokasiId > 0)) {
                alert("Belum ada subinventory yang dipilih!");
                
                $("#formItemSearchDetail select[name=lokasiId]").focus();
            } else if(lotNumber == "") {
                $("#formItemSearchDetail input[name=lotNumber]").focus();
            } else {                
                itemList.push(stokID);
                printItemIntoList(itemID, namaItem, partNumber, deskripsi, category, UOM, tglMasuk, fromLokasiId, stokID, qty, lokasiId, lotNumber);            
                $("#itemModal").modal('hide');
            }
        }
    }

    function printItemIntoList(itemID, namaItem, partNumber, deskripsi, category, UOM, tglMasuk, fromLokasiId, stokID, qty, lokasiId, lotNumber) {
        var content = "<tr id=\"listItem" + stokID + "\" ><input type=\"hidden\" name=\"itemID[" + stokID + "]\" value=\"" + itemID + "\" />";
            content += "<input type=\"hidden\" name=\"stokID[" + stokID + "]\" value=\"" + stokID + "\" />";
            content += "<input type=\"hidden\" name=\"fromLokasiId[" + stokID + "]\" value=\"" + fromLokasiId + "\" />";
            content += "<input type=\"hidden\" name=\"tglMasuk[" + stokID + "]\" value=\"" + tglMasuk + "\" />";
            content += "<td><span id=\"noList" + stokID + "\">" + (itemList.length) + ".</span></td><td>" + namaItem + "</td><td>" + partNumber + "</td><td>" + deskripsi + "</td><td>" + category + "</td><td>" + UOM + "</td>";
            content += "<td><input type=\"hidden\" name=\"quantity[" + stokID + "]\" value=\"" + qty + "\" />" + qty + "</td>";
            content += "<td><input type=\"hidden\" name=\"lokasiId[" + stokID + "]\" value=\"" + lokasiId + "\" />"
            content += "<input type=\"hidden\" name=\"lotNumber[" + stokID + "]\" value=\"" + lotNumber + "\" />" + lotNumber + "</td>";
            content += "<td><button type=\"button\" class=\"btn outline-dark\" onClick=\"deleteListedItem(" + stokID + ")\" title=\"Delete Item\" name=\"delete-item\"><span class=\"fa fa-trash\"></span></button></td></tr>";
            $("#tableItem tbody").append(content);
    }


    

    $('#itemModal').on('show.bs.modal', function (event) {
        showItemModal();
    });

</script>