<nav class="navbar navbar-expand-md navbar-dark main-navigation bg-danger">
    <a class="my-2 my-sm-0 app-name" href="{{url("administrator/")}}">
        <img src="{{ url("assets/img/pelni.png") }}" border="0" height="40" style="padding:0px 10px;" />
    </a>
    <!-- <a class="btn btn-primary my-2 my-sm-0 app-name" href="{{url("administrator/")}}">Pelni || Dashboard</a> -->
    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#menuNavbar"
            aria-controls="navbarsExample06" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse collapse" id="menuNavbar">
        <ul class="navbar-nav mr-auto">
            {!!WorkerAuth::getMenu()!!}
        </ul>
        <ul class="navbar-nav mr-1">
            <li class="nav-item dropdown active">
               <!--  <a class="nav-item nav-link dropdown-toggle mr-md-12" href="#" id="bd-versions" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <span class="badge badge-secondary">10</span> <span class="fa fa-bell"></span>
                </a> -->
                <div class="dropdown-menu dropdown-menu-right notification-list">
                    <div class="notification-list-header">
                        <h7>You Got 10 Messages
                            <small class="pull-right"><a href="#">Mark all as Read</a></small>
                        </h7>
                    </div>
                    <div class="notification-list-body">
                        <a class="dropdown-item notification" href="#">
                            <span class="fa fa-close pull-right"></span>
                            <div class="media">
                                <img class="mr-3" data-src="holder.js/64x64" alt="64x64"
                                     src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2264%22%20height%3D%2264%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2064%2064%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_161601541d7%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_161601541d7%22%3E%3Crect%20width%3D%2264%22%20height%3D%2264%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2213.5546875%22%20y%3D%2236.5%22%3E64  4%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
                                     data-holder-rendered="true" style="width: 35px; height: 35px;">
                                <div class="media-body">
                                    <h6 class="mt-0 mb-1">List-based media object</h6>
                                    <p>Cras sit amet nibh libero,</p>
                                </div>
                                <span class="mx-5"><small>1 second ago</small></span>
                            </div>
                        </a>
                        <a class="dropdown-item notification" href="#">
                            <span class="fa fa-close pull-right"></span>
                            <div class="media">
                                <img class="mr-3" data-src="holder.js/64x64" alt="64x64"
                                     src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2264%22%20height%3D%2264%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2064%2064%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_161601541d7%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_161601541d7%22%3E%3Crect%20width%3D%2264%22%20height%3D%2264%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2213.5546875%22%20y%3D%2236.5%22%3E64x64%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
                                     data-holder-rendered="true" style="width: 35px; height: 35px;">
                                <div class="media-body">
                                    <h6 class="mt-0 mb-1">List-based media object</h6>
                                    <p>Cras sit amet nibh libero,</p>
                                </div>
                                <span class="mx-5"><small>1 second ago</small></span>
                            </div>
                        </a>
                        <a class="dropdown-item notification" href="#">
                            <span class="fa fa-close pull-right"></span>
                            <div class="media">
                                <img class="mr-3" data-src="holder.js/64x64" alt="64x64"
                                     src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2264%22%20height%3D%2264%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2064%2064%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_161601541d7%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_161601541d7%22%3E%3Crect%20width%3D%2264%22%20height%3D%2264%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2213.5546875%22%20y%3D%2236.5%22%3E64x64%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
                                     data-holder-rendered="true" style="width: 35px; height: 35px;">
                                <div class="media-body">
                                    <h6 class="mt-0 mb-1">List-based media object</h6>
                                    <p>Cras sit amet nibh libero,</p>
                                </div>
                                <span class="mx-5"><small>1 second ago</small></span>
                            </div>
                        </a>
                    </div>
                    <div class="notification-list-footer text-center">
                        <a href="{{url("administrator/notifications")}}">Read All</a>
                    </div>
                </div>
            </li>
            <li class="nav-item dropdown active">
                <a class="nav-item nav-link dropdown-toggle mr-md-12" href="#" id="bd-versions1" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <span class="fa fa-user"></span> {{WorkerAuth::auth()->getAuth()->name}}
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="bd-versions1">
                    <a class="dropdown-item" href="{{url("administrator/profile")}}">
                        <div class=""><span class="fa fa-user"></span> <span class="text-left">Profile</span></div>
                    </a>
                    <a class="dropdown-item" href="{{url("administrator/logout")}}"><span class="fa fa-sign-out"></span>
                        Logout</a>
                </div>
            </li>
        </ul>
    </div>
</nav>