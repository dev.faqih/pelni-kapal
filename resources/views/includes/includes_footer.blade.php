<script type="text/javascript" src="{{URL::asset("/assets/js/vendor.bundle.js")}}"></script>
<script type="text/javascript" src="{{URL::asset("/assets/js/page.bundle.js")}}"></script>

<script>
    var $ = vendor.jquery;
    /*
    $("input[type='file']").change(function(evt) {
        var files = evt.target.files;
        var nameOfInput = evt.currentTarget.name;
        for (var i = 0, f; f = files[i]; i++) {

            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function(theFile) {
                $("div[name='" + nameOfInput + "Preview']").remove();
                return function(e) {
                    var span = document.createElement('div');
                    span.setAttribute("name", nameOfInput + "Preview");

                    span.className = "text-center";
                    span.style = "position:relative";

                    span.innerHTML = ['' +
                        '<button type="button" class="removeImg" onclick="removeImg(this)" name="closeImg" data-reff="' + nameOfInput + '"' +
                        ' style="position: absolute;color: white;right: 10px;background-color: #292961;border: none;cursor: pointer;"><span' +
                        '   aria-hidden="true">&times;</span></button>' +
                        '<img  style="max-height:250px;margin-bottom:10px" src="', e.target.result,
                        '" title="' + evt.target.value + '"/>'
                    ].join('');

                    var bera = document.getElementsByName(evt.target.name);
                    bera[0].parentNode.before(span);
                };
            })(f);
            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
    });


    function removeImg(e) {

        var props = e.getAttribute("data-reff");
        var isExist = document.getElementById(props + "Img");
        if (isExist)
            isExist.setAttribute("value", "");
        $("input[name='" + props + "']").val("");
        $("div[name='" + props + "Preview']").remove();
    }
    */
</script>

<script>
    $(document).ready(function() {
        $('.input-daterange input').each(function() {
            $(this).datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                // todayBtn: true,
                clearBtn: true
            });
        });
        //  $('.datepicker').datepicker('update', '');

        /*   $(".datepicker").datepicker({
              autoclose: true,
              "format": 'yyyy-mm-dd',
              todayBtn: true,
              clearBtn: true,
              date: ''
          }); */
    });


    // FORM APPROVAL

    var submitFormApproval = function() {
        if ($("#formApproval textarea[name=feedback]").val() == "") {
            return alert("Mohon isi kolom komentar");
            $("#formApproval textarea[name=feedback]").focus();
        }
        $("#formApproval input[name=status]").val(1);
        $("#formApproval").submit();
    }
</script>

<!-- Modal -->
<div class="modal fade" id="defModal" tabindex="-1" role="dialog" aria-labelledby="defModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="defModalTitle">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" id="defModalBody">
            </div>
            <div class="modal-footer" id="defModalFooter">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>


<footer class="footer">
    <div class="col-md-12 footer add-space bg-danger">
        <nav class="navbar navbar-expand-sm navbar-dark ">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ url("/administrator") }}">Aplikasi Kapal PT. Pelni v.1.0.61<span class="sr-only"></span></a>
                </li>

            </ul>
        </nav>
    </div>
</footer>