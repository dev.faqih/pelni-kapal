@extends('app')
@section('content')
@include('includes.include_navigation')
<div class="container content">

	<div class="card content-box">
		<div class="card-header">
			<div class="pull-left">
				<h4>Buat Adjusment BBM</h4>
			</div>
			<div class="pull-right">@include('includes.include_breadcrumb')</div>
		</div>
	</div>
	<div class="clearfix">&nbsp;</div>

	<form autocomplete="off" class="" action="{{url("administrator/adjustmentbbm")}}" role="form" method="POST" enctype="multipart/form-data">
		<div class="container border rounded bg-light">
			<div class="clearfix">&nbsp;</div>
			@include('includes.include_error_prop')

			<input type="hidden" name="_method" value="POST">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">
			<input type="hidden" name="kodeKapal" value="{{ WorkerAuth::auth()->getAuth()->kodeKapal }}">
			<input type="hidden" name="userId" value="{{ WorkerAuth::auth()->getAuth()->id }}">
			<input type="hidden" name="userJabatan" value="{{ WorkerAuth::auth()->getAuth()->jabatan }}">

			<input type="hidden" name="tipeTransaksi" value="AB">
			<input type="hidden" name="urlTransaksi" value="adjustmentbbm">
			<input type="hidden" name="namaTransaksi" value="Adjustment BBM">
			<input type="hidden" name="itemId" value="{{ $bbmItem->id }}">
			<input type="hidden" name="namaItem" value="{{ $bbmItem->namaItem }}">
			<input type="hidden" name="deskripsi" value="{{ $bbmItem->deskripsi }}">
			<input type="hidden" name="harga_satuan" value="{{ $bbmItem->item_price }}">
			<input type="hidden" name="UOM" value="{{ $bbmItem->UOM }}">
			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#operatingUnit">Unit Operasi</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" class="form-control input-default" required placeholder="Masukkan unit operasi" name="operatingUnit" value="{{ WorkerAuth::auth()->getAuth()->namaKapal }}" readonly title="operatingUnit">
				</div>

				<div class="col-sm-2 col-xs-5">
					<label for="#requesterId">Dibuat Oleh</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input disabled class="form-control input-default" required value="{{ WorkerAuth::auth()->getAuth()->name }}" />
				</div>
			</div><br />

			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#deskripsi">Pelabuhan</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<select class="form-control" name="pelabuhan" required title="Pelabuhan Pengisian">
						@foreach($portBunker as $port)
						<option value="{{ $port }}">{{ $port }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-sm-2 col-xs-5">
					<label for="#deskripsi">Deskripsi</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" class="form-control input-default" required placeholder="Masukkan deskripsi" name="keterangan" value="{{ old("keterangan") }}" title="Keterangan">
				</div>
			</div><br />
			<div class="row">

				<div class="col-sm-2 col-xs-5">
					<label for="#transactionDate">Tanggal Transaksi</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" id="transactionDate" onChange="checkIsSmallerThanPODate('transactionDate','tanggalPO');checkIsMoreThan3Days('transactionDate', '{{ date("Y-m-d H:i:s") }}','{{ WorkerAuth::auth()->getAuth()->sysParams->BACKDATE }}');loadVoyage('{{ url('administrator/popenerimaan/getVoyages') }}', 'transactionDate')" class="form-control datepicker input-default " data-date-clear-btn="true" data-date-format="yyyy-mm-dd" data-date-autoclose="true" required name="transactionDate" title="tanggal penerimaan">
				</div>

			</div><br />
			<div class="row">
				<div class="col-sm-2 col-xs-5">Voyage</div>
				<div class="col-sm-4 col-xs-7">
					<select class="form-control input-default" required name="voyageKode" title="Voyage">
					</select>
				</div>
				<div class="col-sm-2 col-xs-5">Tanggal Voyage</div>
				<div class="col-sm-3 col-xs-7 input-group">
					<input readonly required class=" form-control input-default search-value" name="voyageStart" value="">
					<div class="input-group-addon"> &nbsp; s/d &nbsp; </div>
					<input readonly required class=" form-control input-default search-value" name="voyageEnd" value="">
				</div>
			</div><br />
			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="">Stock On Hand</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" class="form-control input-default " readonly required name="sisaawal" value="{{ $sisaawal }}" title="">
				</div>
				<div class="col-sm-2 col-xs-5">
					<label for="">Penambahan Qty.</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" class="form-control input-default numericOnly" maxlength="10" required placeholder="Masukkan quantity" name="quantity" value="{{old("quantity")}}" title="Quantity">
				</div>
			</div><br />
			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="">Sub Inventory</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<select class="form-control input-default" required name="locationId" title="Sub Inventory">
						@foreach($locations as $location)
						<option value="{{ $location->id }}">{{ $location->namaLokasi }}</option>>
						@endforeach
					</select>
				</div>
				<div class="col-sm-2 col-xs-5">
					<label for="">Lot Number</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" class="form-control input-default " readonly required name="lotNumber" value="{{ WorkerAuth::auth()->getAuth()->kodeKapal . $lotNumber }}" title="">
				</div>
			</div><br />
			<p class="text-center">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Kirim</button> &nbsp;
				<a class="btn btn-primary" href="{{ URL::previous() }}" title="Back"><span class="fa fa-undo"></span> Kembali</a>
			</p>

		</div>
	</form>
</div>
@include('includes.includes_footer')
<script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>
<script>
	$("select[name='voyageKode']").change(function() {
		loadVoyageDate("{{ url('administrator/permintaanbarang/getVoyageDate') }}", $(this).val());
	});
</script>
@endsection