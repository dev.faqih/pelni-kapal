@extends('app')
@section('content')
@include('includes.include_navigation')
<div class="container content">
    <div class="card content-box">
        <div class="card-header">
            <div class="pull-left">
                <h4>Adjusment BBM</h4>
            </div>
            <div class="pull-right">@include('includes.include_breadcrumb')</div>
        </div>
    </div>
    <div class="clearfix">&nbsp;</div>



    <div class="container border rounded bg-light">
        <div class="clearfix">&nbsp;</div>
        @if(WorkerAuth::auth()->getAuth()->aplikasi == "KAPAL")
        <div class="row">
            <div class="col-sm-12 text-right">
                <a class="btn btn-danger" href="{{url("administrator/adjustmentbbm/create")}}" title="Create Data"><span class="fa fa-plus"></span> Buat Adjustment </a>
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>
        @endif
        @include('includes.include_error_prop')
        <div class="row">
            <div class="col-sm-2 col-xs-12">No. Transaksi</div>
            <div class="col-sm-3 col-xs-12">
                <input class="form-control input-default search-value" name="transactionNo" title="Search Value">
            </div>
            <div class="col-sm-2 col-xs-12">Tanggal Transaksi</div>
            <div class="col-sm-3 col-xs-12">
                <input class="form-control input-default datepicker search-value" data-date-clear-btn="true" data-date-format="yyyy-mm-dd" data-date-autoclose="true" required name="transactionDate" title="Search Value">
            </div>
        </div><br />
        @if(WorkerAuth::auth()->getAuth()->aplikasi == "KAPAL")
        <div class="row">
            <div class="col-sm-2 col-xs-12">Voyage</div>
            <div class="col-sm-3 col-xs-12">
                <select class="form-control select2 search-value  js-example-basic-single" name="voyageKode">
                    <option value=""></option>
                    @foreach($voyages as $voyage)
                    <option value="{{ $voyage->voyageKode }}">{{ $voyage->voyageName . " - " . $voyage->tahun }} </option>
                    @endforeach
                </select>
            </div>
        </div><br />
        @endif
        <div class="row">
            <div class="col-sm-2">&nbsp;</div>
            <div class="col-sm-10">
                <button type="button" class="btn btn-primary " onClick="searchList()" name="search-btn-pagination" title="Search"><span class="fa fa-search"></span> Search</button> &nbsp;
                <button type="button" class="btn btn-primary  " name="clear-search" title="Reset Search"><span class="fa fa-trash-o"></span> Reset</button>
            </div>
        </div><br />
    </div>
    <!-- </div> -->

    <div class="clearfix">&nbsp;</div>

    <div class="container border rounded bg-light">
        <div class="card-title content-box-title add-space">
            <div class="row">
                <div class="col-md-12 col-xs-12 ">
                    <div class="pull-left">
                        <div class="addon-box-list">
                            <div class="form-group-inline">
                                <span class="addon">Show</span>
                                <select class="form-control" name="size" title="Total List">
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span class="addon">Data</span>
                            </div>
                        </div>
                    </div>
                    <div class="pull-right">
                        <div class="addon-box-list add-space-left  ">
                            <div class="form-group-inline">
                                <span class="addon ">Ordering by</span>
                                <select class="form-control" title="Sorting Data" name="orderingBy">
                                    <option value="0" selected>Sorting By</option>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table id="adjustmentbbm" class="table table-hover table-striped widget-table" name="table-index">
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th class="header-pagination">No. Transaksi</th>
                        <th class="header-pagination">Penambahan Qty.</th>
                        <th class="header-pagination">Tanggal Transaksi</th>
                        <th class="header-pagination">Voyages</th>
                        <th class="header-pagination">Start Date</th>
                        <th class="header-pagination">End Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($adjustmentBBMs["rows"] as $adjustmentBBM)
                    <tr>
                        <td><a data-toggle="tooltip" data-placement="top" title="View" href="{{url("administrator/adjustmentbbm/show/" . $adjustmentBBM->id )}}" class="btn btn-outline-primary"><span class="fa fa-eye"></span></a></td>
                        <td>{{ $adjustmentBBM->transactionNo }}</td>
                        <td>{{ $adjustmentBBM->quantity }}</td>
                        <td>{{ date("Y-m-d", strtotime($adjustmentBBM->transactionDate)) }}</td>
                        <td>{{ $adjustmentBBM->voyageName }}</td>
                        <td>{{ date("Y-m-d", strtotime($adjustmentBBM->voyageStart)) }}</td>
                        <td>{{ date("Y-m-d", strtotime($adjustmentBBM->voyageEnd)) }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
        <div class=" content-box-footer">
            <div class="row">
                <div class="col-md-6 text-left col-xs-12">
                    Page {{$adjustmentBBMs["page"]}} of <span id="pageCount">{{$adjustmentBBMs["pageCount"]}}</span>
                    Total Row <span id="totalRow">{{$adjustmentBBMs["rowCount"]}}</span>
                </div>
                <div class="col-md-6  col-xs-12">
                    <ul class="pagination pull-right">
                        <li class="page-item ">
                            <button class="page-link" tabindex="-1" name="first" title="First Page">First
                            </button>
                        </li>
                        <li class="page-item" id="pagination-prev">
                            <a class="page-link" title="Prev Page">
                                <i class="fa fa-step-backward"></i></a>
                        </li>
                        <li class="page-item" id="pagination-next">
                            <a class="page-link" title="Next Page">
                                <i class="fa fa-step-forward"></i></a></li>
                        <li class="page-item">
                            <button class="page-link" name="last" title="Last Page">Last</button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>




    <!-- </div> -->
</div>

@include('includes.includes_footer')
<script>
    < ? php $adjustmentBBMs["rows"] = []; ? >
    var paginationParameter = ("{!! json_encode($adjustmentBBMs) !!}");
    var currentUrl = "{{ url($currentPrefix) }}";
</script>
<script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>
@endsection