@extends('app')
@section('content')
@include('includes.include_navigation')
<div class="container content">

	<div class="card content-box">
		<div class="card-header">
			<div class="pull-left">
				<h4>Buat Adjusment BBM</h4>
			</div>
			<div class="pull-right">@include('includes.include_breadcrumb')</div>
		</div>
	</div>
	<div class="clearfix">&nbsp;</div>


	<div class="container border rounded bg-light">
		<div class="clearfix">&nbsp;</div>
		@include('includes.include_error_prop')
		<div class="row">
			<div class="col-sm-2 col-xs-5">
				<label for="#operatingUnit">Transaction No</label>
			</div>
			<div class="col-sm-4 col-xs-7">
				<input type="text" class="form-control input-default " readonly value="{{ $bbm->transactionNo }}">
			</div>

			<div class="col-sm-2 col-xs-5">
				<label for="#requesterId">Dibuat Oleh</label>
			</div>
			<div class="col-sm-4 col-xs-7">
				<input type="text" class="form-control input-default " readonly value="{{ $bbm->userName }}">
			</div>
		</div><br />

		<div class="row">
			<div class="col-sm-2 col-xs-5">
				<label for="#transactionDate">Pelabuhan</label>
			</div>
			<div class="col-sm-4 col-xs-7">
				<input type="text" readonly class="form-control input-default " value="{{ $bbm->pelabuhan }}">
			</div>
			<div class="col-sm-2 col-xs-5">
				<label for="#deskripsi">Deskripsi</label>
			</div>
			<div class="col-sm-4 col-xs-7">
				<input type="text" class="form-control input-default " readonly value="{{ $bbm->keterangan }}">
			</div>
		</div><br />
		<div class="row">
			<div class="col-sm-2 col-xs-5">
				<label for="#transactionDate">Tanggal Transaksi</label>
			</div>
			<div class="col-sm-4 col-xs-7">
				<input type="text" readonly class="form-control input-default " value="{{ date("Y-m-d", strtotime($bbm->transactionDate)) }}">
			</div>
		</div><br />
		<div class="row">
			<div class="col-sm-2 col-xs-5">Voyage</div>
			<div class="col-sm-4 col-xs-7">
				<input type="text" class="form-control input-default " readonly value="{{ $bbm->voyageName }}">
			</div>
			<div class="col-sm-2 col-xs-5">Tanggal Voyage</div>
			<div class="col-sm-3 col-xs-7 input-group">
				<input type="text" class="form-control input-default " readonly value="{{ date("Y-m-d", strtotime($bbm->voyageStart)) }}">
				<div class="input-group-addon"> &nbsp; s/d &nbsp; </div>
				<input type="text" class="form-control input-default " readonly value="{{ date("Y-m-d", strtotime($bbm->voyageEnd)) }}">
			</div>
		</div><br />
		<div class="row">
			<div class="col-sm-2 col-xs-5">
				<label for="#approverId">Stock On Hand</label>
			</div>
			<div class="col-sm-4 col-xs-7">
				<input type="text" class="form-control input-default " readonly required name="sisaawal" value="{{ floatval($bbm->sisaawal) }}" title="">
			</div>
			<div class="col-sm-2 col-xs-5">
				<label for="#approverId">Penambahan Qty.</label>
			</div>
			<div class="col-sm-4 col-xs-7">
				<input type="text" class="form-control input-default " readonly value="{{ floatval($bbm->quantity) }}">
			</div>
		</div><br />
		<div class="row">
			<div class="col-sm-2 col-xs-5">
				<label for="#approverId">Sub Inventory</label>
			</div>
			<div class="col-sm-4 col-xs-7">
				<input type="text" class="form-control input-default " readonly value="{{ $bbm->namaLokasi }}">
			</div>
			<div class="col-sm-2 col-xs-5">
				<label for="#approverId">Lot Number</label>
			</div>
			<div class="col-sm-4 col-xs-7">
				<input type="text" class="form-control input-default " readonly value="{{ $bbm->lotNumber }}">
			</div>
		</div><br />
		<p class="text-center">
			<a class="btn btn-primary" href="{{ URL::previous() }}" title="Back"><span class="fa fa-undo"></span> Kembali</a>
		</p>

	</div>
</div>
@include('includes.includes_footer')
<script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>
<script>
	$("select[name='voyageKode']").change(function() {
		loadVoyageDate("{{ url('administrator/permintaanbarang/getVoyageDate') }}", $(this).val());
	});
</script>
@endsection