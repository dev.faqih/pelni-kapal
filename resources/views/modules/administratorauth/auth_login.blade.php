@extends('app')
@section('content')
    <style>
        .login-page {
            background-image: url(" {{url("/").'/assets/'.'img/bg_login.png'}}");
            background-position: center;
            height: 100%;
            position: absolute;
            background-repeat: no-repeat;
            background-size: cover;
        }

        .login-page {
            margin-top: 0px;
            padding-bottom: 50px;
            min-height: 420px;
        }

        .login-content {
            margin-top: 150px;
        }
    </style>
    <script>
        var isChrome = !!window.chrome ;
        if(isChrome) {
            console.log("isChrome: " + navigator.userAgent.indexOf("Chrome")); // window.location.href = "{{ url("page-not-found")}}";
        } else if(navigator.userAgent.indexOf("Chrome") != -1) {
            console.log("isChrome2: " + navigator.userAgent.indexOf("Chrome")); // window.location.href = "{{ url("page-not-found")}}";
        } else {
            console.log("! isChrome: " + navigator.userAgent.indexOf("Chrome")); // window.location.href = "{{ url("page-not-found")}}";
        }

        // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
        isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
        // Firefox 1.0+
        isFirefox = typeof InstallTrigger !== 'undefined';
        // Safari 3.0+
        isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);
        // Internet Explorer 6-11
        isIE = /*@cc_on!@*/false || !!document.documentMode;
        // Edge 20+
        isEdge = !isIE && !!window.StyleMedia;
        // Chrome 1+
        isChrome = !!window.chrome && !!window.chrome.webstore;
        // Blink engine detection
        isBlink = (isChrome || isOpera) && !!window.CSS;
        /* Results: */
        console.log("isOpera", isOpera);
        console.log("isFirefox", isFirefox);
        console.log("isSafari", isSafari);
        console.log("isIE", isIE);
        console.log("isEdge", isEdge);
        console.log("isChrome", isChrome);
        console.log("isBlink", isBlink);

        if(isBlink || isEdge || isIE || isSafari || isFirefox || isOpera) {
            window.location.href = "{{ url("browser-redirect")}}";
        }
    </script>
    <div class="container-fluid login-page">
        <div class="row justify-content-md-center login-content">
            <div class="col-md-4">
                <div class="card content-box">
                    <div class="card-body">
                        <div class="login-header">
              <span class="logo text-center">
                  <h3>Supply Chain Management</h3>
                  <h5>PT.PELNI (PERSERO)</h5>
              </span>
                        </div>

                        <div class="form-login">
                            @include('includes.include_error_prop')
                            <form autocomplete="off" action="{{url("administrator/login")}}" role="form" method="POST"
                                  enctype="multipart/form-data">
                                <input type="hidden" name="_method" value="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <label>Username</label>
                                    <input type="text" name="username" class="form-control"
                                           placeholder="Enter Username">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control"
                                           placeholder="Password" name="password">
                                </div>
                                <button type="submit" class="btn btn-primary col-md-12">Sign In</button>
                                <!-- <div class="text-center">
                                    <a href="{{url("administrator/recovery")}}">Forgot Password</a>
                                </div> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{URL::asset("/assets/js/vendor.bundle.js")}}"></script>
    <script type="text/javascript" src="{{URL::asset("/assets/js/page.bundle.js")}}"></script>
@endsection