@extends('app')
@section('content')
<style>
body, html {
    height: 100%;
}

.bg { 
    /* The image used */
    /* background-image: url("/images/banners/banner5.jpg");
     */

    /* Full height */
    height: 100%; 

    /* Center and scale the image nicely */
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}
</style>
<div class="bg">

<nav class="navbar bg-danger ">
    <div class="container">
        <div class="center-logo text-light">
            <h2> PELNI: App Login Page</h2>
        </div>
    </div>
</nav>
<div class="container login-page">
    <div class="row no-margin">
        <div class="col-md-5 order-md-2">
            <div class="card panel-default login-form rounded">
                <div class="card-header text-center login-header"><h5>Login Dashboard</h5></div>
                <div class="card-body">
                  @include('includes.include_error_prop')
                    <form action="{{url("administrator/login")}}" role="form" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_method" value="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="form-username">Username</label>
                                <input type="text" class="form-control input-default"
                                       placeholder="Masukkan Username" id="form-username" name="username">
                            </div>
                            <div class="form-group">
                                <label for="form-password">Password</label>
                                <input type="password" class="form-control input-default"
                                       placeholder="Password" id="form-password" name="password">
                            </div>
                            <div class="checkbox">
                                <label class="pull-left">
                                    <input type="checkbox"> Remember Me
                                </label>
                                <label class="pull-right">
                                    <a href="{{url("administrator/recovery")}}">Forgot Password</a>
                                </label>
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary col-md-12">Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-7 order-md-1">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                    <img class="d-block w-100" src="/images/banners/banner1.jpg" alt="First slide">
                    </div>
                    <div class="carousel-item">
                    <img class="d-block w-100" src="/images/banners/banner2.jpg" alt="Second slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>

    </div>
</div>
<script type="text/javascript" src="{{URL::asset("/assets/js/vendor.bundle.js")}}"></script>
<script type="text/javascript" src="{{URL::asset("/assets/js/page.bundle.js")}}"></script>
<footer class="footer">
    <div class="col-md-12 footer add-space bg-danger">
        <nav class="navbar navbar-expand-sm navbar-dark ">
            <ul class="navbar-nav">
                <li class="nav-item active">
                <a class="nav-link" href="{{ url("/administrator") }}">Aplikasi Kapal PT. Pelni <span class="sr-only"></span></a>
                </li>

            </ul>
        </nav>
    </div>
</footer>

</div>

@endsection