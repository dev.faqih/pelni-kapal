@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        @include('includes.include_error_prop')
        <div class="row">
            <div class="col-md-3 col-xs-12 col-sm-12">
                <div class="card">
                    <div class="card-body border-top">
                        <p class="card-text">{{ $users->name }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-9 col-xs-12">
                <div class="card content-box">                        
                    <div class="card-header">
                        <div class="pull-left"><h4>{{ucfirst($users->name)}} Profile's</h4></div>
                        <div class="pull-right">@include('includes.include_breadcrumb')</div>
                    </div>
                    <div class="card-body content-box-body ">
                        <!-- <form action="{{url("administrator/profile")}}" role="form" method="POST" enctype="multipart/form-data"> -->
                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <label>Fullname</label>
                                <input type="text" disabled class="form-control input-default" name="fullname"
                                       placeholder="Masukkan fullname" value="{{old("fullname")?: $users->name}}">
                            </div>
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" disabled class="form-control input-default" name="username" placeholder="Masukkan fullname" value="{{old("username")?: $users->username}}">
                            </div>

                            <div class="form-group">
                                <label>Kapal</label>
                                <input type="text" disabled class="form-control input-default" value="{{ $users->namaKapal }}">
                            </div>

                            <div class="form-group">
                                <label>Jabatan</label>
                                <input type="text" disabled class="form-control input-default" value="{{ $users->jabatan }}">
                            </div>

                            <div class="form-group">
                                <label>NRP</label>
                                <input type="text" disabled name="nrp" class="form-control input-default" value="{{ $users->nrp }}">
                            </div>

                            <div class="row">					
                                <div class="col-sm-5 col-xs-5">
                                    <pre>Sub Inventory Permission:<br /> <?php foreach( WorkerAuth::auth()->getAuth()->roleLokasiItem as $subinv) { echo $subinv->namaLokasi . " <br /> "; } ?>
                                    </pre>
                                </div>
                                <div class="col-sm-5 col-xs-5">
                                    <pre>Item Category Permission:<br /> <?php foreach( WorkerAuth::auth()->getAuth()->roleCategory as $category) { echo $category->category . " <br /> "; } ?>                            
                                    </pre>
                                </div>
                            </div><br />

                        <!-- </form> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes.includes_footer')
@endsection