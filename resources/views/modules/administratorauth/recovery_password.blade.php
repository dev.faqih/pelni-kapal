@extends('app')
@section('content')
    <div class="container login-page recovery-password">
        <div class="row justify-content-md-center">
            <div class="col-md-4">
                <div class="card panel-default login-form">
                    <div class="card-header text-center login-header"><h5>Recovery Password</h5></div>
                    <div class="card-body  ">
                        @include('includes.include_error_prop')
                        <form action="{{url("administrator/recovery")}}" role="form" method="POST"
                              enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input type="email" class="form-control input-default" id="exampleInputEmail1"
                                           placeholder="Masukkan email" name="email">
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary col-md-12"><i class="fa fa-send"></i> Send
                                    Recovery Password
                                </button>
                                <a class="btn btn-primary col-md-12 add-space" href="{{url("administrator/login")}}"><span
                                            class="fa fa-undo"></span> Back</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection