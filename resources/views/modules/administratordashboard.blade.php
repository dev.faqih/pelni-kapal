@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="clearfix">&nbsp;</div>
        <!-- @include('includes.include_breadcrumb') -->
        @include('includes.include_error_prop')
        <div class="text-center">
        <br />&nbsp;<br />
        <!-- <h1 style="color:#FF0000">PT. Pelayaran Nasional Indonesia</h1> -->
        <br />
            <img src="{{ url('assets/img/dashboard.png') }}" border="0" class="rounded" width="800" />
        </div>

<!--         <div class="row">
            <div class="col-sm-3 widget">
                <div class="card widget-counter bg-aqua">
                    <div class="card-body">
                        <div class="inner">
                            <h1 class="widget-counter-title">100</h1>
                            <p class="">Total Order <br/></p>
                        </div>
                        <div class="widget-counter-icon">
                            <span class="fa fa-bell"></span>
                        </div>
                        <div class="widget-counter-footer">
                            <span class="fa fa-refresh"></span> Last Update
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-sm-3 widget">
                <div class="card widget-counter bg-yellow">
                    <div class="card-body">
                        <div class="inner">
                            <h1 class="widget-counter-title">100</h1>
                            <p class="">Total Order <br/></p>
                        </div>
                        <div class="widget-counter-icon">
                            <span class="fa fa-bell"></span>
                        </div>
                        <div class="widget-counter-footer">
                            <span class="fa fa-refresh"></span> Last Update
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-sm-3 widget">
                <div class="card widget-counter bg-red">
                    <div class="card-body">
                        <div class="inner">
                            <h1 class="widget-counter-title">100</h1>
                            <p class="">Total Order <br/></p>
                        </div>
                        <div class="widget-counter-icon">
                            <span class="fa fa-bell"></span>
                        </div>
                        <div class="widget-counter-footer">
                            <span class="fa fa-refresh"></span> Last Update
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-sm-3 widget">
                <div class="card widget-counter bg-green">
                    <div class="card-body">
                        <div class="inner">
                            <h1 class="widget-counter-title">100</h1>
                            <p class="">Total Order <br/></p>
                        </div>
                        <div class="widget-counter-icon">
                            <span class="fa fa-bell"></span>
                        </div>
                        <div class="widget-counter-footer">
                            <span class="fa fa-refresh"></span> Last Update
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="add-space">
            <div class="row">
                <div class="col-sm-8">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Special title treatment</h5>
                            <div class="chart">
                                <canvas id="barChart" style="height: 180px;"></canvas>
                            </div>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                    <div class=" add-space">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="card">
                                    <div class="card-body"><h6 class="card-title">Current Stock</h6></div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card">
                                    <div class="card-body"><h6 class="card-title">Current Stock</h6></div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card">
                                    <div class="card-body"><h6 class="card-title">Current Stock</h6></div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card">
                                    <div class="card-body"><h6 class="card-title">Current Stock</h6></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Special title treatment</h5>
                            <div class="chart">
                                <canvas id="pieChart" style="height: 180px;"></canvas>
                            </div>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                    <div class="card add-space">
                        <div class="card-body">
                            <h5 class="card-title">Special title treatment</h5>
                            <ul class="list-unstyled">
                                <li class=" widget-list">
                                    <div class="media">
                                        <img class="mr-2" data-src="holder.js/64x64" alt="64x64"
                                             src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2264%22%20height%3D%2264%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2064%2064%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_1616417320b%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_1616417320b%22%3E%3Crect%20width%3D%2264%22%20height%3D%2264%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2213.5546875%22%20y%3D%2236.5%22%3E64x64%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
                                             data-holder-rendered="true" style="width: 64px; height: 64px;">
                                        <div class="media-body">
                                            <h6 class="mt-0 mb-1 widget-list-title">List-based media object
                                                <small class="pull-right"><span
                                                            class="fa fa-clock-o"></span> 1 second ago
                                                </small>
                                            </h6>
                                            <p class="widget-list-body"> Cras sit amet nibh libero, in gravida nulla.
                                                Nulla
                                                vel metus scelerisque ante
                                                sollicitudin</p>
                                        </div>
                                    </div>
                                </li>
                                <li class=" widget-list">
                                    <div class="media">
                                        <img class="mr-2" data-src="holder.js/64x64" alt="64x64"
                                             src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2264%22%20height%3D%2264%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2064%2064%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_1616417320b%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_1616417320b%22%3E%3Crect%20width%3D%2264%22%20height%3D%2264%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2213.5546875%22%20y%3D%2236.5%22%3E64x64%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
                                             data-holder-rendered="true" style="width: 64px; height: 64px;">
                                        <div class="media-body">
                                            <h6 class="mt-0 mb-1 widget-list-title">List-based media object
                                                <small class="pull-right"><span
                                                            class="fa fa-clock-o"></span> 1 second ago
                                                </small>
                                            </h6>
                                            <p class="widget-list-body"> Cras sit amet nibh libero, in gravida nulla.
                                                Nulla
                                                vel metus scelerisque ante
                                                sollicitudin</p>
                                        </div>
                                    </div>
                                </li>
                                <li class=" widget-list">
                                    <div class="media">
                                        <img class="mr-2" data-src="holder.js/64x64" alt="64x64"
                                             src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2264%22%20height%3D%2264%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2064%2064%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_1616417320b%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_1616417320b%22%3E%3Crect%20width%3D%2264%22%20height%3D%2264%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2213.5546875%22%20y%3D%2236.5%22%3E64x64%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
                                             data-holder-rendered="true" style="width: 64px; height: 64px;">
                                        <div class="media-body">
                                            <h6 class="mt-0 mb-1 widget-list-title">List-based media object
                                                <small class="pull-right"><span
                                                            class="fa fa-clock-o"></span> 1 second ago
                                                </small>
                                            </h6>
                                            <p class="widget-list-body"> Cras sit amet nibh libero, in gravida nulla.
                                                Nulla
                                                vel metus scelerisque ante
                                                sollicitudin</p>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>

            </div> -->
        </div>
    </div>
    @include('includes.includes_footer')
    <script type="text/javascript" src="{{URL::asset("/assets/js/graphic.bundle.js")}}"></script>
    <script type="text/javascript" src="{{URL::asset("/assets/js/dummy.js")}}"></script>
@endsection