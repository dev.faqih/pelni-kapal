@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        @include('includes.include_breadcrumb')
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Create a New Data of Test</h4></div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{url("administrator/administratormenu")}}" title="Back"><span class="fa fa-undo"></span> Back</a>
                </div>
            </div>
            <div class="card-body content-box-body">
            @include('includes.include_error_prop')
                <form action="{{url("administrator/administratormenu")}}" role="form" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="form-group">
					<label for="#menuname">Menuname</label>
					<input type="text"  class="form-control input-default "  placeholder="Masukkan menuname" name="menuname" value="" title="menuname">
					</div>

					<div class="form-group">
					<label for="#url">Url</label>
					<input type="text"  class="form-control input-default "  placeholder="Masukkan url" name="url" value="" title="url">
					</div>

					<div class="form-group">
					<label for="#parentmenu">Parentmenu</label>
					<input type="text"  class="form-control input-default "  placeholder="Masukkan parentmenu" name="parentmenu" value="" title="parentmenu">
					</div>

					<div class="form-group">
					<label for="#icon">Icon</label>
					<input type="text"  class="form-control input-default "  placeholder="Masukkan icon" name="icon" value="" title="icon">
					</div>

					<div class="form-group">
					<label for="#description">Description</label>
					<input type="text"  class="form-control input-default "  placeholder="Masukkan description" name="description" value="" title="description">
					</div>

					<div class="form-group">
					<label for="#position">Position</label>
					<input type="text"  class="form-control input-default "  placeholder="Masukkan position" name="position" value="" title="position">
					</div>

                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Submit</button>
                </form>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
@endsection