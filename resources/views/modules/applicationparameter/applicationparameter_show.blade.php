@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Detail Application Parameter</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
            <div class="card-body content-box-body">
                @include('includes.include_error_prop')
                <div class="form-group">
                    <label for="#variableName">Variable Name</label>
                    <input type="text" disabled class="form-control input-default "  placeholder="Masukkan variableName" name="variableName" value="{{$applicationparameter->variableName}}" title="variableName">
                </div>

                <div class="form-group">
                    <label for="#variableValue">Variable Value</label>
                    <input type="text" disabled class="form-control input-default "  placeholder="Masukkan variableValue" name="variableValue" value="{{$applicationparameter->variableValue}}" title="variableValue">
                </div>
                <div class="clearfix">&nbsp;</div>
                <p class="text-center">
                    <a class="btn btn-primary" href="{{url("administrator/applicationparameter")}}" title="Back"><span class="fa fa-undo"></span> Back</a>
                    <a class="btn btn-primary" href="{{url("administrator/applicationparameter/$applicationparameter->id/edit")}}" title="Edit data"><span class="fa fa-edit"></span> Edit</a>
                
                </p>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
@endsection