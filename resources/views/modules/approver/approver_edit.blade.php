@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        @include('includes.include_breadcrumb')
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Edit data of{{$title}}</h4></div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{url("administrator/approver")}}" title="Back"><span class="fa fa-undo"></span> Back</a>
                </div>
            </div>
            <div class="card-body content-box-body">
            @include('includes.include_error_prop')

                <form role="form" action="{{url("administrator/approver/$approver->id/update")}}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="form-group">
					<label for="#roleId">RoleId</label>
					<select class="form-control input-default"  name="roleId" title="roleId">
						@foreach($roless as $roles)
							<option value="{{$roles->id}}"@if($approver->roleId== $roles->id) selected @endif>
								{{$roles->id}}</option>
						@endforeach
					</select>
					</div>

					<div class="form-group">
					<label for="#formIdApprover">FormIdApprover</label>
					<input type="text"  class="form-control input-default "  placeholder="Masukkan formIdApprover" name="formIdApprover" value="{{$approver->formIdApprover}}" title="formIdApprover">
					</div>

                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                </form>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
@endsection