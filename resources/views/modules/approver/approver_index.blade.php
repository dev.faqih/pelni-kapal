@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        @include('includes.include_breadcrumb')
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Listing Data of {{$title}}</h4></div>
                <div class="pull-right">
                    <a class="btn btn-danger" href="{{url("administrator/approver/create")}}" title="Create Data"><span class="fa fa-plus"></span> New </a>
                    <!-- <button class="btn btn-primary" title="Delete Data" name="delete-content"><span class="fa fa-trash"></span> Trash</button> -->
                    <!-- <button class="btn btn-info" title="Export Data"><span class="fa fa-file-excel-o"></span> Export</button> -->
                </div>
            </div>
            <div class="card-body content-box-body">
            @include('includes.include_error_prop')
                <div class="row">
                    <div class="col-md-3 col-xs-12">
                      <select class="form-control input-default" name="q" title="Search Something">
                        <option value="">Search By</option>
                            @foreach($criteria as $key => $value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                      </select>
                    </div>
                    <div class="col-md-3 col-xs-12">
                         <input class="form-control input-default" name="search-value" title="Search Value">
                    </div>
                    <div class="col-md-6">
                        <button class="btn btn-primary " name="search-btn-pagination" title="Search">
                            <span class="fa fa-search"></span> Search</button>
                        <button class="btn btn-primary  " name="clear-search" title="Reset Search">
                            <span class="fa fa-trash-o"></span> Reset
                        </button>
                    </div>
                </div>
                <div class="card-title content-box-title add-space">
                    <div class="row">
                        <div class="col-md-12 col-xs-12 ">
                            <div class="pull-left">
                                <div class="addon-box-list">
                                    <div class="form-group-inline">
                                        <span class="addon">Show</span>
                                         <select class="form-control" name="size" title="Total List">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select>
                                        <span class="addon">Data</span>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right">
                                <div class="addon-box-list add-space-left  ">
                                    <div class="form-group-inline">
                                        <span class="addon ">Ordering by</span>
                                       <select class="form-control" title="Sorting Data" name="orderingBy">
                                           <option value="0" selected>Sorting By</option>
                                           @foreach($ordering as $key => $value)
                                               <option value="{{$key}}">{{$value}}</option>
                                           @endforeach
                                       </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
					<table id="approver" class="table table-hover table-striped widget-table"name="table-index">
						<thead>
						<tr>
							<th><input type="checkbox" name="selectAll"></th>
							<th>&nbsp;</th>
								<th class="header-pagination">id</th>
								<th class="header-pagination">roleId</th>
								<th class="header-pagination">formIdApprover</th>
						</tr>
						</thead>
						<tbody>
							@foreach($approvers["rows"]as $approver)
						<tr>
							<td><input type="checkbox" value="{{$approver->id}}" name="checkbox-del[]" title="id"></td>
							<td><a href="{{url("administrator/approver/$approver->id")}}"class="btn btn-outline-primary"><span class="fa fa-eye"></span></a></td>
								<td >{{$approver->id}}</td>
								<td>{{$approver->roleId}}</td>
								<td>{{$approver->formIdApprover}}</td>
						</tr>
							@endforeach
						</tbody>
					</table>

                </div>
                <div class=" content-box-footer">
                    <div class="row">
                        <div class="col-md-6 text-left col-xs-12">
                        Page {{$approvers["page"]}} of <span id="pageCount">{{$approvers["pageCount"]}}</span>
                        Total Row <span id="totalRow">{{$approvers["rowCount"]}}</span>
                        </div>
                        <div class="col-md-6  col-xs-12">
                           <ul class="pagination pull-right">
                               <li class="page-item ">
                                   <button class="page-link" tabindex="-1" name="first" title="First Page">First
                                   </button>
                               </li>
                               <li class="page-item" id="pagination-prev">
                                    <a class="page-link" title="Prev Page">
                                        <i class="fa fa-step-backward"></i></a>
                               </li>
                               <li class="page-item" id="pagination-next">
                                    <a class="page-link" title="Next Page">
                                        <i class="fa fa-step-forward"></i></a></li>
                               <li class="page-item">
                                   <button class="page-link" name="last" title="Last Page">Last</button>
                               </li>
                           </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes.includes_footer')
     <script>
            var paginationParameter = ('{!! json_encode($approvers)!!}');
            var currentUrl = "{{url($currentPrefix)}}";
        </script>
    <script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>
@endsection