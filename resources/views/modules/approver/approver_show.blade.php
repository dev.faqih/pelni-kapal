@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        @include('includes.include_breadcrumb')
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Edit data of{{$title}}</h4></div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{url("administrator/approver")}}" title="Back"><span class="fa fa-undo"></span> Back</a>
                    <a class="btn btn-info" href="{{url("administrator/approver/$approver->id/edit")}}" title="Edit data"><span class="fa fa-edit"></span> Edit</a>
                </div>
            </div>
            <div class="card-body content-box-body">
            @include('includes.include_error_prop')
					<div class="form-group">
					<label for="#roleId">RoleId</label>
					<select class="form-control input-default" disabled name="roleId" title="roleId">
						@foreach($roless as $roles)
							<option value="{{$roles->id}}"@if($approver->roleId== $roles->id) selected @endif>
								{{$roles->id}}</option>
						@endforeach
					</select>
					</div>

					<div class="form-group">
					<label for="#formIdApprover">FormIdApprover</label>
					<input type="text" disabled class="form-control input-default "  placeholder="Masukkan formIdApprover" name="formIdApprover" value="{{$approver->formIdApprover}}" title="formIdApprover">
					</div>


                </form>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
@endsection