<style>
    td {
        font-size: 8px;
    }

    .txctr {
        text-align: center;
    }

    .txlft {
        text-align: left;
    }

    .txtr8 {
        text-align: right;
    }

    .txt7px {
        font-size: 7px;
    }

    .txbold {
        font-weight: bold;
    }

    .text12 {
        font-size: 11px;
        font-weight: normal;
    }

    .bdrtop {
        border-top: 1px solid #000000;
    }

    .bdrbot {
        border-bottom: 1px solid #000000;
    }

    .bdrlef {
        border-left: 1px solid #000000;
    }

    .bdrr8 {
        border-right: 1px solid #000000;
    }

    .pad5 {
        padding: 5px 5px 5px 5px;
    }
</style>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="100%" class="txctr">
            <table width="100%" style="" class="" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="txctr" style="" width="16%"><br />&nbsp;<br />
                        <span><img src="{{ url("assets/img/pelni.png") }}" border="0" height="40" style="padding:0px 10px;"></span>
                    </td>
                    <td width="72%" class=" txctr"><br />&nbsp;<br />
                        <span style="font-size:13px;">PT. PELAYARAN NASIONAL INDONESIA (Persero)</span><br />
                        <span style="font-size:13px;">LAPORAN PENERIMAAN &amp; PEMAKAIAN MINYAK BBM ( PER - {{ $textType }} )</span><br />
                    </td>
                    <td width="14%" class="txlft"><br /> &nbsp;<br />&nbsp;<br />{{ $noLaporan }}</td>
                </tr>
                <tr>
                    <td class="txlft" colspan="3"><span style="font-size:10px;">NAMA KAPAL: {{ $namaKapal }}</span><br /></td>
                </tr>
            </table>


        </td>
    </tr>
</table>

<table width="100%" cellpadding="3" class="bdrtop bdrlef" border="1" cellspacing="0">
    <thead>
        <tr>
            <th class="txctr text12" rowspan="3" width="30">No</th>

            <th class="txctr text12" colspan="3" width="180">Periode</th>

            <th class="txctr text12" rowspan="3" width="70">Jarak Tempuh</th>
            <th class="txctr text12" colspan="7" width="490">Pencatatan BBM</th>
        </tr>
        <tr>
            <th class="txctr text12" rowspan="2" width="60">{{ $textType }}</th>
            <th class="txctr text12" rowspan="2" width="60">Awal</th>
            <th class="txctr text12" rowspan="2" width="60">Akhir</th>

            <th class="txctr text12" rowspan="2" width="70">Saldo Awal <small>(liter)</small></th>

            <th class="txctr text12" colspan="3" width="210">Pengisian</th>

            <th class="txctr text12" rowspan="2" width="70">Koreksi / Dibukukan <small>(liter)</small></th>
            <th class="txctr text12" rowspan="2" width="70">Total Pemakaian <small>(liter)</small></th>
            <th class="txctr text12" rowspan="2" width="70">Saldo Akhir <small>(liter)</small></th>
        </tr>
        <tr>
            <th class="txctr text12" width="70">Pelabuhan</th>
            <th class="txctr text12" width="70">Tanggal</th>
            <th class="txctr text12" width="70">Jumlah <small>(liter)</small></th>
        </tr>
        <tr>
            @for($i=1;$i<=12;$i++) <th class="txctr text12" width="@if($i <=1 ) {{ "30" }} @elseif($i <= 4) {{ "60" }} @else {{ "70" }} @endif"><small><i>{{ $i }}</i></small></th>

                @endfor
        </tr>
    </thead>

    <tbody>
        <?php $previousBulan = "xxx";
        $i = 1;
        $totalPemakaian = 0; ?>
        @foreach ($laporans as $laporan)
        <?php
        if ($laporan->tipe == "VOYAGE") {
            if ($laporan->voyage_id > 0) {
                $bulan = "Voyage " . $laporan->voyage_num;
            } else {
                $bulan = $laporan->voyage_name;
            }
        } else {
            $bulan = $laporan->bulan_text;
        }
        if ($previousBulan != $bulan) {
            $totalPemakaian += $laporan->pemakaian;
        } ?>
        <tr nobr="true">
            <td class="txctr" width="30">@if($previousBulan != $bulan) {{ $i++ }} @endif</td>
            <td class="txctr" width="60">@if($previousBulan != $bulan) {{ $bulan }} @endif &nbsp;</td>
            <td class="txctr" width="60">@if($previousBulan != $bulan) {{ date("d M Y", strtotime($laporan->voyage_from)) }} @else {{ "&nbsp;" }} @endif</td>
            <td class="txctr" width="60">@if($previousBulan != $bulan) {{ date("d M Y", strtotime($laporan->voyage_to)) }} @else {{ "&nbsp;" }} @endif</td>

            <td class="txctr" width="70">@if($previousBulan != $bulan) {{ $laporan->jarak_tempuh }} @else {{ "&nbsp;" }} @endif</td>
            <td class="txctr" width="70">@if($previousBulan != $bulan) {{ floatval($laporan->saldo_awal) }} @else {{ "&nbsp;" }} @endif</td>

            <td class="txctr" width="70">{{ $laporan->pelabuhan }}</td>
            <td class="txctr" width="70">@if(! empty($laporan->transactionDate)) {{ date("d M Y", strtotime($laporan->transactionDate)) }} @endif</td>
            <td class="txctr" width="70">{{ floatval($laporan->qty) }}</td>

            <td class="txctr" width="70">@if($previousBulan != $bulan) {{ floatval($laporan->koreksi) }} @else {{ "&nbsp;" }} @endif</td>
            <td class="txctr" width="70">@if($previousBulan != $bulan) {{ floatval($laporan->pemakaian) }} @else {{ "&nbsp;" }} @endif</td>
            <td class="txctr" width="70">@if($previousBulan != $bulan) {{ floatval($laporan->saldo_akhir) }} @else {{ "&nbsp;" }} @endif</td>
        </tr>
        <?php $previousBulan = $bulan; ?>
        @endforeach

        <tr nobr="true">
            <td class="txctr" width="30">&nbsp;</td>
            <td class="txctr" width="60">&nbsp;</td>
            <td class="txctr" width="60">&nbsp;</td>
            <td class="txctr" width="60">&nbsp;</td>
            <td class="txctr" width="70">&nbsp;</td>
            <td class="txctr" width="70">&nbsp;</td>
            <td class="txctr" width="70">&nbsp;</td>

            <td class="txctr" colspan="3" width="210">Total Pemakaian</td>
            <td class="txctr" width="70">{{ floatval($totalPemakaian) }}</td>
            <td class="txctr" width="70">&nbsp;</td>
        </tr>
    </tbody>

</table>

<br />&nbsp;<br />&nbsp;
<table width="100%" cellpadding="0" class="" border="0" cellspacing="0">
    <tr nobr="true">
        <td>
            <table width="100%" cellpadding="0" class="" border="0" cellspacing="0">
                <tr>
                    <td colspan="2">Keterangan</td>
                </tr>
                <tr>
                    <td width="200">1. Jumlah Jam berlayar dalam {{ strtolower($textType) }} ini</td>
                    <td width="100">: {{ $jamBerlayar }} Jam</td>
                </tr>
                <tr>
                    <td width="200">2. Jumlah Jam Berlabuh / Sandar dalam {{ strtolower($textType) }} ini</td>
                    <td width="100">: {{ $jamBerlabuh }} Jam</td>
                </tr>
                <tr>
                    <td colspan="2">3. Pengisian Jumlah BBM, Jam Kerja dan Jarak disesuaikan dengan Log Book</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br />&nbsp;<br />&nbsp;

<table style="" width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr nobr="true">
        <td>
            <table style="" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="120">&nbsp;</td>
                    <td width="150" class="txctr">MENGETAHUI: <br />NAKHODA</td>
                    <td width="250">&nbsp;</td>
                    <td width="150" class="txctr">{!! $namaKapal . ", " . date("d F Y") . "<br />DIBUAT OLEH: <br /> KEPALA KAMAR MESIN" !!} </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td class=""><br />&nbsp;<br /><br />&nbsp;<br /><br />&nbsp;<br /></td>
                    <td></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="txctr">{{ $approver->name }}</td>
                    <td></td>
                    <td class="txctr">{{ $requester->name }}</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="txctr">{{ $approver->nrp }}</td>
                    <td></td>
                    <td class="txctr">{{ $requester->nrp }}</td>
                </tr>
            </table>
        </td>
    </tr>
</table>