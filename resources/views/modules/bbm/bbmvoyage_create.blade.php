@extends('app')
@section('content')
@include('includes.include_navigation')
<div class="container content">

    <div class="card content-box">
        <div class="card-header">
            <div class="pull-left">
                <h4>Report BBM per Voyage</h4>
            </div>
            <div class="pull-right">@include('includes.include_breadcrumb')</div>
        </div>
    </div>
    <div class="clearfix">&nbsp;</div>

    <form autocomplete="off" action="{{url("administrator/bbmvoyage/show/" . $laporan->id)}}" role="form" method="POST" enctype="multipart/form-data">
        <div class="container border rounded bg-light">
            <div class="clearfix">&nbsp;</div>
            @include('includes.include_error_prop')

            <input type="hidden" name="_method" value="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">
            <input type="hidden" name="kodeKapal" value="{{ WorkerAuth::auth()->getAuth()->kodeKapal }}">
            <input type="hidden" name="userId" value="{{ WorkerAuth::auth()->getAuth()->id }}">
            <input type="hidden" name="laporanID" value="{{ $laporan->id }}">


            <div class="row">
                <div class="col-sm-2 col-xs-5">Voyage</div>
                <div class="col-sm-3 col-xs-7">
                    <input readonly required class=" form-control input-default search-value" name="voyageStart" value="{{$laporan->voyage_name}}">
                </div>
                <div class="col-sm-2  offset-sm-1 col-xs-5">Tanggal Voyage</div>
                <div class="col-sm-3 col-xs-7 input-group">
                    <input readonly required class=" form-control input-default search-value" name="voyageStart" value="{{date('Y-m-d', strtotime($laporan->voyage_from))}}">
                    <div class="input-group-addon"> &nbsp; s/d &nbsp; </div>
                    <input readonly required class=" form-control input-default search-value" name="voyageEnd" value="{{date('Y-m-d', strtotime($laporan->voyage_to))}}">
                </div>
            </div><br />
            <div class="row">
                <div class="col-sm-2 col-xs-5">
                    <label for="#deskripsi">Jarak Tempuh</label>
                </div>
                <div class="col-sm-3 col-xs-7">
                    <input type="text" class="form-control input-default numericOnly" maxlength="15" required placeholder="Masukkan jarak tempuh" name="jarak_tempuh" value="{{ $laporan->jarak_tempuh }}" title="jarak_tempuh">
                </div>
                <div class="col-sm-2  offset-sm-1 col-xs-5">
                    <label for="#requesterId">Saldo Awal</label>
                </div>
                <div class="col-sm-3 col-xs-7"><input readonly class="form-control input-default" value="{{ floatval($laporan->saldo_awal) }}" /></div>
            </div><br />
            <div class="clearfix">&nbsp;</div>
        </div>
        <div class="clearfix">&nbsp;</div>
        <div class="container border rounded bg-light">

            <div class="clearfix">&nbsp;</div>
            <div class="row justify-content-between">
                <div class="col-sm-4 col-xs-6">
                    <h5>Daftar Penerimaan BBM per Voyage</h5>
                </div>
            </div><br />
            <div class="table-responsive">
                <table id="tableItem" class="table table-hover table-striped widget-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Pelabuhan</th>
                            <th>Tanggal Penerimaan</th>
                            <th>No Transaksi</th>
                            <th>Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        @foreach($laporan->details as $item)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td><select class="form-control" name="pelabuhan[{{ $item->id }}]" title="Pelabuhan Pengisian">
                                    @foreach($portBunker as $port)
                                    <option value="{{ $port }}" @if($port==$item->pelabuhan) {{ "selected" }} @endif >{{ $port }}</option>
                                    @endforeach
                                </select></td>
                            <td>@if(! empty($item->transactionDate)) {{ date("Y-m-d", strtotime($item->transactionDate)) }} @endif</td>
                            <td>@if(isset($item->transactionNo)) {{ $item->transactionNo }} @endif</td>
                            <td>@if(isset($item->qty) && $item->qty > 0) {{ floatval($item->qty) }} @endif</td>
                        </tr>
                        @endforeach
                        <?php if (!empty($laporan->details)) { } else {
                            echo "<tr>";
                            echo "<td colspan='5'><center>Data is not found</center></td>";
                            echo "</tr>";
                        } ?>
                    </tbody>
                </table>
            </div><br />

            <div class="clearfix">&nbsp;</div>
        </div>
        <div class="clearfix">&nbsp;</div>
        <div class="container border rounded bg-light">
            <div class="clearfix">&nbsp;</div>


            <div class="clearfix">&nbsp;</div>
            <div class="row">
                <div class="col-sm-2 col-xs-5">
                    <label for="#approverId">Koreksi / Dibukukan</label>
                </div>
                <div class="col-sm-3 col-xs-7">
                    <input type="text" class="form-control input-default " readonly placeholder="Masukkan keperluan" name="koreksi" value="{{ floatval($laporan->koreksi) }}" title="koreksi">
                </div>
                <div class="col-sm-2  offset-sm-1 col-xs-5">
                    <label for="#approverId">Pemakaian</label>
                </div>
                <div class="col-sm-3 col-xs-7">
                    <input type="text" class="form-control input-default " readonly placeholder="" name="pemakaian" value="{{ floatval($laporan->pemakaian)}}" title="pemakaian">
                </div>
            </div><br />
            <div class="row">
                <div class="col-sm-2 col-xs-5">
                    <label for="#approverId">Saldo Akhir</label>
                </div>
                <div class="col-sm-3 col-xs-7">
                    <input type="text" class="form-control input-default " readonly placeholder="" name="saldo_akhir" value="{{ floatval($laporan->saldo_akhir) }}" title="saldo_akhir">
                </div>
                <div class="col-sm-2  offset-sm-1 col-xs-5">
                    <label for="#approverId">Keterangan</label>
                </div>
                <div class="col-sm-3 col-xs-7">
                    <input type="text" class="form-control input-default " required placeholder="Masukkan Keterangan" name="keterangan" value="{{$laporan->keterangan}}" title="Keterangan">
                </div>
            </div><br />
            <div class="row">
                <div class="col-sm-6 col-xs-6">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                </div>
                <div class="col-sm-5 col-xs-6 text-right">
                    <button type="button" onclick="kunciLaporan()" class="btn btn-danger"><i class="fa fa-key"></i> Kunci Laporan</button> &nbsp;
                    <button type="button" onclick="repopulateData()" class="btn btn-primary"><i class="fa fa-refresh"></i> Re-populate Data</button> &nbsp;
                    <a class="btn btn-primary" href="{{ url("administrator/bbmvoyage/") }}" title="Back"><span class="fa fa-undo"></span> Kembali</a>
                </div>
            </div><br />

        </div>
</div>
</form>
</div>
@include('includes.includes_footer')
<script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>
<script>
    function repopulateData(modul) {
        overlayOn();
        $.ajax({
            url: "{{ url('administrator/bbmvoyage/repopulate')}}",
            type: "GET",
            data: {
                laporanID: "{{ $laporan->id }}"
            },
            dataType: "json",
            success: function(data, status, jqXHR) {
                if (data.status == 1) {
                    overlayOff();
                    alert(data.message);
                    location.href = "{{ url('administrator/bbmvoyage/show/' . $laporan->id) }}";
                } else {
                    alert(data.message);
                }
                overlayOff();
            },
            error: function(jqXHR, status, err) {
                overlayOff();
                alert("iTerjadi kesalahan, Tutup peringatan ini dan coba kembali, atau refresh halaman ini dan mulai dari awal.");
            },
            complete: function(jqXHR, status) {
                overlayOff();
            },
            timeout: 6000000
        });
    }

    function kunciLaporan(modul) {
        overlayOn();
        $.ajax({
            url: "{{ url('administrator/bbmvoyage/kuncilaporan')}}",
            type: "GET",
            data: {
                laporanID: "{{ $laporan->id }}"
            },
            dataType: "json",
            success: function(data, status, jqXHR) {
                if (data.status == 1) {
                    overlayOff();
                    alert(data.message);
                    location.href = "{{ url('administrator/bbmvoyage/show/' . $laporan->id) }}";
                } else {
                    alert(data.message);
                }
                overlayOff();
            },
            error: function(jqXHR, status, err) {
                overlayOff();
                alert("Terjadi kesalahan, Tutup peringatan ini dan coba kembali, atau refresh halaman ini dan mulai dari awal.");
            },
            complete: function(jqXHR, status) {
                overlayOff();
            },
            timeout: 6000000
        });
    }
</script>
@endsection
