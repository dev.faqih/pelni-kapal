@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Buat Barang Baru</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
            <div class="card-body content-box-body">
            <div class="row">
					<div class="col-sm-12 text-right">
                    <a class="btn btn-primary" href="{{url("administrator/item")}}" title="Back"><span class="fa fa-undo"></span> Back</a>
					</div>
				</div>
				<div class="clearfix">&nbsp;</div>
            @include('includes.include_error_prop')
                <form action="{{url("administrator/item")}}" role="form" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="form-group">
                        <label for="#namaItem">Nama Item</label>
                        <input type="text"  class="form-control input-default "  placeholder="Masukkan nama item" name="namaItem" value="{{old("namaItem")}}" title="namaItem">
					</div>

					<div class="form-group">
                        <label for="#deskripsi">Deskripsi</label>
                        <input type="text"  class="form-control input-default "  placeholder="Masukkan deskripsi" name="deskripsi" value="{{old("deskripsi")}}" title="deskripsi">
					</div>

                    <div class="form-group">
                        <label for="#category">Category</label>
                        <input type="text"  class="form-control input-default "  placeholder="Masukkan category" name="category" value="{{old("category")}}" title="category">
					</div>

                    <div class="form-group">
                        <label for="#uom">UOM</label>
                        <input type="text"  class="form-control input-default "  placeholder="Masukkan UOM" name="UOM" value="{{old("UOM")}}" title="UOM">
					</div>

                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Submit</button>
                </form>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
@endsection