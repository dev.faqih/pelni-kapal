@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Daftar Barang</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
            <div class="card-body content-box-body">
            <!-- <div class="row">
                <div class="col-sm-12 text-right">
                <a class="btn btn-danger" href="{{url("administrator/item/create")}}" title="Create Data"><span class="fa fa-plus"></span> New </a>
                </div>
            </div>
            <div class="clearfix">&nbsp;</div> -->
            @include('includes.include_error_prop')
                <div class="row">
                    <div class="col-sm-1 col-xs-12">Keyword</div>
                    <div class="col-sm-3 col-xs-12">
                        <input class="form-control input-default search-value" name="keyword">
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <button type="button" class="btn btn-primary" onClick="searchList()" name="search-btn-pagination" title="Search"><span class="fa fa-search"></span> Search</button> &nbsp;
                        <button type="button" class="btn btn-primary" name="clear-search" title="Reset Search"><span class="fa fa-trash-o"></span> Reset</button>  &nbsp; 
                        <button type="button" class="btn btn-primary" name="search-btn-pagination" title="Advance Search" data-toggle="collapse" data-target="#advanceSearch" aria-expanded="false" aria-controls="advanceSearch"><span id="detailSearch" class="fa fa-angle-double-down"></span> Detail Search</button>
                    </div>
                </div><br />
                <div class="collapse multi-collapse" id="advanceSearch">
                    <div class="row">
                        <div class="col-sm-1 col-xs-12">Kode Item</div>
                        <div class="col-sm-2 col-xs-12">
                            <input class="form-control input-default search-value" name="namaItem">
                        </div>
                        <div class="col-sm-1 col-xs-12">Deskripsi</div>
                        <div class="col-sm-2 col-xs-12">
                            <input class="form-control input-default search-value" name="deskripsi">
                        </div>
                        <div class="col-sm-1 col-xs-12">Part No</div>
                        <div class="col-sm-2 col-xs-12">
                            <input class="form-control input-default search-value" name="part_number">
                        </div>
                    </div>
                </div><br />
                <div class="card-title content-box-title add-space">
                    <div class="row">
                        <div class="col-md-12 col-xs-12 ">
                            <div class="pull-left">
                                <div class="addon-box-list">
                                    <div class="form-group-inline">
                                        <span class="addon">Show</span>
                                         <select class="form-control rounded" name="size" title="Total List">
                                            <option value="10">10</option>
                                            <option value="35">35</option>
                                            <option value="70">70</option>
                                            <option value="120">120</option>
                                        </select>
                                        <span class="addon">Data</span>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right">
                                <div class="addon-box-list add-space-left  ">
                                    <div class="form-group-inline">
                                        <button name="buttonGrid" class="btn btn-outline-secondary addon" @if($list == "grid") disabled @endif>
                                            &nbsp;<i class=" fa fa-th"></i>&nbsp;
                                        </button> &nbsp;
                                        <button name="buttonList" class="btn btn-outline-secondary addon" @if($list == "list") disabled @endif>
                                            &nbsp;<i class=" fa fa-th-list"></i>&nbsp;
                                        </button>
                                        <span class="addon ">Ordering by</span>
                                        <select class="form-control rounded" title="Sorting Data" name="orderingBy">
                                           <option value="0" selected>Sorting By</option>
                                           @foreach($ordering as $key => $value)
                                               <option value="{{$key}}">{{$value}}</option>
                                           @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                

                    
                @if($list == "list") 
                    @foreach($items["rows"]as $item)

                        <?php $urlItem = url("administrator/item/" . $item->id);
                        if(WorkerAuth::auth()->getAuth()->aplikasi == "HO") { 
                            $urlItem .= "," . $item->kapalId;
                        } ?>
                        <div class="card flex-row flex-wrap">
                            <div class="card-header bg-light border-0">
                                <?php if($item->image == null) { $item->image = url("assets/img/nopicture.jpg"); } ?>
                                <a data-toggle="tooltip" data-placement="top" title="Click to view" href="{{ $urlItem }}" >
                                    <img class="card-img-top rounded" src="{{ url($item->image) }}" alt="{{ $item->namaItem }}">
                                </a>
                            </div>
                            <div class="card-block px-2">
                                <div class="clearfix">&nbsp;</div>
                                <h4 class="card-title">
                                    <a data-toggle="tooltip" data-placement="top" title="Click to view" href="{{ $urlItem }}">
                                        <span onClick="addItemDesc('{{ $item->id }}')" >{{ $item->namaItem }}</span>
                                    </a>
                                </h4>
                                <h5 class="card-title">
                                    <a data-toggle="tooltip" data-placement="top" title="Click to view" href="{{ $urlItem }}">
                                        <span onClick="addItemDesc('{{ $item->id }}')" >{{ $item->deskripsi }}<br />
                                        @if($item->part_number && !empty($item->part_number)) Part No: {{ $item->part_number }}<br /> @endif
                                        Kapal: {{ $item->namaKapal }}<br />
                                        Aktif: {{ $item->enabled_flex }}</span>
                                    </a>
                                </h5>
                            </div>
                            <div class="w-100"></div>
                        </div>
                        <div class="clearfix">&nbsp;</div>
                        
                    @endforeach
                @else
                        <div class="card-deck">
                        <?php $i = 0; ?>
                            @foreach($items["rows"] as $item)

                                <?php $urlItem = url("administrator/item/" . $item->id);
                                if(WorkerAuth::auth()->getAuth()->aplikasi == "HO") { 
                                    $urlItem .= "," . $item->kapalId;
                                } ?>
                                <div id="item{{ $item->id }}" class="card cardof5 bg-light">
                                    <?php if($item->image == null) { $item->image = url("assets/img/nopicture.jpg"); } ?>
                                    <a data-toggle="tooltip" data-placement="top" title="Click to view" href="{{ $urlItem }}" >
                                        <img class="card-img-top" src="{{ url($item->image) }}" alt="{{ $item->namaItem }}">
                                    </a>
                                    <div class="card-body" style="padding:0px;"></div><div class="card-footer">
                                        <div class=" font-weight-bold">
                                            <a data-toggle="tooltip" data-placement="top" title="Click to view" class="text-primary" href="{{ $urlItem }}">{{ $item->namaItem }}</a></div>
                                        <p class="card-text"><a data-toggle="tooltip" data-placement="top" title="Click to view" class="text-dark" href="{{ $urlItem }}">{{ $item->deskripsi }}</a><br />
                                        @if($item->part_number && !empty($item->part_number)) Part No. {{ $item->part_number }}<br /> @endif
                                        Kapal: {{ $item->namaKapal }}<br />
                                        Aktif: {{ $item->enabled_flex }}</p>
                                    </div>
                                </div>
                                <?php $i++; 
                                if($i % 5 == 0) {
                                    echo "</div><br /><div class=\"card-deck\">";
                                }
                                ?>
                            @endforeach
                        </div>
                        <div class="clearfix">&nbsp;</div>

                @endif

                   
                <div class=" content-box-footer">
                    <div class="row">
                        <div class="col-md-6 text-left col-xs-12">
                        Page {{$items["page"]}} of <span id="pageCount">{{$items["pageCount"]}}</span>
                        Total Row <span id="totalRow">{{$items["rowCount"]}}</span>
                        </div>
                        <div class="col-md-6  col-xs-12">
                           <ul class="pagination pull-right">
                               <li class="page-item ">
                                   <button class="page-link" tabindex="-1" name="first" title="First Page">First
                                   </button>
                               </li>
                               <li class="page-item" id="pagination-prev">
                                    <a class="page-link" title="Prev Page">
                                        <i class="fa fa-step-backward"></i></a>
                               </li>
                               <li class="page-item" id="pagination-next">
                                    <a class="page-link" title="Next Page">
                                        <i class="fa fa-step-forward"></i></a></li>
                               <li class="page-item">
                                   <button class="page-link" name="last" title="Last Page">Last</button>
                               </li>
                           </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes.includes_footer')
     <script>
             <?php $items["rows"] = []; ?>
            var paginationParameter = ('{!! json_encode($items)!!}');
            var currentUrl = "{{url($currentPrefix)}}";
        </script>
    <script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>
@endsection