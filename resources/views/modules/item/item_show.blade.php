@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Detail Barang</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
            <div class="card-body content-box-body">
                @include('includes.include_error_prop')
                <?php $urlItem = url("administrator/item/" . $item->id);
                if(WorkerAuth::auth()->getAuth()->aplikasi == "HO") { 
                    $urlItem .= "," . $item->kapalId;
                }
                if($item->image == null) { $item->image = url("assets/img/nopicture.jpg"); } ?>
                <div class="row">
                    <div class="col-sm-4"> 
                        <div class="form-group text-center">
                            <a href="{{ $urlItem }}" >
                                <img class="card-img-top" src="{{ url($item->image) }}" alt="{{ $item->namaItem }}">
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-8"> 

                        <div class="form-group">
                            <label for="#namaItem">Kode Item</label>
                            <input type="text" disabled class="form-control input-default "  name="namaItem" value="{{$item->namaItem}}" title="Nama Item">
                        </div>

                        <div class="form-group">
                            <label for="#deskripsi">Deskripsi</label>
                            <input type="text" disabled class="form-control input-default "  name="deskripsi" value="{{$item->deskripsi}}" title="Deskripsi">
                        </div>
                        <div class="form-group">
                            <label for="#deskripsi">Part Number</label>
                            <input type="text" disabled class="form-control input-default "  name="part_number" value="{{$item->part_number}}" title="Part Number">
                        </div>

                        <div class="form-group">
                            <label for="#category">Category</label>
                            <input type="text" disabled class="form-control input-default "   name="category" value="{{$item->category}}" title="category">
                        </div>

                        <div class="form-group">
                            <label for="#uom">UOM</label>
                            <input type="text" disabled class="form-control input-default "   name="UOM" value="{{$item->UOM}}" title="UOM">
                        </div>
                        <div class="form-group">
                            <label for="#uom">Kapal</label>
                            <input type="text" disabled class="form-control input-default "   name="UOM" value="{{$item->namaKapal}}" title="Kapal">
                        </div>
                        <div class="form-group">
                            <label for="#uom">Aktif</label>
                            <select disabled class="form-control input-default ">
                                <option @if($item->enabled_flex == "Y") selected @endif>Y</option>
                                <option @if($item->enabled_flex != "Y") selected @endif>N</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="#uom">Stockable</label>
                            <select disabled class="form-control input-default ">
                                <option @if($item->stockable == "Y") selected @endif>Y</option>
                                <option @if($item->stockable != "Y") selected @endif>N</option>
                            </select>
                        </div>
                        <p class="text-center">
                            <a class="btn btn-primary" href="{{ URL::previous() }}" title="Back"><span class="fa fa-undo"></span> Kembali</a>         
                        </p>

                    </div>
                </div>
                
                
                

            </div>

        </div>
    </div>
    @include('includes.includes_footer')
@endsection