@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        @include('includes.include_breadcrumb')
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Create a New Data of Test</h4></div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{url("administrator/itempenerimaanbarang")}}" title="Back"><span class="fa fa-undo"></span> Back</a>
                </div>
            </div>
            <div class="card-body content-box-body">
            @include('includes.include_error_prop')
                <form action="{{url("administrator/itempenerimaanbarang")}}" role="form" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="form-group">
					<label for="#itemId">Item</label>
					<select class="form-control input-default"  name="itemId" title="itemId">
						@foreach($items as $item)
							<option value="{{$item->id}}">
								{{$item->id}}</option>
						@endforeach
					</select>
					</div>

					<div class="form-group">
					<label for="#penerimaanBarangId">PenerimaanBarangId</label>
					<select class="form-control input-default"  name="penerimaanBarangId" title="penerimaanBarangId">
						@foreach($penerimaanbarangs as $penerimaanbarang)
							<option value="{{$penerimaanbarang->poNumber}}">
								{{$penerimaanbarang->poNumber}}</option>
						@endforeach
					</select>
					</div>

					<div class="form-group">
					<label for="#quantity">Quantity</label>
					<input type="text"  class="form-control input-default "  placeholder="Masukkan quantity" name="quantity" value="{{old("quantity")}}" title="quantity">
					</div>

					<div class="form-group">
					<label for="#satuan">Satuan</label>
					<input type="text"  class="form-control input-default "  placeholder="Masukkan satuan" name="satuan" value="{{old("satuan")}}" title="satuan">
					</div>

                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Submit</button>
                </form>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
@endsection