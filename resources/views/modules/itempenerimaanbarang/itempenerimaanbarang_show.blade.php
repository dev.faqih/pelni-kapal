@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        @include('includes.include_breadcrumb')
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Edit data of{{$title}}</h4></div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{url("administrator/itempenerimaanbarang")}}" title="Back"><span class="fa fa-undo"></span> Back</a>
                    <a class="btn btn-info" href="{{url("administrator/itempenerimaanbarang/$itempenerimaanbarang->id/edit")}}" title="Edit data"><span class="fa fa-edit"></span> Edit</a>
                </div>
            </div>
            <div class="card-body content-box-body">
            @include('includes.include_error_prop')
					<div class="form-group">
					<label for="#itemId">Item</label>
					<select class="form-control input-default" disabled name="itemId" title="itemId">
						@foreach($items as $item)
							<option value="{{$item->id}}"@if($itempenerimaanbarang->itemId== $item->id) selected @endif>
								{{$item->id}}</option>
						@endforeach
					</select>
					</div>

					<div class="form-group">
					<label for="#penerimaanBarangId">PenerimaanBarangId</label>
					<select class="form-control input-default" disabled name="penerimaanBarangId" title="penerimaanBarangId">
						@foreach($penerimaanbarangs as $penerimaanbarang)
							<option value="{{$penerimaanbarang->poNumber}}"@if($itempenerimaanbarang->penerimaanBarangId== $penerimaanbarang->poNumber) selected @endif>
								{{$penerimaanbarang->poNumber}}</option>
						@endforeach
					</select>
					</div>

					<div class="form-group">
					<label for="#quantity">Quantity</label>
					<input type="text" disabled class="form-control input-default "  placeholder="Masukkan quantity" name="quantity" value="{{$itempenerimaanbarang->quantity}}" title="quantity">
					</div>

					<div class="form-group">
					<label for="#satuan">Satuan</label>
					<input type="text" disabled class="form-control input-default "  placeholder="Masukkan satuan" name="satuan" value="{{$itempenerimaanbarang->satuan}}" title="satuan">
					</div>


                </form>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
@endsection