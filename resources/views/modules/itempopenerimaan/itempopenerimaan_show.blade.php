@extends('app')
@section('content')
@include('includes.include_navigation')
<div class="container content">
	<div class="card content-box">
		<div class="card-header">
			<div class="pull-left">
				<h4>PO Detail &amp; Daftar Penerimaan Barang</h4>
			</div>
			<div class="pull-right">@include('includes.include_breadcrumb')</div>
		</div>
	</div>
	<div class="clearfix">&nbsp;</div>
	<div class="container border rounded bg-light">
		<div class="clearfix">&nbsp;</div>
		@include('includes.include_error_prop')
		<div class="row">
			<div class="col-sm-2 col-xs-5">
				<label for="#operatingUnit">No PO</label>
			</div>
			<div class="col-sm-4 col-xs-7">
				<input type="text" disabled class="form-control input-default" placeholder="PO Number" name="poNumber" value="{{ $popenerimaan->poNumber }}" title="poNumber">
			</div>
			<div class="col-sm-2 col-xs-5">
				<label for="#operatingUnit">No PR</label>
			</div>
			@if(intval($popenerimaan->prNumberID) > 0)
			<div class="input-group col-sm-4 col-xs-7">
				<input type="text" disabled class="form-control input-default" placeholder="No PR" name="prNumber" value="{{ $popenerimaan->prNumber }}" title="prNumber">
				<div class="input-group-append">
					<a href="{{ url("administrator/permintaanbarang/$popenerimaan->prNumberID") }}" class="text-light btn btn-primary" target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i> Lihat PR</a>
				</div>
			</div>
			@else
			<div class="col-sm-4 col-xs-7">
				<input type="text" readonly class="form-control input-default" placeholder="No PR" name="prNumber" value="{{ $popenerimaan->prNumber }}" title="prNumber">
			</div>
			@endif
		</div><br />

		<div class="row">
			<div class="col-sm-2 col-xs-5">
				<label for="#operatingUnit">Unit Operasi</label>
			</div>
			<div class="col-sm-4 col-xs-7">
				<input type="text" disabled class="form-control input-default" placeholder="Unit Operasi" name="operatingUnit" value="{{ $popenerimaan->operatingUnit }}" title="operatingUnit">
			</div>

			<div class="col-sm-2 col-xs-5">
				<label for="#requesterId">Jumlah Quantity</label>
			</div>
			<div class="col-sm-4 col-xs-7">
				<input type="text" disabled class="form-control input-default" placeholder="Jumlah Quantity" name="quantity" value="{{ number_format($popenerimaan->quantity) }}" title="quantity">
			</div>
		</div><br />


		<div class="row">
			<div class="col-sm-2 col-xs-5">
				<label for="#date">Tanggal PO</label>
			</div>
			<div class="col-sm-4 col-xs-7">
				<input type="text" disabled class="form-control input-default" placeholder="Tanggal PO" name="date" value="<?php if ($popenerimaan->date) {
																																echo date("Y-m-d", strtotime($popenerimaan->date));
																															} ?>" title="date">
			</div>

			<div class="col-sm-2 col-xs-5">
				<label for="#status">Status PO</label>
			</div>
			<div class="col-sm-4 col-xs-7">
				<input type="text" disabled class="form-control input-default" placeholder="Status PO" name="status" value="{{ ($popenerimaan->status > 0) ? 'Close' : 'Open' }}" title="status">
			</div>
		</div><br />

		<div class="row">
			<div class="col-sm-2 col-xs-5">
				<label for="#kapal">Kapal</label>
			</div>
			<div class="col-sm-4 col-xs-7">
				<input type="text" disabled class="form-control input-default" placeholder="Kapal" name="kapal" value="{{ ($popenerimaan->namaKapal) }}" title="kapal">
			</div>
		</div><br />

	</div>
	<div class="clearfix">&nbsp;</div>
	<div class="container border rounded bg-light">

		<div class="clearfix">&nbsp;</div>
		@if(WorkerAuth::auth()->getAuth()->aplikasi == "KAPAL")
		@if($popenerimaan->status > 0)
		@else
		<div class="row justify-content-between">
			<div class="col-sm-4 col-xs-6">&nbsp;</div>
			<div class="col-sm-3 text-right col-xs-5">
				<a class="btn btn-primary" href="{{ url("administrator/popenerimaan/createpenerimaan/$popenerimaan->id") }}" title="Buat Penerimaan"> Buat Penerimaan</a>
			</div>
		</div><br />
		@endif
		@endif
		<div class="table-responsive">
			<table id="tableItem" class="table table-hover table-striped widget-table">
				<thead>
					<tr>
						<th>No</th>
						<th>Kode Barang</th>
						<th>Deskripsi</th>
						<th>Qty PO</th>
						<th>Qty Penerimaan</th>
						<th>Qty Sisa (PO|Kalkulasi)</th>
						<th>Lot Number</th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 1; ?>
					@foreach($items as $item)
					<tr>
						<td>{{ $i++ }}</td>
						<td>{{ $item->namaItem }}</td>
						<td>{{ $item->deskripsi }}</td>
						<td>{{ floatval($item->quantity) }}</td>
						<td>{{ floatval($item->qtyPenerimaan) }}</td>
						<td>{{ floatval($item->qtyRemaining) . " = " . floatval($item->quantity - $item->qtyPenerimaan) }}</td>
						<td>{{ $item->lotNumber }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>

	</div>
	<div class="clearfix">&nbsp;</div>
	<div class="container border rounded bg-light">
		<div class="clearfix">&nbsp;</div>
		<div class="row">
			<div class="col-sm-2 col-xs-5">
				<h5>Catatan</h5>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 col-xs-12">
				<textarea disabled class="form-control input-default" placeholder="-" name="komentar" title="komentar">{{ $popenerimaan->comments }}</textarea>
			</div>
		</div>
		<div class="clearfix">&nbsp;</div>
		<p class="text-center">
			<a class="btn btn-primary" href="{{url("administrator/popenerimaan/")}}" title="Back"><span class="fa fa-undo"></span> Kembali</a>
		</p>
	</div>
</div>
@include('includes.includes_footer')
@endsection