@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Detail Kapal</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
            <div class="card-body content-box-body">
                @include('includes.include_error_prop')
                <div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#poNumber">Kode Kapal</label>
					</div>
					<div class="col-sm-4 col-xs-7">
                    <input type="text" disabled class="form-control input-default "  placeholder="Masukkan kode kapal" name="namaKapal" value="{{$kapal->kodeKapal}}" title="kodeKapal">
					</div>
					<div class="col-sm-2 col-xs-5">
						<label for="#receiptNumber">Nama Kapal</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default "  placeholder="Masukkan nama kapal" name="namaKapal" value="{{$kapal->namaKapal}}" title="namaKapal">
					</div>
				</div><br />
                <p class="text-center">
                    <a class="btn btn-primary" href="{{url("administrator/kapal")}}" title="Back"><span class="fa fa-undo"></span> Kembali</a>         
                </p>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
@endsection