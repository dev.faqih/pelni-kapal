@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        @include('includes.include_breadcrumb')
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Create a New Data of Test</h4></div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{url("administrator/loginlog")}}" title="Back"><span class="fa fa-undo"></span> Back</a>
                </div>
            </div>
            <div class="card-body content-box-body">
            @include('includes.include_error_prop')
                <form action="{{url("administrator/loginlog")}}" role="form" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="form-group">
					<label for="#userId">UserId</label>
					<select class="form-control input-default"  name="userId" title="userId">
						@foreach($userss as $users)
							<option value="{{$users->id}}">
								{{ $users->name }}</option>
						@endforeach
					</select>
					</div>

					<div class="form-group">
					<label for="#loginStartDate">LoginStartDate</label>
					<input type="text"  class="form-control input-default "  placeholder="Masukkan loginStartDate" name="loginStartDate" value="{{old("loginStartDate")}}" title="loginStartDate">
					</div>

					<div class="form-group">
					<label for="#loginEndDate">LoginEndDate</label>
					<input type="text"  class="form-control input-default "  placeholder="Masukkan loginEndDate" name="loginEndDate" value="{{old("loginEndDate")}}" title="loginEndDate">
					</div>

                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Submit</button>
                </form>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
@endsection