@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        @include('includes.include_breadcrumb')
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Edit data of{{$title}}</h4></div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{url("administrator/loginlog")}}" title="Back"><span class="fa fa-undo"></span> Back</a>
                </div>
            </div>
            <div class="card-body content-box-body">
            @include('includes.include_error_prop')

                <form role="form" action="{{url("administrator/loginlog/$loginlog->id/update")}}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="form-group">
					<label for="#userId">UserId</label>
					<select class="form-control input-default"  name="userId" title="userId">
						@foreach($userss as $users)
							<option value="{{$users->id}}"@if($loginlog->userId== $users->id) selected @endif>
								{{ $users->name }}</option>
						@endforeach
					</select>
					</div>

					<div class="form-group">
					<label for="#loginStartDate">LoginStartDate</label>
					<input type="text"  class="form-control input-default "  placeholder="Masukkan loginStartDate" name="loginStartDate" value="{{$loginlog->loginStartDate}}" title="loginStartDate">
					</div>

					<div class="form-group">
					<label for="#loginEndDate">LoginEndDate</label>
					<input type="text"  class="form-control input-default "  placeholder="Masukkan loginEndDate" name="loginEndDate" value="{{$loginlog->loginEndDate}}" title="loginEndDate">
					</div>

                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                </form>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
@endsection