@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Log Sync {{ $title }} </h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
        </div>
		<div class="clearfix">&nbsp;</div>
		<div class="container border rounded bg-light">
            <div class="clearfix">&nbsp;</div>
            @include('includes.include_error_prop')            
            <div class="row">
                <div class="col-sm-1 col-xs-3">Transaksi</div>
                <div class="col-sm-2 col-xs-3"><input class="form-control input-default search-value" name="process_name" title=""></div>
                <div class="col-sm-1 col-xs-3">Tanggal</div>
                <div class="col-sm-2 col-xs-3"><input class="form-control input-default datepicker search-value" data-date-clear-btn="true"  data-date-format="yyyy-mm-dd" data-date-autoclose="true" name="process_date" title=""></div>
                <div class="col-sm-1 col-xs-3">Status</div>
                <div class="col-sm-2 col-xs-3"><input class="form-control input-default search-value" name="status" title=""></div>
                <div class="col-sm-1 col-xs-3">Keterangan</div>
                <div class="col-sm-2 col-xs-3"><input class="form-control input-default search-value" name="error_msg" title=""></div>
            </div><br />
            <div class="row">
                <div class="col-sm-2">&nbsp;</div>
                <div class="col-sm-10">
                    <button type="button" class="btn btn-primary " onClick="searchList()" name="search-btn-pagination" title="Search"><span class="fa fa-search"></span> Search</button> &nbsp;
                    <button type="button" class="btn btn-primary  " name="clear-search" title="Reset Search"><span class="fa fa-trash-o"></span> Reset</button>
                </div>
            </div><br />
        </div>
		<div class="clearfix">&nbsp;</div>
		<div class="container border rounded bg-light">
            <div class="card-title content-box-title add-space">
                <div class="row">
                    <div class="col-md-12 col-xs-12 ">
                        <div class="pull-left">
                            <div class="addon-box-list">
                                <div class="form-group-inline">
                                    <span class="addon">Show</span>
                                        <select class="form-control" name="size" title="Total List">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span class="addon">Data</span>
                                </div>
                            </div>
                        </div>
                        <div class="pull-right">
                            <div class="addon-box-list add-space-left  ">
                                <div class="form-group-inline">
                                    <span class="addon ">Ordering by</span>
                                    <select class="form-control" title="Sorting Data" name="orderingBy">
                                        <option value="0" selected>Sorting By</option>
                                            @foreach($ordering as $key => $value)
                                            <option value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table id="log" class="table table-hover table-striped widget-table"name="table-index">
                    <thead>
                    <tr>
                        <th class="header-pagination">Transaksi</th>
                        <th class="header-pagination">Tanggal</th>    
                        <th class="header-pagination">Status</th>   
                        <th class="header-pagination">Keterangan</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($logs["rows"] as $log)
                        <tr>	                          						
                            <td>{{ $log->process_name }}</td>
                            <td>{{ $log->process_date }}</td>
                            <td>{{ $log->status }}</td>
                            <td>{{ $log->error_msg }}</td>                           
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
            <div class=" content-box-footer">
                <div class="row">
                    <div class="col-md-6 text-left col-xs-12">
                    Page {{ $logs["page"] }} of <span id="pageCount">{{ $logs["pageCount"] }}</span>
                    Total Row <span id="totalRow">{{ $logs["rowCount"] }}</span>
                    </div>
                    <div class="col-md-6  col-xs-12">
                        <ul class="pagination pull-right">
                            <li class="page-item ">
                                <button class="page-link" tabindex="-1" name="first" title="First Page">First
                                </button>
                            </li>
                            <li class="page-item" id="pagination-prev">
                                <a class="page-link" title="Prev Page">
                                    <i class="fa fa-step-backward"></i></a>
                            </li>
                            <li class="page-item" id="pagination-next">
                                <a class="page-link" title="Next Page">
                                    <i class="fa fa-step-forward"></i></a></li>
                            <li class="page-item">
                                <button class="page-link" name="last" title="Last Page">Last</button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes.includes_footer')
     <script>
             <?php $logs["rows"] = []; ?>
            var paginationParameter = ('{!! json_encode($logs)!!}');
            var currentUrl = "{{url($currentPrefix) }}";
        </script>
    <script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js") }}"></script>
@endsection