@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Buat Sub-Inventory Baru</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
            <div class="card-body content-box-body">
            @include('includes.include_error_prop')
                <form action="{{url("administrator/lokasiitem")}}" role="form" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    @if(WorkerAuth::auth()->getAuth()->aplikasi == "KAPAL")
                    <input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">
                    @else
                    <div class="row">
						<div class="col-sm-2 col-xs-5">Kapal</div>
						<div class="col-sm-4 col-xs-7">
							<select class="form-control input-default" name="kapalId" id="kapalId">
								@foreach($kapals as $kapal)
									<option value="{{$kapal->id}}">{{$kapal->namaKapal}}</option>
								@endforeach
							</select>
						</div>
					</div>
                    <div class="clearfix">&nbsp;</div>
                    @endif

                    <div class="row">
						<div class="col-sm-2 col-xs-5">Sub-Inventory</div>
						<div class="col-sm-4 col-xs-7">							
                        <input type="text"  class="form-control input-default "  placeholder="Masukkan nama lokasi" name="namaLokasi" value="{{old("namaLokasi")}}" title="nama lokasi">
						</div>
					</div>
                    <div class="clearfix">&nbsp;</div>

                    <div class="row">
						<div class="col-sm-2 col-xs-5">Lokasi</div>
						<div class="col-sm-4 col-xs-7">							
                        <input type="text"  class="form-control input-default "  placeholder="Masukkan lokasi" name="lokasi" value="{{old("lokasi")}}" title="lokasi">
						</div>
					</div>
                    <div class="clearfix">&nbsp;</div>

                    <div class="row">
						<div class="col-sm-2 col-xs-5">Alamat Lokasi</div>
						<div class="col-sm-4 col-xs-7">							
                            <input type="text"  class="form-control input-default "  placeholder="Masukkan alamat lokasi" name="alamatLokasi" value="{{old("alamatLokasi")}}" title="alamat lokasi">
						</div>
					</div>
                    <div class="clearfix">&nbsp;</div>

                    <div class="row">
						<div class="col-sm-2 col-xs-5">ORG Code</div>
						<div class="col-sm-4 col-xs-7">							
                            <input type="text"  class="form-control input-default "  placeholder="Masukkan ORG Code" name="ORGCode" value="{{old("ORGCode")}}" title="ORG Code">
						</div>
					</div>
                    <div class="clearfix">&nbsp;</div>

                    
                    <p class="text-center">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Submit</button> &nbsp;
                        <a class="btn btn-primary" href="{{url("administrator/lokasiitem")}}" title="Back"><span class="fa fa-undo"></span> Back</a>
                    </p>
					
                </form>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
@endsection