@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Edit Sub-Inventory</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
            <div class="card-body content-box-body">
				<div class="clearfix">&nbsp;</div>
            @include('includes.include_error_prop')

                <form role="form" action="{{url("administrator/lokasiitem/$lokasiitem->id/update")}}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                @if(WorkerAuth::auth()->getAuth()->aplikasi == "KAPAL")
                <input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">
                @else
                <div class="row">
                    <div class="col-sm-2 col-xs-5">Kapal</div>
                    <div class="col-sm-4 col-xs-7">
                        <select class="form-control input-default" name="kapalId" id="kapalId">
                            @foreach($kapals as $kapal)
                                <option value="{{$kapal->id}}" @if($kapal->id == $lokasiitem->kapalId) selected @endif>{{ $kapal->namaKapal }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
                @endif
                
                <div class="row">
                    <div class="col-sm-2 col-xs-5">Sub-Inventory</div>
                    <div class="col-sm-4 col-xs-7">							
                    <input type="text" class="form-control input-default "  placeholder="Masukkan namaLokasi" name="namaLokasi" value="{{$lokasiitem->namaLokasi}}" title="namaLokasi">
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>

                <div class="row">
                    <div class="col-sm-2 col-xs-5">Lokasi</div>
                    <div class="col-sm-4 col-xs-7">							
                    <input type="text" class="form-control input-default "  placeholder="Masukkan lokasi" name="lokasi" value="{{$lokasiitem->lokasi}}" title="lokasi">
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>

                <div class="row">
                    <div class="col-sm-2 col-xs-5">Alamat Lokasi</div>
                    <div class="col-sm-4 col-xs-7">							
                    <input type="text" class="form-control input-default "  placeholder="Masukkan alamat lokasi" name="alamatLokasi" value="{{$lokasiitem->alamatLokasi}}" title="alamat lokasi">
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>

                <div class="row">
                    <div class="col-sm-2 col-xs-5">ORG Code</div>
                    <div class="col-sm-4 col-xs-7">							
                    <input type="text" class="form-control input-default "  placeholder="Masukkan ORG Code" name="ORGCode" value="{{$lokasiitem->ORGCode}}" title="ORG Code">
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>

                    <p class="text-center">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button> &nbsp; 
                        <a class="btn btn-primary" href="{{url("administrator/lokasiitem")}}" title="Back"><span class="fa fa-undo"></span> Back</a>
                    </p>
                </form>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
@endsection