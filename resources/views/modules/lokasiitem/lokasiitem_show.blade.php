@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Detail Sub-Inventory</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
            <div class="card-body content-box-body">
                @include('includes.include_error_prop')           
                <div class="row">
                    <div class="col-sm-2 col-xs-5">Sub-Inventory</div>
                    <div class="col-sm-4 col-xs-7">							
                    <input type="text" disabled class="form-control input-default "  placeholder="Masukkan namaLokasi" name="namaLokasi" value="{{$lokasiitem->namaLokasi}}" title="namaLokasi">
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>

                <div class="row">
                    <div class="col-sm-2 col-xs-5">Deskripsi</div>
                    <div class="col-sm-4 col-xs-7">							
                    <input type="text" disabled class="form-control input-default "  placeholder="-" name="lokasi" value="{{$lokasiitem->lokasi}}" title="lokasi">
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>

                <div class="row">
                    <div class="col-sm-2 col-xs-5">Alamat Lokasi</div>
                    <div class="col-sm-4 col-xs-7">							
                    <input type="text" disabled class="form-control input-default "  placeholder="-" name="alamatLokasi" value="{{$lokasiitem->alamatLokasi}}" title="alamat lokasi">
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>

                @if(WorkerAuth::auth()->getAuth()->aplikasi == "KAPAL")
                @else
                <div class="row">
                    <div class="col-sm-2 col-xs-5">Kapal</div>
                    <div class="col-sm-4 col-xs-7">
                        <!-- <input type="text" disabled class="form-control input-default " value="{{ implode(", ", $lokasiitem->namaKapal) }}"> -->
                        <textarea name="kapal" disabled class="form-control input-default ">{{ implode(", ", $lokasiitem->namaKapal) }}</textarea>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
                @endif
                    
                <p class="text-center">
                    <a class="btn btn-primary" href="{{url("administrator/lokasiitem")}}" title="Back"><span class="fa fa-undo"></span> Kembali</a> &nbsp;
                    <!-- <a class="btn btn-primary" href="{{url("administrator/lokasiitem/$lokasiitem->id/edit")}}" title="Edit data"><span class="fa fa-edit"></span> Edit</a> -->
                </p>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
@endsection