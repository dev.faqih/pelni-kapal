@extends('app')
@section('content')
@include('includes.include_navigation')

    <div class="container content">
        
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Manual Sync {{ $title }} </h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
        </div>
		<div class="clearfix">&nbsp;</div>
		<div class="container border rounded bg-light">
            <div class="clearfix">&nbsp;</div>
            @include('includes.include_error_prop')    
            <div class="table-responsive">
                <h3>Oracle -&gt; HO</h3>
                <table id="log" class="table table-hover table-striped widget-table"name="table-index">
                    <thead>
                    <tr>
                        <th class="header-pagination">Modul</th>
                        <th class="header-pagination">Tanggal Last Sync</th>
                        <th class="header-pagination">&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($oracleHO as $log)                 
                        <tr>	 
                            <td>{{ $log[1] }}</td>
                            <td><span  id="{{ $log[0] }}Date">@if(isset($log[2])) {{ $log[2]->process_date }} @else No Data  @endif</span></td>
                            <td><button type="button" onClick="syncHO('{{ $log[0] }}');" class="rounded btn btn-danger"><span><i class="fa fa-cloud-download"></i></span></button> </td>
                        </tr>                       
                        @endforeach
                    </tbody>
                </table>
                <div class="clearfix">&nbsp;</div>
                <div class="clearfix">&nbsp;</div>
                
                <table class="table table-hover table-striped widget-table"name="table-index">
                    <tr>
                        <td class="header-pagination"><h3>HO -&gt; Oracle</h3></td>
                        <td><button type="button" onClick="syncHO('hoOracle');" class="rounded btn btn-danger"><span><i class="fa fa-cloud-download"></i></span></button> </td>
                    </tr>
                </table>

                <div class="clearfix">&nbsp;</div>
                <table id="log" class="table table-hover table-striped widget-table"name="table-index">
                    <thead>
                    <tr>
                        <th class="header-pagination">Modul</th>
                        <th class="header-pagination">Tanggal Last Sync</th>
                        <!-- <th class="header-pagination">&nbsp;</th> -->
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($hoOracle as $log)                 
                        <tr>	 
                            <td>{{ $log[1] }}</td>
                            <td><span id="{{ $log[0] }}Date">@if(isset($log[2])) {{ $log[2]->process_date }} @else No Data  @endif</span></td>
                            <!-- <td><button type="button" onClick="syncHO('{{ $log[0] }}');" class="rounded btn btn-danger"><span><i class="fa fa-cloud-upload"></i></span></button> </td> -->
                            <td> </td>
                        </tr>                       
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('includes.includes_footer')
    <script>
    function syncHO(modul) {
        overlayOn();

        $.ajax({
            url: "{{url("administrator/manualsync/syncho")}}",
            type: "GET",
            data: { modul : modul },
            dataType: "json",
            success: function (data, status, jqXHR) {
                if(modul == 'hoOracle') {
                    $.each(data, function(idx, row) {
                        if(row[2]) {
                            $("#" + row[0] + "Date").html(row[2]);
                        }
                    });
                } else {
                    if(data) {
                        $('#' + modul + 'Date').html(data.process_date);
                    } else {
                        alert('No Data');
                    }
                }
                overlayOff();
            },
            error: function (jqXHR, status, err) {
                overlayOff();
                alert("Terjadi kesalahan, Tutup peringatan ini dan coba kembali, atau refresh halaman ini dan mulai dari awal.");
            },
            complete: function (jqXHR, status) {
                overlayOff();
            },
            timeout: 60000000
        });
    }

    
    </script>
@endsection