@extends('app')
@section('content')
@include('includes.include_navigation')
    <div class="container content">
        
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Manual Sync {{ $title }} </h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
        </div>
		<div class="clearfix">&nbsp;</div>
		<div class="container border rounded bg-light">
            <div class="clearfix">&nbsp;</div>
            @include('includes.include_error_prop')    
            <div class="table-responsive">
                <h3>HO -&gt; Kapal</h3>
                <table id="log" class="table table-hover table-striped widget-table"name="table-index">
                    <thead>
                    <tr>
                        <th class="header-pagination">Modul</th>
                        <th class="header-pagination">Tanggal Last Sync</th>
                        <th class="header-pagination">&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($hoKapal as $log)                 
                        <tr>	 
                            <td>{{ $log[1] }}</td>
                            <td><span  id="{{ $log[0] }}Date">@if(isset($log[2])) {{ $log[2]->process_date }} @else No Data  @endif</span></td>
                            <td><button type="button" onClick="syncKapal('{{ $log[0] }}');" class="rounded btn btn-danger"><span><i class="fa fa-cloud-download"></i></span></button> </td>
                        </tr>                       
                        @endforeach
                    </tbody>
                </table>
                <div class="clearfix">&nbsp;</div>
                <div class="clearfix">&nbsp;</div>
                <div class="row">
                    <div class="col-sm-6"><h3>Kapal -&gt; HO</h3></h4></div>
                    <div class="col-sm-6 text-right">
                        <button type="button" onClick="syncKapal('kapalHO');" class="rounded btn btn-danger"><span><i class="fa fa-cloud-upload"></i> Transfer</span></button>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
                <table id="log" class="table table-hover table-striped widget-table"name="table-index">
                    <thead>
                    <tr>
                        <th class="header-pagination">Modul</th>
                        <th class="header-pagination">Tanggal Last Sync</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($kapalHO as $log)                 
                        <tr>	 
                            <td>{{ $log[1] }}</td>
                            <td><span  id="{{ $log[0] }}Date">@if(isset($log[2])) {{ $log[2]->process_date }} @else No Data  @endif</span></td>
                            <td> </td>
                        </tr>                       
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('includes.includes_footer')
    <script>
    function syncKapal(modul) {
        overlayOn();

        $.ajax({
            url: "{{url("administrator/manualsync/synckapal")}}",
            type: "GET",
            data: { modul : modul },
            dataType: "json",
            success: function (data, status, jqXHR) {     
                if(modul == 'kapalHO') {
                    console.log(data);
                    $.each(data, function(idx, row) {
                        if(row[2]) {
                            $("#" + row[0] + "Date").html(row[2].process_date);
                        }
                    });
//alert('cek atas kalau sampe');
                } else {
                    if(data) {
                        $('#' + modul + 'Date').html(data.process_date);
                    } else {
                        alert('No Data');
                    }
                }
                overlayOff();
            },
            error: function (jqXHR, status, err) {
                overlayOff();
                alert("Terjadi kesalahan, Tutup peringatan ini dan coba kembali, atau refresh halaman ini dan mulai dari awal.");
            },
            complete: function (jqXHR, status) {
                overlayOff();
            },
            timeout: 6000000
        });
    }

    
    </script>
@endsection
