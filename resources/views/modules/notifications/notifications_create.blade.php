@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        @include('includes.include_breadcrumb')
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Create a New Data of Test</h4></div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{url("administrator/notifications")}}" title="Back"><span class="fa fa-undo"></span> Back</a>
                </div>
            </div>
            <div class="card-body content-box-body">
            @include('includes.include_error_prop')
                <form action="{{url("administrator/notifications")}}" role="form" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="form-group">
					<label for="#userId">UserId</label>
					<select class="form-control input-default"  name="userId" title="userId">
						@foreach($userss as $users)
							<option value="{{$users->id}}">
								{{ $users->name }}</option>
						@endforeach
					</select>
					</div>

					<div class="form-group">
					<label for="#notificationType">NotificationType</label>
					<input type="text"  class="form-control input-default "  placeholder="Masukkan notificationType" name="notificationType" value="{{old("notificationType")}}" title="notificationType">
					</div>

					<div class="form-group">
					<label for="#refId">RefId</label>
					<input type="text"  class="form-control input-default "  placeholder="Masukkan refId" name="refId" value="{{old("refId")}}" title="refId">
					</div>

                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Submit</button>
                </form>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
@endsection