@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        @include('includes.include_breadcrumb')
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Edit data of{{$title}}</h4></div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{url("administrator/notifications")}}" title="Back"><span class="fa fa-undo"></span> Back</a>
                    <a class="btn btn-info" href="{{url("administrator/notifications/$notifications->id/edit")}}" title="Edit data"><span class="fa fa-edit"></span> Edit</a>
                </div>
            </div>
            <div class="card-body content-box-body">
            @include('includes.include_error_prop')
					<div class="form-group">
					<label for="#userId">UserId</label>
					<select class="form-control input-default" disabled name="userId" title="userId">
						@foreach($userss as $users)
							<option value="{{$users->id}}"@if($notifications->userId== $users->id) selected @endif>
								{{ $users->name }}</option>
						@endforeach
					</select>
					</div>

					<div class="form-group">
					<label for="#notificationType">NotificationType</label>
					<input type="text" disabled class="form-control input-default "  placeholder="Masukkan notificationType" name="notificationType" value="{{$notifications->notificationType}}" title="notificationType">
					</div>

					<div class="form-group">
					<label for="#refId">RefId</label>
					<input type="text" disabled class="form-control input-default "  placeholder="Masukkan refId" name="refId" value="{{$notifications->refId}}" title="refId">
					</div>


                </form>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
@endsection