@extends('app')
@section('content')
@include('includes.include_navigation')
<div class="container content">

	<div class="card content-box">
		<div class="card-header">
			<div class="pull-left">
				<h4>Report Pelumas per Bulan</h4>
			</div>
			<div class="pull-right">@include('includes.include_breadcrumb')</div>
		</div>
	</div>
	<div class="clearfix">&nbsp;</div>

	<form autocomplete="off" action="{{ url("administrator/pelumasbulan/show/" . $laporan->id) }}" role="form" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="_method" value="POST">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="userId" value="{{ WorkerAuth::auth()->getAuth()->id }}">
		<input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">
		<input type="hidden" name="laporanID" value="{{ $laporan->id }}">
		<div class="container border rounded bg-light">
			<div class="clearfix">&nbsp;</div>
			@include('includes.include_error_prop')

			<div class="row">
				<div class="col-sm-2 col-xs-5">Bulan</div>
				<div class="col-sm-3 col-xs-7">
					<input readonly required class=" form-control input-default search-value" name="voyageStart" value="{{ $laporan->bulan_text . " " . $laporan->tahun }}">
				</div>
			</div><br />
			<div class="row">
				<div class="col-sm-2 col-xs-5">Keterangan</div>
				<div class="col-sm-3 col-xs-7">
					<input class="form-control input-default" name="keterangan" value="{{ $laporan->keterangan }}">
				</div>
			</div><br />

			<div class="clearfix">&nbsp;</div>
		</div>
		<div class="clearfix">&nbsp;</div>
		<div class="container border rounded bg-light">
			<div class="clearfix">&nbsp;</div>
			<div class="row justify-content-between">
				<div class="col-sm-4 col-xs-6">
					<h5>Daftar Penerimaan Pelumas per Bulan</h5>
				</div>
			</div><br />
			<div class="table-responsive">
				<table id="tableItem" class="table table-hover table-striped widget-table">
					<thead>
						<tr>
							<th>No</th>
							<th>Item</th>
							<th>Saldo Awal</th>

							<th>Pelabuhan</th>
							<th>Tanggal Transaksi</th>
							<th>No Transaksi</th>
							<th>Jumlah</th>
							<th>Pemakaian</th>
							<th>Saldo Akhir</th>
						</tr>
					</thead>
					<tbody>
						<?php $i = 0;
						$prevItem = "xxx"; ?>
						@foreach($laporan->details as $item)
						<tr>
							<td>@if($item->namaItem != $prevItem) {{ ++$i }} @endif</td>
							<td>@if($item->namaItem != $prevItem) {{ $item->deskripsi }} @endif</td>
							<td>@if($item->namaItem != $prevItem && ($item->saldo_awal) > 0) {{ floatval($item->saldo_awal) }} @endif</td>
							<td>@if(isset($item->pelabuhan) && ! empty($item->pelabuhan)) {{ $item->pelabuhan }} @endif</td>
							<td>@if(! empty($item->transactionDate)) {{ date("Y-m-d", strtotime($item->transactionDate)) }} @endif</td>
							<td>@if(isset($item->transactionNo)) {{ $item->transactionNo }} @endif</td>
							<td>@if(isset($item->qty) && $item->qty > 0) {{ floatval($item->qty) }} @endif</td>
							<td>@if($item->namaItem != $prevItem && ($item->pemakaian) > 0) {{ floatval($item->pemakaian) }} @endif</td>
							<td>@if($item->namaItem != $prevItem && ($item->saldo_akhir) > 0) {{ floatval($item->saldo_akhir) }} @endif</td>
						</tr>
						<?php $prevItem = $item->namaItem; ?>
						@endforeach
					</tbody>
				</table>
			</div>

			<div class="row">
				<div class="col-sm-6 col-xs-6">
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
				</div>
				<div class="col-sm-6 col-xs-6 text-right">
					<button type="button" onclick="kunciLaporan()" class="btn btn-danger"><i class="fa fa-key"></i> Kunci Laporan</button> &nbsp;
					<button type="button" onclick="repopulateData()" class="btn btn-primary"><i class="fa fa-refresh"></i> Re-populate Data</button> &nbsp;
					<a class="btn btn-primary" href="{{ url("administrator/pelumasbulan/") }}" title="Back"><span class="fa fa-undo"></span> Kembali</a>
				</div>
			</div><br />

		</div>
	</form>
</div>
</div>
@include('includes.includes_footer')
<script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>
<script>
	function repopulateData(modul) {
		overlayOn();
		$.ajax({
			url: "{{ url('administrator/pelumasbulan/repopulate')}}",
			type: "GET",
			data: {
				laporanID: "{{ $laporan->id }}"
			},
			dataType: "json",
			success: function(data, status, jqXHR) {
				if (data.status == 1) {
					overlayOff();
					alert(data.message);
					location.href = "{{ url('administrator/pelumasbulan/show/' . $laporan->id) }}";
				} else {
					alert(data.message);
				}
				overlayOff();
			},
			error: function(jqXHR, status, err) {
				overlayOff();
				alert("Terjadi kesalahan, Tutup peringatan ini dan coba kembali, atau refresh halaman ini dan mulai dari awal.");
			},
			complete: function(jqXHR, status) {
				overlayOff();
			},
			timeout: 6000000
		});
	}

	function kunciLaporan(modul) {
		overlayOn();
		$.ajax({
			url: "{{ url('administrator/pelumasvoyage/kuncilaporan')}}",
			type: "GET",
			data: {
				laporanID: "{{ $laporan->id }}"
			},
			dataType: "json",
			success: function(data, status, jqXHR) {
				if (data.status == 1) {
					overlayOff();
					alert(data.message);
					location.href = "{{ url('administrator/pelumasbulan/show/' . $laporan->id) }}";
				} else {
					alert(data.message);
				}
				overlayOff();
			},
			error: function(jqXHR, status, err) {
				overlayOff();
				alert("Terjadi kesalahan, Tutup peringatan ini dan coba kembali, atau refresh halaman ini dan mulai dari awal.");
			},
			complete: function(jqXHR, status) {
				overlayOff();
			},
			timeout: 6000000
		});
	}
</script>
@endsection