@extends('app')
@section('content')
@include('includes.include_navigation')
<div class="container content">
    <div class="card content-box">
        <div class="card-header">
            <div class="pull-left">
                <h4>Report Pelumas per Bulan</h4>
            </div>
            <div class="pull-right">@include('includes.include_breadcrumb')</div>
        </div>
    </div>
    <div class="clearfix">&nbsp;</div>



    <div class="container border rounded bg-light">
        <div class="clearfix">&nbsp;</div>

        @include('includes.include_error_prop')
        <div class="row">
            <div class="col-sm-2 col-xs-12">Periode</div>
            <div class="col-sm-2 col-xs-12">
                <select class="form-control select2 search-value  js-example-basic-single" name="tahun">
                    <option value=""></option>
                    @for($i=date("Y");$i>=date("Y")-5;$i--)
                    <option value="{{ $i }}">{{ $i }}</option>
                    @endfor
                </select>
            </div>
            <div class="col-sm-7">
                <button type="button" class="btn btn-primary " onClick="searchList()" name="search-btn-pagination" title="Search"><span class="fa fa-search"></span> Search</button> &nbsp;
                <button type="button" class="btn btn-primary  " name="clear-search" title="Reset Search"><span class="fa fa-trash-o"></span> Reset</button> &nbsp;
                <button type="button" onclick="generatePDF()" class="btn btn-danger" title="Create Data">Generate Report</button>
            </div>
        </div><br />
    </div>
    <!-- </div> -->

    <div class="clearfix">&nbsp;</div>

    <div class="container border rounded bg-light">
        <div class="card-title content-box-title add-space">
            <div class="row">
                <div class="col-md-12 col-xs-12 ">
                    <div class="pull-left">
                        <div class="addon-box-list">
                            <div class="form-group-inline">
                                <span class="addon">Show</span>
                                <select class="form-control" name="size" title="Total List">
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span class="addon">Data</span>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="pull-right">
                        <div class="addon-box-list add-space-left  ">
                            <div class="form-group-inline">
                                <span class="addon ">Ordering by</span>
                                <select class="form-control" title="Sorting Data" name="orderingBy">
                                    <option value="0" selected>Sorting By</option>
                                </select>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table id="laporan" class="table table-hover table-striped widget-table" name="table-index">
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th class="header-pagination">Bulan</th>
                        <!-- <th class="header-pagination">Awal</th>  -->
                        <th class="header-pagination">Keterangan</th>
                        <th class="header-pagination">Is Locked</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($laporans["rows"] as $laporan) {
                        ?>
                        <tr>
                            <td><a data-toggle="tooltip" data-placement="top" title="View" href="{{url("administrator/pelumasbulan/show/" . $laporan->id)}}" class="btn btn-outline-primary"><span class="fa fa-eye"></span></a></td>
                            <td><?php echo $laporan->bulan_text . " " . $laporan->tahun ?></td>
                            <!-- <td>1</td> <td>31</td> -->
                            <td><?php echo $laporan->keterangan; ?></td>
                            <td>{{ $laporan->is_locked }}</td>
                        </tr>

                    <?php } ?>
                </tbody>
            </table>

        </div>
        <div class=" content-box-footer">
            <div class="row">
                <div class="col-md-6 text-left col-xs-12">
                    Page {{$laporans["page"]}} of <span id="pageCount">{{$laporans["pageCount"]}}</span>
                    Total Row <span id="totalRow">{{$laporans["rowCount"]}}</span>
                </div>
                <div class="col-md-6  col-xs-12">
                    <ul class="pagination pull-right">
                        <li class="page-item ">
                            <button class="page-link" tabindex="-1" name="first" title="First Page">First
                            </button>
                        </li>
                        <li class="page-item" id="pagination-prev">
                            <a class="page-link" title="Prev Page">
                                <i class="fa fa-step-backward"></i></a>
                        </li>
                        <li class="page-item" id="pagination-next">
                            <a class="page-link" title="Next Page">
                                <i class="fa fa-step-forward"></i></a></li>
                        <li class="page-item">
                            <button class="page-link" name="last" title="Last Page">Last</button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- </div> -->
</div>



<div class="modal fade" id="pelumasReportModal" tabindex="-1" role="dialog" aria-labelledby="pelumasReportModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <form autocomplete="off" target="_blank" action="{{ url("administrator/pelumasbulan/generatePDF/") }}" method="POST">
                <input type="hidden" class="transactionID" name="transactionID" />
                <input type="hidden" name="_method" value="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="modal-header">
                    <h5 class="modal-title" id="pelumasReportModalLabel">Parameter Laporan Pelumas per Bulan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-5 col-xs-5">
                            <label for="#namaItem">Tahun</label>
                        </div>
                        <div class="col-sm-2 col-xs-7">
                            <input type="text" readonly class="form-control input-default" id="tahunReport" name="tahunReport" value="">
                        </div>
                    </div> <br />

                    <div class="row">
                        <div class="col-sm-5 col-xs-5">
                            <label for="#partNumber">No Laporan</label>
                        </div>
                        <div class="col-sm-2 col-xs-7">
                            <input type="text" class="form-control input-default " name="noLaporan" value="F.2017.4">
                        </div>
                    </div> <br />

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Generate Report</button>
                </div>

            </form>

        </div>
    </div>
</div>

@include('includes.includes_footer')
<script>
    <?php $laporans["rows"] = []; ?>
    var paginationParameter = ('{!! json_encode($laporans)!!}');
    var currentUrl = "{{url($currentPrefix)}}";
</script>
<script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>
<script>
    function generatePDF() {
        var tahun = $("select[name=tahun] option:selected").text();
        if (tahun != "") {
            $("#tahunReport").val(tahun);
            $('#pelumasReportModal').modal('show')
        } else {
            $("#defModalTitle").html("Report Pelumas (Bulan)");
            $("#defModalBody").html("Tahun laporan belum dipilih, atau terjadi kesalahan lainnya.<br /> Tutup peringatan ini dan coba kembali, atau refresh halaman ini dan mulai dari awal!");
            $("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>");
            $('#defModal').modal('show');
        }
    }
</script>
@endsection