<style>
    td {
        font-size: 8px;
    }

    .txctr {
        text-align: center;
    }

    .txlft {
        text-align: left;
    }

    .txtr8 {
        text-align: right;
    }

    .txt7px {
        font-size: 7px;
    }

    .txbold {
        font-weight: bold;
    }

    .text12 {
        font-size: 11px;
        font-weight: normal;
    }

    .text10 {
        font-size: 10px;
        font-weight: normal;
    }

    .text9 {
        font-size: 9px;
        font-weight: normal;
    }

    .bdrtop {
        border-top: 1px solid #000000;
    }

    .bdrbot {
        border-bottom: 1px solid #000000;
    }

    .bdrlef {
        border-left: 1px solid #000000;
    }

    .bdrr8 {
        border-right: 1px solid #000000;
    }

    .pad5 {
        padding: 5px 5px 5px 5px;
    }
</style>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="100%" class="txctr">
            <table width="100%" style="" class="" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="txctr" style="" width="16%"><br />&nbsp;<br />
                        <span><img src="{{ url("assets/img/pelni.png") }}" border="0" height="40" style="padding:0px 10px;"></span>
                    </td>
                    <td width="72%" class=" txctr"><br />&nbsp;<br />
                        <span style="font-size:13px;">PT. PELAYARAN NASIONAL INDONESIA (Persero)</span><br />
                        <span style="font-size:13px;">LAPORAN PENERIMAAN &amp; PEMAKAIAN MINYAK PELUMAS ( PER - {{ $textType }} )</span><br />
                    </td>
                    <td width="14%" class="txlft"><br /> &nbsp;<br />&nbsp;<br />{{ $noLaporan }}</td>
                </tr>
                <tr>
                    <td class="txlft" colspan="3"><span style="font-size:10px;">NAMA KAPAL: {{ $namaKapal }}</span><br /></td>
                </tr>
            </table>


        </td>
    </tr>
</table>

<?php $totalItems = count($data["headerItems"]);
$maxColomns = 10;

$blocksItemArray = array_chunk($data["headerItems"], $maxColomns, true);
// echo "<pre>";
// var_dump($data["headerItems"]);
// var_dump($blocksItemArray);
// echo "</pre>";
$widthItems = 390;

foreach ($blocksItemArray as $headerItems) {
    $nItems = count($headerItems);
    if ($nItems > $maxColomns) {
        $nItems = $maxColomns;
    }
    if ($nItems <= 1) {
        $colWidth = $widthItems;
    } else {
        $colWidth = floor($widthItems / $nItems);
        $widthItems = $colWidth * $nItems;
    }
    $i = 1;
    ?><br />&nbsp;<br />
    @if($nItems > 0)
    <table width="100%" cellpadding="0" class="" border="1" cellspacing="0">
        <thead>
            <tr>
                <th class="txctr text12" rowspan="2" width="30">No</th>
                <th class="txctr text9" rowspan="2" width="40">{{ $textType }}</th>
                <th class="txctr text12" colspan="2" width="100">Periode</th>

                <th class="txctr text12" rowspan="2" width="50">&nbsp;</th>
                <th class="txctr text9" rowspan="2" width="50">Pelabuhan Pengisian</th>
                <th class="txctr text12" rowspan="2" width="50">Tanggal</th>

                <th class="txctr text12" colspan="{{ $nItems }}" width="{{ $widthItems }}">Pencatatan Minyak Pelumas <small><em>(liter)</em></small> </th>
                <th class="txctr text12" rowspan="2" width="65">Keterangan</th>
            </tr>
            <tr>
                <th class="txctr text12" width="50">Awal</th>
                <th class="txctr text12" width="50">Akhir</th>

                @if(count($headerItems) > 0)
                @foreach($headerItems as $headerItem)
                <th class="txctr text12" width="{{ $colWidth }}"><small>{{ $headerItem["deskripsi"] }}</small></th>
                @endforeach
                @else
                <th class="txctr text12" width="{{ $colWidth }}">&nbsp;</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach($data["rowLaporans"] as $laporan)

            <?php
            $idxLaporan = $laporan["idxLaporan"];
            if (!isset($data["nPenerimaan"][$idxLaporan]["transactionDate"]) || !is_array($data["nPenerimaan"][$idxLaporan]["transactionDate"])) {
                $nPenerimaanArray = $data["nPenerimaan"][$idxLaporan]["transactionDate"] = [];
                $nPenerimaan = 0;
            } else {
                $nPenerimaanArray = $data["nPenerimaan"][$idxLaporan]["transactionDate"];
                ksort($nPenerimaanArray);
                $nPenerimaan = count($nPenerimaanArray);
            }

            if ($nPenerimaan > 0) {
                $rowSpan = 3 + $nPenerimaan;
            } else {
                $rowSpan = 4;
            } ?>

            <tr nobr="true">
                <td rowspan="{{ $rowSpan }}" class="txctr" width="30">{{ $i++ }}</td>
                <td rowspan="{{ $rowSpan }}" class="txctr" width="40">{{ $laporan["bulan"] }}</td>
                <td rowspan="{{ $rowSpan }}" class="txctr" width="50">{{ $laporan["awal"] }}</td>
                <td rowspan="{{ $rowSpan }}" class="txctr" width="50">{{ $laporan["akhir"] }}</td>
                <td class="txctr" width="50">Saldo Awal</td>
                <td class="txctr" width="50">&nbsp;</td>
                <td class="txctr" width="50">&nbsp;</td>

                @if(count($headerItems) > 0)
                @foreach($headerItems as $idx => $headerItem)
                <td class="txctr " width="{{ $colWidth }}">@if(isset($laporan[$idx][$idxLaporan]["saldo_awal"])) {{ floatval($laporan[$idx][$idxLaporan]["saldo_awal"]) }} @endif</td>
                @endforeach
                @else
                <td class="txctr " width="{{ $colWidth }}">&nbsp;</td>
                @endif
                <td rowspan="{{ $rowSpan }}" class="txctr" width="65">{{ $laporan["keterangan"] }}</td>

            </tr>

            <?php if (!empty($nPenerimaanArray)) {
                $penerimaanKeN = 0; ?>
                @foreach($nPenerimaanArray as $transactionDate => $pelabuhan)
                <tr>
                    @if($penerimaanKeN < 1) <td class="txctr" rowspan="{{ $nPenerimaan }}">Penerimaan</td>@endif
                        <td class=" txctr">{{ $pelabuhan }}</td>
                        <td class=" txctr">{{ $transactionDate }} </td>
                        <?php if (count($headerItems) < 1) {
                            echo "<td width='{{ $colWidth }}'>&nbsp;</td>";
                        } else { ?>
                            @foreach($headerItems as $idx => $headerItem)
                            <td class="txctr " width="{{ $colWidth }}">@if(isset($laporan[$idx][$idxLaporan]["penerimaan"][$transactionDate]["qty"])) {{ floatval($laporan[$idx][$idxLaporan]["penerimaan"][$transactionDate]["qty"]) }} @endif</td>
                            @endforeach
                        <?php } ?>
                </tr>
                <?php $penerimaanKeN++; ?>
                @endforeach
            <?php } else { ?>
                <tr>
                    <td class="txctr">Penerimaan</td>
                    <td class=" txctr"> &nbsp; </td>
                    <td class=" txctr"> &nbsp; </td>
                    <?php if (count($headerItems) > 0) { ?>
                        @foreach($headerItems as $idx => $headerItem)
                        <td width='{{ $colWidth }}'>&nbsp;</td>
                        @endforeach
                    <?php } else { ?>
                        <td width='{{ $colWidth }}'>&nbsp;</td>
                    <?php } ?>
                </tr>
            <?php } ?>

            <tr>
                <td class="txctr">Pemakaian</td>
                <td class="txctr">&nbsp;</td>
                <td class="txctr">&nbsp;</td>
                @if(count($headerItems) > 0)
                @foreach($headerItems as $idx => $headerItem)
                <td class="txctr " width="{{ $colWidth }}">@if(isset($laporan[$idx][$idxLaporan]["pemakaian"])) {{ floatval($laporan[$idx][$idxLaporan]["pemakaian"]) }} @endif</td>
                @endforeach
                @else
                <td class="txctr " width="{{ $colWidth }}">&nbsp;</td>
                @endif
            </tr>
            <tr>
                <td class="txctr">Saldo Akhir</td>
                <td class="txctr">&nbsp;</td>
                <td class="txctr">&nbsp;</td>
                @if(count($headerItems) > 0)
                @foreach($headerItems as $idx => $headerItem)
                <td class="txctr " width="{{ $colWidth }}">@if(isset($laporan[$idx][$idxLaporan]["saldo_akhir"])) {{ floatval($laporan[$idx][$idxLaporan]["saldo_akhir"]) }} @endif</td>
                @endforeach
                @else
                <td class="txctr " width="{{ $colWidth }}">&nbsp;</td>
                @endif
            </tr>

            @endforeach
        </tbody>
    </table>

    @endif

<?php } // end of FOREACH
?>
<br />&nbsp;<br />&nbsp;
<table width="100%" cellpadding="0" class="" border="0" cellspacing="0">
    <tr nobr="true">
        <td>
            <table width="100%" cellpadding="0" class="" border="0" cellspacing="0">
                <tr>
                    <td width="300">Keterangan</td>
                </tr>
                <tr>
                    <td width="300">1. Jenis minyak pelumas disesuaikan dengan yang ada di kapal</td>
                </tr>
                <tr>
                    <td width="300">2. Pengisian jumlah minyak pelumas disesuaikan dengan Log Book</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br />&nbsp;<br />&nbsp;

<table style="" width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr nobr="true">
        <td>
            <table style="" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="120">&nbsp;</td>
                    <td width="150" class="txctr">MENGETAHUI: <br />NAKHODA</td>
                    <td width="250">&nbsp;</td>
                    <td width="150" class="txctr">{!! $namaKapal . ", " . date("d F Y") . "<br />DIBUAT OLEH: <br /> KEPALA KAMAR MESIN" !!} </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td class=""><br />&nbsp;<br /><br />&nbsp;<br /><br />&nbsp;<br /></td>
                    <td></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="txctr">{{ $approver->name }}</td>
                    <td></td>
                    <td class="txctr">{{ $requester->name }}</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="txctr">{{ $approver->nrp }}</td>
                    <td></td>
                    <td class="txctr">{{ $requester->nrp }}</td>
                </tr>
            </table>
        </td>
    </tr>
</table>