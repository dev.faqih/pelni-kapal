@extends('app')
@section('content')
@include('includes.include_navigation')
<div class="container content">

	<div class="card content-box">
		<div class="card-header">
			<div class="pull-left">
				<h4>Report Pelumas per Bulan</h4>
			</div>
			<div class="pull-right">@include('includes.include_breadcrumb')</div>
		</div>
	</div>
	<div class="clearfix">&nbsp;</div>


	<div class="container border rounded bg-light">
		<div class="clearfix">&nbsp;</div>
		@include('includes.include_error_prop')
		<div class="row">
			<div class="col-sm-2 col-xs-5">Bulan</div>
			<div class="col-sm-3 col-xs-7">
				<input readonly required class=" form-control input-default search-value" name="voyageStart" value="{{ $laporan->bulan_text . " " . $laporan->tahun }}">
			</div>
		</div><br />
		<div class="row">
			<div class="col-sm-2 col-xs-5">Keterangan</div>
			<div class="col-sm-3 col-xs-7">
				<input readonly class=" form-control input-default search-value" name="voyageStart" value="{{ $laporan->keterangan }}">
			</div>
		</div><br />
		<div class="clearfix">&nbsp;</div>
	</div>
	<div class="clearfix">&nbsp;</div>
	<div class="container border rounded bg-light">

		<div class="clearfix">&nbsp;</div>
		<div class="row justify-content-between">
			<div class="col-sm-4 col-xs-6">
				<h5>Daftar Penerimaan Pelumas per Bulan</h5>
			</div>
		</div><br />
		<div class="table-responsive">
			<table id="tableItem" class="table table-hover table-striped widget-table">
				<thead>
					<tr>
						<th>No</th>
						<th>Item</th>
						<th>Saldo Awal</th>
						<th>Pelabuhan</th>
						<th>Tanggal Transaksi</th>
						<th>No Transaksi</th>
						<th>Jumlah</th>
						<th>Pemakaian</th>
						<th>Saldo Akhir</th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 0;
					$prevItem = "xxx"; ?>
					@foreach($laporan->details as $item)
					<tr>
						<td>@if($item->itemId != $prevItem) {{ ++$i }} @endif</td>
						<td>@if($item->itemId != $prevItem) {{ $item->deskripsi }} @endif</td>
						<td>@if($item->itemId != $prevItem) {{ floatval($item->saldo_awal) }} @endif</td>
						<td>{{ $item->pelabuhan }}</td>
						<td>{{ date("Y-m-d", strtotime($item->transactionDate)) }}</td>
						<td>{{ $item->transactionNo }}</td>
						<td>{{ floatval($item->qty) }}</td>
						<td>@if($item->itemId != $prevItem) {{ floatval($item->pemakaian) }} @endif</td>
						<td>@if($item->itemId != $prevItem) {{ floatval($item->saldo_akhir) }} @endif</td>
					</tr>
					<?php $prevItem = $item->itemId; ?>
					@endforeach
				</tbody>
			</table>
		</div><br />
		<p class="text-center">
			<a class="btn btn-primary" href="{{ URL::previous() }}" title="Back"><span class="fa fa-undo"></span> Kembali</a>
		</p>

	</div>
</div>
</div>
@include('includes.includes_footer')
<script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>

@endsection