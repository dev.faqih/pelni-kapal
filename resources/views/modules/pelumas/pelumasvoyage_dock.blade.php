@extends('app')
@section('content')
@include('includes.include_navigation')
<div class="container content">

	<div class="card content-box">
		<div class="card-header">
			<div class="pull-left">
				<h4>Report Pelumas per Voyage</h4>
			</div>
			<div class="pull-right">@include('includes.include_breadcrumb')</div>
		</div>
	</div>
	<div class="clearfix">&nbsp;</div>

	<form autocomplete="off" action="{{ url("administrator/pelumasvoyage/create/") }}" role="form" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="_method" value="POST">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="userId" value="{{ WorkerAuth::auth()->getAuth()->id }}">
		<input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">
		<input type="hidden" name="tahun" value="{{ date("Y") }}">
		<input type="hidden" name="bulan" value="{{ date("n") }}">
		<input type="hidden" name="lastVoyageDate" value="{{ date("Y-m-d", strtotime($latestDataReport->voyage_to)) }}">
		<div class="container border rounded bg-light">
			<div class="clearfix">&nbsp;</div>
			@include('includes.include_error_prop')

			<div class="row">
				<div class="col-sm-2 col-xs-5">Voyage</div>
				<div class="col-sm-3 col-xs-7">
					<select class="form-control input-default" required name="voyageKode" title="Voyage">
						@foreach($voyages as $voyage)
						<option value="{{ $voyage->voyageKode }}">{{ $voyage->voyageName }} </option>
						@endforeach
					</select>
				</div>
				<div class="col-sm-1 col-xs-2">&nbsp;</div>
				<div class="col-sm-2 col-xs-5">Tanggal Voyage</div>
				<div class="col-sm-3 col-xs-7 input-group">
					<input required name="voyage_from" class="form-control datepicker input-default " data-date-clear-btn="true" data-date-format="yyyy-mm-dd" data-date-autoclose="true" onChange="checkDockDate('voyage_from','lastVoyageDate', '{{ date('Y-m-d',strtotime("-1 days")) }}');" value="">
					<div class="input-group-addon"> &nbsp; s/d &nbsp; </div>
					<input required name="voyage_to" class="form-control datepicker input-default " data-date-clear-btn="true" data-date-format="yyyy-mm-dd" data-date-autoclose="true" onChange="checkDockDate('voyage_to','voyage_from', '{{ date('Y-m-d',strtotime("-1 days")) }}');" value="">
				</div>
			</div><br />
			<div class="row">
				<div class="col-sm-2 col-xs-5">Keterangan</div>
				<div class="col-sm-3 col-xs-7">
					<input class="form-control input-default" name="keterangan" placeholder="Keterangan" value="">
				</div>
			</div><br />
			<div class="clearfix">&nbsp;</div>

			<div class="row">
				<div class="col-sm-6 col-xs-6">
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button> &nbsp;
					<a class="btn btn-primary" href="{{ url("administrator/pelumasvoyage/") }}" title="Back"><span class="fa fa-undo"></span> Kembali</a>
				</div>
			</div><br />

		</div>
	</form>
</div>
</div>
@include('includes.includes_footer')
<script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>

@endsection