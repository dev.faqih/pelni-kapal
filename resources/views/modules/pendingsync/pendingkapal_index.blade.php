@extends('app')
@section('content')
    @include('includes.include_navigation') 
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Pending Sync</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
            <div class="card-body content-box-body">
				<div class="clearfix">&nbsp;</div>
            @include('includes.include_error_prop')
                <div class="row">
                    <div class="col-sm-2 col-xs-12">Nama Modul</div>
                    <div class="col-sm-3 col-xs-12">
                        <select class="form-control search-value" title="" name="table_name">
                            <option selected></option>
                            <option value="permintaanbarang">Permintaan Barang</option>
                            <option value="popenerimaan">PO Penerimaan</option>
                            <option value="penerimaanbarang">Penerimaan Barang</option>
                            <option value="pindahbarangio">Transfer IO</option>
                            <option value="popenerimaanio">Penerimaan IO</option>
                            <option value="penerimaanbarangio">Penerimaan Barang IO</option>                            
                            <option value="pindahbarangsubinv">Pindah SubInventory</option>
                            <option value="penggunaanbarang">Penggunaan Barang</option>
                            <option value="penerimaanmakanan">Penerimaan Makanan</option>
                            <option value="penggunaanmakanan">Penggunaan Makanan</option>
                            <option value="itempermintaanbarang">Item Permintaan Barang</option>
                            <option value="itempenerimaanbarang">Item Penerimaan Barang</option>
                            <option value="itempindahbarangio">Item Transfer IO</option>
                            <option value="itempenerimaanbarangio">Item Penerimaan Barang IO</option>
                            <option value="itempindahbarangsubinv">Item Pindah SubInventory</option>
                            <option value="itempenggunaanbarang">Item Penggunaan Barang</option>
                            <option value="itempenerimaanmakanan">Item Penerimaan Makanan</option>
                            <option value="itempenggunaanmakanan">Item Penggunaan Makanan</option>
                            <option value="stg_pelni_iot_int">Staging Transfer/Penerimaan IO</option>
                            <option value="stg_pelni_pr_int">Staging Permintaan Barang</option>
                            <option value="stg_pelni_scm_misc_rcv_int">Staging Penerimaan/Penggunaan Makanan</option>
                            <option value="stg_pelni_scm_moi_int">Staging Penggunaan Barang</option>
                            <option value="stg_pelni_scm_mot_int">Staging Pindah Barang Subinventory</option>
                            <option value="stg_pelni_scm_rcv_int">Staging Penerimaan Barang</option>
                            <option value="stg_pelni_scm_rcv_trx_int">Staging Item Penerimaan Barang</option>
                            <option value="workflow">Workflow</option>
                        </select>
                    </div>
                </div><br />
                <div class="row">
                    <div class="col-sm-2">&nbsp;</div>
                    <div class="col-sm-10 col-xs-12">
                        <button type="button" class="btn btn-primary " onClick="searchList()" name="search-btn-pagination" title="Search"><span class="fa fa-search"></span> Search</button> &nbsp;
                        <button type="button" class="btn btn-primary  " name="clear-search" title="Reset Search"><span class="fa fa-trash-o"></span> Reset</button>
                    </div>
                </div><br />
                <div class="card-title content-box-title add-space">
                    <div class="row">
                        <div class="col-md-12 col-xs-12 ">
                            <div class="pull-left">
                                <div class="addon-box-list">
                                    <div class="form-group-inline">
                                        <span class="addon">Show</span>
                                         <select class="form-control" name="size" title="Total List">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select>
                                        <span class="addon">Data</span>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right">
                                <div class="addon-box-list add-space-left  ">
                                    <div class="form-group-inline">
                                        <span class="addon ">Ordering by</span>
                                       <select class="form-control" title="Sorting Data" name="orderingBy">
                                           <option value="0" selected>Sorting By</option>
                                           @foreach($ordering as $key => $value)
                                               <option value="{{$key}}">{{$value}}</option>
                                           @endforeach
                                       </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
					<table id="log" class="table table-hover table-striped widget-table"name="table-index">
						<thead>
						<tr>
                            <th class="header-pagination">Nama Modul</th>
                            <th class="header-pagination">Detail</th>
						</tr>
						</thead>
						<tbody>
							@foreach($logs["rows"]as $log)
                            <tr>
                                <td>{{$log->table_name}}</td>
                                <td>{{$log->data}}</td>
                            </tr>
							@endforeach
						</tbody>
					</table>

                </div>
                <div class=" content-box-footer">
                    <div class="row">
                        <div class="col-md-6 text-left col-xs-12">
                        Page {{$logs["page"]}} of <span id="pageCount">{{$logs["pageCount"]}}</span>
                        Total Row <span id="totalRow">{{$logs["rowCount"]}}</span>
                        </div>
                        <div class="col-md-6  col-xs-12">
                           <ul class="pagination pull-right">
                               <li class="page-item ">
                                   <button class="page-link" tabindex="-1" name="first" title="First Page">First
                                   </button>
                               </li>
                               <li class="page-item" id="pagination-prev">
                                    <a class="page-link" title="Prev Page">
                                        <i class="fa fa-step-backward"></i></a>
                               </li>
                               <li class="page-item" id="pagination-next">
                                    <a class="page-link" title="Next Page">
                                        <i class="fa fa-step-forward"></i></a></li>
                               <li class="page-item">
                                   <button class="page-link" name="last" title="Last Page">Last</button>
                               </li>
                           </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes.includes_footer')
     <script>
            var paginationParameter = ('{!! json_encode($logs)!!}');
            var currentUrl = "{{url($currentPrefix)}}";
        </script>
    <script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>
@endsection