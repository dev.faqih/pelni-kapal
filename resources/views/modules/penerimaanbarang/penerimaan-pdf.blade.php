<style>
td { font-size:8px; }
.txctr { text-align:center; }
.txbold { font-weight:bold; }
.bdrtop { border-top:1px solid #000000; }
.bdrbot { border-bottom:1px solid #000000; }
.bdrlef { border-left:1px solid #000000; }
.bdrr8 { border-right:1px solid #000000; }
.pad5 { padding: 5px 5px 5px 5px; }

</style>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
    <td width="100%" class="txctr">
        <br />&nbsp;<br />
        <span style="font-size:11px;">PERUSAHAAN PERSEROAN (PERSERO) </span><br />
        <span style="font-size:13px;">PT PELAYARAN NASIONAL INDONESIA (PELNI)</span><br />
        <span style="font-size:8px;">Jl. GAJAH MADA NO. 14 JAKARTA PUSAT</span>
    </td>
</tr>
<tr><td class="txctr" >
    <br />&nbsp;<br />
    <span style="font-size:13px;">LAPORAN BARANG MASUK</span>
</td></tr>
<tr><td class="" style="">
    <br />&nbsp;<br />
    <table style="" border="0" cellpadding="0" cellspacing="4">
    <tr>
        <td width="12%" >Nama Kapal</td>
        <td width="38%" >:  {{ $kapal->namaKapal }}</td>
        <td width="8%">&nbsp;</td>
        <td width="42%">&nbsp;</td>
    </tr>
    <tr>
        <td>GRN No.</td>
        <td>: {{ $penerimaanbarang->detailNumber }} </td>
        <td>Supplier</td>
        <td>: {{ $popenerimaan->vendorName }} </td>
    </tr>
    <tr>
        <td>Date</td>
        <td>: {{ date("d F Y") }}</td>
        <td>Store</td>
        <td>:</td>
    </tr>
    </table>    
    <br />
</td></tr>
<tr><td class="" style="">
    <table cellpadding="3" class="bdrtop bdrlef" cellspacing="0">
        <thead><tr>
            <td class="bdrr8 bdrbot txctr txbold" width="18">No</td>
            <td class="bdrr8 bdrbot txctr txbold" width="100">Item Number</td>
            <td class="bdrr8 bdrbot txctr txbold" width="80">Part Number</td>
            <td class="bdrr8 bdrbot txctr txbold" width="140">Item Description</td>
            <td class="bdrr8 bdrbot txctr txbold" width="30">Qty.</td>
            <td class="bdrr8 bdrbot txctr txbold" width="50">UOM</td>
            <td class="bdrr8 bdrbot txctr txbold" width="110">PO</td>
        </tr></thead>
        <tbody>
        <?php $i = 1; foreach($listItems as $item) { ?>
        <tr nobr="true">
            <td class="bdrr8 bdrbot">{{ $i++ }}</td>
            <td class="bdrr8 bdrbot">{{ $item->namaItem }}</td>
            <td class="bdrr8 bdrbot">{{ $item->partNumber }}</td>
            <td class="bdrr8 bdrbot">{{ $item->deskripsi }}</td>
            <td class="bdrr8 bdrbot txctr">{{ floatval($item->quantity) }}</td>
            <td class="bdrr8 bdrbot txctr">{{ $item->UOM }}</td>
            <td class="bdrr8 bdrbot">{{ $penerimaanbarang->headerNumber }}</td>
        </tr>
        <?php } ?>
        </tbody>
    </table>
</td></tr>
<tr><td>
    <br />&nbsp;<br />&nbsp;<br />
</td></tr><tr nobr="true"><td>
    <table style="" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="120" >&nbsp;</td>
        <td width="100" class="bdrtop bdrlef bdrr8 bdrbot txctr">Pemeriksa Barang</td>
        <td width="100" class="bdrtop bdrlef bdrr8 bdrbot txctr">Manager Pergudangan</td>
    </tr>
    <tr>
        <td></td>
        <td class="bdrlef bdrr8 bdrbot"></td>
        <td class="bdrr8 bdrbot"><br />&nbsp;<br /><br />&nbsp;<br /><br />&nbsp;<br /></td>
    </tr>
    </table>    
    <br />
</td></tr>
</table>