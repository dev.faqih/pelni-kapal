@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Penerimaan Barangg</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
		</div>
        <div class="clearfix">&nbsp;</div>
		<form autocomplete="off" class="submitTransaction" action="{{url("administrator/popenerimaan")}}" role="form" method="POST" enctype="multipart/form-data">
        <div class="container border rounded bg-light">
				<div class="clearfix">&nbsp;</div>
				@include('includes.include_error_prop')
			
				<input type="hidden" name="_method" value="POST">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="userId" value="{{ WorkerAuth::auth()->getAuth()->id }}">
				<input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">
				<input type="hidden" name="kodeKapal" value="{{ WorkerAuth::auth()->getAuth()->kodeKapal }}">
				<input type="hidden" name="status" value="0">
				<input type="hidden" name="qtyDiterima" value="0">
				<input type="hidden" name="poID" value="{{ $popenerimaan->id }}">
				<input type="hidden" name="vendorId" value="{{ intval($popenerimaan->vendorId) }}">
				<input type="hidden" name="vendorName" value="{{ $popenerimaan->vendorName }}">
				<input type="hidden" name="jabatan" value="{{ WorkerAuth::auth()->getAuth()->jabatan }}">
				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#poNumber">No PO</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" readonly class="form-control input-default"  placeholder="Masukkan unit operasi" name="poNumber" value="{{ $popenerimaan->poNumber }}" title="poNumber">
					</div>
					<div class="col-sm-2 col-xs-5">
						<label for="#receiptNumber">No Receipt</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" class="form-control input-default" readonly placeholder="Masukkan unit operasi" name="receiptNumber" value="PELNI/PB {{ date("y") . " " . WorkerAuth::auth()->getAuth()->kodeKapal }} ######" title="receiptNumber">
					</div>
				</div><br />

				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#unitOperasi">Unit Operasi</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text"  class="form-control input-default" readonly placeholder="Masukkan unit operasi" name="unitOperasi" value="{{ $popenerimaan->operatingUnit }}" title="unitOperasi">
					</div>

					<div class="col-sm-2 col-xs-5">
						<label for="#tanggalPO">Tanggal PO</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text"  class="form-control input-default" readonly placeholder="Masukkan unit operasi" name="tanggalPO" value="<?php 
					if($popenerimaan->date) { 
						echo date("Y-m-d", strtotime($popenerimaan->date)); 
					} ?>" title="tanggalPO">
					</div>
				</div><br />

				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#tanggalPenerimaan">Tanggal Diterima</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" id="tanggalPenerimaan" onChange="checkIsSmallerThanPODate('tanggalPenerimaan','tanggalPO');checkIsMoreThan3Days('tanggalPenerimaan', '{{ date("Y-m-d H:i:s") }}','{{ WorkerAuth::auth()->getAuth()->sysParams->BACKDATE }}');loadVoyage('{{ url('administrator/popenerimaan/getVoyages') }}', 'tanggalPenerimaan')" class="form-control datepicker input-default " data-date-clear-btn="true" data-date-format="yyyy-mm-dd" data-date-autoclose="true" required name="tanggalPenerimaan" title="tanggal penerimaan">
					</div>
				</div><br />
				<div class="row">
					<div class="col-sm-2 col-xs-5">Voyage</div>
					<div class="col-sm-4 col-xs-7">
						<select class="form-control input-default" required name="voyageKode" title="Voyage">
						</select>
					</div>
					<div class="col-sm-2 col-xs-5">Tanggal Voyage</div>
					<div class="col-sm-4 col-xs-7 input-group">
						<input readonly required class="form-control input-default search-value" name="voyageStart" >
						<div class="input-group-addon"> &nbsp; s/d &nbsp; </div>
						<input readonly required class=" form-control input-default search-value" name="voyageEnd" >
					</div>
				</div><br />

			</div>
			<div class="clearfix">&nbsp;</div>
			<div class="container border rounded bg-light">

				<div class="clearfix">&nbsp;</div>
				<div class="row justify-content-between">
					<div class="col-sm-4 col-xs-6"><h5>Daftar Barang</h5></div>
					<div class="col-sm-3 text-right col-xs-5">
						<button type="button" id="loadItemModal" class="btn btn-primary">Tambah Item</button>
					</div>
				</div><br />
				<div class="table-responsive">
					<table id="tableItem" class="table table-hover table-striped widget-table">
					<thead>
						<tr>
							<th>No</th>
							<th>Kode Barang</th>
							<th>Part Number</th>
							<th>Deskripsi</th>
							<th>UOM</th>
							<th>Qty Penerimaan</th>
							<th>Sub-Inventory</th>
							<th>Lot Number</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody></tbody>
					</table>
				</div><br />

				<p class="text-center">
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Kirim</button> &nbsp;
					<a class="btn btn-primary" href="{{url("administrator/popenerimaan/$poNumber")}}" title="Back"><span class="fa fa-undo"></span> Kembali</a>
				</p>
			

        </div>
		</form>
    </div>
    @include('includes.includes_footer')
	@include('includes.include_modal_penerimaanbarang')
    <script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>
	<script>
        $(document).ready(function() {            
			$('.datepicker').datepicker('update', '{{ date('Y-m-d') }}');
			loadVoyage('{{ url('administrator/popenerimaan/getVoyages') }}', 'tanggalPenerimaan');
		});
		
		$("select[name='voyageKode']").change(function() {
			loadVoyageDate('{{ url('administrator/permintaanbarang/getVoyageDate') }}', $(this).val() );
		});
	</script>

@endsection
