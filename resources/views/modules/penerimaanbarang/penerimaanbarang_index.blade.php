@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        @include('includes.include_breadcrumb')
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Listing Data of {{$title}}</h4></div>
                @if(WorkerAuth::auth()->getAuth()->aplikasi == "KAPAL")
                <div class="pull-right">
                    <a class="btn btn-danger" href="{{url("administrator/penerimaanbarang/create")}}" title="Create Data"><span class="fa fa-plus"></span> New </a>
                    <!-- <button class="btn btn-primary" title="Delete Data" name="delete-content"><span class="fa fa-trash"></span> Trash</button> -->
                    <!-- <button class="btn btn-info" title="Export Data"><span class="fa fa-file-excel-o"></span> Export</button> -->
                </div>
                @endif
            </div>
            <div class="card-body content-box-body">
            @include('includes.include_error_prop')
                <div class="row">
                    <div class="col-md-3 col-xs-12">
                      <select class="form-control input-default" name="q" title="Search Something">
                        <option value="">Search By</option>
                            @foreach($criteria as $key => $value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                      </select>
                    </div>
                    <div class="col-md-3 col-xs-12">
                         <input class="form-control input-default" name="search-value" title="Search Value">
                    </div>
                    <div class="col-md-6">
                        <button class="btn btn-primary " name="search-btn-pagination" title="Search">
                            <span class="fa fa-search"></span> Search</button>
                        <button class="btn btn-primary  " name="clear-search" title="Reset Search">
                            <span class="fa fa-trash-o"></span> Reset
                        </button>
                    </div>
                </div>
                <div class="card-title content-box-title add-space">
                    <div class="row">
                        <div class="col-md-12 col-xs-12 ">
                            <div class="pull-left">
                                <div class="addon-box-list">
                                    <div class="form-group-inline">
                                        <span class="addon">Show</span>
                                         <select class="form-control" name="size" title="Total List">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select>
                                        <span class="addon">Data</span>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right">
                                <div class="addon-box-list add-space-left  ">
                                    <div class="form-group-inline">
                                        <span class="addon ">Ordering by</span>
                                       <select class="form-control" title="Sorting Data" name="orderingBy">
                                           <option value="0" selected>Sorting By</option>
                                           @foreach($ordering as $key => $value)
                                               <option value="{{$key}}">{{$value}}</option>
                                           @endforeach
                                       </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
					<table id="penerimaanbarang" class="table table-hover table-striped widget-table"name="table-index">
						<thead>
						<tr>
							<!-- <th><input type="checkbox" name="selectAll"></th> -->
							<th>&nbsp;</th>
                            <th class="header-pagination">poNumber</th>
                            <th class="header-pagination">kapalId</th>
                            <th class="header-pagination">locationId</th>
                            <th class="header-pagination">userId</th>
                            <th class="header-pagination">receiptNumber</th>
                            <th class="header-pagination">receiptDate</th>
                            <th class="header-pagination">Voyages</th>
                            <th class="header-pagination">Date Start</th>
                            <th class="header-pagination">Date End</th>
						</tr>
						</thead>
						<tbody>
							@foreach($penerimaanbarangs["rows"]as $penerimaanbarang)
						<tr>
							<!-- <td><input type="checkbox" value="{{$penerimaanbarang->poNumber}}" name="checkbox-del[]" title="poNumber"></td> -->
							<td><a data-toggle="tooltip" data-placement="top" title="View" href="{{url("administrator/penerimaanbarang/$penerimaanbarang->poNumber")}}"class="btn btn-outline-primary"><span class="fa fa-eye"></span></a></td>
                            <td >{{$penerimaanbarang->poNumber}}</td>
                            <td>{{$penerimaanbarang->kapalId}}</td>
                            <td>{{$penerimaanbarang->locationId}}</td>
                            <td>{{$penerimaanbarang->userId}}</td>
                            <td>{{$penerimaanbarang->receiptNumber}}</td>
                            <td>{{$penerimaanbarang->receiptDate}}</td>
                            <td>{{$permintaanbarang->voyageName}}</td>
                            <td>{{$permintaanbarang->voyageStart}}</td>
                            <td>{{$permintaanbarang->voyageEnd}}</td>
						</tr>
							@endforeach
						</tbody>
					</table>

                </div>
                <div class=" content-box-footer">
                    <div class="row">
                        <div class="col-md-6 text-left col-xs-12">
                        Page {{$penerimaanbarangs["page"]}} of <span id="pageCount">{{$penerimaanbarangs["pageCount"]}}</span>
                        Total Row <span id="totalRow">{{$penerimaanbarangs["rowCount"]}}</span>
                        </div>
                        <div class="col-md-6  col-xs-12">
                           <ul class="pagination pull-right">
                               <li class="page-item ">
                                   <button class="page-link" tabindex="-1" name="first" title="First Page">First
                                   </button>
                               </li>
                               <li class="page-item" id="pagination-prev">
                                    <a class="page-link" title="Prev Page">
                                        <i class="fa fa-step-backward"></i></a>
                               </li>
                               <li class="page-item" id="pagination-next">
                                    <a class="page-link" title="Next Page">
                                        <i class="fa fa-step-forward"></i></a></li>
                               <li class="page-item">
                                   <button class="page-link" name="last" title="Last Page">Last</button>
                               </li>
                           </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes.includes_footer')
     <script>
             <?php $penerimaanbarangs["rows"] = []; ?>
            var paginationParameter = ('{!! json_encode($penerimaanbarangs)!!}');
            var currentUrl = "{{url($currentPrefix)}}";
        </script>
    <script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>
@endsection