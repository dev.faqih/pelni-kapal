@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">        
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Detail Penerimaan Barang</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb') </div>				
            </div>
		</div>
        <div class="clearfix">&nbsp;</div>
        <div class="container border rounded bg-light">
            	@include('includes.include_error_prop')		

			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#poNumber">No PO</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" readonly class="form-control input-default"  placeholder="Masukkan unit operasi" name="poNumber" value="{{ $popenerimaan->poNumber }}" title="poNumber">
				</div>
				<div class="col-sm-2 col-xs-5">
					<label for="#receiptNumber">No Receipt</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" class="form-control input-default" readonly placeholder="Masukkan unit operasi" name="receiptNumber" value="{{ $penerimaanbarang->receiptNumber }}" title="receiptNumber">
				</div>
			</div><br />

			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#unitOperasi">Unit Operasi</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text"  class="form-control input-default" readonly placeholder="Masukkan unit operasi" name="unitOperasi" value="{{ $popenerimaan->operatingUnit }}" title="unitOperasi">
				</div>

				<div class="col-sm-2 col-xs-5">
					<label for="#tanggalPO">Tanggal PO</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text"  class="form-control input-default" readonly placeholder="Masukkan unit operasi" name="tanggalPO" value="<?php 
					if($popenerimaan->date) { 
						echo date("Y-m-d", strtotime($popenerimaan->date)); 
					} ?>" title="tanggalPO">
				</div>
			</div><br />

			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#tanggalPenerimaan">Tanggal Diterima</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" readonly class="form-control input-default"  placeholder="Masukkan unit operasi" name="tanggalPenerimaan" value="{{ date("Y-m-d", strtotime($penerimaanbarang->tanggalPenerimaan) ) }}" title="tanggalPenerimaan">
				</div>
			</div><br />
			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#voyageName">Voyage</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" disabled class="form-control input-default" name="voyageName" value="{{ $penerimaanbarang->voyageName }}" title="voyageKode">
				</div>
				<div class="col-sm-2 col-xs-5">
					<label for="#voyageDate">Tanggal Voyage</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" disabled class="form-control input-default" name="voyageDate" value="<?php 
					if($penerimaanbarang->voyageStart) { 
						echo date("Y-m-d", strtotime($penerimaanbarang->voyageStart)); 
					} 
					if($penerimaanbarang->voyageStart && $penerimaanbarang->voyageEnd) { echo "  -  "; }
					if($penerimaanbarang->voyageEnd) { 
						echo date("Y-m-d", strtotime($penerimaanbarang->voyageEnd));
					 } ?>" title="voyageKode">
				</div>
			</div><br />

		</div>
        <div class="clearfix">&nbsp;</div>
        <div class="container border rounded bg-light">

			<div class="clearfix">&nbsp;</div>
			<div class="row justify-content-between">
				<div class="col-sm-4 col-xs-6"><h5>Daftar Barang</h5></div>
			</div><br />
			<div class="table-responsive">
				<table id="tableItem" class="table table-hover table-striped widget-table">
				<thead>	
					<tr>
						<th>No</th>
						<th>Kode Barang</th>
						<th>Part Number</th>
						<th>Deskripsi</th>
						<th>UOM</th>
						<th>Qty Penerimaan</th>
						<th>Sub-Inventory</th>
						<th>Lot Number</th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 1; ?>
					@foreach($listItems as $item)
					<tr>
					 	<td>{{ $i++ }}</td>
						<td>{{ $item->namaItem }}</td>
						<td>{{ $item->partNumber }}</td>
						<td>{{ $item->deskripsi }}</td>
						<td>{{ $item->UOM }}</td>
						<td>{{ floatval($item->quantity) }}</td>
						<td>{{ $item->namaLokasi }}</td>
						<td>{{ $item->lotNumber }}</td>
					</tr>
					@endforeach
				</tbody>
				</table>
			</div><br />
				
			<p class="text-center">
				<a class="btn btn-primary" href="{{url("administrator/popenerimaan/$popenerimaan->id")}}" title="Back"><span class="fa fa-undo"></span> Kembali</a> &nbsp;
				<a class="btn btn-primary" href="{{url("administrator/popenerimaan/reportBarangMasuk/".$penerimaanbarang->id)}}" target="_blank" title="Download PDF"><span class="fa fa-print"></span> Laporan Barang Masuk</a>	
			</p>	
        </div>
    </div>
    @include('includes.includes_footer')
@endsection