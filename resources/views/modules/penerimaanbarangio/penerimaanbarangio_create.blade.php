@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Penerimaan Barang IO</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
		</div>
		<div class="clearfix">&nbsp;</div>
		<form autocomplete="off" class="submitTransaction" action="{{url("administrator/popenerimaanio")}}" role="form" method="POST" enctype="multipart/form-data">
		<div class="container border rounded bg-light">				
			<div class="clearfix">&nbsp;</div>
			@include('includes.include_error_prop')			
			<input type="hidden" name="_method" value="POST">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="userId" value="{{ WorkerAuth::auth()->getAuth()->id }}">
			<input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">
			<input type="hidden" name="kodeKapal" value="{{ WorkerAuth::auth()->getAuth()->kodeKapal }}">
			<input type="hidden" name="status" value="0">
			<input type="hidden" name="qtyDiterima" value="0">
			<input type="hidden" name="rcID" value="{{ $popenerimaanio->id }}">
			<input type="hidden" name="jabatan" value="{{ WorkerAuth::auth()->getAuth()->jabatan }}">
			<input type="hidden" name="organization_id" value="{{ $popenerimaanio->fromKapalId }}">
			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#ioNumber">No PO</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" readonly class="form-control input-default"  name="ioNumber" value="{{ $popenerimaanio->ioNumber }}" title="ioNumber">
				</div>
				<div class="col-sm-2 col-xs-5">
					<label for="#receiptNumber">No Receipt</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" class="form-control input-default" readonly name="receiptNumber" value="xxx" title="receiptNumber">
				</div>
			</div><br />

			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#unitOperasi">Unit Operasi</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text"  class="form-control input-default" readonly name="unitOperasi" value="{{ $popenerimaanio->operatingUnit }}" title="unitOperasi">
				</div>

				<div class="col-sm-2 col-xs-5">
					<label for="#tanggalPO">Tanggal PO</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text"  class="form-control input-default" readonly name="tanggalPO" value="<?php if($popenerimaanio->date) { echo date("Y-m-d", strtotime($popenerimaanio->date)); } ?>" title="tanggalPO">
				</div>
			</div><br />

			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#tanggalPenerimaan">Tanggal Diterima</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text"  class="form-control input-default datepicker" data-date-clear-btn="true" required placeholder="Masukkan tanggal penerimaan" id="tanggalPenerimaan" name="tanggalPenerimaan" data-date-format="yyyy-mm-dd" data-date-autoclose="true" title="tanggalPenerimaan" onChange="checkIsSmallerThanPODate('tanggalPenerimaan','tanggalPO');checkIsMoreThan3Days('tanggalPenerimaan', '{{ date("Y-m-d H:i:s") }}','{{ WorkerAuth::auth()->getAuth()->sysParams->BACKDATE }}');loadVoyage('{{ url('administrator/popenerimaan/getVoyages') }}', 'tanggalPenerimaan');">
				</div>
			</div><br />
			<div class="row">
				<div class="col-sm-2 col-xs-5">Voyage</div>
				<div class="col-sm-4 col-xs-7">
					<select class="form-control input-default" required name="voyageKode" title="Voyage">
					</select>
				</div>
				<div class="col-sm-2 col-xs-5">Tanggal Voyage</div>
				<div class="col-sm-4 col-xs-7 input-group">
					<input readonly class=" form-control input-default search-value" required name="voyageStart" >
					<div class="input-group-addon"> &nbsp; s/d &nbsp; </div>
					<input readonly class=" form-control input-default search-value" required name="voyageEnd" >
				</div>
			</div><br />
		</div>
		<div class="clearfix">&nbsp;</div>
		<div class="container border rounded bg-light">
			<div class="clearfix">&nbsp;</div>
			<div class="row justify-content-between">
				<div class="col-sm-4 col-xs-6"><h5>Daftar Barang</h5></div>
				<div class="col-sm-3 text-right col-xs-5">
					<button type="button" id="loadItemModal" class="btn btn-primary">Tambah Item</button>
				</div>
			</div><br />
			<div class="table-responsive">
				<table id="tableItem" class="table table-hover table-striped widget-table">
				<thead>
					<tr>
						<th>No</th>
						<th>Kode Barang</th>
						<th>Part Number</th>
						<th>Deskripsi</th>
						<th>UOM</th>
						<th>Qty Penerimaan</th>
						<th>Sub-Inventory</th>
						<th>Lot Number</th>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody></tbody>
				</table>
			</div><br />

			<p class="text-center">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Kirim</button> &nbsp;
				<a class="btn btn-primary" href="{{url("administrator/popenerimaanio/$ioNumber")}}" title="Back"><span class="fa fa-undo"></span> Kembali</a>
			</p>
			
		</div>
		</form>
    </div>
    @include('includes.includes_footer')
	@include('includes.include_modal_penerimaanbarangio')
    <script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>
	<script>
        $(document).ready(function() {
            $('.datepicker').datepicker('update', '{{ date('Y-m-d') }}');
			loadVoyage('{{ url('administrator/popenerimaanio/getVoyages') }}', 'tanggalPenerimaan');
		});
		
		$("select[name='voyageKode']").change(function() {
			loadVoyageDate('{{ url('administrator/permintaanbarang/getVoyageDate') }}', $(this).val() );
		});
    </script>
@endsection