@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        @include('includes.include_breadcrumb')
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Edit data of{{$title}}</h4></div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{url("administrator/penerimaanbarang")}}" title="Back"><span class="fa fa-undo"></span> Back</a>
                </div>
            </div>
            <div class="card-body content-box-body">
            @include('includes.include_error_prop')

                <form role="form" action="{{url("administrator/penerimaanbarang/$penerimaanbarang->poNumber/update")}}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="form-group">
					<label for="#kapalId">Kapal</label>
					<select class="form-control input-default"  name="kapalId" title="kapalId">
						@foreach($kapals as $kapal)
							<option value="{{$kapal->id}}"@if($penerimaanbarang->kapalId== $kapal->id) selected @endif>
								{{ $kapal->namaKapal }}</option>
						@endforeach
					</select>
					</div>

					<div class="form-group">
					<label for="#locationId">Location</label>
					<select class="form-control input-default"  name="locationId" title="locationId">
						@foreach($lokasiitems as $lokasiitem)
							<option value="{{$lokasiitem->id}}"@if($penerimaanbarang->locationId== $lokasiitem->id) selected @endif>
								{{ $lokasiitem->namaLokasi }}</option>
						@endforeach
					</select>
					</div>

					<div class="form-group">
					<label for="#userId">UserId</label>
					<select class="form-control input-default"  name="userId" title="userId">
						@foreach($userss as $users)
							<option value="{{$users->id}}"@if($penerimaanbarang->userId== $users->id) selected @endif>
								{{ $users->name }}</option>
						@endforeach
					</select>
					</div>

					<div class="form-group">
					<label for="#receiptNumber">ReceiptNumber</label>
					<input type="text"  class="form-control input-default "  placeholder="Masukkan receiptNumber" name="receiptNumber" value="{{$penerimaanbarang->receiptNumber}}" title="receiptNumber">
					</div>

					<div class="form-group">
					<label for="#receiptDate">ReceiptDate</label>
					<input type="text"  class="form-control input-default "  placeholder="Masukkan receiptDate" name="receiptDate" value="{{$penerimaanbarang->receiptDate}}" title="receiptDate">
					</div>

                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                </form>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
@endsection