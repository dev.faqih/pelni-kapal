@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
				<div class="pull-left"><h4>Detail Penerimaan Makanan</h4></div>
				<div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
		</div>
		<div class="clearfix">&nbsp;</div>				
		<div class="container border rounded bg-light">
			@include('includes.include_error_prop')
			<div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#operatingUnit">Tanggal Transaksi</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default" required placeholder="Masukkan unit operasi" name="date" value="{{ $penerimaanmakanan->date }}" title="date">
					</div>
					<div class="col-sm-2 col-xs-5">
						<label for="#requesterId">Account</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default" required placeholder="Masukkan nama account" name="namaAccount" value="{{ $penerimaanmakanan->namaAccount }}" title="date">
					</div>
				</div><br />
				<div class="row">
					<div class="col-sm-2 col-xs-5">Voyage</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default" required placeholder="Masukkan nama voyage" name="voyageName" value="{{ $penerimaanmakanan->voyageName }}" title="date">					
					</div>
					<div class="col-sm-2 col-xs-5">Tanggal Voyage</div>
					<div class="col-sm-4 col-xs-7">
						<input class="form-control input-default" disabled name="voyageTgl" data-autoclose="true" value="<?php 
						if($penerimaanmakanan->voyageStart) { 
							echo date("Y-m-d", strtotime($penerimaanmakanan->voyageStart)); 
						} 
						if($penerimaanmakanan->voyageStart && $penerimaanmakanan->voyageEnd) { echo " - "; }
						if($penerimaanmakanan->voyageEnd) { 
							echo date("Y-m-d", strtotime($penerimaanmakanan->voyageEnd));
						} ?>">
					</div>
				</div><br />
				<div class="clearfix">&nbsp;</div>
			</div>
			<div class="clearfix">&nbsp;</div>				
			<div class="container border rounded bg-light">
				<div class="clearfix">&nbsp;</div>
				<div class="row justify-content-between">
					<div class="col-sm-4 col-xs-6"><h5>Daftar Penerimaan Makanan</h5></div>
				</div><br />
				<div class="table-responsive">
					<table id="tableItem" class="table table-hover table-striped widget-table">
						<tr>
							<th>Barang</th>
							<th>Kategori</th>
							<th>Deskripsi</th>
							<th>UOM</th>
							<th>Qty</th>
							<th>Lot Number</th>
							<th>Sub-Inventory</th>
						</tr>
						@foreach($listItems as $item)
						<tr class="" >
							<td>{{ $item->namaItem }}</td>
							<td>{{ $item->category }}</td>
							<td>{{ $item->deskripsi }}</td>
							<td>{{ $item->UOM }}</td>
							<td>{{ $item->quantity }}</td>
							<td>{{ $item->lotNumber }}</td>
							<td>{{ $item->namaLokasi }}</td>
						</tr>
						@endforeach
					</table>
				</div><br />


				<div class="clearfix">&nbsp;</div>


				<div class="clearfix">&nbsp;</div>
			</div>
			<div class="clearfix">&nbsp;</div>				
			<div class="container border rounded bg-light">
				<div class="clearfix">&nbsp;</div>
				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<h5>Komentar</h5>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-xs-12">
					<textarea disabled class="form-control input-default" placeholder="Masukkan komentar" required name="komentar" title="komentar">{{ $penerimaanmakanan->komentar }}</textarea>
					</div>
				</div>		
				<div class="clearfix">&nbsp;</div>
				<div class="row">
					<div class="col-sm-6 col-xs-6 text-right">
						<form action="{{url("administrator/penerimaanmakanan/approval/")}}" role="form" method="POST" enctype="multipart/form-data">
							<input type="hidden" name="_method" value="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="mrNumber" value="{{ $penerimaanmakanan->mrNumber }}">
							<input type="hidden" name="status" value="1">		
							<input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">
							<input type="hidden" name="approverId" value="{{ WorkerAuth::auth()->getAuth()->id }}">				
							<button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Setujui</button> &nbsp;					
						</form>
					</div>
					<div class="col-sm-6 col-xs-6">
						<form action="{{url("administrator/penerimaanmakanan/approval/")}}" role="form" method="POST" enctype="multipart/form-data">
							<input type="hidden" name="_method" value="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="mrNumber" value="{{ $penerimaanmakanan->mrNumber }}">
							<input type="hidden" name="status" value="2">	
							<input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">		
							<input type="hidden" name="approverId" value="{{ WorkerAuth::auth()->getAuth()->id }}">
							<button type="submit" class="btn btn-primary"><i class="fa fa-close"></i> Tolak</button> &nbsp;	 &nbsp;		
							<a class="btn btn-primary" href="{{ URL::previous() }}" title="Back"><span class="fa fa-undo"></span> Kembali</a>		
						</form>
					</div>
				</div>
				<div class="clearfix">&nbsp;</div>		

            </div>

        </div>
    </div>
    @include('includes.includes_footer')
@endsection