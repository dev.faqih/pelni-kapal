@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
				<div class="pull-left"><h4>Edit data of{{$title}}</h4></div>
				<div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
            <div class="card-body content-box-body">
				<div class="row">
                    <div class="col-sm-12 text-right">
						<a class="btn btn-primary" href="{{url("administrator/penerimaanmakanan")}}" title="Back"><span class="fa fa-undo"></span> Back</a>
                    </div>
				</div>
				<div class="clearfix">&nbsp;</div>
           		@include('includes.include_error_prop')

                <form role="form" action="{{url("administrator/penerimaanmakanan/$penerimaanmakanan->id/update")}}" method="POST">
					<input type="hidden" name="_method" value="PUT">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					
					<div class="row">
						<div class="col-sm-2 col-xs-5">
							<label for="#locationId">Location</label>
						</div>
						<div class="col-sm-4 col-xs-7">
							<select class="form-control input-default" name="locationId" title="locationId">
								@foreach($lokasiitems as $lokasiitem)
									<option value="{{$lokasiitem->id}}" @if($penerimaanmakanan->locationId== $lokasiitem->id) selected @endif>{{$lokasiitem->namaLokasi}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-sm-2 col-xs-5">
							<label for="#locationId">Tgl Transaksi</label>
						</div>
						<div class="col-sm-4 col-xs-7"></div>
					</div> <br />

					<div class="row">
						<div class="col-sm-2 col-xs-5">
							<label for="#userId">Tujuan Account</label>
						</div>
						<div class="col-sm-4 col-xs-7"></div>
					</div> <br />

					<div class="row justify-content-between">
						<div class="col-4"><h5>Daftar Makanan</h5></div>
						<div class="col-sm-3 text-right col-xs-5">
							<button type="button" id="loadItemModal" class="btn btn-primary">Add Item</button>
						</div>
					</div> 
					<br />
					<div class="table-responsive">
						<table id="tableItem" class="table table-hover table-striped widget-table">
							<tr>
								<th>Item</th>
								<th>Qty</th>
								<th>Lot Number</th>
								<th>&nbsp;</th>
							</tr>
						</table>
					</div>


                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                </form>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
	@include('includes.include_modal_penerimaanmakanan')
	<script>
		@foreach($listItems as $item)
			printItemIntoList( "{{ $item->id }}", "{{ $item->namaItem }}", "{{ $item->quantity }}", "{{ $item->lotNumber }}");
		@endforeach
	</script>
    <script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>
@endsection