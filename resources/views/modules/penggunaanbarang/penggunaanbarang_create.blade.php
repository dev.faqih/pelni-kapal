@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Buat Penggunaan Barang</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
		</div>
		<div class="clearfix">&nbsp;</div>
		
		<form autocomplete="off" class="submitTransaction" id="formPenggunaanBarang" action="{{url("administrator/penggunaanbarang")}}" role="form" method="POST" enctype="multipart/form-data">

		<div class="container border rounded bg-light">
			<div class="clearfix">&nbsp;</div>
			@include('includes.include_error_prop')
				
			<input type="hidden" name="_method" value="POST">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="requesterId" value="{{ WorkerAuth::auth()->getAuth()->id }}">
			<input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">
			<input type="hidden" name="kodeKapal" value="{{ WorkerAuth::auth()->getAuth()->kodeKapal }}">
			<input type="hidden" name="jabatan" value="{{ WorkerAuth::auth()->getAuth()->jabatan }}">
			<div class="row">
				<div class="col-sm-2 col-xs-5"><label for="#locationId">SubInventory Asal</label></div>
				<div class="col-sm-4 col-xs-7">
					<select class="form-control input-default" id="locationId" name="locationId" title="Sub Inventory">
						@foreach($lokasiitems as $lokasiitem)
							<option value="{{$lokasiitem->id}}">{{$lokasiitem->namaLokasi}}</option>
						@endforeach
					</select>
				</div>
				<div class="col-sm-2 col-xs-5">
					<label for="#locationId">Tanggal Penggunaan</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" class="form-control datepicker input-default" id="tglPenggunaan" required name="tglPenggunaan" data-date-clear-btn="true" data-date-format="yyyy-mm-dd" data-date-autoclose="true" title="tglPenggunaan" onChange="checkIsMoreThan3Days('tglPenggunaan', '{{ date("Y-m-d H:i:s") }}','{{ WorkerAuth::auth()->getAuth()->sysParams->BACKDATE }}');loadVoyage('{{ url('administrator/penggunaanbarang/getVoyages') }}', 'tglPenggunaan');">
				</div>
			</div> <br />

			<div class="row">
				<div class="col-sm-2 col-xs-5">Voyage</div>
				<div class="col-sm-4 col-xs-7">
					<select class="form-control input-default" required name="voyageKode" title="Voyage">
					</select>
				</div>
				<div class="col-sm-2 col-xs-5">Tanggal Voyage</div>
				<div class="col-sm-4 col-xs-7 input-group">
					<input readonly class=" form-control input-default search-value" required name="voyageStart" >
					<div class="input-group-addon"> &nbsp; s/d &nbsp; </div>
					<input readonly class=" form-control input-default search-value" required name="voyageEnd" >
				</div>
			</div><br />
		</div>
		<div class="clearfix">&nbsp;</div>
		<div class="container border rounded bg-light">
			<div class="clearfix">&nbsp;</div>
			<div class="row justify-content-between">
				<div class="col-4"><h5>Daftar Barang</h5></div>
				<div class="col-sm-3 text-right col-xs-5">
					<button type="button" id="loadItemModal" class="btn btn-primary">Tambah Barang</button>
				</div>
			</div><br />	
						
			<div class="table-responsive">
				<table id="tableItem" class="table table-hover table-striped widget-table">
					<thead><tr>
						<th>No</th>
						<th>Kode Barang</th>
						<th>Part Number</th>
						<th>Deskripsi</th>
						<th>UOM</th>
						<th>Qty</th>
						<th>Lot Number</th>
						<!-- <th>Sub-Inventory</th> -->
						<th>&nbsp;</th>
					</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
					

			<div class="clearfix">&nbsp;</div>
			<p class="text-center" >
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Kirim</button> &nbsp;
				<a class="btn btn-primary" href="{{ URL::previous() }}" title="Back"><span class="fa fa-undo"></span> Kembali</a>
			</p>			
		</div>
		</form>
	</div>


    
	@include('includes.includes_footer')	
	@include('includes.include_modal_penggunaanbarang')
    <script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>


	<script>
		var valueBeforeChange = 0;
		$("#locationId").on('focus', function () {
			valueBeforeChange = this.value;
		}).change(function() {			
			if(itemList.length > 0) {
				$("#defModalTitle").html("Ubah Sub-Inventory asal ?");
				$("#defModalBody").html("Dengan mengubah sub-inventory asal, akan menghapus semua item yang sudah ada di list.<br />&nbsp;<br /> Apakah anda yakin ingin mengubah sub-inventory asal?");
				$("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" onClick=\"rollBackModal()\">Cancel</button><button type=\"button\" onClick=\"reLoadDropDown()\" class=\"btn btn-primary\">Yes</button>");
				$('#defModal').modal('show');
			} else {
				reLoadDropDown();
			}
		});

		var rollBackModal = function(){
			$("#locationId").val(valueBeforeChange);
			$('#defModal').modal('hide');
		}

		var reLoadDropDown = function() {
			$("#tableItem tbody").html("");
			$('#defModal').modal('hide');
			itemList = [];
		};

		$(document).ready(function() {
			$('.datepicker').datepicker('update', '{{ date('Y-m-d') }}');
			loadVoyage('{{ url('administrator/penggunaanbarang/getVoyages') }}', 'tglPenggunaan');
		});

		$("select[name='voyageKode']").change(function() {
			loadVoyageDate('{{ url('administrator/permintaanbarang/getVoyageDate') }}', $(this).val() );
		});
	</script>
@endsection