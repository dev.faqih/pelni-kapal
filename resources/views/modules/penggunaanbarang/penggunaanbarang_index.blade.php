@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Daftar Penggunaan Barang</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
        </div>
		<div class="clearfix">&nbsp;</div>
		<div class="container border rounded bg-light">
            <div class="clearfix">&nbsp;</div>
            @if(WorkerAuth::auth()->getAuth()->aplikasi == "KAPAL")
            <div class="row">
                <div class="col-sm-12 text-right">
                    <a class="btn btn-danger" href="{{url("administrator/penggunaanbarang/create")}}" title="Create Data"><span class="fa fa-plus"></span> Penggunaan Baru </a>
                    <!-- <button class="btn btn-primary" title="Delete Data" name="delete-content"><span class="fa fa-trash"></span> Trash</button> -->
                    <!-- <button class="btn btn-info" title="Export Data"><span class="fa fa-file-excel-o"></span> Export</button> -->
                </div>
            </div>
            <div class="clearfix">&nbsp;</div>
            @endif
            @include('includes.include_error_prop')
            <div class="row">
                <div class="col-sm-2 col-xs-12">No. Penggunaan</div>
                <div class="col-sm-3 col-xs-12"><input class="form-control input-default search-value" name="miNumber" title=""></div>
                <div class="col-sm-2 col-xs-12">Tanggal Penggunaan</div>
                <div class="col-sm-3 col-xs-12"><input class="form-control datepicker input-default search-value" data-date-clear-btn="true"  data-date-format="yyyy-mm-dd" data-date-autoclose="true" name="tglPenggunaan" title=""></div>
            </div><br />
            <div class="row">
                <div class="col-sm-2 col-xs-12">Barang <br /><span style="color:grey;font-size:smaller;">(Kode/Nama/Part No.)</span></div>
                <div class="col-sm-3 col-xs-12"><input class="form-control input-default search-value" name="namaItem" title="Kode Barang"></div>

                @if(WorkerAuth::auth()->getAuth()->aplikasi == "KAPAL")
                    <div class="col-sm-2 col-xs-12">Voyage</div>
                    <div class="col-sm-3 col-xs-12">
                        <select class="form-control select2 search-value" name="voyageKode">
                            <option value=""></option>
                            @foreach($voyages as $voyage)
                                <option value="{{ $voyage->voyageKode }}">{{ $voyage->voyageName . " - " . $voyage->tahun }} </option>
                            @endforeach
                        </select >
                    </div>
                @endif
            </div><br />
            <div class="row">
                <div class="col-sm-2">&nbsp;</div>
                <div class="col-sm-10 col-xs-12">
                    <button type="button" class="btn btn-primary " onClick="searchList()" name="search-btn-pagination" title="Search"><span class="fa fa-search"></span> Search</button> &nbsp;
                    <button type="button" class="btn btn-primary  " name="clear-search" title="Reset Search"><span class="fa fa-trash-o"></span> Reset</button>
                </div>
            </div><br />
        </div>
        <div class="clearfix">&nbsp;</div>
        <div class="container border rounded bg-light">
            <div class="card-title content-box-title add-space">
                <div class="row">
                    <div class="col-md-12 col-xs-12 ">
                        <div class="pull-left">
                            <div class="addon-box-list">
                                <div class="form-group-inline">
                                    <span class="addon">Show</span>
                                        <select class="form-control" name="size" title="Total List">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span class="addon">Data</span>
                                </div>
                            </div>
                        </div>
                        <div class="pull-right">
                            <div class="addon-box-list add-space-left  ">
                                <div class="form-group-inline">
                                    <span class="addon ">Ordering by</span>
                                    <select class="form-control" title="Sorting Data" name="orderingBy">
                                        <option value="0" selected>Sorting By</option>
                                        @foreach($ordering as $key => $value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table id="penggunaanbarang" class="table table-hover table-striped widget-table"name="table-index">
                    <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th class="header-pagination">No Penggunaan</th>
                        <th class="header-pagination">Tanggal Penggunaan</th>
                        <th class="header-pagination">Asal</th>
                        <th class="header-pagination">Voyages</th>
                        <th class="header-pagination">Total Barang</th>
                        <th class="header-pagination">Deskripsi Barang</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($penggunaanbarangs["rows"] as $penggunaanbarang)
                    <tr>
                        <!-- <td><input type="checkbox" value="{{$penggunaanbarang->id}}" name="checkbox-del[]" title="id"></td> -->
                        <td><a data-toggle="tooltip" data-placement="top" title="View" href="{{url("administrator/penggunaanbarang/$penggunaanbarang->id")}}"class="btn btn-outline-primary"><span class="fa fa-eye"></span></a></td>
                        <td>{{ $penggunaanbarang->miNumber }}</td>
                        <td class="text-center" ><?php if($penggunaanbarang->tglPenggunaan) { echo date("Y-m-d", strtotime($penggunaanbarang->tglPenggunaan)); } ?></td>
                        <td>{{ $penggunaanbarang->namaLokasi }}</td>
                        <td>{{ $penggunaanbarang->voyageName }}</td>
                        <td class="text-center" >{{ floatval($penggunaanbarang->jlhBarang) }}</td>
                        <td>@if($penggunaanbarang->item) {{ $penggunaanbarang->item->deskripsi }} @endif</td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
            <div class=" content-box-footer">
                <div class="row">
                    <div class="col-md-6 text-left col-xs-12">
                    Page {{$penggunaanbarangs["page"]}} of <span id="pageCount">{{$penggunaanbarangs["pageCount"]}}</span>
                    Total Row <span id="totalRow">{{$penggunaanbarangs["rowCount"]}}</span>
                    </div>
                    <div class="col-md-6  col-xs-12">
                        <ul class="pagination pull-right">
                            <li class="page-item ">
                                <button class="page-link" tabindex="-1" name="first" title="First Page">First
                                </button>
                            </li>
                            <li class="page-item" id="pagination-prev">
                                <a class="page-link" title="Prev Page">
                                    <i class="fa fa-step-backward"></i></a>
                            </li>
                            <li class="page-item" id="pagination-next">
                                <a class="page-link" title="Next Page">
                                    <i class="fa fa-step-forward"></i></a></li>
                            <li class="page-item">
                                <button class="page-link" name="last" title="Last Page">Last</button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes.includes_footer')
    <script>
            <?php $penggunaanbarangs["rows"] = []; ?>
        var paginationParameter = ('{!! json_encode($penggunaanbarangs)!!}');
        var currentUrl = "{{url($currentPrefix)}}";
    </script>
    <script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>
@endsection