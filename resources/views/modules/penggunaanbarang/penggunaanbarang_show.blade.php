@extends('app')
@section('content')
@include('includes.include_navigation')
<div class="container content">
	<div class="card content-box">
		<div class="card-header">
			<div class="pull-left">
				<h4>Penggunaan Barang Detail</h4>
			</div>
			<div class="pull-right">@include('includes.include_breadcrumb')</div>
		</div>
	</div>
	<div class="clearfix">&nbsp;</div>
	<div class="container border rounded bg-light">
		<div class="clearfix">&nbsp;</div>
		@include('includes.include_error_prop')
		<div class="row">
			<div class="col-sm-2 col-xs-5">
				<label for="#locationId">No Penggunaan</label>
			</div>
			<div class="col-sm-4 col-xs-7">
				<input type="text" disabled class="form-control input-default" readonly placeholder="" name="locationId" value="{{ $penggunaanbarang->miNumber }}" title="">
			</div>
			<div class="col-sm-2 col-xs-5">
				<label for="#locationId">Tempat Asal</label>
			</div>
			<div class="col-sm-4 col-xs-7">
				<input type="text" disabled class="form-control input-default" readonly placeholder="" name="locationId" value="{{ $penggunaanbarang->namaLokasi }}" title="">
			</div>

		</div> <br />
		<div class="row">
			<div class="col-sm-2 col-xs-5">
				<label for="#locationId">Tanggal Penggunaan</label>
			</div>
			<div class="col-sm-4 col-xs-7">
				<input type="text" disabled class="form-control input-default" readonly placeholder="" name="tglPenggunaan" value="{{ date("Y-m-d", strtotime($penggunaanbarang->tglPenggunaan) ) }}" title="tglPenggunaan">
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-sm-2 col-xs-5">
				<label for="#voyageName">Voyage</label>
			</div>
			<div class="col-sm-4 col-xs-7">
				<input type="text" disabled class="form-control input-default" name="voyageName" value="{{ $penggunaanbarang->voyageName }}" title="voyageKode">
			</div>
			<div class="col-sm-2 col-xs-5">
				<label for="#voyageDate">Tanggal Voyage</label>
			</div>
			<div class="col-sm-4 col-xs-7">
				<input type="text" disabled class="form-control input-default" name="voyageDate" value="<?php
																										if ($penggunaanbarang->voyageStart) {
																											echo date("Y-m-d", strtotime($penggunaanbarang->voyageStart));
																										}
																										if ($penggunaanbarang->voyageStart && $penggunaanbarang->voyageEnd) {
																											echo " - ";
																										}
																										if ($penggunaanbarang->voyageEnd) {
																											echo date("Y-m-d", strtotime($penggunaanbarang->voyageEnd));
																										} ?>" title="voyageKode">
			</div>
		</div><br />
	</div>
	<div class="clearfix">&nbsp;</div>
	<div class="container border rounded bg-light">
		<div class="clearfix">&nbsp;</div>
		<div class="row justify-content-between">
			<div class="col-4">
				<h5>Daftar Barang</h5>
			</div>
		</div>
		<br />
		<div class="table-responsive">
			<table id="tableItem" class="table table-hover table-striped widget-table">
				<thead>
					<tr>
						<th>No</th>
						<th>Kode Barang</th>
						<th>Part Number</th>
						<th>Deskripsi</th>
						<th>UOM</th>
						<th>Qty</th>
						<th>Lot Number</th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 1; ?>
					@foreach($listItems as $item)
					<tr>
						<td>{{ $i++ }}</td>
						<td>{{ $item->namaItem }}</td>
						<td>{{ $item->partNumber }}</td>
						<td>{{ $item->deskripsi }}</td>
						<td>{{ $item->UOM }}</td>
						<td>{{ floatval($item->quantity) }}</td>
						<td>{{ $item->lotNumber }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			</table>
		</div>

		<div class="clearfix">&nbsp;</div>
		<p class="text-center">
			<a class="btn btn-primary" href="{{ URL::previous() }}" title="Back"><span class="fa fa-undo"></span> Kembali</a>
		</p>
	</div>
</div>
@include('includes.includes_footer')
@endsection