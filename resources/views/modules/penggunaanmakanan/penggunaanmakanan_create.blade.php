@extends('app')
@section('content')
@include('includes.include_navigation')
<div class="container content">
	<div class="card content-box">
		<div class="card-header">
			<div class="pull-left">
				<h4>Buat Penggunaan Makanan</h4>
			</div>
			<div class="pull-right">@include('includes.include_breadcrumb')</div>
		</div>
	</div>
	<div class="clearfix">&nbsp;</div>
	<form autocomplete="off" class="submitTransaction" action="{{url("administrator/penggunaanmakanan")}}" role="form" method="POST" enctype="multipart/form-data">

		<div class="container border rounded bg-light">
			<div class="clearfix">&nbsp;</div>
			@include('includes.include_error_prop')
			<input type="hidden" name="_method" value="POST">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="tglTransaksi" value="{{ date("Y-m-d H:i:s") }}">
			<input type="hidden" name="status" value="0">
			<input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">
			<input type="hidden" name="kodeKapal" value="{{ WorkerAuth::auth()->getAuth()->kodeKapal }}">
			<input type="hidden" name="requesterId" value="{{ WorkerAuth::auth()->getAuth()->id }}">
			<input type="hidden" name="tipeTransaksi" value="MS">
			<input type="hidden" name="urlTransaksi" value="penggunaanmakanan">
			<input type="hidden" name="namaTransaksi" value="Penggunaan Makanan">
			<input type="hidden" name="accountId" value="1">
			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#operatingUnit">Tanggal Transaksi</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" class="form-control datepicker input-default" data-date-clear-btn="true" required placeholder="Masukkan tanggal transaksi" id="date" name="date" data-date-format="yyyy-mm-dd" data-date-autoclose="true" value="" title="date" onChange="checkIsMoreThan3Days('date', '{{ date("Y-m-d H:i:s") }}','{{ WorkerAuth::auth()->getAuth()->sysParams->BACKDATE }}');loadVoyage('{{ url('administrator/penggunaanmakanan/getVoyages') }}', 'date');">
				</div>
				<!-- 
				<div class="col-sm-2 col-xs-5">
					<label for="#requesterId">Account</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<select class="form-control input-default" required name="accountId"></select>
				</div>
				 -->
			</div><br />
			<div class="row">
				<div class="col-sm-2 col-xs-5">Voyage</div>
				<div class="col-sm-4 col-xs-7">
					<select class="form-control input-default" required name="voyageKode" title="Voyage">
					</select>
				</div>
				<div class="col-sm-2 col-xs-5">Tanggal Voyage</div>
				<div class="col-sm-4 col-xs-7 input-group">
					<input readonly class=" form-control input-default search-value" required name="voyageStart">
					<div class="input-group-addon"> &nbsp; s/d &nbsp; </div>
					<input readonly class=" form-control input-default search-value" required name="voyageEnd">
				</div>
			</div><br />
		</div>
		<div class="clearfix">&nbsp;</div>
		<div class="container border rounded bg-light">
			<div class="clearfix">&nbsp;</div>
			<div class="row justify-content-between">
				<div class="col-sm-4 col-xs-6">
					<h5>Daftar Barang</h5>
				</div>
				<div class="col-sm-3 text-right col-xs-5">
					<button type="button" id="loadItemModal" class="btn btn-primary">Tambah Item</button>
				</div>
			</div><br />
			<div class="table-responsive">
				<table id="tableItem" class="table table-hover table-striped widget-table">
					<thead>
						<tr>
							<th>No</th>
							<th>Barang</th>
							<th>Deskripsi</th>
							<th>Kategori</th>
							<th>UOM</th>
							<th>Qty</th>
							<th>Lot Number</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div><br />


			<div class="clearfix">&nbsp;</div>


			<div class="clearfix">&nbsp;</div>
		</div>
		<div class="clearfix">&nbsp;</div>
		<div class="container border rounded bg-light">
			<div class="clearfix">&nbsp;</div>

			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<h5>Komentar</h5>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-xs-12">
					<textarea class="form-control input-default" placeholder="Masukkan komentar" required name="komentar" title="komentar">{{old("komentar")}}</textarea>
				</div>
			</div>
			<div class="clearfix">&nbsp;</div>
			<div class="row">
				<div class="col-sm-12 col-xs-12">
					<label><span><input class="form-input" type="checkbox" value="" id="invalidCheck" required> &nbsp; </span><span>Saya setuju untuk melanjutkan proses ini.</span></label>
				</div>
			</div><br />
			<p class="text-center">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Kirim</button> &nbsp;
				<a class="btn btn-primary" href="{{ URL::previous() }}" title="Back"><span class="fa fa-undo"></span> Kembali</a>
			</p>

		</div>
	</form>
</div>



@include('includes.includes_footer')
@include('includes.include_modal_penggunaanmakanan')
<script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>
<script>
	$(document).ready(function() {
		$('.datepicker').datepicker("update", "{{ date('Y-m-d') }}");
		loadVoyage("{{ url('administrator/penggunaanmakanan/getVoyages') }}", "date");
	});

	$("select[name='voyageKode']").change(function() {
		loadVoyageDate("{{ url('administrator/permintaanbarang/getVoyageDate') }}", $(this).val());
	});
</script>
@endsection