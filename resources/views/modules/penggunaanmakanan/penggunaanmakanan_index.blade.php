@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Penggunaan Makanan</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
        </div>
		<div class="clearfix">&nbsp;</div>
		<div class="container border rounded bg-light">
            <div class="clearfix">&nbsp;</div>
            @if(WorkerAuth::auth()->getAuth()->aplikasi == "KAPAL")
            <div class="row">
                <div class="col-sm-12 text-right">
                    <a class="btn btn-danger" href="{{url("administrator/penggunaanmakanan/create")}}" title="Create Data"><span class="fa fa-plus"></span> Penggunaan Makanan Baru </a>
                </div>
            </div>
            <div class="clearfix">&nbsp;</div>
            @endif
            @include('includes.include_error_prop')
            <div class="row">
                <div class="col-sm-2 col-xs-12">No. Transaksi</div>
                <div class="col-sm-3 col-xs-12"><input class="form-control input-default search-value" name="msNumber" title="No Transaksi"></div>
                <div class="col-sm-2 col-xs-12">Tanggal Transaksi</div>
                <div class="col-sm-3 col-xs-12"><input class="form-control input-default datepicker search-value" data-date-clear-btn="true"  data-date-format="yyyy-mm-dd" data-date-autoclose="true" name="date" title="Tanggal Transaksi"></div>
            </div><br />
            <div class="row">
                <div class="col-sm-2 col-xs-12">Status</div>
                <div class="col-sm-3 col-xs-12">
                    <select class="form-control input-default search-value" name="status">
                        <option value=""></option>
                        <option value="0">Open</option>
                        <option value="1">Approved</option>
                        <option value="2">Rejected</option>                       
                    </select>    
                </div>
                @if(WorkerAuth::auth()->getAuth()->aplikasi == "KAPAL")
                <div class="col-sm-2 col-xs-12">Voyage</div>
                <div class="col-sm-3 col-xs-12">
                    <select class="form-control select2 search-value" name="voyageKode">
                        <option value=""></option>
                        @foreach($voyages as $voyage)
                            <option value="{{ $voyage->voyageKode }}">{{ $voyage->voyageName . " - " . $voyage->tahun }} </option>
                        @endforeach
                    </select >
                </div>
                @endif

            </div><br />
            <div class="row">
                <div class="col-sm-2">&nbsp;</div>
                <div class="col-sm-10 col-xs-12">
                    <button type="button" class="btn btn-primary " onClick="searchList()" name="search-btn-pagination" title="Search"><span class="fa fa-search"></span> Search</button> &nbsp;
                    <button type="button" class="btn btn-primary  " name="clear-search" title="Reset Search"><span class="fa fa-trash-o"></span> Reset</button>
                </div>
            </div><br />
        </div>
		<div class="clearfix">&nbsp;</div>
		<div class="container border rounded bg-light">
            <div class="card-title content-box-title add-space">
                <div class="row">
                    <div class="col-md-12 col-xs-12 ">
                        <div class="pull-left">
                            <div class="addon-box-list">
                                <div class="form-group-inline">
                                    <span class="addon">Show</span>
                                        <select class="form-control" name="size" title="Total List">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span class="addon">Data</span>
                                </div>
                            </div>
                        </div>
                        <div class="pull-right">
                            <div class="addon-box-list add-space-left  ">
                                <div class="form-group-inline">
                                    <span class="addon ">Ordering by</span>
                                    <select class="form-control" title="Sorting Data" name="orderingBy">
                                        <option value="0" selected>Sorting By</option>
                                        @foreach($ordering as $key => $value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table id="penggunaanmakanan" class="table table-hover table-striped widget-table"name="table-index">
                    <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th class="header-pagination">No Transaksi</th>
                        <th class="header-pagination">Total Barang</th>		
                        <th class="header-pagination">Tanggal Transaksi</th>
                        <th class="header-pagination">Voyage</th>								
                        <th class="header-pagination">Start Date</th>
                        <th class="header-pagination">End Date</th>
                        <th class="header-pagination">Status</th>		
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($penggunaanmakanans["rows"]as $penggunaanmakanan)
                    <tr>
                        
                        <td><a data-toggle="tooltip" data-placement="top" title="View" href="{{url("administrator/penggunaanmakanan/$penggunaanmakanan->id")}}"class="btn btn-outline-primary"><span class="fa fa-eye"></span></a></td>
                            <td >{{ $penggunaanmakanan->msNumber }}</td>
                            
                            <td>{{ floatval($penggunaanmakanan->totalBarang) }}</td>
                            <td><?php if($penggunaanmakanan->date) { echo date("Y-m-d", strtotime($penggunaanmakanan->date)); } ?></td>
                            <td>{{ $penggunaanmakanan->voyageName }}</td>
                            <td><?php if($penggunaanmakanan->voyageStart) { echo date("Y-m-d", strtotime($penggunaanmakanan->voyageStart)); } ?></td>
                            <td><?php if($penggunaanmakanan->voyageEnd) { echo date("Y-m-d", strtotime($penggunaanmakanan->voyageEnd)); } ?></td>
                            <td><?php 
                                if($penggunaanmakanan->status > 1) { 
                                    echo "Rejected";  
                                } else if($penggunaanmakanan->status > 0) { 
                                    echo "Approved"; 
                                } else { 
                                    echo "Open"; 
                            } ?></td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
            <div class=" content-box-footer">
                <div class="row">
                    <div class="col-md-6 text-left col-xs-12">
                    Page {{$penggunaanmakanans["page"]}} of <span id="pageCount">{{$penggunaanmakanans["pageCount"]}}</span>
                    Total Row <span id="totalRow">{{$penggunaanmakanans["rowCount"]}}</span>
                    </div>
                    <div class="col-md-6  col-xs-12">
                        <ul class="pagination pull-right">
                            <li class="page-item ">
                                <button class="page-link" tabindex="-1" name="first" title="First Page">First
                                </button>
                            </li>
                            <li class="page-item" id="pagination-prev">
                                <a class="page-link" title="Prev Page">
                                    <i class="fa fa-step-backward"></i></a>
                            </li>
                            <li class="page-item" id="pagination-next">
                                <a class="page-link" title="Next Page">
                                    <i class="fa fa-step-forward"></i></a></li>
                            <li class="page-item">
                                <button class="page-link" name="last" title="Last Page">Last</button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes.includes_footer')
    <script>
            <?php $penggunaanmakanans["rows"] = []; ?>
        var paginationParameter = ('{!! json_encode($penggunaanmakanans)!!}');
        var currentUrl = "{{url($currentPrefix)}}";
    </script>
    <script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>
@endsection