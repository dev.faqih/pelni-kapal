<style>
    td {
        font-size: 8px;
    }

    .txctr {
        text-align: center;
    }

    .txlft {
        text-align: left;
    }

    .txtr8 {
        text-align: right;
    }

    .txt7px {
        font-size: 7px;
    }

    .txbold {
        font-weight: bold;
    }

    .bdrtop {
        border-top: 1px solid #000000;
    }

    .bdrbot {
        border-bottom: 1px solid #000000;
    }

    .bdrlef {
        border-left: 1px solid #000000;
    }

    .bdrr8 {
        border-right: 1px solid #000000;
    }

    .pad5 {
        padding: 5px 5px 5px 5px;
    }
</style>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="100%" class="txctr">
            <table width="100%" style="" class="" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="txctr" style="" width="16%"><br />&nbsp;<br />
                        <span><img src="{{ url("assets/img/pelni.png") }}" border="0" height="40" style="padding:0px 10px;"></span>
                    </td>
                    <td width="68%" class=" txctr"><br />&nbsp;<br />
                        <span style="font-size:13px;">PT PELAYARAN NASIONAL INDONESIA (PERSERO)</span><br />
                    </td>
                    <td width="16%" class="txlft"><br />&nbsp;<br /> </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="" style="">
            <br />&nbsp;<br />
            <table style="" border="0" cellpadding="0" cellspacing="4">
                <tr>
                    <td width="15%">Dari</td>
                    <td width="85%">: Nahkoda {{ $permintaanbarang->namaKapal }}</td>
                </tr>
                <tr>
                    <td>Kepada Yth</td>
                    <td>: - Direktur Armada<br />
                        <span style="color:#FFF">:</span> - VP. Pengelolaan Bahan Bakar</td>
                </tr>
                <tr>
                    <td>Tembusan</td>
                    <td>: - Kepala Cabang</td>
                </tr>
                <tr>
                    <td>Nomor</td>
                    <td>: {{ $nomor }}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="" style="">
            <h2 class="txctr">PERMINTAAN BBM &amp; MINYAK PELUMAS</h2> <br />&nbsp;<br />
            <table border="0" width="100%">
                <tr>
                    <td width="20">1. </td>
                    <td width="60">Hari / Tanggal</td>
                    <td width="150">: {{ date("l / j F Y")  }}</td>

                    <td width="100">&nbsp;</td>
                    <td width="60">Voyage</td>
                    <td width="100">: {{ $permintaanbarang->voyageName }}</td>
                </tr>
                <!-- <tr>
            <td>2. </td>
            <td>Posisi Kapal<br />
                a. Lintang / Bujur<br />
                b. Di Perairan<br />
                c. RPM / Kecepatan
            </td>
            <td>:<br/>:<br/>:<br/>: .........rpm &nbsp; ........knot</td>
        </tr>
        <tr>
            <td>3. </td>
            <td colspan="2">ETA Pelabuhan Bunker / Pengisian</td>            
        </tr> -->
                <tr>
                    <td>2. </td>
                    <td colspan="5">Rencana Permintaan Bunker (HSD/MFO) / Pengisian Pelumas (LO)<br />
                        <table style="" border="1" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td class="txctr" width="20">No</td>
                                <td class="txctr" width="120"> Jenis </td>
                                <td class="txctr" width="60">Persediaan Awal / ROB</td>
                                <td class="txctr" width="60">Penggunaan</td>
                                <td class="txctr" width="60">Permintaan </td>
                                <td class="txctr" width="60">Hari/ Tgl Pengisian </td>
                                <td class="txctr" width="60">Pelabuhan Pengisian</td>
                                <td class="txctr" width="60">Keterangan</td>
                            </tr>
                            <?php $i = "a"; ?>
                            @foreach($listItems as $item)
                            <tr nobr="true">
                                <td class="txctr" width="20">{{ $i++ }}.</td>
                                <td width="120"> {{ $item->deskripsi }}</td>
                                <td class="txctr" width="60">{{ floatval($item->qtyRob) }}</td>
                                <td class="txctr" width="60">{{ floatval($item->qtyPenggunaan) }}</td>
                                <td class="txctr" width="60">{{ floatval($item->quantity) }}</td>
                                <td class="txctr" width="60">{{ date("d M Y", strtotime($item->needBy)) }}</td>
                                <td class="txctr" width="60">{{ $item->portBunker  }}</td>
                                <td class="txctr" width="60"></td>
                            </tr>
                            @endforeach
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>3. </td>
                    <td colspan="5">Demikian diajukan rencana permintaan sesuai kebutuhan
                        <br />&nbsp;<br />&nbsp;<br />
                        <table style="" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="40">&nbsp;</td>
                                <td width="100" class=" txctr">Mengetahui,<br />{{ $approver->jabatan }}</td>
                                <td width="140">&nbsp;</td>
                                <td width="100" class=" txctr">Yang mengajukan, <br />{{ $requester->jabatan }}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td class=""></td>
                                <td class=""><br />&nbsp;<br /><br />&nbsp;<br /><br />&nbsp;<br /></td>
                            </tr>
                            <tr>
                                <td width="40">&nbsp;</td>
                                <td width="100" class="txctr">{{ $approver->name }}</td>
                                <td width="140">&nbsp;</td>
                                <td width="100" class=" txctr">{{ $requester->name }}</td>
                            </tr>
                            <tr>
                                <td width="40">&nbsp;</td>
                                <td width="100" class="txctr">Nrp {{ $approver->nrp }}</td>
                                <td width="140">&nbsp;</td>
                                <td width="100" class=" txctr">Nrp {{ $requester->nrp }}</td>
                            </tr>
                        </table>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td colspan="6">Keterangan :</td>
                </tr>
                <tr>
                    <td>a. </td>
                    <td colspan="5">Permintaan dikirim paling lambat 3 (tiga hari) sebelum kapal sampai pelabuhan bunker ke Direktur Armada cq VP. Pengelolaan BBM tembusan Kepala Cabang pelabuhan bunker.</td>
                </tr>
                <tr>
                    <td>b. </td>
                    <td colspan="5">Dalam kolom Jenis cantumkan jenis minyak yang diminta (1Drum = 209 ltr) </td>
                </tr>
                <tr>
                    <td>c. </td>
                    <td colspan="5">Persediaan awal/ROB khusus BBM adalah volume BBM di tangki tidak termasuk Dead stock & Emergency Stock pada saat tiba di pelabuhan bunker.</td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td class="" style="">


        </td>
    </tr>
</table>