@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Permintaan Barang: Approval</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
            <div class="card-body content-box-body">
				@include('includes.include_error_prop')
				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#prNumber">No PR</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default"  placeholder="" name="prNumber" value="{{ $permintaanbarang->prNumber }}" title="prNumber">
					</div>

				</div><br />
				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#operatingUnit">Unit Operasi</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default"  placeholder="" name="operatingUnit" value="{{ $permintaanbarang->operatingUnit }}" title="operatingUnit">
					</div>

					<div class="col-sm-2 col-xs-5">
						<label for="#requesterId">Dibuat Oleh</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input disabled class="form-control input-default" value="{{ $permintaanbarang->requesterName }}" />
					</div>
				</div><br />

				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#deskripsi">Deskripsi</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default" placeholder="" name="deskripsi" value="{{ $permintaanbarang->deskripsi }}" title="deskripsi">
					</div>
					<div class="col-sm-2 col-xs-5">
						<label for="#requesterId">Status</label>
					</div>
					<div class="col-sm-4 col-xs-7"><input class="form-control input-default" disabled value="<?php 
						if($permintaanbarang->isApproved > 0) { echo "Approved"; } else if($permintaanbarang->isApproved < 0) { echo "Rejected"; } else { echo "Open"; }
						?>" /></div>
				</div><br />
				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#voyageName">Voyage</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default" name="voyageName" value="{{ $permintaanbarang->voyageName }}" title="voyageKode">
					</div>
					<div class="col-sm-2 col-xs-5">
						<label for="#voyageDate">Tanggal Voyage</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default" name="voyageDate" value="<?php 
						if($permintaanbarang->voyageStart) { 
							echo date("Y-m-d", strtotime($permintaanbarang->voyageStart)); 
						} 
						if($permintaanbarang->voyageStart && $permintaanbarang->voyageEnd) { echo " - "; }
						if($permintaanbarang->voyageEnd) { 
							echo date("Y-m-d", strtotime($permintaanbarang->voyageEnd));
						} ?>" title="voyageKode">
					</div>
				</div><br />
				<div class="clearfix">&nbsp;</div>
				<div class="clearfix">&nbsp;</div>
				<div class="row justify-content-between">
					<div class="col-4"><h5>Daftar Permintaan Barang</h5></div>
				</div> 
				<br />
				<div class="row">
					<div class="col-sm-1 col-xs-1">&nbsp;</div>
					<div class="col-sm-10 col-xs-1">
						<div class="table-responsive">
							<table id="tableItem" class="table table-hover table-bordered table-striped widget-table">
								<tr class="bg-primary text-light">
									<th>Tipe</th>
									<th>Barang</th>
									<th>Kategori</th>
									<th>Deskripsi</th>
									<th>UOM</th>
									<th>Qty</th>
									<th>Tanggal Dibutuhkan</th>
								</tr>

								@foreach($listItems as $item)
								<tr class="" >
									<td>&nbsp;</td>
									<td>{{ $item->namaItem }}</td>
									<td>{{ $item->category }}</td>
									<td>{{ $item->deskripsi }}</td>
									<td>{{ $item->UOM }}</td>
									<td>{{ $item->quantity }}</td>
									<td>{{ date("Y-m-d", strtotime($item->needBy)) }}</td>
								</tr>
								@endforeach
							</table>
						</div> 
					</div>
				</div>
						
				
				<br />
				<div class="clearfix">&nbsp;</div><div class="clearfix">&nbsp;</div>

				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#keperluan">Keperluan</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default "  placeholder="" name="keperluan" value="{{ $permintaanbarang->keperluan }}" title="keperluan">
					</div>
					<div class="col-sm-2 col-xs-5">
						<label for="#sumber">Sumber</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default "  placeholder="" name="sumber" value="{{ $permintaanbarang->sumber }}" title="sumber">
					</div>
				</div><br />


				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#organization">Organization</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default "  placeholder="" name="organization" value="{{ $permintaanbarang->organization }}" title="organization">
					</div>
					<div class="col-sm-2 col-xs-5">
						<label for="#locationId">Lokasi</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default "  placeholder="" name="locationId" value="{{ $permintaanbarang->namaLokasi }}" title="locationId">
					</div>
				</div><br />

				<div class="clearfix">&nbsp;</div>
				<div class="clearfix">&nbsp;</div>
				<div class="clearfix">&nbsp;</div>
				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<h5>Komentar</h5>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-xs-12">
						<textarea disabled class="form-control input-default" placeholder="Masukkan komentar" name="komentar" title="komentar">{{ $permintaanbarang->komentar }}</textarea>
					</div>
				</div>		
				<div class="clearfix">&nbsp;</div>			

				<div class="row">
				@if(isset($history) && $history)
					<p class="text-center">
						<a class="btn btn-primary" href="{{url("administrator/permintaanbarang")}}" title="Back"><span class="fa fa-undo"></span> Kembali</a>
					</p>
				@else
					<div class="col-sm-6 col-xs-6 text-right">
						<form action="{{url("administrator/permintaanbarang/approval/")}}" role="form" method="POST" enctype="multipart/form-data">
							<input type="hidden" name="_method" value="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="prNumber" value="{{ $permintaanbarang->prNumber }}">
							<input type="hidden" name="isApproved" value="1">		
							<input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">
							<input type="hidden" name="approverId" value="{{ WorkerAuth::auth()->getAuth()->id }}">				
							<button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> &nbsp; Setujui</button> &nbsp;					
						</form>
					</div>
					<div class="col-sm-6 col-xs-6">
						<form action="{{url("administrator/permintaanbarang/approval/")}}" role="form" method="POST" enctype="multipart/form-data">
							<input type="hidden" name="_method" value="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="prNumber" value="{{ $permintaanbarang->prNumber }}">
							<input type="hidden" name="isApproved" value="2">			
							<input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">
							<input type="hidden" name="approverId" value="{{ WorkerAuth::auth()->getAuth()->id }}">
							<button type="submit" class="btn btn-primary"><i class="fa fa-close"></i> &nbsp; Tolak</button> &nbsp;	 &nbsp;		
							<a class="btn btn-primary" href="{{ URL::previous() }}" title="Back"><span class="fa fa-undo"></span> Kembali</a>		
						</form>
					</div>
				@endif
				</div>
            </div>
        </div>
    </div>
    @include('includes.includes_footer')
@endsection