@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Buatt Surat Permintaan Barang</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>                    
            </div>
		</div>
        <div class="clearfix">&nbsp;</div>
		
		<form autocomplete="off" class="submitTransaction" action="{{url("administrator/permintaanbarang")}}" role="form" method="POST" enctype="multipart/form-data">
		<div class="container border rounded bg-light">
            <div class="clearfix">&nbsp;</div>
				@include('includes.include_error_prop')                
				
				<input type="hidden" name="_method" value="POST">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="tglPermintaan" value="{{ date("Y-m-d H:i:s") }}">
				<input type="hidden" name="isApproved" value="0">
				<input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">
				<input type="hidden" name="kodeKapal" value="{{ WorkerAuth::auth()->getAuth()->kodeKapal }}">
				<input type="hidden" name="requesterId" value="{{ WorkerAuth::auth()->getAuth()->id }}">
				<input type="hidden" name="tipeTransaksi" value="PR">
				<input type="hidden" name="tglTransaksi" id="tglTransaksi" value="{{ date('Y-m-d') }}">
				<input type="hidden" name="urlTransaksi" value="permintaanbarang">
				<input type="hidden" name="namaTransaksi" value="Permintaan Barang">
				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#operatingUnit">Unit Operasi</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text"  class="form-control input-default" required placeholder="Masukkan unit operasi" name="operatingUnit" value="{{ WorkerAuth::auth()->getAuth()->namaKapal }}" readonly title="operatingUnit">
					</div>

					<div class="col-sm-2 col-xs-5">
						<label for="#requesterId">Dibuat Oleh</label>
					</div>
					<div class="col-sm-4 col-xs-7">							
						<input disabled class="form-control input-default" required value="{{ WorkerAuth::auth()->getAuth()->name }}" />
					</div>
				</div><br />

				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#deskripsi">Deskripsi</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" class="form-control input-default" required placeholder="Masukkan deskripsi" name="deskripsi" value="{{ old("deskripsi") }}" title="deskripsi">
					</div>
					<div class="col-sm-2 col-xs-5">
						<label for="#requesterId">Status</label>
					</div>
					<div class="col-sm-4 col-xs-7"><input class="form-control input-default" disabled value="Open" /></div>
				</div><br />
				<div class="row">
					<div class="col-sm-2 col-xs-5">Voyage</div>
					<div class="col-sm-4 col-xs-7">
						<select class="form-control input-default" required name="voyageKode" title="Voyage">
						</select>
					</div>
					<div class="col-sm-2 col-xs-5">Tanggal Voyage</div>
					<div class="col-sm-3 col-xs-7 input-group">
						<input readonly required class=" form-control input-default search-value" name="voyageStart" value="" >
						<div class="input-group-addon"> &nbsp; s/d &nbsp; </div>
						<input readonly required class=" form-control input-default search-value" name="voyageEnd" value="" >
					</div>
				</div>
				<div class="clearfix">&nbsp;</div>				
			</div>
			<div class="clearfix">&nbsp;</div>				
			<div class="container border rounded bg-light">

				<div class="clearfix">&nbsp;</div>
				<div class="row justify-content-between">
					<div class="col-sm-4 col-xs-6"><h5>Daftar Barang</h5></div>
					<div class="col-sm-3 text-right col-xs-5">
						<button type="button" id="loadItemModal" class="btn btn-primary">Tambah Item</button>
					</div>
				</div><br />
				<div class="table-responsive">
					<table id="tableItem" class="table table-hover table-striped widget-table">
					<thead>
						<tr>
							<th>No</th>
							<th>Kode Barang</th>
							<th>Part Number</th>
							<th>Deskripsi</th>
							<th>Kategori</th>
							<th>UOM</th>
							<th>Qty</th>
							<th>Tanggal Dibutuhkan</th>
							<th>&nbsp;</th>
						</tr>
					<thead>
					<tbody></tbody>
					</table>
				</div><br />


				<div class="clearfix">&nbsp;</div>
				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#approverId">Keperluan</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text"  class="form-control input-default " required placeholder="Masukkan keperluan" name="keperluan" value="{{old("keperluan")}}" title="keperluan">
					</div>
				</div><br />


				<div class="clearfix">&nbsp;</div>				
			</div>
			<div class="clearfix">&nbsp;</div>				
			<div class="container border rounded bg-light">
				<div class="clearfix">&nbsp;</div>


				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<h5>Komentar</h5>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-xs-12">
					<textarea class="form-control input-default" placeholder="Masukkan komentar" required name="komentar" title="komentar">{{old("komentar")}}</textarea>
					</div>
				</div>		
				<div class="clearfix">&nbsp;</div>
				<div class="row">
					<div class="col-sm-12 col-xs-12">
						<label>
							<span><input class="form-input" type="checkbox" value="" id="invalidCheck" required> &nbsp; </span><span>Saya setuju untuk melanjutkan proses ini.</span>
						</label>
					</div>
				</div><br />				
				<p class="text-center">
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Kirim</button> &nbsp;
					<a class="btn btn-primary" href="{{ URL::previous() }}" title="Back"><span class="fa fa-undo"></span> Kembali</a>
				</p>
			
			</div>
		</div>
		</form>
    </div>
    @include('includes.includes_footer')
	@include('includes.include_modal_permintaanbarang')
    <script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>
	<script>
	$(document).ready(function() {				
		loadVoyage('{{ url('administrator/permintaanbarang/getFutureVoyages') }}', 'tglTransaksi');
	});

	$("select[name='voyageKode']").change(function() {
		loadVoyageDate('{{ url('administrator/permintaanbarang/getVoyageDate') }}', $(this).val() );
	});
	</script>
@endsection
