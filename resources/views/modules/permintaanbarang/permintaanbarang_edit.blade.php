@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Edit data of{{$title}}</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
            <div class="card-body content-box-body">
				<div class="row">
					<div class="col-sm-12 text-right">
						<a class="btn btn-primary" href="{{url("administrator/permintaanbarang")}}" title="Back"><span class="fa fa-undo"></span> Back</a>
					</div>
				</div>
				<div class="clearfix">&nbsp;</div>
            	@include('includes.include_error_prop')

                <form role="form" action="{{url("administrator/permintaanbarang/$permintaanbarang->prNumber/update")}}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#kapalId">Kapal</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<select class="form-control input-default"  name="kapalId" title="kapalId">
							@foreach($kapals as $kapal)
								<option value="{{$kapal->id}}"@if($permintaanbarang->kapalId== $kapal->id) selected @endif>{{$kapal->namaKapal}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-sm-2 col-xs-5">
						<label for="#locationId">Location</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<select class="form-control input-default"  name="locationId" title="locationId">
							@foreach($lokasiitems as $lokasiitem)
								<option value="{{$lokasiitem->id}}"@if($permintaanbarang->locationId== $lokasiitem->id) selected @endif>{{$lokasiitem->namaLokasi}}</option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#approverId">Approver</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<select class="form-control input-default"  name="approverId" title="approverId">
							@foreach($users as $user)
								<option value="{{$user->id}}"@if($permintaanbarang->approverId== $user->id) selected @endif>{{ $user->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-sm-2 col-xs-5">
						<label for="#requesterId">Requester</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<select class="form-control input-default"  name="requesterId" title="requesterId">
							@foreach($users as $user)
								<option value="{{$user->id}}"@if($permintaanbarang->requesterId== $user->id) selected @endif>{{ $user->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				
				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#destinationType">DestinationType</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text"  class="form-control input-default "  placeholder="Masukkan destinationType" name="destinationType" value="{{$permintaanbarang->destinationType}}" title="destinationType">
					</div>
				</div>

				<br />
				<div class="row justify-content-between">
					<div class="col-4"><h5>Daftar Permintaan Barang</h5></div>
					<div class="col-sm-3 text-right col-xs-5">
						<button type="button" id="loadItemModal" class="btn btn-primary">Add Item</button>
					</div>
				</div> 
				<br />
				<div class="table-responsive">
					<table id="tableItem" class="table table-hover table-striped widget-table">
						<tr>
							<th>Tipe</th>
							<th>Barang</th>
							<th>Kategori</th>
							<th>Deskripsi</th>
							<th>UOM</th>
							<th>Qty</th>
							<th>Tgl Dibutuhkan</th>
							<th>&nbsp;</th>
						</tr>
					</table>
				</div> <br />


				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#approverId">Keperluan</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" class="form-control input-default "  placeholder="" name="destinationType" value="{{ $permintaanbarang->destinationType }}" title="destinationType">
					</div>
					<div class="col-sm-2 col-xs-5">
						<label for="#sumber">Sumber</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" class="form-control input-default "  placeholder="" name="sumber" value="{{ $permintaanbarang->sumber }}" title="sumber">
					</div>
				</div><br />


				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#organization">Organization</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" class="form-control input-default "  placeholder="" name="organization" value="{{ $permintaanbarang->organization }}" title="organization">
					</div>
					<div class="col-sm-2 col-xs-5">
						<label for="#locationId">Lokasi</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" class="form-control input-default "  placeholder="" name="locationId" value="{{ $permintaanbarang->locationId }}" title="locationId">
					</div>
				</div><br />


				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#komentar">Komentar</label>
					</div>
					<div class="col-sm-10 col-xs-7">
						<input type="text" class="form-control input-default "  placeholder="" name="komentar" value="{{ $permintaanbarang->komentar }}" title="komentar">
					</div>
				</div><br />

				<p><button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button></p>
                </form>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
	@include('includes.include_modal_permintaanbarang')
	<script>
		@foreach($listItems as $item)
			printItemIntoList( "{{ $item->id }}", "{{ $item->namaItem }}", "{{ $item->deskripsi }}", "{{ $item->category }}", "{{ $item->UOM }}", "{{ $item->quantity }}", "{{ $item->needBy }}");
		@endforeach
	</script>
    <script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>
@endsection