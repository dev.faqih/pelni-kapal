@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Detail Permintaan Barang</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
		</div>
		<div class="clearfix">&nbsp;</div>
		<div class="container border rounded bg-light">
		<div class="clearfix">&nbsp;</div>
			@include('includes.include_error_prop')
			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#prNumber">No PR</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" readonly class="form-control input-default"  placeholder="" name="prNumber" value="{{ $permintaanbarang->prNumber }}" title="prNumber">
				</div>

			</div><br />
			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#operatingUnit">Unit Operasi</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" readonly class="form-control input-default"  placeholder="" name="operatingUnit" value="{{ $permintaanbarang->operatingUnit }}" title="operatingUnit">
				</div>

				<div class="col-sm-2 col-xs-5">
					<label for="#requesterId">Dibuat Oleh</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input readonly class="form-control input-default" value="{{ $permintaanbarang->requesterName }}" />
				</div>
			</div><br />

			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#deskripsi">Deskripsi</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" readonly class="form-control input-default" placeholder="" name="deskripsi" value="{{ $permintaanbarang->deskripsi }}" title="deskripsi">
				</div>
				<div class="col-sm-2 col-xs-5">
					<label for="#requesterId">Status</label>
				</div>
				<div class="col-sm-4 col-xs-7"><input class="form-control input-default" readonly value="<?php 
					if($permintaanbarang->isApproved > 1) { echo "Rejected"; } else if($permintaanbarang->isApproved > 0) { echo "Approved"; } else { echo "Open"; }
					?>" /></div>
			</div><br />
			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#voyageName">Voyage</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" readonly class="form-control input-default" name="voyageName" value="{{ $permintaanbarang->voyageName }}" title="voyageKode">
				</div>
				<div class="col-sm-2 col-xs-5">
					<label for="#voyageDate">Tanggal Voyage</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" readonly class="form-control input-default" name="voyageDate" value="<?php 
					if($permintaanbarang->voyageStart) { 
						echo date("Y-m-d", strtotime($permintaanbarang->voyageStart)); 
					} 
					if($permintaanbarang->voyageStart && $permintaanbarang->voyageEnd) { echo "  -  "; }
					if($permintaanbarang->voyageEnd) { 
						echo date("Y-m-d", strtotime($permintaanbarang->voyageEnd));
					} ?>" title="voyageKode">
				</div>
			</div><br />
			<div class="clearfix">&nbsp;</div>
		</div>
		<div class="clearfix">&nbsp;</div>
		<div class="container border rounded bg-light">
			<div class="clearfix">&nbsp;</div>
			<div class="row justify-content-between">
				<div class="col-4"><h5>Daftar Permintaan Barang</h5></div>
			</div> 
			<br />
			<div class="row">
				<div class="col-sm-12 col-xs-12">
					<div class="table-responsive">
						<table id="tableItem" class="table table-hover table-bordered table-striped widget-table">
						<thead>
							<tr class="bg-primary text-light">
								<th>No</th>
								<th>Kode Barang</th>
								<th>Part Number</th>
								<th>Deskripsi</th>
								<th>Kategori</th>
								<th>UOM</th>
								<th>Qty</th>
								<th>Tanggal Dibutuhkan</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1; ?>
							@foreach($listItems as $item)
							<tr class="" >
								<td>{{ $i++ }}</td>
								<td>{{ $item->namaItem }}</td>
								<td>{{ $item->partNumber }}</td>
								<td>{{ $item->deskripsi }}</td>
								<td>{{ $item->category }}</td>
								<td>{{ $item->UOM }}</td>
								<td>{{ floatval($item->quantity) }}</td>
								<td>{{ date("Y-m-d", strtotime($item->needBy)) }}</td>
							</tr>
							@endforeach
						</tbody>
						</table>
					</div> 
				</div>
			</div>
					
			
			<br />
			<div class="clearfix">&nbsp;</div><div class="clearfix">&nbsp;</div>

			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#keperluan">Keperluan</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" readonly class="form-control input-default "  placeholder="" name="keperluan" value="{{ $permintaanbarang->keperluan }}" title="keperluan">
				</div>				
			</div><br />

			<div class="clearfix">&nbsp;</div>
			
		</div>
		<div class="clearfix">&nbsp;</div>
		<div class="container border rounded bg-light">
			<div class="clearfix">&nbsp;</div>
			<div class="row">
				<div class="col-sm-2 col-xs-5"><h5>Komentar</h5></div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-xs-12">
					<textarea readonly class="form-control input-default" placeholder="Masukkan komentar" name="komentar" title="komentar">{{ $permintaanbarang->komentar }}</textarea>
				</div>
			</div>		
			<div class="clearfix">&nbsp;</div>

			@if($permintaanbarang->isApproved > 0)
			<div class="row">
				<div class="col-sm-2 col-xs-5"><h5>Feedback</h5></div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-xs-12">
					<textarea readonly class="form-control input-default" placeholder="Masukkan Feedback" name="feedback" title="feedback">{{ $permintaanbarang->feedback }}</textarea>
				</div>
			</div>					
			<div class="clearfix">&nbsp;</div>
			@endif

			@if(isset($isApproval) && $isApproval)

				
				@if($permintaanbarang->isApproved > 0)
					<div class="row">
						<div class="col-sm-12 col-xs-12 text-center">
							<a class="btn btn-primary" href="{{ URL::previous() }}" title="Back"><span class="fa fa-undo"></span> Kembali</a>
						</div>
					</div>
				@else
					@if(WorkerAuth::auth()->getAuth()->aplikasi == "KAPAL")
					<div class="clearfix">&nbsp;</div>
					<div class="container border rounded bg-light">
						<div class="clearfix">&nbsp;</div>

						<form id="formApproval" action="{{url("administrator/permintaanbarang/approval/")}}" role="form" method="POST" enctype="multipart/form-data">
							<input type="hidden" name="_method" value="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="prNumber" value="{{ $permintaanbarang->prNumber }}">
							<input type="hidden" name="status" value="2">			
							<input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">
							<input type="hidden" name="approverId" value="{{ WorkerAuth::auth()->getAuth()->id }}">
							
							<div class="row">
								<div class="col-sm-2 col-xs-5"><h5>Feedback</h5></div>
							</div>
							<div class="row">
								<div class="col-sm-12 col-xs-12">
									<textarea class="form-control input-default" placeholder="Masukkan Feedback" name="feedback" title="feedback"></textarea>
								</div>
							</div>					
							<div class="clearfix">&nbsp;</div>							
							<div class="row">
								<div class="col-sm-12 col-xs-12 text-center">											
									<button type="button" onClick="submitFormApproval()" class="btn btn-primary"><i class="fa fa-check"></i> &nbsp;Setujui</button> &nbsp;			
									<button type="submit" class="btn btn-primary"><i class="fa fa-close"></i> &nbsp;Tolak</button> &nbsp; 
									<a class="btn btn-primary" href="{{ URL::previous() }}" title="Back"><span class="fa fa-undo"></span> Kembali</a>											
								</div>
							</div>
							<div class="clearfix">&nbsp;</div>
						</form>
					</div>
					@endif
				@endif
				<div class="clearfix">&nbsp;</div>

			@else
			
			<p class="text-center"> 
				<a class="btn btn-primary" href="{{url("administrator/permintaanbarang")}}" title="Back"><span class="fa fa-undo"></span> Kembali</a>	&nbsp; 
				<!-- <a class="btn btn-primary" href="{{url("administrator/permintaanbarang/generateSPB/".$permintaanbarang->id)}}" target="_blank" title="Download PDF"><span class="fa fa-print"></span> Laporan Umum</a> &nbsp;  -->
				<button class="btn btn-primary" onClick="loadReport('UMUM' ,'{{ $permintaanbarang->id }}', '{{ $permintaanbarang->prNumber }}')" title="Download PDF"><span class="fa fa-print"></span> Laporan Umum</button> &nbsp; 
				<button class="btn btn-primary" title="Download PDF" onClick="loadReport('FOOD' ,'{{ $permintaanbarang->id }}', '{{ $permintaanbarang->prNumber }}')"><span class="fa fa-print"></span> Laporan Makanan</button>  &nbsp;
				<button class="btn btn-primary" onClick="loadReport('BBM' ,'{{ $permintaanbarang->id }}', '{{ $permintaanbarang->prNumber }}')" title="Download PDF"><span class="fa fa-print"></span> Laporan BBM</button>
			</p>
			
			@endif
        </div>		
    </div>
    @include('includes.includes_footer')
	@include('includes.include_modal_permintaanbarang')
    <script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>
@endsection