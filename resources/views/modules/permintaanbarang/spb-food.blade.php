<style>
td { font-size:8px; }
.txctr { text-align:center; }
.txlft { text-align:left; }
.txtr8 { text-align:right; }
.txbold { font-weight:bold; }
.bdrtop { border-top:1px solid #000000; }
.bdrbot { border-bottom:1px solid #000000; }
.bdrlef { border-left:1px solid #000000; }
.bdrr8 { border-right:1px solid #000000; }
.pad5 { padding: 5px 5px 5px 5px; }

</style>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
    <td width="100%" class="txctr">
        <table width="100%" style="" class="" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="4" class="txctr" style="" width="16%"><br />&nbsp;<br />
            <span ><img src="{{ url("assets/img/pelni.png") }}" border="0" height="40" style="padding:0px 10px;"></span>
            </td>
            <td width="68%" rowspan="4" class=" txctr"><br />&nbsp;<br />
                <span style="font-size:12px;">FORMULIR</span><br />
                <span style="font-size:12px;">DAFTAR PERMINTAAN BAHAN MAKANANAN (DPBM)</span><br />
            </td>
            <td width="4%" class="txlft"><br />&nbsp;<br />No </td>         
            <td width="14%" class="txlft"><br />&nbsp;<br />: {{ $permintaanbarang->prNumber }}</td>       
        </tr>
        <tr><td class="txlft">Rev </td><td class="txlft">: @if(isset($rev)) {{ $rev }} @endif</td></tr>
        <tr><td class="txlft">Tgl </td><td class="txlft">: {{ date("j F Y") }}</td></tr>
        <tr><td class="txlft">Hal  </td><td class="txlft">: 1 dari {{ $totalPageCount }} </td></tr>        
        </table>


    </td>
</tr>
<tr><td class="" style="">
    <br />&nbsp;<br />
    Bersama ini kami sampaikan daftar permintaan bahan makanan untuk Penumpang,<br />
    Nahkoda dan ABK: <br />
    <table style="" border="0" cellpadding="0" cellspacing="4">
    <tr><td width="2%" >a. </td><td width="20%" >Kapal</td> <td width="78%" >: {{ $permintaanbarang->namaKapal }}</td></tr>
    <tr><td>b. </td><td>Voyage</td> <td>: {{ $permintaanbarang->voyageName }}</td></tr>
    <tr><td>c. </td><td>Periode</td> <td>:
            @if($permintaanbarang->voyageKode > 0)
                {{ date("j F Y", strtotime($permintaanbarang->voyageStart)) . " - " . date("j F Y", strtotime($permintaanbarang->voyageEnd)) }}
            @else
                {{ date("j F Y", strtotime($permintaanbarang->tglPermintaan)) }}
            @endif
    </td></tr>
    <tr><td>d. </td><td>Estimasi jumlah penumpang </td> <td>: @if(isset($jlhOrang)) {{ $jlhOrang }} @endif Orang</td></tr>
    <tr><td>e. </td><td>Untuk disupply tanggal</td> <td>: @if(isset($tglSupply)) {{ date("d F Y", strtotime($tglSupply)) }} @endif</td></tr>
    <tr><td>f. </td><td>Disupply di pelabuhan</td> <td>: @if(isset($namaPelabuhan)) {{ $namaPelabuhan }} @endif</td></tr>
    </table>    <br />
    <span class="txbold">Rincian daftar permintaan bahan makanan sebagai berikut:</span>
    <br />
</td></tr>
<tr><td class="" style="">

    <br />
    <table width="100%" cellpadding="0" border="0" class="" cellspacing="0">
    <tr><td width="50%"><br />

        <table cellpadding="3" class="bdrtop bdrlef" cellspacing="0">
            <thead><tr nobr="true">
                <td class="bdrr8 bdrbot txctr txbold" width="20"><span style="font-size:10px;">No</span></td>
                <td class="bdrr8 bdrbot txctr txbold" width="98"><span style="font-size:10px;">Nama Bahan</span></td>
                <td class="bdrr8 bdrbot txctr txbold" width="30"><span style="font-size:9px;">UOM</span></td>
                <td class="bdrr8 bdrbot txctr txbold" width="22"><span style="font-size:9px;">Qty</span></td>
                <td class="bdrr8 bdrbot txctr txbold" width="48"><span style="font-size:10px;">Harga</span></td>
                <td class="bdrr8 bdrbot txctr txbold" width="48"><span style="font-size:10px;">Total</span></td>
            </tr>
            </thead>
            <tbody>
            <?php $i = 0; $total = 0; $nRow = ceil(count($listItems) / 2); for($i=0;$i<$nRow;$i++) { if(isset($listItems[$i])) {
                $item = $listItems[$i];
                $total += $item->harga; ?>
            <tr nobr="true">
                <td class="bdrr8 bdrbot bdrlef">{{ ($i+1) }}</td>
                <td class="bdrr8 bdrbot">{{ $item->deskripsi }}</td>
                <td class="bdrr8 bdrbot txctr">{{ $item->UOM }}</td>
                <td class="bdrr8 bdrbot txctr">{{ floatval($item->quantity) }}</td>
                <td class="bdrr8 bdrbot txtr8">Rp. {{ floatval($item->harga_satuan) }}</td>
                <td class="bdrr8 bdrbot txtr8">Rp. {{ floatval($item->harga) }}</td>
            </tr>
            <?php } } ?>
            </tbody>
        </table>
    
    </td><td width="50%"><br />

        <table cellpadding="3" class="bdrtop bdrlef" cellspacing="0">
            <thead><tr nobr="true">
                <td class="bdrr8 bdrbot txctr txbold" width="20"><span style="font-size:10px;">No</span></td>
                <td class="bdrr8 bdrbot txctr txbold" width="98"><span style="font-size:10px;">Nama Bahan</span></td>
                <td class="bdrr8 bdrbot txctr txbold" width="30"><span style="font-size:9px;">UOM</span></td>
                <td class="bdrr8 bdrbot txctr txbold" width="22"><span style="font-size:9px;">Qty</span></td>
                <td class="bdrr8 bdrbot txctr txbold" width="48"><span style="font-size:10px;">Harga</span></td>
                <td class="bdrr8 bdrbot txctr txbold" width="48"><span style="font-size:10px;">Total</span></td>
            </tr>
            </thead>
            <tbody>
            <?php if(isset($listItems[$i])) { for(;$i<count($listItems); $i++) {
                $item = $listItems[$i]; 
                $total += $item->quantity * $item->harga_satuan; ?>
            <tr nobr="true">
                <td class="bdrr8 bdrbot bdrlef">{{ ($i+1) }}</td>
                <td class="bdrr8 bdrbot">{{ $item->deskripsi }}</td>
                <td class="bdrr8 bdrbot txctr">{{ $item->UOM }}</td>
                <td class="bdrr8 bdrbot txctr">{{ floatval($item->quantity) }}</td>
                <td class="bdrr8 bdrbot txtr8">Rp. {{ floatval($item->harga_satuan) }}</td>
                <td class="bdrr8 bdrbot txtr8">Rp. {{ floatval($item->harga) }}</td>
            </tr>
            <?php }  ?>
            <?php } ?>
            
            <tr nobr="true">
                <td class=" bdrbot bdrlef">&nbsp;</td>
                <td colspan="4" class="bdrr8 bdrbot txtr8">Total Biaya Bahan Baku Makanan</td>
                <td class="bdrr8 bdrbot txtr8">Rp. {{ floatval($total) }}</td>
            </tr>

            </tbody>
        </table>
    
    </td></tr></table>
</td></tr>

<tr><td>
    <br />&nbsp;<br />&nbsp;<br />
</td></tr><tr nobr="true"><td>
    <table style="" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="120" >&nbsp;</td>
        <td width="100" class="bdrtop bdrlef bdrr8 bdrbot txctr">Dibuat oleh,</td>
        <td width="100" class="bdrtop bdrlef bdrr8 bdrbot txctr">Disetujui oleh,</td>
    </tr>
    <tr>
        <td></td>
        <td class="bdrlef bdrr8 bdrbot"></td>
        <td class="bdrr8 bdrbot"><br />&nbsp;<br /><br />&nbsp;<br /><br />&nbsp;<br /></td>
    </tr><tr>
        <td width="120" >&nbsp;</td>
        <td width="100" class="bdrtop bdrlef bdrr8 bdrbot txctr">{{ $requester->name }}</td>
        <td width="100" class="bdrtop bdrlef bdrr8 bdrbot txctr">{{ $approver->name }}</td>
    </tr><tr>
        <td width="120" >&nbsp;</td>
        <td width="100" class="bdrtop bdrlef bdrr8 bdrbot txctr">{{ $requester->jabatan }}</td>
        <td width="100" class="bdrtop bdrlef bdrr8 bdrbot txctr">{{ $approver->jabatan }}</td>
    </tr>
    </table>    
    <br />
</td></tr>
</table>