<style>
td { font-size:8px; }
.txctr { text-align:center; }
.txlft { text-align:left; }
.bdrtop { border-top:1px solid #000000; }
.bdrbot { border-bottom:1px solid #000000; }
.bdrlef { border-left:1px solid #000000; }
.bdrr8 { border-right:1px solid #000000; }
</style>
<table width="100%" style="" class="" cellpadding="0" cellspacing="0">
<tr>
    <td rowspan="3" class="bdrtop bdrlef bdrbot txctr" style="" width="16%"><br />&nbsp;<br />
    <span ><img src="{{ url("assets/img/pelni.png") }}" border="0" height="40" style="padding:0px 10px;"></span>
    </td>
    <td width="68%" rowspan="3" class="bdrtop txctr bdrbot bdrr8"><br />&nbsp;<br />
        <span style="font-size:11px;">PERUSAHAAN PERSEROAN (PERSERO) </span><br />
        <span style="font-size:13px;">PT PELAYARAN NASIONAL INDONESIA (PELNI)</span><br />
        <span style="font-size:8px;">Jl. GAJAH MADA NO. 14 JAKARTA PUSAT</span><br />
    </td>
    <td width="8%" rowspan="2" class="bdrtop bdrlef bdrbot bdrr8"> <br />&nbsp;<br /><span style="padding-bottom:5px;"> Kode</span> </td>
    <td width="8%" class="bdrr8 bdrtop">  <br />&nbsp;<br /> &nbsp; {{ $kode }}  </td>
</tr>
<tr><td class="bdrbot bdrr8"><br /></td></tr>
<tr>
    <td class="bdrbot bdrr8" style=""> <br />&nbsp;<br /><span style="padding-bottom:5px;padding-top:10px;"> Revisi</span> </td>
    <td  style="" class="bdrbot bdrr8" > <br />&nbsp;<br />  &nbsp; {{ $rev }} </td>
</tr>
<tr><td colspan="4" width="100%" style="">
    <br />&nbsp;<br />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="">
    <tr>
        <td width="25%"> &nbsp; </td>
        <td width="50%"> &nbsp; </td>
        <td width="25%">Hal, 1 dari {{ $totalPageCount }} </td>
    </tr>
    <tr>
        <td> &nbsp; </td>
        <td class="txctr" ><h3>SURAT PERMINTAAN BARANG (SPB)</h3></td>
        <td> &nbsp; </td>
    </tr>
    <tr>    
        <td style="padding:10px;">  <br />&nbsp;<br />
            <span>Bagian   : {{ $bagian }}</span><br />
            <span>Nomor    : {{ $permintaanbarang->prNumber }} </span><br />
            <span>Tanggal  : {{ date("d F Y") }}</span> <br />
            <span>KAPAL/WH : {{ $permintaanbarang->namaKapal }} </span>
        </td>
        <td> &nbsp; </td>
        <td> &nbsp; </td>
    </tr>
    </table>

        
</td></tr>
<tr><td colspan="4" width="100%" style="">

    <br />&nbsp;<br />
    <table width="100%" cellpadding="1" cellspacing="0" class="bdrbot bdrr8" border="0" style="">
    <tr>
        <td class="txctr bdrtop bdrlef " colspan="3"> INSTALASI: </td>
        <td class="txctr bdrtop " colspan="3"> UNIT: </td>
        <td class="txctr bdrtop "  colspan="4"> SEGMENT: </td>
    </tr>
    <tr>
        <td rowspan="2" width="20" class="txctr bdrtop bdrlef"> No. </td>
        <td rowspan="2" class="txctr bdrtop bdrlef" width="120"> Nama, Type Barang </td>
        <td rowspan="2" class="txctr bdrtop bdrlef"> Kode Barang </td>
        <td rowspan="2" class="txctr bdrtop bdrlef"> Part No. </td>
        <td rowspan="2" class="txctr bdrtop bdrlef"> Kode Pabrik </td>
        <td rowspan="2" width="30" class="txctr bdrtop bdrlef" width="50"> Satuan </td>
        <td class="txctr bdrtop bdrlef" colspan="3" width="120"> Jumlah </td>
        <td rowspan="2" class="txctr bdrtop bdrlef" width="67"> Keterangan </td>
    </tr>
    <tr>
        <td width="40" class="txctr bdrtop bdrlef"> Sisa </td>
        <td width="40" class="txctr bdrtop bdrlef"> Diminta </td>
        <td width="40" class="txctr bdrtop bdrlef"> Disetujui </td>
    </tr>
    <?php $i = 1; ?>
    @foreach($listItems as $item)  
    <tr nobr="true">
        <td class="txctr bdrtop bdrlef" width="20">{{ $i++ }}</td>
        <td class="txlft bdrtop bdrlef">{{ $item->deskripsi }}</td>
        <td class="txctr bdrtop bdrlef">{{ $item->namaItem }}</td>
        <td class="txctr bdrtop bdrlef">{{ $item->partNumber }}</td>
        <td class="txctr bdrtop bdrlef"></td>
        <td class="txctr bdrtop bdrlef">{{ $item->UOM }}</td>
        <td class="txctr bdrtop bdrlef" width="40">{{ floatval($item->sisaawal) }}</td>
        <td class="txctr bdrtop bdrlef" width="40">{{ floatval($item->quantity) }}</td>
        <td class="txctr bdrtop bdrlef" width="40">{{ floatval($item->disetujui) }}</td>
        <!-- <td class="txctr bdrtop bdrlef">&nbsp;</td> -->
        <td class="txlft bdrtop bdrlef">{{ $permintaanbarang->komentar }}</td>
    </tr>
    @endforeach
    </table>
        
</td></tr>

<tr><td>
    <br />&nbsp;<br />
</td></tr>
<tr  nobr="true"><td colspan="4" width="100%" style="padding:5px;">
    <table width="100%" style="" cellpadding="0" cellspacing="0">
    <tr>
        <td width="70%" class="txctr"  style=""> Disetujui, <br /> {{ $approver->jabatan }}:  </td>
        <td width="30%" class="txctr"  style="">Yang Meminta, <br />{{ $requester->jabatan }}</td>
    </tr>
    <tr><td colspan="2"> <p>&nbsp;</p><p>&nbsp;</p> </td></tr>
    <tr>
        <td class="txctr"  style=""> 
        <span style="text-decoration:underline;">{{ $approver->name }}</span><br/>
            <span>{{ $approver->nrp }}</span> 
        </td>
        <td class="txctr"  style=""> 
            <span style="text-decoration:underline;">{{ $requester->name }}</span><br/>
            <span>{{ $requester->nrp }}</span> 
        </td>
    </tr>   
    </table>
        
</td></tr>
</table>