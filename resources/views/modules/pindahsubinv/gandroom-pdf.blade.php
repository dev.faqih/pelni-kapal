<style>
td { font-size:8px; }
.txctr { text-align:center; }
.txlft { text-align:left; }
.txbold { font-weight:bold; }
.bdrtop { border-top:1px solid #000000; }
.bdrbot { border-bottom:1px solid #000000; }
.bdrlef { border-left:1px solid #000000; }
.bdrr8 { border-right:1px solid #000000; }
.pad5 { padding: 5px 5px 5px 5px; }

</style>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
    <td width="100%" class="txctr">
        <table width="100%" style="" class="" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="4" class="txctr" style="" width="16%"><br />&nbsp;<br />
            <span ><img src="{{ url("assets/img/pelni.png") }}" border="0" height="40" style="padding:0px 10px;"></span>
            </td>
            <td width="68%" rowspan="4" class=" txctr"><br />&nbsp;<br />
                <span style="font-size:13px;">FORMULIR</span><br />
                <span style="font-size:13px;">PENYERAHAN BAHAN DARI {{ strtoupper($pindahsubinv->fromLocationName) }}</span><br />
            </td>
            <td width="4%" class="txlft"><br />&nbsp;<br />No </td>         
            <td width="14%" class="txlft"><br />&nbsp;<br />: {{ $pindahsubinv->moNumber }}</td>       
        </tr>
        <tr><td class="txlft">Rev </td><td class="txlft">: </td></tr>
        <tr><td class="txlft">Tgl </td><td class="txlft">: {{ date("d F Y") }}</td></tr>
        <tr><td class="txlft">Hal  </td><td class="txlft">: 1 dari {{ $totalPageCount }}</td></tr>        
        </table>


    </td>
</tr>
<tr><td class="" style="">
    <br />&nbsp;<br />
    <table style="" border="0" cellpadding="0" cellspacing="4">
    <tr><td width="8%" >GRN No.</td> <td width="92%" >: </td></tr>
    <tr><td width="8%" >Tanggal</td> <td width="92%" >: {{ date("d F Y") }}</td></tr>
    <tr><td width="8%" >Kapal</td> <td width="92%" >: {{ $pindahsubinv->namaKapal }}</td></tr>
    <tr><td width="8%" >Menu</td> <td width="92%" >: Pagi / Siang / Malam</td></tr>
    </table>    
    <br />
</td></tr>
<tr><td>
    <table cellpadding="3" class="bdrtop bdrlef" cellspacing="0">
        <thead><tr>
            <td class="bdrr8 bdrbot txctr txbold" rowspan="2" width="30"><span style="font-size:10px;">No</span></td>
            <td class="bdrr8 bdrbot txctr txbold" width="230" rowspan="2"><span style="font-size:10px;">Deskripsi / Item</span></td>
            <td class="bdrr8 bdrbot txctr txbold" colspan="2" width="120">Jumlah</td>
            <td class="bdrr8 bdrbot txctr txbold" rowspan="2" width="140"><span style="font-size:10px;">Keterangan</span></td>
        </tr>
        <tr><td class="bdrr8 bdrbot txctr txbold" width="60">Diterima</td>
            <td class="bdrr8 bdrbot txctr txbold" width="60" >Satuan</td></tr>
        </thead>
        <tbody>
        <?php $i = 1; foreach($listItems as $item) { ?>
        <tr nobr="true">
            <td class="bdrr8 txctr bdrbot bdrlef">{{ $i++ }}</td>
            <td class="bdrr8 bdrbot">{{ $item->deskripsi }}</td>
            <td class="bdrr8 bdrbot txctr">{{ floatval($item->quantity) }}</td>
            <td class="bdrr8 bdrbot txctr">{{ $item->UOM }}</td>
            <td class="bdrr8 bdrbot">&nbsp;</td>
        </tr>
        <?php } ?>
        </tbody>
    </table>
</td></tr>
</table>