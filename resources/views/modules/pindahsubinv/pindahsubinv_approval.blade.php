@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Pindah Barang Sub-Inventory: Detail</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
            <div class="card-body content-box-body">
			@include('includes.include_error_prop')
				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#moNumber">Mo Number</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default" placeholder="" name="moNumber" value="{{ $pindahsubinv->moNumber }}" title="moNumber">
					</div>
					<div class="col-sm-2 col-xs-5">
						<label for="#dateRequired">Tanggal Dibutuhkan</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default" required placeholder="Masukkan tanggal transaksi" name="dateRequired" value="{{ $pindahsubinv->dateRequired }}" title="deskripsi">
					</div>
				</div><br />
				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#fromLocationId">Tempat Asal</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default" required placeholder="Masukkan tanggal transaksi" name="fromLocationName" value="{{ $pindahsubinv->fromLocationName }}">					
					</div>
					<div class="col-sm-2 col-xs-5">
						<label for="#fromLocationId">Tujuan Account</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default" required placeholder="Masukkan tanggal transaksi" name="toLocationName" value="{{ $pindahsubinv->toLocationName }}">	
					</div>
				</div><br />
				<div class="clearfix">&nbsp;</div>
				<div class="clearfix">&nbsp;</div>
				<div class="row justify-content-between">
					<div class="col-4"><h5>Daftar Pindah Barang</h5></div>
				</div> 
				<br />
				<div class="row">
					<div class="col-sm-1 col-xs-1">&nbsp;</div>
					<div class="col-sm-10 col-xs-1">
						<div class="table-responsive">
							<table id="tableItem" class="table table-hover table-bordered table-striped widget-table">
								<tr class="bg-primary text-light">
									<th>Barang</th>
									<th>Kategori</th>
									<th>Deskripsi</th>
									<th>UOM</th>
									<th>Qty</th>
									<th>Lot Number</th>
								</tr>

								@foreach($listItems as $item)
								<tr class="" >
									<td>{{ $item->namaItem }}</td>
									<td>{{ $item->category }}</td>
									<td>{{ $item->deskripsi }}</td>
									<td>{{ $item->UOM }}</td>
									<td>{{ $item->quantity }}</td>
									<td>{{ $item->lotNumber }}</td>
								</tr>
								@endforeach
							</table>
						</div> 
					</div>
				</div>
						
				
				<br />
				<div class="clearfix">&nbsp;</div><div class="clearfix">&nbsp;</div>

				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<h5>Komentar</h5>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-xs-12">
						<textarea disabled class="form-control input-default" placeholder="Masukkan komentar" name="komentar" title="komentar">{{ $pindahsubinv->komentar }}</textarea>
					</div>
				</div>		
				<div class="clearfix">&nbsp;</div>	

				<div class="row">
					<div class="col-sm-6 col-xs-6 text-right">
						<form action="{{url("administrator/pindahsubinv/approval/")}}" role="form" method="POST" enctype="multipart/form-data">
							<input type="hidden" name="_method" value="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="moNumber" value="{{ $pindahsubinv->moNumber }}">
							<input type="hidden" name="status" value="1">
							<input type="hidden" name="lokasiId" value="{{ $pindahsubinv->toLocationId }}">
							<input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">
							<input type="hidden" name="approverId" value="{{ WorkerAuth::auth()->getAuth()->id }}">				
							<button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Setujui</button> &nbsp;					
						</form>
					</div>
					<div class="col-sm-6 col-xs-6">
						<form action="{{url("administrator/pindahsubinv/approval/")}}" role="form" method="POST" enctype="multipart/form-data">
							<input type="hidden" name="_method" value="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="moNumber" value="{{ $pindahsubinv->moNumber }}">
							<input type="hidden" name="status" value="2">	
							<input type="hidden" name="lokasiId" value="{{ $pindahsubinv->toLocationId }}">	
							<input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">	
							<input type="hidden" name="approverId" value="{{ WorkerAuth::auth()->getAuth()->id }}">
							<button type="submit" class="btn btn-primary"><i class="fa fa-close"></i> Tolak</button> &nbsp;	 &nbsp; &nbsp;  &nbsp; 
							<a class="btn btn-primary" href="{{ URL::previous() }}" title="Back"><span class="fa fa-undo"></span> Kembali</a>
						</form>
					</div>
				</div>	
            </div>
        </div>
    </div>
    @include('includes.includes_footer')
@endsection