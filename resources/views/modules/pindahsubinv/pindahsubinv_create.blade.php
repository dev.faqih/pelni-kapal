@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">        
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Buat Pindah Barang Sub-Inventory</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
		</div>
		<div class="clearfix">&nbsp;</div>

			<form autocomplete="off" class="submitTransaction" id="formPindahSubInv" action="{{url("administrator/pindahsubinv")}}" role="form" method="POST" enctype="multipart/form-data">
            <div class="container border rounded bg-light">
            	<div class="clearfix">&nbsp;</div>
          		@include('includes.include_error_prop')
                
				<input type="hidden" name="_method" value="POST">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="status" value="0">
				<input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">
				<input type="hidden" name="kodeKapal" value="{{ WorkerAuth::auth()->getAuth()->kodeKapal }}">
				<input type="hidden" name="requesterId" value="{{ WorkerAuth::auth()->getAuth()->id }}">
				<input type="hidden" name="tipeTransaksi" value="MO">
				<input type="hidden" name="urlTransaksi" value="pindahsubinv">
				<input type="hidden" name="namaTransaksi" value="Pindah Barang Sub-Inventory">
				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#fromLocationId">Sub-Inventory Asal</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<select class="form-control input-default" id="fromLocationId" name="fromLocationId" title="Sub-Inventory Asal">
							@foreach($lokasiitems as $lokasiitem)
								<option value="{{ $lokasiitem->id }}">{{ $lokasiitem->namaLokasi }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-sm-2 col-xs-5">
						<label for="#fromLocationId">Sub-Inventory Tujuan</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<select class="form-control input-default" id="toLocationId" name="toLocationId" title="Sub-Inventory Tujuan">							
						</select>
					</div>
				</div><br />

				<div class="row">
					
					<div class="col-sm-2 col-xs-5">
						<label for="#dateRequired">Tanggal Dibutuhkan</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" class="form-control datepicker input-default" value="{{ date('Y-m-d') }}" data-date-clear-btn="true" data-date-autoclose="true" data-date-format="yyyy-mm-dd" required placeholder="Masukkan tanggal transaksi" name="dateRequired" title="deskripsi" OnChange="checkIsMoreThan3Days('dateRequired', '{{ date("Y-m-d H:i:s") }}','{{ WorkerAuth::auth()->getAuth()->sysParams->BACKDATE }}');">
					</div>
				</div><br />
				<div class="clearfix">&nbsp;</div>
			</div>
			<div class="clearfix">&nbsp;</div>				
			<div class="container border rounded bg-light">
				<div class="clearfix">&nbsp;</div>
				<div class="row justify-content-between">
					<div class="col-sm-4 col-xs-6"><h5>Daftar Barang</h5></div>
					<div class="col-sm-3 text-right col-xs-5">
						<button type="button" id="loadItemModal" class="btn btn-primary">Tambah Item</button>
					</div>
				</div><br />
				<div class="table-responsive">
					<table id="tableItem" class="table table-hover table-striped widget-table">
					<thead>
						<tr>	
							<th>No</th>
							<th>Kode Barang</th>
							<th>Part Number</th>
							<th>Deskripsi</th>
							<th>Kategori</th>
							<th>UOM</th>
							<th>Qty</th>
							<th>Lot Number</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody></tbody>
					</table>
				</div><br />

				<div class="clearfix">&nbsp;</div>				
			</div>
			<div class="clearfix">&nbsp;</div>				
			<div class="container border rounded bg-light">
				<div class="clearfix">&nbsp;</div>


				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<h5>Komentar</h5>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-xs-12">
					<textarea class="form-control input-default" placeholder="Masukkan komentar" required name="komentar" title="komentar">{{old("komentar")}}</textarea>
					</div>
				</div>		
				<div class="clearfix">&nbsp;</div>
				<div class="row">
					<div class="col-sm-12 col-xs-12">
						<label>
							<span><input class="form-input" type="checkbox" value="" id="invalidCheck" required> &nbsp; </span><span>Saya setuju untuk melanjutkan proses ini.</span>
						</label>
					</div>
				</div><br />	
				<p class="text-center">
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Kirim</button> &nbsp;
					<a class="btn btn-primary" href="{{ URL::previous() }}" title="Back"><span class="fa fa-undo"></span> Kembali</a>
				</p>
			</div>
			</form>
        </div>
    </div>
    @include('includes.includes_footer')
	@include('includes.include_modal_pindahsubinv')
    <script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>

	<script>
		$(document).ready(function() {
			loadDropDown();
			$('.datepicker').datepicker('update', '{{ date('Y-m-d') }}');
		});

		var valueBeforeChange = 0;
		$("#fromLocationId").on('focus', function () {
			// Store the current value on focus and on change
			valueBeforeChange = this.value;
		}).change(function() {			
			if(itemList.length > 0) {
				$("#defModalTitle").html("Ubah Sub-Inventory asal ?");
				$("#defModalBody").html("Dengan mengubah sub-inventory asal, akan menghapus semua item yang sudah ada di list.<br />&nbsp;<br /> Apakah anda yakin ingin mengubah sub-inventory asal?");
				$("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" onClick=\"rollBackModal()\">Cancel</button><button type=\"button\" onClick=\"reLoadDropDown()\" class=\"btn btn-primary\">Yes</button>");
				$('#defModal').modal('show');
			} else {
				loadDropDown();
			}
		});

		var rollBackModal = function(){
			$("#fromLocationId").val(valueBeforeChange);
			$('#defModal').modal('hide');
		}

		var reLoadDropDown = function() {
			loadDropDown();
			$('#defModal').modal('hide');
		};

		$(document).ready(function() {
            $('.datepicker').datepicker('update', '{{ date('Y-m-d') }}');
        });

		var loadDropDown = function() {
			var lokasiId = $("#fromLocationId").val();
			$.ajax ({
				type: 'GET',
				url: '{{ url("administrator/pindahsubinv/getSubInvTujuan") }}',
				dataType: "json",
				data: { lokasiId : lokasiId },
				success : function(result) {
					$("#toLocationId").html("");
					$.each(result, function(idx, data) {
						$("#toLocationId").append('<option value="' + data.id + '">' + data.namaLokasi + '</option>');
					});
					$("#tableItem tbody").html("");
					itemList = [];
				}
			});
		}
	</script>
@endsection