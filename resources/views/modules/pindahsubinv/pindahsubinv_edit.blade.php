@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        @include('includes.include_breadcrumb')
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Edit data of{{$title}}</h4></div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{url("administrator/pindahstok")}}" title="Back"><span class="fa fa-undo"></span> Back</a>
                </div>
            </div>
            <div class="card-body content-box-body">
            @include('includes.include_error_prop')

                <form role="form" action="{{url("administrator/pindahstok/$pindahstok->id/update")}}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="form-group">
					<label for="#fromKapalId">FromKapal</label>
					<select class="form-control input-default"  name="fromKapalId" title="fromKapalId">
						@foreach($kapals as $kapal)
							<option value="{{$kapal->id}}"@if($pindahstok->fromKapalId== $kapal->id) selected @endif>
								{{ $kapal->namaKapal }}</option>
						@endforeach
					</select>
					</div>

					<div class="form-group">
					<label for="#toKapalId">ToKapal</label>
					<select class="form-control input-default"  name="toKapalId" title="toKapalId">
						@foreach($kapals as $kapal)
							<option value="{{$kapal->id}}"@if($pindahstok->toKapalId== $kapal->id) selected @endif>
								{{ $kapal->namaKapal }}</option>
						@endforeach
					</select>
					</div>

					<div class="form-group">
					<label for="#fromLocationId">FromLocation</label>
					<select class="form-control input-default"  name="fromLocationId" title="fromLocationId">
						@foreach($lokasiitems as $lokasiitem)
							<option value="{{$lokasiitem->id}}"@if($pindahstok->fromLocationId== $lokasiitem->id) selected @endif>
								{{ $lokasiitem->namaLokasi }}</option>
						@endforeach
					</select>
					</div>

					<div class="form-group">
					<label for="#toLocationId">ToLocation</label>
					<select class="form-control input-default"  name="toLocationId" title="toLocationId">
						@foreach($lokasiitems as $lokasiitem)
							<option value="{{$lokasiitem->id}}"@if($pindahstok->toLocationId== $lokasiitem->id) selected @endif>
								{{ $lokasiitem->namaLokasi }}</option>
						@endforeach
					</select>
					</div>

					<div class="form-group">
					<label for="#approverId">Approver</label>
					<select class="form-control input-default"  name="approverId" title="approverId">
						@foreach($userss as $users)
							<option value="{{$users->id}}"@if($pindahstok->approverId== $users->id) selected @endif>
								{{ $users->name }}</option>
						@endforeach
					</select>
					</div>

					<div class="form-group">
					<label for="#requesterId">Requester</label>
					<select class="form-control input-default"  name="requesterId" title="requesterId">
						@foreach($userss as $users)
							<option value="{{$users->id}}"@if($pindahstok->requesterId== $users->id) selected @endif>
								{{ $users->name }}</option>
						@endforeach
					</select>
					</div>

					<div class="form-group">
					<label for="#type">Type</label>
					<input type="text"  class="form-control input-default "  placeholder="Masukkan type" name="type" value="{{$pindahstok->type}}" title="type">
					</div>

                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                </form>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
@endsection