@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Pindah Barang Sub-Inventory</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>                    
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>



        <div class="container border rounded bg-light">
            <div class="clearfix">&nbsp;</div>
            @if(WorkerAuth::auth()->getAuth()->aplikasi == "KAPAL")
            <div class="row">
                <div class="col-sm-12 text-right">
                    <a class="btn btn-danger" href="{{ url("administrator/pindahsubinv/create") }}" title="Create Data"><span class="fa fa-plus"></span>&nbsp; Buat Pindah SubInv </a>                        
                </div>
            </div>
            <div class="clearfix">&nbsp;</div>
            @endif
            @include('includes.include_error_prop')
            <div class="row">
                <div class="col-sm-2 col-xs-5">No. MO</div>
                <div class="col-sm-3 col-xs-7">
                    <input class="form-control input-default search-value" name="moNumber" title="Search Value">
                </div>
                <div class="col-sm-2 col-xs-5">Tanggal Dibutuhkan</div>
                <div class="col-sm-3 col-xs-7">
                    <input class="form-control input-default datepicker search-value" data-date-clear-btn="true" data-date-format="yyyy-mm-dd" data-date-autoclose="true" name="dateRequired" title="Search Value">
                </div>
            </div><br />
            <div class="row">
                <div class="col-sm-2 col-xs-5">
                    <label for="#fromLocationId">Sub-Inventory Asal</label>
                </div>
                <div class="col-sm-3 col-xs-7">
                    <select class="form-control input-default search-value"  name="fromLocationId" title="fromLocationId">
                        <option value=""></option>
                        @foreach($lokasiitems as $lokasiitem)
                            <option value="{{ $lokasiitem->id }}">{{ $lokasiitem->namaLokasi }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-2 col-xs-5">
                    <label for="#toLocationId">Sub-Inventory Tujuan</label>
                </div>
                <div class="col-sm-3 col-xs-7">
                    <select class="form-control input-default search-value"  name="toLocationId" title="toLocationId">
                        <option value=""></option>
                        @foreach($lokasiitems as $lokasiitem)
                            <option value="{{ $lokasiitem->id }}">{{ $lokasiitem->namaLokasi }}</option>
                        @endforeach
                    </select>
                </div>
            </div><br />
            <div class="row">
                <div class="col-sm-2">&nbsp;</div>
                <div class="col-sm-10">
                    <button type="button" class="btn btn-primary " onClick="searchList()" name="search-btn-pagination" title="Search"><span class="fa fa-search"></span> Search</button> &nbsp;
                    <button type="button" class="btn btn-primary  " name="clear-search" title="Reset Search"><span class="fa fa-trash-o"></span> Reset</button>
                </div>
            </div><br />

        </div>
        <div class="clearfix">&nbsp;</div>
        <div class="container border rounded bg-light">
            <div class="card-title content-box-title add-space">
                <div class="row">
                    <div class="col-md-12 col-xs-12 ">
                        <div class="pull-left">
                            <div class="addon-box-list">
                                <div class="form-group-inline">
                                    <span class="addon">Show</span>
                                        <select class="form-control" name="size" title="Total List">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span class="addon">Data</span>
                                </div>
                            </div>
                        </div>
                        <div class="pull-right">
                            <div class="addon-box-list add-space-left  ">
                                <div class="form-group-inline">
                                    <span class="addon ">Ordering by</span>
                                    <select class="form-control" title="Sorting Data" name="orderingBy">
                                        <option value="0" selected>Sorting By</option>
                                        @foreach($ordering as $key => $value)
                                            <option value="{{ $key }}">{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table id="pindahsubinv" class="table table-hover table-striped widget-table"name="table-index">
                    <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th class="header-pagination">No Transaksi</th>
                        <th class="header-pagination">Sub-Inventory Asal</th>
                        <th class="header-pagination">Sub-Inventory Tujuan</th>
                        <!-- <th class="header-pagination">Tipe</th> -->
                        <th class="header-pagination">Total Barang</th>
                        <th class="header-pagination">Tanggal Dibutuhkan</th>
                        <th class="header-pagination">Status</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($pindahsubinvs["rows"]as $pindahsubinv)
                    <tr>
                        <td><a data-toggle="tooltip" data-placement="top" title="View" href="{{ url("administrator/pindahsubinv/$pindahsubinv->id") }}"class="btn btn-outline-primary"><span class="fa fa-eye"></span></a></td>
                        <td>{{ $pindahsubinv->moNumber }}</td>
                        <td>{{ $pindahsubinv->fromLocationName }}</td>
                        <td>{{ $pindahsubinv->toLocationName }}</td>
                        <td>{{ floatval($pindahsubinv->quantity) }}</td>
                        <td><?php if($pindahsubinv->dateRequired) { echo date("Y-m-d", strtotime($pindahsubinv->dateRequired)); } ?></td>
                        <td><?php if($pindahsubinv->status > 1) { echo "Rejected";  } else if($pindahsubinv->status > 0) { echo "Approved"; } else { echo "Open"; } ?></td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
            <div class=" content-box-footer">
                <div class="row">
                    <div class="col-md-6 text-left col-xs-12">
                    Page {{ $pindahsubinvs["page"] }} of <span id="pageCount">{{ $pindahsubinvs["pageCount"] }}</span>
                    Total Row <span id="totalRow">{{ $pindahsubinvs["rowCount"] }}</span>
                    </div>
                    <div class="col-md-6  col-xs-12">
                        <ul class="pagination pull-right">
                            <li class="page-item ">
                                <button class="page-link" tabindex="-1" name="first" title="First Page">First
                                </button>
                            </li>
                            <li class="page-item" id="pagination-prev">
                                <a class="page-link" title="Prev Page">
                                    <i class="fa fa-step-backward"></i></a>
                            </li>
                            <li class="page-item" id="pagination-next">
                                <a class="page-link" title="Next Page">
                                    <i class="fa fa-step-forward"></i></a></li>
                            <li class="page-item">
                                <button class="page-link" name="last" title="Last Page">Last</button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes.includes_footer')
     <script>
             <?php $pindahsubinvs["rows"] = []; ?>
            var paginationParameter = ('{!! json_encode($pindahsubinvs)!!}');
            var currentUrl = "{{ url($currentPrefix) }}";
        </script>
    <script type="text/javascript" src="{{ URL::asset("/assets/js/pagination.js") }}"></script>
@endsection