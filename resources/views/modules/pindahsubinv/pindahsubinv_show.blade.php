@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Pindah Barang Sub-Inventory: Detail</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
		</div>
		<div class="clearfix">&nbsp;</div>
		<div class="container border rounded bg-light">
		<div class="clearfix">&nbsp;</div>
			@include('includes.include_error_prop')
			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#moNumber">Mo Number</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" readonly class="form-control input-default" placeholder="" name="moNumber" value="{{ $pindahsubinv->moNumber }}" title="moNumber">
				</div>
			</div><br />
			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#moNumber">Status</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" readonly class="form-control input-default" placeholder="" name="moNumber" value="<?php if($pindahsubinv->status > 1) { echo "Rejected"; } else if($pindahsubinv->status > 0) { echo "Approved"; } else { echo "Open"; } ?>" title="moNumber">
				</div>
				<div class="col-sm-2 col-xs-5">
					<label for="#dateRequired">Tanggal Dibutuhkan</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" readonly class="form-control input-default"  name="dateRequired" value="{{ date('Y-m-d', strtotime($pindahsubinv->dateRequired))  }}" title="deskripsi">
				</div>
			</div><br />
			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#fromLocationId">Tempat Asal</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" readonly class="form-control input-default"  name="fromLocationName" value="{{ $pindahsubinv->fromLocationName }}">
				</div>
				<div class="col-sm-2 col-xs-5">
					<label for="#toLocationId">Tujuan Account</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" readonly class="form-control input-default"  name="toLocationName" value="{{ $pindahsubinv->toLocationName }}">	
				</div>
			</div><br />
			<div class="clearfix">&nbsp;</div>

		</div>
		<div class="clearfix">&nbsp;</div>
		<div class="container border rounded bg-light">

			<div class="clearfix">&nbsp;</div>
			<div class="row justify-content-between">
				<div class="col-4"><h5>Daftar Pindah Barang</h5></div>
			</div> 
			<br />
			<div class="row">
				<div class="col-sm-12 col-xs-1">
					<div class="table-responsive">
						<table id="tableItem" class="table table-hover table-bordered table-striped widget-table">
						<thead>
							<tr class="bg-primary text-light">
								<th>No</th>
								<th>Kode Barang</th>
								<th>Part Number</th>
								<th>Deskripsi</th>
								<th>Kategori</th>
								<th>UOM</th>								
								<th>Lot Number</th>
								<th>Qty</th>
								@if(WorkerAuth::auth()->getAuth()->aplikasi == "KAPAL")
									@if($pindahsubinv->status == "0") <th>Qty in Stock</th> @endif
								@endif
							</tr>
						</thead>
						<tbody>
							<?php $i = 1; ?>
							<?php $approvable = true;
							foreach($listItems as $item) {  
								if($item->quantity > $item->qtyStock && $pindahsubinv->status == "0" && WorkerAuth::auth()->getAuth()->aplikasi == "KAPAL") {
									$approvable = false; 
									echo "<tr class='text-danger font-italic'>";
								} else {
									echo "<tr>";
								} ?>					
									<td>{{ $i++ }}</td>			
									<td>{{ $item->namaItem }}</td>
									<td>{{ $item->part_number }}</td>
									<td>{{ $item->deskripsi }}</td>
									<td>{{ $item->category }}</td>
									<td>{{ $item->UOM }}</td>
									<td>{{ $item->lotNumber }}</td>
									<td class="text-center">{{ floatval($item->quantity) }}</td>
									@if(WorkerAuth::auth()->getAuth()->aplikasi == "KAPAL")							
										@if($pindahsubinv->status == "0") <td class="text-center">{{ floatval($item->qtyStock) }}</td> @endif
									@endif
								</tr>
							<?php } ?>
						</tbody>
						</table>
					</div> 
				</div>
			</div>
					
			
			<br />
			<div class="clearfix">&nbsp;</div>

		</div>
		<div class="clearfix">&nbsp;</div>
		<div class="container border rounded bg-light">

			<div class="clearfix">&nbsp;</div>
			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<h5>Komentar</h5>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-xs-12">
					<textarea readonly class="form-control input-default" placeholder="Masukkan komentar" name="komentar" title="komentar">{{ $pindahsubinv->komentar }}</textarea>
				</div>
			</div>		
			<div class="clearfix">&nbsp;</div>			

			@if($pindahsubinv->status > 0)
			<div class="row">
				<div class="col-sm-2 col-xs-5"><h5>Feedback</h5></div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-xs-12">
					<textarea readonly class="form-control input-default" placeholder="Masukkan Feedback" name="feedback" title="feedback">{{ $pindahsubinv->feedback }}</textarea>
				</div>
			</div>					
			<div class="clearfix">&nbsp;</div>
			@endif

			@if(isset($isApproval) && $isApproval)

				@if($pindahsubinv->status > 0)
					<div class="row">
						<div class="col-sm-12 col-xs-12 text-center">
							<a class="btn btn-primary" href="{{ URL::previous() }}" title="Back"><span class="fa fa-undo"></span> Kembali</a>
						</div>
					</div>
				@else
					@if(WorkerAuth::auth()->getAuth()->aplikasi == "KAPAL")
					<div class="clearfix">&nbsp;</div>
					<div class="container border rounded bg-light">
						<div class="clearfix">&nbsp;</div>

						<form id="formApproval" action="{{url("administrator/pindahsubinv/approval/")}}" role="form" method="POST" enctype="multipart/form-data">
							<input type="hidden" name="_method" value="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="moNumber" value="{{ $pindahsubinv->moNumber }}">
							<input type="hidden" name="status" value="2">	
							<input type="hidden" name="lokasiId" value="{{ $pindahsubinv->toLocationId }}">	
							<input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">	
							<input type="hidden" name="approverId" value="{{ WorkerAuth::auth()->getAuth()->id }}">
							
							<div class="row">
								<div class="col-sm-2 col-xs-5"><h5>Feedback</h5></div>
							</div>
							<div class="row">
								<div class="col-sm-12 col-xs-12">
									<textarea class="form-control input-default" required placeholder="Masukkan Feedback" name="feedback" title="feedback">{{ $pindahsubinv->feedback }}</textarea>
								</div>
							</div>					
							<div class="clearfix">&nbsp;</div>
							
							<div class="row">
								<div class="col-sm-12 col-xs-12 text-center">											
									<button type="button" @if(! $approvable) disabled @endif onClick="submitFormApproval()" class="btn btn-primary"><i class="fa fa-check"></i> &nbsp;Setujui</button> &nbsp;			
									<button type="submit" class="btn btn-primary"><i class="fa fa-close"></i> &nbsp;Tolak</button> &nbsp; 
									<a class="btn btn-primary" href="{{ URL::previous() }}" title="Back"><span class="fa fa-undo"></span> Kembali</a>		
									
								</div>
							</div>
							<div class="clearfix">&nbsp;</div>
						</form>
					</div>
					@endif
				@endif
				<div class="clearfix">&nbsp;</div>
			@else
				<p class="text-center">
					<a class="btn btn-primary" href="{{ URL::previous() }}" title="Back"><span class="fa fa-undo"></span> Kembali</a>									&nbsp; 
					<a class="btn btn-primary" href="{{url("administrator/pindahsubinv/gandroom/".$pindahsubinv->id)}}" target="_blank" title="Download PDF"><span class="fa fa-print"></span> Cetak Laporan </a>	
				</p>
			@endif
		</div>
    </div>
    @include('includes.includes_footer')
@endsection