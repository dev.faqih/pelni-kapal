@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">        
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Daftar PO Masuk</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>

        <div class="container border rounded bg-light">
            <div class="clearfix">&nbsp;</div>
            @include('includes.include_error_prop')
            <div class="row">
                <div class="col-sm-2 col-xs-12">No. PO</div>
                <div class="col-sm-3 col-xs-12">
                    <input class="form-control input-default search-value" name="poNumber" title="Search Value">
                </div>
                <div class="col-sm-2 col-xs-12">Tanggal PO</div>
                <div class="col-sm-3 col-xs-12">
                    <input class="form-control input-default datepicker search-value"  data-date-clear-btn="true" data-date-format="yyyy-mm-dd" data-date-autoclose="true" name="date" title="">
                </div>
            </div><br />
            <div class="row">
                <div class="col-sm-2 col-xs-12">Barang<br /><span style="color:grey;font-size:smaller;">(Kode/Nama/Part No.)</span></div>
                <div class="col-sm-3 col-xs-12">
                    <input class="form-control input-default search-value" name="namaItem" title="Search Value">
                </div>
                <div class="col-sm-2 col-xs-12">Status</div>
                <div class="col-sm-3 col-xs-12">
                    <select class="form-control input-default search-value" name="status">
                        <option value=""></option><option value="0">Open</option><option value="1">Close</option>
                    </select>
                </div>
            </div><br />
            @if(WorkerAuth::auth()->getAuth()->aplikasi == "KAPAL")
            <div class="row">
                <div class="col-sm-2 col-xs-12">Voyage</div>
                <div class="col-sm-3 col-xs-12">
                    <select class="form-control input-default select2 search-value" name="voyageKode">
                        <option value=""></option>
                        @foreach($voyages as $voyage)
                            <option value="{{ $voyage->voyageKode }}">{{ $voyage->voyageName . " - " . $voyage->tahun }} </option>
                        @endforeach
                    </select >
                </div>
            </div><br />
            @endif
            <div class="row">
                <div class="col-sm-2">&nbsp;</div>
                <div class="col-sm-10">
                    <button type="button" class="btn btn-primary " onClick="searchList()" name="search-btn-pagination" title="Search"><span class="fa fa-search"></span> Search</button> &nbsp;
                    <button type="button" class="btn btn-primary  " name="clear-search" title="Reset Search"><span class="fa fa-trash-o"></span> Reset</button>
                </div>
            </div><br />
        </div>
        <div class="clearfix">&nbsp;</div>

        <div class="container border rounded bg-light">

            <div class="card-title content-box-title add-space">
                <div class="row">
                    <div class="col-md-12 col-xs-12 ">
                        <div class="pull-left">
                            <div class="addon-box-list">
                                <div class="form-group-inline">
                                    <span class="addon">Show</span>
                                        <select class="form-control" name="size" title="Total List">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span class="addon">Data</span>
                                </div>
                            </div>
                        </div>
                        <div class="pull-right">
                            <div class="addon-box-list add-space-left  ">
                                <div class="form-group-inline">
                                    <span class="addon ">Ordering by</span>
                                    <select class="form-control" title="Sorting Data" name="orderingBy">
                                        <option value="0" selected>Sorting By</option>
                                        @foreach($ordering as $key => $value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table id="popenerimaan" class="table table-hover table-striped widget-table"name="table-index">
                    <thead>
                    <tr>
                        <!-- <th><input type="checkbox" name="selectAll"></th> -->
                        <th>&nbsp;</th>
                        <th class="header-pagination">No PO</th>
                        <th class="header-pagination">Tanggal PO</th>
                        <th class="header-pagination">No PR</th>
                        <th class="header-pagination">Kapal</th>
                        <th class="header-pagination">Status</th>
                        <th class="header-pagination">Qty Item</th>
                        <th class="header-pagination">Qty Receipt</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($popenerimaans["rows"] as $popenerimaan)
                    <tr>
                        <!-- <td><input type="checkbox" value="{{$popenerimaan->poNumber}}" name="checkbox-del[]" title="poNumber"></td> -->
                        <td><a data-toggle="tooltip" data-placement="top" title="View" href="{{url("administrator/popenerimaan/$popenerimaan->id")}}"class="btn btn-outline-primary"><span class="fa fa-eye"></span></a></td>
                            <td>{{ $popenerimaan->poNumber }}</td>
                            <td><?php if($popenerimaan->date) { echo date("Y-m-d", strtotime($popenerimaan->date)); } ?></td>
                            <td>@if(intval($popenerimaan->prNumberID) > 0 ) <a href="{{ url("administrator/permintaanbarang/$popenerimaan->prNumberID") }}" target="_blank"> @endif {{ $popenerimaan->prNumber }} @if(intval($popenerimaan->prNumberID) > 0) </a> @endif</td>
                            <td>{{ $popenerimaan->namaKapal }}</td>
                            <td>{{ ($popenerimaan->status > 0) ? "Close" : "Open" }}</td>
                            <td>{{ floatval($popenerimaan->quantity) }}</td>
                            <td>{{ floatval($popenerimaan->qtyReceiptNumber) }}</td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
            <div class=" content-box-footer">
                <div class="row">
                    <div class="col-md-6 text-left col-xs-12">
                    Page {{$popenerimaans["page"]}} of <span id="pageCount">{{$popenerimaans["pageCount"]}}</span>
                    Total Row <span id="totalRow">{{$popenerimaans["rowCount"]}}</span>
                    </div>
                    <div class="col-md-6  col-xs-12">
                        <ul class="pagination pull-right">
                            <li class="page-item ">
                                <button class="page-link" tabindex="-1" name="first" title="First Page">First
                                </button>
                            </li>
                            <li class="page-item" id="pagination-prev">
                                <a class="page-link" title="Prev Page">
                                    <i class="fa fa-step-backward"></i></a>
                            </li>
                            <li class="page-item" id="pagination-next">
                                <a class="page-link" title="Next Page">
                                    <i class="fa fa-step-forward"></i></a></li>
                            <li class="page-item">
                                <button class="page-link" name="last" title="Last Page">Last</button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes.includes_footer')
     <script>
             <?php $popenerimaans["rows"] = []; ?>
            var paginationParameter = ('{!! json_encode($popenerimaans)!!}');
            var currentUrl = "{{url($currentPrefix)}}";
        </script>
    <script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>
@endsection