@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>PO Detail &amp; Daftar Penerimaan Barang</h4></div>
				<div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
		</div>
        <div class="clearfix">&nbsp;</div>
        <div class="container border rounded bg-light">
			<div class="clearfix">&nbsp;</div>			
			@include('includes.include_error_prop')
			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#operatingUnit">No PO</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" disabled class="form-control input-default"  placeholder="PO Number" name="poNumber" value="{{ $popenerimaan->poNumber }}" title="poNumber">
				</div>
				<div class="col-sm-2 col-xs-5">
					<label for="#operatingUnit">No PR</label>
				</div>
				@if(intval($popenerimaan->prNumberID) > 0)
				<div class="input-group col-sm-4 col-xs-7">
					<input type="text" disabled class="form-control input-default"  placeholder="No PR" name="prNumber" value="{{ $popenerimaan->prNumber }}" title="prNumber">
					<div class="input-group-append">
						<a href="{{ url("administrator/permintaanbarang/$popenerimaan->prNumberID") }}" class="text-light btn btn-primary" target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i> Lihat PR</a>
					</div>
				</div>
				@else 
				<div class="col-sm-4 col-xs-7">
					<input type="text" readonly class="form-control input-default"  placeholder="No PR" name="prNumber" value="{{ $popenerimaan->prNumber }}" title="prNumber">
				</div>
				@endif
			</div><br />

			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#operatingUnit">Unit Operasi</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" disabled class="form-control input-default"  placeholder="Unit Operasi" name="operatingUnit" value="{{ $popenerimaan->operatingUnit }}" title="operatingUnit">
				</div>

				<div class="col-sm-2 col-xs-5">
					<label for="#requesterId">Jumlah Quantity</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" disabled class="form-control input-default"  placeholder="Jumlah Quantity" name="quantity" value="{{ number_format($popenerimaan->quantity) }}" title="quantity">
				</div>
			</div><br />


			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#date">Tanggal PO</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" disabled class="form-control input-default"  placeholder="Tanggal PO" name="date" value="<?php if($popenerimaan->date) { echo date("Y-m-d", strtotime($popenerimaan->date)); } ?>" title="date">
				</div>

				<div class="col-sm-2 col-xs-5">
					<label for="#status">Status PO</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" disabled class="form-control input-default"  placeholder="Status PO" name="status" value="{{ ($popenerimaan->status > 0) ? 'Close' : 'Open' }}" title="status">
				</div>
			</div><br />

			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#kapal">Kapal</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" disabled class="form-control input-default"  placeholder="Kapal" name="kapal" value="{{ ($popenerimaan->namaKapal) }}" title="kapal">
				</div>
			</div><br />

		</div>
        <div class="clearfix">&nbsp;</div>
        <div class="container border rounded bg-light">

			<div class="clearfix">&nbsp;</div>
			@if(WorkerAuth::auth()->getAuth()->aplikasi == "KAPAL")
				@if($popenerimaan->status > 0)
				@else
				<div class="row justify-content-between">
					<div class="col-sm-4 col-xs-6">&nbsp;</div>
					<div class="col-sm-3 text-right col-xs-5">
						<a class="btn btn-primary" href="{{ url("administrator/popenerimaan/createpenerimaan/$popenerimaan->id") }}" title="Buat Penerimaan"> Buat Penerimaan</a>
					</div>
				</div><br />
				@endif
			@endif
			<div class="table-responsive">
				<table id="tableItem" class="table table-hover table-striped widget-table">
					<tr>
						<th>&nbsp;</th>
						<th>No Receipt</th>
						<th>Tanggal Receipt</th>
						<th>Qty Diterima</th>							
					</tr>
					@foreach($penerimaanbarangs["rows"] as $penerimaanbarang)
					<tr>
						<td><a href="{{url("administrator/popenerimaan/penerimaan/" . $penerimaanbarang->id) }}"class="btn btn-outline-primary"><span class="fa fa-eye"></span></a></td>
						<td>{{ $penerimaanbarang->receiptNumber}}</td>
						<td><?php if($penerimaanbarang->tanggalPenerimaan) { echo date("Y-m-d", strtotime($penerimaanbarang->tanggalPenerimaan)); } ?></td>
						<td>{{ floatval($penerimaanbarang->qtyDiterima) }}</td>
					</tr>
					@endforeach
				</table>
			</div>
			<div class=" content-box-footer">
                <div class="row">
                    <div class="col-md-6 text-left col-xs-12">
                    Page {{$penerimaanbarangs["page"]}} of <span id="pageCount">{{$penerimaanbarangs["pageCount"]}}</span>
                    Total Row <span id="totalRow">{{$penerimaanbarangs["rowCount"]}}</span>
                    </div>
                    <div class="col-md-6  col-xs-12">
                        <ul class="pagination pull-right">
                            <li class="page-item ">
                                <button class="page-link" tabindex="-1" name="first" title="First Page">First
                                </button>
                            </li>
                            <li class="page-item" id="pagination-prev">
                                <a class="page-link" title="Prev Page">
                                    <i class="fa fa-step-backward"></i></a>
                            </li>
                            <li class="page-item" id="pagination-next">
                                <a class="page-link" title="Next Page">
                                    <i class="fa fa-step-forward"></i></a></li>
                            <li class="page-item">
                                <button class="page-link" name="last" title="Last Page">Last</button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div><br />
        </div>
		<div class="clearfix">&nbsp;</div>
		<div class="container border rounded bg-light">
			<div class="clearfix">&nbsp;</div>
			<div class="row">
				<div class="col-sm-2 col-xs-5"><h5>Catatan</h5></div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-xs-12">
					<textarea disabled class="form-control input-default" placeholder="-" name="komentar" title="komentar">{{ $popenerimaan->comments }}</textarea>
				</div>
			</div>		
			<div class="clearfix">&nbsp;</div>
			<p class="text-center">
				<a class="btn btn-primary" href="{{url("administrator/popenerimaan/")}}" title="Back"><span class="fa fa-undo"></span> Kembali</a>
			</p>
		</div>
    </div>
    @include('includes.includes_footer')
@endsection