@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">        
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Daftarr Penerimaan Barang IO</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
        </div>
		<div class="clearfix">&nbsp;</div>
		<div class="container border rounded bg-light">
            @include('includes.include_error_prop')
            <div class="row">
                <div class="col-sm-2 col-xs-12">No. IO</div>
                <div class="col-sm-3 col-xs-12">
                    <input class="form-control input-default search-value" name="ioNumber" title="Search Value">
                </div>
                <div class="col-sm-2 col-xs-12">Tanggal IO</div>
                <div class="col-sm-3 col-xs-12">
                    <input class="form-control datepicker input-default search-value" name="date" title="" data-date-format="yyyy-mm-dd" data-date-autoclose="true" data-date-clear-btn="true">
                </div>
            </div><br />
            <div class="row">
                <div class="col-sm-2 col-xs-12">Barang<br /><span style="color:grey;font-size:smaller;">(Kode/Nama/Part No.)</span></div>
                <div class="col-sm-3 col-xs-12">
                    <input class="form-control input-default search-value" name="namaItem" title="Search Value">
                </div>
                <div class="col-sm-2 col-xs-12">Status</div>
                <div class="col-sm-3 col-xs-12">
                    <select class="form-control input-default search-value" name="status">
                        <option value=""></option><option value="0">Open</option><option value="1">Close</option>
                    </select>
                </div>
            </div> <br />
            @if(WorkerAuth::auth()->getAuth()->aplikasi == "KAPAL")
            <div class="row">
                <div class="col-sm-2 col-xs-12">Voyage</div>
                <div class="col-sm-3 col-xs-12">
                    <select class="form-control select2 search-value" name="voyageKode">
                        <option value=""></option>
                        @foreach($voyages as $voyage)
                            <option value="{{ $voyage->voyageKode }}">{{ $voyage->voyageName . " - " . $voyage->tahun }} </option>
                        @endforeach
                    </select >
                </div>
            </div><br />
            @endif
            <div class="row">
                <div class="col-sm-2">&nbsp;</div>
                <div class="col-sm-10">
                    <button type="button" class="btn btn-primary " onClick="searchList()" name="search-btn-pagination" title="Search"><span class="fa fa-search"></span> Search</button> &nbsp;
                    <button type="button" class="btn btn-primary  " name="clear-search" title="Reset Search"><span class="fa fa-trash-o"></span> Reset</button>
                </div>
            </div><br />

            
        </div>
		<div class="clearfix">&nbsp;</div>
		<div class="container border rounded bg-light">



            <div class="card-title content-box-title add-space">
                <div class="row">
                    <div class="col-md-12 col-xs-12 ">
                        <div class="pull-left">
                            <div class="addon-box-list">
                                <div class="form-group-inline">
                                    <span class="addon">Show</span>
                                        <select class="form-control" name="size" title="Total List">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span class="addon">Data</span>
                                </div>
                            </div>
                        </div>
                        <div class="pull-right">
                            <div class="addon-box-list add-space-left  ">
                                <div class="form-group-inline">
                                    <span class="addon ">Ordering by</span>
                                    <select class="form-control" title="Sorting Data" name="orderingBy">
                                        <option value="0" selected>Sorting By</option>
                                        @foreach($ordering as $key => $value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table id="popenerimaanio" class="table table-hover table-striped widget-table"name="table-index">
                    <thead>
                    <tr>
                        <!-- <th><input type="checkbox" name="selectAll"></th> -->
                        <th>&nbsp;</th>
                        <th class="header-pagination">No IO</th>
                        <th class="header-pagination">Tanggal IO</th>
                        <th class="header-pagination">Status</th>
                        <th class="header-pagination">Qty Item</th>
                        <th class="header-pagination">Qty Receipt</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($popenerimaanios["rows"]as $popenerimaanio)
                    <tr>
                        <td><a data-toggle="tooltip" data-placement="top" title="View" href="{{url("administrator/popenerimaanio/$popenerimaanio->id")}}"class="btn btn-outline-primary"><span class="fa fa-eye"></span></a></td>
                            <td>{{ $popenerimaanio->ioNumber }}</td>
                            <td><?php if($popenerimaanio->date) { echo date("Y-m-d", strtotime($popenerimaanio->date)); } ?></td>
                            <td>{{ ($popenerimaanio->status > 0) ? "Close" : "Open" }}</td>
                            <td>{{ floatval($popenerimaanio->quantity) }}</td>
                            <td>{{ floatval($popenerimaanio->qtyReceiptNumber) }}</td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
            <div class=" content-box-footer">
                <div class="row">
                    <div class="col-md-6 text-left col-xs-12">
                    Page {{$popenerimaanios["page"]}} of <span id="pageCount">{{$popenerimaanios["pageCount"]}}</span>
                    Total Row <span id="totalRow">{{$popenerimaanios["rowCount"]}}</span>
                    </div>
                    <div class="col-md-6  col-xs-12">
                        <ul class="pagination pull-right">
                            <li class="page-item ">
                                <button class="page-link" tabindex="-1" name="first" title="First Page">First
                                </button>
                            </li>
                            <li class="page-item" id="pagination-prev">
                                <a class="page-link" title="Prev Page">
                                    <i class="fa fa-step-backward"></i></a>
                            </li>
                            <li class="page-item" id="pagination-next">
                                <a class="page-link" title="Next Page">
                                    <i class="fa fa-step-forward"></i></a></li>
                            <li class="page-item">
                                <button class="page-link" name="last" title="Last Page">Last</button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes.includes_footer')
     <script>
             <?php $popenerimaanios["rows"] = []; ?>
            var paginationParameter = ('{!! json_encode($popenerimaanios)!!}');
            var currentUrl = "{{url($currentPrefix)}}";
        </script>
    <script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>
@endsection
