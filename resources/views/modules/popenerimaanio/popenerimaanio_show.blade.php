@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Daftarr Penerimaan Barang IO</h4></div>
				<div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
		</div>
		<div class="clearfix">&nbsp;</div>
		<div class="container border rounded bg-light">
			<div class="clearfix">&nbsp;</div>
			@include('includes.include_error_prop')
			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#operatingUnit">No IO</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" disabled class="form-control input-default"  placeholder="No PO" name="ioNumber" value="{{ $popenerimaanio->ioNumber }}" title="ioNumber">
				</div>
			</div><br />


			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#operatingUnit">Unit Operasi</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" disabled class="form-control input-default"  placeholder="Unit Operasi" name="operatingUnit" value="{{ $popenerimaanio->operatingUnit }}" title="operatingUnit">
				</div>

				<div class="col-sm-2 col-xs-5">
					<label for="#requesterId">Jumlah PO</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" disabled class="form-control input-default"  placeholder="Jumlah PO" name="quantity" value="{{ number_format($popenerimaanio->quantity) }}" title="quantity">
				</div>
			</div><br />


			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#date">Tanggal PO</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" disabled class="form-control input-default"  placeholder="Tanggal PO" name="date" value="<?php if($popenerimaanio->date) { echo date("Y-m-d", strtotime($popenerimaanio->date)); } ?>" title="date">
				</div>

				<div class="col-sm-2 col-xs-5">
					<label for="#status">Status PO</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" disabled class="form-control input-default"  placeholder="Status PO" name="status" value="{{ ($popenerimaanio->status > 0) ? "Close" : "Open" }}" title="status">
				</div>
			</div><br />

		</div>
		<div class="clearfix">&nbsp;</div>
		<div class="container border rounded bg-light">

			<div class="clearfix">&nbsp;</div>
			@if(WorkerAuth::auth()->getAuth()->aplikasi == "KAPAL")
				@if($popenerimaanio->status > 0)
				@else
				<div class="row justify-content-between">
					<div class="col-sm-4 col-xs-6">&nbsp;</div>
					<div class="col-sm-3 text-right col-xs-5">
						<a class="btn btn-primary" href="{{ url("administrator/popenerimaanio/createpenerimaanio/$popenerimaanio->id") }}" title="Buatt Penerimaan"> Buatt Penerimaan</a>
					</div>
				</div><br />
				@endif
			@endif
			<div class="table-responsive">
				<table id="tableItem" class="table table-hover table-striped widget-table">
					<tr>
						<th>&nbsp;</th>
						<th>No Receipt</th>
						<th>Tanggal Receipt</th>
						<th>Qty Diterima</th>							
					</tr>
					@foreach($penerimaanbarangios["rows"] as $penerimaanbarangio)
					<tr>
						<td><a href="{{url("administrator/popenerimaanio/penerimaanio/" . $penerimaanbarangio->id) }}"class="btn btn-outline-primary"><span class="fa fa-eye"></span></a></td>
						<td>{{ $penerimaanbarangio->rcNumber}}</td>
						<td><?php if($penerimaanbarangio->tanggalPenerimaan) { echo date("Y-m-d", strtotime($penerimaanbarangio->tanggalPenerimaan)); } ?></td>
						<td>{{ floatval($penerimaanbarangio->qtyDiterima) }}</td>
					</tr>
					@endforeach
				</table>
			</div>
			<div class=" content-box-footer">
				<div class="row">
					<div class="col-md-6 text-left col-xs-12">
					Page {{$penerimaanbarangios["page"]}} of <span id="pageCount">{{$penerimaanbarangios["pageCount"]}}</span>
					Total Row <span id="totalRow">{{$penerimaanbarangios["rowCount"]}}</span>
					</div>
					<div class="col-md-6  col-xs-12">
						<ul class="pagination pull-right">
							<li class="page-item ">
								<button class="page-link" tabindex="-1" name="first" title="First Page">First
								</button>
							</li>
							<li class="page-item" id="pagination-prev">
								<a class="page-link" title="Prev Page">
									<i class="fa fa-step-backward"></i></a>
							</li>
							<li class="page-item" id="pagination-next">
								<a class="page-link" title="Next Page">
									<i class="fa fa-step-forward"></i></a></li>
							<li class="page-item">
								<button class="page-link" name="last" title="Last Page">Last</button>
							</li>
						</ul>
					</div>
				</div>
			</div><br />
			<p class="text-center">
				<a class="btn btn-primary" href="{{url("administrator/popenerimaanio/")}}" title="Back"><span class="fa fa-undo"></span> Kembali</a>
			</p>
        </div>
    </div>
    @include('includes.includes_footer')
@endsection
