@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>REKAPITULASI PEMAKAIAN BAHAN MAKANAN</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>                    
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>

        <div class="container border rounded bg-light">
            <div class="clearfix">&nbsp;</div>
            @include('includes.include_error_prop')
            <form id="formRekapitulasi"  autocomplete="off" target="_blank" action="{{url("administrator/bahanmakanan")}}" role="form" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="requesterId" value="{{ WorkerAuth::auth()->getAuth()->id }}">
                
                
                <div class="row">
                    <div class="col-sm-5"><h5>Parameter Laporan</h5> </div>
                    <div class="col-sm-1 col-xs-12">&nbsp;</div>
                    <!-- <div class="col-sm-4"><h5>Tampilkan report: </h5></div> -->
                </div><br />
                <div class="row">
                    <div class="col-sm-2 col-xs-5">
                        <label for="#deskripsi">No Surat</label>
                    </div>
                    <div class="col-sm-3 col-xs-7">
                        <input type="text" class="form-control input-default" placeholder="No" name="headerNo" value="" title="No" maxlength="25">
                    </div>
                    <div class="col-sm-1 col-xs-12">&nbsp;</div>                    
                  
                    <div class="col-sm-2 col-xs-10">Kapal</div>
                    <div class="col-sm-4 col-xs-12">
                        @if(WorkerAuth::auth()->getAuth()->aplikasi == "HO")
                            <select class="form-control search-value" id="kapalId" name="kapalId">
                            @foreach($kapals as $kapal)
                                <option value="{{ $kapal->id }}">{{ $kapal->namaKapal }}</option>
                            @endforeach
                            </select >
                        @else
                            <input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}"> {{ WorkerAuth::auth()->getAuth()->namaKapal }}
                        @endif
                    </div>
                </div><br />
                <div class="row">
                    <div class="col-sm-2 col-xs-5">
                        <label for="#partNumber">Rev</label>
                    </div>
                    <div class="col-sm-3 col-xs-7">
                        <input type="text" class="form-control input-default" placeholder="Rev" name="headerRev" value="" title="Rev" maxlength="25">
                    </div>
                    <div class="col-sm-1 col-xs-12">&nbsp;</div> 

                    <div class="col-sm-2 col-xs-10">Rentang Tanggal</div>
                    <div class="col-sm-2 col-xs-4">
                        <input type="text"  class="form-control input-default datepicker" placeholder="Tanggal Mulai" id="dateStart" name="dateStart"
                        required data-date-clear-btn="true" data-date-format="yyyy-mm-dd" data-date-autoclose="true" title="Date Start" >
                    </div>
                    <div class="col-sm-2 col-xs-4">
                        <input type="text"  class="form-control input-default datepicker" placeholder="Tanggal Akhir" id="dateEnd" name="dateEnd"
                        required data-date-clear-btn="true" data-date-format="yyyy-mm-dd" data-date-autoclose="true" title="Date End" >
                    </div>   
                </div><br />
                <div class="row">
                    <div class="col-sm-2 col-xs-5">
                        <label for="#partNumber">Realisasi Jumlah Penumpang</label>
                    </div>
                    <div class="col-sm-3 col-xs-7">
                        <input type="text" required class="form-control input-default" placeholder="Realisasi Jumlah Penumpang" name="jlhOrang" value="" title="Realisasi Jumlah Penumpang" maxlength="7">
                    </div>
                    <div class="col-sm-1 col-xs-12">&nbsp;</div> 

                    <div class="col-sm-2 col-xs-10">Tanggal Voyage</div>
                    <div class="col-sm-4 col-xs-12">
                        <select class="form-control select2 search-value  js-example-basic-single" id="voyageKode" name="voyageKode">                            
                            
                        </select >
                    </div>
                </div><br />

                <div class="row">
                    <div class="col-sm-8">&nbsp;</div>
                    <div class="col-sm-4">
                        <button type="submit" class="btn btn-primary " name="search-btn-pagination" title="Search"><span class="fa fa-search"></span> Search</button> &nbsp;
                        <button type="button" class="btn btn-primary  " name="clear-search" title="Reset Search"><span class="fa fa-trash-o"></span> Reset</button>
                    </div>
                </div><br />
            </form>
        </div>        
    </div>
    @include('includes.includes_footer')
     <script>
            var paginationParameter = "[]";
            var currentUrl = "{{ url($currentPrefix) }}";
        </script>
    <script type="text/javascript" src="{{ URL::asset("/assets/js/pagination.js") }}"></script>
    <script>
    $('.datepicker').change(function() {
        if( $("input[name='dateStart']").val() && $("input[name='dateEnd']").val() ) {
            var dateStart   = new Date( $("input[name='dateStart']").val() ).getTime();
            var dateEnd     = new Date( $("input[name='dateEnd']").val() ).getTime();
            if(dateEnd < dateStart) {                
                $("#voyageDate").html("");
                $("#defModalTitle").html("REKAPITULASI PEMAKAIAN BAHAN MAKANAN");
                $("#defModalBody").html("Masukkan kembali tanggal dengan benar! Tanggak akhir harus lebih besar atau sama dengan tanggal awal.");
                $("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>");
                $('#defModal').modal('show');
                return false;
            } else {
                loadVoyagesByDateRange("{{ url('administrator/bahanmakanan/getVoyagesByDateRange?kapalId='.WorkerAuth::auth()->getAuth()->kapalId) }}", "dateStart", "dateEnd");
            }
        } else {
        }
    });

    $("input[name='dateEnd']").on("focus", function() {
        // this.datepicker('startDate', '{{ date('Y-m-d') }}');
    }).on("change", function() {
        var dateStart   = new Date( $("input[name='dateStart']").val() ).getTime();
        var dateEnd     = new Date( $("input[name='dateEnd']").val() ).getTime();
        if(dateEnd < dateStart) {
            $("input[name='dateEnd']").val("");
        }
    });
   
    $('#formRekapitulasi').submit(function(e) { 
        e.preventDefault();
        e.returnValue = false;
    
        if( $("select[name='voyageKode']").val() ) {
            return this.submit();
        } else {
            $("#defModalTitle").html("REKAPITULASI PEMAKAIAN BAHAN MAKANAN");
            $("#defModalBody").html("Voyage belum dipilih. Silahkan pilih rentang waktu, lalu pilih voyage!");
            $("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>");
            $('#defModal').modal('show');
            return false;
        } 
    });
    </script>
@endsection