<style>
td { font-size:8px; }
.txctr { text-align:center; }
.txlft { text-align:left; }
.txtr8 { text-align:right; }
.txt7px { font-size:7px; }
.txbold { font-weight:bold; }
.bdrtop { border-top:1px solid #000000; }
.bdrbot { border-bottom:1px solid #000000; }
.bdrlef { border-left:1px solid #000000; }
.bdrr8 { border-right:1px solid #000000; }
.pad5 { padding: 5px 5px 5px 5px; }

</style>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
    <td width="100%" class="txctr">
        <table width="100%" style="" class="" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="4" class="txctr" style="" width="16%"><br />&nbsp;<br />
            <span ><img src="{{ url("assets/img/pelni.png") }}" border="0" height="40" style="padding:0px 10px;"></span>
            </td>
            <td width="68%" rowspan="4" class=" txctr"><br />&nbsp;<br />
                <span style="font-size:13px;">FORMULIR</span><br />
                <span style="font-size:13px;">PEMAKAIAN BAHAN MAKANAN</span><br />
            </td>
            <td width="4%" class="txlft"><br />&nbsp;<br />No </td>         
            <td width="14%" class="txlft"><br />&nbsp;<br />: {{ $headerNo }} </td>       
        </tr>
        <tr><td class="txlft">Rev </td><td class="txlft">: {{ $headerRev }}</td></tr>
        <tr><td class="txlft">Tgl </td><td class="txlft">: {{ date("d F Y") }}</td></tr>
        <tr><td class="txlft">Hal  </td><td class="txlft">: 1 dari {{ $totalPageCount }} </td></tr>        
        </table>


    </td>
</tr>
<tr><td class="" style="">
    <br />&nbsp;<br />
    <table style="" border="0" cellpadding="0" cellspacing="4">
    @if(WorkerAuth::auth()->getAuth()->aplikasi == "HO")
        <tr><td width="25%" >Kapal</td> <td width="75%" >: {{ $kapal->namaKapal }}</td></tr>
    @else
        <tr><td width="25%" >Kapal</td> <td width="75%" >: {{ WorkerAuth::auth()->getAuth()->namaKapal }}</td></tr>
    @endif
    <tr><td >Voyage</td> <td >: {{ $voyage->voyageName }} </td></tr>
    <tr><td >Periode</td> <td >: 
    @if($voyage->voyageKode <= 0 ) 
        {{ date("d F Y", strtotime($dateStart)) . " s/d " . date("d F Y", strtotime($dateEnd)) }} 
    @else 
        {{ date("d F Y", strtotime($voyage->voyage_from)) . " s/d " . date("d F Y", strtotime($voyage->voyage_to)) }} 
    @endif </td></tr>
    <tr><td >Realisasi Jumlah Penumpang</td> <td >:  {{ $jlhOrang }} &nbsp; Orang</td></tr>
    </table>    
    <br />
</td></tr>
<tr><td class="" style="">
    <table cellpadding="3" class="" border="1" cellspacing="0">
        <thead><tr class="txbold ">
            <td class="txctr" width="20">No</td>
            <td class="txctr" width="80">Nama Bahan</td>
            <td class="txctr" width="35" >Satuan</td>
            <td class="txctr" width="45">Harga</td>
            <td class="txctr" colspan="2" width="85" >Stok Bahan</td>
            <td class="txctr" colspan="2" width="85" >Suplai</td>
            <td class="txctr" colspan="2" width="85" >Pemakaian</td>
            <td class="txctr" colspan="2" width="85">Sisa Bahan</td>
        </tr>
            
        </thead>
        <tbody>
        <?php $i = 1; $stok = $jlhStok = $suplai = $jlhSuplai = $pemakaian = $jlhPemakaian = $sisa = $jlhSisa = 0;
        foreach($items as $item) {
            $stok = $suplai = $pemakaian = $sisa = 0;
            $stok = $item->sisaawal * $item->harga_satuan;
            $jlhStok += $stok;
            if($item->mutasi == "IN") {
                $suplai = $item->harga;
                $jlhSuplai +=  $suplai;
            } else if($item->mutasi == "OUT") {
                $pemakaian = $item->harga * -1;
                $jlhPemakaian += $pemakaian;
            }
            $sisa = $item->sisaakhir * $item->harga_satuan;
            $jlhSisa += $sisa;

            ?>
        <tr nobr="true">
            <td class=" txt7px txctr  ">{{ $i++ }}</td>
            <td class=" txt7px ">{{ $item->deskripsi }} </td>
            <td class=" txt7px  txctr">{{ $item->UOM }}</td>
            <td class=" txt7px txtr8 ">Rp. {{ $item->harga_satuan }}</td>
            
            <td class=" txt7px txctr " width="27">{{ floatval($item->sisaawal) }}</td>
            <td class=" txt7px txtr8 " width="58">Rp. {{ floatval($stok) }}</td>
            
            <td class=" txt7px txctr " width="27">@if($item->mutasi == "IN") {{ floatval($item->jumlah) }} @endif</td>
            <td class=" txt7px txtr8 " width="58">Rp. @if($item->mutasi == "IN") {{ floatval($item->harga) }} @endif</td>

            <td class=" txt7px txctr " width="27">@if($item->mutasi == "OUT") {{ floatval($item->jumlah * -1) }} @endif</td>
            <td class=" txt7px txtr8 " width="58">Rp. @if($item->mutasi == "OUT") {{ floatval($item->harga * -1) }} @endif</td>

            <td class=" txt7px txctr " width="27">{{ floatval($item->sisaakhir) }}</td>
            <td class=" txt7px txtr8 " width="58">Rp. {{ floatval($sisa) }}</td>
        </tr>
        <?php } ?>

        <tr nobr="true">
            <td class=" txt7px txctr txbold " colspan="4">TOTAL</td>

            <td class=" txt7px txctr " width="27"></td>
            <td class=" txt7px txtr8 " width="58">Rp. {{ floatval($jlhStok) }}</td>

            <td class=" txt7px txctr " width="27"></td>
            <td class=" txt7px txtr8 " width="58">Rp. {{ floatval($jlhSuplai) }}</td>

            <td class=" txt7px txctr " width="27"></td>
            <td class=" txt7px txtr8 " width="58">Rp. {{ floatval($jlhPemakaian) }}</td>

            <td class=" txt7px txctr " width="27"></td>
            <td class=" txt7px txtr8 " width="58">Rp. {{ floatval($jlhSisa) }}</td>
        </tr>
        </tbody>
    </table>
</td></tr>

<tr><td>
    <br />&nbsp;<br />&nbsp;<br />
</td></tr><tr nobr="true"><td>
    <table style="" border="0" cellpadding="0" cellspacing="0">
    <tr nobr="true">
        <td width="120" >&nbsp;</td>
        <td width="100" class="bdrtop bdrlef bdrr8 bdrbot txctr">Dibuat Oleh</td>
        <td width="100" class="bdrtop bdrlef bdrr8 bdrbot txctr">Disetujui Oleh</td>
    </tr>
    <tr nobr="true">
        <td></td>
        <td class="bdrlef bdrr8 bdrbot"></td>
        <td class="bdrr8 bdrbot"><br />&nbsp;<br /><br />&nbsp;<br /><br />&nbsp;<br /></td>
    </tr><tr nobr="true">
        <td width="120" >&nbsp;</td>
        <td width="100" class="bdrtop bdrlef bdrr8 bdrbot txctr">{{ $requester->name }}</td>
        <td width="100" class="bdrtop bdrlef bdrr8 bdrbot txctr">{{ $approver->name }}</td>
    </tr><tr nobr="true">
        <td width="120" >&nbsp;</td>
        <td width="100" class="bdrtop bdrlef bdrr8 bdrbot txctr">{{ $requester->jabatan }}</td>
        <td width="100" class="bdrtop bdrlef bdrr8 bdrbot txctr">{{ $approver->jabatan }}</td>
    </tr>
    </table>    
    <br />

</td></tr>
</table>