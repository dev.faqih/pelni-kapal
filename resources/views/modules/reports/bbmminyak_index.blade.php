@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>PERMINTAAN  BBM &amp; MINYAK PELUMAS</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>                    
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>

        <div class="container border rounded bg-light">
            <div class="clearfix">&nbsp;</div>
            @include('includes.include_error_prop')
            <form autocomplete="off"  target="_blank" action="{{url("administrator/bbmminyak")}}" role="form" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">
                <input type="hidden" name="requesterId" value="{{ WorkerAuth::auth()->getAuth()->id }}">
                <div class="row">
                    <div class="col-sm-2 col-xs-6">Laporan Bulan </div>
                    <div class="col-sm-2 col-xs-6">
                        <input required class=" form-control datepicker input-default search-value" data-date-view-mode="months" data-date-clear-btn="true" data-date-autoclose="true" data-date-format="yyyy-mm" name="monthYear" >
                    </div>
                </div><br />
                <div class="row">
                    <div class="col-sm-2">&nbsp;</div>
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary " name="search-btn-pagination" title="Search"><span class="fa fa-search"></span> Search</button> &nbsp;
                        <button type="button" class="btn btn-primary  " name="clear-search" title="Reset Search"><span class="fa fa-trash-o"></span> Reset</button>
                    </div>
                </div><br />
            </form>
        </div>        
    </div>
    @include('includes.includes_footer')
     <script>
            var paginationParameter = "[]";
            var currentUrl = "{{ url($currentPrefix) }}";
        </script>
    <script type="text/javascript" src="{{ URL::asset("/assets/js/pagination.js") }}"></script>
@endsection