<style>
td { font-size:8px; }
.txctr { text-align:center; }
.txlft { text-align:left; }
.txtr8 { text-align:right; }
.txbold { font-weight:bold; }
.bdrtop { border-top:1px solid #000000; }
.bdrbot { border-bottom:1px solid #000000; }
.bdrlef { border-left:1px solid #000000; }
.bdrr8 { border-right:1px solid #000000; }
.pad5 { padding: 5px 5px 5px 5px; }

</style>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
    <td width="100%" class="txctr">
        <table width="100%" style="" class="" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="4" class="txctr" style="" width="16%"><br />&nbsp;<br />
            <span ><img src="{{ url("assets/img/pelni.png") }}" border="0" height="40" style="padding:0px 10px;"></span>
            </td>
            <td width="68%" rowspan="4" class=" txctr"><br />&nbsp;<br />
                <span style="font-size:12px;">FORMULIR</span><br />
                <span style="font-size:12px;">REKAPITULASI BIAYA LAYANAN MAKAN</span><br /><br />
            </td>
            <td width="4%" class="txlft"><br />&nbsp;<br />No </td>         
            <td width="14%" class="txlft"><br />&nbsp;<br />: {{ $headerNo }} </td>       
        </tr>
        <tr><td class="txlft">Rev </td><td class="txlft">: {{ $headerRev }} </td></tr>
        <tr><td class="txlft">Tgl </td><td class="txlft">: {{ date("d F Y") }}</td></tr>
        <tr><td class="txlft">Hal  </td><td class="txlft">: 1 dari {{ $totalPageCount }}</td></tr>        
        </table>


    </td>
</tr>

<tr><td class="" style="">
    <br />&nbsp;<br />
    <table style="" border="0" cellpadding="0" cellspacing="4">
        <tr><td width="50">Kapal</td> <td width="300" >:
            @if(WorkerAuth::auth()->getAuth()->aplikasi == "HO")
                {{ $kapal->namaKapal }}
            @else
                {{ WorkerAuth::auth()->getAuth()->namaKapal }}
            @endif
        </td></tr>
        <tr><td >Voyage</td> <td >: {{ $voyage->voyageName }} </td></tr>
        <tr><td >Periode</td> <td >:
            @if($voyage->voyageKode <= 0 )
                {{ date("d F Y", strtotime($dateStart)) . " s/d " . date("d F Y", strtotime($dateEnd)) }}
            @else
                {{ date("d F Y", strtotime($voyage->voyage_from)) . " s/d " . date("d F Y", strtotime($voyage->voyage_to)) }}
            @endif
        </td></tr>
    </table>
    <br />
</td></tr>

<tr><td class="" style="">

    <br />
    <br />

    <table align="center" cellpadding="3" class="" border="1" cellspacing="0">
        <thead><tr>
            <td class=" txctr txbold" width="30"><span style="font-size:10px;">No</span></td>
            <td class=" txctr txbold" width="250"><span style="font-size:10px;">Catering Service</span></td>
            <td class=" txctr txbold" width="50"><span style="font-size:10px;">Hari Makan</span></td>
            <td class=" txctr txbold" width="100"><span style="font-size:10px;">Harga Satuan (Food Cost)</span></td>
            <td class=" txctr txbold" width="100"><span style="font-size:10px;">Jumlah Harga</span></td>
        </tr>
        </thead>
        <tbody>
        <?php $i = 1; $a = "A"; $mealClass = ""; $subTotal = $total = 0; 
        foreach($items as $item) { 
            if($mealClass != $item->meal_class && ! empty($item->meal_class)) { 
                if($i > 1) { ?>
                    <tr nobr="true">
                        <td class="  "></td>
                        <td class=" "></td>
                        <td class="  "></td>
                        <td class=" txlft">Jumlah</td>
                        <td class=" txtr8">Rp. {{ number_format($subTotal) }} </td>
                    </tr>
                <?php } ?>
                <tr nobr="true">
                    <td class="  txbold">{{ $a }}</td>
                    <td class="  txlft txbold">{{ $item->meal_class }}</td>
                    <td class="  "></td>
                    <td class="  "></td>
                    <td class=" txtr8"></td>
                </tr>
                <?php $a++; $i = 1; $mealClass = $item->meal_class; $subTotal = 0;
            }   ?>
        <tr nobr="true">
            <td class=" ">{{ $i++ }}</td>
            <td class=" txlft ">{{ $item->deskripsi }}</td>
            <td class=" txctr"><?php echo floatval($item->nItem);
                    $total += $item->harga; $subTotal += $item->harga; ?></td>
            <td class=" txtr8 ">Rp. {{ floatval($item->harga_satuan) }}</td>
            <td class=" txtr8">Rp.  {{ floatval($item->harga) }}</td>
        </tr>
        <?php } ?>
        <tr nobr="true">
            <td class="  "></td>
            <td class=" "></td>
            <td class="  "></td>
            <td class=" txlft">Jumlah</td>
            <td class=" txtr8">Rp. {{ number_format($subTotal) }} </td>
        </tr>
        <tr nobr="true">
            <td class="  "></td>
            <td class=" "></td>
            <td class="  "></td>
            <td class=" txlft">Total Keseluruhan</td>
            <td class=" txtr8">Rp. {{ number_format($total) }} </td>
        </tr>
        </tbody>
    </table>

</td></tr>
<tr><td>
    <br />&nbsp;<br />&nbsp;<br />
</td></tr><tr nobr="true"><td>
    <table style="" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="120" >&nbsp;</td>
        <td width="100" class="bdrtop bdrlef bdrr8 bdrbot txctr">Dibuat Oleh</td>
        <td width="100" class="bdrtop bdrlef bdrr8 bdrbot txctr">Disetujui Oleh</td>
    </tr>
    <tr>
        <td></td>
        <td class="bdrlef bdrr8 bdrbot"></td>
        <td class="bdrr8 bdrbot"><br />&nbsp;<br /><br />&nbsp;<br /><br />&nbsp;<br /></td>
    </tr><tr>
        <td width="120" >&nbsp;</td>
        <td width="100" class="bdrtop bdrlef bdrr8 bdrbot txctr">{{ $requester->name }}</td>
        <td width="100" class="bdrtop bdrlef bdrr8 bdrbot txctr">{{ $approver->name }}</td>
    </tr><tr>
        <td width="120" >&nbsp;</td>
        <td width="100" class="bdrtop bdrlef bdrr8 bdrbot txctr">{{ $requester->jabatan }}</td>
        <td width="100" class="bdrtop bdrlef bdrr8 bdrbot txctr">{{ $approver->jabatan }}</td>
    </tr>
    </table>    
    <br />
</td></tr>
</table>