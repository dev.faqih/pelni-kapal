<style>
td { font-size:8px; }
.txctr { text-align:center; }
.txlft { text-align:left; }
.txtr8 { text-align:right; }
.txt7px { font-size:7px; }
.txbold { font-weight:bold; }
.bdrtop { border-top:1px solid #000000; }
.bdrbot { border-bottom:1px solid #000000; }
.bdrlef { border-left:1px solid #000000; }
.bdrr8 { border-right:1px solid #000000; }
.pad5 { padding: 5px 5px 5px 5px; }

</style>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
    <td width="100%" class="txctr">
        <table width="100%" style="" class="" cellpadding="0" cellspacing="0">
        <tr>
            <td class="txctr" style="" width="16%"><br />&nbsp;<br />
            <span ><img src="{{ url("assets/img/pelni.png") }}" border="0" height="40" style="padding:0px 10px;"></span>
            </td>
            <td width="68%" class=" txctr"><br />&nbsp;<br />
                <span style="font-size:13px;">PT PELAYARAN NASIONAL INDONESIA (PERSERO)</span><br />
            </td>
            <td width="16%" class="txlft"><br />&nbsp;<br /> </td>     
        </tr>
        </table>
    </td>
</tr>
<tr><td class="" style="">
    <h2 class="txctr">BERITA  ACARA  BUNKER</h2> <br />&nbsp;<br />
    <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td width="50%">
                <table border="0" width="100%"model>
                    <tr>
                        <td width="50%">Pada hari ini ...........</td>
                        <td width="50%">Tanggal ........-..........-201....</td>
                    </tr>        
                </table>
            </td>
            <td width="50%">
                <table border="0" width="100%"model>
                    <tr>
                        <td width="50%">di Pelabuhan ....................</td>
                        <td width="50%">Jam..............	WIB</td>
                    </tr>        
                </table>
            </td>
        </tr>    

        <tr>
            <td width="50%">
                <table border="0" width="100%"model><tr><td>Dilaksanakan pengisian (bunker) Bahan Bakar untuk</td></tr></table>
            </td>
            <td width="50%">
                <table border="0" width="100%"model>
                    <tr>
                        <td width="50%">KM.............................   	 </td>
                        <td width="50%">Sebanyak.............	KL</td>
                    </tr>        
                </table>
            </td>
        </tr>

        <tr>
            <td width="50%">
            <table border="0" width="100%"model><tr><td>Menggunakan Tongkang...............</td></tr></table></td>
            <td width="50%"> &nbsp;Milik Perusahaan..................</td>
        </tr>

        <tr>
            <td colspan="2">
                <table border="0" width="100%"model><tr><td>Hasil Pemeriksaan oleh Pihak kapal dan Pihak Tongkang bersama petugas Bunker PT.PELNI</td></tr></table> 
            </td>            
        </tr>

        <tr>
            <td width="50%">
                <table border="0" width="100%"model>
                    <tr>
                        <td width="50%">Cabang...................... </td>
                        <td width="50%">adalah sebagai berikut :</td>
                    </tr>        
                </table>
            </td>
            <td width="50%">&nbsp;</td>
        </tr>    
        <tr>
            <td width="50%">
                <table border="0" width="100%"model><tr><td>Flowmeter Kapal</td></tr></table> 
            </td>
            <td width="50%">
                <table border="0" width="100%"model><tr><td>Flowmeter Tongkang</td></tr></table> 
            </td>
        </tr>    
        <tr>
            <td width="50%">
                <table border="0" width="100%"model>
                    <tr>
                        <td width="30%">Sesudah </td>
                        <td width="70%"><input type="text" style="border:1px solid #000; width:100%" /></td>
                    </tr>        
                </table>
            </td>
            <td width="50%">
                <table border="0" width="100%"model>
                    <tr>
                        <td width="30%">Sesudah </td>
                        <td width="70%"><input type="text"/></td>
                    </tr>        
                </table></td>
        </tr>    
        <tr>
            <td width="50%">
                <table border="0" width="100%"model>
                    <tr>
                        <td width="30%">Sebelum </td>
                        <td width="70%"><input type="text" style="border:1px solid #000; width:100%" /></td>
                    </tr>        
                </table>
            </td>
            <td width="50%">
                <table border="0" width="100%"model>
                    <tr>
                        <td width="30%">Sebelum </td>
                        <td width="70%"><input type="text"/></td>
                    </tr>        
                </table></td>
        </tr>    
        <tr>
            <td width="50%">
                <table border="0" width="100%"model><tr><td>Hasil Sounding Kapal</td></tr></table> 
            </td>
            <td width="50%">
                <table border="0" width="100%"model><tr><td>Hasil Sounding Tongkang</td></tr></table> 
            </td>
        </tr>    


        <tr>
            <td width="50%"> 
                <table border="0" width="100%"model>
                    <tr>
                        <td class="txctr bdrr8 bdrlef bdrtop  bdrbot" rowspan="2" width="15">No</td>
                        <td class="txctr bdrr8 bdrtop bdrbot" rowspan="2">Tk No</td>
                        <td class="txctr bdrr8 bdrtop bdrbot" colspan="2">Sebelum Bunker</td>
                        <td class="txctr bdrr8 bdrtop bdrbot" colspan="2">Sesudah Bunker</td>
                    </tr>      
                    <tr>
                        <td class="txctr bdrr8 bdrbot" >Sounding (cm)</td>
                        <td class="txctr bdrr8 bdrbot" >Volume (KL)</td>
                        <td class="txctr bdrr8 bdrbot" >Sounding (cm)</td>
                        <td class="txctr bdrr8 bdrbot" >Volume (KL)</td>
                    </tr>  
                    <tr>
                        <td class="txctr bdrr8 bdrlef bdrbot" width="15">1.</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                    </tr>  
                    <tr>
                        <td class="txctr bdrr8 bdrlef bdrbot" width="15">2.</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                    </tr> 
                    <tr>
                        <td class="txctr bdrr8 bdrlef bdrbot" width="15">2.</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                    </tr> 
                    <tr>
                        <td class="txctr bdrr8 bdrlef bdrbot" width="15">4.</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                    </tr> 
                    <tr>
                        <td class="txctr bdrr8 bdrlef bdrbot" width="15">5.</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                    </tr> 
                    <tr>
                        <td class="txctr bdrr8 bdrlef bdrbot" width="15">6.</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                    </tr> 
                    <tr>
                        <td class="txctr bdrr8 bdrlef bdrbot" width="15">7.</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                    </tr> 
                    <tr>
                        <td class="txctr bdrr8 bdrlef bdrbot" width="15">8.</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                    </tr> 
                    <tr>
                        <td class="txctr bdrr8 bdrlef bdrbot" colspan="2">Total</td>
                        <td class="bdrr8 bdrbot"  colspan="2">&nbsp;</td>
                        <td class="bdrr8 bdrbot" colspan="2" >&nbsp;</td>
                    </tr> 
                </table> 
            </td>
            <td width="50%"> 
                <table border="0" width="100%"model>
                    <tr>
                        <td class="txctr bdrr8 bdrlef bdrtop  bdrbot" rowspan="2" width="15">No</td>
                        <td class="txctr bdrr8 bdrtop bdrbot" rowspan="2">Tk No</td>
                        <td class="txctr bdrr8 bdrtop bdrbot" colspan="2">Sebelum Bunker</td>
                        <td class="txctr bdrr8 bdrtop bdrbot" colspan="2">Sesudah Bunker</td>
                    </tr>      
                    <tr>
                        <td class="txctr bdrr8 bdrbot" >Sounding (cm)</td>
                        <td class="txctr bdrr8 bdrbot" >Volume (KL)</td>
                        <td class="txctr bdrr8 bdrbot" >Sounding (cm)</td>
                        <td class="txctr bdrr8 bdrbot" >Volume (KL)</td>
                    </tr>  
                    <tr>
                        <td class="txctr bdrr8 bdrlef bdrbot" width="15">1.</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                    </tr>  
                    <tr>
                        <td class="txctr bdrr8 bdrlef bdrbot" width="15">2.</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                    </tr> 
                    <tr>
                        <td class="txctr bdrr8 bdrlef bdrbot" width="15">2.</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                    </tr> 
                    <tr>
                        <td class="txctr bdrr8 bdrlef bdrbot" width="15">4.</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                    </tr> 
                    <tr>
                        <td class="txctr bdrr8 bdrlef bdrbot" width="15">5.</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                    </tr> 
                    <tr>
                        <td class="txctr bdrr8 bdrlef bdrbot" width="15">6.</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                    </tr> 
                    <tr>
                        <td class="txctr bdrr8 bdrlef bdrbot" width="15">7.</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                    </tr> 
                    <tr>
                        <td class="txctr bdrr8 bdrlef bdrbot" width="15">8.</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                        <td class="bdrr8 bdrbot" >&nbsp;</td>
                    </tr> 
                    <tr>
                        <td class="txctr bdrr8 bdrlef bdrbot" colspan="2">Total</td>
                        <td class="bdrr8 bdrbot"  colspan="2">&nbsp;</td>
                        <td class="bdrr8 bdrbot" colspan="2" >&nbsp;</td>
                    </tr> 
                </table> 
            </td>
        </tr>  


        <tr>
            <td width="50%">
                <table border="0" width="100%"model>
                    <tr>
                        <td width="40%">Hasil Pengisian Kapal </td>
                        <td width="60%"><input type="text" style="border:1px solid #000; width:100%" /></td>
                    </tr>        
                </table>
            </td>
            <td width="50%">
                <table border="0" width="100%"model>
                    <tr>
                        <td width="40%">Hasil Pengisian Tongkang </td>
                        <td width="60%"><input type="text"/></td>
                    </tr>        
                </table></td>
        </tr>    
        <tr>
            <td colspan="2">
                <table border="0" width="100%"model>
                    <tr>
                        <td width="75%">Dari hasil data-data pengisian kedua pihak tersebut diatas didapat selisih sebanyak</td>
                        <td width="25%"><input type="text" style="border:1px solid #000; width:100%" /></td>
                    </tr>        
                </table>
            </td>           
        </tr>    
        <tr>
            <td colspan="2"> <br />&nbsp;<br />&nbsp;<br />
                <table border="0" width="100%"model>
                    <tr>
                        <td class="txctr" width="5%">&nbsp;</td>
                        <td class="txctr" width="30%">Pihak Tongkang</td>
                        <td class="txctr" width="30%">Surveyor Independent</td>
                        <td class="txctr" width="30%">Pengawas Bunker PT.PELNI</td>
                        <td class="txctr" width="5%">&nbsp;</td>
                    </tr>      
                    <tr><td colspan="5"><p>&nbsp;</p>&nbsp;<p>&nbsp;</p></td></tr>
                    <tr>
                        <td class="txctr">&nbsp;</td>
                        <td class="txctr">(..........................)</td>
                        <td class="txctr">(..........................)</td>
                        <td class="txctr">(..........................)</td>
                        <td class="txctr">&nbsp;</td>
                    </tr>     
                    <tr><td class="txctr" colspan="5"><br />&nbsp;<br />&nbsp;<br />Mengetahui Pihak Kapal<br /></td></tr>
                    <tr>
                        <td class="txctr">&nbsp;</td>
                        <td class="txctr">Masinis II</td>
                        <td class="txctr">KKM</td>
                        <td class="txctr">Nahkoda</td>
                        <td class="txctr">&nbsp;</td>
                    </tr>      
                    <tr><td colspan="5"><p>&nbsp;</p>&nbsp;<p>&nbsp;</p></td></tr>
                    <tr>
                        <td class="txctr">&nbsp;</td>
                        <td class="txctr">(..........................)</td>
                        <td class="txctr">(..........................)</td>
                        <td class="txctr">(..........................)</td>
                        <td class="txctr">&nbsp;</td>
                    </tr>     
                </table>
            </td>           
        </tr>
    </table>
</td></tr>

<tr><td class="" style="">
   

</td></tr>
</table>