<style>
td { font-size:8px; }
.txctr { text-align:center; }
.txlft { text-align:left; }
.txtr8 { text-align:right; }
.txt7px { font-size:7px; }
.txbold { font-weight:bold; }
.bdrtop { border-top:1px solid #000000; }
.bdrbot { border-bottom:1px solid #000000; }
.bdrlef { border-left:1px solid #000000; }
.bdrr8 { border-right:1px solid #000000; }
.pad5 { padding: 5px 5px 5px 5px; }

</style>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
    <td width="100%" class="txctr">
        <table width="100%" style="" class="" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="3" class="   txctr" style="" width="16%"><br />&nbsp;<br />
            <span ><img src="{{ url("assets/img/pelni.png") }}" border="0" height="40" style="padding:0px 10px;"></span>
            </td>
            <td width="68%" rowspan="3" class=" txctr  "><br />&nbsp;<br />
                <span style="font-size:11px;">PERUSAHAAN PERSEROAN (PERSERO) </span><br />
                <span style="font-size:13px;">PT PELAYARAN NASIONAL INDONESIA (PELNI)</span><br />
                <span style="font-size:8px;">Jl. GAJAH MADA NO. 14 JAKARTA PUSAT</span><br />
            </td>
            <td width="8%" rowspan="2" class="   "> <br />&nbsp;<br />&nbsp;<br /><span style="padding-bottom:5px;"> Kode :</span>&nbsp;<br /> </td>
            <td width="8%" class=" "> &nbsp; </td>
        </tr>
        <tr><td class=" "><br />&nbsp;<br /></td></tr>
        <tr>
            <td class=" " style=""><span style="padding-bottom:5px;padding-top:10px;"> Revisi :</span> </td>
            <td  style="" class=" " > &nbsp; </td>
        </tr>     
        </table>

        <br />&nbsp;<br />
        <span style="font-size:11px;">
            LAPORAN BULANAN PENERIMAAN DAN PEMAKAIAN SUKU CADANG, PERKAKAS DAN LAIN-LAIN YANG DITERIMA DARI DIVISI PENGADAAN ATAUPUN DARI PERWAKILAN LAINNYA
        </span><br />


    </td>
</tr>
<tr><td class="" style="">
    <table cellpadding="3" class="" border="1" cellspacing="0">
        <thead>
        <tr>
            <td class="txctr" colspan="2" width="180">NAMA KAPAL</td>            
            <td class="txctr" colspan="3" width="170">NAMA {{ strtoupper(WorkerAuth::auth()->getAuth()->jabatan) }} </td>            
            <td class="txctr" colspan="4" width="180">BULAN / TAHUN</td>
        </tr>
        <tr>
            <td class="txctr" colspan="2" width="180">{{ WorkerAuth::auth()->getAuth()->namaKapal }}</td>            
            <td class="txctr" colspan="3" width="170">{{ WorkerAuth::auth()->getAuth()->name }}</td>            
            <td class="txctr" colspan="4" width="180">{{ $periode }}</td>
        </tr>
        <tr>
            <td class="txctr" width="20">No</td>
            <td class="txctr" width="160">Nama Barang</td>
            <td class="txctr" width="90">Part No.</td>

            <td class="txctr" width="40">Sisa Bulan Lalu</td>

            <td class="txctr" width="40">Penerimaan</td>
            <td class="txctr" width="40">Pemakaian</td>
            <td class="txctr" width="40">Sisa</td>
            <td class="txctr" width="30">UOM</td>
            <td class="txctr" width="70">Keterangan</td>

        </tr>
        </thead>
        <tbody>
        <?php foreach($items as $item) { $sisa = "";
            if(empty($item["penerimaan"]) && empty($item["penggunaan"])) {
            } else {
                $item["saldoAwal"] = floatval($item["saldoAwal"]);
                $item["penerimaan"] = floatval($item["penerimaan"]);
                $item["penggunaan"] = floatval($item["penggunaan"]) * -1;
                $sisa = floatval($item["saldoAwal"] + $item["penerimaan"] - $item["penggunaan"]);
            }
                ?>
            
            
            <tr nobr="true">
                <td class=" txt7px txctr " width="20">{!! $item["no"] !!}</td>
                <td class=" txt7px  " width="160">{!! $item["deskripsiItem"] !!}</td>
                <td class=" txt7px  txctr" width="90">{{ $item["partNo"] }}</td>

                <td class=" txt7px  txctr" width="40">{{ $item["saldoAwal"] }}</td>

                <td class=" txt7px  txctr" width="40">{{ $item["penerimaan"] }}</td>
                <td class=" txt7px  txctr" width="40">{{ $item["penggunaan"] }}</td>
                <td class=" txt7px  txctr" width="40">{{ $sisa }}</td>
                <td class=" txt7px  txctr" width="30">{{ $item["satuan"] }}</td>

                <td class="  txctr" width="70" >{{ $item["keterangan"] }}</td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</td></tr>

<tr><td>
    <br />&nbsp;<br />&nbsp;<br />
</td></tr><tr nobr="true"><td>
    <table style="" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="300" >&nbsp;</td>
        <td width="140" class="txctr">{!! WorkerAuth::auth()->getAuth()->namaKapal . ", " . date("d F Y") . " <br />\n<b>" .  $approver->jabatan . "</b>" !!} </td>
    </tr>
    <tr>
        <td></td>
        <td class=""><br />&nbsp;<br /><br />&nbsp;<br /><br />&nbsp;<br /></td>
    </tr><tr>
        <td width="320" >&nbsp;</td>
        <td width="100" class="txctr">{{ $approver->name }}</td>
    </tr><tr>
        <td width="320" >&nbsp;</td>
        <td width="100" class="bdrtop txctr">Nrp. {{ $approver->nrp }}</td>
    </tr>
    </table>    
    <br />

</td></tr>
</table>