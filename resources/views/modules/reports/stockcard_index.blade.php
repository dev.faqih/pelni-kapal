@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>CATATAN HARIAN PERSEDIAAN KAPAL </h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>                    
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>

        <div class="container border rounded bg-light">
            <div class="clearfix">&nbsp;</div>
            @include('includes.include_error_prop')
            <form id="formStockCard" autocomplete="off" target="_blank" action="{{url("administrator/stockcard")}}" role="form" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="requesterId" value="{{ WorkerAuth::auth()->getAuth()->id }}">
                <div class="row">
                    <div class="col-sm-5 col-xs-6">

                        @if(WorkerAuth::auth()->getAuth()->aplikasi == "HO")
                        <div class="row">
                            <div class="col-sm-4 col-xs-6">Kapal</div>
                            <div class="col-sm-6 col-xs-6">
                                <select class="form-control search-value" id="kapalId" name="kapalId">
                                @foreach($kapals as $kapal)
                                    <option value="{{ $kapal->id }}">{{ $kapal->namaKapal }}</option>
                                @endforeach
                                </select >
                            </div>
                        </div>
                        <br />
                        @else
                            <input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">
                        @endif
                        <div class="row">
                            <div class="col-sm-4 col-xs-6">Laporan Bulan</div>
                            <div class="col-sm-6 col-xs-6">
                                <input required class=" form-control datepicker input-default search-value" data-date-clear-btn="true" data-date-autoclose="true" data-date-format="yyyy-mm" name="monthYear" >
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-6">Sub-Inventory </div> 
                    <div class="col-sm-3 col-xs-6">
                            <ul class="list-group" style="max-height: 250px;overflow-x: auto">
                                <li class="list-group-item" style="background-color:#ccffff;">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" style="" id="deSelectSub" value="" name="" onClick="selectAll('deSelectSub', 'lokasiitem')">
                                        <label class="custom-control-label font-weight-bold" for="deSelectSub">Select/Deselect All</label>                                        
                                    </div>
                                </li>
                                <?php foreach(WorkerAuth::auth()->getAuth()->roleLokasiItem as $lokasiitem) { ?>
                                    <li class="list-group-item">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input lokasiitem" id="subInv{{$lokasiitem->id}}" value="{{$lokasiitem->id}}" name="lokasiId[]">
                                            <label class="custom-control-label" for="subInv{{$lokasiitem->id}}">{{$lokasiitem->namaLokasi}}</label>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>                            
                        </select>
                    </div>
                        
                </div><br />
                <div class="row">
                    <div class="col-sm-2">&nbsp;</div>
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary " name="search-btn-pagination" title="Search"><span class="fa fa-search"></span> Search</button> &nbsp;
                        <button type="button" class="btn btn-primary  " name="clear-search" title="Reset Search"><span class="fa fa-trash-o"></span> Reset</button>
                    </div>
                </div><br />
            </form>
        </div>        
    </div>
    @include('includes.includes_footer')
     <script>
            var paginationParameter = "[]";
            var currentUrl = "{{ url($currentPrefix) }}";
        </script>
    <script type="text/javascript" src="{{ URL::asset("/assets/js/pagination.js") }}"></script>
@endsection