<style>
td { font-size:8px; }
.txctr { text-align:center; }
.txlft { text-align:left; }
.txtr8 { text-align:right; }
.txt7px { font-size:7px; }
.txbold { font-weight:bold; }
.bdrtop { border-top:1px solid #000000; }
.bdrbot { border-bottom:1px solid #000000; }
.bdrlef { border-left:1px solid #000000; }
.bdrr8 { border-right:1px solid #000000; }
.pad5 { padding: 5px 5px 5px 5px; }

</style>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
    <td width="100%" class="txctr">
        <table width="100%" style="" class="" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="4" class="txctr" style="" width="16%"><br />&nbsp;<br />
            <span ><img src="{{ url("assets/img/pelni.png") }}" border="0" height="40" style="padding:0px 10px;"></span>
            </td>
            <td width="68%" rowspan="4" class=" txctr"><br />&nbsp;<br />
                <span style="font-size:13px;">LAPORAN</span><br />
                <span style="font-size:13px;">PERSEDIAAN KAPAL</span><br />
            </td>
            <td width="4%" class="txlft"><br />&nbsp;<br />No </td>         
            <td width="14%" class="txlft"><br />&nbsp;<br />: </td>       
        </tr>
        <tr><td class="txlft">Rev </td><td class="txlft">: </td></tr>
        <tr><td class="txlft">Tgl </td><td class="txlft">: {{ date("d F Y") }}</td></tr>
        <tr><td class="txlft">Hal  </td><td class="txlft">: 1 dari {{ $totalPageCount }} </td></tr>        
        </table>


    </td>
</tr>
<tr><td class="" style="">
    <br />&nbsp;<br />
    <table style="" border="0" cellpadding="0" cellspacing="4">
    <tr><td width="15%" >Kapal</td> <td width="75%" >: {{ $kapal->namaKapal }}</td></tr>
    <!-- <tr><td >Voyage</td> <td >: </td></tr> -->
    <tr><td >Periode</td> <td >: {{ $periode }} </td></tr>
    </table>    
    <br />
</td></tr>
<tr><td class="" style="">
    <table cellpadding="3" class="bdrtop bdrlef" border="1" cellspacing="0">
        <thead>
        <tr>
            <td class="txctr " rowspan="2" width="20">No</td>
            <td class="txctr " rowspan="2" width="45">Lokasi</td>
            <td class="txctr " colspan="3" width="150">Segment 1</td>

            <td class="txctr " width="70" colspan="2" >Saldo Awal</td>

            <td class="txctr " width="205" colspan="5" >Mutasi Penerimaan</td>

            <td class="txctr " width="205" colspan="5" >Mutasi Penggunaan</td>

            <td class="txctr " width="70" colspan="2" >Saldo Akhir</td>
        </tr>
        <tr>
        
            <td class="txctr " width="45">Kategori</td>
            <td class="txctr " width="70">Deskripsi</td>
            <td class="txctr " width="35" >Kode</td>

            <td class="txctr " width="25" >Qty</td>
            <td class="txctr " width="45" >Tgl</td>

            <td class="txctr " width="45" >Type</td>
            <td class="txctr " width="25" >Qty</td>
            <td class="txctr " width="45" >Tgl</td>
            <td class="txctr " width="45" >Transaksi</td>
            <td class="txctr " width="45" >Transaksi No#</td>

            <td class="txctr " width="45" >Type</td>
            <td class="txctr " width="25" >Qty</td>
            <td class="txctr " width="45" >Tgl</td>
            <td class="txctr " width="45" >Transaksi</td>
            <td class="txctr " width="45" >Transaksi No#</td>

            <td class="txctr " width="25" >Qty</td>
            <td class="txctr " width="45" >Tgl</td>
        </tr>
            
        </thead>
        <tbody>
        <?php $i = 1; $namaLokasi = $namaItem = $tglSisa = ""; $sisa = 0;
        foreach($items as $item) { $qtyNow = 0;
            if($namaItem != $item->namaItem || $namaLokasi != $item->namaLokasi) {
                $sisa = $item->sisaawal; $tglSisa = $item->stokawal_periode;
                $namaItem = $item->namaItem; $namaLokasi = $item->namaLokasi;
            }
            if(isset($namaTransaksi[$item->transaksi])) { } else {
                $namaTransaksi[$item->transaksi] = $item->transaksi;
            } ?>
        <tr nobr="true">
            <td class=" txt7px txctr  ">{{ $i++ }}</td>
            <td class=" txt7px ">{{ $item->namaLokasi }}</td>
            <td class=" txt7px  txctr">{{ $item->category }}</td>
            <td class=" txt7px ">{{ $item->deskripsi }}</td>
            <td class=" txt7px  txctr">{{ $item->namaItem }}</td>

            <td class=" txt7px txctr " width="25">{{ floatval($sisa) }}</td>
            <td class=" txt7px txtr8 " width="45">{{ date("d-m-Y",strtotime($tglSisa)) }}</td>
            
            <td class=" txt7px txctr" width="45" >&nbsp;</td>
            <td class=" txt7px txctr" width="25" >@if($item->mutasi == "IN") {{ floatval($item->jumlah) }} @endif</td>
            <td class=" txt7px txctr" width="45" >@if($item->mutasi == "IN") {{ date("d-m-Y", strtotime($item->periode)) }} @endif</td>
            <td class=" txt7px txctr" width="45" >@if($item->mutasi == "IN") {{ $namaTransaksi[$item->transaksi] }} @endif</td>
            <td class=" txt7px txctr" width="45" >@if($item->mutasi == "IN") {{ $item->noTransaksi }} @endif</td>

            <td class=" txt7px txctr" width="45" >&nbsp;</td>
            <td class=" txt7px txctr" width="25" >@if($item->mutasi == "OUT") {{ floatval(abs($item->jumlah)) }} @endif</td>
            <td class=" txt7px txctr" width="45" >@if($item->mutasi == "OUT") {{ date("d-m-Y", strtotime($item->periode)) }} @endif</td>
            <td class=" txt7px txctr" width="45" >@if($item->mutasi == "OUT") {{ $namaTransaksi[$item->transaksi] }} @endif</td>
            <td class=" txt7px txctr" width="45" >@if($item->mutasi == "OUT") {{ $item->noTransaksi }} @endif</td>
            <?php if($item->mutasi == "IN") { $sisa += abs($item->jumlah); } else { $sisa -= abs($item->jumlah); } $tglSisa = $item->periode; ?>
            <td class=" txt7px txctr" width="25" >{{ floatval($sisa) }}</td>
            <td class=" txt7px txctr" width="45" >{{ date("d-m-Y", strtotime($tglSisa)) }}</td>
        </tr>
            <?php 
        } ?>
        </tbody>
    </table>
</td></tr>

<tr><td>
    <br />&nbsp;<br />&nbsp;<br />
</td></tr><tr nobr="true"><td>
    <table style="" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="120" >&nbsp;</td>
        <td width="100" class="bdrtop bdrlef bdrr8 bdrbot txctr">Dibuat Oleh</td>
        <td width="100" class="bdrtop bdrlef bdrr8 bdrbot txctr">Disetujui Oleh</td>
    </tr>
    <tr>
        <td></td>
        <td class="bdrlef bdrr8 bdrbot"></td>
        <td class="bdrr8 bdrbot"><br />&nbsp;<br /><br />&nbsp;<br /><br />&nbsp;<br /></td>
    </tr><tr>
        <td width="120" >&nbsp;</td>
        <td width="100" class="bdrtop bdrlef bdrr8 bdrbot txctr">{{ $requester->name }}</td>
        <td width="100" class="bdrtop bdrlef bdrr8 bdrbot txctr">{{ $approver->name }}</td>
    </tr><tr>
        <td width="120" >&nbsp;</td>
        <td width="100" class="bdrtop bdrlef bdrr8 bdrbot txctr">{{ $requester->jabatan }}</td>
        <td width="100" class="bdrtop bdrlef bdrr8 bdrbot txctr">{{ $approver->jabatan }}</td>
    </tr>
    </table>    
    <br />

</td></tr>
</table>