@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Create Role</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
            <div class="card-body content-box-body">
				<div class="clearfix">&nbsp;</div>
                @include('includes.include_error_prop')
                <form id="formCreateRole" autocomplete="off" action="{{url("administrator/roles")}}" role="form" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-sm-2 col-xs-5">Role</div>
                        <div class="col-sm-4 col-xs-7">
                            <input type="text"  class="form-control input-default " required placeholder="Masukkan nama role" name="roleName" value="{{old("roleName")}}" title="Nama role">
                        </div>
                        
                    </div><br />
                    
                    <div class="row">
                        <div class="col-sm-2 col-xs-5">Access Permissions</div>
                        <div class="col-sm-4 col-xs-7">
                            <ul class="list-group" style="max-height: 250px;overflow-x: auto">
                                <li class="list-group-item" style="background-color:#ccffff;">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" style="" id="deSelectAll" value="" name="" onClick="selectAll('deSelectAll', 'aksesMenu')">
                                        <label class="custom-control-label font-weight-bold" for="deSelectAll">Select/Deselect All</label>                                        
                                    </div>
                                </li>
                                <?php $background = $parent = "";
                                foreach($access as $acc) {
                                    $acc->parentID .= "parent"; 
                                    
                                    if($parent != $acc->parentID) { 
                                        $parent = $acc->parentID; 
                                        if($background != "#FFFFFF") { $background = "#FFFFFF"; } else { $background = "#f2f2f2"; }
                                     ?>
                                    <li class="list-group-item" style="background-color:{{ $background }};">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input aksesMenu" id="access{{ $acc->parentID }}" value="" name="" onClick="selectAll('access{{ $acc->parentID }}', 'subAccess{{ $acc->parentID }}')">
                                            <label class="custom-control-label font-weight-bold" for="access{{ $acc->parentID }}">Select/Deselect {{ $acc->parentMenu }}</label>                                        
                                        </div>
                                    </li>
                                    <?php } ?>

                                    <li class="list-group-item" style="background-color:{{ $background }};">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input subAccess{{ $acc->parentID }} aksesMenu" id="subAccess{{$acc->id}}" value="{{$acc->id}}" name="access[]">
                                            <label class="custom-control-label" for="subAccess{{$acc->id}}">{{$acc->menuname}}</label> <br/>
                                            <small>{{$acc->parentMenu}}</small>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>   

                        <div class="col-sm-2 col-xs-5">Sub Inventories</div>
                        <div class="col-sm-4 col-xs-7">
                            <ul class="list-group" style="max-height: 250px;overflow-x: auto">
                                <li class="list-group-item" style="background-color:#ccffff;">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" style="" id="deSelectSub" value="" name="" onClick="selectAll('deSelectSub', 'lokasiitem')">
                                        <label class="custom-control-label font-weight-bold" for="deSelectSub">Select/Deselect All</label>                                        
                                    </div>
                                </li>
                                <?php foreach($lokasiitems as $lokasiitem) { ?>
                                    <li class="list-group-item" style="background-color:{{ $background }};">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input lokasiitem" id="subInv{{$lokasiitem->id}}" value="{{$lokasiitem->id}}" name="lokasiitem[]">
                                            <label class="custom-control-label" for="subInv{{$lokasiitem->id}}">{{$lokasiitem->namaLokasi}}</label>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>               
                    </div><br />
                    <div class="row">
                        <div class="col-sm-2 col-xs-5">Item Category</div>
                        <div class="col-sm-4 col-xs-7">
                            <ul class="list-group" style="max-height: 250px;overflow-x: auto">
                                <li class="list-group-item" style="background-color:#ccffff;">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" style="" id="deSelectCategory" value="" name="" onClick="selectAll('deSelectCategory', 'category')">
                                        <label class="custom-control-label font-weight-bold" for="deSelectCategory">Select/Deselect All</label>                                        
                                    </div>
                                </li>
                                <?php foreach($categories as $category) { ?>
                                    <li class="list-group-item" style="background-color:{{ $background }};">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input category" id="category{{$category->id}}" value="{{$category->id}}" name="category[]">
                                            <label class="custom-control-label" for="category{{$category->id}}">{{$category->category}}</label>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>    
                    </div><br />
                    
                    <p class="text-center">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Submit</button> &nbsp;
                        <a class="btn btn-primary" href="{{url("administrator/roles")}}" title="Back"><span class="fa fa-undo"></span> Back</a>
                    </p>
                </form>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
    <script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>
@endsection