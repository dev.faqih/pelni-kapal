@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Daftar {{$title}}</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
            <div class="card-body content-box-body">
                @if(WorkerAuth::auth()->getAuth()->aplikasi == "HO")
                <div class="row">
					<div class="col-sm-12 text-right">
                    <a class="btn btn-danger" href="{{url("administrator/roles/create")}}" title="Create Data"><span class="fa fa-plus"></span> New </a>
                    <!-- <button class="btn btn-primary" title="Delete Data" name="delete-content"><span class="fa fa-trash"></span> Delete</button> -->
                    <!-- <button class="btn btn-info" title="Export Data"><span class="fa fa-file-excel-o"></span> Export</button> -->
					</div>
				</div>
				<div class="clearfix">&nbsp;</div>
                @endif
            @include('includes.include_error_prop')
                <div class="row">
                    <div class="col-sm-2 col-xs-12">Role</div>
                    <div class="col-sm-3 col-xs-12">
                        <input class="form-control input-default search-value" name="roleName">
                    </div>
                </div><br />
                <div class="row">
                    <div class="col-sm-2">&nbsp;</div>
                    <div class="col-sm-10 col-xs-12">
                        <button type="button" class="btn btn-primary " onClick="searchList()" name="search-btn-pagination" title="Search"><span class="fa fa-search"></span> Search</button> &nbsp;
                        <button type="button" class="btn btn-primary  " name="clear-search" title="Reset Search"><span class="fa fa-trash-o"></span> Reset</button>
                    </div>
                </div><br />

                <div class="card-title content-box-title add-space">
                    <div class="row">
                        <div class="col-md-12 col-xs-12 ">
                            <div class="pull-left">
                                <div class="addon-box-list">
                                    <div class="form-group-inline">
                                        <span class="addon">Show</span>
                                         <select class="form-control" name="size" title="Total List">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select>
                                        <span class="addon">Data</span>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right">
                                <div class="addon-box-list add-space-left  ">
                                    <div class="form-group-inline">
                                        <span class="addon ">Ordering by</span>
                                       <select class="form-control" title="Sorting Data" name="orderingBy">
                                           <option value="0" selected>Sorting By</option>
                                           @foreach($ordering as $key => $value)
                                               <option value="{{$key}}">{{$value}}</option>
                                           @endforeach
                                       </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
					<table id="roles" class="table table-hover table-striped widget-table"name="table-index">
						<thead>
						<tr>
                        @if(WorkerAuth::auth()->getAuth()->aplikasi == "HO" && false) // nanti false nya di ganti, sementara di hilangin dulu biar ga bisa delete role
							<th><input type="checkbox" name="selectAll"></th>
                        @endif
							<th>&nbsp;</th>
                            <th class="header-pagination">Role</th>
						</tr>
						</thead>
						<tbody>
							@foreach($roless["rows"]as $roles)
						<tr>
                        @if(WorkerAuth::auth()->getAuth()->aplikasi == "HO" && false)
							<td><input type="checkbox" value="{{$roles->id}}" name="checkbox-del[]" title="id"></td>
                        @endif
							<td><a data-toggle="tooltip" data-placement="top" title="View" href="{{url("administrator/roles/$roles->id")}}"class="btn btn-outline-primary"><span class="fa fa-eye"></span></a></td>								
								<td>{{$roles->roleName}}</td>
						</tr>
							@endforeach
						</tbody>
					</table>

                </div>
                <div class=" content-box-footer">
                    <div class="row">
                        <div class="col-md-6 text-left col-xs-12">
                        Page {{$roless["page"]}} of <span id="pageCount">{{$roless["pageCount"]}}</span>
                        Total Row <span id="totalRow">{{$roless["rowCount"]}}</span>
                        </div>
                        <div class="col-md-6  col-xs-12">
                           <ul class="pagination pull-right">
                               <li class="page-item ">
                                   <button class="page-link" tabindex="-1" name="first" title="First Page">First
                                   </button>
                               </li>
                               <li class="page-item" id="pagination-prev">
                                    <a class="page-link" title="Prev Page">
                                        <i class="fa fa-step-backward"></i></a>
                               </li>
                               <li class="page-item" id="pagination-next">
                                    <a class="page-link" title="Next Page">
                                        <i class="fa fa-step-forward"></i></a></li>
                               <li class="page-item">
                                   <button class="page-link" name="last" title="Last Page">Last</button>
                               </li>
                           </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes.includes_footer')
     <script>
            var paginationParameter = ('{!! json_encode($roless)!!}');
            var currentUrl = "{{url($currentPrefix)}}";
        </script>
    <script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>
@endsection