@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Detail Role</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
            <div class="card-body content-box-body">
				<div class="clearfix">&nbsp;</div>
                @include('includes.include_error_prop')
                <div class="row">
                    <div class="col-sm-2 col-xs-5">Role</div>
                    <div class="col-sm-4 col-xs-7">
                        <input type="text" disabled class="form-control input-default "  placeholder="Masukkan roleName" name="roleName" value="{{$roles->roleName}}" title="roleName">
                    </div>                    
                </div><br />

                <div class="row">
                    <div class="col-sm-2 col-xs-5">Access Permissions</div>
                    <div class="col-sm-4 col-xs-7">
                        <ul class="list-group" style="max-height: 250px;overflow-x: auto">
                            <?php $background = $parent = "";
                            foreach($access as $acc) {
                                $acc->parentID .= "parent"; 
                                
                                if($parent != $acc->parentID) { 
                                    $parent = $acc->parentID; 
                                    if($background != "#FFFFFF") { $background = "#FFFFFF"; } else { $background = "#f2f2f2"; }
                                    ?>
                                <li class="list-group-item" style="background-color:{{ $background }};">
                                    <div class="custom-control font-weight-bold">                                       
                                        {{ $acc->parentMenu }}
                                    </div>
                                </li>
                                <?php } ?>

                                <li class="list-group-item" style="background-color:{{ $background }};">
                                    <div class="custom-control custom-checkbox">
                                        <input disabled type="checkbox" class="custom-control-input" id="{{$acc->id}}"
                                            @if ($groupHasAccess)
                                                @if(Utils::isValueExistInObject($acc->id, $groupHasAccess, "accessid"))
                                                    checked
                                                @endif
                                            @endif
                                            value="{{$acc->id}}" name="access[]">
                                        <label class="custom-control-label" for="{{$acc->id}}">{{$acc->menuname}}</label>
                                        <br />
                                        <small>{{$acc->parentMenu}}</small>
                                    </div>
                                </li>
                                <?php } ?>
                        </ul>
                    </div>       
                    <div class="col-sm-2 col-xs-5">Sub Inventories</div>
                    <div class="col-sm-4 col-xs-7">
                        <ul class="list-group" style="max-height: 250px;overflow-x: auto">
                            <?php foreach($lokasiitems as $lokasiitem) { ?>
                                <li class="list-group-item">
                                    <div class="custom-control custom-checkbox">
                                        <input disabled type="checkbox" class="custom-control-input lokasiitem" id="{{$lokasiitem->id}}" value="{{$lokasiitem->id}}" name="lokasiitem[]"
                                        @if ($groupHasSubinv)
                                            @if(Utils::isValueExistInObject($lokasiitem->id, $groupHasSubinv, "lokasiId"))
                                                checked
                                            @endif
                                        @endif
                                        >
                                        <label class="custom-control-label" for="{{$lokasiitem->id}}">{{$lokasiitem->namaLokasi}}</label>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>                
                </div> <br />

                <div class="row">
                    <div class="col-sm-2 col-xs-5">Item Category</div>
                    <div class="col-sm-4 col-xs-7">
                        <ul class="list-group" style="max-height: 250px;overflow-x: auto">
                            <?php foreach($categories as $category) { ?>
                                <li class="list-group-item" style="background-color:{{ $background }};">
                                    <div class="custom-control custom-checkbox">
                                        <input disabled type="checkbox" class="custom-control-input category" id="category{{$category->id}}"
                                        @if($groupHasCategories)
                                            @if(Utils::isValueExistInObject($category->id, $groupHasCategories, "categoryId"))
                                                checked
                                            @endif
                                        @endif
                                        value="{{$category->id}}" name="category[]">
                                        <label class="custom-control-label" for="category{{$category->id}}">{{$category->category}}</label>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>    
                </div><br />
                
                <p class="text-center">
                    <a class="btn btn-primary" href="{{ URL::previous() }}" title="Back"><span class="fa fa-undo"></span> Back</a> &nbsp; 
                    @if(WorkerAuth::auth()->getAuth()->aplikasi == "HO")
                    <a class="btn btn-primary" href="{{url("administrator/roles/$roles->id/edit")}}" title="Edit data"><span class="fa fa-edit"></span> Edit</a>
                    @endif
                </p>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
@endsection