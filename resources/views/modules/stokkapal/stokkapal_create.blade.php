@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Buat Stok Barang</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
            <div class="card-body content-box-body">
            @include('includes.include_error_prop')
                <form action="{{url("administrator/stokkapal")}}" role="form" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">
					<input type="hidden" name="status" value="1">
					<div class="row">
						<div class="col-sm-2 col-xs-5">
							<label for="#itemId">Item</label>
						</div>
						<div class="col-sm-4 col-xs-7">
						<input type="text"  class="form-control input-default "  placeholder="Masukkan Item ID" name="itemId" value="" title="Item ID">
						</div>
						<div class="col-sm-2 col-xs-5">
							<label for="#satuan">Satuan</label>
						</div>
						<div class="col-sm-4 col-xs-7">
							<input type="text"  class="form-control input-default "  placeholder="Masukkan satuan" name="satuan" value="{{old("satuan")}}" title="satuan">
						</div>
					</div>
					<div class="clearfix">&nbsp;</div>
					<div class="row">
						<div class="col-sm-2 col-xs-5">
							<label for="#lokasiId">Lokasi</label>
						</div>
						<div class="col-sm-4 col-xs-7">
						<input type="text"  class="form-control input-default "  placeholder="Masukkan Item ID" name="lokasiId" value="" title="Item ID">
						</div>
						<div class="col-sm-2 col-xs-5">
							<label for="#jumlah">Jumlah</label>
						</div>
						<div class="col-sm-4 col-xs-7">
							<input type="text"  class="form-control input-default "  placeholder="Masukkan jumlah" name="jumlah" value="{{old("jumlah")}}" title="jumlah">
						</div>
					</div>
					<div class="clearfix">&nbsp;</div>
					<div class="row">
						<div class="col-sm-2 col-xs-5">
							<label for="#lokasiId">Lot Number</label>
						</div>
						<div class="col-sm-4 col-xs-7">
						<input type="text"  class="form-control input-default "  placeholder="Masukkan Item ID" name="lotNumber" value="" title="Item ID">
						</div>
						<div class="col-sm-2 col-xs-5">
							<label for="#jumlah">Kapal ID</label>
						</div>
						<div class="col-sm-4 col-xs-7">
							<input type="text"  class="form-control input-default "  placeholder="Masukkan jumlah" name="kapalId" value="" title="jumlah">
						</div>
					</div>
					<div class="clearfix">&nbsp;</div>
                    <p class="text-center" > 
						<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Submit</button> &nbsp;
						<a class="btn btn-primary" href="{{url("administrator/stokkapal")}}" title="Back"><span class="fa fa-undo"></span> Back</a>
					</p>
                </form>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
@endsection