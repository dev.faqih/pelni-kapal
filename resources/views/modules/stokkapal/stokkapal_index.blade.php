@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Daftar Stok Barang</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
            <div class="card-body content-box-body">
                @include('includes.include_error_prop')
                <div class="row">
                    <div class="col-sm-1 col-xs-12">Kode Item</div>
                    <div class="col-sm-3 col-xs-12">
                        <input class="form-control input-default search-value" name="namaItem">
                    </div>
                    <div class="col-sm-1 col-xs-12">Deskripsi</div>
                    <div class="col-sm-3 col-xs-12">
                        <input class="form-control input-default search-value" name="deskripsi">
                    </div>
                    <div class="col-sm-1 col-xs-12">Part Number</div>
                    <div class="col-sm-3 col-xs-12">
                        <input class="form-control input-default search-value" name="part_number">
                    </div>
                </div><br />
                <div class="row">
                    <div class="col-sm-2">&nbsp;</div>
                    <div class="col-sm-10 col-xs-12">
                        <button type="button" class="btn btn-primary " onClick="searchList()" name="search-btn-pagination" title="Search"><span class="fa fa-search"></span> Search</button> &nbsp;
                        <button type="button" class="btn btn-primary  " name="clear-search" title="Reset Search"><span class="fa fa-trash-o"></span> Reset</button>
                    </div>
                </div><br />
                <div class="card-title content-box-title add-space">
                    <div class="row">
                        <div class="col-md-12 col-xs-12 ">
                            <div class="pull-left">
                                <div class="addon-box-list">
                                    <div class="form-group-inline">
                                        <span class="addon">Show</span>
                                         <select class="form-control" name="size" title="Total List">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select>
                                        <span class="addon">Data</span>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right">
                                <div class="addon-box-list add-space-left  ">
                                    <div class="form-group-inline">
                                        <span class="addon ">Ordering by</span>
                                       <select class="form-control" title="Sorting Data" name="orderingBy">
                                           <option value="0" selected>Sorting By</option>
                                           @foreach($ordering as $key => $value)
                                               <option value="{{$key}}">{{$value}}</option>
                                           @endforeach
                                       </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
					<table id="stokkapal" class="table table-hover table-striped widget-table"name="table-index">
						<thead>
						<tr>
							<th>&nbsp;</th>
                            <th class="header-pagination">Kode Item</th>
                            <th class="header-pagination">Deskripsi Item</th>
                            <th class="header-pagination">Part Number</th>
                            <th class="header-pagination">(n) Lot Number</th>
                            <th class="header-pagination">Total</th>
                            <th class="header-pagination">Satuan</th>
						</tr>
						</thead>
						<tbody>
							@foreach($stokkapals["rows"]as $stokkapal)
						<tr>
							<td><a href="{{url("administrator/stokkapal/$stokkapal->itemId")}}"class="btn btn-outline-primary"><span class="fa fa-eye"></span></a></td>
                         
                            <td>{{$stokkapal->namaItem}}</td>
                            <td>{{$stokkapal->deskripsi}}</td>
                            <td>{{$stokkapal->part_number}}</td>
                            <td>{{$stokkapal->nLot}}</td>
                            <td>{{ floatval($stokkapal->total) }}</td>
                            <td>{{$stokkapal->UOM}}</td>
						</tr>
							@endforeach
						</tbody>
					</table>
                </div>
                <div class=" content-box-footer">
                    <div class="row">
                        <div class="col-md-6 text-left col-xs-12">
                        Page {{$stokkapals["page"]}} of <span id="pageCount">{{$stokkapals["pageCount"]}}</span>
                        Total Row <span id="totalRow">{{$stokkapals["rowCount"]}}</span>
                        </div>
                        <div class="col-md-6  col-xs-12">
                           <ul class="pagination pull-right">
                               <li class="page-item ">
                                   <button class="page-link" tabindex="-1" name="first" title="First Page">First
                                   </button>
                               </li>
                               <li class="page-item" id="pagination-prev">
                                    <a class="page-link" title="Prev Page">
                                        <i class="fa fa-step-backward"></i></a>
                               </li>
                               <li class="page-item" id="pagination-next">
                                    <a class="page-link" title="Next Page">
                                        <i class="fa fa-step-forward"></i></a></li>
                               <li class="page-item">
                                   <button class="page-link" name="last" title="Last Page">Last</button>
                               </li>
                           </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes.includes_footer')
    <?php $stokkapals["rows"] = []; ?>
     <script>
            var paginationParameter = ('{!! json_encode($stokkapals)!!}');
            var currentUrl = "{{url($currentPrefix)}}";
        </script>
    <script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>
@endsection