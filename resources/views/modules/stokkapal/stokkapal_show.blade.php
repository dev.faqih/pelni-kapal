@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Detail Stok Barang</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
            <div class="card-body content-box-body">
                <div class="clearfix">&nbsp;</div>
            	@include('includes.include_error_prop')
				<?php 
                if(strtolower($item->image) == null) { $item->image = url("assets/img/nopicture.jpg"); } ?>
				<div class="row">
                    <div class="col-sm-3"> 
                        <div class="form-group text-center">
                                <img class="card-img-top" src="{{ url($item->image) }}" alt="{{ $item->namaItem }}">
                        </div>
                    </div>
                    <div class="col-sm-8"> 
                        <h2><span class="text-secondary">{{ $item->namaItem }}</span> {{ $item->deskripsi }} 
                        <span class="text-secondary">{{ $item->part_number }}</span></h2><br />
                        <div class="table-responsive">
                            <table id="stokkapal" class="table table-hover table-striped widget-table"name="table-index">
                                <thead>
                                <tr>
                                    <th class="header-pagination">Sub-Inventory</th>
                                    <th class="header-pagination">Jumlah</th>
                                    <th class="header-pagination">Lot Number</th>
                                    @if(WorkerAuth::auth()->getAuth()->aplikasi == "HO")
                                    <th class="header-pagination">Kapal</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($stokkapals as $stokkapal)
                                <tr>
                                    <td>{{ $stokkapal->namaLokasi }}</td>
                                    <td>{{ floatval($stokkapal->jumlah) }}</td>
                                    <td>{{ $stokkapal->lotNumber }}</td>
                                    @if(WorkerAuth::auth()->getAuth()->aplikasi == "HO")
                                    <td>{{ $stokkapal->namaKapal }}</td>
                                    @endif
                                </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
				<div class="clearfix">&nbsp;</div>
				<p class="text-center">				
					<a class="btn btn-primary" href="{{ URL::previous() }}" title="Back"><span class="fa fa-undo"></span> Kembali </a>
				</p>

            </div>

        </div>
    </div>
    @include('includes.includes_footer')
@endsection