@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Ubah System Parameter</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
            <div class="card-body content-box-body">
            <div class="row">
					<div class="col-sm-12 text-right">
                    </div>
				</div>
				<div class="clearfix">&nbsp;</div>
            @include('includes.include_error_prop')

                <form role="form" action="{{url("administrator/systemparameter/$systemparameter->id/update")}}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="row">
					<div class="col-sm-2 col-xs-5">
                        <label for="#variableName">Variable Name</label>
					</div>
					<div class="col-sm-4 col-xs-7">
                        <input type="text" readonly class="form-control input-default "  placeholder="Masukkan Variable Name" name="variableName" value="{{$systemparameter->variableName}}" title="variableName">
					</div>

					<div class="col-sm-2 col-xs-5">
                        <label for="#variableValue">Variable Value</label>
					</div>
					<div class="col-sm-4 col-xs-7">							
                        <input type="text"  class="form-control input-default "  placeholder="Masukkan Variable Value" name="variableValue" value="{{$systemparameter->variableValue}}" title="variableValue">
					</div>
				</div><br />

                    <p class="text-center">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button> &nbsp;
                        <a class="btn btn-primary" href="{{url("administrator/systemparameter")}}" title="Back"><span class="fa fa-undo"></span> Back</a>					
                    </p>
                </form>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
@endsection