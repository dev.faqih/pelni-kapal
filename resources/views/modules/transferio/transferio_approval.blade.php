@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Perpindahan Barang antar IO</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
            <div class="card-body content-box-body">
            @include('includes.include_error_prop')
				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#operatingUnit">Tanggal Transaksi</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default" required value="{{ $transferio->tglTransaksi }}" title="tglTransaksi">
					</div>

					<div class="col-sm-2 col-xs-5">
						<label for="#requesterId">Organisasi Tujuan</label>
					</div>
					<div class="col-sm-4 col-xs-7">							
						<input type="text" disabled class="form-control input-default" value="{{ $transferio->toNamaKapal }}" title="tglTransaksi">
					</div>
				</div><br />

				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#deskripsi">Tanggal Sampai Tujuan</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default" value="{{ $transferio->expectedReceivedDate }}" title="tglTransaksi">
					</div>
					<div class="col-sm-2 col-xs-5">
						<label for="#requesterId">Status</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default" value="<?php if($transferio->status > 1) { echo "Rejected"; } else if($transferio->status > 0) { echo "Approved"; } else { echo "Open"; } ?>" title="tglTransaksi">
					</div>
				</div><br />
				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#voyageName">Voyage</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default" name="voyageName" value="{{ $transferio->voyageName }}" title="voyageKode">
					</div>
					<div class="col-sm-2 col-xs-5">
						<label for="#voyageDate">Tanggal Voyage</label>
					</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default" name="voyageDate" value="{{ date("Y-m-d", strtotime($transferio->voyageStart) ) . " - " . date("Y-m-d", strtotime($transferio->voyageEnd) ) }}" title="voyageKode">
					</div>
				</div><br />

				<div class="clearfix">&nbsp;</div>
				<div class="row justify-content-between">
					<div class="col-sm-4 col-xs-6"><h5>Daftar Permintaan Barang</h5></div>
				</div><br />
				<div class="table-responsive">
					<table id="tableItem" class="table table-hover table-striped widget-table">
						<tr>
							<th>Barang</th>
							<th>Kategori</th>
							<th>Deskripsi</th>
							<th>UOM</th>
							<th>Qty</th>
							<th>Lot Number</th>
						</tr>
						@foreach($listItems as $item)
						<tr>							
							<td>{{ $item->namaItem }}</td>
							<td>{{ $item->category }}</td>
							<td>{{ $item->deskripsi }}</td>
							<td>{{ $item->UOM }}</td>
							<td>{{ $item->quantity }}</td>
							<td>{{ $item->lotNumber }}</td>
						</tr>
						@endforeach
					</table>
				</div><br />
				<div class="clearfix">&nbsp;</div>
				<div class="row">
					<div class="col-sm-2 col-xs-5">
						<label for="#komentar">Komentar</label>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-xs-12">
					<textarea class="form-control input-default" disabled title="komentar">{{ $transferio->komentar }}</textarea>
					</div>
				</div>		
				<div class="clearfix">&nbsp;</div>
				
				<div class="row">
					<div class="col-sm-6 col-xs-6 text-right">
						<form action="{{url("administrator/transferio/approval/")}}" role="form" method="POST" enctype="multipart/form-data">
							<input type="hidden" name="_method" value="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="ioNumber" value="{{ $transferio->ioNumber }}">
							<input type="hidden" name="status" value="1">		
							<input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">
							<input type="hidden" name="approverId" value="{{ WorkerAuth::auth()->getAuth()->id }}">				
							<button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Setujui</button> &nbsp;					
						</form>
					</div>
					<div class="col-sm-6 col-xs-6">
						<form action="{{url("administrator/transferio/approval/")}}" role="form" method="POST" enctype="multipart/form-data">
							<input type="hidden" name="_method" value="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="ioNumber" value="{{ $transferio->ioNumber }}">
							<input type="hidden" name="status" value="2">			
							<input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">
							<input type="hidden" name="approverId" value="{{ WorkerAuth::auth()->getAuth()->id }}">
							<button type="submit" class="btn btn-primary"><i class="fa fa-close"></i> Tolak</button> &nbsp;	 &nbsp;		
							<a class="btn btn-primary" href="{{ URL::previous() }}" title="Back"><span class="fa fa-undo"></span> Kembali</a>		
						</form>
					</div>
				</div>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
@endsection