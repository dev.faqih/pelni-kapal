@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Perpindahan Barang antar IO</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
		</div>
		<div class="clearfix">&nbsp;</div>
		<form  autocomplete="off" class="submitTransaction" action="{{url("administrator/transferio")}}" role="form" method="POST" enctype="multipart/form-data">
		<div class="container border rounded bg-light">
			<div class="clearfix">&nbsp;</div>
            @include('includes.include_error_prop')
			
			<input type="hidden" name="_method" value="POST">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="dateCreated" value="{{ date("Y-m-d H:i:s") }}">
			<input type="hidden" name="status" value="0">
			<input type="hidden" name="type" value="1">
			<input type="hidden" name="fromKapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">
			<input type="hidden" name="toLocationId" value="1">
			<input type="hidden" name="kapalId" value="{{ WorkerAuth::auth()->getAuth()->kapalId }}">
			<input type="hidden" name="kodeKapal" value="{{ WorkerAuth::auth()->getAuth()->kodeKapal }}">
			<input type="hidden" name="requesterId" value="{{ WorkerAuth::auth()->getAuth()->id }}">
			<input type="hidden" name="tipeTransaksi" value="IO">
			<input type="hidden" name="urlTransaksi" value="transferio">
			<input type="hidden" name="namaTransaksi" value="Transfer IO">
			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#operatingUnit">Tanggal Transaksi</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text"  class="form-control input-default datepicker" data-date-clear-btn="true" data-date-format="yyyy-mm-dd" data-date-autoclose="true" required placeholder="Masukkan tanggal transaksi" id="tglTransaksi" name="tglTransaksi" title="tglTransaksi" OnChange="checkIsBiggerThanArrivedDate('tglTransaksi','expectedReceivedDate');checkIsMoreThan3Days('tglTransaksi', '{{ date("Y-m-d H:i:s") }}','{{ WorkerAuth::auth()->getAuth()->sysParams->BACKDATE }}');loadVoyage('{{ url('administrator/transferio/getVoyages') }}', 'tglTransaksi')">
				</div>

				<div class="col-sm-2 col-xs-5">
					<label for="#requesterId">Organisasi Tujuan</label>
				</div>
				<div class="col-sm-4 col-xs-7">							
					<select class="form-control input-default search-value" name="toKapalId" id="toKapalId">
						@foreach($kapals as $kapal)
							<option value="{{ $kapal->id }}">{{ $kapal->namaKapal }}</option>
						@endforeach
					</select>
				</div>
			</div><br />

			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<label for="#deskripsi">Tanggal Sampai Tujuan</label>
				</div>
				<div class="col-sm-4 col-xs-7">
					<input type="text" class="form-control input-default datepicker" data-date-clear-btn="true" data-date-format="yyyy-mm-dd" data-date-autoclose="true" required placeholder="Masukkan tanggal sampai tujuan" name="expectedReceivedDate" OnChange="checkIsBiggerThanArrivedDate('expectedReceivedDate','tglTransaksi');">
				</div>
				<div class="col-sm-2 col-xs-5">
					<label for="#requesterId">Status</label>
				</div>
				<div class="col-sm-4 col-xs-7"><input class="form-control input-default" disabled value="Open" /></div>
			</div><br />

			<div class="row">
				<div class="col-sm-2 col-xs-5">Voyage</div>
				<div class="col-sm-4 col-xs-7">
					<select class="form-control input-default" required name="voyageKode" title="Voyage">
					</select>
				</div>
				<div class="col-sm-2 col-xs-5">Tanggal Voyage</div>
				<div class="col-sm-4 col-xs-7 input-group">
					<input readonly class=" form-control input-default search-value" required name="voyageStart" >
					<div class="input-group-addon"> &nbsp; s/d &nbsp; </div>
					<input readonly class=" form-control input-default search-value" required name="voyageEnd" >
				</div>
			</div><br />
		</div>
		<div class="clearfix">&nbsp;</div>
		<div class="container border rounded bg-light">
			<div class="clearfix">&nbsp;</div>
			<div class="row justify-content-between">
				<div class="col-sm-4 col-xs-6"><h5>Daftar Barang</h5></div>
				<div class="col-sm-3 text-right col-xs-5">
					<button type="button" id="loadItemModal" class="btn btn-primary">Tambah Item</button>
				</div>
			</div><br />
			<div class="table-responsive">
				<table id="tableItem" class="table table-hover table-striped widget-table">
				<thead>
					<tr>
						<th>No</th>
						<th>Kode Barang</th>
						<th>Part Number</th>
						<th>Deskripsi</th>
						<th>Kategori</th>
						<th>UOM</th>
						<th>Qty</th>
						<th>Lot Number</th>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody></tbody>
				</table>
			</div><br />
			<div class="clearfix">&nbsp;</div>				
		</div>
		<div class="clearfix">&nbsp;</div>				
		<div class="container border rounded bg-light">
			<div class="clearfix">&nbsp;</div>


			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<h5>Komentar</h5>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-xs-12">
				<textarea class="form-control input-default" placeholder="Masukkan komentar" required name="komentar" title="komentar">{{old("komentar")}}</textarea>
				</div>
			</div>		
			<div class="clearfix">&nbsp;</div>
			<div class="row">
				<div class="col-sm-12 col-xs-12">
					<label>
						<span><input class="form-input" type="checkbox" value="" id="invalidCheck" required> &nbsp; </span><span>Saya setuju untuk melanjutkan proses ini.</span>
					</label>
				</div>
			</div><br />		

			<p class="text-center"> 
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Kirim</button> &nbsp;
				<a class="btn btn-primary" href="{{url("administrator/transferio")}}" title="Back"><span class="fa fa-undo"></span> Kembali</a>	
			</p>
		
		</div>		
		</form>
	</div>
    @include('includes.includes_footer')
	@include('includes.include_modal_transferio')
    <script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>

	<script>
		var valueBeforeChange = 0;
		$("#toKapalId").on('focus', function () {
			valueBeforeChange = this.value;
		}).on('change', function() {
			if(itemList.length > 0) {
				$("#defModalTitle").html("Ubah Organisasi Tujuan ?");
				$("#defModalBody").html("Dengan mengubah organisasi tujuan, akan menghapus semua item yang sudah ada di list.<br />&nbsp;<br /> Apakah anda yakin ingin mengubah organisasi tujuan?");
				$("#defModalFooter").html("<button type=\button\" class=\"btn btn-secondary\" onClick=\"rollBackModal()\">Cancel</button><button type=\"button\" onClick=\"reLoadDropDown()\" class=\"btn btn-primary\">Yes</button>");
				$('#defModal').modal('show');
			}		
		});

		var rollBackModal = function(){
			$("#toKapalId").val(valueBeforeChange);
			$('#defModal').modal('hide');
		}

		var reLoadDropDown = function() {
			$("#tableItem tbody").html("");
			itemList = [];
			$('#defModal').modal('hide');
		};

		$(document).ready(function() {
            $('.datepicker').datepicker('update', '{{ date('Y-m-d') }}');
			loadVoyage('{{ url('administrator/transferio/getVoyages') }}', 'tglTransaksi');
		});
		
		$("select[name='voyageKode']").change(function() {
			loadVoyageDate('{{ url('administrator/permintaanbarang/getVoyageDate') }}', $(this).val() );
		});
	</script>
@endsection