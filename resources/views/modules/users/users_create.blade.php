@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Create User</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
            <div class="card-body content-box-body">
            @include('includes.include_error_prop')
                <form autocomplete="off" class="nrpValidation" action="{{url("administrator/users")}}" role="form" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

					<div class="row">
						<div class="col-sm-2 col-xs-5">Username</div>
						<div class="col-sm-4 col-xs-7">
							<input type="text"  class="form-control input-default " required placeholder="Masukkan username" id="username" name="username" value="{{old("username")}}" title="username" >
						</div>
						<div class="col-sm-2 col-xs-5">Nama</div>
						<div class="col-sm-4 col-xs-7">
							<input type="text"  class="form-control input-default " required placeholder="Masukkan name" name="name" value="{{old("name")}}" title="name">
						</div>
					</div><br />

					<div class="row">
						<div class="col-sm-2 col-xs-5">Password</div>
						<div class="col-sm-4 col-xs-7">
							<input type="password"  class="form-control input-default " required placeholder="Masukkan password" name="password" value="{{old("password")}}" title="password">
						</div>
						<div class="col-sm-2 col-xs-5">Password</div>
						<div class="col-sm-4 col-xs-7">
							<input type="password"  class="form-control input-default " required placeholder="Masukkan ulang password" name="re-password" value="{{old("re-password")}}" title="password">
						</div>
					</div><br />

					<div class="row">
						<div class="col-sm-2 col-xs-5">Kapal</div>
						<div class="col-sm-4 col-xs-7">
							<select class="form-control input-default" name="kapalId" id="kapalId">
								@foreach($kapals as $kapal)
									<option value="{{$kapal->id}}">
										{{$kapal->namaKapal}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-sm-2 col-xs-5">Role</div>
						<div class="col-sm-4 col-xs-7">
							<select class="form-control input-default" required name="roleId" id="roleId">			
								@foreach($roles as $role)
									<option value="{{$role->id}}">{{ $role->roleName }}</option>
								@endforeach					
							</select>
						</div>
					</div><br />

					<div class="row">
						<div class="col-sm-2 col-xs-5">Jabatan</div>
						<div class="col-sm-4 col-xs-7">
							<input type="text"  class="form-control input-default " required placeholder="Masukkan jabatan" name="jabatan" value="{{old("jabatan")}}" title="jabatan">
						</div>
						<div class="col-sm-2 col-xs-5">NRP</div>
						<div class="col-sm-4 col-xs-7">
							<input type="text"  class="form-control input-default " id="nrp" maxlength="5" required placeholder="Masukkan NRP" name="nrp" value="{{old("nrp")}}" title="NRP">
						</div>
					</div><br />

					<div class="row">
						<div class="col-sm-2 col-xs-5">Aplikasi</div>
						<div class="col-sm-4 col-xs-7">
							<select class="form-control input-default" name="aplikasi" id="aplikasi">		
							<option value="KAPAL">Kapal</option>	
							<option value="HO">Head Office</option>
							</select>
						</div>
					</div> <br />

					<p class="text-center"> 
						<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Submit</button> &nbsp; 
						<a class="btn btn-primary" href="{{url("administrator/users")}}" title="Back"><span class="fa fa-undo"></span> Back</a>
					</p>
				</form>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
	<script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>
	
	<script>

	$('#username').keypress(function (e) {
		var regex = new RegExp("^[a-z0-9]+$");
		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if (regex.test(str)) {
			return true;
		}

		e.preventDefault();
		return false;
	});

	$('#nrp').keypress(function (e) {
		var regex = new RegExp("^[0-9]+$");
		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if (regex.test(str)) {
			return true;
		}

		e.preventDefault();
		return false;
	});


	/* $(document).ready(function() {
		loadDropDown();
	});

	$("#kapalId").on('change', function() {
		loadDropDown();
	});

	var loadDropDown = function() {
		var kapalID = $("#kapalId").val();
		$.ajax ({
			type: 'GET',
			url: '{{ url("administrator/users/getListRoles") }}',
			dataType: "json",
			data: { kapalID : kapalID },
			success : function(result) {
				$("#roleId").html("");
				$.each(result, function(idx, data) {
					$("#roleId").append('<option value="' + data.id + '">' + data.roleName + '</option>');
				});
			}
		});
		
		$.ajax ({
			type: 'GET',
			url: '{{ url("administrator/users/getListSubInv") }}',
			dataType: "json",
			data: { kapalID : kapalID },
			success : function(result) {
				$("#lokasiId").html("");
				$.each(result, function(idx, data) {
					$("#lokasiId").append('<li class="list-group-item">' +
						'<div class="custom-control custom-checkbox">' +
							'<input type="checkbox" class="custom-control-input" id="' + data.id + '" value="' + data.id + '" name="locations[]">' +
							'<label class="custom-control-label" for="' + data.id + '">' + data.namaLokasi + '</label>' +
						'</div></li>');
				});
			}
		});
	} */
	</script>
@endsection