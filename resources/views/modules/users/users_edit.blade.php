@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Edit User</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
            <div class="card-body content-box-body">
				<div class="clearfix">&nbsp;</div>
            @include('includes.include_error_prop')

                <form role="form" class="nrpValidation" action="{{url("administrator/users/$users->id/update")}}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
					
					<div class="row">
						<div class="col-sm-2 col-xs-5">Username</div>
						<div class="col-sm-4 col-xs-7">
							<input type="text" readonly class="form-control input-default "  placeholder="Masukkan username" name="username" value="{{ $users->username }}" title="username">
						</div>
						<div class="col-sm-2 col-xs-5">Nama</div>
						<div class="col-sm-4 col-xs-7">
							<input type="text"  class="form-control input-default "  placeholder="Masukkan name" name="name" value="{{ $users->name }}" title="name">
						</div>
					</div><br />

					<div class="row">
						<div class="col-sm-2 col-xs-5">Kapal</div>
						<div class="col-sm-4 col-xs-7">
							<select class="form-control input-default"  name="kapalId" id="kapalId">
								@foreach($kapals as $kapal)
									<option value="{{$kapal->id}}"@if($users->kapalId== $kapal->id) selected @endif>
										{{$kapal->namaKapal}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-sm-2 col-xs-5">Role</div>
						<div class="col-sm-4 col-xs-7">
							<select class="form-control input-default"  name="roleId" id="roleId">
								@foreach($roles as $role)
									<option value="{{$role->id}}"@if($users->roleId== $role->id) selected @endif>{{$role->roleName}}</option>
								@endforeach
							</select>
						</div>
					</div><br />

					<div class="row">
						<div class="col-sm-2 col-xs-5">Jabatan</div>
						<div class="col-sm-4 col-xs-7">
							<input type="text"  class="form-control input-default " required placeholder="Masukkan jabatan" name="jabatan" value="{{ $users->jabatan }}" title="jabatan">
						</div>
						<div class="col-sm-2 col-xs-5">NRP</div>
						<div class="col-sm-4 col-xs-7">
							<input type="text"  class="form-control input-default " required placeholder="Masukkan NRP" id="nrp" name="nrp" value="{{ $users->nrp }}" title="NRP" maxlength="5">
						</div>
					</div><br />

					<div class="row">
						<div class="col-sm-2 col-xs-5">Aplikasi</div>
						<div class="col-sm-4 col-xs-7">
							<select class="form-control input-default" name="aplikasi" id="aplikasi">	
								<option value="KAPAL" @if($users->aplikasi == "KAPAL") selected @endif >Kapal</option>	
								<option value="HO" @if($users->aplikasi == "HO") selected @endif >Head Office</option>
							</select>
						</div>
					</div> <br />
					
					<p class="text-center"> 
						<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button> &nbsp; 
						<a class="btn btn-primary" href="{{url("administrator/users")}}" title="Back"><span class="fa fa-undo"></span> Back</a>
					<p>
                </form>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
	<script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>

<script>

$('#username').keypress(function (e) {
	var regex = new RegExp("^[a-z0-9]+$");
	var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
	if (regex.test(str)) {
		return true;
	}

	e.preventDefault();
	return false;
});

/* $(document).ready(function() { loadDropDown(); });
$("#kapalId").on('change', function() { loadDropDown(); });

var loadDropDown = function() {
	var kapalID = $("#kapalId").val();
	$.ajax ({
		type: 'GET',
		url: '{{ url("administrator/users/getListRoles") }}',
		dataType: "json",
		data: { kapalID : kapalID },
		success : function(result) {
			console.log(result);
			$("#roleId").html("");
			$.each(result, function(idx, data) {
				$("#roleId").append('<option value="' + data.id + '">' + data.roleName + '</option>');
			});
		}
	});
	
	$.ajax ({
		type: 'GET',
		url: '{{ url("administrator/users/getListSubInv") }}',
		dataType: "json",
		data: { kapalID : kapalID },
		success : function(result) {
			console.log(result);
			$("#lokasiId").html("");
			$.each(result, function(idx, data) {
				$("#lokasiId").append('<li class="list-group-item">' +
					'<div class="custom-control custom-checkbox">' +
						'<input type="checkbox" class="custom-control-input" id="' + data.id + '" value="' + data.id + '" name="locations[]">' +
						'<label class="custom-control-label" for="' + data.id + '">' + data.namaLokasi + '</label>' +
					'</div></li>');
			});
		}
	});
} */
</script>
@endsection