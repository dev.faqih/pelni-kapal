@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Daftar Users</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
            <div class="card-body content-box-body">
                @if(WorkerAuth::auth()->getAuth()->aplikasi == "HO")
                <div class="row">
					<div class="col-sm-12 text-right">
                        <a class="btn btn-danger" href="{{url("administrator/users/create")}}" title="Create Data"><span class="fa fa-plus"></span> Buat User </a>                        
                   </div>
				</div>
				<div class="clearfix">&nbsp;</div>                
                @endif
                @include('includes.include_error_prop')
                <div class="row">
                    <div class="col-sm-2 col-xs-12">User Name</div>
                    <div class="col-sm-3 col-xs-12">
                        <input class="form-control input-default search-value" name="username">
                    </div>
                    <div class="col-sm-2 col-xs-12">Full Name</div>
                    <div class="col-sm-3 col-xs-12">
                        <input class="form-control input-default search-value" name="name">
                    </div>
                </div><br />
                <div class="row">
                    <div class="col-sm-2 col-xs-12">Role</div>
                    <div class="col-sm-3 col-xs-12">
                        <select class="form-control input-default search-value"  name="roleId" id="roleId">
                        <option value=""></option>
                        @foreach($roles as $role) 
                            <option value="{{ $role->id }}">{{ $role->roleName }}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="col-sm-2 col-xs-12">NRP</div>
                    <div class="col-sm-3 col-xs-12">
                        <input class="form-control input-default search-value" name="nrp">
                    </div>
                </div><br />
                @if(WorkerAuth::auth()->getAuth()->aplikasi == "HO")
                <div class="row">
                    <div class="col-sm-2 col-xs-12">Kapal</div>
                    <div class="col-sm-3 col-xs-12">
                        <select class="form-control input-default search-value"  name="kapalId" id="kapalId">
                        <option value=""></option>
                        @foreach($kapals as $kapal) 
                            <option value="{{ $kapal->id }}">{{ $kapal->namaKapal }}</option>
                        @endforeach
                        </select>
                    </div>
                </div><br />
                @endif
                <div class="row">
                    <div class="col-sm-2">&nbsp;</div>
                    <div class="col-sm-10 col-xs-12">
                        <button type="button" class="btn btn-primary " onClick="searchList()" name="search-btn-pagination" title="Search"><span class="fa fa-search"></span> Search</button> &nbsp;
                        <button type="button" class="btn btn-primary  " name="clear-search" title="Reset Search"><span class="fa fa-trash-o"></span> Reset</button>
                    </div>
                </div><br />
                <div class="card-title content-box-title add-space">
                    <div class="row">
                        <div class="col-md-12 col-xs-12 ">
                            <div class="pull-left">
                                <div class="addon-box-list">
                                    <div class="form-group-inline">
                                        <span class="addon">Show</span>
                                        <select class="form-control" name="size" title="Total List">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select>
                                        <span class="addon">Data</span>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right">
                                <div class="addon-box-list add-space-left  ">
                                    <div class="form-group-inline">
                                        <span class="addon ">Ordering by</span>
                                        <select class="form-control" title="Sorting Data" name="orderingBy">
                                            <option value="0" selected>Sorting By</option>
                                            @foreach($ordering as $key => $value)
                                                <option value="{{$key}}">{{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="users" class="table table-hover table-striped widget-table" name="table-index">
                        <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th class="header-pagination">User Name</th>                            
                            <th class="header-pagination">Full Name</th>
                            <th class="header-pagination">Jabatan</th>
                            <th class="header-pagination">NRP</th>
                            <th class="header-pagination">Kapal</th>
                            <th class="header-pagination">Role</th>
                            @if(WorkerAuth::auth()->getAuth()->aplikasi == "KAPAL")
                            <th class="header-pagination">Status</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($userss["rows"]as $users)
                            <tr>
                                <td>
                                    <a data-toggle="tooltip" data-placement="top" title="View" href="{{url("administrator/users/$users->id")}}"class="btn btn-outline-primary"><span class="fa fa-eye"></span></a> &nbsp; 
                                    @if(WorkerAuth::auth()->getAuth()->aplikasi == "HO")
                                    <a data-toggle="tooltip" data-placement="top" title="Set Password" href="{{url("administrator/users/setpassword/$users->id")}}"class="btn btn-outline-primary"><span class="fa fa-key"></span></a>  &nbsp; 
                                        @if($users->aktif > 0)
                                            <button class="btn btn-outline-primary" title="Deaktifkan User" value="0" id="button{{ $users->id }}" onClick="userActivation({{ $users->id }})" ><input type="hidden" id="value{{ $users->id }}" value="{{ $users->aktif }}" /><span id="activate{{ $users->id }}" class="fa fa-toggle-on"></span></button>
                                        @else
                                            <button class="btn btn-outline-primary" title="Aktifkan User" value="1"  id="button{{ $users->id }}" onClick="userActivation({{ $users->id }})" ><input type="hidden" id="value{{ $users->id }}" value="{{ $users->aktif }}" /><span  id="activate{{ $users->id }}" class="fa fa-toggle-off"></span></button>
                                        @endif
                                    @endif
                                </td>
                                <td>{{$users->username}}</td>                                
                                <td>{{ $users->name }}</td>
                                <td>{{ $users->jabatan }}</td>
                                <td>{{ $users->nrp }}</td>
                                <td>{{$users->namaKapal . " [" . $users->kodeKapal . "]"}}</td>
                                <td>{{$users->roleName}}</td>
                                @if(WorkerAuth::auth()->getAuth()->aplikasi == "KAPAL")
                                <td>@if($users->aktif > 0) Aktif @else Tidak Aktif @endif</td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
                <div class=" content-box-footer">
                    <div class="row">
                        <div class="col-md-6 text-left col-xs-12">
                            Page {{$userss["page"]}} of <span id="pageCount">{{$userss["pageCount"]}}</span>
                            Total Row <span id="totalRow">{{$userss["rowCount"]}}</span>
                        </div>
                        <div class="col-md-6  col-xs-12">
                            <ul class="pagination pull-right">
                                <li class="page-item ">
                                    <button class="page-link" tabindex="-1" name="first" title="First Page">First
                                    </button>
                                </li>
                                <li class="page-item" id="pagination-prev">
                                    <a class="page-link" title="Prev Page">
                                        <i class="fa fa-step-backward"></i></a>
                                </li>
                                <li class="page-item" id="pagination-next">
                                    <a class="page-link" title="Next Page">
                                        <i class="fa fa-step-forward"></i></a></li>
                                <li class="page-item">
                                    <button class="page-link" name="last" title="Last Page">Last</button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes.includes_footer')
    <script>
        <?php $userss["rows"] = []; ?>
        var paginationParameter = ('{!! json_encode($userss)!!}');
        var currentUrl = "{{url($currentPrefix)}}";
    </script>
    <script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js")}}"></script>
@endsection