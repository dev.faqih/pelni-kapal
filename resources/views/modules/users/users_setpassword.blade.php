@extends('app')
@section('content')
@include('includes.include_navigation')
<div class="container content">
	<div class="card content-box">
		<div class="card-header">
			<div class="pull-left">
				<h4>Reset Password</h4>
			</div>
			<div class="pull-right">@include('includes.include_breadcrumb')</div>
		</div>
		<div class="card-body content-box-body">
			@include('includes.include_error_prop')
			<form autocomplete="off" action="{{url("administrator/users/setpassword/$users->id")}}" role="form" method="POST" enctype="multipart/form-data">
				<input type="hidden" name="_method" value="POST">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="row">
					<div class="col-sm-2 col-xs-5">Username</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default " placeholder="Masukkan username" name="username" value="{{$users->username}}" title="username">
					</div>
					<div class="col-sm-2 col-xs-5">Name</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default " name="name" value="{{ $users->name }}" title="name">
					</div>
				</div><br />

				<div class="row">
					<div class="col-sm-2 col-xs-5">Password</div>
					<div class="col-sm-4 col-xs-7">
						<input type="password" class="form-control input-default " required placeholder="Masukkan password" name="password" value="{{old("password")}}" title="password">
					</div>
					<div class="col-sm-2 col-xs-5">Re-type Password</div>
					<div class="col-sm-4 col-xs-7">
						<input type="password" class="form-control input-default " required placeholder="Masukkan ulang password" name="re-password" value="{{old("re-password")}}" title="password">
					</div>
				</div><br />


				<p class="text-center">
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Submit</button> &nbsp;
					<a class="btn btn-primary" href="{{url("administrator/users")}}" title="Back"><span class="fa fa-undo"></span> Back</a>
				</p>
			</form>
		</div>

	</div>
</div>
@include('includes.includes_footer')
@endsection