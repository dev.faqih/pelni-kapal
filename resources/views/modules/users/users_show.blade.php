@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Detail User</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
            <div class="card-body content-box-body">
            	
				<div class="clearfix">&nbsp;</div>
				@include('includes.include_error_prop')

				<div class="row">
					<div class="col-sm-2 col-xs-5">Username</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default "  placeholder="Masukkan username" name="username" value="{{$users->username}}" title="username">
					</div>
					<div class="col-sm-2 col-xs-5">Name</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default "  name="name" value="{{ $users->name }}" title="name">
					</div>
				</div><br />

				<div class="row">
					<div class="col-sm-2 col-xs-5">Kapal</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default "  name="namaKapal" value="{{ $users->namaKapal }}" title="namaKapal">			
					</div>
					<div class="col-sm-2 col-xs-5">Role</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default "  name="roles" value="{{ $users->roleName }}" title="roles">			
					</div>
				</div><br />

				<div class="row">
					<div class="col-sm-2 col-xs-5">Jabatan</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default " name="jabatan" value="{{ $users->jabatan }}" title="jabatan">
					</div>
					<div class="col-sm-2 col-xs-5">NRP</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default " name="nrp" value="{{ $users->nrp }}" title="NRP">
					</div>
				</div><br />

				<div class="row">
					<div class="col-sm-2 col-xs-5">Aplikasi</div>
					<div class="col-sm-4 col-xs-7">
						<input type="text" disabled class="form-control input-default "  name="aplikasi" value="{{ $users->aplikasi }}" title="aplikasi">			
					</div>
				</div> <br />
				<p class="text-center">
					<a class="btn btn-primary" href="{{url("administrator/users")}}" title="Back"><span class="fa fa-undo"></span> Back</a> &nbsp;
                    @if(WorkerAuth::auth()->getAuth()->aplikasi == "HO")
						<a class="btn btn-primary" href="{{url("administrator/users/$users->id/edit")}}" title="Edit data"><span class="fa fa-edit"></span> Edit</a>					
					@endif
				</p>
            </div>

        </div>
    </div>
    @include('includes.includes_footer')
@endsection