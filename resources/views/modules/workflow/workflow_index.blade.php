@extends('app')
@section('content')
    @include('includes.include_navigation')
    <div class="container content">
        
        <div class="card content-box">
            <div class="card-header">
                <div class="pull-left"><h4>Workflow</h4></div>
                <div class="pull-right">@include('includes.include_breadcrumb')</div>
            </div>
        </div>
		<div class="clearfix">&nbsp;</div>
		<div class="container border rounded bg-light">
            <div class="clearfix">&nbsp;</div>
            <div class="row">
                <div class="col-sm-12 text-right">
                    @if($history)
                    <a class="btn btn-danger" href="{{url("administrator/workflow") }}" title="Open Transaction"><span class="fa fa-paper-plane-o"></span>&nbsp; Waiting for Approval </a>
                    @else
                    <a class="btn btn-danger" href="{{url("administrator/workflow/history") }}" title="History Data"><span class="fa fa-paper-plane-o"></span>&nbsp; History </a>
                    @endif
                </div>
            </div>
            <div class="clearfix">&nbsp;</div>
            @include('includes.include_error_prop')
            <div class="row">
                <div class="col-sm-2 col-xs-12">Jenis Transaksi</div>
                <div class="col-sm-3 col-xs-12">
                    <select class="form-control input-default search-value" name="tipeTransaksi">
                        <option value=""></option>
                        <option value="IO">Transfer IO</option>
                        <option value="PR">Permintaan Barang</option>
                        <option value="MO">Transer Sub-Inventory</option>
                        <option value="MR">Penerimaan Makanan</option>
                        <option value="MS">Penggunaan Makanan</option>                    
                    </select>
                </div>
                <div class="col-sm-2 col-xs-12">Tanggal Transaksi</div>
                {{--<div class="col-sm-3 col-xs-12">--}}
                    {{--<input class="form-control input-default datepicker search-value" data-date-clear-btn="true"  data-date-format="yyyy-mm-dd" data-date-autoclose="true" name="dateCreated" title="">--}}
                {{--</div>--}}
                <div class="col-sm-2 col-xs-4">
                    <input type="text"  class="form-control input-default datepicker search-value" placeholder="Range Tanggal Mulai" id="dateStart" name="dateStart"
                           autocomplete="off" required data-date-clear-btn="true" data-date-format="yyyy-mm-dd" data-date-autoclose="true" title="Date Start" >
                </div>
                <div class="col-sm-2 col-xs-4">
                    <input type="text"  class="form-control input-default datepicker search-value" placeholder="Range Tanggal Akhir" id="dateEnd" name="dateEnd"
                           autocomplete="off" required data-date-clear-btn="true" data-date-format="yyyy-mm-dd" data-date-autoclose="true" title="Date End" >
                </div>
            </div><br />
            <div class="row">
                <div class="col-sm-2 col-xs-12">No. Transaksi</div>
                <div class="col-sm-3 col-xs-12">
                    <input autocomplete="off" class="form-control input-default search-value" name="refId" title="">
                </div>
                <div class="col-sm-2 col-xs-12">Pemohon Transaksi</div>
                <div class="col-sm-4 col-xs-12">
                    <input autocomplete="off" class="form-control input-default search-value" name="requesterName" title="">
                </div>
            </div><br />
            <div class="row">
                <div class="col-sm-2">&nbsp;</div>
                <div class="col-sm-10">
                    <button type="button" class="btn btn-primary " onClick="searchList()" name="search-btn-pagination" title="Search"><span class="fa fa-search"></span> Search</button> &nbsp;
                    <button type="button" class="btn btn-primary  " name="clear-search" title="Reset Search"><span class="fa fa-trash-o"></span> Reset</button>
                </div>
            </div><br />
        </div>
		<div class="clearfix">&nbsp;</div>
		<div class="container border rounded bg-light">
            <div class="card-title content-box-title add-space">
                <div class="row">
                    <div class="col-md-12 col-xs-12 ">
                        <div class="pull-left">
                            <div class="addon-box-list">
                                <div class="form-group-inline">
                                    <span class="addon">Show</span>
                                        <select class="form-control" name="size" title="Total List">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span class="addon">Data</span>
                                </div>
                            </div>
                        </div>
                        <div class="pull-right">
                            <div class="addon-box-list add-space-left  ">
                                <div class="form-group-inline">
                                    <span class="addon ">Ordering by</span>
                                    <select class="form-control" title="Sorting Data" name="orderingBy">
                                        <option value="0" selected>Sorting By</option>
                                        @foreach($ordering as $key => $value)
                                            <option value="{{ $key }}">{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table id="workflow" class="table table-hover table-striped widget-table"name="table-index">
                    <thead>
                    <tr>
                    <th class="header-pagination"></th>
                        <th class="header-pagination">No Transaksi</th>
                        <th class="header-pagination">Jenis Transaksi</th>
                        <th class="header-pagination">Pemohon Transaksi</th>
                        <th class="header-pagination">Tanggal Transaksi</th>
                        @if($history)                    
                        <th class="header-pagination">Status</th>        
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($workflows["rows"] as $workflow)
                        <tr>	
                            <td><a data-toggle="tooltip" data-placement="top" title="View" href="{{ url("administrator/$workflow->urlTransaksi/approval") }}/<?php 
                            if($workflow->ioID > 0) { 
                                echo $workflow->ioID; 
                            } else if($workflow->prID > 0) { 
                                echo $workflow->prID; 
                            } else if($workflow->msID > 0) { 
                                echo $workflow->msID; 
                            } else if($workflow->mrID > 0) { 
                                echo $workflow->mrID; 
                            } else { 
                                echo $workflow->moID; 
                            } 
                            ?>"class="btn btn-outline-primary"><span class="fa fa-eye"></span></a></td>							
                            <td>{{ $workflow->refId }}</td>
                            <td>{{ $workflow->namaTransaksi }}</td>
                            <td>{{ $workflow->requesterName }}</td>
                            <td><?php if($workflow->dateCreated) { echo date("Y-m-d", strtotime($workflow->dateCreated)); } ?></td>
                            @if($history)    
                                <td>   
                                @if($workflow->status > 1)
                                    Rejected
                                @else 
                                    Approved
                                @endif     
                                </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
            <div class=" content-box-footer">
                <div class="row">
                    <div class="col-md-6 text-left col-xs-12">
                    Page {{ $workflows["page"] }} of <span id="pageCount">{{ $workflows["pageCount"] }}</span>
                    Total Row <span id="totalRow">{{ $workflows["rowCount"] }}</span>
                    </div>
                    <div class="col-md-6  col-xs-12">
                        <ul class="pagination pull-right">
                            <li class="page-item ">
                                <button class="page-link" tabindex="-1" name="first" title="First Page">First
                                </button>
                            </li>
                            <li class="page-item" id="pagination-prev">
                                <a class="page-link" title="Prev Page">
                                    <i class="fa fa-step-backward"></i></a>
                            </li>
                            <li class="page-item" id="pagination-next">
                                <a class="page-link" title="Next Page">
                                    <i class="fa fa-step-forward"></i></a></li>
                            <li class="page-item">
                                <button class="page-link" name="last" title="Last Page">Last</button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes.includes_footer')
     <script>
             <?php $workflows["rows"] = []; ?>
            var paginationParameter = ('{!! json_encode($workflows)!!}');
            var currentUrl = "{{url($currentPrefix) }}";
        </script>
    <script type="text/javascript" src="{{URL::asset("/assets/js/pagination.js") }}"></script>
@endsection