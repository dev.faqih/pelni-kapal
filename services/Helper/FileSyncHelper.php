<?php
/**
 * Created by IntelliJ IDEA.
 * User: almustafa dekaulitz (sulaimanfahmi@gmail.com)
 * Date: 15/07/2018
 * Time: 22:53
 */

namespace Services\Helper;


use App\Http\Models\LogsyncoracleModel;
use Carbon\Carbon;

class FileSyncHelper
{
    public $url;
    protected $path;
    protected $rawFile;
    protected $nameFile, $file;
    protected $hasFile = false;


    /**
     * @param $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @param $nameFile
     * @return $this
     */
    public function setNameFile($nameFile)
    {
        $this->nameFile = $nameFile;
        return $this;
    }

    private function getRawFile()
    {
        try {
            $this->rawFile = file_get_contents($this->url);
        } catch (\Exception $e) {
            (new LogsyncoracleModel)->setLogSync("Download Image : ".$this->url, Carbon::now()->toDateTimeString(), $e->getMessage(), "failed");
            $this->rawFile = null;
        }

        return $this;
    }

    public function productFile()
    {
        $this->path = BASE_PATH . "../public/upload/items";
//        if (!file_exists($this->path)) {
//            if (!mkdir($this->path, 0777, true)) {
//                return $this;
//            }
//        }
        return $this;
    }


    public function reportFile()
    {
        $this->path = "./upload/reports/jrxml/";
        if (!file_exists($this->path)) {
            mkdir($this->path, 0777, true);
        }
        return $this;
    }

    public function store()
    {
        if ($this->getRawFile()) {
            file_put_contents($this->path . "/" . $this->nameFile, $this->rawFile);
            $this->setFile("upload/items/" . $this->nameFile);
            $this->hasFile = true;
        }

    }

    public function storingKapal()
    {
        if ($this->getRawFile()) {
            file_put_contents($this->path . "/" . $this->nameFile, $this->rawFile);
            $this->setFile($this->nameFile);
        }

    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    public function hasFile()
    {
        return $this->hasFile;
    }

}