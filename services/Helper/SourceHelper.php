<?php
/**
 * Created by IntelliJ IDEA.
 * User: almustafa dekaulitz (sulaimanfahmi@gmail.com)
 * Date: 19/06/2018
 * Time: 1:50
 */

namespace Services\Helper;


class SourceHelper
{
    static function pullLatestSources()
    {
        $username = getenv("GIT_USERNAME");
        $password = getenv("GIT_PASSWORD");
        exec('git reset --hard');
        exec("git pull https://" . $username . ":" . $password . "@gitlab.com/dekaulitz/pelni-dev.git master", $result);
        foreach ($result as $res) {
            print_r($res);
        }
        exec("cd ..");
        exec("composer dump-autoload -o");
    }
}
