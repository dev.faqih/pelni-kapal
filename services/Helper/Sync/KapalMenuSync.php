<?php
/**
 * Created by IntelliJ IDEA.
 * User: almustafa dekaulitz (sulaimanfahmi@gmail.com)
 * Date: 01/10/2018
 * Time: 11:23
 */

function getMenu()
{
    $url = $GLOBALS["env"]["HEAD_OFFICE_URL"] . "/api/sync_menu";
    $syncTable = (new \App\Http\Models\SyncUtilities\SyncTableHandlerModel())->getQuery()->where("sync_table.id", "=", 5)->first();
    $fetch = true;
    $page = 1;
    do {
        if ($syncTable) {

            $syncHandler = new \App\Http\Handler\CurlHandler();
            try {
                $response = $syncHandler->setUrl($url)->send();
                $res = json_decode($response, false);
                $no = 1;
                if (!empty($res->data))
                    foreach ($res->data as $item) {
                        echo "data " . $no;
                        \App\Http\Models\AdministratormenuModel::storeFromHO($item);
                        $lastUpdate = $item->date_updated;
                        $no = $no + 1;
                    }
                else $fetch = false;
                $sync = \App\Http\Models\SyncUtilities\SyncTableHandlerModel::find($syncTable->id);
                $sync->last_sync = $lastUpdate;
                $sync->update();
            } catch (Exception $e) {
                echo $e->getMessage();
            }
            $page = $page + 1;
        } else {
            $fetch = false;
        }
    } while ($fetch);
}