<?php
/**
 * Created by IntelliJ IDEA.
 * User: almustafa dekaulitz (sulaimanfahmi@gmail.com)
 * Date: 01/10/2018
 * Time: 11:23
 */

function getUsers()
{
    $url = $GLOBALS["env"]["HEAD_OFFICE_URL"] . "/api/sync_users";
    $syncTable = (new \App\Http\Models\SyncUtilities\SyncTableHandlerModel())->getQuery()->where("sync_table.id", "=", 3)->first();
    $fetch = true;
    $page = 1;
    do {
        if ($syncTable) {

            $lastUpdate = \Carbon\Carbon::now()->toAtomString();
            $lastUpdate = !empty($syncTable->last_sync) ? \Carbon\Carbon::parse($syncTable->last_sync)->toAtomString() : $lastUpdate;
            $url = $url . "?date_updated=" . $lastUpdate . "&kapalId=" . $GLOBALS["env"]["KAPAL_ID"] . "&page=" . $page;
            $syncHandler = new \App\Http\Handler\CurlHandler();
            try {
                $response = $syncHandler->setUrl($url)->send();
                $res = json_decode($response, false);
                $no = 1;
                foreach ($res->data as $item) {
                    echo "data " . $no;
                    \App\Http\Models\UsersModel::storeFromHO($item);
                    $lastUpdate = $item->date_updated;
                    $no = $no + 1;
                }
                $sync = \App\Http\Models\SyncUtilities\SyncTableHandlerModel::find($syncTable->id);
                $sync->last_sync = $lastUpdate;
                $sync->update();
            } catch (Exception $e) {
                echo $e->getMessage();
            }
            $page = $page + 1;
        } else {
            $fetch = false;
        }
    } while ($fetch);
}