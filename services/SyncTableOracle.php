<?php
/**
 * Created by IntelliJ IDEA.
 * User: almustafa dekaulitz (sulaimanfahmi@gmail.com)
 * Date: 17/09/2018
 * Time: 1:40
 */

use App\Http\Models\ItemModel;
use App\Http\Models\PopenerimaanModel;
use App\Http\Models\ItempopenerimaanModel;
use App\Http\Models\PopenerimaanioModel;
use App\Http\Models\ItempopenerimaanioModel;
use App\Http\Models\KapalModel;
use App\Http\Models\VoyageModel;
use App\Http\Models\LokasiitemModel;
use App\Http\Models\ItemonhandModel;
use App\Http\Models\AdjustmentModel;
use App\Http\Models\AdjustmentLogModel;
use App\Http\Models\ItemcategoryModel;

use App\Http\Models\StgpelniscmrcvintModel;
use App\Http\Models\StgpelniscmrcvtrxintModel;
use App\Http\Models\StgpelniprintModel;
use App\Http\Models\StgpelniscmmoiintModel;
use App\Http\Models\StgpelniscmmotintModel;
use App\Http\Models\StgpelniiotintModel;
use App\Http\Models\StgpelniscmmiscrcvintModel;   

function getStagingData($stag)
{
    switch ($stag->target_table) {
        case "item":
            (new ItemModel())->syncFromOracleHO($stag);
            break;
        case "popenerimaan":
            (new PopenerimaanModel())->syncFromOracleHO($stag);
            break;
        case "itempopenerimaan":
            (new ItempopenerimaanModel())->syncFromOracleHO($stag);
            break;
        case "popenerimaanio":
            (new PopenerimaanioModel())->syncFromOracleHO($stag);
            break;
        case "itempopenerimaanio":
            (new ItempopenerimaanioModel())->syncFromOracleHO($stag);
            break;
        case "kapal":
            (new KapalModel())->syncFromOracleHO($stag);
            break;
        case "voyages":
            (new VoyageModel())->syncFromOracleHO($stag);
            break;
        case "lokasiitem":
            (new LokasiitemModel())->syncFromOracleHO($stag);
            break;
        case "item_onhand":
            (new ItemonhandModel())->syncFromOracleHO($stag);
            break;
        case "pelni_scm_adj_trx_v":
            (new AdjustmentModel())->syncFromOracleHO($stag);
            break;
        case "pelni_scm_others_trx_v":
            (new AdjustmentLogModel())->syncFromOracleHO($stag);
            break;
        case "itemcategory":
            (new ItemcategoryModel())->syncFromOracleHO($stag);
            break;
        default:
            echo "no table defined", PHP_EOL;
            break;
    }
}


function postStagingData()
{    
    (new StgpelniprintModel())->prepareSaveToOracle(); 
    (new StgpelniscmrcvintModel())->prepareSaveToOracle(); 
    (new StgpelniscmrcvtrxintModel())->prepareSaveToOracle(); 
    (new StgpelniscmmoiintModel())->prepareSaveToOracle();  
    (new StgpelniscmmotintModel())->prepareSaveToOracle(); 
    (new StgpelniiotintModel())->prepareSaveToOracle(); 
    (new StgpelniscmmiscrcvintModel())->prepareSaveToOracle();    
}
