<?php
/**
 * Created by IntelliJ IDEA.
 * User: almustafa dekaulitz (sulaimanfahmi@gmail.com)
 * Date: 19/06/2018
 * Time: 1:53
 */

use Illuminate\Database\Capsule\Manager;
use Carbon\Carbon;

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . "/../config/env.php";
$env = $environment["development"];
$conn = new Manager();
$conn->addConnection($env["database"]);
$conn->setAsGlobal();
$conn->bootEloquent();
Request::instance();
define("BASE_PATH", __DIR__ . "./");

class Request
{
    public $input;
    private static $instance;

    static function instance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    static function input($value)
    {

    }
}