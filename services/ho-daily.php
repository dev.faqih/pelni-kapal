<?php
/**
 * Created by IntelliJ IDEA.
 * User: almustafa dekaulitz (sulaimanfahmi@gmail.com)
 * Date: 03/10/2018
 * Time: 16:08
 */

include 'conf.php';
use App\Http\Models\SyncerrorModel;

include 'SyncTableOracle.php';

(new SyncerrorModel())->syncFromOracleHO();

$syncTable = new \App\Http\Models\SyncUtilities\StagingSyncTableModel();
$syncTable->getQuery()->whereIn("staging_sync_table.id", [1, 2, 3, 11, 12]);
// $syncTable->getQuery()->whereIn("staging_sync_table.id", [3]);
$syncSchedule = $syncTable->getQuery()->get();

foreach ($syncSchedule as $sync) {
    getStagingData($sync);
}
