<?php

include 'conf.php';

use App\Http\Models\SyncerrorModel;

include 'SyncTableOracle.php';

(new SyncerrorModel())->syncFromOracleHO();

$syncTable = new \App\Http\Models\SyncUtilities\StagingSyncTableModel();
$syncTable->getQuery()->whereIn("staging_sync_table.id", [8]);
$syncSchedule = $syncTable->getQuery()->get();

foreach ($syncSchedule as $sync) {
    getStagingData($sync);
}
