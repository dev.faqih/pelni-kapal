<?php
/**
 * Created by IntelliJ IDEA.
 * User: almustafa dekaulitz (sulaimanfahmi@gmail.com)
 * Date: 03/10/2018
 * Time: 16:08
 */

include 'conf.php';
use App\Http\Models\LogsyncoracleModel;

include 'SyncTableOracle.php';

LogsyncoracleModel::trimLogSync();
postStagingData();
