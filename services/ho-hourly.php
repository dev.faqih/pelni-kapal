<?php
/**
 * Created by IntelliJ IDEA.
 * User: almustafa dekaulitz (sulaimanfahmi@gmail.com)
 * Date: 03/10/2018
 * Time: 16:08
 */

include 'conf.php';
use App\Http\Models\SyncerrorModel;
use App\Http\Models\LogsyncoracleModel;

include 'SyncTableOracle.php';

(new SyncerrorModel())->syncFromOracleHO();

$syncTable = new \App\Http\Models\SyncUtilities\StagingSyncTableModel();
$syncTable->getQuery()->whereIn("staging_sync_table.id", [4, 5, 7, 8, 9]);
// $syncTable->getQuery()->whereIn("staging_sync_table.id", [4, 9]);
$syncSchedule = $syncTable->getQuery()->get();

LogsyncoracleModel::trimLogSync();
foreach ($syncSchedule as $sync) {
    getStagingData($sync);
}

//postStagingData();
