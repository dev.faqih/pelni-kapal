<?php
/**
 * Created by IntelliJ IDEA.
 * User: almustafa dekaulitz (sulaimanfahmi@gmail.com)
 * Date: 02/10/2018
 * Time: 13:54
 */
include 'conf.php';

\App\Http\Models\KapalModel::getKapal();
\App\Http\Models\AdministratormenuModel::getMenu();
\App\Http\Models\UsersModel::getUsers();
\App\Http\Models\RolesModel::getRole();
\App\Http\Models\AdministratorgroupaccessModel::getMenuAccess();
\App\Http\Models\LokasiitemModel::getLokasiItem();
\App\Http\Models\VoyageModel::getVoyages();
\App\Http\Models\ItemonhandModel::getItemOnHand();


/**
 * hourly
 */
\App\Http\Models\ItemModel::getItem();
\App\Http\Models\PopenerimaanioModel::getPOIO();
\App\Http\Models\PopenerimaanModel::getPo();