<?php

/**
 * Created by IntelliJ IDEA.
 * User: almustafa dekaulitz (sulaimanfahmi@gmail.com)
 * Date: 02/10/2018
 * Time: 19:53
---update eko tgl 6/5/2020 ini proses untuk ho ke kapal user, role, item, kapal, voyage 
*/
include 'conf.php';

use App\Http\Models\SyncerrorModel;
use App\Http\Models\AdministratorgroupaccessModel;
use App\Http\Models\AdministratormenuModel;
use App\Http\Models\ItemModel;
use App\Http\Models\KapalModel;
use App\Http\Models\RolesModel;
use App\Http\Models\UsersModel;
use App\Http\Models\VoyageModel;
use App\Http\Models\LokasiitemModel;
use App\Http\Models\ItemonhandModel;
use App\Http\Models\SystemparameterModel;
use App\Http\Models\ItemcategoryModel;
use App\Http\Models\KapallokasiitemModel;
use App\Http\Models\RolelokasiitemModel;
use App\Http\Models\RolecategoryModel;
use App\Http\Models\AccountModel;
use App\Http\Models\AdjustmentModel;
use App\Http\Models\AdjustmentLogModel;
use App\Http\Models\LaporanpelumasModel;
use App\Http\Models\LaporanbbmModel;

(new SyncerrorModel())->syncFromHOKapal();
KapalModel::getKapal();
ItemModel::getItem();
AdministratormenuModel::getMenu();
UsersModel::getUsers();
RolesModel::getRole();
SystemparameterModel::getSystemParameter();
ItemcategoryModel::getItemCategory();
VoyageModel::getVoyages();
LokasiitemModel::getLokasiItem();

AdministratorgroupaccessModel::getMenuAccess();
KapallokasiitemModel::getKapalLokasiItem();
AdjustmentModel::getAdjustment();
AdjustmentLogModel::getAdjustmentLog();
ItemonhandModel::getItemOnHand();
RolelokasiitemModel::getRoleLokasiItem();
RolecategoryModel::getRoleCategory();
AccountModel::getAccount();

(new LaporanpelumasModel())->cronjobPelumas();
(new LaporanbbmModel())->cronjobBbm();
