<?php
/**
 * Created by IntelliJ IDEA.
 * User: almustafa dekaulitz (sulaimanfahmi@gmail.com)
 * Date: 30/09/2018
 * Time: 20:47
--- update eko tgl 6/5/2020 ini proses kirim PR kapal ke HO
 */
include 'conf.php';
use App\Http\Models\SyncerrorModel;
use App\Http\Models\PopenerimaanModel;
use App\Http\Models\PopenerimaanioModel;
use App\Http\Models\LogsynckapalModel;
use App\Http\Base\BaseController;

BaseController::syncConsole();
(new SyncerrorModel())->syncFromHOKapal();
LogsynckapalModel::trimLogSync();
PopenerimaanModel::getPo();
PopenerimaanioModel::getPOIO();
