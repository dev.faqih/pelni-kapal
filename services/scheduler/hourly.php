<?php
/**
 * Created by IntelliJ IDEA.
 * User: almustafa dekaulitz (sulaimanfahmi@gmail.com)
 * Date: 30/09/2018
 * Time: 20:47
 */
include './../conf.php';
include './../Helper/SyncTableOracle.php';

$syncTable = new \App\Http\Models\SyncUtilities\StagingSyncTableModel();
$syncTable->getQuery()->whereIn("staging_sync_table.id", [9]);
$syncSchedule = $syncTable->getQuery()->get();

foreach ($syncSchedule as $sync) {
    getStagingData($sync);
}


//(new \App\Http\Models\ItemModel())->syncFromOracleHO();