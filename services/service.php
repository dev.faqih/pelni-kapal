<?php
/**
 * Created by IntelliJ IDEA.
 * User: almustafa dekaulitz (sulaimanfahmi@gmail.com)
 * Date: 20/06/2018
 * Time: 18:27
 */
include 'conf.php';
$client = new Predis\Client($env["redis"]);
$pubsub = $client->pubSubLoop();

try {
    $pubsub->subscribe('sync');
} catch (\Predis\PredisException $exceeption) {
    echo \Carbon\Carbon::now()->toDateTimeString() . " " . $exception->getMessage();

} catch (Exception $exception) {
    echo \Carbon\Carbon::now()->toDateTimeString() . " " . $exception->getMessage();
}

foreach ($pubsub as $message) {
    switch ($message->kind) {
        case 'subscribe':
            echo \Carbon\Carbon::now()->toDateTimeString() . " Subscribed to {$message->channel}", PHP_EOL;
            break;
        case 'message':

            $payload = json_decode($message->payload);
            if ($message->channel == "sync") {
                $mainUrl = $GLOBALS["env"]["HEAD_OFFICE_URL"];
                $sync = new \App\Http\Models\SyncUtilities\SyncHandlersModel();
                $sync->getQuery()->where("sync.need_sync", "=", 1);
                $syncData = $sync->getQuery()->get();
                foreach ($syncData as $sync) {
                    $url = "";
                    $url .= $mainUrl . "/api/syncronize?table=" . $sync->table_name;
                    if ($sync->server_id != 0) $url .= "&server_id=" . $sync->server_id;
                    $table = \Illuminate\Database\Capsule\Manager::table($sync->table_name);
                    if ($sync->table_name == "item_onhand")
                        $table->where("ID", "=", $sync->local_id);
                    else
                        $table->where("id", "=", $sync->local_id);
                    $model = $table->first();
                    $syncData = collect($model)->toJson();
                    $syncHandler = new \App\Http\Handler\CurlHandler();
                    $syncHandler->setBody(collect($model)->toArray());
                    $syncHandler->setUrl($url);
                    $test = $syncHandler->send();
                    $result = json_decode($test);
                    $syncUpdate = \App\Http\Models\SyncUtilities\SyncHandlersModel::find($sync->id);
                    $syncUpdate->server_id = $result->data;
                    if(intval($result->data) > 1) {
                        $syncUpdate->need_sync = 0;
                    } else {
                        $syncUpdate->need_sync = 1;
                    }
                    $syncUpdate->sync_date = \Carbon\Carbon::now()->toDateTimeString();
                    $syncUpdate->last_sync = \Carbon\Carbon::now()->toDateTimeString();

                    $noTransaksi = $sync->local_id;
                    $namaTable = $sync->table_name;
		    echo "table: ".$namaTable. "", PHP_EOL;
                    switch($sync->table_name) {
                        case "permintaanbarang"         : $noTransaksi = $model->prNumber; $namaTable = "Permintaan Barang"; break;
                        case "itempermintaanbarang"     : $noTransaksi = $model->prNumber; $namaTable = "Item Permintaan Barang"; break;
			case "itempermintaanbarang_"	: $noTransaksi = $model->prNumber; $namaTable = "Item Permintaan Barang"; break;

                        case "popenerimaan"             : $noTransaksi = $model->poNumber; $namaTable = "PO Penerimaan"; break;
                        case "itempopenerimaan"         : $noTransaksi = $model->poNumber; $namaTable = "Item PO Penerimaan"; break;

                        case "penerimaanbarang"         : $noTransaksi = $model->receiptNumber; $namaTable = "Penerimaan Barang"; break;
                        case "itempenerimaanbarang"     : $noTransaksi = $model->penerimaanBarangId; $namaTable = "Item Penerimaan Barang"; break;

                        case "popenerimaanio"           : $noTransaksi = $model->ioNumber; $namaTable = "Penerimaan IO"; break;
                        case "itempopenerimaanio"       : $noTransaksi = $model->ioNumber; $namaTable = "Item Penerimaan IO"; break;

                        case "penerimaanbarangio"       : $noTransaksi = $model->rcNumber; $namaTable = "Penerimaan Barang IO"; break;
                        case "itempenerimaanbarangio"   : $noTransaksi = $model->rcNumber; $namaTable = "Item Penerimaan Barang IO"; break;

                        case "pindahbarangio"           : $noTransaksi = $model->ioNumber; $namaTable = "Transfer IO"; break;
                        case "itempindahbarangio"       : $noTransaksi = $model->ioNumber; $namaTable = "Item Transfer IO"; break;

                        case "pindahbarangsubinv"       : $noTransaksi = $model->moNumber; $namaTable = "Transfer SubInventory"; break;
                        case "itempindahbarangsubinv"   : $noTransaksi = $model->moNumber; $namaTable = "Item Transfer SubInventory"; break;

                        case "penggunaanbarang"         : $noTransaksi = $model->miNumber; $namaTable = "Penggunaan Barang"; break;
                        case "itempenggunaanbarang"     : $noTransaksi = $model->miNumber; $namaTable = "Item Penggunaan Barang"; break;
			case "itempenggunaanbarang_"	: $noTransaksi = $model->miNumber; $namaTable = "Item Penggunaan Barang"; break;

                        case "penggunaanmakanan"        : $noTransaksi = $model->msNumber; $namaTable = "Penggunaan Makanan"; break;
                        case "itempenggunaanmakanan"    : $noTransaksi = $model->msNumber; $namaTable = "Item Penggunaan Makanan"; break;
			case "itempenggunaanmakanan_copy1"	: $noTransaksi = $model->msNumber; $namaTable = "Item Penggunaan Makanan"; break;

                        case "penerimaanmakanan"        : $noTransaksi = $model->mrNumber; $namaTable = "Penerimaan Makanan"; break;
                        case "itempenerimaanmakanan"    : $noTransaksi = $model->mrNumber; $namaTable = "Item Penerimaan Makanan"; break;

                        case "workflow"                 : $noTransaksi = $model->namaTransaksi . " #" . $model->refId; $namaTable = "Workflow"; break;

                        case "stg_pelni_iot_int"            :
                            $noTransaksi = $model->transaction_number . " item " . $model->item_segment1; $namaTable = "Transfer/Penerimaan Barang IO"; break;
                        case "stg_pelni_scm_misc_rcv_int"   :
                            $noTransaksi = $model->transaction_number . " item " . $model->item_segment1; $namaTable = "Penerimaan/Penggunaan Makanan"; break;
                        case "stg_pelni_pr_int"             :
                            $noTransaksi = $model->req_number_segment1 . " item " . $model->item_segment1; $namaTable = "Permintaan Barang"; break;
                        case "stg_pelni_scm_rcv_int"        :
                            $noTransaksi = $model->transaction_number; $namaTable = "Penerimaan Barang"; break;
                        case "stg_pelni_scm_rcv_trx_int"    :
                            $noTransaksi = $model->transaction_number . " item " . $model->item_segment1; $namaTable = "Item Penerimaan Barang"; break;
                        case "stg_pelni_scm_moi_int"        :
                            $noTransaksi = $model->transaction_number . " item " . $model->item_segment1; $namaTable = "Penggunaan Barang"; break;
                        case "stg_pelni_scm_mot_int"        :
                            $noTransaksi = $model->transaction_number . " item " . $model->item_segment1; $namaTable = "Transfer Sub-Inventory"; break;
                    }
                    try {
                        $model = $syncUpdate->save();
                        echo "sync success on ".$namaTable."-". \Carbon\Carbon::now()->toDateTimeString() . "", PHP_EOL;
                        (new App\Http\Models\LogsynckapalModel())->setLogSync($namaTable . " Kapal to HO | " . " #" . $noTransaksi, \Carbon\Carbon::now()->toDateTimeString(),"", "success");
                    } catch (Exception $exception) {
                        (new App\Http\Models\LogsynckapalModel())->setLogSync($namaTable . " Kapal to HO | " . " #" . $noTransaksi, \Carbon\Carbon::now()->toDateTimeString(), $exception->getMessage(), "failed");
                        echo $exception->getMessage(), PHP_EOL;
                    }
                }
            } else if ($message->channel == "sync-manual-item") {

            }
    }
}
