<?php
/**
 * Created by IntelliJ IDEA.
 * User: almustafa dekaulitz (sulaimanfahmi@gmail.com)
 * Date: 12/08/2018
 * Time: 22:39
 */
include 'conf.php';

use Carbon\Carbon;


$syncBulk = new \App\Http\Models\SyncUtilities\SyncTableHandlerModel();
$datas = $syncBulk->getQuery()->get();

foreach ($datas as $data) {
    $connection = hasInternetConnection();
    \Illuminate\Database\Capsule\Manager::connection()->beginTransaction();
    if ($connection) {
        $url = $GLOBALS["env"]["HEAD_OFFICE_URL"] . "/syncronize/bulk?table=" . $data->table_name."&limit=100";
        $syncHandler = new \App\Http\Handler\CurlHandler();
        $syncHandler->setUrl($url);
        $test = $syncHandler->send();
        $results = json_decode($test, true);

        foreach ($results["data"] as $res) {
            $syncModel = new \App\Http\Models\SyncUtilities\SyncHandlersModel();
            $syncModel->getQuery()->where("server_id", "=", $res["id"]);
            $syncModelExist = $syncModel->getQuery()->first();
            if ($syncModelExist) {
                $syncUpdate = \Illuminate\Database\Capsule\Manager::table($data->table_name);
                $syncUpdate->where("id", "=", $syncModelExist->local_id);
                if (!$syncUpdate->update($res)) \Illuminate\Database\Capsule\Manager::connection()->rollBack();
            } else {
                $syncStore = \Illuminate\Database\Capsule\Manager::table($data->table_name);
                try {
                    $result = $syncStore->insertGetId($res);
                    $sync = new \App\Http\Models\SyncUtilities\SyncHandlersModel();
                    $sync->local_id = $result;
                    $sync->server_id = $res["id"];
                    $sync->table_name = $data->table_name;
                    $sync->marked_deleted = 0;
                    $sync->need_sync = 0;
                    $sync->created_at = Carbon::now()->toDateTimeString();
                    $sync->updated_at = Carbon::now()->toDateTimeString();
                    $sync->save();
                } catch (\Exception $e) {
                    \Illuminate\Database\Capsule\Manager::connection()->rollBack();
                    return $e->getMessage();
                }
            }
        }
        $syncSchedule=\App\Http\Models\SyncUtilities\SyncTableHandlerModel::find($data->id);
        $syncSchedule->last_sync=Carbon::now()->toDateTimeString();
        if(!$syncSchedule->update()) \Illuminate\Database\Capsule\Manager::connection()->rollBack();
        \Illuminate\Database\Capsule\Manager::connection()->commit();
    }
}


function hasInternetConnection()
{
    $is_conn = false;
    $connected = @fsockopen("localhost", 8090, $errno, $errstr, $timeout = 10);
    if ($connected) {
        $is_conn = true; //action when connected
        fclose($connected);
    }
    return $is_conn;
}