CREATE TABLE public.staging_sync_table
(
    id integer DEFAULT nextval('public.staging_sync_table_id_seq'::regclass) PRIMARY KEY NOT NULL,
    staging_table varchar(25),
    target_table varchar(25),
    target_model varchar(25),
    last_sync timestamp
);
INSERT INTO public.staging_sync_table (id, staging_table, target_table, target_model, last_sync) VALUES (3, 'pelni_scm_pr_to_po_v
', 'popenerimaan', null, '2001-01-01 01:01:01.000000');
INSERT INTO public.staging_sync_table (id, staging_table, target_table, target_model, last_sync) VALUES (4, 'pelni_scm_po_lines_v', 'itempopenerimaan', null, '2001-01-01 01:01:01.000000');
INSERT INTO public.staging_sync_table (id, staging_table, target_table, target_model, last_sync) VALUES (1, 'pelni_scm_items_v', 'item', null, '2001-01-01 01:01:01.000000');
INSERT INTO public.staging_sync_table (id, staging_table, target_table, target_model, last_sync) VALUES (2, 'pelni_scm_io_v
', 'kapal', null, '2001-01-01 01:01:01.000000');
INSERT INTO public.staging_sync_table (id, staging_table, target_table, target_model, last_sync) VALUES (5, 'pelni_iot_v', 'popenerimaanio', null, '2001-01-01 01:01:01.000000');
INSERT INTO public.staging_sync_table (id, staging_table, target_table, target_model, last_sync) VALUES (6, 'pelni_iot_v', 'itempopenerimaanio', null, '2001-01-01 01:01:01.000000');
INSERT INTO public.staging_sync_table (id, staging_table, target_table, target_model, last_sync) VALUES (7, 'pelni_voyage_v', 'voyages', null, '2001-01-01 01:01:01.000000');
INSERT INTO public.staging_sync_table (id, staging_table, target_table, target_model, last_sync) VALUES (8, 'pelni_scm_item_onhand_v', 'item_onhand', null, '2001-01-01 01:01:01.000000');
INSERT INTO public.staging_sync_table (id, staging_table, target_table, target_model, last_sync) VALUES (9, 'pelni_scm_subinv_v', 'lokasiitem', null, '2001-01-01 01:01:01.000000');