CREATE TABLE public.sync_table
(
    id bigint DEFAULT nextval('public.sync_table_id_seq'::regclass) NOT NULL,
    table_name varchar(255),
    last_sync timestamp
);
INSERT INTO public.sync_table (id, table_name, last_sync) VALUES (3, 'users', '2001-01-01 01:01:01.000000');
INSERT INTO public.sync_table (id, table_name, last_sync) VALUES (6, 'administratorgroupaccess', '2001-01-01 01:01:01.000000');
INSERT INTO public.sync_table (id, table_name, last_sync) VALUES (7, 'roles', '2001-01-01 01:01:01.000000');
INSERT INTO public.sync_table (id, table_name, last_sync) VALUES (9, 'popenerimaanio', '2001-01-01 01:01:01.000000');
INSERT INTO public.sync_table (id, table_name, last_sync) VALUES (4, 'lokasiitem', '2001-01-01 01:01:01.000000');
INSERT INTO public.sync_table (id, table_name, last_sync) VALUES (1, 'item', '2001-01-01 01:01:01.000000');
INSERT INTO public.sync_table (id, table_name, last_sync) VALUES (5, 'administratormenu
', '2001-01-01 01:01:01.000000');
INSERT INTO public.sync_table (id, table_name, last_sync) VALUES (2, 'kapal', '2001-01-01 01:01:01.000000');
INSERT INTO public.sync_table (id, table_name, last_sync) VALUES (8, 'popenerimaan', '2001-01-01 01:01:01.000000');