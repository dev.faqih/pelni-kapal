--
-- PostgreSQL database dump
--

-- Dumped from database version 10.4
-- Dumped by pg_dump version 10.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: sync; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sync (
    id bigint NOT NULL,
    local_id bigint,
    server_id bigint,
    table_name character varying(255) NOT NULL,
    marked_deleted smallint DEFAULT 0,
    need_sync smallint DEFAULT 1,
    sync_error text,
    sync_date timestamp without time zone,
    last_sync timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.sync OWNER TO postgres;

--
-- Name: sync_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sync_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sync_id_seq OWNER TO postgres;

--
-- Name: sync_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sync_id_seq OWNED BY public.sync.id;


--
-- Name: sync id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sync ALTER COLUMN id SET DEFAULT nextval('public.sync_id_seq'::regclass);


--
-- Data for Name: sync; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.sync (id, local_id, server_id, table_name, marked_deleted, need_sync, sync_error, sync_date, last_sync, created_at, updated_at) VALUES (7, 2, NULL, 'item', 0, 1, NULL, NULL, NULL, '2018-08-08 15:39:49', '2018-08-08 17:45:38');


--
-- Name: sync_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sync_id_seq', 7, true);


--
-- Name: sync sync_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sync
    ADD CONSTRAINT sync_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

