--
-- PostgreSQL database dump
--

-- Dumped from database version 10.4
-- Dumped by pg_dump version 10.4

-- Started on 2018-08-12 23:35:33

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 278 (class 1259 OID 27457)
-- Name: sync_table; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sync_table (
    id bigint NOT NULL,
    table_name character varying(255),
    last_sync timestamp without time zone
);


ALTER TABLE public.sync_table OWNER TO postgres;

--
-- TOC entry 277 (class 1259 OID 27455)
-- Name: sync_table_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sync_table_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sync_table_id_seq OWNER TO postgres;

--
-- TOC entry 3009 (class 0 OID 0)
-- Dependencies: 277
-- Name: sync_table_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sync_table_id_seq OWNED BY public.sync_table.id;


--
-- TOC entry 2878 (class 2604 OID 27460)
-- Name: sync_table id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sync_table ALTER COLUMN id SET DEFAULT nextval('public.sync_table_id_seq'::regclass);


--
-- TOC entry 3003 (class 0 OID 27457)
-- Dependencies: 278
-- Data for Name: sync_table; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sync_table (id, table_name, last_sync) FROM stdin;
2	applicationparameter	2018-08-12 18:25:42
1	item	2018-08-12 18:25:43
\.


--
-- TOC entry 3010 (class 0 OID 0)
-- Dependencies: 277
-- Name: sync_table_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sync_table_id_seq', 2, true);


--
-- TOC entry 2880 (class 2606 OID 27462)
-- Name: sync_table sync_table_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sync_table
    ADD CONSTRAINT sync_table_pkey PRIMARY KEY (id);


-- Completed on 2018-08-12 23:35:34

--
-- PostgreSQL database dump complete
--

